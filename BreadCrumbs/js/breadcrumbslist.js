/**
 * BreadCrumbsList controlling class - it manages list of breadcrumbs under given crumb.
 * This files is part of the BreadCrumbs project inspired by www.jankoatwarpspeed.com
 */ 
var BreadCrumbsList = Class.create(
{
	/**
	 * UL element representing this crumb list
	 */
	breadcrumbslistul: null,
	/**
	 * pointer to the timeout ID of the awaiting for menu hide
	 */
	awaitingMenuHide: null,
	/**
	 * BreadCrumbs that holds the Crumb with this list of alternate-crumbs
	 */
	breadcrumbs: null,
	/**
	 * Crumb that holds the list of these alternate-crumbs
	 */
	crumb:null,
	
	/**
	 * Shows the menu
	 */
	show: function(crumb, breadcrumbs)
	{
		if ( crumb != null )
		{
			var pos = crumb.licrumb.positionedOffset();
			this.breadcrumbslistul.style.left = (pos[0]-8)+"px";
			this.breadcrumbslistul.style.top  = (pos[1]+crumb.licrumb.getHeight())+"px";
			
			if ( breadcrumbs != null )
			{
				this.crumb = crumb;
				this.breadcrumbs = breadcrumbs;
				this.breadcrumbs.isfocusing = true;
				this.breadcrumbs.previousFocused = null;
			}
		}
		this.breadcrumbslistul.appear();
	},
	/**
	 * Hides the menu
	 */
	hide: function()
	{
		if ( this.breadcrumbs != null )
		{
			this.breadcrumbs.previousFocused = this.crumb;
			this.breadcrumbs.isfocusing = false;
			this.breadcrumbs.focusOn(null, null);
			this.breadcrumbs = null;
		}
		this.breadcrumbslistul.fade();
	},
	/**
	 * Hides menu - wraps this.hide() to ensure proper handling and time frames
	 */
	hideMenu: function()
	{
		this.clearHideMenu();
		this.awaitingMenuHide = window.setTimeout(this.hide.bind(this), 100);
	},
	clearHideMenu: function()
	{
		if ( this.awaitingMenuHide != null )
		{
			window.clearTimeout(this.awaitingMenuHide);
			this.awaitingMenuHide = null;
		}
	},
	/**
	 * Analyzes given element in the list of breadcrumbs and extends it to be more visually appealing 
	 * (matching overall style) - adds <div class="listitem"> around <a>
	 * @param lielement LI element to be prepared
	 * @return length (in pixels) of the text in the A element inside of LI
	 */
	prepareList: function(lielement)
	{
		var cont = Element.descendants(lielement).detect(function(el)
		{ 
			return el.tagName == "A"; 
		}); // -- find a element
		
		if ( cont.descendantOf(lielement) )
		{
			var div = document.createElement("div");
			Element.addClassName(div, "listitem");
			
			div.appendChild(cont);
			lielement.appendChild(div);
		}
		
		return { 
			fontSize: parseInt(cont.getStyle("font-size")),
			len:  cont.getText().length, 
			text: cont.getText()
		};
		
		Event.observe(lielement, "mouseover", this.clearHideMenu.bindAsEventListener(this));
	},
	/**
	 * Constructor of the BreadCrumbList
	 */
	initialize: function(crumblistid)
	{
		this.breadcrumbslistul = $(crumblistid);
		
		var maxLen = 0;
		var maxEl = null;
		
		$A(this.breadcrumbslistul.childNodes).findAll(function(node) 
		{ // list all li elements in this crumblist
			return node.tagName == "LI";
		}).each(function(lielement)
		{ // for each element call prepareList()
			var ael = this.prepareList(lielement);
			
			if (ael.len*ael.fontSize > maxLen) //we need that to determine the longest text
			{
				maxLen = ael.len*ael.fontSize;
				maxEl = ael;
			}
		}
		.bind(this));

		// - calculate the right size
		var span = document.createElement("span");
		span.style.fontSize = maxEl.fontSize;
		span.innerHTML = maxEl.text.gsub(/\s+/, "&nbsp;");
		span.hide();
		$$("body")[0].appendChild(span);
		
		maxLen = span.getWidth()+40;
		
		$$("body")[0].removeChild(span);
		
		this.breadcrumbslistul.style.width = maxLen+"px";
		this.hide();
		
		Event.observe(this.breadcrumbslistul, "mouseout", this.hideMenu.bindAsEventListener(this));
		Event.observe(this.breadcrumbslistul, "mouseover", this.clearHideMenu.bindAsEventListener(this));
	}
});

/**
 * Collection of all registered BreadCrumbsList-s
 */
BreadCrumbsList.collection = $H({});
/**
 * Initilizes those breadcrumb lists that were created in advance in HTML
 */
BreadCrumbsList.preload = function()
{
	$$("ul.breadcrumblist").each(function(element)
	{
		var breadcrumbslist = new BreadCrumbsList(element);
		BreadCrumbsList.collection.set(element, breadcrumbslist);
	});
}

Event.observe(window, "load", BreadCrumbsList.preload);
