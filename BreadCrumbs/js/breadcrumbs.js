/**
 * BreadCrumbs controlling class.
 * This files is part of the BreadCrumbs project inspired by www.jankoatwarpspeed.com
 */ 
var BreadCrumbs = Class.create(
{
	/**
	 * main ul element with breadcrumbs
	 */
	breadcrumbs: null,
	/**
	 * Array with objects representing intermediate crumbs (without first and last)
	 */
	crumbs: null,
	/**
	 * the first of crumbs - it is the home crumb
	 */
	homecrumb: null,
	/**
	 * the last of crumbs - it is the current crumb
	 */
	currentcrumb: null,
	/**
	 * Informs if the breadcrumbs are focusing on one of the crumbs at the moment
	 */
	isfocusing: false,
	/**
	 * previously focused crumb
	 */
	previousFocused: null,
	/**
	 * pointer to the timeout ID of the awaiting focus call
	 */
	awaitingFocus: null,
	/**
	 * pointer to the timeout ID of the awaiting for menu show
	 */
	awaitingMenuShow: null,

	/**
	 * Checks if the separate crumbs does not take too much of the screen to show the last element
	 */
	adjustSize: function()
	{
		//current max width
		var ulwidth = this.breadcrumbs.getDimensions().width;
		//required max width
		var twidth = 0;
		//last and first elements' width
		var lastwidth = this.currentcrumb.origdim.width;
		var firstwidth = this.homecrumb.origdim.width;
		
		this.crumbs.each(function(crumb)
		{ 
			twidth += crumb.origdim.width; 
		});
		
		if ( twidth + lastwidth + firstwidth > ulwidth ) // oops we have a problem - need to shrink some of the crumbs
		{
			//first see how much space we have left
			var remainwidth = ulwidth - lastwidth - firstwidth;
			
			if ( remainwidth > BreadCrumbs.separatorWidth*this.crumbs.length ) // we are on the save side - simply resize intermediate crumbs
			{ 
				var widthpercrumb = Math.floor(remainwidth/this.crumbs.length);
				
				this.crumbs.each(function(crumb)
				{
					crumb.setWidth(widthpercrumb);
				});
			}
			else // we are kind of in a deep shit - will need to resize the last element as well
			{ 
				this.crumbs.each(function(crumb)
				{
					crumb.setWidth(0);
				});
			}
		}
		else
		{ // remember to re-set the width
			this.crumbs.each(function(crumb)
			{
				crumb.resetWidth();
			});
		}
	},

	focusOn: function(event, crumb)
	{
		
		if ( !this.isfocusing )
		{
			this.awaitingFocus = null;
			
			if ( this.previousFocused != null )
			{
				this.isfocusing = true;
				this.previousFocused.shrink();
				
				if ( this.awaitingMenuShow != null )
				{
					window.clearTimeout(this.awaitingMenuShow);
					this.awaitingMenuShow = null;
				}
			}
			
			this.previousFocused = crumb;

			if ( crumb != null )
			{
				this.isfocusing = true;
				this.previousFocused.expand();
				
				this.clearAwaitingMenuShow();
				this.awaitingMenuShow = window.setTimeout(crumb.showMenu.bind(crumb), 1000); //
			}
		}
		else if ( crumb != null )
		{
			if ( this.awaitingFocus != null )
			{
				window.clearTimeout(this.awaitingFocus);
			} 
			this.clearAwaitingMenuShow();
			this.awaitingFocus = window.setTimeout(this.focusOn.bind(this, event, crumb), 200);//.defer();
		}
	},

	/**
	 * Ensures awaitingMenuShow is stopped
	 */
	clearAwaitingMenuShow: function()
	{
		if ( this.awaitingMenuShow != null )
		{
			window.clearTimeout(this.awaitingMenuShow);
			this.awaitingMenuShow = null;
		}
	},
	
	notFocusing: function()
	{
		this.isfocusing = false;
	},
	
	/**
	 * Constructor
	 */
	initialize: function(crumbsid)
	{
		this.breadcrumbs = $(crumbsid);

		var _crumbs = $A(this.breadcrumbs.childNodes).findAll(function(node) 
		{ // list all li elements in this crumblist
			return node.tagName == "LI";
		});

		this.homecrumb = new Crumb(_crumbs.first(), this);
		this.currentcrumb = new Crumb(_crumbs.last(), this);
		
		this.crumbs = $A();
		
		_crumbs.without(this.homecrumb.licrumb, this.currentcrumb.licrumb).each(function(li)
		{
			this.crumbs.push(new Crumb(li, this));
		}
		.bind(this));
		
		this.adjustSize();
		
		Event.observe(this.breadcrumbs, "mouseout", this.focusOn.bindAsEventListener(this, null));
	}
});


/**
 * Identifies the width of the separator for crumbs.
 * IMPORTANT: Remember to keep it in sync with CSS !
 */
BreadCrumbs.separatorWidth = 10; 
/**
 * This will hold collection of references to all BreadCrumbs in the project
 */
BreadCrumbs.collection = $A();
/**
 * This loads javascript BreadCrumbs object for each ul.breadcrumbs element
 */
BreadCrumbs.preload = function()
{
	$$('ul.breadcrumbs').each(function(element)
	{
		BreadCrumbs.collection.push(new BreadCrumbs(element));
	});
}
/**
 * Enumerates over all breadcrumbs and re-adjusts their sizes
 */
BreadCrumbs.adjustSizeAll = function()
{
	BreadCrumbs.collection.each(function(breadcrumb)
	{
		breadcrumb.adjustSize();
	});
}

// -- ensure preload is invoked after loading the whole document
Event.observe(window, "load", BreadCrumbs.preload);
Event.observe(window, "resize", BreadCrumbs.adjustSizeAll);






