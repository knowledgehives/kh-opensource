/**
 * Single Crumbs controlling class.
 * This files is part of the BreadCrumbs project inspired by www.jankoatwarpspeed.com
 */
// ============================ Crumbs ==========================================
/**
 * Crumb class delivers a wrapper for a single Crumb element in the BreadCrumbs
 */
var Crumb = Class.create(
{
	/**
	 * LI element representing original crumb
	 */
	licrumb: null,
	/**
	 * The master breadcrumbs object
	 */
	breadcrumbs: null,
	/**
	 * A element representing original crumbs content
	 */
	acrumb: null,
	/**
	 * Menu with crumb list
	 */
	crumbmenu: null,
	/**
	 * Dimensions: original for the LI element
	 */
	origdim: null,
	/**
	 * Dimensions: currently set for the view (will not get updated when temporaly changing size)
	 */
	dimensions: null,
	/**
	 * Temporary width - a cache between expand() and shrink() invokations
	 */
	tmpwidth: null,
	/**
	 * Original text
	 */
	text: null,
	/**
	 * Information if the crumb is resizing
	 */
	isresizing: false,
	/**
	 * informs if the size has been changed since the original size
	 */
	isresized: false,
	
	/**
	 * Unhides menu associated with this Crumb
	 */
	showMenu: function()
	{
		if ( this.crumbmenu != null )
		{
			BreadCrumbsList.collection.get(this.crumbmenu).show(this, this.breadcrumbs);
		}
	},
	
	/**
	 * Expands given crumb to the full size of the original text
	 */
	expand: function()
	{
		if ( this.isresized )
		{
			this.resetWidth();
		}
	},
	
	/**
	 * Shrinks given crumb to the currently set size
	 */
	shrink: function()
	{
		if ( this.isresized && this.tmpwidth != null )
		{
			this.setWidth(this.tmpwidth + BreadCrumbs.separatorWidth);
		}
	},
	
	
	/**
	 * Updates width and dimentions of the this crumb
	 */
	setWidth: function(width)
	{
		this.isresized = true;
		
		var w = ((width < BreadCrumbs.separatorWidth) ? 0 : (width - BreadCrumbs.separatorWidth));
		
		new Effect.Morph(this.licrumb,
			{
				style: 'width:'+w+'px',
				duration: 0.5,
				afterFinish: this.afterSetWidth.bind(this)
			});
	},
	
	/**
	 * Operation invoked after when the final width is reached
	 */
	afterSetWidth: function()
	{
		this.dimensions = this.licrumb.getDimensions();
		this.tmpwidth = parseInt(this.licrumb.style.width, 10);
		
		if ( this.acrumb != null )
		{
			var fsize = parseInt(this.acrumb.getStyle("font-size"), 10);
			var len = Math.floor(this.dimensions.width/fsize);

			if ( len > this.text.length )
			{
				this.acrumb.innerHTML = this.text;
			}
			else if ( len > 0 )
			{
				this.acrumb.innerHTML = this.text.substr(0, len);
			}
			else
			{
				this.acrumb.innerHTML = "&nbsp;";
			}
		}
		
		this.breadcrumbs.isfocusing = false;
	},

	/**
	 * Returns width to the original value
	 */	
	resetWidth: function()
	{
		var w = this.origdim.width; // - BreadCrumbs.separatorWidth;
		var newwidth = 'width:'+w+'px';

		new Effect.Morph(this.licrumb,
			{
				style: newwidth,
				duration: 0.5,
				afterFinish: this.afterResetWidth.bind(this)
			});
	},

	/**
	 * Called after reseting the width
	 */
	afterResetWidth: function()
	{
		this.dimensions = this.origdim;
		this.acrumb.innerHTML = this.text;
		this.breadcrumbs.isfocusing = false;
	},
	  
	/**
	 * Constructor
     * @param licrumb LI element representing original crumb
	 */
	initialize: function(licrumb, breadcrumbs)
	{
		this.licrumb = licrumb;
		this.breadcrumbs = breadcrumbs;
		
		this.origdim = this.licrumb.getDimensions();
		this.dimensions = this.origdim;
//		this.acrumb = $$("#"+this.licrumb.identify()+" a")[0];
		this.acrumb = $A(this.licrumb.childNodes).find(function(node) 
		{ // list all li elements in this crumblist
			return node.tagName == "A";
		});
		
		this.crumbmenu = $(this.acrumb.readAttribute("rel"));
		
		if ( Object.isUndefined(this.acrumb) )
		{
			this.acrumb = null;
		}

		this.text = ( this.acrumb != null ) ? ( this.acrumb.getText() ) : "";
		
		Event.observe(this.licrumb, "mouseover", this.breadcrumbs.focusOn.bindAsEventListener(this.breadcrumbs, this, this));
		
	}
});

Object.extend(Element.prototype, {
	getText: function(){
		return (!Object.isUndefined(this.textContent)) ? this.textContent : this.innerText;
	}
});

