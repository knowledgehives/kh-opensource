<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="yuipp" tagdir="/tags" %>
<%@ taglib prefix="c" 	   uri="http://java.sun.com/jsp/jstl/core"%>
<%--

This file demonstrates how to use tags/bookmarks.tag and tags/bookmarksWindows.tag within jsp page.

--%>
	<c:if test="${ not empty person }">
			<yuipp:bookmarks dir="" newUri="" uri="${person.uri}" mbox="${person.mbox}" standalone="true" addform="${param.addform}" addurl="${param.addurl}" addname="${param.addname}" seluri="${param.id}"/>
	</c:if>
	
	<yuipp:bookmarksWindows />

