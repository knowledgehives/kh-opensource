/**
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  */
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Example of AbstractFilter implementation.
 * 
 * This class contains implementation of the processFilter method.
 * It shows hot to build the result to be accepted by JS code on the client side
 * 
 */
public class PlainTextFilter extends AbstractFilter 
{

	public PlainTextFilter(String _filterValue, String _user, List<String> _possibleTypes, SearchScope _searchScope, FilterSubTypes _filterSubType)
	{
		super(_filterValue, _user, _possibleTypes, _searchScope, _filterSubType);
	}
	
	/* (non-Javadoc)
	 * @AbstractFilter#processFilter()
	 */
	@Override
	public Map<String, ? extends Object> processFilter()
	{
		//array for keeping result
		JSONArray resultJson = new JSONArray();
		
		//Example service that can do the filtering
		SearchQuery query = new SearchQuery(filterValue, false);

		//Adding the types of resources that can be returned
		for ( String possibleType : possibleTypes ) 
		{
			query.addResourceType(ResourceType.valueOf(possibleType));
		}
		
		//Performing query
		List<SearchResult> searchResults = SearchService.findInIndex(query).getResources();
				
		//Filling result array with results
		for (SearchResult result : searchResults) 
		{
			JSONObject jo = new JSONObject();
			jo.put("uri", result.getURI());
			jo.put("type", result.getType());
			jo.put("label", result.getLabel());
			jo.put("content", result.getDescription());
			jo.put("creator", result.getCreatorName());
			jo.put("creatorId", result.getCreatorID());
			jo.put("group", groupId);
				
			resultJson.add(jo);
		}
		
		Map<String,Object> finalResult = new HashMap<String,Object>();

		//Results can be grouped. In this case group should contain names of the groups and groupsID
		finalResult.put("result", resultJson);
		finalResult.put("groups", null);
		
		return finalResult;
	}

}
