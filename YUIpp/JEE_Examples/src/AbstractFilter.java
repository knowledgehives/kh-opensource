/**
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *  */
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Example of AbstractFilter class that works with filtering module in JS code
 */
abstract public class AbstractFilter 
{
	
	String filterValue;
	String user;
	List<String> possibleTypes = new ArrayList<String>();
	SearchScope searchScope;
	FilterSubTypes filterSubType;
	
	/**
	 * @param _filterValue
	 * @param _user
	 * @param _possibleTypes
	 * @param _searchScope
	 * @param _filterSubType
	 */
	public AbstractFilter(String _filterValue, String _user, List<String> _possibleTypes, SearchScope _searchScope, FilterSubTypes _filterSubType)
	{
		this.filterValue = _filterValue;
		this.user = _user;
		if ( _possibleTypes==null )
		{
			this.possibleTypes = Collections.emptyList();
		}
		else 
		{
			this.possibleTypes = _possibleTypes;
		}
		this.searchScope = _searchScope;
		this.filterSubType = _filterSubType;
	}
	
	/**
	 * You will need to implement this method to deliver your own filtering service
	 */
	abstract public Map<String,? extends Object> processFilter();
	
	public String getJSON()
	{
		JSONObject jomain = getJSONObject(); 
		return jomain.toString();
	}
	
	/**
	 *  Format of the return:
	 * 	- count - number of results found
	 * 	- results - array with results
	 *  - groups - array with results' groups
	 *  - person - currently logged person
	 *  
	 *  If the filter returns no results then only count:0 will be returned
	 *  (along with person)
	 * 
	 * @return JSON representation of the result
	 */
	public JSONObject getJSONObject()
	{
		Map<String, ? extends Object> results = processFilter();
		JSONArray result = new JSONArray();
		JSONObject jomain = null;
		
		if ( results!=null )
		{
			result = (JSONArray)results.get("result");
			
			if ( result!=null && result.size()>0 )
			{
				jomain = new JSONObject();
				jomain.put("count", result.size());
				jomain.put("results", result);
				jomain.put("groups", results.get("groups"));
			}
		}

		if ( jomain == null)
		{
			jomain = new JSONObject();
			jomain.put("count",0);
		}
		jomain.put("person", this.user);
		
		return jomain;
	}
}
