
<%@ tag description="Bookmarks management tag" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/taglibs/appapi.tld" prefix="appapi"%>
<%@ taglib prefix="helpers" tagdir="/WEB-INF/tags" %>
<%@ attribute name="dir" rtexprvalue="true"%>
<%@ attribute name="standalone" rtexprvalue="true"%>
<%@ attribute name="newUri" rtexprvalue="true"%>
<%@ attribute name="addform"%>
<%@ attribute name="addurl"%>
<%@ attribute name="addname"%>
<%@ attribute name="seluri"%>
<%@ attribute name="mbox"%>
<%@ attribute name="uri"%>

<c:choose>
	<c:when test="${empty seluri}">
		<c:set var="stritems2" value="${appapi:getJSONByUid(uri,2)}"></c:set>
		<script type="text/javascript">
			var stritems2 = ${stritems2};
		</script>
	</c:when>
	<c:otherwise>
		<c:set var="stritems2" value="${appapi:getPaths(seluri,uri,false,true)}"></c:set>
		<script type="text/javascript">
			var stritems2 = ${stritems2};
			if (typeof stritems2.trees != 'undefined')
				stritems2 = stritems2.trees;
		</script>
	</c:otherwise>
</c:choose>

<c:if test="${stritems2 != null }">
	<c:set var="showAddFormAtNoStandalone" value="false"></c:set>
	<%-- //for hexagon bookmarks use hex - to be parametriesed --%>
	<c:set var="addBookmarkType" value="web"></c:set>

	<%-- ---------------------------------------------------------------- 
	---	  Script files definition
	---------------------------------------------------------------- --%>
	<script type="text/javascript" src="/js/favorites/yuiTreeExt/yahoo.js"></script>
	<script type="text/javascript" src="/js/favorites/yuiTreeExt/event.js"></script>
	<script type="text/javascript" src="/js/favorites/yuiTreeExt/animation.js"></script>
	<script type="text/javascript" src="/js/favorites/yuiTreeExt/dom.js"></script>
	<script type="text/javascript" src="/js/favorites/yuiTreeExt/treeview.js"></script>

	<script type="text/javascript" src="/js/favorites/yuiTreeExt/PersonNode.js"></script>
	<script type="text/javascript" src="/js/favorites/yuiTreeExt/FriendNode.js"></script>
	<script type="text/javascript" src="/js/favorites/yuiTreeExt/DirectoryNode.js"></script>
	<script type="text/javascript" src="/js/favorites/yuiTreeExt/BookmarkNode.js"></script>
	<script type="text/javascript" src="/js/favorites/yuiTreeExt/DiuDirectoryNode.js"></script>
	<script type="text/javascript" src="/js/favorites/yuiTreeExt/SugDirectoryNode.js"></script>
	<script type="text/javascript" src="/js/favorites/yuiTreeExt/WebBookmarkNode.js"></script>

	<script type="text/javascript" src="/js/favorites/sscf/Globals.js"></script>
	<script type="text/javascript" src="/js/favorites/sscf/UIHelper.js"></script>
	<script type="text/javascript" src="/js/favorites/sscf/BookmarksTree.js"></script>
	<script type="text/javascript" src="/js/favorites/sscf/Clipboard.js"></script>
	<script type="text/javascript" src="/js/favorites/sscf/CreateDir.js"></script>
	<script type="text/javascript" src="/js/favorites/sscf/BookmarkAdd.js"></script>
	<script type="text/javascript" src="/js/favorites/sscf/DescriptionBox.js"></script>
	<script type="text/javascript" src="/js/favorites/sscf/FriendAdd.js"></script>
	<script type="text/javascript" src="/js/favorites/sscf/Feed.js"></script>
	<script type="text/javascript" src="/js/favorites/sscf/main.js"></script>
	<script type="text/javascript" src="/js/favorites/sscf/Tooltip.js"></script>

	<script type="text/javascript" src="/js/favorites/sscf/sidebar/filter.js"></script>
	<script type="text/javascript" src="/js/favorites/sscf/sidebar/recommendations.js"></script>
	<script type="text/javascript" src="/js/favorites/sscf/sidebar/foldingBox.js"></script>

	<script type="text/javascript" src="/js/favorites/sscf/json.js"></script>

	<script type="text/javascript" src="/js/favorites/sscf/Inbox.js"></script>




	<c:if test="${showAddFormAtNoStandalone eq 'true' || (standalone eq 'true' || standalone eq 'on')}">
		<c:if test="${empty addurl}">
			<c:set var="addurl" value="http://"></c:set>
		</c:if>
	
		<%-- Workaround for encoding problem when going through login process --%>
		<c:if test="${!empty addNameSession}">
			<c:set var="addname" value="${addNameSession}" scope="page" />
			<c:remove var="addNameSession" scope="session" />
		</c:if>
	
		<%-- ---------------------------------------------------------------- 
		---	  Go back link
		---------------------------------------------------------------- --%>
		<c:if test="${(addform eq 'true') and (not empty addurl) and (showAddFormAtNoStandalone eq 'true' or (standalone eq 'true' or standalone eq 'on'))}">
			<div id="topMenuCont" class="margin6">
				<div id="topMenuLinkCont">
					<c:if test="${!empty addname}">
						<span class="topMenu" id="addPageGoBack">
							<a class="inBoxABig" href="${addurl}">
								Go back
								<i>${addname}</i>&raquo;
							</a>
						</span>
					</c:if>
					<c:if test="${empty addname}">
						<span class="topMenu" id="addPageGoBack">
							<a class="inBoxABig" href="${addurl}">
								Go back
								<i>${addurl}</i>&raquo;
							</a>
						</span>
					</c:if>
		
					<span class="topMenu">
					</span>
				</div>
			</div>
		</c:if>
	
		
		<%-- ---------------------------------------------------------------- 
		---	  Add/Edit bookmark container 
		---------------------------------------------------------------- --%>
		<div id="bookmarkEditCont" class="shadowyBox" style="display:none">
			<%-- TODO Implement bookmark add/edit window 
			<helpers:bookmarkedit id="bookmarkEditTag" />
			--%>
		</div>
		
	</c:if>
	
	<%-- ---------------------------------------------------------------- 
	---	  Clipboard placeholder 
	---------------------------------------------------------------- --%>
	<c:if test="${true eq standalone||'on' eq standalone}">
		<div id="clipboard" class="outBox margin10">
			Clipboard item
			[<a href="javascript:emptyClipboard()">
				emtpy
			</a>]
			<br>
			<div id="clipboardItem" class="inBox"></div>
		</div>
	</c:if>
	
	<%-- ---------------------------------------------------------------- 
	---	  Trees placeholder
	---------------------------------------------------------------- --%>
	<div id="content">
		<div id="treeContainer">
			<span class="blabel">Bookmarks</span>
			<div id="treeDiv1"></div>
			<div id="addBookmarkDiv">
				<button type="button" onclick="addBookmark()">
					Add to selected directories
				</button>
			</div>
		</div>	
	</div>

	<%-- ---------------------------------------------------------------- 
	---	  Initiation of the script variables
	---------------------------------------------------------------- --%>
	<script type="text/javascript">

		var standalone = false;
		var newBookmarkUri = "${newUri}";
		var dir = "${dir}";
		var viewer = "${uri}";
		var viewerUid = "${uri}".substring("${uri}".length-8);
		var stritems = {};//${stritems};
		var seluri = "${seluri}";
		
		var addBookmarkType = "${addBookmarkType}"; 
		
		var addform = false;
		var showAddFormAtNoStandalone = false;
		
		var usersServlet = l10n.URLUsers;

		registerOnLoad(initTree);
		
	</script>
	<c:if test="${standalone ne null and (standalone eq 'true' or standalone eq 'on')}">
		<script>standalone = true;</script>
	</c:if>
	<c:if test="${addform ne null and addform eq 'true'}">
		<script>
			addform = true;
			addurlTmp = "${addurl}";
			addnameTmp = "${addname}";
		</script>
	</c:if>
	<c:if test="${showAddFormAtNoStandalone eq 'true'}">
		<script>showAddFormAtNoStandalone = true;</script>
	</c:if>
</c:if>