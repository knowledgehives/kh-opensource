
<%@ tag description="Page with absolute divs used by bookmarks" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib prefix="helpers" tagdir="/WEB-INF/tags" %>

	
	<%-- ---------------------------------------------------------------- 
	---	 Directory creation/edit "window"
	---------------------------------------------------------------- --%>

	<div id="dirEditCont">
		<%-- TODO Implement directory add/edit window 
		<helpers:directoryedit id="dirEditTag" />
		--%>
	</div>

	
	<%-- ---------------------------------------------------------------- 
	---	 Import module
	---------------------------------------------------------------- --%>
	<div id="fallBlack"></div>
	<div id="importBookmarksCont">
		<%-- TODO Implement import window 
		<helpers:import/>	
		--%>
	</div>
	
	<%-- ---------------------------------------------------------------- 
	---	 Centered message window(s)
	---------------------------------------------------------------- --%>
	<div class="horizon">
		<div id="illegal" class="contentMes">Illegal operation<br><br>
		[<a href="javascript:showHideMes(10,false)">Close</a>]
		</div>
	</div>
	
	<%-- ---------------------------------------------------------------- 
	---	 Directory/bookmark description "window"
	---------------------------------------------------------------- --%>
	<div id="dirDescIn" class="infoBox">
		Directory description
		<br>
		<div id="dirDescCont" class="inBox">Loading</div>
	</div>
	
	<%-- ---------------------------------------------------------------- 
	---	 AddFriend form and placeholder 
	---------------------------------------------------------------- --%>
	<div id="addFriendCont" >
		Add friend
		<br/>
		<div class="inBox">
			<div id="addFriendForm" style="visibility:hidden; display:none">
			<table  width="300" cellpadding="1" cellspacing="0" border="0">
				<tr class="tableNormOut" >
					<td class="capt">Name </td>   
					<td><span id="addFriendName"></span></td>
					<td></td>
				</tr>
				<tr class="tableNormOut">
					<td class="capt">Level</td>   
					<td><input class="inputNorm" style="width:50px" type="text" id="addFriendLevel"></td>
					<td id="addFriendLevelInfo" class="errorText"></td>
				</tr>
			</table>
			</div>
			<div id="addFriendInfo"  style="visibility:hidden;display:none">
			<table  width="300" cellpadding="1" cellspacing="0" border="0">
				<tr class="tableNormOut" >
					<td><span id="addFriendInfoName"></span> Is your friend<br/>	
						<span id="addFriendInfoText"></span>
					</td>
				</tr>
			</table>
			</div>
		</div>
		<button type="button" id="addFriendSaveBut" onclick="addFriend();">Save</button>
		<button type="button" id="addFriendCloseBut">Close</button>
	</div>