
var feedCreationUri = "/createFeed";

function getFeed(dirid,type,linkDiv)
{
	if(type==1||type=="atom_0.3")
		feedType = "atom_0.3";
	else 
		feedType = "rss_2.0";
		
		
	defNumOfChanges = 10;
	
	//set proper request parameters
	var parameters = "id=" + dirid +
				"&userid=" + viewer +
				"&type=" + feedType + 
				"&numofchanges=" + defNumOfChanges;
			
	var ajax = new Ajax.Request(feedCreationUri, {
	 method : "post",
	 parameters : parameters,
	 onComplete : function(linkDiv,resp,oColl){
	 	setFeedLink(linkDiv,resp,oColl);
	 }.bind(this,linkDiv)
	});	
	
};

function setFeedLink(divId,resp,oColl) {
	if(oColl==null)
		try {
			oColl = eval('('+resp.responseText+')');
		}
		catch(ex) { 
			//console.log(ex); 
		}
	
	if(oColl.feedid) {
		var feedLink = serverAddr + "feed?id=" + oColl.feedid;
		if(divId=="") 
			;//console.log(feedLink)
		else {
			try {
				$(divId).href = feedLink;
			} catch(e) {}
			window.location.href = feedLink;
		}
	}
}