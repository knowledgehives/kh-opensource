
var Inbox = Class.create();

Inbox.EXPANDED = "&laquo;";
Inbox.UNEXPANDED = "&raquo;"

/* How many results should be displayed */
Inbox.RESULTS_DISP = 10;



Inbox.prototype = {
	/* divs holders */
	inboxSwitch:null,
	inboxSwitchLink:null,
	inboxSwitchText:null,
	inboxSwitchTextCount:null,
	inboxSwitchTextInd:null,
	
	inboxDiv:null,
	inboxDivIn:null,
	
	inboxInfo:null,
	currentPage:0,
	
	inboxItems:null,
	
	/*visible/hidden indicator*/
	visible:false,
	
	inboxItems:$A(),
	inboxUriGlob:null,
	
	selectedRes:$A(),
	
	showHide:function(event) {
		if(this.visible)
		{
			this.inboxDiv.style.display = "none";
			this.inboxSwitchTextInd.innerHTML = Inbox.UNEXPANDED;
		}
		else {
			this.inboxDiv.style.display = "block";
			this.inboxSwitchTextInd.innerHTML = Inbox.EXPANDED;
		}
		this.visible = !this.visible;
	},
	
	selectResource:function(id) {
		var inInbox = this.inboxItems.find(function(item){
			return item.uri == id;
		});
		
		if(inInbox)
		{
			//switch page....
			var generated = false;
			var selectedPosition = -1;
			this.inboxItems.each(function(item,index) {
				if(item.uri==id)
				{
					selectedPosition = index;
					return true;
				} 
			});
		
			if(selectedPosition >= Inbox.RESULTS_DISP) 
			{
				whichPage = parseInt(selectedPosition/Filter.RESULTS_DISP);	
				if(whichPage!=this.currentPage)
				{
					this.currentPage = whichPage;
					this.generateInbox();
					generated = true;
				}
			}	

			if(!generated) {
				$$(".inboxSelected").each(function(item){
					Element.removeClassName(item,"inboxSelected");
				});
			}
			
			resource = $(id);
			
			if(resource != null && typeof resource != 'undefined')
			{
				if(!this.visible)
					this.showHide();
				
				Element.addClassName(resource,"inboxSelected");
				resource.scrollTo();
				
				return true;
			}
		}
	},
	
	generateInbox:function() {
		
		if(this.inboxItems && this.inboxItems !=null && this.inboxItems != "" &&
				this.inboxItems.length>0)
		{
			this.showResultsPageNav(this.inboxItems.length);
	
			var start = (this.currentPage*Inbox.RESULTS_DISP);
			var end = (this.inboxItems.length<start+Inbox.RESULTS_DISP)?this.inboxItems.length:start+Inbox.RESULTS_DISP;
			
			
			this.inboxSwitchTextCount.innerHTML = "["+this.inboxItems.length+"]";
			this.inboxDivIn.innerHTML = "";
				
			for(index=start;index<end;index++)
			{
				//inboxObj.result.each(function(item,index) {
				var item = this.inboxItems[index];
				
				var res = document.createElement("div");
				Element.addClassName(res, "inboxResource");
				res.id = item.uri;
				var resNameSpan = document.createElement("span");
				var resName = document.createTextNode(item.label);	
				Element.addClassName(resNameSpan, "inboxResourceLink");
				resNameSpan.appendChild(resName);
				
				resNameSpan.onclick = this.fillEditFormRequest.bind(this,item.uri);
				//TODO: Add IE6 support
				
				var actions = document.createElement('div');
				Element.addClassName(actions,'inboxCommand');
				var commText = document.createTextNode(' ['+JS_MESSAGES["Bookmarks.command_actions"]+']');
				actions.appendChild(commText);
				actions.onclick = showTooltip.bindAsEventListener(this,'inbox'+index+'tooltip');
				
				var tooltip = document.createElement('div');
				Element.addClassName(tooltip,"inboxTooltip");
				tooltip.id = "inbox"+index+"tooltip";
				
				//cut, copy, edit
				var commandCopy = document.createElement("div");
				Element.addClassName(commandCopy,'ttInbox');
				var commText = document.createTextNode(JS_MESSAGES["Bookmarks.command_copy"]);
				commandCopy.appendChild(commText);
				
				commandCopy.onclick = copyNodeAbs.bind(this,item.uri,item.label,null,this.inboxUriGlob,'web',false);
				
				var commandCut = document.createElement("div");
				Element.addClassName(commandCut,'ttInbox');
				var commText = document.createTextNode(JS_MESSAGES["Bookmarks.command_cut"]);
				commandCut.appendChild(commText);
				
				commandCut.onclick = copyNodeAbs.bind(this,item.uri,item.label,null,this.inboxUriGlob,'web',true);
				
				var commandEdit = document.createElement("div");
				Element.addClassName(commandEdit,'ttInbox');
				var commText = document.createTextNode(JS_MESSAGES["Bookmarks.command_edit"]);
				commandEdit.appendChild(commText);
				
				commandEdit.onclick = this.fillEditFormRequest.bind(this,item.uri);
				
				var commandDel = document.createElement("div");
				Element.addClassName(commandDel,'ttInbox');
				var commText = document.createTextNode(JS_MESSAGES["Bookmarks.command_remove"]);
				commandDel.appendChild(commText);
				
				commandDel.onclick = this.removeNodeRequest.bind(this,item.uri,this.inboxUriGlob);
				
				var commandGoDiv = document.createElement('div');
				var commandGo = document.createElement("a");
				Element.addClassName(commandGoDiv,'ttInbox');
				commandGo.innerHTML = JS_MESSAGES["Bookmarks.command_go"]+"&raquo;";
				commandGo.href = item.uri.substring(0,item.uri.lastIndexOf("__"));
				
				commandGoDiv.appendChild(commandGo);				
				
				res.appendChild(resNameSpan);
				res.appendChild(actions);
				
				tooltip.appendChild(commandCopy);
				tooltip.appendChild(commandCut);
				tooltip.appendChild(commandEdit);
				tooltip.appendChild(commandDel);
				tooltip.appendChild(commandGoDiv);
				
				res.appendChild(tooltip);
				
				this.inboxDivIn.appendChild(res);
			}
		}
		else
		{
			this.inboxInfo.innerHTML =  JS_MESSAGES["Bookmarks.inbox_empty"];
		}
	},
	
	contains:function(id) {
		return this.inboxItems.any(function(item) {
			return item.uri==id;
		});
	},
	
	addNode:function(uri,resName) {
		//newInbox = new Object();
		//newInbox.count = this.inboxItems.length + numberToAdd;
		//newInbox.result = $A();
		
		var saving = this.contains(uri);
		
		if(saving && this.inboxItems.length>0)
		{
			this.inboxItems.each(function(item) {
				if(saving&&item.uri==uri)
					item.label = resName;
				/*else newLabel = item.label;
				
				var newObj = new Object();
				newObj.label = newLabel;
				newObj.uri = item.uri;
				this.inboxItems.push(newObj);*/
			});
		}
		if(!saving) {
			var newObj = new Object();
			newObj.label = resName;
			newObj.uri = uri;
			this.inboxItems.push(newObj);
		}
		
		//newInbox.inboxUri = this.inboxUriGlob;;
		
		this.generateInbox();
	},
	
	removeNodeRequest:function(nodeId,parentId) {
		
		var ajax = new Ajax.Request("ajaxResults/nodeExample", {
			 method : "delete",
			 onComplete : function(nodeId,parentId,resp,oColl){
			 	this.removeTheNode(nodeId,parentId,resp,oColl);
			 }.bind(this,nodeId,parentId)
			});	
	},
	
	removeTheNode:function(id,inboxId) {
		//newInbox = new Object();
		//newInbox.count = this.inboxItems.length - 1;
		
		//newInbox.result = $A();
		howManyPagesBefore = parseInt(this.inboxItems.length/Filter.RESULTS_DISP);
		
		if(this.inboxItems.length>0)
		{
			this.inboxItems = this.inboxItems.reject(function(el){
				return el.uri == id;
			});
		}
		
		//newInbox.inboxUri = inboxId;
		howManyPages = parseInt(this.inboxItems.length/Filter.RESULTS_DISP);
		
		if(howManyPagesBefore-howManyPages!=0&&this.currentPage==(howManyPagesBefore-1))
			this.currentPage--;
		
		this.generateInbox();
	},
	
	fillEditFormRequest:function(nodeId) {
		alert("Implement Resource edit window...");
		//addPageShow(true,"EDITINBOX",nodeId);
	},
	
	
	showResultsPageNav: function(count) {
		this.inboxInfo.innerHTML = "";
		
		if(this.currentPage>0) {
			var back = document.createElement('a');
			back.innerHTML="&laquo;&nbsp;&nbsp;";
			back.style.cursor = "pointer";
								
			back.onclick = this.changePage.bind(this,true,count);
			//back.onmouseover = this.onMouseOverTwo.bindAsEventListener(this,back,"link",true);
			//back.onmouseout = this.onMouseOverTwo.bindAsEventListener(this,back,"link",false);
			this.inboxInfo.appendChild(back);
		}

		from = (this.currentPage*Filter.RESULTS_DISP)+1;
		to = (count<from + Filter.RESULTS_DISP)?count:from + Filter.RESULTS_DISP - 1;

		this.inboxInfo.appendChild(document.createTextNode(from+" - "+to+" ["+count+"]"));
		
		howManyPages = parseInt(this.inboxItems.length/Filter.RESULTS_DISP);
		if(this.inboxItems.length%Filter.RESULTS_DISP==0) howManyPages--;
		
		if(this.currentPage<howManyPages)
		{
			var next = document.createElement('a');
			next.innerHTML="&nbsp;&nbsp;&raquo;";
			next.style.cursor = "pointer";
					
			next.onclick = this.changePage.bind(this,false,count);
			//next.onmouseover = this.onMouseOverTwo.bindAsEventListener(this,next,"link",true);
			//next.onmouseout = this.onMouseOverTwo.bindAsEventListener(this,next,"link",false);
			this.inboxInfo.appendChild(next);
		}
	},
	
	changePage: function(prev,count) {
		if(prev)
			this.currentPage--;	
		else this.currentPage++;
		//this.showResultsPageNav(count);
		this.generateInbox();
	},
	
	addToInbox:function() {
		
	},
	
	getInboxUri:function() {
		return this.inboxUriGlob;
	},
	
	initialize : function(_inboxObj) {
		this.inboxSwitch=$('inboxSwitch');
		this.inboxSwitchLink=$('inboxSwitchLink');
		this.inboxSwitchText=$('inboxSwitchText');
		this.inboxSwitchTextCount=$('inboxSwitchTextCount');
		this.inboxSwitchTextInd=$('inboxSwitchTextInd');
	
		this.inboxDiv=$('inboxDiv');
		this.inboxDivIn=$('inboxDivIn');
		
		this.inboxInfo = $('inboxInfo');
		
		//add actions to the show/hide
		Event.observe(this.inboxSwitchLink, 'click', this.showHide.bindAsEventListener(this));

		if(_inboxObj.inboxUri && _inboxObj.inboxUri!=null)
				this.inboxUriGlob = _inboxObj.inboxUri;

		if(_inboxObj.result && _inboxObj.result!=null&&_inboxObj.result.length>0)
		{
			this.inboxItems = _inboxObj.result;
		}
		
		this.generateInbox();

	}	
	
}