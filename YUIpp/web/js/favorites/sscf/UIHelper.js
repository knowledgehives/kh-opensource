/**
 * 
 * =================== User Interface helpers =====================
 * 
 * 
 * Set of the methods and variables that are used in the other 
 * 'classes' to handle UI specific task
 * Task supported:
 * - getting rendered style (does not work/works differently in ie)
 * - getting page/window size
 * - panels handling
 * - showing full/part of the text
 * - Showing messages
 */


/* contains the class of the element which will be changed to inform about the error */
var setErrorClass; 


/**
 * Gets the rendered style
 * In gecko/opera/safari - rendered style
 * In IE - renered style but not in absolute measurements
 * 
 */
function getStyle(oElm, strCssRule){
	var strValue = "";
	if(document.defaultView && document.defaultView.getComputedStyle){
		strValue = document.defaultView.getComputedStyle(oElm, "").getPropertyValue(strCssRule);
	}
	else if(oElm.currentStyle){
		strCssRule = strCssRule.replace(/\-(\w)/g, function (strMatch, p1){
			return p1.toUpperCase();
		});
		strValue = oElm.currentStyle[strCssRule];
	}
	return strValue;
}

/** 
 * getPageSize()
 * Returns array with page width, height and window width, height
 * Core code from - quirksmode.org
 * Edit for Firefox by pHaez
 */
function getPageSize(){
	
	var xScroll, yScroll;
	
	if (window.innerHeight && window.scrollMaxY) {	
		xScroll = document.body.scrollWidth;
		yScroll = window.innerHeight + window.scrollMaxY;
	} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
		xScroll = document.body.scrollWidth;
		yScroll = document.body.scrollHeight;
	} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
		xScroll = document.body.offsetWidth;
		yScroll = document.body.offsetHeight;
	}
	
	var windowWidth, windowHeight;
	if (self.innerHeight) {	// all except Explorer
		windowWidth = self.innerWidth;
		windowHeight = self.innerHeight;
	} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
		windowWidth = document.documentElement.clientWidth;
		windowHeight = document.documentElement.clientHeight;
	} else if (document.body) { // other Explorers
		windowWidth = document.body.clientWidth;
		windowHeight = document.body.clientHeight;
	}	
	
	// for small pages with total height less then height of the viewport
	if(yScroll < windowHeight){
		pageHeight = windowHeight;
	} else { 
		pageHeight = yScroll;
	}

	// for small pages with total width less then width of the viewport
	if(xScroll < windowWidth){	
		pageWidth = windowWidth;
	} else {
		pageWidth = xScroll;
	}

	arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight) 
	return arrayPageSize;
}

/**
 * Shadow creation.
 * 
 * adds several classes around given DOM object. Together with 
 *  CSS styling it creates the shadow effect.
 * 
 * @param id - id of the DIV that should have shadow
 * @param width - optional width of the DIV (if not set to auto)
 * @param inId - not used
 * @param align - not used
 */
function createShadow(id,width,inId,align)
{
	var outDiv = document.getElementById(id);
    
    div = document.createElement('div');
    div.className = "shadow-top";
    
    div1 = document.createElement('div');
    div1.className = "left";
    div2 = document.createElement('div');
    div2.className = "center";
    div3 = document.createElement('div');
    div3.className = "right";
    
    div.appendChild(div1);
    div.appendChild(div2);
    div.appendChild(div3);
    
    outDiv.appendChild(div);
    
    div = document.createElement('div');
    div.className = "shadow-middle";
    
    div1 = document.createElement('div');
    div1.className = "left";
    div3 = document.createElement('div');
    div3.className = "right";
    
    div.appendChild(div1);
    div.appendChild(div3);
    
    outDiv.appendChild(div);
    
    div = document.createElement('div');
    div.className = "shadow-bottom";
    
    div1 = document.createElement('div');
    div1.className = "left";
    div2 = document.createElement('div');
    div2.className = "center";
    div3 = document.createElement('div');
    div3.className = "right";
    
    div.appendChild(div1);
    div.appendChild(div2);
    div.appendChild(div3);
    
    outDiv.appendChild(div);	
}


/**
 * Sets the information about error in directory creation window,
 * it is also used in other forms to inform about failed validation of the value 	
 * (the red border of the input)
 *
 * @param div - for which div should we change the class
 * @param enable - enable or disable information about error
 * @param text - error message
 */
function setError(div,enable,text)
{
	if(enable)
	{
		setErrorClass = div.className;
		div.className = "inputNormError";
		if(text)
		{
			document.getElementById(div.id+"Info").innerHTML=text;
		} 	
	}
	else if(setErrorClass)
	{
		div.className = setErrorClass;
		if(text)
		{
			document.getElementById(div.id+"Info").innerHTML="";	
		}
	}
}


/**
 * Sets the directory description message. Used to communicate errors in directory description DIV.
 * 
 * @param type - type of message
 * @param div - divClass in which message should be inserted.
 */
function setDirectoryDescMes(type,div)
{	
	if(type==WORK)
		div.innerHTML = JS_MESSAGES["Bookmarks.loading"];
	if(type==EMPTY)
		div.innerHTML = JS_MESSAGES["Bookmarks.dir_desc_empty"];
	if(type==TIMEOUT)
		div.innerHTML = "<span style=\"color:red\">"+JS_MESSAGES["Bookmarks.connection_problem"]+"<br>"+JS_MESSAGES["Bookmarks.try_later"]+"</span>";
}

/**
 * shows or hides screen message
 * This method has two styles of invoking 
 * 1. showHideMes(int type,boolean show/hide) - to see the types - go into the method body
 * 2. showHideMes(-1,boolean show/hide,divClass,int height) - more universal, you need to pass
 * 		the height of the div (because hide sets 0px)
 *
 * @param type - type of a message
 * @param show - boolean show=true, hide=false
 * @param div - optional parameter, read only if type==-1, it is class of div (after documnet.getElementById)
 * @param height - optional parameter, read only if type==-1, height of showing block
 */
function showHideMes(type,show,div,height)
{
	//shows/hide given DIV (if type==-1)
	if(type==-1)
	{
		if(show) 
		{
			div.style.visibility = "visible";
			div.style.display = "block";
			if(typeof height != 'undefined')
				div.style.height = height+"px";
		}
		else 
		{
			div.style.visibility = "hidden";
			div.style.display = "none";
			//div.style.height = "0px";
		}
	}
	
	//show hide definite DIVs
	if(show) {stl = "visible";dspl = "block";}
	else {stl = "hidden";dspl = "none";}

	
	//directory description
	if(type==2) {
		dirDesc.style.visibility = stl;
		dirDesc.style.display = dspl;
	}
	//policy editing
	if(type==3) {
		policyBoxDiv.style.visibility = stl;
		policyBoxDiv.style.display = dspl;	
	}
	//directory creation
	if(type==4) {
		dirCreationDiv.style.visibility = stl;
		dirCreationDiv.style.display = dspl;	
		if(!show) {
			//reset directory creation on show
			//hideAllPanels();
			dirCreationDataNameDiv.value="";
			dirCreationDataDescDiv.value="";
			resetOVConcepts();
		}
	}
	//illegal operation
	if(type==ILLEGAL) {
		moveToCenter(illegalDiv);
		illegalDiv.style.visibility = stl;
	}
}


/**
 * Moves the given element to the mouse position
 * 
 * Can be aware of the edge of the page - so will avoid moving
 *  the window outside the page - to do so 3rd and 4th parameter needs
 *  to be set.
 * 
 * @param divName - element to move
 * @param e - event.
 * @param divWith - width of a div that should be placed
 * @param margin - margin between div and a edge of a page/window
 */
function moveToMousePosition(divName,e,divWidth,margin)
{
	var posx = 0;
	var posy = 0;
	
	if (!e) var e = window.event;
	if (e.pageX || e.pageY)
	{
		posx = e.pageX;
		posy = e.pageY;
	}
	else if (e.clientX || e.clientY)
	{
		posx = e.clientX + document.body.scrollLeft;
		posy = e.clientY + document.body.scrollTop;
	}
		
	if(typeof sscf!="undefined" && sscf != null) {
		var sscfPos = sscf.getPosition();
		posx = posx - sscfPos[0];
		posy = posy - sscfPos[1];
	}
	
	if(divWidth!=null&&typeof divWidth!="Undefined")
	{
		//getscreen width
		screenData = getPageSize();
		
		if(posx+divWidth>screenData[2])
			posx = (screenData[2]-divWidth-10);
	}
	if(margin!=null&&typeof margin!="Undefined") 
		posy+=margin;
	
	divName.style.left = posx+"px";
	divName.style.top = posy+"px";
}


/**
 * Moves to center the given divClass
 */
function moveToCenter(divName)
{
	divName.style.top = document.body.scrollTop;
}


//======== not used ==========

//notused

function setWidths()
{
	//width for tree and for filter
	pageSize = getPageSize();
	filterWidth=getInt(getStyle(document.getElementById("filterContainer"),"width"));
	paddingRight=getInt(getStyle(document.getElementById("content").parentNode,"padding-right"));
	
	document.getElementById("treeContainer").style.width = (pageSize[0]-(60+parseInt(paddingLeft)+parseInt(paddingRight)+parseInt(filterWidth)))+"px";
}

function toVertical(el)
{
	text = el.innerHTML;
	newText = "";
	for(i=0;i<text.length-1;i++)
	{
		newText += text[i] +"<br>";
	}
	newText += text[text.length-1];
	
	el.innerHTML = newText;
}


function getInt(value)
{
	var matches = value.match(/([0-9]+).*?/);
	if(matches)
		return matches[1];
	return 0;
}