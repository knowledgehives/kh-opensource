/**
 * 
 * ================= Bookmarks adding handler ======================
 * 
 * This 'class' is responsible for handling the adding new bookmark process.
 * 
 * 
 */


/* Following variables are used to keep references to DOM objects */
var addPageLink;

var bkmrkWinObj;
var selectedUuids;

/*mode of the bookmark add window */
var BOOKMARKADD_ADD = "ADD";
var BOOKMARKADD_EDIT = "EDIT";
var BOOKMARKADD_EDITINBOX = "EDITINBOX";
var bookmarkWinMode = BOOKMARKADD_ADD;

var editTree = null;
var editIndex = null;

/* in case of edit we need to have uri of the resource */
var addPageUri="";

 
/**
 * Initialisation of variables
 * Showing/hiding adding form according to the global 'addform' variable
 * 
 * @param[global] addform - boolean that shows if add form should be visible
 * @param[global] standalone - boolean that shows the mode of the treeview.
 * @param[global] showAddFormAtNoStandalone - boolean that indicates if add form
 * 					should be seen at not standalone mode.
 */
function initBookmarkAdd() {
	selectedUuids = [];

	creatingUri = false;
	
	//this is the global 'addform' variable
	if(addform) addPageShow(true);
}


/**
 * Changes state of checkboxes
 * Searches for all accurences of dir.
 * 
 * @param id - dir uuid
 * @param checked - check or uncheck
 */
function checkboxChange(id,checked)
{
	//console.log(id,checked);
	//0. save selected
	if(checked)
		selectedUuids.push(id);
	else
		selectedUuids = selectedUuids.without(id);
	
	//1. find other nodes with the same id
	nodes = tree.getNodesByProperty("id",id);
	
	//2. build checkbox id		
	//and check others
	for(var i=0;i<nodes.length;i++)
	{			
		checkboxId = nodes[i].tree.id+'||'+nodes[i].index+'||'+id;
		if($(checkboxId))
			$(checkboxId).checked = checked;
	}
}

/**
 * Handles selecting dir checkboxes
 */
function checkboxSelect(id,checked)
{
	//checkbox operators
	checkboxChange(id,checked);
	
	//other operation here....
}


/**
 * Shows/hides the 'add bookmark' form
 * 
 * @param show - true=show, false=hide 
 */
function addPageShow(show,mode,nodeId)
{
	var editing = false;
	var editingInbox = false;
	creatingUri = true;
	if(typeof mode != "undefined"&&mode!=null&&mode!=""&&mode==BOOKMARKADD_EDIT )
	{
		editing = true;
		creatingUri = false;
	}
	if(typeof mode != "undefined"&&mode!=null&&mode!=""&&mode==BOOKMARKADD_EDITINBOX )
	{
		editingInbox = true;
	}
	
	if(editing || editingInbox) {
		//TODO IMPLEMENT bookamrk editing window
		//bkmrkWinObj.show();
		
		//bkmrkWinObj.onSave = handleBkmrkEdit;
	}
	else {
		//TODO IMPLEMENT bookamrk add window
		//bkmrkWinObj.show();
		
		//bkmrkWinObj.onSave = handleBkmrkCreation;
	}
	
	
	if(show==true)
	{
		bkmrkWinObj.show();	
		Event.observe($("bookmarkEditTag_closebut"), "mouseup", window.addPageShow.bindAsEventListener(window,false));
		
		if(!editingInbox)
			switchCheckboxes(creatingUri);

		if(!editing&&!editingInbox)
		{
			//close/show page add link
			
			bookmarkWinMode = BOOKMARKADD_ADD;
			
		}
		if(editingInbox)
		{ 
			
			bookmarkWinMode = BOOKMARKADD_EDITINBOX;
		}
		if(editing) {
			
			bookmarkWinMode = BOOKMARKADD_EDIT;
		}
		
	}
	else
	{
		creatingUri = false;
		switchCheckboxes(creatingUri);
		//bkmrkWinObj.hide();
		
	}
}


function handleBkmrkCreation(Bookmark,success) {
	if(success) {
		
		//TODO Implement
			
		rebuildTree();
		
		//bkmrkWinObj.hide();
	}
}

function handleBkmrkEdit(Bookmark,success) {
	if(success) {
	
		//TODO Implement
	
		rebuildTree();
	}
	//bkmrkWinObj.hide();
}


/**
 * Checks if the given url is correct
 * 
 * @param url - url to check
 */
function isValidURL(url){ 
    //var RegExp=/^(([\w]+:)\/\/)(([\d\w]|%[a-fA-F\d]{2,2})+(:([\d\w]|%[a-fA-F\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.?)+[\w]{2,4}(:[\d]+)?(\/([-+_~,@.\d\w]|%[a-fA-F\d]{2,2})*)*(\?(&?([-+_~,@.\d\w]|%[a-fA-F\d]{2,2})=?)*)?(#([-+_~,@.\d\w]|%[a-fA-F\d]{2,2})*)?$/;
    var RegExp = /^(([\w]+:)\/\/)(([\d\w]|%[a-fA-F\d]{2,2})+(:([\d\w]|%[a-fA-F\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.?)+[\w]{2,4}(:[\d]+)?((\/)|(\/.*))?$/;
    if(RegExp.test(url)){ 
        return true; 
    }else{ 
        return false; 
    } 
}

/*========= bookmark edit ============*/
function fillBkmrkEditFormRequest(nodeId,tree,index) {
	
	alert("Implement add/edit bookmark window");
	//addPageShow(true,"EDIT",nodeId);
	
	
}
