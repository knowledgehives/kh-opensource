/**
 * 
 * ================= Recommendations sidebar handler =====================
 * 
 * Class contains methods for handling the recommendations sidebar.
 * 
 */
var Recommendations = Class.create();

Recommendations.prototype = {
	
	//uri of the servlet
	servletUri:'/SscfRecommendations',
	//DOM object for displaying info
	infoDiv:null,
	//super class
	filterObj:null,
	callerObj:null,
	//person uri
	person:null,
	
	recomFB:null,
	
	/**
	 * Gets recommendations already generated for user.
	 * Calls a super class to display results
	 */
	fetchRecommendations : function()
	{
		try {
		this.showNewRecInfo(false);
		
		//REST API call: var ajax = new Ajax.Request("/favorites/recom/", {
		var ajax = new Ajax.Request("ajaxResults/recomItems", {
		 method : "get",
		 onSuccess : function(resp, oColl){
		 	recomFoldingBox = new FoldingBox('recom','recomResultsGroup');
			this.recomFB = recomFoldingBox;	 	
		 	this.callerObj.presentResults(resp, oColl,recomFoldingBox.setCount.bind(recomFoldingBox)); 	
		 	
		 }.bind(this),
		 onFailure : function(resp) {
		 	this.callerObj.showError();
		 }.bind(this)
		});
		
		} catch(e) {console.log(e);}
		
	},
	
	/**
	 * Gets new recommendations - in other words
	 * 	it starts the recommendation engine on a server side
	 * 
	 * Displays results using super class methods
	 */
	getNewRecommendations : function()
	{
		var params = new Object();
		this.showNewRecInfo(false);
		this.infoDiv.style.display = "block";
		this.infoDiv.innerHTML = progresImgSmallRecommend;
		
		var ajax = new Ajax.Request("/favorites/recom/new", {
		 method : "get",
		 onComplete : function(resp, oColl){	
		 	//check if there is some information delivered
		 	if(oColl==null)
				try {
					response = eval('('+ resp.responseText +')');
				}
				catch(ex) {  }
			
			//switch off the spinner
			this.infoDiv.innerHTML = "";
			this.infoDiv.style.display = "none";
			
			this.recomFB.setCount(response.count);
			
			//if there is no info
			if(response.count==0)
			{
				this.showNewRecInfo(true);
			}
			else  
			{	
		 		this.callerObj.presentResults(resp, oColl,true);
		 		loadDataForNode(mainNode,function(){},"insert");
		 		rebuildTree();
			} 	
		 }.bind(this),
		 onFailure : function(resp) {
		 	this.infoDiv.innerHTML = "";
			this.infoDiv.style.display = "none";
		 	this.callerObj.showError();
		 	this.recomFB.setCount(0);
		 }.bind(this)
		});
	},
	
	
	/**
	 * Displays information
	 */
	showNewRecInfo : function(show) {
		if(show){
			this.infoDiv.innerHTML = JS_MESSAGES["Recommendations.notfound"];
			this.infoDiv.style.display = "block";
		}else {
			this.infoDiv.innerHTML = "";
			this.infoDiv.style.display = "none";
		}
	},
	
	/**
	 * Object initialisation
	 * 
	 * @param _caller - object that is calling this one and has a methods
	 * 			that displays the resutls
	 * @param _person - parson that views the tree
	 */
	initialize : function(_caller,_person){
		this.infoDiv = $('recomContentInfo');
		this.invoker = $('recomContentInvoker');
		
		this.infoDiv.style.display = "none";
		
		this.callerObj = _caller;
		this.person = _person;
		
		//Event.observe(this.invoker, 'click', this.getNewRecommendations.bindAsEventListener(this));
	}	
}