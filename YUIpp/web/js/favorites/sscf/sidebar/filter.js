/**
 * 
 * =============== Class that handles sidebar filtering =================
 * 
 * 
 * The following class handles filtering feature in SSCF UI interface. 
 * It handles:
 * - showing/hideing sidebar
 * - switching between different modes of filtering
 * - switching between different types of sidebar (filter/recommendations)
 * - sending filter request
 * - receiving and presenting results.
 */
 
//due to some races it needs to be global
var selectedNodes = $A();

var Filter = Class.create();

/* Text displayed in tag which shows/hides sidebar */
Filter.EXPANDED = "&laquo;";
Filter.UNEXPANDED = "&raquo;"

/* resources types */
Filter.TYPE_DIRECTORY = "http://s3b.corrib.org/sscf/0.1/Directory";
Filter.TYPE_WEBRES = "http://s3b.corrib.org/sscf/0.1/WebResource";
Filter.TYPE_RESOURCE = "http://s3b.corrib.org/sscf/0.1/Resource";
Filter.TYPE_HEXRES = "http://s3b.corrib.org/sscf/0.1/HexResource";

/* Filtering types */
Filter.TYPECLASS = "filterMode";
Filter.TYPECLASSSELECTED = "filterModeSelected";
Filter.SBTYPECLASSSELECTED = "sidebarModeTagSelected";

/* How many results should be displayed */
Filter.RESULTS_DISP = 10;

Filter.prototype = {
	id : null,
	//Sidebar DOM object
	divContainer : null,
	
	idPrefix: null,
	
	form : null,
	//expanded indicator
	expanded : false,
	//ajax results holder
	results : null,
	//input DOM object
	filterInput: null,
	filterContent:null,
	filterInfo:null,
	//table with results
	resTab:$A(),
	//keeps always all results received from the server. resTab is used also for displaying
	// only some part of the results (when group is selected) - in such case only some part
	// of the result is copied from resTabCont into resTab
	resTabCont:$A(),
	//viewer uri
	person:null,
	//holds uris of selected nodes in the tree
	//selectedNodes:$A(),
	//results paging support
	currentPage:0,
	//servlet uri
	uri:null,
	//table with filtering modes
	modes:$A(),
	//hidden inputs that convoys the filter,resource mode to servlet
	filterModeInput:null,
	resourceTypeInput:null,
	//DOM placeholder for filter groups
	filterGroups:null,
	//current filtering mode
	currentMode: 0,

	//selected group id
	currentGroupId:null,
	//temp value of current group
	currentGroupIdTmp:null,
	//not used?
	exactValue:null,
	
	//sidebar viewer
	sidebarPerson:null,
	//groups of filtered results - check filterGroups
	filterResultsGroup:null,
	
	//indicates if page was scrolled to the result
	scrolled:false,
	
	//recommendations - mode (get/new)
	recomMode:false,
	//instance of recommendations class
	recomObj:null,
	
	//synchronizations - mode
	syncMode:false,
	//instance of synchronizations class
	syncObj:null,
	syncContentInfo:null,
	
	//tip classes - display when cursor is over some text
	tipOjb:null,
	tipTitle:null,
	tipContent:null,
	
	formOptions:null,
	showAdvanced:null,
	showAdv:false,
	
	hideLink:null,
	showHideLink:true,
	hideLinkNifted:false,
	
	/**
	 */
	start : function(){

	},
	
	/**
	 * Shows/hides the whole sidebar.
	 * State of sidebar is kept in expanded variable
	 */
	onExpand : function() {
		if(this.expanded) {
			this.divContainer.style.width = "0px";
			this.filterResultsGroup.style.display = "none";
			
			this.expanded = false;
			this.form.style.visibility = "hidden";
			this.spanF.innerHTML = Filter.EXPANDED;
		}
		else {

			this.showMode(null,this.idPrefix);
			
			//this.filterResultsGroup.style.display = "block";
			//this.divContainer.style.width = "200px"; //to be parametriesed
			this.expanded = true;
			if(this.form)
				this.form.style.visibility = "visible";
		}
	},
	
	/**
	 * This method can be used to invoke filtering from js code
	 *  (instead of submitting the form). Is used to filter by 
	 *  concepts in directories' description.
	 */
	filterByValue : function(type,valueUri,value)
	{
		//this.exactValue.value = value;
		if(!this.expanded) this.onExpand();
		var e=null;
		//switch to proper mode in filtering
		//if(type.toLowerCase()=="wordnet")
		//	this.switchModes(e,1);
		//else
		//	this.switchModes(e,2);
		//this.currentGroupIdTmp = valueUri;
		//console.log(value);
		//console.log(value.indexOf(' '));
		if(value.indexOf(' ')>-1) value = "\""+value+"\"";
		this.onSubmit(e,value);
	},
	
	/**
	 * Method that is invoke after submitting filter form.
	 * Sends Ajax request to the server.
	 * Uses form.request method from prototype 1.5.1+
	 */
	onSubmit : function(event,value) {
		this.filterResultsGroup.style.display = "block";
		
		if((this.filterInput.value!=null&&this.filterInput.value!="")
			||
			(typeof value!="unspecified"&&value!=null))
		{
			if(typeof value!="unspecified"&&value!=null)
				this.filterInput.value = value;
	
			this.filterInfo.innerHTML = progresImgSmallFilter;
			this.currentPage = 0;
			this.form.request({
			  onComplete : function(resp, oColl){	
			 	this.presentResults(resp,oColl);
			 }.bind(this),
			 onFailure: function(){
			 	this.filterInfo.innerHTML = JS_MESSAGES['Bookmarks.error_try_again']; 
			 	this.setTreeContainerHeight();
			  }.bind(this)
			});
		}
		return false;
	},
	
	/**
	 * Shows error
	 */
	showError: function() {
		this.filterInfo.innerHTML = JS_MESSAGES['Bookmarks.error_try_again']; 
	},
	
	/**
	 * According to the type returns suitable style
	 * 
	 * @param type - uri of the type of the resource
	 */
	selectStyle : function(type) {
		
		//in case of recomendations
		if(this.recomMode&&
				(type==Filter.TYPE_DIRECTORY||type=="DIRECTORY"))
			return "rec";
		
		if(type==Filter.TYPE_DIRECTORY||
			type=="DIRECTORY")
			return "dirf";
		else if(type==Filter.TYPE_WEBRES||
			type=="BOOKMARK")
			return "web";
		
		else return "web";
	},
	
	/**
	 * Generates results navigation page
	 * 
	 * @param count - number of the resutls
	 */
	showResultsPageNav: function(count) {
		this.filterInfo.innerHTML = "";
		
		if(this.currentPage>0) {
			var back = document.createElement('a');
			back.innerHTML="&laquo;&nbsp;&nbsp;";
			back.style.cursor = "pointer";
								
			back.onclick = this.changePage.bind(this,true,count);
			//back.onmouseover = this.onMouseOverTwo.bindAsEventListener(this,back,"link",true);
			//back.onmouseout = this.onMouseOverTwo.bindAsEventListener(this,back,"link",false);
			this.filterInfo.appendChild(back);
		}

		from = (this.currentPage*Filter.RESULTS_DISP)+1;
		to = (count<from + Filter.RESULTS_DISP)?count:from + Filter.RESULTS_DISP - 1;

		this.filterInfo.appendChild(document.createTextNode(from+" - "+to+" ["+count+"]"));
		
		howManyPages = parseInt(this.resTab.length/Filter.RESULTS_DISP);
		if(this.resTab.length%Filter.RESULTS_DISP==0) howManyPages--;
		
		if(this.currentPage<howManyPages)
		{
			var next = document.createElement('a');
			next.innerHTML="&nbsp;&nbsp;&raquo;";
			next.style.cursor = "pointer";
					
			next.onclick = this.changePage.bind(this,false,count);
			//next.onmouseover = this.onMouseOverTwo.bindAsEventListener(this,next,"link",true);
			//next.onmouseout = this.onMouseOverTwo.bindAsEventListener(this,next,"link",false);
			this.filterInfo.appendChild(next);
		}
	},
	
	/**
	 * Switches the page with results
	 * 
	 * @param prev - if true - previous page is displayed, otherwise next one.
	 */
	changePage: function(prev,count) {
		if(prev)
		{
			this.currentPage--;
			
		}
		else this.currentPage++;
		this.showResultsPageNav(count);
		this.generateResults();
	},
	
	hideResults:function() {
		this.filterResultsGroup.style.display = "none";
		//doesn't want to work in ie...
		//$(this.filterResultsGroup).fade();
		//Effect.Fade(this.filterResultsGroup.id);
	},
	
	createHideLink : function(prefix) {
		var tmp = document.createElement('div');
		this.hideLink = document.createElement('div');
		Element.addClassName(tmp,"hideLink");
		Element.addClassName(this.hideLink,"hideLinkBkg");
		
		tmp.id = prefix + 'hideLink';
		
		var text = document.createElement('a');
		text.appendChild(document.createTextNode(JS_MESSAGES["Bookmarks.close"]));
			
		text.onclick = this.hideResults.bind(this);
		text.href = "javascript:void(0)";
	
		tmp.appendChild(text);
		this.hideLink.appendChild(tmp);
	},
	
	/**
	 * Generation of the filtering results
	 * It uses global resTab table with results obtained from server.
	 * 
	 */
	generateResults:function() {
		//try{
		this.results.style.display="block";
		this.results.innerHTML = "";
		var start = (this.currentPage*Filter.RESULTS_DISP);
		var end = (this.resTab.length<start+Filter.RESULTS_DISP)?this.resTab.length:start+Filter.RESULTS_DISP;
		for(i=start;i<end;i++)
		{
			var res = document.createElement("div");
			Element.addClassName(res, "filterResult");
			var span = document.createElement("div");
			Element.addClassName(span, "filterResultText");
			Element.addClassName(span, this.selectStyle(this.resTab[i].type));
			var progress = document.createElement("span");
			Element.addClassName(progress, "filterShowProgress");
			
			var text = document.createElement('a');
			text.appendChild(document.createTextNode(this.resTab[i].label));
			try {
				if(this.resTab[i].content&&this.resTab[i].content!="Unspecified")
					text.title = JS_MESSAGES['Bookmarks.dir_desc']+": "+this.resTab[i].content;
			} catch(e) {}
			
			text.onclick = this.showDir.bind(this,this.resTab[i].uri,tree.getRoot(),this.person,progress,this.resTab[i].creatorId);
			text.onmouseover = this.onMouseOver.bindAsEventListener(this,text,true);
			text.onmouseout = this.onMouseOver.bindAsEventListener(this,text,false);
	
			span.appendChild(text);
			
			try {
			if(this.resTab[i].creator&&this.resTab[i].creator!=null&&this.resTab[i].creator!=""
					&&this.resTab[i].creator!=this.person)
			{
				var creator = document.createElement("span");
				Element.addClassName(creator, "filterShowProgress");
				creator.appendChild(document.createTextNode(" ["+this.resTab[i].creator+"]"));
				span.appendChild(creator);
			}
			}
			catch(e) {}
			
			span.appendChild(progress);
			res.appendChild(span);
				
			this.results.appendChild(res);
			
			this.setTreeContainerHeight();
			
		}
		
		// Hide button
		if(this.showHideLink) {
			this.results.appendChild(this.hideLink);
			if(!this.hideLinkNifted) {
				//add corners to: ('div#'+this.idPrefix+'hideLink','bottom');
				this.hideLinkNifted = true;
			}
		}
		//}catch(e) {console.log(e);}
		
	},
	
	/**
	 * Method is able to set the external tree container (in which the sidebar is placed)
	 *  height.
	 */
	setTreeContainerHeight : function() {
		/* adjusting the height of treeContainer */
		var height = parseInt(getStyle($(this.idPrefix+'ResultsGroup'),'height'));
		
		var constHeight = 160;
		
		if(height+constHeight>200)
			$('treeContainer').style.minHeight = height + constHeight + "px";
		else $('treeContainer').style.minHeight = 200 +"px";
	},

	


	/**
	 * Responsible for presenting the results obtained from server.
	 * This method:
	 * - creates groups (if available) with 
	 * 		- generateGroups
	 * 		- and showGroup
	 * - generates results and shows page nav with
	 * 		- showResultsPageNav
	 * 		- generateResults
	 * 
	 */
	presentResults : function(xml,resp,callback) {
		if(resp==null)
			try {
				resp = eval('('+ xml.responseText +')');
			}
			catch(ex) {  
				console.log(ex);
			}
		//debugger;
		this.person = resp.person;
		if(resp.count > 0)
		{
			this.filterGroups.innerHTML="";this.filterGroups.style.height = "0px";
			this.results.innerHTML = "";
			//check groups
			if(resp.groups!=null&&resp.groups!="")
			{
				//generate groups
				this.generateGroups(resp.groups);
				//save all results in container
				this.resTabCont = resp.results;
				//display group
				this.showGroup();
			}
			//simple without groups
			else
			{
				this.resTab = resp.results;
				
				try {	
					this.showResultsPageNav(resp.count);
					if(typeof callback == 'function')
						callback(resp.count);
				}
				catch(ex) {
					//console.log(ex);
				}
				
				this.generateResults();
			}
		}
		else 
		{
			this.filterInfo.innerHTML = JS_MESSAGES['Bookmarks.no_results'];
			this.results.innerHTML = "";
			this.results.style.display="none";
			this.filterGroups.innerHTML="";
			this.filterGroups.style.height = "0px"; //ie bug
			this.setTreeContainerHeight();
		}
	},

	/**
	 * Generates groups. 
	 * After clicking a group only results which belongs to such group should be
	 * presented.
	 */
	generateGroups:function(groups)
	{
		this.filterGroups.innerHTML="";this.filterGroups.style.height = "0px"; //ie bug
		try {
			var i=0;
			this.filterGroups.style.height = "auto"; //ie bug
			groups.each(function(item) {
				//initialize current group id
				if(i==0)//&&this.currentGroupId==null)
					this.currentGroupId = item.uri;
				if(this.currentGroupIdTmp!=null) 
				{
					this.currentGroupId = this.currentGroupIdTmp;
					this.currentGroupIdTmp = null;
				}
				
				var grName = item.name;
				if(item.name && item.name.search("key:")==0)
					grName = JS_MESSAGES[item.name.substring(13)];
					
				var grMean = item.mean;
				if(item.mean && item.mean.search("key:")==0)
					grMean = JS_MESSAGES[item.mean.substring(13)];
				
				var indicator = document.createElement('span');
				indicator.className = "groupIndicator";
				indicator.id = item.uri;
				
				var res = document.createElement("div");
				Element.addClassName(res, this.idPrefix+"Group");
				var span = document.createElement("span");
				Element.addClassName(span, this.idPrefix+"GroupText");
				
				var text = document.createElement('a');
				text.appendChild(document.createTextNode(grName));
				//text.title = "Meaning: "+item.mean;
				
				var meaning = document.createElement('a');
				meaning.className = "filterWNMean";
				meaning.innerHTML = " <img src=\"/images/info_sm.png\">";
				
				if(item.type)
					var desc = document.createTextNode(" ("+item.type+")["+item.count+"]");
				else var desc = document.createTextNode(" ["+item.count+"]");
				
				text.onclick = this.showGroup.bind(this,item.uri);
				text.onmouseover = this.changeIndicator.bindAsEventListener(this,item.uri,false,true);
				text.onmouseout = this.changeIndicator.bindAsEventListener(this,item.uri,false,false);
		
				var typeDesc = "";
				if(item.type && typeof item.type != 'undefined' && item.type !=null)
					typeDesc += ' ['+item.type+']';
				
				meaning.onmouseover = showTip.bindAsEventListener(this,true,grName+typeDesc,grMean);
				meaning.onmouseout = showTip.bindAsEventListener(this,false);	
	
				span.appendChild(text);
				span.appendChild(meaning);
				span.appendChild(desc);
				
				res.appendChild(indicator);
				res.appendChild(span);
				this.filterGroups.appendChild(res);
							
				i++;
			}.bind(this));
			
			this.changeIndicator(null,this.currentGroupId,true);	
		}
		catch(e) {
			//console.log(e);
		}		
	},
	
	/**
	 * Presents results only from one, selected group.
	 * It copies the group results from resTabCont to resTab and calles 
	 * standard generateResults (which operates on resTab variable)
	 * 
	 * It also updates page navigations links.
	 * 
	 * @param groupId - which groupd should be showed
	 */
	showGroup:function(groupId)
	{
		if(groupId!=null&&typeof groupId!="unspecified")
		{
			this.currentGroupId = groupId;
			this.changeIndicator(null,groupId,true);
		}
		else groupId = this.currentGroupId;
		
		this.resTab = $A();
		this.resTabCont.each(function(item) {
			
			if(typeof item.group!="unspecified"&&item.group!=null
				&&item.group==groupId)
			{
				this.resTab.push(item);
			}
		}.bind(this));		
				
		try {	
			this.showResultsPageNav(this.resTab.length);
		}
		catch(ex) {
			//console.log(ex);
		}
		
		this.generateResults();
	},
	
	/**
	 * Shows tooltip - floating window.
	 * 
	 * @param e - event - to get mouse position
	 * @param show - show/hide
	 * @param title - title of the floating window
	 * @param content - content of the floating window
	 * 
	 */	
	showTip : function(e,show,title,content) {
		//call external function to move the div
		moveToMousePosition(tipObj,e,150,20);
		
		if(show)
		{
			tipTitle.innerHTML = title;
			tipContent.innerHTML = content;
			tipObj.style.display = 'block';
		}
		else
		{
			tipTitle.innerHTML = "";
			tipContent.innerHTML = "";
			tipObj.style.display = 'none';
		}
		
	},
	
	/**
	 * Indicator that shows which group is selected
	 * It also displays indicator when mouse is over other group
	 * 
	 * @param e - event
	 * @param id - id of the group
	 * @param permanent - should be showing of indicator permanent or
	 * 			is only for mouseover
	 * @param isOver - indicates if there is mouseover event
	 */
	changeIndicator:function(e,id,permanent,isOver)
	{
		on = "<b>&nbsp;&raquo;&nbsp;</b>";
		off = "&nbsp;&nbsp;&nbsp;&nbsp;";
		
		if(!permanent)
		{
			if(isOver)
				$(id).innerHTML=on;
			else
			{
				if(id!=this.currentGroupId)
					$(id).innerHTML=off;
			}
		}
		else 
		{
			var indicators = $$('span.groupIndicator');
			indicators.each(function(item){
				item.innerHTML = off;
			});
			
			$(id).innerHTML = on;
		}
	},

	/**
	 * Mouseover action for standard filter results
	 * 
	 * @param e - event
	 * @param element - what element is mouse over
	 * @param isOver - is over or out
	 */
	onMouseOver:function(e,element,isOver) {
		if(isOver)
		{
			Element.removeClassName(element,"filterResultText");
			Element.addClassName(element,"filterResultTextHover");
		}
		else {
			Element.removeClassName(element,"filterResultTextHover");
			Element.addClassName(element,"filterResultText");
		
		}
	},
	
	/**
	 * More universal version of mouseover handler
	 *
	 * @param e - event
	 * @param element - what element is mouse over
	 * @param className - adds/removes given className from Element
	 * @param isOver - is over or out
	 */
	onMouseOverTwo:function(e,element,className,isOver) {
		if(isOver)
		{
			Element.addClassName(element,className);
		}
		else {
			Element.removeClassName(element,className);
		}
	},

	/**
	 * Unselects all nodes in the tree
	 */
	unselectAll:function() {
		selectedNodes.each(function(item) {
			item.selected = false;
		});
		//selectedNodes.clear();
		selectedNodes = $A();
		tree.collapseAll();
		mainNode.expanded = true;
		rebuildTree(true);
	},
	
	showAdvancedOptions:function(event) {
		if(this.showAdv) {
			this.formOptions.style.display = "none";
			this.showAdv = false;
		}
		else { 
			this.formOptions.style.display = "block";
			this.showAdv = true;
		}
	},
	
	/**
	 * Displays selected nodes fetched from the server.
	 * If some selected node is not available locally this method
	 * gets from the server complete path to this node and than 
	 * generate it with standard (BookmarksTree::generateNodes) method.
	 * 
	 * @param displayed - int value, number of already displayed node
	 * @param uri - URI of the selected resource
	 * @param progressDiv - DOM object keeper of place where the progress
	 * 			of showing all directories is presented
	 * @param resp - response from ajax request
	 * @param oColl - optinal - if request is automatically parsed it contains 
	 * 			the result.
	 */
	fetchFromServer:function(displayed,uri,progressDiv,resp,oColl)
	{	
		if(oColl==null)
			try {
				oColl = eval('('+resp.responseText+')');
			}
			catch(ex) { 
				//console.log(ex); 
			}

		if(oColl)
		{
			//it means that we havn't done the collapseALL
			if(displayed==0) tree.collapseAll();
			//process the paths
			generateNodes2(oColl.trees,null,true);
			rebuildTree();
			
			var selNodesMain = null;
				
			selNodesMain = tree.getNodesByProperty("id",uri);
			if(selNodesMain) {
				selNodesMain.each(function(node) {
					tree.expandToRoot(node);
				});
			}
				
			if(selNodesMain!=null) { 
				
				if(!this.scrolled)			
					$(selNodesMain[0].getToggleElId()).scrollTo();
				//prepare for next scrolling
				this.scrolled = false;
				
				progressDiv.innerHTML = "["+(selNodesMain.length)+"]";
			}
			else progressDiv.innerHTML = "["+displayed+"]";
			
		}
		else
		{
			progressDiv.innerHTML = "["+displayed+"]";
		}
	},

	/**
	 * Method shows selected filter result resource in the tree.
	 * 
	 * Starting point for recursive _showDir method.
	 * 
	 * If the number of nodes available locally is less than the 
	 * amount that should be displayed - then the rest of nodes 
	 * (with paths) are fetched from the server and presented 
	 * with fetchFromServer method. 
	 * 
	 * @param uri - URI of the selected resource
	 * @param node - tree Node object (it is started from root)
	 * @param person - uri of the tree viewer
	 * @param progressDiv - DOM object keeper of place where the progress
	 * 			of showing all directories is presented
	 */
	showDir:function(uri,node,person,progressDiv,creatorId) {
		this.scrolled = false;

		seluri = uri;

		progressDiv.innerHTML = progresImgSmallFilterFind;
		
		if(selectedNodes && selectedNodes.length>0) this.unselectAll();
		
		//show all existing
		this._showDir(uri,node,null);
	
		if(selectedNodes.length>0)
			rebuildTree();
			
		//select how many paths was found
		if(selectedNodes.length>=0)
		{
			//check if we see all
			var resultsCount = selectedNodes.length;
			
			//try to show in inbox
			if(inbox.selectResource(uri)) resultsCount++;
			
				//REST API Call: var ajax = new Ajax.Request('/services/paths/'+uri, {
			//TODO Example:
			var ajax = new Ajax.Request('ajaxResults/treeFilteringEx', {
			 method : "get",
			 onComplete : function(resultsCount,uri,progressDiv,resp,oColl){
			 	this.fetchFromServer(resultsCount,uri,progressDiv,resp,oColl);
			 }.bind(this,resultsCount,uri,progressDiv)
			});	
		}
		
		//this.scrolled = false;
	},

	/**
	 * Method that checks if the node is a representation of a selected resoruce (uri).
	 * If the node has children - they are checked recursively  
	 * 
	 * @param uri - URI of the selected resource
	 * @param node - tree node object
	 */
	_showDir:function(uri,node,_tree) {
		treeIn = tree;
		if(_tree!=null) treeIn = _tree; 
		//using global tree variable
		if(node.data) 
			if(uri==node.data.id &&
				(!node.isSuggested()||
				(node.isSuggested()&&this.recomMode))
				)
			{
				node.selected = true;
				treeIn.expandToRoot(node);
				selectedNodes.push(node);
				if(!this.scrolled)
				{
					$(node.getToggleElId()).scrollTo();
					this.scrolled = true;
				}
			}
		
		if(node.children.length>0)
			for (var i=0;i<node.children.length;i++) {	
				this._showDir(uri,node.children[i],_tree);	
			}
	},
	
	/**
	 * Switches the filtering mode.
	 * 
	 * @param event
	 * @param selected - which mode has been selected (int value from 0) 
	 */
	/*switchModes : function(event,selected) {
		var i = 0;
		try {
			
			this.modes.each(function(div){
				if(selected==i) {
		    		Element.addClassName(div,Filter.TYPECLASSSELECTED);
		    		this.currentMode = i;
		    		this.filterModeInput.value = this.modes[i].id;
		    		//enable/disable resource type select
		    		if(i!=0)
		    			this.resourceTypeInput.disabled = true;
		    		else this.resourceTypeInput.disabled = false;
				}
		    	else Element.removeClassName(div,Filter.TYPECLASSSELECTED);
		    	i++;
		    }.bind(this));
		} catch(e) {
			//console.log(e);
		}
	},*/
	
	/**
	 * Switches sidebar modes.
	 * Currently between filtering and recommendations
	 * 
	 * @param event 
	 * @param selected - which mode has been selected (int from 0)
	 * 
	 */
	showMode : function(event,type) {
		try {
			$(this.idPrefix+"Content").style.display = "block";
		    		
    		if(type=="recom") 
    		{
    			this.recomMode = true;
    			this.syncMode = false;
    			this.showHideLink = false;
    			if(this.recomObj==null)
    			{
    				this.recomObj = new Recommendations(this,this.sidebarPerson.value);
    			}	
    			//fetch current recommendations
    			this.filterInfo.innerHTML = progresImgSmallFilter;
				this.currentPage = 0;
    			this.recomObj.fetchRecommendations();
    			//this.filterResultsGroup.style.display = "block";
    		}else if (type =="sync"){
				this.syncMode = true;
				this.recomMode = false;
				if(this.syncObj==null){
					this.syncObj = new Synchronization(this,this.sidebarPerson.value);
					this.syncContentInfo = $('syncContentInfo');
				}
				this.filterInfo.innerHTML = progresImgSmallFilter;
				this.syncObj.fetchSynchronization();
			}
    		else {
    			this.recomMode = false;
    			this.syncMode = false;
    			this.results.innerHTML = "";
    			this.filterInfo.innerHTML = "&nbsp;";
    			this.results.style.display="none";
    		}
		} catch(e) {
			console.log(e);
		}
	},

	/**
	 * Initialization of variables
	 * 
	 * @param _id
	 * @param _tag - what name should be displayed in a tag that is
	 * 				used to show/hide the sidebar.
	 */
	initialize : function(_id, _idPrefix){
		this.id = _id;
		this.divContainer = $(this.id);	
		this.idPrefix = _idPrefix;
		if($(this.idPrefix+'FormInputForm'))
			this.form =$(this.idPrefix+'FormInputForm'); 
		this.results = $(this.idPrefix+'Results');
		this.results.style.display = "none";
		this.filterInput = $(this.idPrefix+'FormInputValue');
		this.filterContent = $(this.idPrefix+'Content');
		this.filterInfo = $(this.idPrefix+'Info');
		this.formOptions = $(this.idPrefix+"FormOptions");
		this.showAdvanced = $(this.idPrefix+'ShowAdvanced');
		this.uri = "/UIFilter"
		if($(this.idPrefix+"Mode"))
			this.filterModeInput = $(this.idPrefix+"Mode");
		if($("resourceType"))
			this.resourceTypeInput = $("resourceType"); 
		this.filterGroups = $(this.idPrefix+'Groups');
		this.exactValue = $('exactValue');
		/*
		this.tipObj = $('filterTip');
		this.tipTitle = $('filterTipTitle');
		this.tipContent = $('filterTipContent');
		*/
		this.sidebarPerson = $('sidebarPerson');
		this.filterResultsGroup = $(this.idPrefix+'ResultsGroup');
		
		if(this.showAdvanced) {
			this.showAdvanced.onclick = this.showAdvancedOptions.bindAsEventListener(this);
		}
		
		if(this.form)
			this.form.onsubmit = this.onSubmit.bindAsEventListener(this);
			
		if(this.showHideLink) {
			this.createHideLink(this.idPrefix);
		}

		this.onExpand();
	}
}