var FoldingBox = Class.create();

FoldingBox.EXPANDED = "&laquo;";
FoldingBox.UNEXPANDED = "&raquo;"

FoldingBox.prototype = {
	/* divs holders */
	fbSwitch:null,
	fbSwitchLink:null,
	fbSwitchText:null,
	fbSwitchTextCount:null,
	fbSwitchTextInd:null,
	
	fbDiv:null,

	/*visible/hidden indicator*/
	visible:false,
	
	setCount:function(count) {	
		this.fbSwitchTextCount.innerHTML = '['+count+']';
	},
	
	showHide:function(event) {
		if(this.visible)
		{
			this.fbDiv.style.display = "none";
			this.fbSwitchTextInd.innerHTML = FoldingBox.UNEXPANDED;
		}
		else {
			this.fbDiv.style.display = "block";
			this.fbSwitchTextInd.innerHTML = FoldingBox.EXPANDED;
		}
		this.visible = !this.visible;
	},


	initialize : function(id,divId) {
		this.fbSwitch=$(id+'Switch');
		this.fbSwitchLink=$(id+'SwitchLink');
		this.fbSwitchText=$(id+'SwitchText');
		this.fbSwitchTextCount=$(id+'SwitchTextCount');
		this.fbSwitchTextInd=$(id+'SwitchTextInd');
	
		this.fbDiv=$(divId);
		
		//add actions to the show/hide
		Event.observe(this.fbSwitchLink, 'click', this.showHide.bindAsEventListener(this));
		
	}		
}