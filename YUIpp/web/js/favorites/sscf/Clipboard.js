/**
 * 
 * ================= Clipboard handler =================
 * 
 * Generally subclass of BookmarksTree.js. Contains the methods
 * that operates on a clipboard.
 * 
 */


/* clipboard DOM elements keepers */
var clipboard;
var clipboardItem;

/* clipboards values, uri is moved to Globals.js */
var clipboardItemValue = "";
var clipboardItemObj = "";

/* clipboard element type */
var clipboardType = "";

/**
 * Inits clipboard DOM elements, and initially hides the clipboard
 */
function initClipboard() {
	clipboard = document.getElementById("clipboard");
	clipboardItem = document.getElementById("clipboardItem");
	
	if(clipboard) 
	{
		clipboard.style.display = "none";
	}
}

function copyNodeAbs(nodeId,nodeLabel,nodeOwner,parentId,type,cut)
{
	clipboard.style.visibility = "visible";
	clipboard.style.display = "block";
	
	//console.log(type);
	
	//if bookmark
	if(type=="bm"||type=="post"||type=="hex"||type=="web")
	{
		clipboardItem.innerHTML = nodeLabel;
	}
	//if directory - display with image.
	else if(type=="dir"||type=="site"||type=="forum")
	{
		label = nodeLabel;
		owner = nodeOwner;
		clipboardItem.innerHTML = "<img src=\""+dir+"/images/treeview/dir.gif\" style=\"padding-left:3px;padding-right:3px\"> "+label;
		if(owner!=null&&owner!="")
			clipboardItem.innerHTML +=  " ["+ owner + "]"; 
	}
	//save node id
	clipboardItemValue = nodeId;
	
	//save type
	clipboardType = type;
	
	//if the action is cut -> remeber the parent of node
	if(cut==true)
	{
		clipboardParentItemValue = parentId;
	}
	else
	{
		clipboardParentItemValue = "";
	}
	
	tree.clipboardItem = nodeId;
	tree.clipboardItemParent = parentId;
	
	//enable paste buttons if needed
	tree.pasteButtons(mainNode,nodeId,parentId,true,pasteText);
}


/**
 * Node copy action. 
 * Puts the node into the clipboard and switches on the '[paste]' text
 * 
 * @param treeId - id of the tree
 * @param index - index of a node in a tree
 * @param type - possible values 'bm' or 'dir'
 * @param cut - boolean - is it a cut action
 */
function copyNode(treeId,index,type,cut)
{
	//get node using treeId and index
	var getNode = YAHOO_FAVS.widget.TreeView.getNode(treeId,index);

	//save clipboard item class
	clipboardItemObj = getNode;
	
	copyNodeAbs(getNode.data.id , getNode.label,getNode.ownerName , getNode.parent.data.id , type , cut);

	
}

/**
 * Empties clipboard, removes '[paste]' texts
 */	
function emptyClipboard()
{
	clipboard.style.visibility = "hidden";
	clipboard.style.display = "none";
	clipboardItem.innerHTML = "";
	clipboardItemValue = "";
	clipboardParentItemValue = "";
	clipboardItemObj = "";
	tree.clipboardItem = null;
	tree.pasteButtons(mainNode,null,null,false,pasteText);
}