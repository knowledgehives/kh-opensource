/**
 * 
 * ================= Showing description of the resourecs ==================
 * 
 * 'Class' with methods used to show the resources description - small windows 
 *  displayed when '[more]' button is clicked.
 * 
 */

/* DOM objects keepers */
var dirDesc;
var dirDescCont;

/**
 * Initialise the DOM objects variables
 */
function initDescriptionBox() {
	
	dirDesc = document.getElementById("dirDesc");
	dirDescCont = document.getElementById("dirDescCont");
}

/**
 * Shows directory description
 * 
 * @param dirId - directory identifier
 * @param type - type of th resource. type is one of post/forum/site/webBookmark
 * @param event - browser event needed to get mouse position.
 */
function showDesc(dirId,type,event)
{
	dirDescCont.innerHTML = JS_MESSAGES["Bookmarks.loading"];
	webBookmark=false;
	
	var _url = "/directories/";
		
	if(type!=null&&type=="webBookmark")
	{
		_url = "/bookmarks/";
		webBookmark = true;
	}
	
	_url = _url + dirId;
	//for directory edit function is used to get dir info
	
	//REST API call: var ajax = new Ajax.Request(_url, {
	var ajax = new Ajax.Request('ajaxResults/nodeExample', {
		method : "get",
		parameters :null,
		onSuccess : function(transport) 
		{ 
			//IMPLEMENT descwindow that will be able to show description
			//descwindow.reveal($("dirDescIn"));
			//$("dirDescIn").style.display = "block";
			window.handleDescription(true,webBookmark,dirId,transport); 
		},
		onFailure : function() 
		{
			 window.handleDescription(false); 
		}
	});
}

/**
 * Handles description information received from the serwer.
 * 
 */
function handleDescription(success,webBookmark,dirId,transport)
{
	try 
	{	
 		if (success) 
 		{
 			var message = getMessage(transport.responseText);
    		if(message!="error"&&message!="empty")
    		{  		
				var dirObj =  eval('(' + message + ')');
				if(dirObj) 
				{
					if(webBookmark)
					{
						fillDescription(dirObj,dirId);
					}
					else
					{
						fillDirectoryDesc(dirObj,dirId);
					}
				}
    		}
    		else if(message=="empty") 
    		{
	    		setDirectoryDescMes(EMPTY,dirDescCont);
    		}
    		else
    		{
	    		setDirectoryDescMes(TIMEOUT,dirDescCont);
	    	}
 		}
 		if (!success) 
 		{
 			setDirectoryDescMes(TIMEOUT,dirDescCont);
 		}
 	} 
	catch(exc) 
	{
	 	console.log(exc);
	 	setDirectoryDescMes(TIMEOUT,dirDescCont);
	}
}

/**
 * Fills directory description window
 * 
 * @param desc - object with description details
 * @param dirId - id of the directory that the description is shown to.
 */
 
 /*
  * {"deleted":false,
  * "type":"REGULAR",
  * "label":"car",
  * "uuid":"24fbe5d9-0593-47c6-be3a-4e34cc68b374",
  * "provenance":null,
  * "readConditions":[],
  * "description":"about cars 5555",
  * "writeConditions":[],
  * "tagger":
  * 	{
  * 		"uri":"http:\/\/localhost:8080\/users\/0i0udlwn",
  * 		"person":"http:\/\/service.com\/users\/0i0udlwn",
  * 		"modifyDate":"2009-02-18T11:24:49.000+00:00",
  * 		"createDate":"2009-01-31T18:51:30.000+00:00",
  * 		"name":"TomuÅ ÄÄÄÅºÅ¼ÅÅÃ³ JakiÅ ÄÅÄ l"
  * 	},
  * "terms":
  * 	[
  * 		{"::type":"Term","type":"SUBJECT","label":"Career and Job Advancement","definition":null,"contextURI":"http:\/\/www.openvocabulary.info\/taxonomies\/dmoz\/all\/","modifyDate":"2009-01-31T18:54:17.000+00:00","createDate":"2009-01-31T18:51:30.000+00:00"},
  * 		{"::type":"Term","type":"KEYWORD","label":"car","definition":"(4-wheeled motor vehicle; usually propelled by an internal combustion engine; \"he needs a car to get to work\")","contextURI":"http:\/\/www.w3.org\/2006\/03\/wn\/wn20\/","modifyDate":"2009-01-31T18:54:17.000+00:00","createDate":"2009-01-31T18:51:30.000+00:00"},
  * 		{"::type":"Term","type":"KEYWORD","label":"car","definition":"(a wheeled vehicle adapted to the rails of railroad; \"three cars had jumped the rails\")","contextURI":"http:\/\/www.w3.org\/2006\/03\/wn\/wn20\/","modifyDate":"2009-01-31T18:54:17.000+00:00","createDate":"2009-01-31T18:51:30.000+00:00"}
  * 	],
  * "modifyDate":"2009-04-24T12:06:05.000+01:00",
  * "createDate":"2008-11-12T00:46:01.000+00:00"}
  */
function fillDirectoryDesc(desc,dirId)
{
	//dirDesc;
	var toPut = "";
	var termEntries = $A();
	
	//direcotry name
	if(desc.label)
	{
		toPut+= ("<b>"+JS_MESSAGES["Bookmarks.dir_name"]+" : </b>"+desc.label);
	}
	if(desc.description)
	{
		toPut+= ("<br><b>"+JS_MESSAGES["Bookmarks.dir_desc"]+" : </b>"+desc.description);
	}
	if(desc.tagger)
	{
		toPut+= ("<br><b>"+JS_MESSAGES["Bookmarks.dir_owner"]+" : </b>"+desc.tagger.name);
	}
	
	if(desc.terms)
	{
		var termsTab = new $H();
		desc.terms.each(function(item) {
			a = termsTab.get(item.contextURI)
			if(!a)
			{
				a = new $A();
			}
			a.push(item);

			termsTab.set(item.contextURI,a);
		});

		termsTab.values().each(function(tab) {
			toPut+= "<br><b>"+ tab[0].contextLabel +" : </b>";
			tab.each(function(itemIn) 
			{
				//FIXME: hard reference to filter class.
				toPut+= "<a href=\"javascript:void(1);\" onClick=\"filter.filterByValue('";
				toPut += tab[0].contextURI + "','";  //change to taxonomy name
				toPut += itemIn.uri + "','"; //this may be null now
				toPut += itemIn.label + "')\"";
				toPut += "title=\""+JS_MESSAGES["Bookmarks.dir_filter_by"]+"\">";         
				toPut += itemIn.label + "</a>";
				
				//[?] with meaning
				if(itemIn.definition!=null) {
					toPut += "<a href=\"javascript:void(1);\" onMouseOver=\"showTip(event,true,'";
					toPut += itemIn.label + "','";
					toPut += itemIn.definition.replace(/"/g,"`") + "')\" onMouseOut=\"showTip(event,false)\">";
					toPut += "<img src=\"/images/info.png\"></a> ";		
				}
				
				/*
				 * if(desc.thesauri[i].items[j].entrydef!=null&&desc.thesauri[i].items[j].entrydef!="") {
					toPut += "<a href=\"javascript:void(1);\" ";
					toPut += "id=\"term"+j+"\">";
					toPut += "<img src=\"/images/info.png\"></a> ";
				
					termDesc = $H({
						id : "term"+j,
						entry : desc.thesauri[i].items[j].entry,
						entrydef : desc.thesauri[i].items[j].entrydef
					});
					termEntries.push(termDesc);
				}
				 * 
				 */
			});
		});
	}
	
	if(desc.createDate&&desc.createDate!=""&&desc.createDate!=null)
	{
		cd = desc.createDate.split(/\D/);
		var d = new Date(cd[0],cd[1]-1,cd[2],cd[3],cd[4],cd[5],cd[6]);
		
		toPut+= ("<Br><b>"+JS_MESSAGES["Bookmarks.dir_createdWhen"]+" : </b>"+d.toLocaleString());
	}
	
	var putLinking = false;
	var toPutTmp = "";
	if(desc.linking) {
		toPutTmp+= "<br><b>"+JS_MESSAGES["Bookmarks.dir_linking"]+" : </b>";
		for(i=0;i<desc.linking.length;i++)
		{
			if(viewer.search(desc.linking[i].userid)==-1) {
				toPutTmp+= "<a href=\""+usersServlet+desc.linking[i].userid+"\">";         
				toPutTmp += desc.linking[i].username + "</a>, ";
				putLinking = true;
			}
		}
	}
	if(putLinking) toPut += toPutTmp;
	
	if(dirId)
	{
		//TODO: i18n
		toPut+= ("<br><b>"+JS_MESSAGES["Bookmarks.dir_direct_link"]+" : </b><a href=\""+dirId+"\">"+JS_MESSAGES['Bookmarks.command_copy']+"</a>");
	}
	/* TODO: uncomment when new feed will be implemented (FEED)
	if(dirId)
	{
		toPut+= ("<br><b>"+JS_MESSAGES["Bookmarks.dir_changes"]+" : </b>");
		//I decided to use whole directory uri - it may help with directories that have been created on
		//different servers
		//var pureDirId = dirId.substring(dirId.length - 40);
		toPut+= ("<a href=\"javascript:getFeed('"+dirId+"','rss_2.0','feedRssDiv')\" id=\"feedRssDiv\">rss</a>|<a href=\"javascript:getFeed("+dirId+",'atom_0.3','feedAtomDiv')\" id=\"feedAtomDiv\">atom</a>");
	}
	*/ 
	
	//IMPLEMENT showing the result in the window
	//dirDescCont.innerHTML = toPut;
	
	alert(toPut);
	
}

/**
 * Fills description of a webbookmark or SiocPost.
 * 
 * @param desc - object with description received from server.
 */
function fillDescription(desc)
{
	//dirDesc;
	var toPut = "";
	putTerms = false;
	tagsInfo = null;
	
	if(desc.label&&desc.label!=""&&desc.label!=null)
	{
		toPut+= ("<b>"+JS_MESSAGES["Bookmarks.dir_title"]+" : </b>"+desc.label+"<br>");
	}
	if(desc.description&&desc.description!=""&&desc.description!=null)
	{
		toPut+= ("<b>"+JS_MESSAGES["Bookmarks.dir_desc"]+" : </b>"+desc.description+"<br>");
	}
	if(desc['document'].url&&desc['document'].url!=""&&desc['document'].url!=null)
	{
		go = desc['document'].url;
		if(desc['document'].goURL!= null) go = desc['document'].goURL;
		toPut+= ("<b>"+JS_MESSAGES["Bookmarks.dir_link"]+" : </b><a href=\""+go+"\" title=\""+desc['document'].url+"\">"+desc['document'].url+"</a><br>");
	}
	if(desc.terms && desc.terms!=null)
	{
		for(i=0;i<desc.terms.length;i++)
		{
			tagsInfo = "";
			//tagsInfo+= "<a href=\""+usersServlet+desc.linking[i].userid+"\">";         
			tagsInfo += desc.terms[i].label;
			//tagsInfo += "</a>,";
			tagsInfo += " ";
			putTerms = true;
		}
	}
	if(tagsInfo!=null) toPut+= ("<b>"+JS_MESSAGES["Bookmarks.dir_tags"]+" : </b>"+tagsInfo+"<br>"); 		
	
	
	if(desc.createDate&&desc.createDate!=""&&desc.createDate!=null)
	{
		//2009-05-13T01:23:59.000+01:00
		//var d = new Date(desc.createDate.replace(/^(....).(..).(..).*$/, "$1/$2/$3"));
		
		cd = desc.createDate.split(/\D/);
		var d = new Date(cd[0],cd[1]-1,cd[2],cd[3],cd[4],cd[5],cd[6]);
		
		toPut+= ("<b>"+JS_MESSAGES["Bookmarks.dir_createdWhen"]+" : </b>"+d.toLocaleString()+"<br>");
	}
	
	/*
	 * TODO when will be available in API
	if(desc.savedBy&&desc.savedBy!=""&&desc.savedBy!=null)
	{
		toPut+= "<b>"+JS_MESSAGES["Bookmarks.dir_savedby_ext"]+" : </b>";
		for(i=0;i<desc.savedBy.length;i++)
		{
			if(viewer.search(desc.savedBy[i].userid)==-1) {
				toPut+= "<a href=\""+usersServlet+desc.savedBy[i].userid+"\">";         
				toPut += desc.savedBy[i].username + "</a>, ";
			}
		}
	}
	*/ 
	
	//IMPLEMENT showing the result in the window
	//dirDescCont.innerHTML = toPut;
	
	alert(toPut);
}