/**
 * 
 * ===================== Friend add window handler ===================
 * 
 * This 'class' handles the actions connected with 'friend add' window.
 * In UI user can click on a person name and either send mail or add the person
 * to the friends list.
 */


/* tmp variables keepers */
var friendId;
var friendName;

/* DOM objects keepers */
var addFriendDiv;
var addFriendLevel;
var addFriendSaveBut;
var addFriendCloseBut;

/**
 * Inits the DOM object keeper
 */
function initFriendAdd() {
	
	//IMPLEMENT - window that can contain the friendAdd form
	//addFriendDiv = new Window("addFriendCont",true); 
	//document.getElementById("addFriend");
	//$("addFriendCloseBut").onclick = addFriendDiv.hide.bindAsEventListener(addFriendDiv);
}


/**
 * Validates if the level given by user is correct. 
 * number from 0 to 999 is accepted
 * 
 * @param level - string to validate
 * @return true if level is correct, otherwise - false. 
 * 
 */
function validateLevel(level)
{
	var RegExp = /^([\d]{1,3})$/;
    if(RegExp.test(level)){ 
        return true; 
    }else{ 
        return false; 
    } 
}

//TODO - currently we are showing the possibility to send mail to some friend, but now there will be no mailbox availabel!!

/**
 * Shows 'add friend' window.
 * Window may contain: 
 *  - information that the given person is already a friend of invoker.
 *  - for that allows invoker to add the person as a friend.
 * 
 * @param id - friend id (mbox)
 * @param frName - friend's name
 * @param event - needed to get mouse cursor position
 */
function showAddFriend(id,frName,event)
{
	addFriendSaveBut = document.getElementById("addFriendSaveBut");
	if(Friendship.findFriend(id))
	{
		//if(event)
		//	moveToMousePosition(addFriendDiv,event);	
		//showHideMes(-1,true,addFriendDiv);
		
		document.getElementById("addFriendForm").style.display = "none";
		document.getElementById("addFriendForm").style.visibility = "hidden";
		document.getElementById("addFriendInfo").style.display = "block";
		document.getElementById("addFriendInfo").style.visibility = "visible";
		document.getElementById("addFriendInfoName").innerHTML = frName;
		document.getElementById("addFriendInfoText").innerHTML = "";
		addFriendSaveBut.disabled = true;
		
		addFriendDiv.show();
	}
	else if(viewer.search(id)>=0)
	{
		return;
	}
	else
	{
		//if(event)
		//	moveToMousePosition(addFriendDiv,event);	
		//showHideMes(-1,true,addFriendDiv);
		addFriendSaveBut.disabled = false;
		
		document.getElementById("addFriendForm").style.display = "block";
		document.getElementById("addFriendForm").style.visibility = "visible";
		document.getElementById("addFriendInfo").style.display = "none";
		document.getElementById("addFriendInfo").style.visibility = "hidden";
		
		document.getElementById("addFriendName").innerHTML = frName;
		addFriendLevel = document.getElementById("addFriendLevel");
		
		friendName = frName;
		friendId = id;
		
		addFriendDiv.show();
	}
}

/**
 * Add new friend to the database.
 * Read the parameters from the form, check the level and eventually 
 *  sends ajax request.
 * If server operation ends with success node with new friend is returned -
 *  - this node is then added to the tree and displayed.
 */
function addFriend()
{
	//validate the knowledge level
	var process = false;
	var valid = false
	setError(addFriendLevel,false,"foo");
	
	if(addFriendLevel.value!="")
	{
		process = true;
		valid = validateLevel(addFriendLevel.value);
		if(valid)
		{
			if(addFriendLevel.value>=0&&addFriendLevel.value<=100)
				valid=true;
			else valid = false
		}
	}
	else 
	{
		setError(addFriendLevel,true,JS_MESSAGES["Bookmarks.add_web_page_required"]);
	}
	if(process&&!valid)
	{
		setError(addFriendLevel,true,"Wrong level value!");
		process = false;
	}
	
	if(process)
	{
		_parameters = "item=" + encodeURI( friendId ) +
				"&level=" + addFriendLevel.value;	

		var ajax = new Ajax.Request("/favorites/friends", {
			method : "post",
			parameters : _parameters,
			onCreate : Notifications.WORKING.show(),
			onSuccess : function(transport) { 
				window.handleFriendAdd(true,transport); 
			},
			onFailure : function() { window.handleFriendAdd(false); }
		});
		
		addFriendSaveBut.disabled = true;
	}
	
}

function handleFriendAdd(success,transport) 
{
	Notifications.WORKING.hide();
	addFriendDiv.hide();
	try {	
 		if (success) {
 			var message = getMessage(transport.responseText);
    		if(message!="error")
    		{  		  
	    		Friendship.addFriend("friendusers",friendId);
    		}
    		else
    		{
    			addFriendSaveBut.disabled = false;
    		} 		
 		}
 		if (!success) {
 			addFriendSaveBut.disabled = false;
 			Notifications.TIMEOUT.show();
 		}
 	} 
	catch(exc) 
	{
	 	addFriendSaveBut.disabled = false;
	 	Notifications.TIMEOUT.show();
	}
}

