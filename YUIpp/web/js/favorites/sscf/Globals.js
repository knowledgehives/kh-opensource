/**
 * 
 * ======================= Globals ===============================
 * 
 * It contains variables that are used by more than one 'class'.s
 * 
 */
/* used for separete the uris in requests */
var SEP=";;";

/* main tree object */
//may be moved and used only by BookmarksTree, but only if Clipbaord will be 
//sub-class of BookmarksTree
var tree;

/* first node */
//check: move to BookmarksTree?
var n_0;

var progresImg = "<img src=\"images/spinner.gif\">";
var progresImgSmall = "<img src=\"images/spinnersm2.gif\">";
var progresImgSmallFilter = "<img src=\"images/spinnersm-filter.gif\">";
var progresImgSmallRecommend = "<img src=\"images/spinnersm-recommend.gif\">";
var progresImgSmallFilterFind = "<img src=\"images/spinnersm-filterfind.gif\">";

/* array with last roots - used for building the tree */
var roots = new Array();

//check if it is the same as n_0
var mainNode;

var creatingUri;

/* global timeOut */
//need to be moved one to Ajax
var timeoutId;

/* path to the servlet */	
var uriBegining = "SscfAjax";

/* types of the messages */
var WORK = 0;
var TIMEOUT = 1;
var EMPTY = 2;
var ILLEGAL = 10;

var INBOX;
var MAINBOX;

/* paste command value */
var pasteText = JS_MESSAGES["Bookmarks.command_paste"];

/* friends array */
var friendsArray = new Array();

var illegalDiv;

var GLOBALTIMEOUT = 45000;

/* Used in Clipboard.js, but is also needed to indicate if the bookmark
 * should be also cut (in BookmarksTree.js::pasteNode() method) 
 */
var clipboardParentItemValue = "";

var importWindow;

var recomFoldingBox;

/**
 * Checks if the returned response contains any of the standard messages.
 * If not - it returns the response
 * 
 * @param resp - reponsonse text that have been received from the server
 * @param type - not used
 * @return - if message was standard - content of this message, otherwise - 
 * 				unchanged response.
 */	
function getMessage(resp,type)
{
	//console.log("getMessage"+resp);

 	if(resp.search("{\"message\":\"error\"}")!=-1 ||
 		resp.search("{message:\"error\"}")!=-1 ) return "error";
 	if(resp.search("{\"message\":\"empty\"}")!=-1 ||
 		resp.search("{message:\"empty\"}")!=-1 ) return "empty";
 	if(resp.search("{\"message\":\"ok\"}")!=-1 ||
 		resp.search("{message:\"ok\"}")!=-1 )  return "ok";

	return resp;
}

function encodeURIforSscf(uri)
{
	var newUri = uri;
	if(newUri != null) {
		newUri = newUri.replace(/\&/g,"!!");
		newUri = newUri.replace(/#/g,"::");
	}

	return encodeURI(newUri);
}

function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}