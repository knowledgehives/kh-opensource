/**
 * 
 * ====================== 'Class' loader ========================
 * 
 * Invokes init* methods on pseudo classes and sets some specific variables.
 * 
 */

/** 
 * inits the application
 */
function initTree() {
	
	//get INBOX and MAINBOX uuids
	INBOX = null;MAINBOX = null;
	if(stritems2!={})
	{
		if(typeof stritems2.tagger != "undefined") {
			INBOX = stritems2.tagger.inbox.uuid;
			MAINBOX = stritems2.tagger.mainbox.uuid;
		}
		else if(Object.isArray(stritems2))
		{
			var l = stritems2.length;
			for(var i=0;i<l;i++)
			{
				if(typeof stritems2[i].tagger != "undefined" && 
					stritems2[i].tagger.person == viewer) {
					INBOX = stritems2[i].tagger.inbox.uuid;
					MAINBOX = stritems2[i].tagger.mainbox.uuid;
				}	
			}
		}
	}

	buildTree();
	
	//setWidths();
	//window.onresize = setWidths();
	
	//clipboard
	initClipboard();
	
	//direcotry descriptions
	initDescriptionBox();

	//directory creation/edit
	initCreateDir();
	
	initBookmarkAdd();
	
	//initImportFirst();
	
	initFriendAdd();

	uriBegining= contextPath + "/" + uriBegining; 
	
	illegalDiv = document.getElementById("illegal");
	
	if(document.getElementById("page_loading"))
		document.getElementById("page_loading").style.display="none";
	
	tree.subscribe("expandComplete", function(node) {           
	  		if(tree.clipboardItem!=null) {
 				tree.pasteButtons(node,tree.clipboardItem,tree.clipboardItemParent,true,pasteText);
			} 
	  	});	
	  	  	
	//init recomendations module
	var recom = new Filter("recomContainer","recom");
};