var visibleTooltips = new Array();
var tooltipTimers = new Array();

var tipObj = null;
var tipTitle = null;
var tipContent = null;
var tipInited = false;

/**
 * Functions for displaying menu-like tooltip
 */
function showTooltip(event,id) {
	var show = false;
	if(typeof visibleTooltips[id] != 'undefined')
	{	
		if(visibleTooltips[id] == false) show = true;
	}
	else show = true;
	
	if(show)
	{
		document.body.appendChild($(id));
		moveToMousePosition($(id),event);
		$(id).style.display = "block";
		visibleTooltips[id] = true;
		$(id).onmouseout = function() {
			tooltipTimers[id] = setTimeout("hideTooltip('"+id+"')",600);
		};
		$(id).onmouseover = function() {
			clearTimeout(tooltipTimers[id]);
		};
		$(id).onclick = function() {
			clearTimeout(tooltipTimers[id]);
			hideTooltip(id);
		};
	}
}


function hideTooltip(id) {
	$(id).style.display = "none";
	visibleTooltips[id] = false;
}

/**
 * Function shows small infomational tooltip
 * without any user action available
 */
function showTip(e,show,title,content) {
	
	if(!tipInited)
		createTip();

	moveToMousePosition(tipObj,e,150,20);
	
	if(show)
	{
		tipTitle.innerHTML = title;
		tipContent.innerHTML = content;
		tipObj.style.display = 'block';
	}
	else
	{
		tipTitle.innerHTML = "";
		tipContent.innerHTML = "";
		tipObj.style.display = 'none';
	}
}

function createTip() {
	var div = document.createElement("div");
	div.id = "toolTipInfo";
	
	var divTipTitle = document.createElement("div");
	divTipTitle.id = "toolTipInfoTitle";
	
	var divTipCont = document.createElement("div");
	divTipCont.id = "toolTipInfoContent";
	
	div.appendChild(divTipTitle);
	div.appendChild(divTipCont);
	
	document.getElementsByTagName("body")[0].appendChild(div);
	
	tipObj = $("toolTipInfo");
	tipTitle = $("toolTipInfoTitle");
	tipContent = $("toolTipInfoContent");
	
	tipInited = true;
}