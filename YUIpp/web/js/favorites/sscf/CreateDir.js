/**
 * 
 * ==================== Create/edit directory ======================
 * 
 * 'Class' that hadles directory creation and edit process.
 * 
 */ 

/* stores last values of name and description in edit process */
var lastName;
var lastDesc;

/* following two variables creates a id of a tmp node */
var tmpTreeId;
var tmpIndex;

var winObj;

/* ethier add or edit */
var EDIT = "EDIT";
var ADD = "ADD";

/* default mode */
var dirMode = ADD;

/**
 * Inits DOM objects and panels in creation directory window.
 */
function initCreateDir() {
	//IMPLEMENT winObj object that will allow to 
	//edit/add directory
	//winObj = new Window("dirEditCont");
}

/**
 * Shows create directory window
 * 
 * @param treeId - tree identifier
 * @param index - index of the directory node. (together with treeId - node identifier)
 * @param dirId - id of the directory in which new directory should be added
 * @param event - needed to determine mouse position.
 * @param edit - indicates mode - if true - this is mode edit
 */
function showCreateDir(treeId,index,dirId,event,edit)
{
	alert("Implement window editing/adding window.")
	
	//Adding directory into dirId directory
	if(!(edit!=null&&typeof edit!="Unspecified"))
	{
		//IMPLEMENT setting up create window

		//winObj.show();	
		
		//winOb.onSave = handleDirCreation;
		
		dirMode = ADD;
	}
	else {
	//Editing directory given by dirId
		dirMode = EDIT;

		//winObj.show();
		
		//winObj.onSave = handleDirEdit;
	}
	
	//save information about selected directory
	tmpTreeId = treeId;
	tmpIndex = index;
}

function handleDirCreation(Directory,success) {
	if(success) {
		//TODO Implement
		
		rebuildTree();
		
		winObj.hide();
	}
}

function handleDirEdit(Directory,success) {
	if(success) {
		//TODO Implement
	}
	winObj.hide();
}

