/**
 * 
 * ======================= Bookmarks tree handling =====================
 * 
 * 'Class' containes the methods for handling the YUI TreeView object:
 * - It generates the tree from given objects containing nodes description.
 * - rebuilds/builds the tree (TreeView object)
 * - Handles nodes':
 * 		- removing
 * 		- importing (accepting) sugested node
 * 		- pasting 
 * - Loads data for dynamic node
 * - enables/disables checkboxes near the tree nodes (for selection)
 * 
 */

var callbackNodeClass;



/**
 * Generates tree nodes from object containing nodes data.
 * For nodes object format see:
 * http://wiki.corrib.org/index.php/S3B/SSCF/SOASpec
 * 
 * Supports also node inserting - parts of the tree can be inserted to the node
 *  that have been set to dynamic load.
 * 
 * Method also creates the array of the friends (while generating friends nodes)
 * 
 * @param nodesObj - object with nodes data.
 * @param insert - it needs to be set to true in case of inserting parts of the tree.
 */
 /*
function generateNodes(nodesObj,insert,_frId)
{
	var frId = null;
	if(typeof _frId != 'undefined')
		frId = _frId; 
	
	
	for(i=0;i<nodesObj.nodes.length;i++)
	{
		//Get the root node for current node.
		// Roots are generated recursivelly (on the server side), so generated 
		// nodes will always have correct parent node. (in root array)
		level = nodesObj.nodes[i].content.level;
		if(frId!=null) root = frRoots[frId][level-1];
		else root = roots[level-1];
		
		var process = true;
		if(insert)
		{
			//wait for friend's nodes...
			if(frId==null && mainNode !=null && nodesObj.nodes[i].content.id.length==8 && 
				nodesObj.nodes[i].content.id != mainNode.data.id)
			{
				//change the mode to fr tree...
				frId = nodesObj.nodes[i].content.id;
				if(!frTrees[frId])
				{
					createFrTree(frId);
					visibleFrTreeId = frId;	
				}
				else if(frId!=visibleFrTreeId) showFrTree(frId);
				level = nodesObj.nodes[i].content.level;
				root = frRoots[frId][level-1];
			}
			
			if(frId!=null)
				var node = frTrees[frId].getChildNodeByProperty(root,"id",nodesObj.nodes[i].content.id);
			else var node = tree.getChildNodeByProperty(root,"id",nodesObj.nodes[i].content.id);
			if(node!=null)
			{
				process = false;
				if(frId!=null) frRoots[frId][nodesObj.nodes[i].content.level] = node;
				else roots[nodesObj.nodes[i].content.level] = node;
				
				//TODO - change if such identifying of person node is wrong
				if(node.data.id.length == 8) 
				{
					node.setDynamicLoad(null);
					node.expanded = true;
				}
				else 
				{
					if(nodesObj.nodes[i].dyn&&(nodesObj.nodes[i].dyn==false||
						nodesObj.nodes[i].dyn&&nodesObj.nodes[i].dyn=="false")) {		
						node.setDynamicLoad(null);
					} 
					else if(!nodesObj.nodes[i].dyn) node.setDynamicLoad(null);
				
					if(nodesObj.nodes[i].content.isOpened
						&&(nodesObj.nodes[i].content.isOpened==true||
						nodesObj.nodes[i].content.isOpened=="true"))
					{ 
						node.expanded = true;
					}
				}
			}
		}
		
		if(process)
		{
			tmpNode = getNewNode(root,nodesObj.nodes[i].type,nodesObj.nodes[i].content,false,false);
			
			if(nodesObj.nodes[i].type=="person")
			{
				if(!mainNode) mainNode = tmpNode; 	
			}
			
			
			if((nodesObj.nodes[i].dyn=="true"||nodesObj.nodes[i].dyn==true)&&tmpNode!=mainNode)
			{
				if(frId!=null)
					tmpNode.setDynamicLoad(loadDataForNodeBinded.bind(this,frId));
				else tmpNode.setDynamicLoad(loadDataForNode);
			}
			
			//set just created node in roots array 
			//if next node will be in the lower leyer, this node will be the root for that node
			if(frId!=null) frRoots[frId][level]=tmpNode;
			else roots[level]=tmpNode;
		}
	}
}
*/


function generateNodes2(nodes,root,insert,_frId)
{
	try {
	//	debugger;
	
	if(root==null) root=tree.getRoot();	
	var rootToBe = null;
	var goDeep = true;
	
	//nodes may not be array
	if(!Object.isArray(nodes)) nodes = [nodes];
	
	for(var i=0;i<nodes.length;i++)
	{
		if(nodes[i].type != "INBOX" && nodes[i].type != "DROPBOX")
		{	
			process = true;
			if(insert)
			{
				//change some properties of existing nodes
				node = tree.getChildNodeByProperty(root,"id",nodes[i].uuid);
				if(node!=null)
				{
					process = false;
					
					rootToBe = node;
								
					//TODO - change if such identifying of person node is wrong
					if(typeof node.getStyle() != '' &&
							node.getStyle().search("ygpv") > -1) 
					{
						node.setDynamicLoad(null);
						node.expanded = true;
					}
					else 
					{
						if((typeof nodes[i].isEmpty != "undefined") &&
				  			nodes[i].isEmpty == true) {		
							node.setDynamicLoad(null);
						} 
					
						if(nodes[i].isOpened && (nodes[i].isOpened==true||
							nodes[i].isOpened=="true"))
						{ 
							node.expanded = true;
							node.setDynamicLoad(null);
						}
					}
				}
			}
			
			if(process)
			{
				root.setDynamicLoad(null);
				var type;
				
				if(typeof nodes[i].type != "undefined") type = nodes[i].type;
				//if(typeof nodes[i]['::type'] != "undefined") type = nodes[i]['::type'];
				
				tmpNode = getNewNode(root,type,nodes[i]);
				
				if(nodes[i].uuid == MAINBOX)
				{
					if(!mainNode) mainNode = tmpNode; 	
				}
				
				
				if((typeof nodes[i].isEmpty != "undefined") &&
				  	nodes[i].isEmpty == false &&
				  	tmpNode!=mainNode)//check for entries
				{
					tmpNode.setDynamicLoad(loadDataForNode);
				}
				
				if(seluri!="" && seluri==nodes[i].uuid) {
					tmpNode.selected = true;
					selectedNodes.push(tmpNode);
				}
				
				rootToBe = tmpNode;
			}
			
			//go deeper...
			if(typeof nodes[i].entries != "undefined" &&
				nodes[i].entries != []
				&& rootToBe != null)
			{
				generateNodes2(nodes[i].entries,rootToBe,insert,_frId)
			}
		}
	}
	}catch(e) {console.log(e);}
}

function getMainBox() {
	var ajax = new Ajax.Request("/taggers/"+viewerUid, {
		method : "get",
		onSuccess : function(transport) { 
			obj = eval('('+transport.responseText+')');
    		if(obj && typeof obj.mainbox != "undefined")
    		{
				MAINBOX = obj.mainbox.uuid;
				INBOX = obj.inbox.uuid;
				generateMainBox();
    		}	
		}
	});
}

function generateMainBox() {
	//REST API call: var ajax = new Ajax.Request("/directories/"+MAINBOX, {
	var ajax = new Ajax.Request("js/items.js", {
		method : "get",
		onSuccess : function(transport) { 
			obj = eval('('+transport.responseText+')');
    		if(obj)
    		{
				generateNodes2(obj,null);
				rebuildTree();
    		}	
		}
	});
}

/**
  Generates the tree for the first time
 */
function buildTree(_frId,_itemsObj) {
	
	tree = new YAHOO_FAVS.widget.TreeView("treeDiv1");
	tree.setExpandAnim(YAHOO_FAVS.widget.TVAnim.FADE_IN);
	tree.setCollapseAnim(YAHOO_FAVS.widget.TVAnim.FADE_OUT);
	
	//Owners bookmark creation
	var nodesObj2 = stritems2;

	//console.log(nodesObj2);

	//generate rest nodes
	if(seluri!="") {
		generateNodes2(nodesObj2,tree.getRoot(),true);
	
		tree.draw();
		//there might be no results in main tree
		if(MAINBOX == null)
		{	
			getMainBox();			
		}
		else { 
			//expand to selected
			nodes = tree.getNodesByProperty("id",seluri);
			nodes.each(function(node) {
				tree.expandToRoot(node);
			});
		}			
	}
	else { 
		//generateNodes(nodesObj);
		generateNodes2(nodesObj2,tree.getRoot());

		//draw the tree
		tree.draw();
	}
	
	//it breaks ie.
	if(!window.ActiveXObject) {
		YAHOO_FAVS.widget.TreeView.preload("ygfv");
		YAHOO_FAVS.widget.TreeView.preload("ygdv");
		YAHOO_FAVS.widget.TreeView.preload("ygbv");
		YAHOO_FAVS.widget.TreeView.preload("ygwbv");
	}
} 

/**
 * Rebuilds tree - invoked when tree has been created 
 *  and some parts of the tree has changed.
 */	
function rebuildTree(insert)
{
	tree.draw();

	//redraw the paste button
	if(typeof tree.clipboardItem != 'undefined' && tree.clipboardItem != "")
		tree.pasteButtons(mainNode,tree.clipboardItem,tree.clipboardItemParent,true,pasteText);	
	if(creatingUri==false)
    	switchCheckboxes(!standalone);
    else
    	switchCheckboxes(creatingUri);
}

/**
 * Special version of LoadDataForNode that allows to pass additional parameter to this method
 */
function loadDataForNodeBinded(param,node,onCompleteCallback)
{
	loadDataForNode(node, onCompleteCallback,param);
}

/**
 * Loads bookmarks for the given, dynamic node
 * 
 * @param node - node object for which data should be loaded
 * @param onCompleteCallback - function which informs the tree that
 * 		it is updated. It needs to be invoked in the body of the 
 * 		following function.
 */
function loadDataForNode(node, onCompleteCallback,param) {
	
	var frId = null;
	var insert = null;
	if(typeof param != 'undefined')
	{
		if(param=="insert")
			insert = true;
		else
			frId = param;
	}
		
	callbackNodeClass = node;
	
	//it will not be used
	//if(frId!=null) frRoots[frId][node.data.level] = node;
	//else roots[node.data.level]=node;
	
	nodObj="";
	message="";
		
	//REST API call: var ajax = new Ajax.Request("/directories/"+node.data.id+"?depth=1", 
	//Example call:
	var ajax = new Ajax.Request("ajaxResults/levelExample", 
	{
		method : "get",
		onSuccess : function(transport) { 
			var message = transport.responseText;
    		try {
    		if(message!="")
    		{
	    		var nodObj =  eval('('+message+')');
	    		
	    		if ( frId != null )
	    		{
	    			generateNodes2(nodObj.entries,node,insert,frId);
	    		}
	    		else 
	    		{
	    			generateNodes2(nodObj.entries,node,insert);
	    		}
    		}
    		}catch(e){console.log(e);}
		},
		onFailure : function() {
			Notifications.TIMEOUT.show();
		},
		onComplete : function() {
    		//notify the tree!
			onCompleteCallback();
		}
	});			
}
	
/**
 * If totNew == true - that means that this is totally new element
 *  and parameters may have different format that the one that is 
 *  commont among YUI++ classes.
 * 
 *  setUpDynamic - set to true if you will not be setting up dynamic 
 *    content loading
 */
function getNewNode(parentObj,type,parameters,setUpDynamic,totNew,debug)
{
	try {
	
	if(typeof totNew == "undefined") totNew = true;
	if(typeof setUpDynamic == "undefined") setUpDynamic = false;
	
	if(typeof parameters.status != "undefined")
		var recom = (parameters.status =="RECOMMENDED");
	
	if(debug)
		debugger;
	
	//if totally new - process parameters
	if(totNew) {
		newOptions= new Object();
		
		if(parameters.dirId)
		 	newOptions.dirId = parameters.dirId;
		
		if(parameters.id)	
			newOptions.id = parameters.id;
		else if(parameters.uuid)
			newOptions.id = parameters.uuid;
		
		newOptions.isLinkedIn = false;
		newOptions.isOpened = false;
		
		own = false;
		
		if(typeof parameters.tagger != 'undefined') 
			own = parameters.tagger.person == viewer;
		else if(typeof parentObj.data != "undefined" && parentObj.data != null)
			own = parentObj.data.ownerUid == viewerUid; 
		
		newOptions.own = own;
		newOptions.removable = own;
			
		if(parameters.ownerUid)
		 	newOptions.ownerUid = parameters.ownerUid;
		else if(typeof parameters.tagger != 'undefined' ) {
			newOptions.ownerUid = parameters.tagger.uri.substring(parameters.tagger.uri.length-8);
		}
		 	
		if(parameters.ownerName)
		 	newOptions.ownerName = parameters.ownerName;
		else if(typeof parameters.tagger != "undefined")
		 	newOptions.ownerName = parameters.tagger.name;
		 	
		if(parameters.label)
		 	newOptions.label = parameters.label;
	}
	else newOptions = parameters;
	
	if(typeof parameters['::type'] != 'undefined') 
		var addType = parameters['::type'];
	else addType = null;
		
	//===== genertediferent types of nodes. =====
	if(type=="MAINBOX" && own)
	{
		newOptions.label = JS_MESSAGES["Bookmarks.bookmarks_label_owner"];
		var tmpNode = new YAHOO_FAVS.widget.PersonNode(newOptions,parentObj,(seluri == ""));		
	}
	else if(type=="MAINBOX" && !own) {
		if(totNew && typeof newOptions.ownerName != "undefined")
			newOptions.label = newOptions.ownerName+" - "+JS_MESSAGES["Bookmarks.bookmarks_label"];
		else
			newOptions.label += " - "+JS_MESSAGES["Bookmarks.bookmarks_label"];
		var tmpNode = new YAHOO_FAVS.widget.FriendNode(newOptions,parentObj,true);	
		
	}
	else if(type=="dir"||(type=="REGULAR" && !recom && addType=="Directory"))
	{
		if(newOptions.isOpened||newOptions.isOpened=="true") 
		{
			opened=true; 
			root.expanded = true;
		}
		else opened=false;
		var tmpNode = new YAHOO_FAVS.widget.DirectoryNode(newOptions,parentObj,opened);
	}
	else if(type=="REGULAR" && addType=="SmartDirectory")
	{
		var tmpNode = new YAHOO_FAVS.widget.DiuDirectoryNode(newOptions,parentObj,false);
	}
	else if(type=="REGULAR" && recom)
		var tmpNode = new YAHOO_FAVS.widget.SugDirectoryNode(newOptions,parentObj,false);
	else if(type=="webBookmark"||type=="web"||addType=="Bookmark") {
		var tmpNode = new YAHOO_FAVS.widget.WebBookmarkNode(newOptions,parentObj,false);
	}
	else 
		var tmpNode = new YAHOO_FAVS.widget.BookmarkNode(newOptions,parentObj,false);
	
	tmpNode.standalone = standalone;
	
	if(tmpNode!=null && setUpDynamic)
	{
		var isEmpty = true;
		if(typeof parameters.isEmpty != "undefined")
			isEmpty = parameters.isEmpty;
		
		if(!isEmpty)
		{
			tmpNode.setDynamicLoad(loadDataForNode);
		}
	}
	
	}catch(e) {console.log(e);}
	return tmpNode;
}


/**
 * Pastes node into new location, could also cut the old one.
 * 
 * @param treeId
 * @param index - together with treeId parameters are an unique node identifier.
 * @param[global] clipboardParentItemValue - if this variable is set it means that
 * 						the bookmark should be cut. (Variable is set in Clipboard.js)
 */
function pasteNode(treeId,index) {
	var getNode = YAHOO_FAVS.widget.TreeView.getNode(treeId,index);

	if(!getNode.expanded) getNode.expand();
	
	//wait for dynamic load to end before pasting anything.
	if(!tree.locked) {
		//check if node do not contain node that should be pasted
		proceed = true;
		for (var i=0;i<getNode.children.length;++i)
		{
			if(getNode.children[i].data.id===clipboardItemValue){
				proceed = false;
				break;
			}
		}
		
		if(proceed)
		{
			roots[getNode.data.level]=getNode;
			// -- code to get your data, possibly using Connect --
			
			if(clipboardParentItemValue != "") cut = true; else cut=false;
			
			_method='post';
			uri = '/';
			if(clipboardType=="dir")
				uri += "directories/";
			else uri+= "bookmarks/";
			
			uri += clipboardItemValue + '/';
			
			if(cut) {
				uri +=  clipboardParentItemValue + '?moveTo='+ getNode.data.id;
			}
			else {
				uri += getNode.data.id;
				_method = 'put';
			}
			
			//REST API call: var ajax = new Ajax.Request(uri, {
			//Example call:
			var ajax = new Ajax.Request('ajaxResults/nodeExample', {
				method : _method,
				onSuccess : function(transport) { 
			       	
			       	result = eval('('+transport.responseText+')');			
			       		
		    		if(cut)
		    		{
		    			if(clipboardItemObj!="") {
		    				tree.popNode(clipboardItemObj);
		    				clipboardItemObj.appendTo(getNode);  
							getNode.refresh(); 
		    			}
		    			else
		    			{
		    				//todo: create the node in case operation is done from inbox
		    				getNewNode(getNode,clipboardType,result);
		    				inbox.removeTheNode(tree.clipboardItem,null);
		    			}	
		    		}
		    		else
		    		{
						getNewNode(getNode,clipboardType,result,true);
		    		}	
		    		rebuildTree();
				},
				onFailure : function() {
					;
				}
			});	
		}
	}
	else 
	{
		//wait for tree to be unlocked.
		window.setTimeout(
			"pasteNode('"+treeId+"','"+index+"')",
			200 
		);	
	}
}

/**
 * Import (accepts) suggested node
 * 
 * @param treeId
 * @param index - both parameters created an unique node identifier.
 */
function importSugNode(treeId,index) {
	var nodeToImport = YAHOO_FAVS.widget.TreeView.getNode(treeId,index);
	var parentNode = nodeToImport.parent;
	var item = nodeToImport.data.id
		
	//REST API call: var ajax = new Ajax.Request("/recommendations/"+item+"/"+parentNode.data.id+"?action=accept", {
	var ajax = new Ajax.Request('ajaxResults/nodeExample', {
		method : "post",
		onSuccess : function(transport) { 
				   
    		var nodObj = eval('(' + transport.responseText + ')');
    		
    		//remove the node from the tree
    		tree.removeNode(nodeToImport);
    		
    		getNewNode(parentNode,"dir",nodObj,true,true);
    		
    		//now - rebuild tree.
    		rebuildTree();
		},
		onFailure : function() {
			;
		}
	});
}

/**
 * Rejcts suggested node
 * 
 * @param treeId
 * @param index - both parameters created an unique node identifier.
 */
function rejectSugNode(treeId,index) {
	var nodeToImport = YAHOO_FAVS.widget.TreeView.getNode(treeId,index);
	var parentNode = nodeToImport.parent;
	var item = nodeToImport.data.id
		
	//REST API call: var ajax = new Ajax.Request("/recommendations/"+item+"/"+parentNode.data.id+"?action=reject", {
	//Example call:
	var ajax = new Ajax.Request('ajaxResults/nodeExample', {
		method : "post",
		onSuccess : function(transport) { 
			
    		//remove the node from the tree
    		tree.removeNode(nodeToImport);
    	
    		//now - rebuild tree.
    		rebuildTree();
    		
		},
		onFailure : function() {
			;
		}
	});
}


/**
 * Removes node from the tree.
 * 
 * @param treeId
 * @param index - both parameters created an unique node identifier.
 */
function removeTreeNode(treeId,index,permanently) {
	var getNode = YAHOO_FAVS.widget.TreeView.getNode(treeId,index);
	 
	_parameters = "parent=" + encodeURI( getNode.parent.data.id );
				if((typeof permanently != "undefined" )&& permanently)
					parameters +="&permanently=1";
		
	var url = "/directories";
	if(getNode.getResType()!="node")
		url ="/bookmarks";
		
	url = url + '/' + encodeURI( getNode.data.id ) + '/' + getNode.parent.data.id;
		
	//REST API call: var ajax = new Ajax.Request(url, {
	//Example call
	var ajax = new Ajax.Request('ajaxResults/nodeExample', {
		method : "delete",
		parameters : _parameters,
		onSuccess : function(transport) { 
		
			//request success means that removal was successful  
    		parentNode = getNode.parent;
	    	tree.removeNode(getNode);
	    	if(parentNode.children.length==0)
	    		parentNode.removable = true;
	    	rebuildTree();
    	
		},
		onFailure : function() {
			;
		}
	});	
}

/**
 * Shows/hides the checkboxes near the nodes. This checkboxes
 *  are used to indicate if the node is selected
 * 
 * @param on - true=show checkboxes, false=hide checkboxes
 * 
 */
function switchCheckboxes(on)
{
	var divs = document.getElementsByName("SSCFSelectedDirs");
	if(on==true)
	{
		for(i=0;i<divs.length;i++)
		{
			divs[i].style.visibility = "visible";
			divs[i].style.display = "block";
		}
	}
	else if(standalone)
	{
		for(i=0;i<divs.length;i++)
		{
			divs[i].style.visibility = "hidden";
			divs[i].style.display = "none";
		}
	}
}

function switchCheckboxesDemo(on) {
	creatingUri = on;
	switchCheckboxes(on);
}

/**
 * Checks if element exist in given destination.
 * 
 * @param dest - destination directory
 * @param itemId - item to check if exist
 */
function checkIfExists(dest,itemId)
{
	for(var j=0;j<dest.children.length;++j)
	{
		if(itemId==dest.children[j].data.id)
		{
			return true;
		}
	}
	return false;
}