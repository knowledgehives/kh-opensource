/* Copyright (c) 2006 Yahoo! Inc. All rights reserved. */

/**
 * The default node presentation.  The first parameter should be
 * either a string that will be used as the node's label, or an object
 * that has a string propery called label.  By default, the clicking the
 * label will toggle the expanded/collapsed state of the node.  By
 * changing the href property of the instance, this behavior can be
 * changed so that the label will go to the specified href.
 *
 * @extends YAHOO_FAVS.widget.Node
 * @constructor
 * @param oData {object} a string or object containing the data that will
 * be used to render this node
 * @param oParent {YAHOO_FAVS.widget.Node} this node's parent node
 * @param expanded {boolean} the initial expanded/collapsed state
 */
YAHOO_FAVS.widget.PersonNode = function(oData, oParent, expanded) {
	if (oParent) { 
		this.init(oData, oParent, expanded);
		this.setUpLabel(oData);
		this.setStandalone(oData);
	}
};


YAHOO_FAVS.extend(YAHOO_FAVS.widget.PersonNode, YAHOO_FAVS.widget.Node, {


/**
 * The CSS class for the label href.  Defaults to ygtvlabel, but can be
 * overridden to provide a custom presentation for a specific node.
 *
 * @type string
 */
labelStyle : "ygpvlabel",

/**
 * The derived element id of the label for this node
 *
 * @type string
 */
labelElId : null,

/**
 * The text for the label.  It is assumed that the oData parameter will
 * either be a string that will be used as the label, or an object that
 * has a property called "label" that we will use.
 *
 * @type string
 */
label : null,

/**
 * describes if bookmarks are run as standalone or embaded.
 */
standalone : true,

/**
 * Sets up the node label
 * 
 * @param oData string containing the label, or an object with a label property
 */
setUpLabel : function(oData) { 
	if (typeof oData == "string") {
		oData = { label: oData };
	}
	this.label = oData.label;
	
	// update the link
	if (oData.href) {
		this.href = oData.href;
	}

	// set the target
	if (oData.target) {
		this.target = oData.target;
	}

	this.labelElId = "ygtvlabelel" + this.index;
},

setStandalone : function(oData) {
	if(oData.standalone)
	{
		this.standalone = oData.standalone;
	}
	else this.standalone = true;
},

/**
 * Returns the label element
 *
 * @return {object} the element
 */
getLabelEl : function() { 
	return document.getElementById(this.labelElId);
},

/**
 * Returns the css style name for the toggle
 *
 * @return {string} the css class for this node's toggle
 */
getStyle : function() {
    if (this.isLoading) {
        //this.logger.debug("returning the loading icon");
        return "ygtvloading";
    } else {
        // location top or bottom, middle nodes also get the top style
        var loc = (this.nextSibling) ? "t" : "l";

        // type p=plus(expand), m=minus(collapase), n=none(no children)
        var type = "n";
        if (this.hasChildren(true) || this.isDynamic()) {
            type = (this.expanded) ? "m" : "p";
        }

        //this.logger.debug("ygtv" + loc + type);
        return "ygpv" + loc + type;
    }
},

/**
 * Returns the hover style for the icon
 * @return {string} the css class hover state
 */
getHoverStyle : function() { 
    var s = this.getStyle();
    if (this.hasChildren(true) && !this.isLoading) { 
        s += "h"; 
    }
    return s;
},

// overrides YAHOO_FAVS.widget.Node
getNodeHtml : function() { 
	var sb = new Array();

	//it is set for false until rss will be rewritten
	var showRss = false;

	sb[sb.length] = '<table border="0" cellpadding="0" cellspacing="0">';
	sb[sb.length] = '<tr>';
	
	for (i=0;i<this.depth;++i) {
		// sb[sb.length] = '<td class="ygtvdepthcell">&nbsp;</td>';
		sb[sb.length] = '<td class="' + this.getDepthStyle(i) + '">&nbsp;</td>';
	}

	var getNode = 'YAHOO_FAVS.widget.TreeView.getNode(\'' +
					this.tree.id + '\',' + this.index + ')';

	sb[sb.length] = '<td';
	// sb[sb.length] = ' onselectstart="return false"';
	sb[sb.length] = ' id="' + this.getToggleElId() + '"';
	sb[sb.length] = ' class="' + this.getStyle() + '"';
	if (this.hasChildren(true)) {
		sb[sb.length] = ' onmouseover="this.className=';
		sb[sb.length] = getNode + '.getHoverStyle()"';
		sb[sb.length] = ' onmouseout="this.className=';
		sb[sb.length] = getNode + '.getStyle()"';
	}
	sb[sb.length] = ' onclick="javascript:' + this.getToggleLink() + '">&nbsp;';
	sb[sb.length] = '</td>';
	sb[sb.length] = '<td>';
	sb[sb.length] = '<a';
	sb[sb.length] = ' id="' + this.labelElId + '"';
	sb[sb.length] = ' class="' + this.labelStyle + '"';
	sb[sb.length] = ' href="' + this.href + '"';
	sb[sb.length] = ' target="' + this.target + '"';
	if (this.hasChildren(true)) {
		sb[sb.length] = ' onmouseover="document.getElementById(\'';
		sb[sb.length] = this.getToggleElId() + '\').className=';
		sb[sb.length] = getNode + '.getHoverStyle()"';
		sb[sb.length] = ' onmouseout="document.getElementById(\'';
		sb[sb.length] = this.getToggleElId() + '\').className=';
		sb[sb.length] = getNode + '.getStyle()"';
	}
	sb[sb.length] = ' >';
	sb[sb.length] = this.label;
	sb[sb.length] = '</a>';
	
	if(showRss) {
		sb[sb.length] = ' ';
		sb[sb.length] = '<div';
		sb[sb.length] = ' class="' + this.labelStyle + 'small"';
		sb[sb.length] = ' id="personrss'+this.tree.id+''+this.index+'"';
		sb[sb.length] = ' onclick="';
		sb[sb.length] = 'getFeed(\''+this.data.id+'\',0,\'personrss'+this.tree.id+''+this.index+'\')';
		sb[sb.length] = '" onmouseover="this.className=\'' + this.labelStyle + 'smallHover\'" onmouseout="this.className=\'' + this.labelStyle + 'small\'"';
		sb[sb.length] = ' style="display:inline;cursor:pointer">';
		sb[sb.length] = '<img src="/images/rss.png">';
		sb[sb.length] = '</div>';
	}
 
	/*sb[sb.length] = '<span style="font-weight:normal">|</span>';
	sb[sb.length] = '<div';
	sb[sb.length] = ' class="' + this.labelStyle + 'small"';
	sb[sb.length] = ' id="personatom'+this.tree.id+''+this.index+'"';
	sb[sb.length] = ' onclick="';
	sb[sb.length] = 'getFeed(\''+this.data.id+'\',1,\'personatom'+this.tree.id+''+this.index+'\')';
	sb[sb.length] = '" onmouseover="this.className=\'' + this.labelStyle + 'smallHover\'" onmouseout="this.className=\'' + this.labelStyle + 'small\'"';
	sb[sb.length] = ' style="display:inline;cursor:pointer">';
	sb[sb.length] = 'atom]';
	sb[sb.length] = '</div>';*/
	
	//if(this.standalone == true)
	//{
		sb[sb.length] = ' ';
		sb[sb.length] = '<div';
		sb[sb.length] = ' class="' + this.labelStyle + 'small"';
		sb[sb.length] = ' onclick="';
		sb[sb.length] = 'showCreateDir(\'' + this.tree.id + '\',' + this.index + ',\''+this.data.id+'\',event)';
		sb[sb.length] = '" onmouseover="this.className=\'' + this.labelStyle + 'smallHover\'" onmouseout="this.className=\'' + this.labelStyle + 'small\'"';
		sb[sb.length] = ' style="display:inline;cursor:pointer">';
		sb[sb.length] = '['+JS_MESSAGES["Bookmarks.command_new_dir"]+']';
		sb[sb.length] = '</div>';
		sb[sb.length] = '';
	//}
	
	sb[sb.length] = ' ';
	sb[sb.length] = '<div';
	sb[sb.length] = ' class="' + this.labelStyle + 'small"';
	sb[sb.length] = ' onclick="';
	sb[sb.length] = 'pasteNode(\'' + this.tree.id + '\',' + this.index +')" ';
	sb[sb.length] = '" onmouseover="this.className=\'' + this.labelStyle + 'smallHover\'" onmouseout="this.className=\'' + this.labelStyle + 'small\'"';
	sb[sb.length] = ' style="display:inline;cursor:pointer" ';
	sb[sb.length] = 'id="' + this.getToggleElId() +'Paste">';
	sb[sb.length] = '';
	sb[sb.length] = '</div>';
	sb[sb.length] = '';
	
	sb[sb.length] = '</td>';
	sb[sb.length] = '</tr>';
	sb[sb.length] = '</table>';

	return sb.join("");
}

});

