/* Copyright (c) 2006 Yahoo! Inc. All rights reserved. */

/**
 * The default node presentation.  The first parameter should be
 * either a string that will be used as the node's label, or an object
 * that has a string propery called label.  By default, the clicking the
 * label will toggle the expanded/collapsed state of the node.  By
 * changing the href property of the instance, this behavior can be
 * changed so that the label will go to the specified href.
 *
 * @extends YAHOO_FAVS.widget.Node
 * @constructor
 * @param oData {object} a string or object containing the data that will
 * be used to render this node
 * @param oParent {YAHOO_FAVS.widget.Node} this node's parent node
 * @param expanded {boolean} the initial expanded/collapsed state
 */
YAHOO_FAVS.widget.DirectoryNode = function(oData, oParent, expanded) {
	if (oParent) { 
		this.init(oData, oParent, expanded);
		this.setUpLabel(oData);
		this.setProperties(oData);
	}
};


YAHOO_FAVS.extend(YAHOO_FAVS.widget.DirectoryNode, YAHOO_FAVS.widget.Node, {

/**
 * The CSS class for the label href.  Defaults to ygtvlabel, but can be
 * overridden to provide a custom presentation for a specific node.
 *
 * @type string
 */
labelStyle : "ygdvlabel",

/**
 * The derived element id of the label for this node
 *
 * @type string
 */
labelElId : null,

/**
 * The text for the label.  It is assumed that the oData parameter will
 * either be a string that will be used as the label, or an object that
 * has a property called "label" that we will use.
 *
 * @type string
 */
label : null,

/**
 * describes if bookmarks are run as standalone or embaded.
 */
standalone : true,

ownerName : null,

ownerUid : null,
/**
 * Is viewer the creator and owner of the resource
 */
own : true,

removable : true,

pastable : true,

isLinkedIn : false,

isFriend : false,

isOpened : false,

selected: false,

dirId: null,
/**
 * Sets up the node label
 * 
 * @param oData string containing the label, or an object with a label property
 */
setUpLabel : function(oData) { 
	if (typeof oData == "string") {
		oData = { label: oData };
	}
	this.label = oData.label;
	
	// update the link
	if (oData.href) {
		this.href = oData.href;
	}

	// set the target
	if (oData.target) {
		this.target = oData.target;
	}

	this.labelElId = "ygtvlabelel" + this.index;
},

setProperties : function(oData) {
	//if(oData.standalone)
	//{
		if(oData.standalone == "true"||oData.standalone == true)
			this.standalone = true;
		else this.standalone = false;
	//} 
	
	if(oData.ownerName)
	{
		this.ownerName = oData.ownerName;
	}
	
	if(oData.ownerUid)
	{
		this.ownerUid = oData.ownerUid;
	}

	if(oData.dirId)
		this.dirId = oData.dirId;
	
	if(oData.own)
	{
		if(oData.own == "true"||oData.own == true)
			this.own = true;
		else this.own = false;
	} else this.own = false
	
	if(oData.removable)
	{
		if(oData.removable == "true"||oData.removable == true)
			this.removable = true;
		else this.removable = false;
	} else this.removable = false;
	
	if(oData.isLinkedIn)
	{
		if(oData.isLinkedIn == "true"||oData.isLinkedIn == true)
			this.isLinkedIn = true;
		else this.isLinkedIn = false;
	} else this.isLinkedIn = false;
	
	if(oData.isOpened)
	{
		if(oData.isOpened == "true"||oData.isOpened == true)
			this.isOpened = true;
		else this.isOpened = false;
	} else this.isOpened = false;
	
	if(oData.selected)
	{
		if(oData.selected == "true"||oData.selected == true)
			this.selected = true;
		else this.selected = false;
	} else this.selected = false;
	
	//if(oData.pastable)
	//{
	//	this.pastable = oData.pastable;
	//}
},

/**
 * Returns the label element
 *
 * @return {object} the element
 */
getLabelEl : function() { 
	return document.getElementById(this.labelElId);
},

/**
 * Returns the css style name for the toggle
 *
 * @return {string} the css class for this node's toggle
 */
getStyle : function() {
    if (this.isLoading) {
        //this.logger.debug("returning the loading icon");
        return "ygtvloading";
    } else {
        // location top or bottom, middle nodes also get the top style
        var loc = (this.nextSibling) ? "t" : "l";

        // type p=plus(expand), m=minus(collapase), n=none(no children)
        var type = "n";
        if (this.hasChildren(true) || this.isDynamic()) {
            type = (this.expanded) ? "m" : "p";
        }

        //this.logger.debug("ygtv" + loc + type);
        return "ygdv" + loc + type;
    }
},

/**
 * Returns the hover style for the icon
 * @return {string} the css class hover state
 */
getHoverStyle : function() { 
    var s = this.getStyle();
    if (this.hasChildren(true) && !this.isLoading) { 
        s += "h"; 
    }
    return s;
},

setName : function(newName) {
	try {
		name = document.getElementById(this.labelElId);
		name.innerHTML = newName;
	} catch(e) {}
	this.label = newName;
},

// overrides YAHOO_FAVS.widget.Node
getNodeHtml : function() { 
	var sb = new Array();

	var overstyle = "contover";
	var outstyle = "cont";

	sb[sb.length] = '<table border="0" cellpadding="0" cellspacing="0"';
	sb[sb.length] = ' onmouseover="document.getElementById(\''+this.labelElId+'options\').className=\'' + this.labelStyle + overstyle +'\'"';
	sb[sb.length] = ' onmouseout="document.getElementById(\''+this.labelElId+'options\').className=\'' + this.labelStyle + outstyle + '\'"';
	sb[sb.length] = '>';	
	sb[sb.length] = '<tr>';
	
	for (i=0;i<this.depth;++i) {
		// sb[sb.length] = '<td class="ygtvdepthcell">&nbsp;</td>';
		sb[sb.length] = '<td class="' + this.getDepthStyle(i) + '">&nbsp;</td>';
	}

	var getNode = 'YAHOO_FAVS.widget.TreeView.getNode(\'' +
					this.tree.id + '\',' + this.index + ')';

	sb[sb.length] = '<td';
	// sb[sb.length] = ' onselectstart="return false"';
	sb[sb.length] = ' id="' + this.getToggleElId() + '"';
	sb[sb.length] = ' class="' + this.getStyle() + '"';
	if (this.hasChildren(true)) {
		sb[sb.length] = ' onmouseover="this.className=';
		sb[sb.length] = getNode + '.getHoverStyle()"';
		sb[sb.length] = ' onmouseout="this.className=';
		sb[sb.length] = getNode + '.getStyle()"';
	}
	sb[sb.length] = ' onclick="javascript:' + this.getToggleLink() + '">&nbsp;';
	sb[sb.length] = '</td>';
	
	//input for non standalone version
	//!this.standalone
	if(this.own)
	{
		selectBoxClassName = "SSCFSelectedDirsDivStyle";
		if(!this.standalone) selectBoxClassName += "Visible";
		
		sb[sb.length] = '<td>';
		sb[sb.length] = '<div name="SSCFSelectedDirsDiv"> ';
		sb[sb.length] = '<input type="checkbox" class="'+selectBoxClassName+'" ';
		sb[sb.length] = 'name="SSCFSelectedDirs" ';
		sb[sb.length] = 'id="'+this.tree.id+'||'+this.index+'||'+this.data.id +'" ';
		sb[sb.length] = 'onChange="checkboxSelect(\''+this.data.id+'\',this.checked)" />';
		sb[sb.lenght] = '</div>';
		sb[sb.length] = '</td>';
	}


	sb[sb.length] = '<td';
	if(this.selected) 
	{
		sb[sb.length] = ' class="opened"';
		sb[sb.length] = ' onmouseover="this.className=\'openedh\'"';
		sb[sb.length] = ' onmouseout="this.className=\'opened\'"';
	}
	sb[sb.length] = '>';
	sb[sb.length] = '<a';
	sb[sb.length] = ' id="' + this.labelElId + '"';
	sb[sb.length] = ' class="' + this.labelStyle + '"';
	sb[sb.length] = ' href="' + this.href + '"';
	sb[sb.length] = ' target="' + this.target + '"';
	if (this.hasChildren(true)) {
		sb[sb.length] = ' onmouseover="document.getElementById(\'';
		sb[sb.length] = this.getToggleElId() + '\').className=';
		sb[sb.length] = getNode + '.getHoverStyle()"';
		sb[sb.length] = ' onmouseout="document.getElementById(\'';
		sb[sb.length] = this.getToggleElId() + '\').className=';
		sb[sb.length] = getNode + '.getStyle()"';
	}
	sb[sb.length] = ' >';
	sb[sb.length] = this.label;
	sb[sb.length] = '</a>';
	
	if(!this.own&&this.ownerName != null)
	{
		sb[sb.length] = ' ';
		sb[sb.length] = '<div';
		sb[sb.length] = ' class="' + this.labelStyle + 'sub"';
		sb[sb.length] = ' onclick="';
		sb[sb.length] = 'showAddFriend(\''+this.ownerUid+'\',\''+this.ownerName+'\',event)" ';
		sb[sb.length] = 'onmouseover="this.className=\'' + this.labelStyle + 'subHover\'" ';
		sb[sb.length] = 'onmouseout="this.className=\'' + this.labelStyle + 'sub\'"';
		sb[sb.length] = ' style="display:inline;cursor:pointer">';
		sb[sb.length] = '['+this.ownerName+']';
		sb[sb.length] = '</div>';
		sb[sb.length] = '';
	}
	
	//all the options closed in one div
	sb[sb.length] = '<div id="'+this.labelElId+'options" style="display:inline" class="'+this.labelStyle+'cont">';
	
	if(this.own)
	{
		
		sb[sb.length] = ' ';
		sb[sb.length] = '<div';
		sb[sb.length] = ' class="' + this.labelStyle + 'small"';
		sb[sb.length] = ' onclick="';
		sb[sb.length] = 'pasteNode(\'' + this.tree.id + '\',' + this.index +')" ';
		sb[sb.length] = '" onmouseover="this.className=\'' + this.labelStyle + 'smallHover\'" onmouseout="this.className=\'' + this.labelStyle + 'small\'"';
		sb[sb.length] = ' style="display:inline;cursor:pointer" ';
		sb[sb.length] = 'id="' + this.getToggleElId() +'Paste">';
		sb[sb.length] = '';
		sb[sb.length] = '</div>';
		sb[sb.length] = '';
	}
	
	sb[sb.length] = ' ';
	sb[sb.length] = '<div';
	sb[sb.length] = ' class="' + this.labelStyle + 'small"';
	//sb[sb.length] = ' href="';
	sb[sb.length] = ' onclick="';
	sb[sb.length] = 'showTooltip(event,\''+this.labelElId+'tooltip\')';
	sb[sb.length] = '" onmouseover="this.className=\'' + this.labelStyle + 'smallHover\'" onmouseout="this.className=\'' + this.labelStyle + 'small\'"';
	sb[sb.length] = ' style="display:inline;cursor:pointer;">';
	sb[sb.length] = '['+JS_MESSAGES["Bookmarks.command_actions"]+']';
	sb[sb.length] = '</div>';
	sb[sb.length] = '';
	
	sb[sb.length] = '</div>';
	
	sb[sb.length] = '<div id="'+this.labelElId+'tooltip" class="'+this.labelStyle+'tooltip">';
	
	//copy, cut, paste
	if(this.standalone)
	{
		sb[sb.length] = ' ';
		sb[sb.length] = '<div';
		sb[sb.length] = ' class="smalltt"';
		sb[sb.length] = ' onclick="';
		sb[sb.length] = 'copyNode(\'' + this.tree.id + '\',' + this.index + ',\'dir\',false)';
		sb[sb.length] = '" onmouseover="this.className=\'smallttHover\'" onmouseout="this.className=\'smalltt\'"';
		sb[sb.length] = ' style="cursor:pointer">';
		sb[sb.length] = ''+JS_MESSAGES["Bookmarks.command_copy"]+'';
		sb[sb.length] = '</div>';
		sb[sb.length] = '';
		if(this.own||this.isLinkedIn)
		{
			sb[sb.length] = ' ';
			sb[sb.length] = '<div';
			sb[sb.length] = ' class="smalltt"';
			sb[sb.length] = ' onclick="';
			sb[sb.length] = 'copyNode(\'' + this.tree.id + '\',' + this.index + ',\'dir\',true)';
			sb[sb.length] = '" onmouseover="this.className=\'smallttHover\'" onmouseout="this.className=\'smalltt\'"';
			sb[sb.length] = ' style="cursor:pointer">';
			sb[sb.length] = ''+JS_MESSAGES["Bookmarks.command_cut"]+'';
			sb[sb.length] = '</div>';
			sb[sb.length] = '';
		
			if(this.removable||this.isLinkedIn)
			{
				sb[sb.length] = ' ';
				sb[sb.length] = '<div';
				sb[sb.length] = ' class="smalltt"';
				sb[sb.length] = ' onclick="';
				sb[sb.length] = 'removeTreeNode(\'' + this.tree.id + '\',' + this.index + ')';
				sb[sb.length] = '" onmouseover="this.className=\'smallttHover\'" onmouseout="this.className=\'smalltt\'"';
				sb[sb.length] = ' style="cursor:pointer">';
				sb[sb.length] = ''+JS_MESSAGES["Bookmarks.command_remove"]+'';
				sb[sb.length] = '</div>';
				sb[sb.length] = '';
			}
		}	
		if(this.own)
		{
			/* TODO: enable/rewrite policies setting - temporarly switched off.
			sb[sb.length] = ' ';
			sb[sb.length] = '<div';
			sb[sb.length] = ' class="smalltt"';
			sb[sb.length] = ' onclick="';
			sb[sb.length] = 'showPolicyBox(\''+this.data.id+'\',event)';
			sb[sb.length] = '" onmouseover="this.className=\'smallttHover\'" onmouseout="this.className=\'smalltt\'"';
			sb[sb.length] = ' style="cursor:pointer">';
			sb[sb.length] = ''+JS_MESSAGES["Bookmarks.command_policy"]+'';
			sb[sb.length] = '</div>';
			sb[sb.length] = '';
			*/ 
			
			sb[sb.length] = ' ';
			sb[sb.length] = '<div';
			sb[sb.length] = ' class="smalltt"';
			sb[sb.length] = ' onclick="';//loadDirEdit
			sb[sb.length] = 'showCreateDir(\'' + this.tree.id + '\',' + this.index + ',\''+this.data.id+'\',event,true)';
			sb[sb.length] = '" onmouseover="this.className=\'smallttHover\'" onmouseout="this.className=\'smalltt\'"';
			sb[sb.length] = ' style="cursor:pointer">';
			sb[sb.length] = ''+JS_MESSAGES["Bookmarks.command_edit"]+'';
			sb[sb.length] = '</div>';
			sb[sb.length] = '';
		}
	}
	
	if(this.own)
	{
		sb[sb.length] = ' ';
		sb[sb.length] = '<div';
		sb[sb.length] = ' class="smalltt"';
		sb[sb.length] = ' onclick="';
		sb[sb.length] = 'showCreateDir(\'' + this.tree.id + '\',' + this.index + ',\''+this.data.id+'\',event)';
		sb[sb.length] = '" onmouseover="this.className=\'smallttHover\'" onmouseout="this.className=\'smalltt\'"';
		sb[sb.length] = ' style="cursor:pointer">';
		sb[sb.length] = ''+JS_MESSAGES["Bookmarks.command_new_dir"]+'';
		sb[sb.length] = '</div>';
		sb[sb.length] = '';
	}
	
	sb[sb.length] = ' ';
	sb[sb.length] = '<div';
	sb[sb.length] = ' class="smalltt" rel="facebox" href="#dirDescIn"';
	//sb[sb.length] = ' href="';
	sb[sb.length] = ' onclick="';
	sb[sb.length] = 'showDesc(\''+this.data.id+'\',null,event)';
	sb[sb.length] = '" onmouseover="this.className=\'smallttHover\'" onmouseout="this.className=\'smalltt\'"';
	sb[sb.length] = ' style="cursor:pointer">';
	sb[sb.length] = ''+JS_MESSAGES["Bookmarks.command_info"]+'';
	sb[sb.length] = '</div>';
	sb[sb.length] = '';
	
	sb[sb.length] = '</div>';
	
	sb[sb.length] = '</td>';
	sb[sb.length] = '</tr>';
	sb[sb.length] = '</table>';

	return sb.join("");
}

});