/* Copyright (c) 2006 Yahoo! Inc. All rights reserved. */

/**
 * The default node presentation.  The first parameter should be
 * either a string that will be used as the node's label, or an object
 * that has a string propery called label.  By default, the clicking the
 * label will toggle the expanded/collapsed state of the node.  By
 * changing the href property of the instance, this behavior can be
 * changed so that the label will go to the specified href.
 *
 * @extends YAHOO_FAVS.widget.Node
 * @constructor
 * @param oData {object} a string or object containing the data that will
 * be used to render this node
 * @param oParent {YAHOO_FAVS.widget.Node} this node's parent node
 * @param expanded {boolean} the initial expanded/collapsed state
 */
YAHOO_FAVS.widget.WebBookmarkNode = function(oData, oParent, expanded) {
	if (oParent) { 
		this.init(oData, oParent, expanded);
		this.setUpLabel(oData);
		this.setProperties(oData);
	}
};

YAHOO_FAVS.extend(YAHOO_FAVS.widget.WebBookmarkNode, YAHOO_FAVS.widget.Node, {

/**
 * The CSS class for the label href.  Defaults to ygtvlabel, but can be
 * overridden to provide a custom presentation for a specific node.
 *
 * @type string
 */
labelStyle : "ygbvlabel",

/**
 * The derived element id of the label for this node
 *
 * @type string
 */
labelElId : null,

/**
 * The text for the label.  It is assumed that the oData parameter will
 * either be a string that will be used as the label, or an object that
 * has a property called "label" that we will use.
 *
 * @type string
 */
label : null,

/**
 * describes if bookmarks are run as standalone or embaded.
 */
standalone : true,

/**
 * Is viewer the creator and owner of the resource
 */
own : true,

/**
 * how many hits this site has
 */
hits : 0,

/**
 * How many others saved this bookmark
 */
savedBy : 0,

selected: false,

/**
 * Gets the style for the saved by label
 */
getSavedByStyle : function(savedBy) {	

	if(savedBy<5) return "1";
	else if(savedBy<10) return "2";
	else if(savedBy<25) return "3";
	else if(savedBy<50) return "4";
	else if(savedBy<100) return "5";
	else if(savedBy<250) return "6";
	else if(savedBy<500) return "7";
	
	return "1";
},

getResType:function(){
	return "webBookmark";
},

/**
 * Sets up the node label
 * 
 * @param oData string containing the label, or an object with a label property
 */
setUpLabel : function(oData) { 
	if (typeof oData == "string") {
		oData = { label: oData };
	}
	this.label = oData.label;
	
	// update the link
	if (oData.href) {
		this.href = oData.href;
	}
	else
		this.href = oData.id;

	// set the target
	if (oData.target) {
		this.target = oData.target;
	}
	
	if(oData.hits)
		this.hits = oData.hits;
		
	if(oData.savedBy)
		this.savedBy = oData.savedBy;
	
	//else this.target = "_blank";

	this.labelElId = "ygtvlabelel" + this.index;
},

setProperties : function(oData) {
	if(oData.standalone)
	{
		if(oData.standalone == "true"||oData.standalone == true)
			this.standalone = true;
		else this.standalone = false;
	}
	else this.standalone = true;
	
	if(oData.own)
	{
		if(oData.own == "true"||oData.own == true)
			this.own = true;
		else this.own = false;
	}else this.own = false;
	
	if(oData.selected)
	{
		if(oData.selected == "true"||oData.selected == true)
			this.selected = true;
		else this.selected = false;
	}else this.selected = false;
},

/**
 * Returns the label element
 *
 * @return {object} the element
 */
getLabelEl : function() { 
	return document.getElementById(this.labelElId);
},

/**
 * Returns the css style name for the toggle
 *
 * @return {string} the css class for this node's toggle
 */
getStyle : function() {
    if (this.isLoading) {
        //this.logger.debug("returning the loading icon");
        return "ygtvloading";
    } else {
        // location top or bottom, middle nodes also get the top style
        var loc = (this.nextSibling) ? "t" : "l";

        // type p=plus(expand), m=minus(collapase), n=none(no children)
        var type = "n";
       // if (this.hasChildren(true) || this.isDynamic()) {
        //    type = (this.expanded) ? "m" : "p";
        //}

        //this.logger.debug("ygtv" + loc + type);
        return "ygwbv" + loc + type;
    }
},

/**
 * Returns the hover style for the icon
 * @return {string} the css class hover state
 */
getHoverStyle : function() { 
    var s = this.getStyle();
    if (this.hasChildren(true) && !this.isLoading) { 
        s += "h"; 
    }
    return s;
},

  setName : function(newName) {
  		try {
			name = document.getElementById(this.labelElId);
			name.innerHTML = newName;
  		} catch(e) {}
		this.label = newName;
	},

// overrides YAHOO_FAVS.widget.Node
getNodeHtml : function() { 
	var sb = new Array();

	sb[sb.length] = '<table border="0" cellpadding="0" cellspacing="0"';
	sb[sb.length] = ' onmouseover="document.getElementById(\''+this.labelElId+'options\').className=\'' + this.labelStyle + 'contover\'"';
	sb[sb.length] = ' onmouseout="document.getElementById(\''+this.labelElId+'options\').className=\'' + this.labelStyle + 'cont\'"';
	sb[sb.length] = '>';
	sb[sb.length] = '<tr>';
	
	for (i=0;i<this.depth;++i) {
		// sb[sb.length] = '<td class="ygtvdepthcell">&nbsp;</td>';
		sb[sb.length] = '<td class="' + this.getDepthStyle(i) + '">&nbsp;</td>';
	}

	var getNode = 'YAHOO_FAVS.widget.TreeView.getNode(\'' +
					this.tree.id + '\',' + this.index + ')';

	sb[sb.length] = '<td';
	// sb[sb.length] = ' onselectstart="return false"';
	sb[sb.length] = ' id="' + this.getToggleElId() + '"';
	sb[sb.length] = ' class="' + this.getStyle() + '"';
	if (this.hasChildren(true)) {
		sb[sb.length] = ' onmouseover="this.className=';
		sb[sb.length] = getNode + '.getHoverStyle()"';
		sb[sb.length] = ' onmouseout="this.className=';
		sb[sb.length] = getNode + '.getStyle()"';
	}
	sb[sb.length] = ' onclick="javascript:' + this.getToggleLink() + '">&nbsp;';
	sb[sb.length] = '</td>';
	
	sb[sb.length] = '<td';
	if(this.selected) 
	{
		sb[sb.length] = ' class="opened"';
		sb[sb.length] = ' onmouseover="this.className=\'openedh\'"';
		sb[sb.length] = ' onmouseout="this.className=\'opened\'"';
	}
	sb[sb.length] = '>';
	sb[sb.length] = '<a';
	sb[sb.length] = ' id="' + this.labelElId + '"';
	sb[sb.length] = ' class="' + this.labelStyle + '"';
	sb[sb.length] = ' href="/go/'+encodeURIforSscf(this.data.id)+'&type=web"';
	sb[sb.length] = ' title="'+this.href+'"';
	sb[sb.length] = ' target="' + this.target + '"';
	if (this.hasChildren(true)) {
		sb[sb.length] = ' onmouseover="document.getElementById(\'';
		sb[sb.length] = this.getToggleElId() + '\').className=';
		sb[sb.length] = getNode + '.getHoverStyle()"';
		sb[sb.length] = ' onmouseout="document.getElementById(\'';
		sb[sb.length] = this.getToggleElId() + '\').className=';
		sb[sb.length] = getNode + '.getStyle()"';
	}
	sb[sb.length] = ' >';
	sb[sb.length] = this.label;
	sb[sb.length] = '</a>';
	
	//all the options closed in one div
	sb[sb.length] = '<span id="'+this.labelElId+'options" class="'+this.labelStyle+'cont">';
	
	//hits, savedby number
	if(this.hits>0||this.savedBy>0) sb[sb.length] = ' ';
	if(this.hits>0)
		sb[sb.length] = '<span class="' + this.labelStyle + 'small">['+this.hits+']</span>';
	if(this.savedBy>0) {
		sb[sb.length] = '<span class="' + this.labelStyle + 'small savedBySpan'+this.getSavedByStyle(this.savedBy)+'"';
		sb[sb.length] = ' style="cursor:pointer" ';
		sb[sb.length] = ' onclick="showDesc(\''+this.data.id+'\',\'webBookmark\',event)" >';
		sb[sb.length] = '['+JS_MESSAGES['Bookmarks.dir_savedby']+': <b>'+this.savedBy+'</b>]';
		sb[sb.length] = '</span>';	
	}

	sb[sb.length] = ' ';
	sb[sb.length] = '<div';
	sb[sb.length] = ' class="' + this.labelStyle + 'small"';
	sb[sb.length] = ' onclick="';
	sb[sb.length] = 'showTooltip(event,\''+this.labelElId+'tooltip\')';
	sb[sb.length] = '" onmouseover="this.className=\'' + this.labelStyle + 'smallHover\'" onmouseout="this.className=\'' + this.labelStyle + 'small\'"';
	sb[sb.length] = ' style="display:inline;cursor:pointer">';
	sb[sb.length] = '['+JS_MESSAGES["Bookmarks.command_actions"]+']';
	sb[sb.length] = '</div>';
	sb[sb.length] = '';

	sb[sb.length] = '</span>';

	sb[sb.length] = '<div id="'+this.labelElId+'tooltip" class="'+this.labelStyle+'tooltip">';

	//copy, cut, paste
	if(this.standalone)
	{
		sb[sb.length] = ' ';
		sb[sb.length] = '<div';
		sb[sb.length] = ' class="smalltt"';
		sb[sb.length] = ' onclick="';
		sb[sb.length] = 'copyNode(\'' + this.tree.id + '\',' + this.index + ',\'web\',false)';
		sb[sb.length] = '" onmouseover="this.className=\'smallttHover\'" onmouseout="this.className=\'smalltt\'"';
		sb[sb.length] = ' style="cursor:pointer">';
		sb[sb.length] = JS_MESSAGES["Bookmarks.command_copy"];
		sb[sb.length] = '</div>';
		sb[sb.length] = '';
		
		if(this.own) //why?
		{			
			sb[sb.length] = ' ';
			sb[sb.length] = '<div';
			sb[sb.length] = ' class="smalltt"';
			sb[sb.length] = ' onclick="';
			sb[sb.length] = 'copyNode(\'' + this.tree.id + '\',' + this.index + ',\'web\',true)';
			sb[sb.length] = '" onmouseover="this.className=\'smallttHover\'" onmouseout="this.className=\'smalltt\'"';
			sb[sb.length] = ' style="cursor:pointer">';
			sb[sb.length] = JS_MESSAGES["Bookmarks.command_cut"];
			sb[sb.length] = '</div>';
			sb[sb.length] = '';
			
			sb[sb.length] = ' ';
			sb[sb.length] = '<div';
			sb[sb.length] = ' class="smalltt"';
			sb[sb.length] = ' onclick="';
			sb[sb.length] = 'fillBkmrkEditFormRequest(\'' +this.data.id+ '\',\'' + this.tree.id + '\',' + this.index + ')';
			sb[sb.length] = '" onmouseover="this.className=\'smallttHover\'" onmouseout="this.className=\'smalltt\'"';
			sb[sb.length] = ' style="cursor:pointer">';
			sb[sb.length] = JS_MESSAGES["Bookmarks.command_edit"];
			sb[sb.length] = '</div>';
			sb[sb.length] = '';
			
			sb[sb.length] = ' ';
			sb[sb.length] = '<div';
			sb[sb.length] = ' class="smalltt"';
			sb[sb.length] = ' onclick="';
			sb[sb.length] = 'removeTreeNode(\'' + this.tree.id + '\',' + this.index + ')';
			sb[sb.length] = '" onmouseover="this.className=\'smallttHover\'" onmouseout="this.className=\'smalltt\'"';
			sb[sb.length] = ' style="cursor:pointer">';
			sb[sb.length] = JS_MESSAGES["Bookmarks.command_remove"];
			sb[sb.length] = '</div>';
			sb[sb.length] = '';
		}
	}
	
	sb[sb.length] = ' ';
	sb[sb.length] = '<div';
	sb[sb.length] = ' class="smalltt"';
	sb[sb.length] = ' onclick="';
	sb[sb.length] = 'showDesc(\''+this.data.id+'\',\'webBookmark\',event)';
	sb[sb.length] = '" onmouseover="this.className=\'smallttHover\'" onmouseout="this.className=\'smalltt\'"';
	sb[sb.length] = ' style="cursor:pointer">';
	sb[sb.length] = JS_MESSAGES["Bookmarks.command_info"];
	sb[sb.length] = '</div>';
	sb[sb.length] = '';
	
	sb[sb.length] = '</DIV>';	
	
	sb[sb.length] = '</td>';
	sb[sb.length] = '</tr>';
	sb[sb.length] = '</table>';

	return sb.join("");
}

});

