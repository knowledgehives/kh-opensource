/* ----------------- onload support -------------------------- */

/**
 * Functions that will be called on load
 */
var aonloadFunctions = $A({});

function registerOnLoad(func)
{
	aonloadFunctions.push(func);
}

function invokeOnLoad() 
{
	aonloadFunctions.each(function(func)
	{
		if (func != null && typeof func == "function")
		{
			try 
			{
				func();
			}
			catch (e)
			{
				console.error("Problem when invoking "+func+": "+e.message+" / "+e.description);
			}
		}
	})
		
}

registerOnLoad(window.onload);
window.onload = invokeOnLoad;
