/*
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.kh.vulcan.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import com.kh.vulcan.exception.MergeException;

/**
 * This class provides methods for reading and writing files.
 * 
 * @author Mariusz Cygan
 */
public class MergeUtils {

	/**
	 * Reads Document from file.
	 * 
	 * @param file
	 * @return
	 * @throws MergeException
	 */
	public static Document readDocument(File file) throws MergeException {
		SAXBuilder saxBuilder = new SAXBuilder();
		saxBuilder.setIgnoringBoundaryWhitespace(true);
		Document document = null;
		try {
			document = saxBuilder.build(file);
		} catch (JDOMException e) {
			throw new MergeException(e);
		} catch (IOException e) {
			throw new MergeException(e);
		}

		return document;
	}

	/**
	 * Reads string from file.
	 * 
	 * @param file
	 * @return
	 * @throws MergeException
	 */
	public static String readString(File file) throws MergeException {
		String string = null;

		Reader reader = null;
		try {
			reader = new InputStreamReader(new FileInputStream(file), "UTF-8");
			StringBuilder stringBuilder = new StringBuilder();
			char[] buffor = new char[1024];
			int length = reader.read(buffor);
			while (length > -1) {
				stringBuilder.append(buffor, 0, length);
				length = reader.read(buffor);
			}
			string = stringBuilder.toString();
		} catch (IOException e) {
			throw new MergeException(e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					throw new MergeException(e);
				}
			}
		}

		return string;
	}

	public static StringBuilder readStringBuilder(File file) throws MergeException {
		return new StringBuilder(readString(file));
	}

	/**
	 * Writes XML Document to file.
	 * 
	 * @param document
	 * @param file
	 * @throws MergeException
	 */
	public static void write(Document document, File file) throws MergeException {
		long time = file.lastModified();

		OutputStream outputStream = null;
		try {
			outputStream = new BufferedOutputStream(new FileOutputStream(file));

			XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
			xmlOutputter.output(document, outputStream);
		} catch (FileNotFoundException e) {
			throw new MergeException(e);
		} catch (IOException e) {
			throw new MergeException(e);
		} finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {
					throw new MergeException(e);
				}
			}
		}

		file.setLastModified(time);
	}

	/**
	 * Writes string to file.
	 * 
	 * @param string
	 * @param file
	 * @throws MergeException
	 */
	public static void write(String string, File file) throws MergeException {
		long time = file.lastModified();

		Writer writer = null;
		try {
			writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
			writer.write(string);
		} catch (IOException e) {
			throw new MergeException(e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					throw new MergeException(e);
				}
			}
		}

		file.setLastModified(time);
	}

	public static void write(StringBuilder stringBuilder, File file) throws MergeException {
		write(stringBuilder.toString(), file);
	}
}
