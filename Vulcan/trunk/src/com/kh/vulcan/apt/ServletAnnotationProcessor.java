/*
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.kh.vulcan.apt;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.filter.ElementFilter;

import com.kh.vulcan.annotation.Servlet;
import com.kh.vulcan.exception.MergeException;
import com.kh.vulcan.util.MergeUtils;
import com.sun.mirror.apt.AnnotationProcessor;
import com.sun.mirror.apt.AnnotationProcessorEnvironment;
import com.sun.mirror.declaration.ClassDeclaration;
import com.sun.mirror.declaration.TypeDeclaration;
import com.sun.mirror.util.DeclarationVisitors;
import com.sun.mirror.util.SimpleDeclarationVisitor;

public class ServletAnnotationProcessor implements AnnotationProcessor {

	private class ServletClassVisitor extends SimpleDeclarationVisitor {

		@Override
		public void visitClassDeclaration(ClassDeclaration declaration) {
			if (declaration.getAnnotation(Servlet.class) == null) {
				return;
			}
			
			env_.getMessager().printNotice("Processing annotation for: " + declaration.getQualifiedName());

			Servlet servlet = declaration.getAnnotation(Servlet.class);
			String name = servlet.name().trim().length() > 0 ? servlet.name() : declaration.getSimpleName();

			try {
				Document document = MergeUtils.readDocument(file_);
				Element root = document.getRootElement();

				// removing old elements with the same name
				List<Element> elements = new ArrayList<Element>();
				for (Object object : root.getContent(new ElementFilter())) {
					Element element = (Element) object;
					if (element.getChild("servlet-name", root.getNamespace()) != null) {
						if (name.equals(element.getChild("servlet-name", root.getNamespace()).getText())) {
							elements.add(element);
						}
					}
				}
				for (Element element : elements) {
					element.detach();
				}

				Element servletElement = new Element("servlet", root.getNamespace());

				Element element = new Element("servlet-name", root.getNamespace());
				element.setText(name);
				servletElement.addContent(element);

				if (servlet.description().length() > 0) {
					element = new Element("description", root.getNamespace());
					element.setText(servlet.description());
					servletElement.addContent(element);
				}

				element = new Element("servlet-class", root.getNamespace());
				element.setText(declaration.getQualifiedName());
				servletElement.addContent(element);

				if (servlet.loadOnStartup() >= 0) {
					element = new Element("load-on-startup", root.getNamespace());
					element.setText(String.valueOf(servlet.loadOnStartup()));
					servletElement.addContent(element);
				}

				root.addContent(servletElement);

				if (servlet.urlMappings().length > 0) {
					for (String urlMapping : servlet.urlMappings()) {
						Element servletMappingElement = new Element("servlet-mapping", root.getNamespace());

						element = new Element("servlet-name", root.getNamespace());
						element.setText(name);
						servletMappingElement.addContent(element);

						element = new Element("url-pattern", root.getNamespace());
						element.setText(urlMapping.trim());
						servletMappingElement.addContent(element);

						root.addContent(servletMappingElement);
					}
				}

				MergeUtils.write(document, file_);
			} catch (MergeException e) {
				env_.getMessager().printError(e.getMessage());
			}
		}
	}

	private AnnotationProcessorEnvironment env_;

	private File file_;

	public ServletAnnotationProcessor(AnnotationProcessorEnvironment env) {
		this.env_ = env;
	}

	public void process() {
		for (String key : env_.getOptions().keySet()) {
			if (key.startsWith("-Afile=")) {
				file_ = new File(key.substring("-Afile=".length()));
				break;
			}
		}
		env_.getMessager().printNotice("Processing annotations to: " + file_);
		if (file_ == null || !file_.exists()) {
			env_.getMessager().printError("File web.xml is not defined or does not exist!");
			return;
		}

		for (TypeDeclaration declaration : env_.getSpecifiedTypeDeclarations()) {
			if (declaration.getAnnotation(Servlet.class) != null) {
				declaration.accept(DeclarationVisitors.getDeclarationScanner(new ServletClassVisitor(), DeclarationVisitors.NO_OP));
			}
		}
	}
}
