/*
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.kh.vulcan.ant;

import java.io.File;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import com.kh.vulcan.exception.MergeException;
import com.kh.vulcan.util.MergeUtils;

/**
 * This is an Ant Task for merging menu files.
 * 
 * @author Mariusz Cygan
 */
public class FileMergeTask extends Task {

	private static final String CLOSING_COMMENT = "<!-- } -->";

	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

	private static String OPENING_COMMENT_1 = "<!-- %s(";

	private static final String OPENING_COMMENT_2 = ") { -->";

	private static String TOKEN_COMMENT = "<!-- %s -->";

	private File file_;

	private String name_;

	private String token_;

	private File withFile_;

	private void addString(StringBuilder builder, String string) throws MergeException {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(OPENING_COMMENT_1).append(withFile_.lastModified()).append(OPENING_COMMENT_2).append(LINE_SEPARATOR);
		stringBuilder.append(string).append(LINE_SEPARATOR);
		stringBuilder.append(CLOSING_COMMENT).append(LINE_SEPARATOR);

		int index = builder.indexOf(String.format(TOKEN_COMMENT, token_));
		if (index >= 0) {
			builder.insert(index, stringBuilder);
		} else {
			throw new MergeException("token not found");
		}
	}

	@Override
	public void execute() throws BuildException {
		if (name_ != null) {
			OPENING_COMMENT_1 = String.format(OPENING_COMMENT_1, name_);
		} else {
			throw new BuildException("Property not set: name");
		}
		if (token_ != null) {
			TOKEN_COMMENT = String.format(TOKEN_COMMENT, token_);
		} else {
			throw new BuildException("Property not set: token");
		}

		if (withFile_.exists()) {
			try {
				merge();
			} catch (MergeException e) {
				throw new BuildException(e);
			}
		}
	}

	private Long getTime(StringBuilder stringBuilder) {
		int i = stringBuilder.indexOf(OPENING_COMMENT_1) + OPENING_COMMENT_1.length();
		int j = stringBuilder.indexOf(OPENING_COMMENT_2, i);

		Long time = null;
		if (i >= 0 && j >= i) {
			String string = stringBuilder.substring(i, j);
			time = Long.parseLong(string);
		}

		return time;
	}

	public void merge() throws MergeException {
		StringBuilder stringBuilder = MergeUtils.readStringBuilder(file_);
		Long time = getTime(stringBuilder);
		if (time == null || time < withFile_.lastModified()) {
			log("Merging " + file_.getAbsolutePath() + " with " + withFile_.getAbsolutePath());

			String string = MergeUtils.readString(withFile_);

			removeString(stringBuilder);
			if (string.trim().length() > 0) {
				addString(stringBuilder, string);
			}

			MergeUtils.write(stringBuilder.toString(), file_);
		}
	}

	private void removeString(StringBuilder builder) {
		int i = builder.indexOf(OPENING_COMMENT_1);
		int j = builder.indexOf(CLOSING_COMMENT, i) + CLOSING_COMMENT.length() + LINE_SEPARATOR.length();
		if (i >= 0 && j >= i) {
			builder.delete(i, j);
		}
	}

	public void setFile(File file) {
		this.file_ = file;
	}

	public void setName(String name) {
		this.name_ = name;
	}

	public void setToken(String token) {
		this.token_ = token;
	}

	public void setWithFile(File withFile) {
		this.withFile_ = withFile;
	}
}
