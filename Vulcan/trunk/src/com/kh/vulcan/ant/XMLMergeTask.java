/*
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.kh.vulcan.ant;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.jdom.Comment;
import org.jdom.Content;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.filter.ContentFilter;
import org.jdom.filter.ElementFilter;

import com.kh.vulcan.exception.MergeException;
import com.kh.vulcan.util.MergeUtils;

/**
 * This is an Ant Task for merging two web.xml files.
 * 
 * @author Mariusz Cygan
 */
public class XMLMergeTask extends Task {

	private static String COMMENT = " %s %ss (%d) ";

	private static String COMMENT_PATTERN = ".*\\([0-9]+\\) ";

	private File file_;

	private String name_;

	private File withFile_;

	@SuppressWarnings("unchecked")
	private void addElements(Element rootElement, String elementsName, List<Element> elements) {
		for (Element element : elements) {
			element.detach();
		}

		List contents = rootElement.getContent(new ElementFilter(elementsName));
		if (!contents.isEmpty()) {
			Element lastElement = (Element) contents.get(contents.size() - 1);
			int index = rootElement.indexOf(lastElement) + 1;

			rootElement.addContent(index, new Comment(String.format(COMMENT, name_, elementsName, withFile_.lastModified())));
			rootElement.addContent(index + 1, elements);
		} else {
			rootElement.addContent(new Comment(String.format(COMMENT, name_, elementsName, withFile_.lastModified())));
			rootElement.addContent(elements);
		}
	}

	@Override
	public void execute() throws BuildException {
		if (name_ == null) {
			throw new BuildException("Property not set! name");
		}

		if (withFile_.exists()) {
			try {
				merge();
			} catch (MergeException e) {
				throw new BuildException(e);
			}
		}
	}

	private Element getElement(Element rootElement, String elementName) {
		List<Element> elements = getElements(rootElement, elementName);

		Element element = null;
		if (elements.size() > 0) {
			element = elements.iterator().next();
		}

		return element;
	}

	private List<Element> getElements(Element rootElement, String elementsName) {
		List<Element> elements = new ArrayList<Element>();
		if (rootElement != null) {
			for (Object object : rootElement.getContent(new ElementFilter(elementsName))) {
				elements.add((Element) object);
			}
		}

		return elements;
	}

	private Long getTime(Document documentBuilder) {
		Long time = null;
		for (Object object : documentBuilder.getRootElement().getContent(new ContentFilter(ContentFilter.COMMENT))) {
			Comment comment = (Comment) object;
			if (comment.getText() != null && comment.getText().matches(COMMENT_PATTERN)) {
				int i = comment.getText().indexOf('(') + 1;
				int j = comment.getText().indexOf(')');

				if (i >= 0 && j > i) {
					String string = comment.getText().substring(i, j);
					time = Long.parseLong(string);
				}
			}
		}

		return time;
	}

	public void merge() throws MergeException {
		Document document = MergeUtils.readDocument(file_);
		Long time = getTime(document);
		if (time == null || time < withFile_.lastModified()) {
			log("Merging " + file_.getAbsolutePath() + " with " + withFile_.getAbsolutePath());

			Document withDocument = MergeUtils.readDocument(withFile_);

			removeElements(document.getRootElement(), "filter");
			List<Element> elements = getElements(withDocument.getRootElement(), "filter");
			if (!elements.isEmpty()) {
				addElements(document.getRootElement(), "filter", elements);
			}
			
			removeElements(document.getRootElement(), "filter-mapping");
			elements = getElements(withDocument.getRootElement(), "filter-mapping");
			if (!elements.isEmpty()) {
				addElements(document.getRootElement(), "filter-mapping", elements);
			}
			
			removeElements(document.getRootElement(), "listener");
			elements = getElements(withDocument.getRootElement(), "listener");
			if (!elements.isEmpty()) {
				addElements(document.getRootElement(), "listener", elements);
			}

			removeElements(document.getRootElement(), "servlet");
			elements = getElements(withDocument.getRootElement(), "servlet");
			if (!elements.isEmpty()) {
				addElements(document.getRootElement(), "servlet", elements);
			}

			removeElements(document.getRootElement(), "servlet-mapping");
			elements = getElements(withDocument.getRootElement(), "servlet-mapping");
			if (!elements.isEmpty()) {
				addElements(document.getRootElement(), "servlet-mapping", elements);
			}

			if (getElement(document.getRootElement(), "jsp-config") != null) {
				removeElements(getElement(document.getRootElement(), "jsp-config"), "taglib");
				elements = getElements(getElement(withDocument.getRootElement(), "jsp-config"), "taglib");
				if (!elements.isEmpty()) {
					addElements(getElement(document.getRootElement(), "jsp-config"), "taglib", elements);
				}
			}

			removeElements(document.getRootElement(), "security-constraint");
			elements = getElements(withDocument.getRootElement(), "security-constraint");
			if (!elements.isEmpty()) {
				addElements(document.getRootElement(), "security-constraint", elements);
			}

			MergeUtils.write(document, file_);
		}
	}

	private void removeElements(Element rootElement, String elementsName) {
		List<Integer> indexes = new ArrayList<Integer>();
		boolean matches = false;
		for (Object object : rootElement.getContent()) {
			Content content = (Content) object;

			if (!matches && content instanceof Comment) {
				Comment comment = (Comment) object;
				if (comment.getText().contains(" " + name_ + " " + elementsName + "s ")) {
					matches = true;
				}
			}

			if (matches && object instanceof Element) {
				Element element = (Element) object;
				if (!elementsName.equals(element.getName())) {
					matches = false;
				}
			}

			if (matches) {
				indexes.add(rootElement.indexOf(content));
			}
		}

		Collections.reverse(indexes);
		for (int index : indexes) {
			rootElement.removeContent(index);
		}
	}

	public void setFile(File file) {
		this.file_ = file;
	}

	public void setName(String name) {
		this.name_ = name;
	}

	public void setWithFile(File withFile) {
		this.withFile_ = withFile;
	}
}
