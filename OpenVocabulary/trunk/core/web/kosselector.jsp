<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib prefix="ov" tagdir="/WEB-INF/tags"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Testing KOS Selector (OV Terms Filter)</title>
    <script src="/js/protoculous-effects-shrinkvars.js" type="text/javascript" charset="utf-8"></script>
    <script src="/js/tooltips.js" type="text/javascript" charset="utf-8"></script>
    <script src="/js/textboxlist.js" type="text/javascript" charset="utf-8"></script>
    <script src="/js/entryselector.js" type="text/javascript" charset="utf-8"></script>
    <script src="/js/kosselector.js" type="text/javascript" charset="utf-8"></script>
    
 	<link rel="stylesheet" href="/css/tooltip.css" type="text/css" charset="utf-8" />
 	<link rel="stylesheet" href="/css/kosselector.css" type="text/css" charset="utf-8" />
</head>
<body>
	<ov:kosselector id="ovtermsfilter" label="Simple input" hint="Start typing to choose a term" 
					preloaduris="http://www.openvocabulary.info/taxonomies/loc/Technology/Home_economics/The_house;http://www.openvocabulary.info/thesauri/ot/pl/instances/synset-dupa_wollowa-1;http://www.w3.org/2006/03/wn/wn20/instances/synset-house-noun-3"/>
	<a href="javascript:KOSSelector.instances.get('ovtermsfilter').saveNewConcepts();">Save</a>
</body>
</html>