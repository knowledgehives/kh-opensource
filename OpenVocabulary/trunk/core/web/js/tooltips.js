/*
Tooltips (c)2006 Matthias.Platzer@knallgrau.at, MIT-sytle LICENSE
depends on script.acolo.us (http://script.aculo.us).
v0.3 23.01.2006
*/

window.tooltips = [];
TooltipFactory = Class.create();
TooltipFactory.prototype = {
	initialize: function(selector, options) {
		this.selector = selector;
		this.elements = [];
		this.tooltips = [];
		this.options = options||{};
	},

	areSupported: function() {
		return (document.getElementById!=null);
	},

	activate: function(elements, options) {
		this.elements = document.getElementsBySelector(this.selector);
		Object.extend(this.options, options||{});
		for (var i=0; i<this.elements.length; i++){
			var tooltip = new Tooltip(this.elements[i], this);
			if (tooltip) {
				this.tooltips.push(tooltip);
			}
		}
	},

	activateOnLoad: function(options) {
		if (this.areSupported()) {
			Object.extend(this.options, options||{});
			Event.observe(window, "load", this.activate.bind(this));
		}
	}
	
}

Tooltip = Class.create();
Tooltip.prototype = {
	/**
	 * Should be set to <code>true</code> if this.popUp hasClassName "lazyload" 
	 */
	lazyLoad: false,	
		
	initialize: function(trigger, options, _lazyload) {
		this.trigger = trigger;
		if (options instanceof TooltipFactory) {
			this.factory = options;
			options = options.options;
		}
		this.setOptions(options);
		if (this.options.getPopUp) {
			this.popUp = this.options.getPopUp.bind(this)(event);
		} else if (!this.popUp && this.options.popUp) {
			this.popUp = $(this.options.popUp);
		} else if (this.trigger.href && this.trigger.href.indexOf("#") != -1) {
			this.popUp = $(this.trigger.href.split("#").pop());
		}
		if (!this.popUp && this.trigger.id) this.popUp = $(this.trigger.id + "PopUp");
		if (!this.popUp && this.trigger.title) {
			this.popUp = document.createElement("div");
			document.body.appendChild(this.popUp);
			this.popUp.className = this.options.popUpClassName;
			this.popUp.innerHTML = this.trigger.title;
			this.trigger.removeAttribute("title");
		}
		if (!this.popUp) return;
		
		if ( !Object.isUndefined(_lazyload) )
		{
			this.lazyLoad = _lazyload;
		}
		
		this.addCloseButton();

		window.tooltips.push(this);
		this.runningEffect = null;
		if (this.options.openingEvent==this.options.closingEvent) {
			Event.observe(this.trigger, this.options.openingEvent||'click', this.toggle.bind(this));
		} else {
			Event.observe(this.trigger, this.options.openingEvent||'mouseover', this.open.bind(this));
			Event.observe(this.trigger, this.options.closingEvent||'mouseout', this.close.bind(this));
		}
		if (this.options.onInitialize) this.options.onInitialize.bind(this)();
	},

	/**
	 * This method adds button for closing the tooltip
	 * @author sebastiankruk
	 */
	addCloseButton : function(){
		
		//<img class="closebut" onclick="$('tutorialEntry${ tutorial.id }PopUp').style.display='none';void(0);" src="/images/closebox.png" width="16" />
		var img = document.createElement("img");
		Element.addClassName(img,"closebut");
		img.onmouseup = this.close.bindAsEventListener(this);
		img.src = "/images/closebox.png";
		img.width = 16;
		
		this.popUp.appendChild(img);
	},

	setOptions: function(options) {
		this.options = {
			queue: { 
				position: "end", 
				scope: "tooltip"+window.tooltips.length, 
				limit: 1
			}
		};
		Object.extend(this.options, Tooltips.options);
		Object.extend(this.options, options || {});
		if (this.setInitialPosition) this.setInitialPosition = this.options.setInitialPosition.bind(this);
		if (this.getPosition) this.getPosition = this.options.getPosition.bind(this);
		// opera can't handle opacity effects
		if (window.opera && this.options.effect.toLowerCase() == "appear") {
			this.options.duration = 0;
		}
	},

	open: function(event) 
	{
		var duration = this.options.duration;
		if (this.runningEffect) {
			var queue = Effect.Queues.get(this.options.queue.scope);
			duration = Math.min(new Date().getTime() - this.runningEffect.startOn, duration);
			queue.remove(this.runningEffect);
		}
		if (this.options.setPosition) this.options.setPosition.bind(this)(event);
		this.runningEffect = Effect[Effect.PAIRS[this.options.effect][0]](this.popUp, Object.extend(Object.extend(
			Object.extend({}, this.options), {duration: duration}), this.options.open||{}
		));

		if (this.options.onOpen) 
		{
			if (!Object.isUndefined(this.lazyLoad) && 
			    this.lazyLoad &&
			    !this.lazyLoad.isloaded && 
			    !this.lazyLoad.isloading)
			{
				var tooltipwait = new Image();
				tooltipwait.src = "/images/tooltipwait.gif";
				Element.addClassName(tooltipwait, "tooltipwait");
				
				this.popUp.insert({ "top": tooltipwait });

				this.lazyLoad.contentLoad(this.options.onOpen.bind(this, event));
			}
			else
			{
				this.options.onOpen.bind(this)(event);
			}
		}
	},

	close: function(event) {		
		var duration = this.options.duration;
		if (this.runningEffect) {
			var queue = Effect.Queues.get(this.options.queue.scope);
			duration = Math.min(new Date().getTime() - this.runningEffect.startOn, duration);
			queue.remove(this.runningEffect);
		}
		this.runningEffect = Effect[Effect.PAIRS[this.options.effect][1]](this.popUp, Object.extend(Object.extend(
			Object.extend({}, this.options), {duration: duration}), this.options.close||{}
		));
		if (this.options.onClose) this.options.onClose.bind(this)(event);
	},

	toggle: function(event) {
		Element.visible(this.popUp) ? this.close(event) : this.open(event);
	}
};

////// IMPLEMENTATIONS of TooltipFactory ///////
/// extend this as you need it

// element hover tooltips
Tooltips = new TooltipFactory(".tooltipTrigger", {
	popUpClassName: "tooltip",
	offsetLeft: 20,
	offsetTop: 10,
	effect: "appear",
	duration: 0.7,
	openingEvent: "mouseover",
	closingEvent: "mouseout",
	onInitialize: function() {
		this.popUp.style.position = "absolute";
		this.popUp.style.left = "0";
		this.popUp.style.top = "0";
		Element.hide(this.popUp);
	},
	onOpen: function(event) {
		var x = Event.pointerX(event) + this.options.offsetLeft;
		var triggerY = Position.cumulativeOffset(this.trigger)[1];
		var y = triggerY + this.trigger.offsetHeight + this.options.offsetTop;
		Position.prepare();
		var popUp = Element.getDimensions(this.popUp);
		if (x + popUp.width + this.options.offsetLeft > (Position.deltaX+document.viewport.getWidth()))//Position.visibleWidth)) 
			x = Math.max( this.options.offsetLeft, ((Position.deltaX+document.viewport.getWidth()/*Position.visibleWidth*/) - popUp.width - this.options.offsetLeft));
		if (y + popUp.height + this.options.offsetTop > (Position.deltaY+document.viewport.getHeight())) {
			y = triggerY - popUp.height - this.options.offsetTop;
		}
		this.popUp.style.left = x+"px";
		this.popUp.style.top = y+"px";
		
		console.debug("showing: "+this);
	}
});


// button/link info: appears after klicking something
ActionHints = new TooltipFactory(".actionHintTrigger", {
	popUpClassName: "tooltip",
	offsetLeft: 20,
	offsetTop: 40,
	effect: "appear",
	openingEvent: "click",
	closingEvent: "click",
	open: {
		duration: 0.5
	},
	close: {
		delay: 0.0,
		duration: 0.5
	},
	onInitialize: function() {
		this.popUp.style.position = "absolute";
		this.popUp.style.left = "0";
		this.popUp.style.top = "0";
		Element.hide(this.popUp);
	},
	onOpen: function(event) {
		var triggerPos = Position.cumulativeOffset(this.trigger);
		var x = triggerPos[0] + this.options.offsetLeft;
		var y = triggerPos[1] - this.options.offsetTop - this.trigger.offsetHeight;
		Position.prepare();
		var popUp = Element.getDimensions(this.popUp);
		if (x + popUp.width + this.options.offsetLeft > (Position.deltaX+Position.visibleWidth)) 
			x = Math.max( this.options.offsetLeft, ((Position.deltaX+Position.visibleWidth) - popUp.width - this.options.offsetLeft));
		this.popUp.style.left = x+"px";
		this.popUp.style.top = y+"px";
	}
});

// button/link info: appears after klicking something
StickyTooltips = new TooltipFactory(".actionHintTrigger", {
	popUpClassName: "tooltip",
	offsetLeft: 20,
	offsetTop: 20,
	effect: "appear",
	openingEvent: "mouseover",
	closingEvent: "click",
	open: {
		duration: 0.5
	},
	close: {
		delay: 0.0,
		duration: 0.5
	},
	onInitialize: function() {
		this.popUp.style.position = "absolute";
		this.popUp.style.left = "0";
		this.popUp.style.top = "0";
		Element.hide(this.popUp);
	},
	onOpen: function(event) {
		var triggerPos = Position.cumulativeOffset(this.trigger);
		var x = triggerPos[0] + this.options.offsetLeft;
		var y = triggerPos[1] - this.options.offsetTop - this.trigger.offsetHeight;
		Position.prepare();
		var popUp = Element.getDimensions(this.popUp);
		if (x + popUp.width + this.options.offsetLeft > (Position.deltaX+Position.visibleWidth)) 
			x = Math.max( this.options.offsetLeft, ((Position.deltaX+Position.visibleWidth) - popUp.width - this.options.offsetLeft));
		this.popUp.style.left = x+"px";
		this.popUp.style.top = y+"px";
	}
});

// trigger for showing/hidding a section in your page
SectionTriggers = new TooltipFactory(".sectionTrigger", {
	effect: "appear",
	openingEvent: "click",
	closingEvent: "click",
	text: {
		show: "show",
		hide: "hide"
	},
	duration: 1.0,
	onInitialize: function() {
		// Element.hide(this.popUp);
	},
	onOpen: function(event) {
		this.trigger.innerHTML = this.trigger.getAttribute("hide")||this.options.text.hide;
		Event.stop(event);
	},
	onClose: function(event) {
		this.trigger.innerHTML = this.trigger.getAttribute("show")||this.options.text.show;
		Event.stop(event);
	}
});

var TooltipLazyLoader = Class.create();
TooltipLazyLoader.prototype = {
	//--if the content is loaded
	isloaded: false,
	// -- if it is currently loading
	isloading : false,
	//--content loading handler - to be implemented by tooltip provider
	_onContentLoad: null, //make sure it is a function before calling it + that the implementation calls this.onContentLoaded when finished
	//--will be called when stuff is loaded (to be implemented by the tooltip)
	onContentLoaded: null, //make sure you will assign something to it before calling, or pass your callback method to contentLoad
	// -- div element where content should be put 
	tooltipContent: null,
	
	//--invoke to load the content
	contentLoad: function(_onContentLoaded)
	{
		this.isloading = true;
		
		if ( _onContentLoaded != null && !Object.isUndefined(_onContentLoaded) )
		{
			this.onContentLoaded = _onContentLoaded;
		}
		if ( this._onContentLoad != null && !Object.isUndefined(this._onContentLoad) )
		{
			this._onContentLoad();
		}
		else
			this.contentLoaded(); 
	},
	
	/**
	 * When content is loaded
	 */
	contentLoaded: function()
	{
		this.isloading = false;
		this.isloaded = true;
		
		if ( this.onContentLoaded != null && !Object.isUndefined(this.onContentLoaded) )
		{
			this.onContentLoaded();
		}
	},
	
	initialize: function(_content)
	{
		this.tooltipContent = _content;
	}
};
