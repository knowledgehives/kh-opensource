/*
  EntrySelector - based on Facebooklist from Proto!TextboxList + Autocomplete 0.1
  - Prototype version required: 6.0
  
  Credits:
  * Idea: Facebook
  * Guillermo Rauch: Original MooTools script
  * Sebastian Ryszard Kruk: complete update to support REST API 
*/

/* Copyright: InteRiders <http://interiders.com/> - Distributed under MIT - Keep this message! */

var EntrySelector = Class.create(TextboxList, { 
  
  /**
   * locking operations when processing ajax call
   */
  isprocessing : false,
  /**
   * will hold reference handler obtained from delay()
   */
  delayref : null,
  /**
   * Currently set value of the this.holder.className
   */
  holderClass : null,
  /**
   * Three services that are used to communicate with this selector
   */
  services : {
  	/**
  	 * Allows to retrieve JSON information about the entry (GET)
  	 */
  	lookup : null,
  	/**
  	 * Allows to find matching entries (GET)
  	 */
  	search : null,
  	/**
  	 * Allows to create new entry (PUT)
  	 */
  	create : null	
  },
  /**
   * Texts used for rendering communication with the user 
   */
  texts : {
  	addtag: "Add {1} as a tag (WARNING: no meaning attached)", //TODO-i18n
  	warningtag: "(WARNING: this is a tag, no meaning attached)", //TODO-i18n
  	subcategory: " subcategory of: ", //TODO-i18n
  	inhierarchy: " in hierarchy: ", //TODO-i18n
  	saveddescr: "Tag saved"//TODO-i18n
  },
  
  searchCache : $H({}),

  loptions: $H({    
    autocomplete: {
      'opacity': 0.8,
      'maxresults': 10,
      'minchars': 2
    }
  }),
  
  preload: function(preloaded){
	    $A(preloaded).each(function(entry) 
	    {
	    	if (Object.isString(entry)) // -- only URI provided
	    	{
	    		this.addEntryByURI(entry);
	    	}
	    	else // -- we assume - complete entry provided
	    	{
		    	this.addEntry(entry); 
	    	}
	    }, 
	    this);
  },
  
  /**
   * Activated when a user starts typing - 
   * it controls delay to make sure that the searching will not be invoked immediately
   */
  prepareToShow: function(input)
  {
	if ( this.dosearch && input.value.match(/^[\s*]*$/) === null ) 
	{
		if ( this.delayref !== null )
		{
			window.clearTimeout(this.delayref);
		}
		
		this.setHolderClassName("selector_waitinput");
		
		this.delayref = this.autoShowInit.bind(this, input.value).delay(1);
	}
	else
	{
		window.clearTimeout(this.delayref);
		this.delayref = null;
		this.setHolderClassName("selector_inactive");
	}
  },

  /**
   * Adds entry with given URI
   */
  addEntryByURI: function(entry)
  {
  	 if ( this.services.lookup != null )
  	 {
		 var req = new Ajax.Request(this.services.lookup+entry, 
		  {
				method: "get",
				onSuccess: function(resp) 
				{
					if ( resp.responseJSON === null )
					{
						resp.responseJSON = resp.responseText.evalJSON();
					}
					this.addEntry(resp.responseJSON); 
				}.bind(this),
				onFailure: function(transport, problem)
				{
					try 
					{ 
						console.debug(problem); 
					} 
					catch (e) {
						//nothing
					}
				}.bind(this)
			});
  	 }
  },
  
  /**
   * Start searching for given phrase
   */  
  autoShowInit: function(search) 
  {
	  this.delayref = null;  
	  
    this.autoholder.setStyle({'display': 'block'});
    this.autoholder.descendants().each(function(e) { e.setStyle({'display': 'none'}) });
    if (! search || ! search.strip() || (! search.length || search.length < this.loptions.get('autocomplete').minchars)) 
    {
      this.autoholder.select('.default').first().setStyle({'display': 'block'});
      this.resultsshown = false;
    } 
    else 
    if (! this.isprocessing){
		this.setHolderClassName("selector_active");
		
		if (this.searchCache[search] != null)
		{
			this.data = this.searchCache[search];
			this.autoShow(search);
		}
		else
		{
	    	this.isprocessing = true;
	    	this.data = [];
	    	
		  	 if ( this.services.search != null )
		  	 {
		    	new Ajax.Request(this.services.search.gsub(/\{1\}/, search), {
		    		method: "get",
		    		onSuccess: function(resp) 
		    		{
						if ( resp.responseJSON == null )
						{
							resp.responseJSON = resp.responseText.evalJSON();
						}
		    			resp.responseJSON.results.each(function(t){this.autoFeed(t)}.bind(this));
		    			
		    			this.searchCache[search] = this.data;
		    			
		    			this.autoShow(resp.responseJSON.label);
		    		}.bind(this),
		    		onComplete: function(transport) 
		    		{
		    			this.isprocessing = false;
		    			this.setHolderClassName("selector_inactive");
		    		}.bind(this),
		    		onFailure: function(transport, problem)
		    		{
		    			try 
		    			{ 
		    				console.debug(problem) 
		    			} 
		    			catch (e) {};
		    		}.bind(this)
		    	});
		  	 }
		  	 else // if could not run a query
		  	 {
    			this.isprocessing = false;
    			this.setHolderClassName("selector_inactive");
		  	 }
		}// - if not this.searchCache
    }
    return this;
  },
  
  /**
   * This method continues from autoShowInit() after the ajax results are returned
   */
  autoShow: function(search)
  {
      this.resultsshown = true;
      this.autoresults.setStyle({'display': 'block'}).update('');
      
      var areresults = false;
      
      this.data
//      .filter(function(str) 
//      { 
//    	  return str ? new RegExp(search,'i').test(str) : false; 
//      })
      .each(function(result, ti) 
      {
        if (ti < this.loptions.get('autocomplete').maxresults) 
        {
	        var el = this.createElement(result, search);
	        
	        if (ti == 0) 
	        {
	        	this.autoFocus(el);
	        	areresults = true;
	        }
        }
      }, 
      this);
      
      this.autoShowNewEntry(search, areresults);

      this.oldSearch = search;
  },
  
  /**
   * Creates complex element representing given data
   * ! reimplement in subclasses
   */
  createElement: function(result, search)
  {
      var that  = this;

      //--main elements with result
	  var el = new Element('li');
	  var eldiv = new Element("div");
	  Element.addClassName(eldiv,	"element");
	  el.appendChild(eldiv);

	  //--label of the result
      var label = this.autoHighlight(result.label, search);
	  var ellabel = new Element("div").update(label);
	  Element.addClassName(ellabel, "label");
	  eldiv.appendChild(ellabel);
  },
  
  
  /**
   * 
   */
  autoHighlight: function(html, highlight) 
  {
	if (html == null) {
		return "";
	}
    return html.gsub(new RegExp(highlight,'i'), function(match) 
    {
      return '<em>' + match[0] + '</em>';
    });
  },
  
  /**
   * 
   */
  autoHide: function() 
  {    
    this.resultsshown = false;
    this.autoholder.setStyle({'display': 'none'});    
    this.setHolderClassName("selector_inactive");
    return this;
  },
  
  autoFocus: function(el) 
  {
    if ( !el ) 
    {
    	return;
    }
    if ( this.autocurrent ) 
    {
    	this.autocurrent.removeClassName('auto-focus');
    }
    this.autocurrent = el.addClassName('auto-focus');
    
    return this;
  },
  
  autoMove: function(direction) 
  {    
    if ( !this.resultsshown ) 
    {
    	return;
    }
    this.autoFocus(this.autocurrent[(direction == 'up' ? 'previous' : 'next')]());
    return this;
  },
  
  autoFeed: function(text) 
  {
    if (this.data.indexOf(text) == -1)
        this.data.push(text);
    return this;
  },
  
  autoAdd: function(el) 
  {
    if (!el || ! el.retrieveData('result')) 
    {
    	return;
    }
    this.addEntry(el.retrieveData('result'));
    delete this.data[this.data.indexOf(el.retrieveData('result'))];
    this.autoHide();
    var input = this.lastinput || this.current.retrieveData('input');
    input.clear().focus();
    return this;
  },

  /**
   * adds more complex element (wraps the call to add(), extends @param entry with toString() function)
   */
  addEntry: function(entry)
  {
	if ( entry.label ) 
	{
		entry.toString = function(){
			return this.label;
		}
	}
	var el = this.add(entry.toString(), null, entry); 
	
	this.createTooltip(entry, el, ActionHints);
  },
  
  /**
   * adds parent with entry
   */
  addEntryParent: function(event, entry, tooltip)
  {
	  tooltip.parentNode.style.display = "none";
	  this.addEntry(entry);
  },
  
  /**
   * Creates a tooltip (dynamic?) and puts the reference to the element (@param el)
   * This method wrapps #_createTooltip and this one can be overwritten in subclasses
   */
  createTooltip: function(entry, el, tooltipType)
  {
  	return this._createTooltip(entry, el, tooltipType, entry, true);
  },  
  

  /**
   * Performs the actual actions for creating a tooltip (see #createTooltip())
   */  
  _createTooltip: function(entry, el, tooltipType, calluri, skipPath)
  {
	  //class="tooltipTrigger" id="tooltipPopularTagger${ tagger.UID }"
	  //<div class="info tooltip" id="tooltipPopularTagger${ tagger.UID }PopUp" style="display: none; ">
	  
	  //Element.addClassName(el, "tooltipTrigger");
	  
	  var divTooltip = new Element("div");
	  
	  Element.addClassName(divTooltip, "info");
	  Element.addClassName(divTooltip, "tooltip");
	  
	  divTooltip.id = el.identify() + "PopUp";
	  divTooltip.style.display = "none";
	  
	  document.getElementsByTagName("body")[0].appendChild(divTooltip);
	  
	  var lazyload = new TooltipLazyLoader(divTooltip);
	  lazyload._onContentLoad = this.lazyContentLoad.bind(this, calluri, lazyload, skipPath);
	  
	  var tooltip = new Tooltip(el, tooltipType, lazyload);
	  if (tooltip) 
	  {
		  tooltipType.tooltips.push(tooltip);
	  }
		
	  return divTooltip;
  },

  /**
   * Creates complex element describing previously fetched URI
   */
  createDescriptionElement: function(result)
  {
      var that  = this;

      //--main elements with result
	  var eldiv = new Element("div");
	  Element.addClassName(eldiv,	"element");

	  //--label of the result
	  var ellabel = new Element("div").update(result.label);
	  Element.addClassName(ellabel, "label");
	  eldiv.appendChild(ellabel);
  },  

  
  /**
   * Method calls /vocabularies/lookup to get info about given @param aboutUrl, and renders the results to @param divTooltip
   */
  lazyContentLoad: function(aboutUrl, lazyload, skipPath)
  {
  	if ( Object.isString(aboutUrl) )
  	{ // normal interaction - data from ajax
  		
	  	var uri = ( skipPath ) ? aboutUrl : (this.services.lookup+aboutUrl); //TODO XXX 
		  new Ajax.Request(uri, {
			method: "get",
			onSuccess: function(resp) 
			{
				if ( resp.responseJSON == null )
				{
					resp.responseJSON = resp.responseText.evalJSON();
				}
				
				var el = this.createDescriptionElement(resp.responseJSON);
				
				lazyload.tooltipContent.insert({ "top" : el});
				
			}.bind(this),
			onComplete: function(transport) 
			{
				var tooltiploader = lazyload.tooltipContent.childElements().find(function(el)
				{
					return el.hasClassName("tooltipwait");
				});
				lazyload.tooltipContent.removeChild(tooltiploader);
	
				// -- notify tooltip about finishing the process
				lazyload.onContentLoaded();
				
			}.bind(this),
			onFailure: function(transport, problem)
			{
				lazyload.tooltipContent.style.display= "none";
				try 
				{ 
					console.debug(problem) 
				} 
				catch (e) {};
			}.bind(this)
		});
  		
  	}
  	else
  	{ // skip the ajax part
		lazyload.tooltipContent.insert({ "top" : this.createDescriptionElement(aboutUrl)});
		var tooltiploader = lazyload.tooltipContent.childElements().find(function(el)
		{
			return el.hasClassName("tooltipwait");
		});
		lazyload.tooltipContent.removeChild(tooltiploader);

		// -- notify tooltip about finishing the process
		lazyload.onContentLoaded();
  	}
  },
  
  createInput: function($super,options) 
  {
    var li = $super(options);
    var input = li.retrieveData('input');
    input.observe('keydown', function(e) {
        this.dosearch = false;
        switch(e.keyCode) {
          case Event.KEY_UP: return this.autoMove('up');
          case Event.KEY_DOWN: return this.autoMove('down');        
          case Event.KEY_RETURN:
            if (!this.autocurrent) 
            {
            	break;
            }
            this.autoAdd(this.autocurrent);
            this.autocurrent = false;
            this.autoenter = true;
            break;
          case Event.KEY_ESC: 
            this.autoHide();
            if (this.current && this.current.retrieveData('input')) 
            {
            	this.current.retrieveData('input').clear();
            }
            break;
          default: this.dosearch = true;
        }
    }.bind(this));
    
    input.observe('keyup', this.prepareToShow.bind(this, input));
    
    input.observe(Prototype.Browser.IE ? 'keydown' : 'keypress', function(e) 
    { 
      if (this.autoenter)
      {
    	  Event.stop(e);
      }
      this.autoenter = false;
    }.bind(this));
    
    return li;
  },
  
  createBox: function($super,text, options) 
  {
    var li = $super(text, options);
    li.observe('mouseover',function() { 
        this.addClassName('bit-hover');
    });
    li.observe('mouseout',function() { 
        this.removeClassName('bit-hover') 
    });
    var a = new Element('a', {
      'href': '#',
      'class': 'closebutton'
    });
    a.observe('click',function(e) 
    {
          Event.stop(e);
          if ( !this.current )
          {
        	  this.focus(this.maininput);
          }
          this.dispose(li);
    }.bind(this));
    li.insert(a).cacheData('text', text);
    return li;
  },
  
  /**
   * Updates class name of the holder
   */
  setHolderClassName: function(className)
  {
	  
	  Element.removeClassName(this.holder, this.holderClass);
	  Element.addClassName(this.holder, className);
	  
	  this.holderClass = className;
  },
  
  /**
   * Copies required properties from the responseJSON to newtag. Please override in subclasses.
   */
  copyProperties: function(newtag, responseJSON)
  {
  	newtag.uri = responseJSON.uri;
  },
  /**
   * Checks if the given entry is new (and should be created)
   */
  isEntryNew: function(newtag)
  {
  	return newtag.uri == null;
  },
  
  /**
   * Communicates with the server to create a new tag
   */
  createNewEntry: function(newtag)
  {
  	if ( this.services.create != null )
  	{
  	 var hnewtag = $H(newtag);
  	 hnewtag.unset("description");
  	 
	 var req = new Ajax.Request(this.services.create+newtag.label, 
	  {
		method: "put",
		contentType: "application/json",
		postBody: hnewtag.toJSON(),
		onSuccess: function(resp) 
		{
			if ( resp.responseJSON === null )
			{
				resp.responseJSON = resp.responseText.evalJSON();
			}
			this.copyProperties(newtag, resp.responseJSON);
		}.bind(this),
		onFailure: function(transport, problem)
		{
			try 
			{ 
				console.debug(problem); 
			} 
			catch (e) {
				//nothing
			}
		}.bind(this)
	});
  	} 	
  },
  
  /**
   * Call this method to send PUT calls to REST API to create new concepts (e.g. new tags)
   */
  saveNewConcepts: function()
  {
  	this.bits.values().each(function(newtag)
  	{
  		if ( this.isEntryNew(newtag) )
  		{
	  		this.createNewEntry(newtag);
  		}	
  	}
  	.bind(this));
  },
  
  /**
   * Returns those entries that were selected by the user
   */
  getEntries: function()
  {
  	return this.bits.values();
  },

  /**
   * Initializes the component
   */
  initialize: function($super, element, autoholder, options, preloaded) 
  {
    $super(element, options);

    this.data = [];
	this.autoholder = $(autoholder).setOpacity(this.loptions.get('autocomplete').opacity);
	this.autoresults = this.autoholder.select('ul').first();
	
	if ( preloaded )
	{
		this.preload(preloaded);
	}
    
    this.setHolderClassName("selector_inactive");
	
	EntrySelector.instances.set(element, this);
  }
  

  
});

// -------------------- static content ----------- 

/**
 * Holds map of all instances of EntrySelector created in this context
 */
EntrySelector.instances = $H({});






// ----------- extensions to the javascript model --------------------

Element.addMethods({
    onBoxDispose: function(item,obj) 
    { 
		obj.autoFeed(item.retrieveData('text')); 
	},
    onInputFocus: function(el,obj) 
    {
		obj.autoShowInit(); 
	},    
    onInputBlur: function(el,obj) 
    { 
      obj.lastinput = el;
      obj.blurhide = obj.autoHide.bind(obj).delay(0.1);
    },
    filter: function(D,E)
    {
    	var C=[];
    	for ( var B=0, A=this.length; B<A ;B++ )
    	{
    		if ( D.call(E,this[B],B,this) )
    		{
    			C.push(this[B]);
    		}
    	}
    	return C;
    }
});  


