/*
  KOSSelector - based on Facebooklist from Proto!TextboxList + Autocomplete 0.1
  - Prototype version required: 6.0
  
  Credits:
  - Idea: Facebook
  - Guillermo Rauch: Original MooTools script
  
  Changelog:
  - 0.1: translation of MooTools script
  - 0.2: designing facebooklist for OV (skruk) 
*/

/* Copyright: InteRiders <http://interiders.com/> - Distributed under MIT - Keep this message! */

var KOSSelector = Class.create(EntrySelector, { 

  /**
   * Copies required properties from the responseJSON to newtag. Please override in subclasses.
   */
  copyProperties: function(newtag, responseJSON)
  {
	newtag.context = responseJSON.context;
	newtag.description = (responseJSON.description != null) ? responseJSON.context : this.texts.saveddescr; 
	newtag.lang = responseJSON.lang;
	newtag.type = responseJSON.type;
	newtag.uri =  responseJSON.uri;
  },
  
  /**
   * Checks if the given entry is new (and should be created)
   */
  isEntryNew: function(newtag)
  {
  	return newtag.context == null;
  },
  
  getLocalCallPath: function(url)
  {
	var regex = /^(\w+[:][\/]*\w+(?:[.]\w+)*(?:[:]\d+)?[\/])(.*)$/;
	return url.gsub(regex, "/json/$2");  	
  },

  
  /**
   * Initializes the component
   */
  initialize: function($super, element, autoholder, options, preloaded) 
  {
	this.services = KOSSelector.services;
	
	if (KOSSelector.texts != null)
	{
		this.texts = KOSSelector.texts;
	}

    $super(element, autoholder, options, preloaded);
  	
	KOSSelector.instances.set(element, this);
  }
});

// -------------------- static content ----------- 

/**
 * Holds map of all instances of KOSSelector created in this context
 */
KOSSelector.instances = $H({});

KOSSelector.services = 
{
	  	/**
	  	 * Allows to retrieve JSON information about the entry (GET)
	  	 */
	  	lookup : '/vocabularies/lookup?uri=',
	  	/**
	  	 * Allows to find matching entries (GET)
	  	 */
	  	search : '/vocabularies/search?size=10&label=',
	  	/**
	  	 * Allows to create new entry (PUT)
	  	 */
	  	create : '/json/tags/?_method=PUT&label='	
};

KOSSelector.texts = null;



