<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="digime" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="robots" content="all" />
		<meta name="generator" content="RapidWeaver" />
		
		<title>Searching in OpenVocabulary for ${ query }</title>
		<link rel="stylesheet" type="text/css" media="screen" href="/rw_common/themes/clean/styles.css"  />
		<link rel="stylesheet" type="text/css" media="print" href="/rw_common/themes/clean/print.css"  />
		<link rel="stylesheet" type="text/css" media="handheld" href="/rw_common/themes/clean/handheld.css"  />
		<link rel="stylesheet" type="text/css" media="screen" href="/rw_common/themes/clean/css/styles/red.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="/rw_common/themes/clean/css/sidebar/sidebar_right.css" />
		
		<script type="text/javascript" src="/rw_common/themes/clean/javascript.js"></script>
		
		<style>
			label { font-weight: bold; };
		</style>
		
	</head>
<body>
<div id="container"><!-- Start container -->
	<div class="top">&nbsp;</div>
	
	<div id="pageHeader"><!-- Start page header -->
		<h1>OpenVocabulary</h1>
		<h2>Linking Public Vocabularies</h2>
	</div><!-- End page header -->

	<div id="navcontainer"><!-- Start Navigation -->
		<ul><li><a href="/" rel="self">Welcome</a></li><li><a href="/vocabularies/" rel="self">Vocabularies</a></li><li><a href="/vocabularies/lookup.html" rel="self">Browse</a></li><li><a href="#" rel="self" id="current">Search</a></li><li><a href="/sources/" rel="self">Sources</a></li></ul>
	</div><!-- End navigation -->
	
	<div id="contentContainer"><!-- Start main content wrapper -->
		<div id="content"><!-- Start content -->
			You can also search through the full text index of the Open Vocabulary repository. 

			<hr/>
			
			<form class="search" id="adv_search_form" action="/vocabularies/search" method="get">
				<table>
				<tr><td><label for="search-q" >Search for: </label></td><td><input value="${ query }" type="search" id="search-q" name="q" size="30" autosave="at.search" results="5" placeholder="anywhere in the index" value="" /></td></tr>
				<tr><td><label for="search-size" style="width: 150px;">Limit search to: </label></td><td><input value="${ size }" type="search" id="search-size" name="size" size="20" autosave="at.search.size" results="5" placeholder="# of results" value="" /></td></tr>
				<tr><td><label for="search-threshold" style="width: 150px;">With accuracy above: </label></td><td><input value="${ threshold }" type="search" id="search-threshold" name="threshold" size="10" autosave="at.search.threshold" results="5" placeholder="0-1" value="" /></td></tr>
				<tr><td colspan="2" style="text-align: center;"><button type="submit">search</button></td></tr>
				</table>
			</form>
			
			<hr/>

			<ol>
				<c:forEach items="${ results }" var="result">
					<c:if test="${ not empty result.entry.label }">
					<li><h3>${ result.entry.label }</h3> 
					
					<c:if test="${ not empty result.entry.description }">
					<blockquote>${ result.entry.description }</blockquote>
					</c:if>
					
					<ul>
						<li><label>Accuracy: </label><span><fmt:formatNumber minFractionDigits="3" maxFractionDigits="3" value="${ result.rank }"/></span> </li>
						<li><label>Language: </label><span>${ result.entry.lang }</span></li>
						<li><label>Browse: </label><c:choose>
								<c:when test="${ result.entry.context.label eq 'WordNet' }">
									<li><a href="/vocabularies/lookup?uri=${ result.entry.URI }">${ result.entry.URI }</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="${ result.entry.URI }">${ result.entry.URI }</a></li>
								</c:otherwise>
							</c:choose></li>
					</ul>
					<br/>
					</c:if>
				</c:forEach>
			</ol>	


			<div class="clearer"></div>
			<div id="breadcrumbcontainer"><!-- Start the breadcrumb wrapper -->
				<ul><li><a href="/">Welcome</a>&nbsp;>&nbsp;</li><li><a href="#">Search</a>&nbsp;>&nbsp;</li></ul>
			</div><!-- End breadcrumb -->
		</div><!-- End content -->
	</div><!-- End main content wrapper -->
	
	<div id="sidebarContainer"><!-- Start Sidebar wrapper -->
		<div class="top"></div>
		<div id="logo"><img src="/rw_common/images/ovlogo.png" width="181" height="130" alt="Site logo"/></div>
		<div id="sidebar"><!-- Start sidebar content -->
			<h1 class="sideHeader"></h1><!-- Sidebar header -->
			<span style="font:10px Monaco; "></span> <br /><!-- sidebar content you enter in the page inspector -->
			 <!-- sidebar content such as the blog archive links -->
		</div><!-- End sidebar content -->
		<div class="bottom"></div>
	</div><!-- End sidebar wrapper -->

<div class="clearer"></div>
<div id="footer"><!-- Start Footer -->
	<p>&copy; 2009 <a href="http://www.knowledgehives.com/">Knowledge Hives</a> |  <a href="#" id="rw_email_contact">Contact Us</a><script type="text/javascript">var _rwObsfuscatedHref0 = "mai";var _rwObsfuscatedHref1 = "lto";var _rwObsfuscatedHref2 = ":op";var _rwObsfuscatedHref3 = "env";var _rwObsfuscatedHref4 = "oca";var _rwObsfuscatedHref5 = "bul";var _rwObsfuscatedHref6 = "ary";var _rwObsfuscatedHref7 = "@kn";var _rwObsfuscatedHref8 = "owl";var _rwObsfuscatedHref9 = "edg";var _rwObsfuscatedHref10 = "ehi";var _rwObsfuscatedHref11 = "ves";var _rwObsfuscatedHref12 = ".co";var _rwObsfuscatedHref13 = "m";var _rwObsfuscatedHref = _rwObsfuscatedHref0+_rwObsfuscatedHref1+_rwObsfuscatedHref2+_rwObsfuscatedHref3+_rwObsfuscatedHref4+_rwObsfuscatedHref5+_rwObsfuscatedHref6+_rwObsfuscatedHref7+_rwObsfuscatedHref8+_rwObsfuscatedHref9+_rwObsfuscatedHref10+_rwObsfuscatedHref11+_rwObsfuscatedHref12+_rwObsfuscatedHref13; document.getElementById('rw_email_contact').href = _rwObsfuscatedHref;</script></p>
</div><!-- End Footer -->
<div class="bottom">&nbsp;</div>
</div><!-- End container -->
<!-- Start Google Analytics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-5357587-3");
pageTracker._trackPageview();
} catch(err) {}</script><!-- End Google Analytics --></body>
</html>
