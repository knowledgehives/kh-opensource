<%@ tag description="Basic component for filtering terms" pageEncoding="UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="id" %>
<%@ attribute name="label" %>
<%@ attribute name="hint" %>
<%@ attribute name="preloaduris" type="java.lang.String" %>
<form action="${ id }submit" method="get" class="kosselector" accept-charset="utf-8">
  <ol>
    <li id="${ id }list" class="kosinput input-text">
      <label>${ label }</label>
      <input type="text" value="" id="${ id }" />
      <div id="${ id }auto" class="kosauto">
        <div class="default">${ hint }</div>
        <ul class="feed">
        </ul>
      </div>
    </li>
  </ol>   
</form>  

<script type="text/javascript">
	<c:choose>
		<c:when test="${ not empty preloaduris }">
			var preload = $A([<c:forEach items="${ fn:split(preloaduris, ';') }" var="item" varStatus="status">
							"${ item }"<c:if test="${ not status.last }">,</c:if>
		  					</c:forEach>]);
		</c:when>
		<c:otherwise>
			var preload = null;
		</c:otherwise>
	</c:choose>

	<fmt:bundle basename="com.kh.ov.i18n.jsmessages">
	KOSSelector.texts = {
		addtag: "<fmt:message key='KOSSelector.addtag'/>", 
		warningtag: "<fmt:message key='KOSSelector.warningtag'/>", 
		subcategory: "<fmt:message key='KOSSelector.subcategory'/>", 
		inhierarchy: "<fmt:message key='KOSSelector.inhierarchy'/>", 
		saveddescr: "<fmt:message key='KOSSelector.saveddescr'/>"
	};	
	</fmt:bundle>

	<c:choose>
		<c:when test="${ lang eq 'pl' }">KOSSelector.services.create = "/json/tags/?lang=pl-pl&_method=PUT&label=";</c:when>
		<c:otherwise>KOSSelector.services.create = "/json/tags/?lang=en-us&_method=PUT&label=";</c:otherwise>
	</c:choose>
	
	new KOSSelector('${ id }', '${ id }auto', null, preload);
</script>