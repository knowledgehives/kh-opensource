<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ov" uri="http://www.openvocabulary.info/jsp/ov" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="robots" content="all" />
		<meta name="generator" content="RapidWeaver" />
		
		<title>Browse - ${ term.label }</title>
		<link rel="stylesheet" type="text/css" media="screen" href="/rw_common/themes/clean/styles.css"  />
		<link rel="stylesheet" type="text/css" media="print" href="/rw_common/themes/clean/print.css"  />
		<link rel="stylesheet" type="text/css" media="handheld" href="/rw_common/themes/clean/handheld.css"  />
		<link rel="stylesheet" type="text/css" media="screen" href="/rw_common/themes/clean/css/styles/red.css" />
		<link rel="stylesheet" type="text/css" media="screen" href="/rw_common/themes/clean/css/sidebar/sidebar_right.css" />
		
    	<link rel="alternate" type="application/rdf+xml" title="RDF Version" href="${ term.URI }?rdf=true" />		
		
		
		<script type="text/javascript" src="/rw_common/themes/clean/javascript.js"></script>
		
		<style>
			label { font-weight: bold; };
		</style>
		
		
	</head>
<body>
<div id="container"><!-- Start container -->
	<div class="top">&nbsp;</div>
	
	<div id="pageHeader"><!-- Start page header -->
		<h1>OpenVocabulary</h1>
		<h2>Linking Public Vocabularies</h2>
	</div><!-- End page header -->

	<div id="navcontainer"><!-- Start Navigation -->
		<ul><li><a href="/" rel="self">Welcome</a></li><li><a href="/vocabularies/" rel="self">Vocabularies</a></li><li><a href="#" rel="self" id="current">Browse</a></li><li><a href="/vocabularies/search.html" rel="self">Search</a></li><li><a href="/sources/" rel="self">Sources</a></li></ul>
	</div><!-- End navigation -->
	
	<div id="contentContainer"><!-- Start main content wrapper -->
		<div id="content"><!-- Start content -->
		
			<c:choose>
				<c:when test="${ not empty term }">
					<h1>${ term.label }</h1>
					
					<c:if test="${ not empty term.description }">
					<blockquote>${ term.description }</blockquote>
					</c:if>
					
					<c:set var="iswn" value="${ term.context.label eq 'WordNet'}"/>
					
					<h2>URIs:</h2><%-- TODO-i18n --%>
					<ul>
						<li><a href="${ term.URI }">${ term.URI }</a></li>
						<c:if test="${ not iswn }">
							<li><a href="${ term.URI }?json=true">JSON</a></li>
							<li><a href="${ term.URI }?rdf=true">RDF/XML</a></li>
						</c:if>
					</ul>
					
					<h2>Context:</h2> <%-- TODO-i18n --%>
					<ul>
						<li><label for="contextlabel">label: </label><span id="contextlabel">${ term.context.label }</span></li> <%-- TODO-i18n --%>
						<c:if test="${ not empty term.context.description }">
							<li><label for="contextdescription">description: </label><span id="">${ term.context.description }</span></li> <%-- TODO-i18n --%>
						</c:if>
						<li><label for="contexturi">URI: </label><span id="contexturi">${ term.context.URI }</span></li> <%-- TODO-i18n --%>
					</ul>
					
					<h2>Related Items:</h2> <%-- TODO-i18n --%>
					<c:if test="${ term.context.type eq 'TAXONOMY' and term.class.name ne 'com.kh.ov.entities.EntryEntity' }">			
						<c:if test="${ not empty term.parent }">
						<h4>Parent term:</h4>
							<ul>
							<c:choose>
								<c:when test="${ iswn }">
									<li><a href="/vocabularies/lookup?uri=${ term.parent.URI }">${ term.parent.label }</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="${ term.parent.URI }">${ term.parent.label }</a></li>
								</c:otherwise>
							</c:choose>
							</ul>
						</c:if>
			
						<c:if test="${ not empty term.subEntries }">
						<h4>Children terms:</h4>
						<ul>
						<c:forEach items="${ term.subEntries }" var="relatedterm">
							<c:choose>
								<c:when test="${ iswn }">
									<li><a href="/vocabularies/lookup?uri=${ relatedterm.URI }">${ relatedterm.label }</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="${ relatedterm.URI }">${ relatedterm.label }</a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						</ul>
						</c:if>
					</c:if>
					
					<c:if test="${ term.context.type eq 'THESAURUS'  and term.class.name ne 'com.kh.ov.entities.EntryEntity' }">			
						<c:if test="${ not empty term.synonyms }">
						<h4>Synonyms:</h4>
						<ul>
						<c:forEach items="${ term.synonyms }" var="relatedterm">
							<c:choose>
								<c:when test="${ iswn }">
									<li><a href="/vocabularies/lookup?uri=${ relatedterm.URI }">${ relatedterm.label }</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="${ relatedterm.URI }">${ relatedterm.label }</a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						</ul>
						</c:if>
						
						<c:if test="${ not empty term.hypernyms }">
						<h4>Hypernyms:</h4>
						<ul>
						<c:forEach items="${ term.hypernyms }" var="relatedterm">
							<c:choose>
								<c:when test="${ iswn }">
									<li><a href="/vocabularies/lookup?uri=${ relatedterm.URI }">${ relatedterm.label }</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="${ relatedterm.URI }">${ relatedterm.label }</a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						</ul>
						</c:if>
						
						<c:if test="${ not empty term.hyponyms }">
						<h4>Hyponyms:</h4>
						<ul>
						<c:forEach items="${ term.hyponyms }" var="relatedterm">
							<c:choose>
								<c:when test="${ iswn }">
									<li><a href="/vocabularies/lookup?uri=${ relatedterm.URI }">${ relatedterm.label }</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="${ relatedterm.URI }">${ relatedterm.label }</a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						</ul>
						</c:if>
						
						<c:if test="${ not empty term.antonyms }">
						<h4>Antonyms:</h4>
						<ul>
						<c:forEach items="${ term.antonyms }" var="relatedterm">
							<c:choose>
								<c:when test="${ iswn }">
									<li><a href="/vocabularies/lookup?uri=${ relatedterm.URI }">${ relatedterm.label }</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="${ relatedterm.URI }">${ relatedterm.label }</a></li>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						</ul>
						</c:if>
					</c:if>
					
					<hr/>			
					 
					<c:forEach items="${ term.context.treeProperties }" var="treeprop">
						<h4><a href="${ treeprop }">${ treeprop }</a></h4>	
						<ul>
							<c:forEach items="${ ov:getRelatedEntries( term, treeprop ) }" var="relatedterm">
							<c:choose>
								<c:when test="${ iswn }">
									<li><a href="/vocabularies/lookup?uri=${ relatedterm.URI }">${ relatedterm.label }</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="${ relatedterm.URI }">${ relatedterm.label }</a></li>
								</c:otherwise>
							</c:choose>
							</c:forEach>
						</ul>
					</c:forEach>
					
					
					<c:forEach items="${ term.context.additionalProperties }" var="addprop">
						<h3><a href="${ addprop }">${ addprop }</a></h3>	
						<ul>
							<c:forEach items="${ ov:getRelatedEntries( term, addprop ) }" var="relatedterm">
								<c:when test="${ iswn }">
									<li><a href="/vocabularies/lookup?uri=${ relatedterm.URI }">${ relatedterm.label }</a></li>
								</c:when>
								<c:otherwise>
									<li><a href="${ relatedterm.URI }">${ relatedterm.label }</a></li>
								</c:otherwise>
							</c:forEach>
						</ul>
					</c:forEach>
				</c:when>
				<c:otherwise>
					The best way to start browsing through the OpenVocabulary repository is to start with one of <a href="/vocabularies/" rel="self" title="Vocabularies">vocabularies</a>.
				</c:otherwise>
			</c:choose>
		
			<div class="clearer"></div>
			<div id="breadcrumbcontainer"><!-- Start the breadcrumb wrapper -->
				<ul><li><a href="/">Welcome</a>&nbsp;>&nbsp;</li><li><a href="#">Browse</a>&nbsp;>&nbsp;</li></ul>
			</div><!-- End breadcrumb -->
		</div><!-- End content -->
	</div><!-- End main content wrapper -->
	
	<div id="sidebarContainer"><!-- Start Sidebar wrapper -->
		<div class="top"></div>
		<div id="logo"><img src="/rw_common/images/ovlogo.png" width="181" height="130" alt="Site logo"/></div>
		<div id="sidebar"><!-- Start sidebar content -->
			<h1 class="sideHeader"></h1><!-- Sidebar header -->
			<span style="font:10px Monaco; "><form class="search" id="search_form" action="/vocabularies/search" method="get">
	<input type="search" id="proj-search" name="q" size="15" accesskey="f" autosave="at.search" results="5" placeholder="Search" value="" /><button type="submit">search</button>
	</form></span> <br /><!-- sidebar content you enter in the page inspector -->
			 <!-- sidebar content such as the blog archive links -->
		</div><!-- End sidebar content -->
		<div class="bottom"></div>
	</div><!-- End sidebar wrapper -->

<div class="clearer"></div>
<div id="footer"><!-- Start Footer -->
	<p>&copy; 2009 <a href="http://www.knowledgehives.com/">Knowledge Hives</a> |  <a href="#" id="rw_email_contact">Contact Us</a><script type="text/javascript">var _rwObsfuscatedHref0 = "mai";var _rwObsfuscatedHref1 = "lto";var _rwObsfuscatedHref2 = ":op";var _rwObsfuscatedHref3 = "env";var _rwObsfuscatedHref4 = "oca";var _rwObsfuscatedHref5 = "bul";var _rwObsfuscatedHref6 = "ary";var _rwObsfuscatedHref7 = "@kn";var _rwObsfuscatedHref8 = "owl";var _rwObsfuscatedHref9 = "edg";var _rwObsfuscatedHref10 = "ehi";var _rwObsfuscatedHref11 = "ves";var _rwObsfuscatedHref12 = ".co";var _rwObsfuscatedHref13 = "m";var _rwObsfuscatedHref = _rwObsfuscatedHref0+_rwObsfuscatedHref1+_rwObsfuscatedHref2+_rwObsfuscatedHref3+_rwObsfuscatedHref4+_rwObsfuscatedHref5+_rwObsfuscatedHref6+_rwObsfuscatedHref7+_rwObsfuscatedHref8+_rwObsfuscatedHref9+_rwObsfuscatedHref10+_rwObsfuscatedHref11+_rwObsfuscatedHref12+_rwObsfuscatedHref13; document.getElementById('rw_email_contact').href = _rwObsfuscatedHref;</script></p>
</div><!-- End Footer -->
<div class="bottom">&nbsp;</div>
</div><!-- End container -->
<!-- Start Google Analytics -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-5357587-3");
pageTracker._trackPageview();
} catch(err) {}</script><!-- End Google Analytics --></body>
</html>

