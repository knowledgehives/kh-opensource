/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.servlets.lod;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.junit.Test;

/**
 * @author sebastiankruk
 *
 */
public class TagsServletTest {

	String[] labels = {"test", "test2", "dupa"};
	
	String[] accepts = { "application/json", "application/rdf+xml", "text/html" };

	HttpClientParams params = new HttpClientParams();
	HttpClient client = new HttpClient(params);
	
	/**
	 * Test method for {@link com.kh.ov.servlets.lod.TagsServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}.
	 */
	@Test
	public void testDoPostHttpServletRequestHttpServletResponse() {
		for ( String accept : accepts )
		{
			params.setParameter("Accept", accept);
			
			for (String label : labels)
			{
				PostMethod method = new PostMethod("http://localhost:8080/tags/");
				method.addParameter("label", label);
				method.addParameter("lang", "en-us");
				method.addParameter("_method", "PUT");
				try 
				{
					int code = client.executeMethod(method);
					
					System.out.println(label+"["+accept+"]["+code+"] : "+method.getResponseContentLength());
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}// try-catch
				
			}// -- for uris
		}// for accepts
	}

	/**
	 * Test method for {@link com.kh.ov.servlets.lod.TagsServlet#doPut(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}.
	 */
	@Test
	public void testDoPutHttpServletRequestHttpServletResponse() {
		for ( String accept : accepts )
		{
			params.setParameter("Accept", accept);
			
			for (String label : labels)
			{
				PutMethod method = new PutMethod("http://localhost:8080/tags/");
				method.setQueryString("label="+label+"&lang=pl-pl");
				try 
				{
					int code = client.executeMethod(method);
					
					System.out.println(label+"["+accept+"]["+code+"] : "+method.getResponseContentLength());
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}// try-catch
				
			}// -- for uris
		}// for accepts
	}

}
