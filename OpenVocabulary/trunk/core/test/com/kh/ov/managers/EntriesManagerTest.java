/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.managers;

import org.junit.Test;
import org.ontoware.rdf2go.model.node.impl.URIImpl;

import com.kh.ov.model.Entry;

/**
 * @author sebastiankruk
 *
 */
public class EntriesManagerTest {

	/**
	 * Test method for {@link com.kh.ov.managers.EntriesManager#getEntry(org.ontoware.rdf2go.model.node.URI, org.ontoware.rdf2go.model.Model)}.
	 * 
	 */
	@Test
	public final void testGetEntry() {
		ContextsManager.loadContexts();
		
		String[] uris = {
//				"http://www.openvocabulary.info/thesauri/ot/pl/instances/concept-boczycc_siee",
				"http://www.openvocabulary.info/taxonomies/acm/Computing_Methodologies/Artificial_intelligence/Programming_Languages_and_Software",
				"http://www.openvocabulary.info/taxonomies/loc/General_works"
//				"http://www.openvocabulary.info/thesauri/ot/pl/instances/concept-ablucja"
//				"http://www.openvocabulary.info/thesauri/ot/pl/instances/wordsense-alfabet-1",
//				"http://www.openvocabulary.info/thesauri/ot/pl/instances/wordsense-subskrybent_-ksiaazzk---1",
//				"http://www.openvocabulary.info/tags/en_US/czarnego_kota",
//				"http://www.openvocabulary.info/tags/en_US/ali_pies",
//				"http://www.w3.org/2006/03/wn/wn20/instances/synset-regularity-noun-2",
//				"http://www.w3.org/2006/03/wn/wn20/instances/synset-niceness-noun-2",
//				"http://www.openvocabulary.info/taxonomies/dmoz/pl/Top/World/Polska",
//				"http://www.openvocabulary.info/taxonomies/dmoz/all/Arts/Magazines_and_E-zines"
		};
//		
		for(String uri : uris){
			Entry entry = EntriesManager.getEntry(new URIImpl(uri), null);
			System.out.println("URI: "+entry.getURI());
			System.out.println("Label: "+entry.getLabel());
			System.out.println("Lang: "+entry.getLang());
			System.out.println("Descr: "+entry.getDescription());
			System.out.println("Context: "+entry.getContext().getURI());
			System.out.println("Class: "+entry.getClass());
			System.out.println("Type: "+entry.getContext().getType());
//		}
		
//			Entry _entry = EntriesManager.getEntry(new URIImpl(uri), null);
//			System.out.println(_entry.getClass().getCanonicalName());
		}		
		
	}

}
