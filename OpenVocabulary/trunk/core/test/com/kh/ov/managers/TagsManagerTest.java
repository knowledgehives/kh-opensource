/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.managers;

import java.util.Locale;

import org.junit.Test;
import org.ontoware.rdf2go.model.node.URI;

import com.kh.ov.model.Context;
import com.kh.ov.model.Entry;

/**
 * @author sebastiankruk
 *
 */
public class TagsManagerTest {

	/**
	 * Test method for {@link com.kh.ov.managers.TagsManager#extractEntries(java.lang.String)}.
	 */
	@Test
	public final void testExtractEntries() {
		ContextsManager.loadContexts();
		
		
		String[] tests = {
			"ala ma kota",
			"kot, ma ale",
			"ala \"ma psa\"",
			"'ala nie' \"ma psa\"",
			"pies \"nie lubi\" \"czarnego kota\"",
			"ale, \"ali pies\", \"lubi inne koty\" tez ali"
		};
		
		for(String test : tests){
			TagsManager.extractEntries(test, null, null, null);
		}
		
	}
	
	@Test
	public final void testExplodeEntries(){
		
		Locale.setDefault(new Locale("pl", "PL"));
		
		System.out.println("ąśćżź".matches("[:alnum:]+"));
		
		String[] tests = {
				"ala ma f:kota",
				"kot, f:ma ale",
				"ala f:\"ma psa\"",
				"ala f: \"ma psa\"",
				"t:'ala nie' \"ma psa\"",
				"t:pies \"nie lubi\" \"czarnego kota\"",
				"ale, \"ali pies\", \"lubi inne koty\" f:tez ali",
				"fotografia cyfrowa",
				"cyfrowe zdjecia",
				"cyfrowe zdjęcia",
				"cyfrowe zdjecia d: fotografia",
				"cyfrowe zdjecia d:fotografia",
				"d:fotografia cyfrowe zdjecia",
				".NET",
				"digi.me",
				"\"digi.me\" dupa",
				"dupa, digi.me",
				"objective-c C# c++ ++i"
//				,
//				"ala ma kota",
//				"kot, ma ale",
//				"ala \"ma psa\"",
//				"'ala nie' \"ma psa\"",
//				"pies \"nie lubi\" \"czarnego kota\"",
//				"ale, \"ali pies\", \"lubi inne koty\" tez ali"
			};
			
			for(String test : tests){
				for(String[] parts : TagsManager.explodeEntries(test))
					System.out.print(parts[0]+" : "+parts[1]+" | ");
				System.out.println(" #");
			}
	}
	
	@Test
	public final void testCreateURI() {
		ContextsManager.loadContexts();

		String[] tests = {
				"entag1",
				"entag2",
				"pies"
		};
		
		for(String test : tests){
			URI u = TagsManager.createURI(test, "en", ContextsManager.getTagContext("en"));
			System.out.println(u);
		}
		
	} 
	
	@Test
	public final void testGetTag(){
		ContextsManager.loadContexts();
	
		String[] tests = {
				"ala",
				"ma ale",
				"pies"
		};
		
		Context tagContext = ContextsManager.getTagContexts().values().iterator().next();
		
		for(String test : tests){
			URI u = TagsManager.createURI(test, null, tagContext);
			Entry e = TagsManager.getTag(u, tagContext, null);
			System.out.println(e);
		}
		
	}
	
	@Test
	public final void testCreateTag(){
		ContextsManager.loadContexts();
		
		String[] tests = {
				"cup",
				"office",
				"tv",
				"a tag"
		};
		
		for(String test : tests){
			Entry e = TagsManager.createTag(test, null, null, null);
			System.out.println(e);
		}
		
	}

}
