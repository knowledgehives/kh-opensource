/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.persistance;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.lucene.index.CorruptIndexException;
import org.ontoware.aifbcommons.collection.ClosableIterator;
import org.ontoware.rdf2go.exception.ModelRuntimeException;
import org.ontoware.rdf2go.model.ModelSet;
import org.ontoware.rdf2go.model.Statement;
import org.ontoware.rdf2go.model.Syntax;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.Variable;
import org.ontoware.rdf2go.vocabulary.RDF;

import com.kh.ov.entities.ContextEntity;
import com.kh.ov.managers.EntriesManager;
import com.kh.ov.model.Context;
import com.kh.ov.model.Entry;
import com.kh.ov.model.TaxonomyEntry;
import com.kh.ov.utils.AddableHashMap;
import com.kh.ov.utils.EntryResult;
import com.kh.ov.utils.StringChecker;

/**
 * Wrapps all necessary operations for KOS storing/caching 
 * Implements singleton pattern - with exception for custom Storage based on the model (see {@link #createStorage(Model)})
 * 
 * @author skruk
 *
 */
public class Storage {

	/**
	 * Singleton instance
	 */
	protected static Storage instance = null;

	
	protected ModelSet model = null;
	
	/**
	 * 
	 */
	protected Storage() {
		/// --- we need to establish communication first
		this.model = Repository.getInstance().getModel();
	}
	
	protected Storage(ModelSet _model){
		this.model = _model;
	}
	
	public ModelSet getModel() {
		return model;
	}
	
	/**
	 * Returns singleton instance
	 * 
	 * @return
	 */
	public static Storage getInstance(){
		synchronized(Storage.class){
			if(instance == null){
				instance = new Storage();
			}// -- if instance == null
		}// -- synch
		
		return instance;
	}
	
	/**
	 * Allows to create custom Storage - based on the model
	 * @param model
	 * @return
	 */
	public static Storage createStorage(ModelSet model){
		return ( model != null ) ? new Storage(model) : new Storage(); 
	}
	
	
	/**
	 * Checks if instance with given URI exists in the model.
	 * 
	 * @param subjectURI URI of the subject to be checked
	 * @return <code>true</code> if given instance exists in the model
	 */
	public boolean checkForInstance(URI subjectURI){
		if(subjectURI == null)
			return false;
		
		return this.model.containsStatements(Variable.ANY, subjectURI, Variable.ANY, Variable.ANY);
	}
	
	/**
	 * Returns set with <code>rdf:type</code>s of give <code>resource</code> 
	 * @param resource
	 * @return
	 */
	public Set<URI> getType(URI resource){
		Set<URI> result = new HashSet<URI>();
		ClosableIterator<Statement> it;
		
		for(it = this.model.findStatements(Variable.ANY, resource, RDF.type, Variable.ANY);
			it.hasNext(); ) {
			result.add(it.next().getObject().asURI());
		}
		it.close();
		
		return result;
	}
	

	/**
	 * Loads RDF stream into the repository
	 * 
	 * @param _resource rdf stream to load
	 * @param rdfformat format of the stream
	 * @throws IOException 
	 * @throws ModelRuntimeException 
	 */
	public void loadRdf(InputStream _resource, String rdfformat) throws ModelRuntimeException, IOException{
		this.model.readFrom(_resource, Syntax.forName(rdfformat));
	}
	
	
	
	/**
	 * Gets literal value for given entry and property
	 * 
	 * @param _entry
	 * @param property
	 * @return
	 */
	public String getLiteral(Entry _entry, Concepts property){
		return this.getLiteral(_entry.getURI(), property.get());
	}

	/**
	 * Gets literal value for given entry and property
	 * 
	 * @param _entry
	 * @param property
	 * @return
	 */
	public String getLiteral(URI _uri, URI property){
		String literal = null;
		
		ClosableIterator<Statement> stmtit = this.model.findStatements(Variable.ANY, _uri, property, Variable.ANY); 
		if(stmtit.hasNext()) {
			literal = stmtit.next().getObject().asLiteral().getValue();
		}
		stmtit.close();
		
		return literal;
	}	
	
	/**
	 * Gets literal value for given entry and property
	 * 
	 * @param _entry
	 * @param property
	 * @return
	 */
	public String[] getLiterals(Entry _entry, String property){
		List<String> lliteral = new ArrayList<String>();
		
		for(Iterator<Statement> stmtit = this.model.findStatements(Variable.ANY, _entry.getURI(), this.model.createURI(property), Variable.ANY); stmtit.hasNext(); ) {
			lliteral.add(stmtit.next().getObject().asLiteral().getValue());
		}
		
		return lliteral.toArray(new String[lliteral.size()]);
	}
	
	
	/**
	 * Sets given literal 
	 * 
	 * @param _entry
	 * @param property
	 * @param _value
	 * @throws AccessDeniedException
	 * @deprecated
	 */
	@Deprecated
	public void setLiterals(Entry _entry, String property, String ... _values){
		this.model.removeStatements(Variable.ANY, _entry.getURI(), this.model.createURI(property), Variable.ANY);
	
		this.addLiterals(_entry, property, _values);
	}
	
	/**
	 * Sets given literal 
	 * 
	 * @param _entry
	 * @param property
	 * @param _value
	 * @throws AccessDeniedException
	 * @deprecated
	 */
	@Deprecated
	public void addLiterals(Entry _entry, String property, String ... _values){
		for(String _value : _values) {
			this.model.addStatement(null, _entry.getURI(), this.model.createURI(property), this.model.createPlainLiteral(_value));
		}
	}
	
	/**
	 * @param _entry
	 * @param _prop
	 * @return
	 */
	public Collection<? extends Entry> listBy(Entry _entry, String[] _prop){
		Collection<Entry> result = new HashSet<Entry>();
		
//		boolean isTaxonomy = (_entry instanceof TaxonomyEntry);
		
		for(String treeProp : _prop) 
			for(Iterator<Statement> stmtit = this.model.findStatements(Variable.ANY, _entry.getURI(), this.model.createURI(treeProp), Variable.ANY); stmtit.hasNext(); )
				result.add(EntriesManager.getEntryByURI(stmtit.next().getObject().asURI(), 
														_entry.getContext(), null));
//				if(isTaxonomy)
//					result.add(new TaxonomyEntryEntity((TaxonomyContext)_entry.getContext(), stmtit.next().getObject().asURI()));
//				else
//					result.add(new EntryEntity(_entry.getContext(), stmtit.next().getObject().asURI()));
	
		
		return result;
	}
	
	
	/**
	 * Allows to find taxonomy concepts with a given text in description
	 * @param _context
	 * @param content
	 * @param threshold TODO
	 * @param property
	 * @param ignoreCase
	 * @return
	 */
	public Map<Entry, Float> search(Context _context, String content, Float threshold){
		Map<Entry, Float> mresult = new AddableHashMap<Entry, Float>(); 

		Map<URI, Float> mfound = null;
		try {
			mfound = FulltextIndex.getInstance().findUris(content, threshold, (String[])null);
		} catch (CorruptIndexException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
			
		if(mfound != null)
			for(Map.Entry<URI, Float> entry : mfound.entrySet()){
				Entry kos = EntriesManager.getEntryByURI(entry.getKey(), _context, null);
				mresult.put(kos, entry.getValue());
			}
		
		return mresult;
	}
	
	/**
	 * Wrapps {@link #search(Context, String, Float)} and delivers sorted results using given comparator (if null - default sorting is used)
	 * @param context
	 * @param content
	 * @param size TODO
	 * @param threshold TODO
	 * @param comparator
	 * @return
	 */
	public SortedSet<EntryResult> sortedSearch(Context context, String content, Integer size, Float threshold, Comparator<EntryResult> comparator){
		Map<Entry, Float> mresult = this.search(context, content, threshold);
		
		SortedSet<EntryResult> result = (comparator != null)?new TreeSet<EntryResult>(comparator):new TreeSet<EntryResult>();
		for(Map.Entry<Entry, Float> entry : mresult.entrySet())
			result.add(new EntryResult(entry.getKey(), entry.getValue()));
		
		if(size != null && result.size() > size){
			EntryResult[] res = result.toArray(new EntryResult[0]);
			
			result = result.headSet(res[size]);
		}
		
		
		return result;
	}
	
	/**
	 * Allows for find taxonomy concepts with a given text in description and within given taxonomy
	 * @param _context
	 * @param content
	 * @param threshold TODO
	 * @param property
	 * @param ignoreCase
	 * @return
	 */
	public Map<Entry, Float>  searchByContentAndContext(ContextEntity _context, String content, Float threshold){
		Map<Entry, Float> mresult = new AddableHashMap<Entry, Float>(); 

		Map<URI, Float> mfound = null;
		try {
			mfound = FulltextIndex.getInstance().findUris(content+" AND inScheme:"+_context.getLabel(), threshold, (String[])null);
		} catch (CorruptIndexException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
			
		if(mfound != null)
			for(Map.Entry<URI, Float> entry : mfound.entrySet()){
				Entry kos = EntriesManager.getEntryByURI(entry.getKey(), _context, null);
				mresult.put(kos, entry.getValue());
			}
		
		return mresult;
	}
	
	//--imported from taxonomystorage:
	/**
	 * Synchronizing parent-child triples
	 * 
	 * @param _parent
	 * @param _child
	 */
	public void addChild(TaxonomyEntry _parent, TaxonomyEntry _child){
		String[] cTreeProps = _parent.getContext().getTreeProperties();
		
		if(cTreeProps.length > 0){
			this.addChild(_parent, _child, cTreeProps[0]);
		}
	}
	
	/**
	 * Adds given Taxonomy entry to the RDF storage
	 * @param _entry
	 */
	public void addEntry(TaxonomyEntry _entry) {
		this.model.addStatement(_entry.getContext().getNamespaceURI(), _entry.getURI(), Concepts.SKOS_PREFLABEL.get(), this.model.createLanguageTagLiteral(_entry.getLabel(), _entry.getContext().getLocale()));
		this.model.addStatement(_entry.getContext().getNamespaceURI(), _entry.getURI(), Concepts.SKOS_INSCHEME.get(), _entry.getContext().getNamespaceURI());
		if(StringChecker.isNotEmpty(_entry.getDescription()))
			this.model.addStatement(_entry.getContext().getNamespaceURI(), _entry.getURI(), Concepts.SKOS_DEFINITION.get(), this.model.createLanguageTagLiteral(_entry.getDescription(), _entry.getContext().getLocale()));
		
		for(TaxonomyEntry subentry : _entry.getSubEntries()) {
			this.model.addStatement(_entry.getContext().getNamespaceURI(), _entry.getURI(), Concepts.SKOS_NARROWER.get(), subentry.getURI());
			this.model.addStatement(_entry.getContext().getNamespaceURI(), subentry.getURI(), Concepts.SKOS_BROADER.get(), _entry.getURI());
		}
		
		
	}
	
	/**
	 * Synchronizing parent-child triples
	 * 
	 * @param _parent
	 * @param _child
	 */
	public void addChild(TaxonomyEntry _parent, TaxonomyEntry _child, String treeProp){
		this.model.addStatement(_parent.getContext().getNamespaceURI(), _parent.getURI(), this.model.createURI(treeProp), _child.getURI());
	}
	
	
	/**
	 * Lists all children from this parent - gets them from RDF Schema
	 * 
	 * @param _parent
	 */
	public<K> List<TaxonomyEntry> syncChildren(TaxonomyEntry _parent){
		List<TaxonomyEntry> list = new ArrayList<TaxonomyEntry>();
		
		for(String treeProp : _parent.getContext().getTreeProperties()) {
			for(Iterator<Statement> stmtit = this.model.findStatements(Variable.ANY, _parent.getURI(), this.model.createURI(treeProp), Variable.ANY); stmtit.hasNext(); ) {
				TaxonomyEntry child = (TaxonomyEntry) EntriesManager.getEntryByURI(stmtit.next().getObject().asURI(), 
																	_parent.getContext(), null);
				list.add(child);
			}
		}
		
		_parent.setSubEntries(list);
		return list;
	}
	
	
	/**
	 * Returns parent entry for the given entry.
	 * Checks all known tree properties in given context.
	 * If more than one parent exists - selected the first one.
	 * If the _entry is one of the root in the context - returns null
	 * 
	 * @param _entry given entry 
	 * @return parent entry
	 */
	public TaxonomyEntry getParent(TaxonomyEntry _entry){
		TaxonomyEntry parent = null;
		
		for(String treeProp : _entry.getContext().getTreeProperties()) {
			Iterator<Statement> stmtit = this.model.findStatements(Variable.ANY, Variable.ANY, this.model.createURI(treeProp), _entry.getURI()); 
			
			if(stmtit.hasNext()) {
				parent = (TaxonomyEntry)EntriesManager.getEntryByURI(stmtit.next().getSubject().asURI(), _entry.getContext(), null);
				break;
			}
		}
		
		return parent;
	}
	
	
	
}
