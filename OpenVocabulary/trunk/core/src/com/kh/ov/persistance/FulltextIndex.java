/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.persistance;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.LockObtainFailedException;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kh.ov.Configuration;
import com.kh.ov.managers.ContextsManager;
import com.kh.ov.model.Context;
import com.kh.ov.utils.AddableHashMap;
import com.kh.ov.utils.Ref;

/**
 * @author sebastiankruk
 * 
 * Indexing framework (singleton)
 * 
 */
public class FulltextIndex {

	/**
	 * Singleton instance
	 */
	protected static final FulltextIndex instance = new FulltextIndex();
	
	/**
	 * Default logger for indexing
	 */
	protected static final Logger logger = LoggerFactory.getLogger(FulltextIndex.class);
	
	/**
	 * Analyzer implementation
	 */
	protected Analyzer analyzer;
	/**
	 * Dictionary reference
	 */
	protected Directory directory;
	
	
	public FulltextIndex() {
	    String dir = Configuration.getProperty("com.kh.ov.index.path");

	    File fdir = new File(dir);
	    if(!fdir.exists()) {
	    	fdir.mkdirs();
			try {
				IndexWriter writer = new IndexWriter(fdir, this.analyzer, true);
				Document indexDoc = new Document();
					
				writer.addDocument(indexDoc);
				writer.close();
			} catch (java.io.IOException ex) {
				logger.warn(ex.toString());
			}
	    }
	    
	    System.out.println(">>>> "+fdir.getPath());
	    
		this.analyzer = new StandardAnalyzer();
		try {
			this.directory = FSDirectory.getDirectory(dir);
		} catch (IOException e) {
			logger.error("Problem with accessing index directory", e);
		}
		
//	    IndexWriter iwriter = new IndexWriter(directory, analyzer);
//	    iwriter.setMaxFieldLength(25000);
//	    
//	    
//	    Document doc = new Document();
//	    String text = "This is the text to be indexed.";
//	    doc.add(new Field("fieldname", text, Field.Store.YES,
//	        Field.Index.TOKENIZED));
//	    iwriter.addDocument(doc);
//	    iwriter.optimize();
//	    iwriter.close();
//	    
//	    // Now search the index:
//	    IndexSearcher isearcher = new IndexSearcher(directory);
//	    // Parse a simple query that searches for "text":
//	    QueryParser parser = new QueryParser("fieldname", analyzer);
//	    Query query = parser.parse("text");
//	    Hits hits = isearcher.search(query);
//	    //assertEquals(1, hits.length());
//	    // Iterate through the results:
//	    for (int i = 0; i < hits.length(); i++) {
//	      Document hitDoc = hits.doc(i);
//	      //assertEquals("This is the text to be indexed.", hitDoc.get("fieldname"));
//	    }
//	    isearcher.close();
//	    directory.close();		
	}
	
	public Analyzer getAnalyzer() {
		return this.analyzer;
	}
	
	public Directory getDirectory() {
		return this.directory;
	}
	
	public void add(URI uri, String label, String text, String[] types, Map<String, String[]> other, boolean overwrite, String inScheme, IndexWriter indexWriter) throws CorruptIndexException, IOException{
		Collection<String> labels = new HashSet<String>();
		labels.add(label);
		
		this.add(uri, labels, text, Arrays.asList(types), other, overwrite, inScheme, indexWriter);
	}
	
	
	/**
	 * Adds document to the index, if the document exists it will be the information will be appended if <code>overwrite != null</code> 
	 * @param uri
	 * @param text
	 * @param types TODO
	 * @param other
	 * @param inScheme TODO
	 * @param indexWriter TODO
	 * @param label_
	 * @throws IOException 
	 * @throws CorruptIndexException 
	 */
	public void add(URI uri, Collection<String> labels, String text, Collection<String> types, Map<String, String[]> other, boolean overwrite, String inScheme, IndexWriter indexWriter) throws CorruptIndexException, IOException {
		Document doc = (overwrite) ? this.get(uri) : null;
		
		IndexWriter iwriter;
		
		synchronized(instance){
			iwriter = (indexWriter != null) ? indexWriter : new IndexWriter(directory, analyzer);

			try {
				//first remove the document if required
				if(doc != null && overwrite) {
					this.remove(uri);
					doc = null;
				}
		
				if(doc == null)
					doc = new Document();
				
				if(types != null)
					for(String type : types)
						doc.add(new Field("type", type, Field.Store.YES, Field.Index.UN_TOKENIZED));
				
				doc.add(new Field("inScheme", inScheme, Field.Store.YES, Field.Index.UN_TOKENIZED));
				doc.add(new Field("URI", uri.toString(), Field.Store.YES, Field.Index.UN_TOKENIZED));
				
				if(labels != null)
					for(String label : labels)
						doc.add(new Field("label", label, Field.Store.YES, Field.Index.TOKENIZED));
				
				doc.add(new Field("content", text, Field.Store.COMPRESS, Field.Index.TOKENIZED));
				if(other != null && other.size() > 0)
					for(Map.Entry<String, String[]> entry : other.entrySet())
						for(String value : entry.getValue())
							doc.add(new Field(entry.getKey(), value, Field.Store.YES, Field.Index.TOKENIZED));
				
				iwriter.addDocument(doc);
			}catch(Exception ex) {
				logger.error("Problem with adding new document to index", ex);
			}finally {
				if(indexWriter == null)
					iwriter.close();
			}
		}
		
		//logger.info("Indexing of uri="+uri+" label="+labels+" completed");
		
//		    Document doc = new Document();
//		    String text = "This is the text to be indexed.";
//		    doc.add(new Field("fieldname", text, Field.Store.YES,
//		        Field.Index.TOKENIZED));
//		    iwriter.addDocument(doc);
//		    iwriter.optimize();
//		    iwriter.close();
	}
	
	/**
	 * Allows to access
	 * @param uri
	 * @return
	 * @throws IOException 
	 * @throws CorruptIndexException 
	 */
	public Document get(URI uri) throws CorruptIndexException, IOException{
		Document doc = null;
		Term tDoc = new Term("URI", uri.toString());

		//--first try to determing if the document already exist
		synchronized(instance) {
			IndexSearcher isearcher = new IndexSearcher(this.directory);
		    Query query = new TermQuery(tDoc);
		    Hits hits = isearcher.search(query);
		
		    if(hits.length() > 0)
		    	doc = hits.doc(0);
		    
		    isearcher.close();
		}

		return doc;//TODO
	}
	
	/**
	 * Allows to remove given documents from index. If <code>uri</code> is null, than the whole index will be cleaned.
	 * @param uri
	 * @throws IOException 
	 * @throws CorruptIndexException 
	 */
	public void remove(URI ...  uri) throws CorruptIndexException, IOException {
		synchronized(instance) {
			IndexWriter iwriter = new IndexWriter(directory, analyzer);

			Term[] terms = new Term[uri.length];
			int i = 0;
			
			for(URI u : uri)
				terms[i++] = new Term("URI", u.toString());
				
			iwriter.deleteDocuments(terms);
		}
	}
	
	/**
	 * Allows to optimize 
	 * @throws CorruptIndexException
	 * @throws LockObtainFailedException
	 * @throws IOException
	 */
	public void optimize() throws CorruptIndexException, LockObtainFailedException, IOException {
		synchronized(instance) {
			new IndexWriter(directory, analyzer).optimize();
		}
	}
	
	
	/**
	 * Default set of fields when non are specified
	 */
	protected static final String[] DEF_FIELDS = new String[] {"label", "content"};
	
	/**
	 * Finds documents in the registery
	 * 
	 * @param query
	 * @param refIndexSearcher TODO
	 * @param ... fields
	 * @return
	 * @throws IOException 
	 * @throws CorruptIndexException 
	 * @throws ParseException 
	 */
	public Hits find(String query, Ref<IndexSearcher> refIndexSearcher, String ... _fields) throws CorruptIndexException, IOException{
		String[] fields = (_fields == null)?DEF_FIELDS:_fields;
	    
	    // Parse a simple query that searches for "text":
	    QueryParser parser = new MultiFieldQueryParser(fields, analyzer);
	    Query query_ = null;
		try {
			query_ = parser.parse(query);
		} catch (ParseException e) {
			logger.warn("Problem with parsing the query: "+query, e);
		}

		logger.info("FT Query: "+query+" -> "+query_);

	    Hits hits = null;
	    
		if(query_ != null) {
		    synchronized(instance) {
			    // Now search the index:
			    IndexSearcher isearcher = new IndexSearcher(directory);
			    hits = isearcher.search(query_);
			    
			    if(refIndexSearcher == null)
				    isearcher.close();
			    else
			    	refIndexSearcher.set(isearcher);
			}
		}
		return hits;
	}
	
	
	/**
	 * Finds entities and lists only their URIs
	 * @param query
	 * @param threshold TODO
	 * @param key
	 * @return
	 * @throws ParseException 
	 * @throws IOException 
	 * @throws CorruptIndexException 
	 */
	public Map<URI, Float> findUris(String query, Float threshold, String ... key) throws CorruptIndexException, IOException{
		Ref<IndexSearcher> refIndexSearcher = new Ref<IndexSearcher>(null);
		Hits hits = this.find(query, refIndexSearcher, key);
		Map<URI, Float> results = new AddableHashMap<URI, Float>();
		
		if(hits != null) {			
		    for (int i = 0; i < hits.length(); i++)
		    	if(threshold == null || hits.score(i) >= threshold){
			    	Document hitDoc;
					try {
						hitDoc = hits.doc(i);
					    results.put(new URIImpl(hitDoc.get("URI")), hits.score(i));
					} catch (CorruptIndexException e) {
						logger.error("looks like your index is corrupted", e);
					} catch (IOException e) {
						logger.error("Problem with writing to the index", e);
					}
			    }
		    
		    refIndexSearcher.get().close();
		}
	    
	    return results;
	}
	
	
	/**
	 * @return singleton instance
	 */
	public static FulltextIndex getInstance() {
		return instance;
	}

	/**
	 * Calling this method will start reindexing of all contexts - one by one
	 * @param cleanIndex <code>true</code> if this is the first time your index the whole repository
	 */
	public static void reindexAll(boolean cleanIndex) {
		FulltextIndex.getInstance();
		for(Context context : ContextsManager.loadContexts()){
			try {
				context.reindex(cleanIndex);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Problem when indexing context: "+context.getLabel(), e);
			}
		}
	}
	
	public static void main(String[] args){
		reindexAll(true);
	}
	
}
