/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.persistance;

import org.corrib.openrdf.OpenRDF;
import org.ontoware.rdf2go.model.ModelSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kh.ov.Configuration;

/**
 * This class replaces previous SesameWrapper. 
 * 
 * @author Sebastian Kruk
 * @version 2.0
 */
public class Repository {

	/**
	 * Singleton instance of the repository
	 */
	protected static Repository instance;

	public synchronized static Repository getInstance() {
		if (instance == null) {
			instance = new Repository();
		}

		return instance;
	}

	/**
	 * Default logger that should be used by with stuff related to RDF:DB
	 */
	public final Logger logger = LoggerFactory.getLogger(Repository.class);

	/**
	 * RDF2Go model reference
	 */
	protected ModelSet modelSet_;

	/**
	 * Singleton constructor. It also parses the
	 * <code>com.kh.openvocabulary.rdf.respository.id</code> System property and sets up RDF2Go model.
	 */
	protected Repository() {
		String repositoryID = Configuration.getProperty("com.kh.ov.respository.id");
		String serverURL = Configuration.getProperty("com.kh.ov.respository.server.url");
		String username = Configuration.getProperty("com.kh.ov.respository.server.username");
		String password = Configuration.getProperty("com.kh.ov.respository.server.password");
		
		// if serverURL is not specified then use local repository
		if ( serverURL != null) 
			modelSet_ = OpenRDF.getInstance().getRemoteModelSet(serverURL, repositoryID, username, password);
		else
			modelSet_ = OpenRDF.getInstance().getModelSet(repositoryID);
		modelSet_.open();
	}

	@Override
	protected void finalize() throws Throwable {
		modelSet_.close();

		super.finalize();
	}

	public ModelSet getModel() {
		return modelSet_;
	}
}
