/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.persistance;

import org.ontoware.rdf2go.model.node.URI;


/**
 * @author sebastiankruk
 * List of RDF concepts used in OV
 */
public enum Concepts {

	/**
	 * http://www.openvocabulary.info/ontology/Tag
	 */
	TAG("http://www.openvocabulary.info/ontology/Tag"),
	/**
	 * http://www.openvocabulary.info/ontology/AbstractConcept
	 */
	ABSTRACTCONCEPT("http://www.openvocabulary.info/ontology/AbstractConcept"),
	/**
	 * http://www.openvocabulary.info/ontology/TaxonomyConcept
	 */
	TAXONOMYCONCEPT("http://www.openvocabulary.info/ontology/TaxonomyConcept"),
	/**
	 * http://www.openvocabulary.info/ontology/hasMeaning
	 */
	HASMEANING("http://www.openvocabulary.info/ontology/hasMeaning"),
	
	/**
	 * http://www.openvocabulary.info/ontology/wordType
	 */
	WORDTYPE("http://www.openvocabulary.info/ontology/wordType"),
	
	/**
	 * skos:inScheme
	 */
	SKOS_INSCHEME("http://www.w3.org/2004/02/skos/core#inScheme"),
	/**
	 * skos:narrower
	 */
	SKOS_NARROWER("http://www.w3.org/2004/02/skos/core#narrower"),
	/**
	 * skos:broader
	 */
	SKOS_BROADER("http://www.w3.org/2004/02/skos/core#broader"),
	/**
	 * skos:related
	 */
	SKOS_RELATED("http://www.w3.org/2004/02/skos/core#related"),
	 /**
	 * skos:definition
	 */
	SKOS_DEFINITION("http://www.w3.org/2004/02/skos/core#definition"),
	 /** 
	  * skos:prefLabel
	  */
	SKOS_PREFLABEL("http://www.w3.org/2004/02/skos/core#prefLabel"), 
	/**
	 * (previously jonto:DecimalClassificationNumber) //TODO check existing databases
	 */
	XSKOS_DTAX_NUMBER("http://www.openvocabulary.info/ontology/hasDecimalClassificationNumber"), 
	 
	XSKOS_NARROWER("http://www.openvocabulary.info/ontology/x-narrower"),
	XSKOS_BROADER("http://www.openvocabulary.info/ontology/x-broader"),
	XSKOS_ANTONYM("http://www.openvocabulary.info/ontology/x-antonym"),
	
	WN_SYNSET("http://www.w3.org/2006/03/wn/wn20/schema/Synset"),
	WN_COLLOCATION("http://www.w3.org/2006/03/wn/wn20/schema/Collocation"),
	WN_WORD("http://www.w3.org/2006/03/wn/wn20/schema/Word"),
	WN_WORDSENSE("http://www.w3.org/2006/03/wn/wn20/schema/WordSense"),

	WN_CLASSIFIEDBYTOPIC("http://www.w3.org/2006/03/wn/wn20/schema/classifiedByTopic"),
	WN_CLASSIFIEDBYUSAGE("http://www.w3.org/2006/03/wn/wn20/schema/classifiedByUsage"),
	WN_CLASSIFIEDBYREGION("http://www.w3.org/2006/03/wn/wn20/schema/classifiedByRegion"),
	WN_CLASSIFIEDBY("http://www.w3.org/2006/03/wn/wn20/schema/classifiedBy"),
	WN_CONTAINSWORDSENSE("http://www.w3.org/2006/03/wn/wn20/schema/containsWordSense"),
	WN_HYPONYMOF("http://www.w3.org/2006/03/wn/wn20/schema/hyponymOf"),
	WN_ANTONYMOF("http://www.w3.org/2006/03/wn/wn20/schema/antonymOf"),
	WN_GLOSS("http://www.w3.org/2006/03/wn/wn20/schema/gloss"),
	WN_PARTMERONYMOF("http://www.w3.org/2006/03/wn/wn20/schema/partMeronymOf"),
	WN_MERONYMOF("http://www.w3.org/2006/03/wn/wn20/schema/meronymOf"),
	WN_SIMILARTO("http://www.w3.org/2006/03/wn/wn20/schema/similarTo"),
	WN_SYNSETID("http://www.w3.org/2006/03/wn/wn20/schema/synsetId"),
	WN_TAGCOUNT("http://www.w3.org/2006/03/wn/wn20/schema/tagCount"),
	WN_HASWORD("http://www.w3.org/2006/03/wn/wn20/schema/word"),
	WN_LEXICALFORM("http://www.w3.org/2006/03/wn/wn20/schema/lexicalForm");
	
	protected URI uri;
	
	Concepts(String _uri){
		this.uri = Repository.getInstance().getModel().createURI(_uri);
	}
	
	/**
	 * Returns the Sesame URI of the concept
	 *  
	 * @param g
	 * @return
	 */
	public URI get() {
		return this.uri;
	}
	
	@Override
	public String toString() {
		return this.uri.toString();
	}
}
