/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.model;

import java.util.Collection;


/**
 * @author sebastiankruk
 * This interface extends the definition of an abstract KOS entry to support Thesaurus entries
 */
public interface ThesaurusEntry extends Entry {

	/**
	 * Lists all concepts with more precise meaning.
	 * They can be retrieved through <code>skos:narrower</code> or inverse of <code>wns:hyponymOf</code>
	 * @return
	 */
	public Collection<ThesaurusEntry> getHyponyms();
	
	/**
	 * Lists all concepts with more general meaning.
	 * They can be retrieved through <code>skos:broader</code> or <code>wns:hyponymOf</code>
	 * @return
	 */
	public Collection<ThesaurusEntry> getHypernyms();
	
	/**
	 * Lists all concepts with opposite meaning.
	 * They can be retrieved through <code>wns:antonymOf</code>
	 * @return
	 */
	public Collection<ThesaurusEntry> getAntonyms();
	
	/**
	 * Lists all concepts with similar meaning.
	 * They can be retrieved through <code>skos:related</code> or by similarities on inverse property <code>wns:containsWordSense</code>
	 * @return
	 */
	public Collection<ThesaurusEntry> getSynonyms();
	
}
