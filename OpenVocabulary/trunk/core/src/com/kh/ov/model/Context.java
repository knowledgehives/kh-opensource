/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.model;

import java.io.IOException;
import java.util.Collection;
import java.util.SortedSet;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.store.LockObtainFailedException;
import org.ontoware.rdf2go.model.node.URI;

import com.kh.ov.persistance.Storage;

/**
 * @author sebastiankruk
 * This inteface defines operations possible on the definition of KOS context 
 */
public interface Context {

	/**
	 * @return label of the context
	 */
	public String getLabel();
	/**
	 * @return description of the context
	 */
	public String getDescription();
	/**
	 * @return Complete URI of the context
	 */
	public URI getNamespaceURI();
	/**
	 * @return Abbreviated name of the context
	 */
	public String getNamespaceAbbr();
	/**
	 * @return default locale of the context
	 */
	public String getLocale();
	/**
	 * @return RDF storage wrapper reference
	 */
	public Storage getStorage();
	/**
	 * @return URI of the context as {@link URI}
	 */
	public URI getURI();
	/**
	 * @return type of this context
	 */
	public ContextTypes getType();
	/**
	 * @param props list of additional properties in the context
	 */
	public void addAdditionalProperties(String...props);
	/**
	 * @param entry collection of root elements (taxonomy only)
	 */
	public void addRoot(Entry entry);
	/**
	 * @param props list of additional hierarchy creation properties
	 */
	public void addTreeProperties(String...props);
	/**
	 * @return list of additional properties in the context
	 */
	public String[]  getAdditionalProperties();
	/**
	 * @return collection of root elements (taxonomy only)
	 */
	public SortedSet<Entry> getRoots();
	/**
	 * @return list of additional hierarchy creation properties
	 */
	public String[] getTreeProperties();	
	
	public String getUrlPattern() ;
	public void setUrlPattern(String urlPattern) ;
	
	
	/**
	 * This method should be called to initiate reindexing of complete content of the KOS context
	 * @param cleanIndex <code>true</code> if this is the first time you reindex this context
	 */
	public void reindex(boolean cleanIndex) throws CorruptIndexException, LockObtainFailedException, IOException;
	
	
	/**
	 * Finds concepts by label_ values using full text index
	 * @param _label
	 * @return
	 */
	public Collection<Entry> listMatchingByLabel(String _label);
	/**
	 * Added for filtering function
	 */
	public Collection<Entry> listMatchingByLabelAndContext(String _label);
	
}