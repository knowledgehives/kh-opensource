/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.model.wordnet;

import java.util.Collection;

import com.kh.ov.entities.wordnet.PartOfSpeech;
import com.kh.ov.entities.wordnet.WordSenseEntity;
import com.kh.ov.model.ThesaurusEntry;

public interface Synset {

	/**
	 * Returns glossary description of the entry (see <code>wns:gloss</code>).
	 * @return
	 */
	public abstract String getGlossaryDescription();

	public abstract String getGloss();

	/**
	 * @return set of {@link WordSenseEntity} objects related to this <code>SynsetEntity</code> through <code>http://www.w3.org/2006/03/wn/wn20/schema/containsWordSense</code>
	 */
	public abstract Collection<ThesaurusEntry> getWordSenses();

	public abstract Collection<ThesaurusEntry> getClassifiedBy();

	public abstract Collection<ThesaurusEntry> getClassifiedByRegion();

	public abstract Collection<ThesaurusEntry> getClassifiedByTopic();

	public abstract Collection<ThesaurusEntry> getClassifiedByUsage();

	public abstract Collection<ThesaurusEntry> getMeronymOf();

	public abstract Collection<ThesaurusEntry> getPartMeronymOf();

	public abstract Collection<ThesaurusEntry> getSimilarTo();

	/**
	 * <code>WARNING</code> can return null if no POS has been identified.<br/>
	
	 * @return
	 */
	public abstract PartOfSpeech getPOS();

}