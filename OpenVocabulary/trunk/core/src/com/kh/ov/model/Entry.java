/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.model;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

import org.json.jdk5.simple.JSONObject;
import org.ontoware.aifbcommons.collection.ClosableIterable;
import org.ontoware.rdf2go.model.ModelSet;
import org.ontoware.rdf2go.model.Statement;
import org.ontoware.rdf2go.model.Syntax;
import org.ontoware.rdf2go.model.node.URI;


public interface Entry extends Comparable<Entry>{

	/**
	 * @return dc:label_, rdfs:label_ or any other short name associated with this entry
	 */
	public abstract String getLabel();

	/**
	 * @return dc:description, rdfs:comment or any other long description associated with this entry
	 */
	public abstract String getDescription();

	/**
	 * @return URI representing this entry in RDF
	 */
	public abstract URI getURI();

	/**
	 * @return reference to the taxonomy context object
	 */
	public Context getContext();

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Entry obj);

	/**
	 * Lists resources by given additional property (URI)
	 * 
	 * @param _additionalProperty additional property in this taxonomy context
	 * @return collection of resources linked from this one by given additional property
	 * @throws UnsupportedAdditionalPropertyException If given property is not registered in given taxonomy context
	 */
	public Collection<? extends Entry> getBy(String _additionalProperty) throws UnsupportedAdditionalPropertyException;

	/**
	 * @return Language associated with this entry (usually a copy from the context)
	 */
	public String getLang();
	
	/**
	 * @param _model ModelSet on which to compute the RDF graph. If <code>null</code> than the default model in JOnto is used
	 * @return Returns an RDF representation of the given KOS entry
	 * @deprecated
	 */
	@Deprecated
	public ClosableIterable<Statement> getAsRDF(ModelSet _model);
	
	/**
	 * Renders Entry as JSON
	 * @param nofollow
	 * @return
	 */
	public JSONObject getJSON(boolean nofollow);

	/**
	 * Renders this entity into RDF with given syntax 
	 * @param _model (can be {@code null})
	 * @param out
	 * @param syntax (can be {@code null})
	 * @throws IOException
	 */
	public void writeRDF(ModelSet _model, Writer out, Syntax syntax) throws IOException;
	
	
}