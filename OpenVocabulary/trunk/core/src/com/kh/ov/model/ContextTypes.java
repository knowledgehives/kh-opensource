/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.model;

/**
 * @author sebastiankruk
 * Types of contexts recognized in the system
 */
public enum ContextTypes {

	/**
	 * Context with entries which can be structured in the hierarchy
	 * 
	 * <pre>PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>
			PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>
			SELECT ?uri ?l
			WHERE { ?uri skos:inScheme <%1$s> .
			      { { ?uri skos:prefLabel ?l } 
			        UNION
			        { ?uri rdfs:label ?l } } }</pre>
	 */
	TAXONOMY("PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>\r\n" + 
			"PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>\r\n" + 
			"SELECT DISTINCT ?uri ?l ?type\r\n" + 
			"WHERE { ?uri skos:inScheme <%1$s> .\r\n" + 
			"        OPTIONAL { ?uri a ?type }\n" + 
			"      { { ?uri skos:prefLabel ?l } \r\n" + 
			"        UNION\r\n" + 
			"        { ?uri rdfs:label ?l } } }"),
	/**
	 * Contexts for thesuarus
	 * 
	 * <pre>PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>
			PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>
			PREFIX wns: <http://www.w3.org/2006/03/wn/wn20/schema/>
			PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
			SELECT  ?uri ?l ?gloss ?synsetId ?type
			WHERE {       ?uri skos:inScheme <%1$s>      ;
												 rdf:type      ?type       .
			   { { ?uri rdfs:label ?l } UNION { ?uri wns:lexicalForm ?l } } .
			   OPTIONAL { ?uri wns:synsetId  ?synsetId } .
			   OPTIONAL { ?uri wns:gloss     ?gloss    } .
			   OPTIONAL { ?synset wns:containsWordSense ?uri; 
			                      wns:gloss             ?gloss; 
			                      wns:synsetId          ?synsetId }  }</pre>
	 */
	THESAURUS("PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>\n" + 
			"PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>\n" + 
			"PREFIX wns: <http://www.w3.org/2006/03/wn/wn20/schema/>\n" + 
			"PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" +
			"SELECT DISTINCT  ?uri ?l ?gloss ?synsetId ?type \n" + 
			"WHERE {       ?uri skos:inScheme <%1$s>      ;\n" +
			"                   rdf:type      ?type       .\n" + 
			"   { { ?uri rdfs:label ?l } UNION { ?uri wns:lexicalForm ?l } } .\n" + 
			"   OPTIONAL { ?uri wns:synsetId  ?synsetId } .\n" + 
			"   OPTIONAL { ?uri wns:gloss     ?gloss    } .\n" + 
			"   OPTIONAL { ?synset wns:containsWordSense ?uri; \n" + 
			"                      wns:gloss             ?gloss; \n" + 
			"                      wns:synsetId          ?synsetId }  }"),
	/**
	 * Contexts for tagging
	 * 
	 * <pre>PREFIX ov: <http://www.openvocabulary.info/ontology/>
			PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>
			PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>
			SELECT DISTINCT ?uri ?l
			WHERE { ?uri skos:inScheme  <%1$s> ;
			"        OPTIONAL { ?uri a ?type }\n" + 
			        { { ?uri rdfs:label ?l } 
			          UNION 
			          { ?uri skos:prefLabel ?l } 
			         } }</pre>
	 */
	TAGGING("PREFIX ov: <http://www.openvocabulary.info/ontology/>\n" + 
			"PREFIX skos:  <http://www.w3.org/2004/02/skos/core#>\n" + 
			"PREFIX rdfs:  <http://www.w3.org/2000/01/rdf-schema#>\n" + 
			"SELECT DISTINCT ?uri ?l ?type\n" + 
			"WHERE { ?uri skos:inScheme  <%1$s> ." +
			"        OPTIONAL { ?uri a ?type }\n" + 
			"        { { ?uri rdfs:label ?l } \n" + 
			"          UNION \n" + 
			"          { ?uri skos:prefLabel ?l } \n" + 
			"         } }");
	
	/**
	 * SPARQL query used for indexing
	 */
	private String indexQuery_;
	
	/**
	 * @param indexQuery_
	 */
	private ContextTypes(String indexQuery){
		this.indexQuery_ = indexQuery;
	}
	
	public String getIndexQuery() {
		return this.indexQuery_;
	}
}
