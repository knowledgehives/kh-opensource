/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.model.wordnet;

import org.ontoware.rdf2go.model.node.URI;

import com.kh.ov.entities.wordnet.SynsetEntity;
import com.kh.ov.entities.wordnet.WordEntity;
import com.kh.ov.entities.wordnet.WordSenseEntity;
import com.kh.ov.model.ThesaurusEntry;
import com.kh.ov.persistance.Repository;

/**
 * @author sebastiankruk
 * List of concept types defined in WordNet 
 */
public enum WordNetTypes {

	WT_Synset("http://www.w3.org/2006/03/wn/wn20/schema/Synset", SynsetEntity.class),
	WT_Word("http://www.w3.org/2006/03/wn/wn20/schema/Word", WordEntity.class),
	WT_WordSense("http://www.w3.org/2006/03/wn/wn20/schema/WordSense", WordSenseEntity.class),
	WT_AdjectiveSynset("http://www.w3.org/2006/03/wn/wn20/schema/AdjectiveSynset", SynsetEntity.class),
	WT_AdjectiveSatelliteSynset("http://www.w3.org/2006/03/wn/wn20/schema/AdjectiveSatelliteSynset", SynsetEntity.class),
	WT_AdverbSynset("http://www.w3.org/2006/03/wn/wn20/schema/AdverbSynset", SynsetEntity.class),
	WT_NounSynset("http://www.w3.org/2006/03/wn/wn20/schema/NounSynset", SynsetEntity.class),
	WT_VerbSynset("http://www.w3.org/2006/03/wn/wn20/schema/VerbSynset", SynsetEntity.class),
	WT_Collocation("http://www.w3.org/2006/03/wn/wn20/schema/Collocation", WordEntity.class),
	WT_AdjectiveWordSense("http://www.w3.org/2006/03/wn/wn20/schema/AdjectiveWordSense", WordSenseEntity.class),
	WT_AdjectiveSatelliteWordSense("http://www.w3.org/2006/03/wn/wn20/schema/AdjectiveSatelliteWordSense", WordSenseEntity.class),
	WT_AdverbWordSense("http://www.w3.org/2006/03/wn/wn20/schema/AdverbWordSense", WordSenseEntity.class),
	WT_NounWordSense("http://www.w3.org/2006/03/wn/wn20/schema/NounWordSense", WordSenseEntity.class),
	WT_VerbWordSense("http://www.w3.org/2006/03/wn/wn20/schema/VerbWordSense", WordSenseEntity.class);
			
		
	protected URI uri_;
	protected Class<? extends ThesaurusEntry> klass_;
			
	WordNetTypes(String uri, Class<? extends ThesaurusEntry> klass){
		this.uri_ = Repository.getInstance().getModel().createURI(uri);
		this.klass_ = klass;
	}

	public URI getUri() {
		return uri_;
	}
	
	
	public Class<? extends ThesaurusEntry> getKlass() {
		return klass_;
	}

	/**
	 * returns wordnet type by URI
	 * @param uri
	 * @return
	 */
	public static WordNetTypes get(URI uri) {
		WordNetTypes result = null;
		
		for(WordNetTypes r : values())
			if(r.getUri().equals(uri)) {
				result = r;
				break;
			}
		
		return result;
	}
	
	
}
