/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.model;

import java.util.Collection;
import java.util.List;



/**
 * This interface defines basic concepts that will allow to access different taxonomies in nomalzed way.
 * 
 * 
 * @author skruk
 * @created 17/08/2006
 */
public interface TaxonomyEntry extends Entry {

	/**
	 * Returns entry in the taxonomy that is parent in the tree to this one
	 * 
	 * @return Parent taxonomy entry
	 */
	public Entry getParent();
	
	
	/**
	 * Returns array of entries that are children of this one in the tree
	 * 
	 * @return Array of children entries
	 */
	public List<TaxonomyEntry> getSubEntries();
	
	
	
	/**
	 * Returns path from Root to this entry
	 * 
	 * @return path from Root to this entry as a array of TaxonomyEntry objects
	 */
	public List<TaxonomyEntry> getPathFromRoot();
	
	
	
	/**
	 * Return all entries below this entry
	 * 
	 * @return all entries below this entry as a collection of Taxonomy objects
	 */
	public Collection<TaxonomyEntry> getDescendents();
	
	
	/**
	 * Adds new descendant element to the list
	 * 
	 * @param _desc new descendand element
	 */
	public void addSubEntry(TaxonomyEntry _desc);
	
	
	/**
	 * Adds a list of new descendat elements to the list
	 * 
	 * @param _desc a list of new descendat elements
	 */
	public void setSubEntries(List<? extends TaxonomyEntry> _desc);
	
	
	/**
	 * Sets parent element
	 * 
	 * @param _parent parent element
	 */
	public void setParent(TaxonomyEntry _parent);
	
	
	/**
	 * Lists resources by given additional property (URI)
	 * 
	 * @param _additionalProperty additional property in this taxonomy context
	 * @return collection of resources linked from this one by given additional property
	 * @throws UnsupportedAdditionalPropertyException If given property is not registered in given taxonomy context
	 */
	public Collection<? extends TaxonomyEntry> getBy(String[] _additionalProperty)  throws UnsupportedAdditionalPropertyException;
	
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(T)
	 */
	public int compareTo(TaxonomyEntry o);
	
	public boolean isMatch();

	public void setMatch(boolean match);
}
