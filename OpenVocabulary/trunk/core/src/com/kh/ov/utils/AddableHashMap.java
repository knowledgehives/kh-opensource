/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.utils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Implementation of Map interface that holds numerical values and adds values with the same key while inserting 
 * (instead of default replace operation)
 * 
 * @author    Sebastian Ryszard Kruk, Adam Westerski
 */
public class AddableHashMap<L, T extends Number> extends LinkedHashMap<L, T> {

	/**
	 * serial version id of this class
	 */
	private static final long serialVersionUID = 3895847880066618703L;

	/**
	 * @param arg0
	 * @param arg1
	 */
	public AddableHashMap(int arg0, float arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 */
	public AddableHashMap(int arg0) {
		super(arg0);
	}

	/**
	 * 
	 */
	public AddableHashMap() {
		super();
	}

	/**
	 * @param arg0
	 */
	public AddableHashMap(Map<L, T> arg0) {
		super(arg0);
	}
	
	
	
	@SuppressWarnings({ "unchecked" })
	@Override
	public T put(L key, T value) {
		T tmpV = this.get(key);

		if(tmpV != null){
			if(value instanceof Float){
				tmpV = (T)(Float)(tmpV.floatValue() + value.floatValue());
			}else
			if(value instanceof Double){
				tmpV = (T)(Double)(tmpV.doubleValue() + value.doubleValue());
			}else
			if(value instanceof Long){
				tmpV = (T)(Long)(tmpV.longValue() + value.longValue());
			}else
			if(value instanceof Integer){
				tmpV = (T)(Integer)(tmpV.intValue() + value.intValue());
			}else
			if(value instanceof Byte){
				tmpV = (T)(Integer)(tmpV.byteValue() + value.byteValue());
			}else
			if(value instanceof Short){
				tmpV = (T)(Integer)(tmpV.shortValue() + value.shortValue());
			}
		}else{
			tmpV = value;
		}
		
		return super.put(key, tmpV);
	}
	
	@Override
	public void putAll(Map<? extends L, ? extends T> arg0) {
		for(L key : arg0.keySet()){
			this.put(key, arg0.get(key));
		}
	}
	
}
