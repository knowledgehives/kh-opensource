/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author sebastiankruk
 * A tool class for making tags into latin forms
 */
public class Latinize {

	/**
	 * Map of non-latin characters to their latin representations
	 */
	protected static final Map<Character, String> LATINFORMS= new HashMap<Character, String>();

	/**
	 * Ensures that all letters in the _local param are [a-zA-Z0-9_-]
	 * @param _local
	 * @param skipSlash TODO
	 * @return
	 */
	public static String latinize(String _local, boolean skipSlash) {
		StringBuilder sb = new StringBuilder();
		int len = _local.length();
		
		for(int i = 0; i < len; i++) {
			char c = _local.charAt(i);
			if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9'))
				sb.append(c);
			else if (Character.isWhitespace(c) || c == '_')
				sb.append("_");
			else if (Character.isLetter(c))
				sb.append(LATINFORMS.get(c));
			else if(skipSlash && c == '/')
				sb.append("/");
			else
				sb.append("-");
		}
		return sb.toString();
	}

	static {
		LATINFORMS.put('\u0105', "aa");
		LATINFORMS.put('\u0119', "ee");
		LATINFORMS.put('\u00F3', "oo");
		LATINFORMS.put('\u015B', "ss");
		LATINFORMS.put('\u0142', "ll");
		LATINFORMS.put('\u017C', "zz");
		LATINFORMS.put('\u017A', "xx");
		LATINFORMS.put('\u0107', "cc");
		LATINFORMS.put('\u0144', "nn");
		LATINFORMS.put('\u0104', "AA");
		LATINFORMS.put('\u0118', "EE");
		LATINFORMS.put('\u00D3', "OO");
		LATINFORMS.put('\u015A', "SS");
		LATINFORMS.put('\u0141', "LL");
		LATINFORMS.put('\u017B', "ZZ");
		LATINFORMS.put('\u0179', "XX");
		LATINFORMS.put('\u0106', "CC");
		LATINFORMS.put('\u0143', "NN");
	}
	
}
