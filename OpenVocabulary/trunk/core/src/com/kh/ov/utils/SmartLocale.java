/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.utils;

import java.util.Locale;


/**
 * Extension of the standard Locale - it accepts one string and decods it into 3 parts
 *
 * 
 * @author Sebastian Ryszard Kruk &lt;sebastian.kruk@deri.org&gt;
 */
public abstract class SmartLocale {

//	REMOVE protected static Map<String, Locale> cacheLocale = new WeakHashMap<String, Locale>();
	
	/**
	 * 
	 */
	@SuppressWarnings("boxing")
	public static Locale createLocale(String def) {
		Locale locale = null;

		if(StringChecker.isNotEmpty(def)) {
			
//			REMOVE 			locale = cacheLocale.get(def);
//			REMOVE 			if(locale == null) {
				String[] asdef = def.split("_");//$NON-NLS-1$
				if(asdef != null) {
					
					if(asdef.length == 1)
						locale = new Locale(asdef[0]);
					else if(asdef.length == 2)
						locale = new Locale(asdef[0], asdef[1]);
					else if(asdef.length == 3)
						locale = new Locale(asdef[0], asdef[1], asdef[2]);
				}// asdef != null

//				REMOVE 				if(locale != null)
//				REMOVE 					cacheLocale.put(def,locale);
//				REMOVE 			}// locale == nul
		}// def !empty
		
		
		return locale;
	}

}
