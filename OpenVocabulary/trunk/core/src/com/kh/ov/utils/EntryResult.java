/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.utils;

import com.kh.ov.model.Entry;

/**
 * @author sebastiankruk
 * This class wraps {@link Entry} and binds it with a {@link Float} value with the randking of the result
 */
public class EntryResult implements Comparable<EntryResult>{

	/**
	 * {@link Entry} of with the value
	 */
	private Entry entry_;
	/**
	 * rank of this value
	 */
	private Float rank_;
	
	/**
	 * the only possible conttructor - creates object with {@link Entry} and ranking set up
	 * @param entry
	 * @param rank
	 */
	public EntryResult(Entry entry, Float rank){
			this.entry_ = entry;
			this.rank_ = (rank != null)?rank:0F;
	}
	
	/**
	 * Compares first by {@link #rank_} than by {@link Entry#getLabel()}, finally by {@link Entry#getURI()}
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(EntryResult o) {
		int res =  this.getRank().compareTo(o.getRank());
		if(res == 0 && this.getEntry() != null && this.getEntry().getLabel() != null && o.getEntry() != null && o.getEntry().getLabel() != null)
			res = this.getEntry().getLabel().compareTo(o.getEntry().getLabel());
		if(res == 0 && this.getEntry() != null && this.getEntry().getURI() != null && o.getEntry() != null  && o.getEntry().getURI() != null)
			res = this.getEntry().getURI().compareTo(o.getEntry().getURI());
		
		return -1*res;
	}
	
	

	public Entry getEntry() {
		return entry_;
	}

	public void setEntry(Entry entry) {
		if(entry != null)
			entry_ = entry;
	}

	public Float getRank() {
		return rank_;
	}

	public void setRank(Float rank) {
		if(rank != null)
			rank_ = rank;
	}
	
	
	
	
}
