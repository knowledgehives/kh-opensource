/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Properties with some smart helping features
 * 
 * @author skruk
 * @date 23/02/2006
 */
public class SmartProperties extends Properties{

	/**
	 * Default logger for this package 
	 */
	private static final Logger logger = LoggerFactory.getLogger(SmartProperties.class);

	/**
	 * generated serial version id 
	 */
	private static final long	serialVersionUID	= -7598766617422698593L;


	public SmartProperties() {
		super();
	}
	
	/**
	 * Generates {@link SmartProperties} based on either file given as a value of the <code>systemProperty</code> or as 
	 * package path given as <code>packagePath</code> (in the latter case you need to provide <code>baseClass</code>)
	 * @param packagePath
	 * @param systemProperty
	 * @param baseClass
	 * @return in the worse case - it will return an empty properties object (never <code>null</code>)
	 */
	@SuppressWarnings("unchecked")
	public static SmartProperties load(String packagePath, String systemProperty, Class baseClass){
		String path = (systemProperty != null) ? System.getProperty(systemProperty) : null;
		SmartProperties prop = null;
		
		if(path != null && !"".equals(path))
			prop = new SmartProperties(new File(path));
		else if(packagePath != null && baseClass != null)
			prop = new SmartProperties(packagePath, baseClass);
		else 
			prop = new SmartProperties();
		
		return prop;
	}
	
	@SuppressWarnings("unchecked")
	public SmartProperties(String _propfile, Class baseClass){
		this();
		InputStream is = baseClass.getClassLoader().getResourceAsStream(_propfile+".properties");//$NON-NLS-1$
		
		if(is != null) {
			try {
				this.load(is);
			  is.close();
			} catch (IOException e) {
				logger.error("Cound not load property file: "+_propfile, e); //$NON-NLS-1$
			}
		}
	}
	
	/**
	 * This version can open external property files
	 * @param _fpropfile
	 */
	public SmartProperties(File _fpropfile) {
		this();
		try {
			InputStream is = new FileInputStream(_fpropfile);
   			this.load(is);
			is.close();
		} catch (IOException e) {
			logger.error("Cound not load property file: "+_fpropfile, e); //$NON-NLS-1$
		}
		
	}
	
	
	/**
	 * Returns list of strings created out of information in properties file
	 * 
	 * e.g.:
	 * <pre>
	 * key_count=2
	 * key_0=value1
	 * key_1=value2
	 * </pre>
	 * 
	 * @param key
	 * @return
	 */
	@SuppressWarnings("boxing")
	public String[] getAsStringArray(String key) throws NoSuchElementException{
		//List<String> result = new ArrayList<String>();
		// -- adding additional properties
		String scount = this.getProperty(key+".count");
		
		if(scount == null)
			throw new NoSuchElementException(key+".count");
		
		Integer iaddprops = Integer.valueOf(scount);
		
		if(iaddprops != null){
			String[] result = new String[iaddprops];
			
			for(int i = 0; i < iaddprops; i++){
				result[i] = this.getProperty(key+"."+i);
				//result.add(addprop);
			}// for

			return result;
		}// --if

		throw new NumberFormatException(key+".count = "+scount);
		
	}
	
	/**
	 * Allows to access array of objects encoded in properties file 
	 * 
	 * @param key
	 * @param subkeys
	 * @return
	 * @throws NoSuchElementException
	 */
	@SuppressWarnings("unchecked")
	public Map<String, String>[] getAsMap(String key, String ... subkeys) throws NoSuchElementException {
		String scount = this.getProperty(key+".count");
		
		if(scount == null)
			throw new NoSuchElementException(key+".count");
		
		Integer iaddprops = Integer.valueOf(scount);
		if(iaddprops != null) {
			Map<String, String>[] result = new Map[iaddprops];
			
			for(int i = 0; i < iaddprops; i++) {
				result[i] = new HashMap<String, String>();
				
				if(subkeys != null)
					for(String subkey : subkeys)
						result[i].put(subkey, this.getProperty(key+"."+i+"."+subkey));
			}
			
			return result;
		}

		throw new NumberFormatException(key+".count = "+scount);
	}

}
