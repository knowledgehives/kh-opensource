/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.utils;

import java.util.Comparator;

import com.kh.ov.model.Entry;

/**
 * @author sebastiankruk
 * Allows to compare two results from the search - so that they are finally sorted according to their rank
 */
public class AlphabeticalResultsComparator implements Comparator<EntryResult> {

	/**
	 * Compares by {@link Entry#getLabel()} and than by {@link Entry#getURI()}
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	public int compare(EntryResult o1, EntryResult o2) {
		int res = (o1 != null && o2 != null)?o1.getEntry().getLabel().compareTo(o2.getEntry().getLabel()) : ( (o1 != null)?1:-1 );
		
		if ( res == 0 && o1 != null && o2 != null)
			res = o1.getEntry().getURI().compareTo(o2.getEntry().getURI());
		
		return res;
	}


}
