/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.utils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

/**
 * @author sebastiankruk
 * Type of possible HTTP Responses supported by the service
 */
public enum ResponseType {

	JSON("application/json",
		 "text/x-json",
		 "text/x-javascript"){
		@Override
		protected boolean accepts(HttpServletRequest request) {
			boolean result = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
			
			if(!result)
				result = _accepts(request);
			
			return result;
		}
	},
	OPENSEARCH("application/rss+xml"){
		@Override
		protected boolean accepts(HttpServletRequest request) {
			return _accepts(request);
		}
	},
	OPENSEARCH_SUGGESTION("application/x-suggestions+json"){
		@Override
		protected boolean accepts(HttpServletRequest request) {
			return _accepts(request);
		}
	},
	HTML{
		@Override
		protected boolean accepts(HttpServletRequest request) {
			return false;
		}
	};
	
	Set<String> types_; 
	
	/**
	 * Mime types of associated with the given HTTP response 
	 * @param types
	 */
	private ResponseType(String ... types){
		this.types_ = new HashSet<String>();
		if(types != null)
			this.types_.addAll(Arrays.asList(types));
	}
	
	/**
	 * Determines if given type can support provided request.
	 * @param request
	 * @return
	 */
	protected abstract boolean accepts(HttpServletRequest request);
	
	/**
	 * To be called by {@link #accepts(HttpServletRequest)} internally
	 * @param request
	 * @return
	 */
	protected boolean _accepts(HttpServletRequest request){
		boolean result = false;
		
		String saccept = request.getHeader("Accept");
		Set<String> accept;
		
		if(saccept != null)
			accept = new HashSet<String>(Arrays.asList(saccept.split(",")));
		else
			accept = new HashSet<String>();
		
		accept.retainAll(this.types_);
		
		result = !accept.isEmpty();
		
		if(!result){
			String contentType = request.getContentType();
			result = this.types_.contains(contentType);
		}
		
		return result;
	}
	
	/**
	 * Allows to find the type of accepted request. 
	 * @param request
	 * @return
	 */
	public static ResponseType determine(HttpServletRequest request){
		for(ResponseType type : values())
			if(type.accepts(request))
				return type;
		return HTML;
	}
	
}
