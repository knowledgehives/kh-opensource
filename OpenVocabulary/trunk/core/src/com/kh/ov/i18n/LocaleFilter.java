/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.i18n;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author  Piotr Piotrowski
 * @version
 */

public class LocaleFilter implements Filter {
    
    public static final String LANG = "lang";
    public static final String LOCALE = "lang.locale";
    public static final String DEFAULT_LANG = "en";

    // The filter configuration object we are associated with.  If
    // this value is null, this filter instance is not currently
    // configured.
    private FilterConfig filterConfig = null;
    
    public LocaleFilter() {
    	//CHECK empty why?
    }
    
    /**
     *
     * @param request The servlet request we are processing
     * @param result The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {
        setLocale((HttpServletRequest) request);
        
        chain.doFilter(request, response);
    }

    /**
     * Do not use this method. It is public for one reason and one reason only.
     * It is used in login.jsp as a hack - no filters are called before it.
     */
    @SuppressWarnings("unchecked")
	public static void setLocale(final HttpServletRequest request) {
        // XXX this method should not be public, but is for use in login.jsp
        HttpSession session = request.getSession();
        String lang = request.getParameter(LANG);
        if (lang != null && !lang.equals("")) {
            session.setAttribute(LANG, lang.split("_")[0]);
            session.setAttribute(LOCALE, getLocale(lang));
        } else if (session.getAttribute(LANG) == null && request.getHeader("accept-language") != null) {
            Locale pref = getPrefferedLocale(request.getLocales());
            
            if (pref != null) {
                session.setAttribute(LANG, pref.getLanguage().toString());
                session.setAttribute(LOCALE, pref);
            } else {
                session.setAttribute(LANG, request.getLocale().getLanguage().toString());
                session.setAttribute(LOCALE, request.getLocale());
            }
        } else if (session.getAttribute(LANG) == null) {
            session.setAttribute(LANG, getDefaultLocale());
            session.setAttribute(LOCALE, getLocale(getDefaultLocale()));
        }
    }
    
    /**
     * Iterates through the given Enumeration of Locale objects.
     * Returns the first Locale objects which lang part is known to the system.
     * If no such Locale is found null is returned.
     */
    private static Locale getPrefferedLocale(Enumeration<Locale> locales) {
        while (locales.hasMoreElements()) {
            Locale locale = locales.nextElement();
            if (Langs.isLangKnown(locale.getLanguage())) {
                return locale;
            }
        }
        return null;
    }
    
    /**
     * Returns a Locale object based on the given locale name.
     * The locale name is in the format returned by Locale.toString().
     */
    private static Locale getLocale(String lang) {
        String[] tmp = lang.split("_", 3);
        if (tmp.length == 3) {
            return new Locale(tmp[0], tmp[1], tmp[2]);
        } else if (tmp.length == 2) {
            return new Locale(tmp[0], tmp[1]);
        } else {
            return new Locale(tmp[0]);
        }
    }
    
    private static synchronized String getDefaultLocale() {
    	return DEFAULT_LANG;
    }
    
    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }
    
    /**
     * Set the filter configuration object for this filter.
     *
     * @param _filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig _filterConfig) {
        this.filterConfig = _filterConfig;
    }
    
    /**
     * Destroy method for this filter
     *
     */
    public void destroy() {
    	//CHECK empty why?
    }
    
    /**
     * Init method for this filter
     *
     */
    public void init(FilterConfig _filterConfig) {
        this.filterConfig = _filterConfig;
    }
    
    /**
     * Return a String representation of this object.
     */
    @Override
	public String toString() {
        
        if (filterConfig == null) return ("LocaleFilter()");
        StringBuffer sb = new StringBuffer("LocaleFilter(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());
        
    }
    
}
