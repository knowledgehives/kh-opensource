/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.entities;

import java.util.Collection;

import org.ontoware.rdf2go.model.ModelSet;
import org.ontoware.rdf2go.model.node.URI;

import com.kh.ov.model.Concept;
import com.kh.ov.model.Context;
import com.kh.ov.model.ThesaurusEntry;
import com.kh.ov.persistance.Concepts;

/**
 * @author sebastiankruk
 * Represents the high level concept in the open thesaurus ontology. 
 * It defines a thesaurus entry for given word or phrase; however, 
 * precise meanings of this entry have to be resolved by checking <code>meanings</code> attribute 
 */
public class ConceptEntity extends ThesaurusEntryEntity implements Concept  {

	/**
	 * Inits the object by setting <code>meanings</code> attribute to <code>null</code>, 
	 * and by retriving the <code>label_</code> for the resource (through invocation of super constructors, especially {@link ThesaurusEntryEntity})
	 * @param _context
	 * @param _uri
	 */
	public ConceptEntity(Context _context, URI _uri, ModelSet _model) {
		super(_context, _uri, _model);
	}

	/* (non-Javadoc)
	 * @see com.kh.ov.entities.Concept#getMeanings()
	 */
	public Collection<ThesaurusEntry> getMeanings() {
		return this.get("meaning", Concepts.HASMEANING.get(), false);
	}
	
}
