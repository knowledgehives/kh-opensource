/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.entities.wordnet;

import java.util.Collection;

import org.ontoware.rdf2go.model.ModelSet;
import org.ontoware.rdf2go.model.node.URI;

import com.kh.ov.entities.ThesaurusEntryEntity;
import com.kh.ov.model.ThesaurusEntry;
import com.kh.ov.model.wordnet.Word;
import com.kh.ov.persistance.Concepts;

/**
 * @author sebastiankruk
 *
 */
public class WordEntity extends ThesaurusEntryEntity implements Word  {

//	<http://www.w3.org/2006/03/wn/wn20/instances/word-sign_of_the_zodiac>
//	#? http://www.w3.org/1999/02/22-rdf-syntax-ns#type <http://www.w3.org/2006/03/wn/wn20/schema/Collocation> ;
//	http://www.w3.org/1999/02/22-rdf-syntax-ns#type <http://www.w3.org/2006/03/wn/wn20/schema/WordEntity> ;
//	http://www.w3.org/2006/03/wn/wn20/schema/lexicalForm "adversarz"@pl-pl .				
	
	/**
	 * value for <code>http://www.w3.org/2006/03/wn/wn20/schema/lexicalForm</code> property
	 */
	protected String lexicalForm_;
	
	
	public WordEntity(URI _uri, ModelSet _model) {
		super(_uri, _model);
		this.lexicalForm_ = this.context.getStorage().getLiteral(this, Concepts.WN_LEXICALFORM);
	}


	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.Word#getLexicalForm()
	 */
	public String getLexicalForm() {
		return lexicalForm_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.Word#getInWordSenses()
	 */
	public Collection<ThesaurusEntry> getInWordSenses() {
		return this.get("inWordSense", Concepts.WN_WORD.get(), true);
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.Word#getLabel()
	 */
	@Override
	public String getLabel() {
		return (this.lexicalForm_ != null)?this.lexicalForm_:super.getLabel();
	}
	
}
