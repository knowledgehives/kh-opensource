/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.entities;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.store.LockObtainFailedException;
import org.corrib.rdf2go.KeyValue;
import org.corrib.rdf2go.Lang;
import org.corrib.rdf2go.ModelSetWrapper;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.Variable;
import org.ontoware.rdf2go.model.node.impl.URIImpl;
import org.ontoware.rdf2go.vocabulary.RDF;
import org.ontoware.rdf2go.vocabulary.RDFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kh.ov.model.Context;
import com.kh.ov.model.ContextTypes;
import com.kh.ov.model.Entry;
import com.kh.ov.persistance.Concepts;
import com.kh.ov.persistance.FulltextIndex;
import com.kh.ov.persistance.Storage;
import com.kh.ov.utils.EntryResult;

public class ContextEntity implements Context {

	/**
	 * Label/name of the taxonomy
	 */
	protected String label;
	/**
	 * Description of the taxonomy
	 */
	protected String description;
	/**
	 * Based uri of the taxonomy
	 */
	protected URI namespaceUri;
	/**
	 * Abbreviation of the namespace
	 */
	protected String namespaceAbbr;
	/**
	 * (default) Locale of this Context
	 */
	protected String locale;
	/**
	 * RDF storage used in this context 
	 */
	protected Storage storage;
	/**
	 * URI of the context
	 */
	protected URI uri_;
	/**
	 * Type of the context
	 */
	protected ContextTypes type_;
	/**
	 * Additional properties that are accessible in this taxonomy
	 */
	protected Set<String> sAdditionalProps = null;
	/**
	 * Tree properties that are accessible in this taxonomy
	 */
	protected Set<String> sTreeProps = null;
	/**
	 * List of root elements of the taxonomy
	 */
	protected SortedSet<Entry> lstRoots = new TreeSet<Entry>();
	/**
	 * Cache of the additional properties 
	 */
	protected String[] asAdditionalPropsCache = null;
	/**
	 * Pattern used to create new Tags
	 */
	protected String urlPattern_;

	
	
	
	/**
	 * Default logger for KOS in JOnto
	 */
	static final Logger logger = LoggerFactory.getLogger(ContextEntity.class);

	public ContextEntity(URI uri){
		this("dummy_"+uri, "Dummy KOS Context ("+uri+")", uri.toString(), "dummy"+new Date().getTime(), null, ContextTypes.TAGGING, null);
	}
	
	/**
	 * 
	 */
	public ContextEntity(String _label, 
						 String _description, 
						 String _namespaceURI, 
						 String _namespaceAbbr,
						 String _locale,
						 ContextTypes type,
						 Storage _storage) {
		this.label = _label;
		this.description = _description;
		this.namespaceAbbr = _namespaceAbbr;
		this.namespaceUri = new URIImpl(_namespaceURI);
		this.storage = ( _storage != null ) ? _storage : Storage.getInstance();
		this.locale = (_locale == null)?"en":_locale;
		this.uri_ = new URIImpl(_namespaceURI);
		this.sTreeProps = new HashSet<String>();
		this.type_ = type;
	}
	
	public String getLabel() {
		return this.label;
	}

	public String getDescription() {
		return this.description;
	}

	public URI getNamespaceURI() {
		return this.namespaceUri;
	}

	public String getNamespaceAbbr() {
		return this.namespaceAbbr;
	}

	public String getLocale() {
		return locale;
	}

	public Storage getStorage() {
		return storage;
	}

	public URI getURI() {
		return uri_;
	}
	public ContextTypes getType() {
		return this.type_;
	}
	
	public String getUrlPattern() {
		return this.urlPattern_;
	}
	public void setUrlPattern(String urlPattern) {
		this.urlPattern_ = urlPattern;
	}
	

	//============================== operations ===============================
	
	
	/**
	 * Used to easily convert from generic to specific type of map in the context of given {@link Context}
	 * @param cte
	 * @return
	 */
	protected  SortedSet<EntryResult> convert(Map<Entry, Float> cte){
		SortedSet<EntryResult> result = new TreeSet<EntryResult>();
		for(Map.Entry<? extends Entry, Float> ce : cte.entrySet())
			result.add(new EntryResult(ce.getKey(), ce.getValue()));
		return result;
	}

	/**
	 * Adds new additional properties to the internal set
	 * 
	 * @param _prop new additional properties
	 */
	public void addAdditionalProperties(String ... props){
		synchronized(this){
			if(this.sAdditionalProps == null){
				this.sAdditionalProps = new HashSet<String>();
			}
			if(props != null){
				if(props.length == 1)
					this.sAdditionalProps.add(props[0]);
				else
					this.sAdditionalProps.addAll(Arrays.asList(props));
			}
		}
	}

	public String[] getAdditionalProperties() {
		if (this.sAdditionalProps == null)
		{
			this.sAdditionalProps = new HashSet<String>(); 
		}
		if (this.asAdditionalPropsCache == null)
		{
			this.asAdditionalPropsCache = this.sAdditionalProps.toArray(new String[this.sAdditionalProps.size()]); 
		}
		return this.asAdditionalPropsCache;
	}
	
	
	/**
	 * Adds new root element to the list
	 * 
	 * @param _root new root element to be added to the list
	 */
	public void addRoot(Entry _root){
		this.lstRoots.add(_root);
	}
	public SortedSet<Entry> getRoots() {
		return this.lstRoots;
	}
	public String[] getTreeProperties() {
		return this.sTreeProps.toArray(new String[this.sTreeProps.size()]);
	}
	
	/**
	 * Adds tree properties
	 * 
	 * @param _treeProps
	 */
	public void addTreeProperties(String ... _treeProps){
		if(_treeProps != null && _treeProps.length == 1)
			this.sTreeProps.add(_treeProps[0]);
		else
			this.sTreeProps.addAll(Arrays.asList(_treeProps));
	}
	
	
	/**
	 * This method should be called to initiate reindexing of complete content of the KOS context
	 * @throws IOException 
	 * @throws LockObtainFailedException 
	 * @throws CorruptIndexException 
	 */
	public void reindex(boolean cleanIndex) throws CorruptIndexException, LockObtainFailedException, IOException {
		Map<String, String[]> map = new HashMap	<String, String[]>();
		String inScheme = this.getURI().toString();
		map.put("inSchemeLabel", new String[]{ this.getLabel() });
		IndexWriter indexWriter = new IndexWriter(FulltextIndex.getInstance().getDirectory(), 
												  FulltextIndex.getInstance().getAnalyzer());

		logger.info("Preparing to reindex thesaurus "+this.namespaceAbbr);

		ModelSetWrapper modelSet = new ModelSetWrapper(getStorage().getModel());
		Collection<URI> uris = modelSet.findSubjectsAsURIs(Variable.ANY, Concepts.SKOS_INSCHEME.get(), namespaceUri);
		
		logger.info("Reindexing thesaurus "+this.namespaceAbbr);
		for (URI uri : uris) {
			try {
				KeyValue<String, Locale> literal = modelSet.findObjectAsStringAndLocale(Variable.ANY, uri, RDFS.label);
				if (literal == null) {
					literal = modelSet.findObjectAsStringAndLocale(Variable.ANY, uri, Concepts.SKOS_PREFLABEL.get());
				}
				if (literal == null) {
					literal = modelSet.findObjectAsStringAndLocale(Variable.ANY, uri, Concepts.WN_LEXICALFORM.get());
				}
				if (literal == null) {
					throw new RuntimeException(uri + " has no label");
				}
				
				Collection<String> stypes = new HashSet<String>();
				Collection<URI> types = modelSet.findObjectsAsURIs(Variable.ANY, uri, RDF.type);

				for(URI utype : types){
					String type = utype.toString();
					String stype = null;
					
					if(type != null && type.indexOf('#') > 0)
						stype = type.substring(type.lastIndexOf('#')+1).toLowerCase();
					else if(type != null)
						stype = type.substring(type.lastIndexOf('/')+1).toLowerCase();
					
					if(type != null && type.endsWith("Synset"))
						stypes.add("synset");
					
					stypes.add(stype);
				}

				String gloss = null;
				if(this.getType() == ContextTypes.THESAURUS){
					stypes.add("thesaurusconcept");
					
					String synsetId = modelSet.findObjectAsString(Variable.ANY, uri, Concepts.WN_SYNSETID.get());
					gloss = modelSet.findObjectAsString(Variable.ANY, uri, Concepts.WN_GLOSS.get());
					
					if (literal.getValue() == null) {
						logger.error("Un-tagged label?: " + uri);
					}
					
					if(literal.getValue() != null)
						map.put("lang", new String[] {new Lang(literal.getValue()).toString()});
					if(gloss != null)
						map.put("gloss", new String[] {gloss});
					if(synsetId != null)
						map.put("synsetId", new String[] {synsetId});
				}
				
				FulltextIndex.getInstance().add(uri, 
												Collections.singleton(literal.getKey()), 
												literal.getKey() + ((gloss != null)?" "+gloss:""), 
												stypes, 
												map, !cleanIndex, inScheme, indexWriter);
				
			} catch (Exception e) {
				logger.warn("Problem with reindexing WordNet", e);
			} finally {
				map.remove("gloss");
				map.remove("synsetId");
			}
			
		}
		
		indexWriter.optimize();
		indexWriter.close();
	}

	
	
	//========================== statics =========================
	
	
	/**
	 * Finds concepts by label_ values using full text index
	 * @param _label
	 * @return
	 */
	public Collection<Entry> listMatchingByLabel(String _label) {
		return this.storage.search(this, "label:"+_label, null).keySet();
	}
	/**
	 * Added for filtering function
	 */
	public Collection<Entry> listMatchingByLabelAndContext(String _label) {
		return this.storage.searchByContentAndContext(this, "label:"+_label, null).keySet();
	}

	
}