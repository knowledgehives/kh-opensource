/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.entities;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ontoware.rdf2go.model.ModelSet;
import org.ontoware.rdf2go.model.node.URI;

import com.kh.ov.model.Context;
import com.kh.ov.model.TaxonomyEntry;
import com.kh.ov.model.UnsupportedAdditionalPropertyException;

/**
 * Proxy implementation of taxonomy entry 
 * 
 * @author skruk
 *
 */
@SuppressWarnings("unchecked")
public class TaxonomyEntryEntity extends EntryEntity implements TaxonomyEntry {

	/**
	 * Cache of super entry object
	 */
	SoftReference<TaxonomyEntry> superEntry = null;
	/**
	 * Cache of the list of subentries
	 */
	SoftReference<List<TaxonomyEntry>> subEntires = null;
	
	/**
	 * Cache of the map of values for additional properties
	 */
	Map<String, SoftReference<List<TaxonomyEntry>>> mapAdditionalProperties = new HashMap<String, SoftReference<List<TaxonomyEntry>>>();
	
	protected boolean match = false;
	
	/**
	 * @param _context
	 * @param _uri
	 */
	public TaxonomyEntryEntity(Context _context, URI _uri, ModelSet _model) {
		super(_context, _uri, _model);
	}

	
	

	/* (non-Javadoc)
	 * @see com.kh.ov.model#addSubEntry(com.kh.ov.model)
	 */
	public void addSubEntry(TaxonomyEntry _desc) {
		synchronized(this){
			List<TaxonomyEntry> children = this.getSubEntries();

			if(!children.contains(_desc)){
				// -- add to sub-entries
				children.add(_desc);
				// -- sync with model
				this.context.getStorage().addChild(this, _desc);
			}
		}
	}
	
	
	/* (non-Javadoc)
	 * @see com.kh.ov.model#addSubEntries(java.util.List)
	 */
	public void setSubEntries(List<? extends TaxonomyEntry> _desc) {
		List<TaxonomyEntry> children = null;
		
		synchronized(this){
			if(this.subEntires == null || this.subEntires.get() == null){
				children = new ArrayList<TaxonomyEntry>();
				this.subEntires = new SoftReference<List<TaxonomyEntry>>(children);
			}else{
				children = this.subEntires.get();
			}
			
			
			children.addAll(_desc);
		}
	}


	/* (non-Javadoc)
	 * @see com.kh.ov.model#getDescendents()
	 */
	public Collection<TaxonomyEntry> getDescendents() {
		// --- calculate complete list
		synchronized(this){
			List<TaxonomyEntry> result = this.getSubEntries();
	
			for(TaxonomyEntry entry : this.getSubEntries()){
				result.addAll(entry.getDescendents());
				result.add(entry);
			}
			
			return result;
		}
	}


	/* (non-Javadoc)
	 * @see com.kh.ov.model#getParent()
	 */
	public TaxonomyEntry getParent() {
		synchronized(this){
			TaxonomyEntry result = null;
			
			if(this.superEntry == null || this.superEntry.get() == null){
				result = this.context.getStorage().getParent(this);
				this.superEntry = new SoftReference<TaxonomyEntry>(result);
			}else{
				result = this.superEntry.get();
			}
			
			return result;
		}	}



	/* (non-Javadoc)
	 * @see com.kh.ov.model#getPathFromRoot()
	 */
	public List<TaxonomyEntry> getPathFromRoot() {
		return this.createPathFromRoot(new ArrayList<TaxonomyEntry>());
	}
	
	
	/**
	 * Implementes path creation - based on recurrence calls
	 * @param _path initial path 
	 * @return _path till current element
	 */
	protected List<TaxonomyEntry> createPathFromRoot(List<TaxonomyEntry> _path){
		assert _path != null;
		
		if(this.getParent() != null){
			_path.addAll(this.getParent().getPathFromRoot());
		}
		
		_path.add(this);
		
		return _path;
	}


	/* (non-Javadoc)
	 * @see com.kh.ov.model#getSubEntries()
	 */
	public List<TaxonomyEntry> getSubEntries() {
		// -- synchronize with repository
		List<TaxonomyEntry> children = null;
		
		synchronized(this){
			if(this.subEntires == null || this.subEntires.get() == null){
				children = this.context.getStorage().syncChildren(this);
			}else{
				children = this.subEntires.get();
			}
		}
		
		return children;
	}

	/* (non-Javadoc)
	 * @see com.kh.ov.model#setParent(com.kh.ov.model)
	 */
	public void setParent(TaxonomyEntry _parent) {
		synchronized(this){
			this.superEntry = new SoftReference<TaxonomyEntry>(_parent);
			this.context.getStorage().addChild(_parent, this);
		}
	}


	public boolean isMatch() {
		return match;
	}

	public void setMatch(boolean _match) {
		this.match = _match;
	}



	/* (non-Javadoc)
	 * @see com.kh.ov.model#getBy(java.lang.String[])
	 */
	public Collection<? extends TaxonomyEntry> getBy(String[] _additionalProperty)  throws UnsupportedAdditionalPropertyException {
		Collection<String> lstProps = Arrays.asList(this.context.getAdditionalProperties());
		
		for(String prop : _additionalProperty){
			if(!lstProps.contains(prop)){
				throw new UnsupportedAdditionalPropertyException(prop);
			}
		}
		
		return (Collection<? extends TaxonomyEntry>) this.context.getStorage().listBy(this, _additionalProperty);
	}
}
