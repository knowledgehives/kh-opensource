/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.entities;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

import org.json.jdk5.simple.JSONArray;
import org.json.jdk5.simple.JSONObject;
import org.ontoware.aifbcommons.collection.ClosableIterable;
import org.ontoware.aifbcommons.collection.ClosableIterator;
import org.ontoware.rdf2go.RDF2Go;
import org.ontoware.rdf2go.model.ModelSet;
import org.ontoware.rdf2go.model.Statement;
import org.ontoware.rdf2go.model.Syntax;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.vocabulary.RDFS;

import com.kh.ov.managers.ContextsManager;
import com.kh.ov.model.Context;
import com.kh.ov.model.Entry;
import com.kh.ov.model.TaxonomyEntry;
import com.kh.ov.model.UnsupportedAdditionalPropertyException;
import com.kh.ov.persistance.Concepts;
import com.kh.ov.persistance.Repository;
import com.kh.ov.persistance.Storage;

public class EntryEntity implements Entry {

	/**
	 * The context of this entry
	 */
	protected Context context;
	/**
	 * Cache of the label_
	 */
	protected String label_;
	/**
	 * Cache of the description
	 */
	protected String description;
	/**
	 * 
	 */
	protected URI uri;
	/**
	 * Storage kept for reference 
	 */
	protected Storage storage;
	
	/**
	 * This constructor will attempt to figure out the context information based on the skos:inScheme information
	 * @param _uri
	 */
	public EntryEntity(URI _uri, ModelSet _model){
		this(ContextsManager.findInWhichContext(_uri, _model), _uri, _model);
	}

	/**
	 * @param _context
	 * @param _uri
	 */
	public EntryEntity(Context _context, URI _uri, ModelSet _model) {
		this.context = _context;
		this.uri = _uri;
		
		this.storage = (_model != null)?Storage.createStorage(_model):this.context.getStorage();

		this.description = storage.getLiteral(this.uri, RDFS.comment);
		this.label_ = storage.getLiteral(this.uri, RDFS.label);
		
		if(this.label_ == null)
			this.label_ = storage.getLiteral(this, Concepts.SKOS_PREFLABEL);
		
		if(this.description == null)
			this.description = storage.getLiteral(this, Concepts.SKOS_DEFINITION);
	}
	
	
	/**
	 * <pre>
	CONSTRUCT  { 
		?tag ?p ?o .
		?ss ?pp ?tag .
	}
	WHERE      { 
		?tag ?p ?o .
		OPTIONAL { ?ss ?pp ?tag . }
		FILTER ( ?tag = <%1$s> )
	}
	</pre>
	**/
	protected static final String GET_RDF_Q = "CONSTRUCT  {" +
											  "	?tag ?p ?o . \n" +
											  "	?ss ?pp ?tag . \n" +
											  "	} \n" +
											  "WHERE      { \n" +
											  "			   ?tag ?p ?o . \n" +
											  "	OPTIONAL { ?ss ?pp ?tag . } \n" +
											  "	FILTER ( ?tag = <%1$s> ) \n" +
											  "} \n";   
	
	protected static final String GET_RDF_QUERY = "CONSTRUCT  {" +
	  "	<%1$s> ?p ?o . \n" +
	  "	?ss ?pp <%1$s> . \n" +
	  "	} WHERE { <%1$s> ?p ?o . \n" +
	  "OPTIONAL { ?ss ?pp <%1$s> . } } \n";   

	
	
	/* (non-Javadoc)
	 * @see com.kh.openvocabulary.core.kos.KOSEntry#getAsRDF(org.ontoware.rdf2go.model.Model)
	 */
	@Deprecated
	public ClosableIterable<Statement> getAsRDF(ModelSet _model) {
		ModelSet model = (_model != null)?_model:Repository.getInstance().getModel();
		
		return model.queryConstruct(String.format(GET_RDF_Q, this.uri.toString()), "SPARQL");
	}
	
	/**
	 * Renders this entity into RDF with given syntax 
	 * @param _model (can be {@code null})
	 * @param out
	 * @param syntax (can be {@code null})
	 * @throws IOException
	 */
	public void writeRDF(ModelSet _model, Writer out, Syntax syntax) throws IOException
	{
		ModelSet model = (_model != null)?_model:Repository.getInstance().getModel();
        ModelSet temp = RDF2Go.getModelFactory().createModelSet();
        

        ClosableIterator<Statement> it = model.queryConstruct(String.format(GET_RDF_QUERY, this.uri.toString()), "SPARQL").iterator();
		
        temp.open();
        temp.removeAll();
		temp.addAll(it);
		
		try {
			if (syntax != null)
			{
				temp.writeTo(out, syntax);
			}
			else
			{
				temp.writeTo(out);
			}
		} catch (IOException e) {
			throw e;
		} finally {
			it.close();
	        temp.close();
		}
	}


	public Context getContext() {
		return this.context;
	}

	public String getDescription() {
		return this.description;
	}

	public String getLabel() {
		return this.label_;
	}

	public URI getURI() {
		return this.uri;
	}

	@Override
	public String toString() {
		return this.uri.toString();
	}

	public int compareTo(TaxonomyEntry o) {
			if(o == null)
				throw new NullPointerException("Not an object");
	//		if(this.getLabel() != null)
	//			return this.getLabel().compareTo(o.getLabel());
			//changed by mkaczmarek - there are some different entries
			//with the same labels (URIs should be used to recognize them)
			if(this.getURI() != null)
				return this.getURI().compareTo(o.getURI());
			return this.toString().compareTo(o.toString());
		}


	/* (non-Javadoc)
	 * @see com.kh.ov.model#equals(com.kh.ov.model)
	 */
	public boolean equals(Entry obj) {
		return (obj!=null?this.uri.equals(obj.getURI()):false);
	}

	@Override
	public boolean equals(Object obj) {
		try
		{
			return (obj!=null?this.uri.equals(((Entry)obj).getURI()):false);
		}
		catch (ClassCastException e) 
		{
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.kh.ov.model#getBy(java.lang.String)
	 */
	public Collection<? extends Entry> getBy(String _additionalProperty) throws UnsupportedAdditionalPropertyException {
		return this.storage.listBy(this, new String[] {_additionalProperty});
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ov.model#compareTo(K)
	 */
	public int compareTo(Entry o) {
		if(o == null)
			throw new NullPointerException("Not an object");
		if(this.equals(o))
			return 0;
		if(this.getLabel() != null && o.getLabel() != null && this.getLabel().compareTo(o.getLabel()) != 0)
			return this.getLabel().compareTo(o.getLabel());

		if(o.getLabel() == null)
			ContextEntity.logger.warn("The label of "+o+" was null");
			
		return this.toString().compareTo(o.toString());
	}

	/* (non-Javadoc)
	 * @see com.kh.openvocabulary.core.kos.KOSEntry#getLang()
	 */
	public String getLang() {
		return context.getLocale();
	}
	
	@Override
	public int hashCode() {
		return this.uri.hashCode();
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ov.model.Entry#getJSON(boolean)
	 */
	public JSONObject getJSON(boolean nofollow)
	{
		JSONObject object = new JSONObject();
		
		Context context = this.getContext();
		
		object.put("label", this.getLabel());
		object.put("description", this.getDescription());
		object.put("lang", this.getLang());
		object.put("uri", this.getURI().toString());
		object.put("context", context.getURI().toString());
		object.put("contextName", context.getLabel());
		object.put("type", context.getType().toString().toLowerCase());
		
		if (!nofollow)
		{
			if (this instanceof TaxonomyEntry)
			{
				Entry eparent = ((TaxonomyEntry)this).getParent();
				
				if (eparent != null)
				{
					object.put("parent", eparent.getJSON(true));
					
					JSONArray jsonarray = new JSONArray();
					for (TaxonomyEntry pathEl : ((TaxonomyEntry)this).getPathFromRoot())
					{
						jsonarray.add(pathEl.getJSON(true));
					}
					object.put("pathToRoot", jsonarray);
				}
			}
		}
			
		return object;
	}
	
	/**
	 * Wrapps {@link Entry#getBy(String)} for JSP/EL support 
	 * @param entry
	 * @param _additionalProperty
	 * @return
	 * @throws UnsupportedAdditionalPropertyException
	 */
	public static final Collection<? extends Entry> getEntriesRelatedBy(Entry entry, String _additionalProperty) 
		throws UnsupportedAdditionalPropertyException {
		return entry.getBy(_additionalProperty);
	}

	

}