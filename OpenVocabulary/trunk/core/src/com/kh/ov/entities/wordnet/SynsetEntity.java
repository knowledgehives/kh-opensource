/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.entities.wordnet;

import java.util.Collection;
import java.util.regex.Pattern;

import org.ontoware.rdf2go.model.ModelSet;
import org.ontoware.rdf2go.model.node.URI;

import com.kh.ov.entities.ThesaurusEntryEntity;
import com.kh.ov.model.ThesaurusEntry;
import com.kh.ov.model.wordnet.Synset;
import com.kh.ov.persistance.Concepts;

/**
 * @author sebastiankruk
 *
 */
public class SynsetEntity extends ThesaurusEntryEntity implements Synset  {

//	<synset-przeciwnik-nn-1>
//	http://www.w3.org/1999/02/22-rdf-syntax-ns#type <http://www.w3.org/2006/03/wn/wn20/schema/SynsetEntity> ;
//	http://www.w3.org/2006/03/wn/wn20/schema/containsWordSense <http://www.w3.org/2006/03/wn/wn20/instances/wordsense-adwersarz-nn-1> ;
//	http://www.w3.org/2006/03/wn/wn20/schema/containsWordSense <http://www.w3.org/2006/03/wn/wn20/instances/wordsense-antagonista-nn-11> ;
//	http://www.w3.org/2006/03/wn/wn20/schema/containsWordSense <http://www.w3.org/2006/03/wn/wn20/instances/wordsense-oponent-nn-1> ;
//	http://www.w3.org/2006/03/wn/wn20/schema/gloss "adwersarz, antagonista, oponent"@pl-pl ;
//	http://www.w3.org/2006/03/wn/wn20/schema/synsetId "108154326"@en-us ;
//	http://www.w3.org/2000/01/rdf-schema#label "przeciwnik"@pl-pl .
//	#! http://www.w3.org/2006/03/wn/wn20/schema/hyponymOf <http://www.w3.org/2006/03/wn/wn20/instances/wordsense-dysputant-nn-1> ;
//	#! http://www.w3.org/2006/03/wn/wn20/schema/hyponymOf <http://www.w3.org/2006/03/wn/wn20/instances/wordsense-polemista-nn-1> ;
//	#? http://www.w3.org/2006/03/wn/wn20/schema/classifiedByTopic <http://www.w3.org/2006/03/wn/wn20/instances/synset-astrology-noun-1> ;
//	#? http://www.w3.org/2006/03/wn/wn20/schema/partMeronymOf <http://www.w3.org/2006/03/wn/wn20/instances/synset-zodiac-noun-1> ;

	/**
	 * Glossary description of the synset in WordNet
	 */
	protected String gloss_;
	/**
	 * SynsetEntity ID in WordNet
	 */
	protected String synsedId_;
	/**
	 * Identifies POS of this synset
	 */
	protected PartOfSpeech pos;
	
	
	/**
	 * Constructs <code>SynsetEntity</code> object. Initializes {@link gloss_}, and {@link synsedId_}.
	 * @param _context
	 * @param _uri
	 * @param _storage
	 */
	public SynsetEntity(URI _uri, ModelSet _model) {
		super(_uri, _model);
		this.gloss_ = this.context.getStorage().getLiteral(this, Concepts.WN_GLOSS);
		this.synsedId_ = this.context.getStorage().getLiteral(this, Concepts.WN_SYNSETID);
		this.description = this.gloss_;
		this.pos = PartOfSpeech.identifyPOS(_uri, pPOSTypeRegExp, this.context.getStorage());
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.Synset#getGlossaryDescription()
	 */
	public String getGlossaryDescription() {
		return this.gloss_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.Synset#getGloss()
	 */
	public String getGloss(){
		return this.gloss_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.Synset#getWordSenses()
	 */
	public Collection<ThesaurusEntry> getWordSenses() {
		return this.get("containsWordSense", Concepts.WN_CONTAINSWORDSENSE.get(), false);
	}


	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.Synset#getClassifiedBy()
	 */
	public Collection<ThesaurusEntry> getClassifiedBy() {
		return this.get("classifiedBy", Concepts.WN_CLASSIFIEDBY.get(), false);
	}



	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.Synset#getClassifiedByRegion()
	 */
	public Collection<ThesaurusEntry> getClassifiedByRegion() {
		return this.get("classifiedByRegion", Concepts.WN_CLASSIFIEDBYREGION.get(), false);
	}



	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.Synset#getClassifiedByTopic()
	 */
	public Collection<ThesaurusEntry> getClassifiedByTopic() {
		return this.get("classifiedByTopic", Concepts.WN_CLASSIFIEDBYTOPIC.get(), false);
	}



	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.Synset#getClassifiedByUsage()
	 */
	public Collection<ThesaurusEntry> getClassifiedByUsage() {
		return this.get("classifiedByUsage", Concepts.WN_CLASSIFIEDBYUSAGE.get(), false);
	}



	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.Synset#getMeronymOf()
	 */
	public Collection<ThesaurusEntry> getMeronymOf() {
		return this.get("meronymOf", Concepts.WN_MERONYMOF.get(), false);
	}



	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.Synset#getPartMeronymOf()
	 */
	public Collection<ThesaurusEntry> getPartMeronymOf() {
		return this.get("partMeronymOf", Concepts.WN_PARTMERONYMOF.get(), false);
	}



	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.Synset#getSimilarTo()
	 */
	public Collection<ThesaurusEntry> getSimilarTo() {
		return this.get("similarTo", Concepts.WN_SIMILARTO.get(), false);
	}



	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.Synset#getPOS()
	 */
	public PartOfSpeech getPOS() {
		return this.pos;
	}
	
	/**
	 * Regular expression which we will use to detect and extract POS
	 */
	protected static final Pattern pPOSTypeRegExp = Pattern.compile("http[:]//www[.]w3[.]org/2006/03/wn/wn20/schema/(.+)SynsetEntity");
	
}
