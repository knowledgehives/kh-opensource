/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.entities.wordnet;

import java.util.Collection;
import java.util.regex.Pattern;

import org.ontoware.aifbcommons.collection.ClosableIterator;
import org.ontoware.rdf2go.model.ModelSet;
import org.ontoware.rdf2go.model.Statement;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.Variable;

import com.kh.ov.entities.ThesaurusEntryEntity;
import com.kh.ov.managers.EntriesManager;
import com.kh.ov.model.ThesaurusEntry;
import com.kh.ov.model.wordnet.Synset;
import com.kh.ov.model.wordnet.WordSense;
import com.kh.ov.persistance.Concepts;

/**
 * @author sebastiankruk
 * IMplements class handling WordSenseEntity objects in WordNet dictionary
 */
public class WordSenseEntity extends ThesaurusEntryEntity implements WordSense {

//	<http://www.w3.org/2006/03/wn/wn20/instances/wordsense-adwersarz-nn-1>
//	http://www.w3.org/1999/02/22-rdf-syntax-ns#type <http://www.w3.org/2006/03/wn/wn20/schema/WordSenseEntity> ;
//	#? http://www.w3.org/2006/03/wn/wn20/schema/tagCount "0"@en-us ;
//	http://www.w3.org/2006/03/wn/wn20/schema/word <http://www.w3.org/2006/03/wn/wn20/instances/word-sign_of_the_zodiac> ;
//	http://www.w3.org/2000/01/rdf-schema#label "adversarz"@pl-pl .

	/**
	 * the synset in which this word sense is used in WordNet
	 */
	protected Synset inSynset_;
	/**
	 * the tagcount value for word net
	 */
	protected String tagCount;
	/**
	 * Identifies POS of this synset
	 */
	protected PartOfSpeech pos;
	
	
	public WordSenseEntity(URI _uri, ModelSet _model) {
		super(_uri, _model);
		this.tagCount = this.context.getStorage().getLiteral(this, Concepts.WN_TAGCOUNT);
		
		ClosableIterator<Statement> it = this.context.getStorage().getModel().findStatements(Variable.ANY, Variable.ANY, Concepts.WN_CONTAINSWORDSENSE.get(), this.uri);
		if(it.hasNext()) {
			this.inSynset_ = (Synset)EntriesManager.getEntryByURI(it.next().getSubject().asURI(), 
																	this.context, _model);
			this.description = this.inSynset_.getGlossaryDescription();
		}else
			this.inSynset_ = null;
		
		it.close();
		
		this.pos = PartOfSpeech.identifyPOS(_uri, pPOSTypeRegExp, this.context.getStorage());
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.WordSense#getGlossaryDescription()
	 */
	public String getGlossaryDescription() {
		return (this.inSynset_ != null)?this.inSynset_.getGlossaryDescription():"";
	}

	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.WordSense#getInSynset()
	 */
	public Synset getInSynset() {
		return inSynset_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.WordSense#getTagCount()
	 */
	public String getTagCount() {
		return tagCount;
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.WordSense#getWords()
	 */
	public Collection<ThesaurusEntry> getWords() {
		return this.get("word", Concepts.WN_WORD.get(), false);
	}


	/* (non-Javadoc)
	 * @see com.kh.ov.entities.wordnet.WordSense#getPOS()
	 */
	public PartOfSpeech getPOS() {
		return this.pos;
	}

	/**
	 * Regular expression which we will use to detect and extract POS
	 */
	protected static final Pattern pPOSTypeRegExp = Pattern.compile("http[:]//www[.]w3[.]org/2006/03/wn/wn20/schema/(.+)WordSenseEntity");
	
}
