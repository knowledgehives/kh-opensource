/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.entities;

import java.lang.ref.SoftReference;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.ontoware.aifbcommons.collection.ClosableIterator;
import org.ontoware.rdf2go.model.ModelSet;
import org.ontoware.rdf2go.model.QueryRow;
import org.ontoware.rdf2go.model.Statement;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.Variable;
import org.ontoware.rdf2go.vocabulary.RDFS;

import com.kh.ov.managers.ContextsManager;
import com.kh.ov.managers.EntriesManager;
import com.kh.ov.model.Context;
import com.kh.ov.model.Entry;
import com.kh.ov.model.ThesaurusEntry;
import com.kh.ov.persistance.Concepts;
import com.kh.ov.persistance.Storage;

/**
 * @author sebastiankruk
 * This is a proxy implementation of the {@link ThesaurusEntry} interface. 
 * It allows to manage memory more efficiently as only URI and context are kept
 */
public class ThesaurusEntryEntity extends EntryEntity implements ThesaurusEntry {

	/**
	 * Cached reference to the list of various *nyms
	 */
	protected Map<String, SoftReference<Collection<ThesaurusEntry>>> refRelations;

	/**
	 * Reference to the model used to communicate with the RDF storage
	 */
	protected ModelSet model;

	
	/**
	 * <pre>PREFIX wns: <http://www.w3.org/2006/03/wn/wn20/schema/>
			SELECT DISTINCT ?synonym
			WHERE { ?synset wns:containsWordSense <%1$s> .
			        ?synset wns:containsWordSense ?synonym .
			        FILTER ( ?synonym != <%1$s> ) }</pre>
	 */
	protected static final String GET_SYNSETS = "PREFIX wns: <http://www.w3.org/2006/03/wn/wn20/schema/>\n" + 
			"SELECT DISTINCT ?synonym\n" + 
			"WHERE { ?synset wns:containsWordSense <%1$s> .\n" + 
			"        ?synset wns:containsWordSense ?synonym .\n" + 
			"        FILTER ( ?synonym != <%1$s> ) }";

	
	/**
	 * This constructor will attempt to figure out the context information based on the skos:inScheme information
	 * @param _uri
	 */
	public ThesaurusEntryEntity(URI _uri, ModelSet _model){
		this(ContextsManager.findInWhichContext(_uri, _model), _uri, _model);
	}
	
	/**
	 * @param _context
	 * @param _uri
	 */
	public ThesaurusEntryEntity(Context _context, URI _uri, ModelSet _model) {
		super(_context, _uri, _model);

		Storage _storage = (_model != null)?Storage.createStorage(_model):this.context.getStorage();
		
		this.model = (_model != null)?_model:this.context.getStorage().getModel();
		this.refRelations = new HashMap<String, SoftReference<Collection<ThesaurusEntry>>>();
		this.label_ = _storage.getLiteral(_uri, RDFS.label);
		if(this.label_ == null)
			this.label_ = _storage.getLiteral(_uri, Concepts.WN_LEXICALFORM.get());
		this.description = "";
	}

	/* (non-Javadoc)
	 * @see com.kh.openvocabulary.core.thesaurus.ThesaurusEntry#getAntonyms()
	 */
	public Collection<ThesaurusEntry> getAntonyms() {
		Collection<ThesaurusEntry> result = null;
		synchronized(this) {
			result = ((this.refRelations.get("antonyms") != null)?this.refRelations.get("antonyms").get():null);
		}
		
		if(result == null) {
			Set<URI> sRes = new HashSet<URI>();
			ClosableIterator<Statement> res;
			for(res = this.model.findStatements(Variable.ANY, this.uri, Concepts.WN_ANTONYMOF.get(), Variable.ANY);
				res.hasNext(); ) {
				sRes.add(res.next().getSubject().asURI());
			}
			res.close();
			
			for(res = this.model.findStatements(Variable.ANY, Variable.ANY, Concepts.WN_ANTONYMOF.get(), this.uri);
				res.hasNext(); ) {
				sRes.add(res.next().getSubject().asURI());
			}
			res.close();
			
			result = new HashSet<ThesaurusEntry>();
			for(URI uRes : sRes) {
				Entry te = EntriesManager.getEntry(uRes, this.model);
				if(te != null)
					result.add((ThesaurusEntry)te);
			}
			
			result = Collections.unmodifiableSet((Set<ThesaurusEntry>)result);
			
			synchronized(this) {
				this.refRelations.put("antonyms", new SoftReference<Collection<ThesaurusEntry>>(result));
			}
		}
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.kh.openvocabulary.core.thesaurus.ThesaurusEntry#getHypernyms()
	 */
	public Collection<ThesaurusEntry> getHypernyms() {
		return this.get("hypernym", Concepts.WN_HYPONYMOF.get(), false);
	}

	/* (non-Javadoc)
	 * @see com.kh.openvocabulary.core.thesaurus.ThesaurusEntry#getHyponyms()
	 */
	public Collection<ThesaurusEntry> getHyponyms() {
		return this.get("hyponym", Concepts.WN_HYPONYMOF.get(), true);
	}

	/* (non-Javadoc)
	 * @see com.kh.openvocabulary.core.thesaurus.ThesaurusEntry#getSynonyms()
	 */
	public Collection<ThesaurusEntry> getSynonyms() {
		Collection<ThesaurusEntry> result = null;
		synchronized(this) {
			result = (this.refRelations.get("synonym") != null)?this.refRelations.get("synonym").get():null;
		}
		
		if(result == null) {
			Set<URI> sRes = new HashSet<URI>();
			ClosableIterator<Statement> res;
			for(res = this.model.findStatements(Variable.ANY, Variable.ANY, Concepts.SKOS_RELATED.get(), this.uri);
				res.hasNext(); ) {
				sRes.add(res.next().getSubject().asURI());
			}
			res.close();
			
			for(QueryRow row : this.model.querySelect(String.format(GET_SYNSETS, this.uri.toString()), "SPARQL")) 
				sRes.add(row.getValue("synonym").asURI());
			

			result = new HashSet<ThesaurusEntry>();
			for(URI uRes : sRes){
				Entry te = EntriesManager.getEntry(uRes, this.model);
				if(te != null)
					result.add((ThesaurusEntry)te);
			}

			result = Collections.unmodifiableSet((Set<ThesaurusEntry>)result);
			
			synchronized(this) {
				this.refRelations.put("synonym", new SoftReference<Collection<ThesaurusEntry>>(result));
			}
		}
		
		return result;
	}

	/**
	 * A generic method for retriving related resources with given <code>relation</code> of given <code>property</code> URI
	 * @param relation name of the relation
	 * @param property URI of the relation
	 * @param inverted does the relation use an inverted property to the given one?
	 * @return a collection of related {@link ThesaurusEntry}s
	 */
	protected Collection<ThesaurusEntry> get(String relation, URI property, boolean inverted){
		Collection<ThesaurusEntry> result = null;
		synchronized(this) {
			result = (this.refRelations.get(relation) != null)?this.refRelations.get(relation).get():null;
		}
		
		if(result == null) {
			Set<URI> sRes = new HashSet<URI>();
			ClosableIterator<Statement> res;
			for(res = this.model.findStatements(Variable.ANY, (inverted)?Variable.ANY:this.uri, property, (inverted)?this.uri:Variable.ANY);
				res.hasNext(); ) {
				sRes.add(res.next().getSubject().asURI());
			}
			res.close();
	
			result = new HashSet<ThesaurusEntry>();
			for(URI uRes : sRes){
				Entry te = EntriesManager.getEntry(uRes, this.model);
				if(te != null)
					result.add((ThesaurusEntry)te);
			}
	
			result = Collections.unmodifiableSet((Set<ThesaurusEntry>)result);
			
			synchronized(this) {
				this.refRelations.put(relation, new SoftReference<Collection<ThesaurusEntry>>(result));
			}
		}
		
		return result;
	}

	

}
