/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.servlets.lod;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kh.ov.managers.TagsManager;
import com.kh.ov.model.Entry;
import com.kh.vulcan.annotation.Servlet;

/**
 * @author sebastiankruk
 * THis service extends capabilities of LODServlet by allowing to modify tags (POST/PUT)
 */
@Servlet(urlMappings={"/tags/*", 
					  "/json/tags/*",
					  "/html/tags/*",
					  "/rdf/tags/*"})
public class TagsServlet extends LODServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 901922656235344307L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		if ("PUT".equals(req.getParameter("_method")))
		{
			doPut(req, resp);
		}
		else
		{
			super.doPost(req, resp);
		}
	}
	
	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String label = req.getParameter("label");//required
		String lang = req.getParameter("lang");//optional ? TODO can we get it from the session somehow?

		/*
		 * TODO later
		String description = req.getParameter("description");//optional
		String[] asrelated = req.getParameterValues("related"); //optional list of URIs to related terms
		String[] asnarrower = req.getParameterValues("narrower"); //optional list of URIs to narrower terms
		String[] asbroader = req.getParameterValues("broader"); //optional list of URIs to broader terms
		*/
		
		if ( label != null )
		{
			Entry entry = TagsManager.createTag(label, lang, null, null);
			
			ResponseType respType = ResponseType.determine(req);
			if ( req.getRequestURI().startsWith("/tags/") )
			{
				respType.redirect(entry, req, resp);
			}
			else
			{
				respType.send(entry, req, resp);
			}
		}
		else
		{
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Missing required <label> property");
		}
	}
	
}
