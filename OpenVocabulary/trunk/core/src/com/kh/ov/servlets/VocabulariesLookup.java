/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.servlets;

import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ontoware.rdf2go.model.node.impl.URIImpl;

import com.kh.ov.managers.EntriesManager;
import com.kh.ov.model.Entry;
import com.kh.ov.servlets.lod.ResponseType;
import com.kh.vulcan.annotation.Servlet;

/**
 * @author sebastiankruk
 * Allows to lookup a term in the vocabulary
 */
@Servlet(urlMappings = "/vocabularies/lookup")
public class VocabulariesLookup extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6563726979764076302L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String suri = req.getParameter("uri");
		Entry entry = null;
		
		if (suri != null)
		{
			String uri = URLDecoder.decode(suri, "UTF-8");
			
			entry = EntriesManager.getEntryByURI(new URIImpl(uri), null, null);
			
		}
		if (entry != null)
		{
			ResponseType type = ResponseType.determine(req);
			type.send(entry, req, resp);
		}
		else
		{
			super.doGet(req, resp);
		}
	}
}
