/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.servlets.lod;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ontoware.rdf2go.model.node.impl.URIImpl;

import com.kh.ov.Configuration;
import com.kh.ov.managers.EntriesManager;
import com.kh.ov.model.Entry;
import com.kh.vulcan.annotation.Servlet;

/**
 * @author sebastiankruk
 * This servlet allows to browse through the linked vocabulary, 
 * provided the URI of the vocabulary entries points to the OV service
 */
@Servlet(urlMappings={"/taxonomies/*", 
					  "/thesauri/*",
					  "/json/taxonomies/*", 
					  "/json/thesauri/*",
					  "/html/taxonomies/*", 
					  "/html/thesauri/*",
					  "/rdf/taxonomies/*", 
					  "/rdf/thesauri/*"})
public class LODServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5952711010845244123L;

	/**
	 * {@link LODServlet} supports only GET method. The requestURL must match an existing vocabulary item.
	 * TODO deliver support for all-vocabulary information
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		ResponseType type = ResponseType.determine(req);
		String typePre = "/"+type.name().toLowerCase();
		String itemUri = req.getRequestURL().toString().replace(typePre, "");
		
		Entry entry = null;
		
		if (itemUri != null)
		{
			entry = EntriesManager.getEntryByURI(new URIImpl(itemUri), null, null);			

			if (entry == null) //fallback when calling from different domains - this might be required when embedding into other services or testing
			{
				entry = EntriesManager.getEntryByURI(new URIImpl(Configuration.getProperty("com.kh.ov.default.path")+req.getRequestURI().replace(typePre, "")), null, null);
			}
		}
		if (entry != null)
		{
			String uri = req.getRequestURI();
			if (uri.startsWith("/taxonomies/") ||
				uri.startsWith("/thesauri/") ||
				uri.startsWith("/tags/"))
			{
				type.redirect(entry, req, resp); //303 redirect
			}
			else
			{
				type.send(entry, req, resp); //show the content
			}
		}
		else
		{
			resp.sendError(HttpServletResponse.SC_NOT_FOUND, "Could not find given entry");
		}
		
	}
}
