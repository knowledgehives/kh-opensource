/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.servlets;

import java.io.IOException;
import java.util.SortedSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.jdk5.simple.JSONArray;
import org.json.jdk5.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kh.ov.model.Context;
import com.kh.ov.model.Entry;
import com.kh.ov.model.TaxonomyEntry;
import com.kh.ov.persistance.Storage;
import com.kh.ov.utils.EntryResult;
import com.kh.ov.utils.ResponseType;
import com.kh.vulcan.annotation.Servlet;

/**
 * @author sebastiankruk
 * This servlet provides unifies access to all KOS search
 */
@Servlet( urlMappings = "/vocabularies/search" )
public class VocabulariesSearch extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	/**
	 * precooked query to Lucene index - it will look only for key terms with selected label
	 */
	private static final String LOOKUP_TEMPLATE = "label:\"%1$s\" AND (type:tag OR type:taxonomyconcept OR type:synset)";
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String query = req.getParameter("q");
		String sthreshold = req.getParameter("threshold");
		String ssize = req.getParameter("size");
		String label = req.getParameter("label");
		
		Float threshold = null;
		Integer size = null;
		
		//--than check the params
		try
		{
			if (sthreshold != null)
			{
				threshold = Float.valueOf(sthreshold);
			}
		}
		catch (Exception e) 
		{
			threshold = 0.5F;
		}
		try
		{
			if (ssize != null)
			{
				size = Integer.valueOf(ssize);
			}
		}
		catch (Exception e)
		{
			size = null;
		}
		if (query == null) 
		{
			if (label != null) 
			{
				query = String.format(LOOKUP_TEMPLATE, label);
			}
			else
			{
				resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "label or q parameter required");
				return;
			}
			
			
		}// -- if q
		
		
		SortedSet<EntryResult> results = Storage.getInstance().sortedSearch(null, query, size, threshold, null);
		
		switch (ResponseType.determine(req)) {
		case JSON:
			JSONObject result = new JSONObject();
			result.put("label", label);
			result.put("threshold", threshold);
			result.put("size", size);
			result.put("results", getJSON(results));
			result.put("query", query);
			
			String json = result.toString();
			resp.setContentLength(json.getBytes("UTF-8").length);
			resp.setContentType("application/json;charset=UTF-8");
			resp.setHeader("Cache-Control", "no-cache");
			resp.setHeader("Pragma","no-cache"); //HTTP 1.0

			try {
				resp.getWriter().append(json);
				resp.getWriter().flush();
			} catch (IOException e) {
				logger.warn("Problem when sending JSON output", e);
			}
			
			break;
		default:
			req.setAttribute("query", query);
			req.setAttribute("label", label);
			req.setAttribute("threshold", threshold);
			req.setAttribute("size", size);
			req.setAttribute("results", results);
			req.getRequestDispatcher("/kossearch.jsp").forward(req, resp);
			break;
		}
	}
	
	
	/**
	 * Generates results in a form of a JSON Array
	 * @param results
	 * @return
	 */
	protected JSONArray getJSON(SortedSet<EntryResult> results){
		JSONArray array = new JSONArray();
		
		for(EntryResult result : results){
			JSONObject object = new JSONObject();
			Entry entry = result.getEntry();
			
			if (entry == null)
			{
				logger.error("Null entry appeared: "+result);
				continue;
			}
			
			Context context = entry.getContext();
			
			object.put("rank", result.getRank());
			object.put("label", entry.getLabel());
			object.put("description", entry.getDescription());
			object.put("lang", entry.getLang());
			object.put("uri", entry.getURI().toString());
			object.put("context", context.getURI().toString());
			object.put("contextName", context.getLabel());
			object.put("type", context.getType().toString().toLowerCase());
			
			if (entry instanceof TaxonomyEntry)
			{
				Entry eparent = ((TaxonomyEntry)entry).getParent();
				
				if (eparent != null)
				{
					JSONObject parent = new JSONObject();

					parent.put("label", eparent.getLabel());
					parent.put("description", eparent.getDescription());
					parent.put("uri", eparent.getURI().toString());
					
					object.put("parent", parent);
				}
			}
			
			array.add(object);
		}
		
		return array;
	}

	private static final Logger logger = LoggerFactory.getLogger(VocabulariesSearch.class);
}
