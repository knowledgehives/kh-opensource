/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.servlets.lod;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ontoware.rdf2go.model.Syntax;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kh.ov.model.Entry;

/**
 * @author sebastiankruk
 * Type of possible HTTP Responses supported by the LOD service
 */
public enum ResponseType {

	
	/**
	 * handling JSON response
	 */
	JSON("/json/*",
		 "application/json", //$NON-NLS-1$
		 "text/x-json", //$NON-NLS-1$
		 "text/x-javascript"){ //$NON-NLS-1$
		@Override
		protected boolean checkCustomAccept(HttpServletRequest request) {
			boolean result = "XMLHttpRequest".equals(request.getHeader("X-Requested-With")); //$NON-NLS-1$ //$NON-NLS-2$
			
			if(!result)
				result = "true".equals(request.getParameter("json"));
			
			return result;
		}
		
		@Override
		public void send(Entry entry, HttpServletRequest request, HttpServletResponse resp) throws IOException, ServletException {
			String json = entry.getJSON(false).toString();
			
			try { resp.setContentLength(json.getBytes("UTF-8").length); } catch (UnsupportedEncodingException e1) { logger.info(e1.toString()); }

			resp.setContentType("application/json;charset=UTF-8");
			resp.setHeader("Cache-Control", "no-cache");
			resp.setHeader("Pragma","no-cache"); //HTTP 1.0

			try {
				resp.getWriter().append(json);
				resp.getWriter().flush();
			} catch (IOException e) {
				logger.warn("Problem when sending JSON output", e);
			}
		}
		
	},
	/**
	 * Handling RDF response
	 */
	RDF("/rdf/*",
		"text/rdf", //$NON-NLS-1$
		"application/rdf+xml" ){
		@Override
		public boolean checkCustomAccept(HttpServletRequest request) {
			return ( "true".equals(request.getParameter("rdf")) );
		}
		
		@Override
		public void send(Entry entry, HttpServletRequest request, HttpServletResponse resp) throws IOException, ServletException {
			resp.setContentType("application/rdf+xml;charset=UTF-8");
			resp.setHeader("Cache-Control", "no-cache");
			resp.setHeader("Pragma","no-cache"); //HTTP 1.0

			try {
				entry.writeRDF(null, resp.getWriter(), Syntax.RdfXml);
			} catch (IOException e) {
				logger.warn("Problem when sending RDF/XML output", e);
			}
		}

	},
	/**
	 * Handling HTML response
	 */
	HTML("/html/*",
		 "text/html"){
		@Override
		protected boolean checkCustomAccept(HttpServletRequest request) {
			return false;
		}
		
		@Override
		public void send(Entry entry, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
			request.setAttribute("term", entry);
			request.getRequestDispatcher("/ovterm.jsp").forward(request, response);
		}
	};
	
	/**
	 * Accepted mime types
	 */
	Set<String> types_;
	/**
	 * name mime type
	 */
	String mainType_;
	/**
	 * Accepted URI pattern
	 */
	Pattern uriPattern;
	/**
	 * Pattern used to bisect URL
	 */
	static final Pattern urlPattern = Pattern.compile("^(\\w+[:][/]*\\w+(?:[.]\\w+)*(?:[:]\\d+)?[/])(.*)$");
	
	/**
	 * Mime types of associated with the given HTTP response 
	 * @param pattern RegEx pattern for matching URI types
	 * @param types
	 * 
	 */
	private ResponseType(String pattern, String mainType, String ... types)
	{
		this.uriPattern = Pattern.compile(pattern);
		
		this.types_ = new HashSet<String>();
		if (types != null)
		{
			this.types_.addAll(Arrays.asList(types));
		}
		this.types_.add(mainType);
		
		this.mainType_ = mainType;
	}
	
	/**
	 * Determines if given type can support provided request using custom rules for given type.
	 * @param request
	 * @return
	 */
	protected abstract boolean checkCustomAccept(HttpServletRequest request);
	
	
	/**
	 * Determines if given type can support provided request
	 * @param request
	 * @return
	 */
	protected boolean checkAccepts(HttpServletRequest request)
	{
		boolean result = this.uriPattern.matcher(request.getRequestURI()).find();
		
		if ( !result )
			result = this.checkCustomAccept(request);
		
		if ( !result )
			result = this.checkAcceptHeader(request);
		
		return result;
	}
	
	
	/**
	 * Defines a method that should be called to send response to the client
	 * @param entry
	 * @param request TODO
	 * @param response
	 * @throws IOException TODO
	 * @throws ServletException TODO
	 */
	public abstract void send(Entry entry, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException;
	
	public void redirect(Entry entry, HttpServletRequest request, HttpServletResponse response)
	{
		response.setHeader("Location", this.getLocationURL(request.getRequestURL().toString(), entry.getURI().toString()));
		response.setHeader("Content-Type", this.mainType_.concat("; charset=UTF-8"));
		response.setStatus(303);
		try {
			response.flushBuffer();
		} catch (IOException e) {
			logger.warn(e.toString());
		}
	}
	
	
	/**
	 * Prepares an URL for given {@code entry} 
	 * @param entry
	 * @return
	 */
	public String getLocationURL(String calluri, String uri) {
		String[] parts = bisectURL(uri);
		String[] calluriparts = bisectURL(calluri);
		return (parts != null) ? (calluriparts[0] + this.name().toLowerCase() + "/" + parts[1]) : uri;
	}
	
	/**
	 * uses {@link #urlPattern} to bisect given URL 
	 * @param url
	 * @return
	 */
	protected static String[] bisectURL(String url)
	{
		Matcher m = urlPattern.matcher(url);
		
		if (m.find())
		{
			return new String[]{ m.group(1), m.group(2) };
		}
		
		return null;
	}
	
	/**
	 * To be called by {@link #checkCustomAccept(HttpServletRequest)} internally
	 * @param request
	 * @return
	 */
	protected boolean checkAcceptHeader(HttpServletRequest request)
	{
		boolean result = false;
		
		String saccept = request.getHeader("Accept"); //$NON-NLS-1$
		Set<String> accept;
		
		if (saccept != null)
		{
			accept = new HashSet<String>(Arrays.asList(saccept.split(","))); //$NON-NLS-1$
		}
		else
		{
			accept = new HashSet<String>();
		}
		
		accept.retainAll(this.types_);
		
		result = !accept.isEmpty();
		
		if ( !result )
		{
			String contentType = request.getContentType();
			result = this.types_.contains(contentType);
		}
		
		return result;
	}
	
	/**
	 * Allows to find the type of accepted request. 
	 * @param request
	 * @return
	 */
	public static ResponseType determine(HttpServletRequest request)
	{
		for (ResponseType type : values())
		{
			if ( type.checkAccepts(request) )
			{
				return type;
			}
		}
		return HTML;
	}
	
	static final Logger logger = LoggerFactory.getLogger(ResponseType.class);
	
}
