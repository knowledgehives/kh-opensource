/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.managers;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.index.CorruptIndexException;
import org.ontoware.rdf2go.model.ModelSet;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kh.ov.entities.EntryEntity;
import com.kh.ov.model.Context;
import com.kh.ov.model.Entry;
import com.kh.ov.persistance.Concepts;
import com.kh.ov.persistance.FulltextIndex;
import com.kh.ov.persistance.Storage;
import com.kh.ov.utils.Latinize;

/**
 * @author sebastiankruk Allows to manage and create new Tags
 */
public class TagsManager {

	/**
	 * Creates URI for given <code>tag</code>
	 * 
	 * @param tag
	 * @return
	 */
	public static URI createURI(String tag, String lang, Context context) {
		String lang_ = (lang != null) ? lang : context.getLocale();
		return context.getStorage().getModel().createURI(String.format(context.getUrlPattern(), Latinize.latinize(tag, false), lang_));
	}

	/**
	 * Creates URI for given <code>tag</code> but allows to choose custome RDF
	 * model and url pattern (URL pattern should define two placeholders)
	 * 
	 * @param tag
	 * @param lang
	 * @param urlPattern
	 * @param _model
	 * @return
	 */
	public static URI createURI(String tag, String lang, String urlPattern,
			ModelSet _model, Context context) {
		return ((_model != null) ? _model : context.getStorage().getModel())
				.createURI(String.format((urlPattern != null) ? urlPattern
						: context.getUrlPattern(), Latinize
						.latinize(tag, false), lang));
	}

	/**
	 * Returns a tag concept by given URI. Ensures that the label_ is set
	 * properly. This can be later enhanced to provide caching mechanism behind
	 * the scene.
	 * 
	 * @param uri
	 * @return
	 */
	public static Entry getTag(URI uri, Context context, ModelSet model) {
		return EntriesManager.getEntryByURI(uri, context, model);
	}

	/**
	 * Creates (or retrieves existing one) tag with given language
	 * 
	 * @param label
	 * @param lang_
	 *            can be null
	 * @param urlPattern
	 *            can be null
	 * @param _model
	 *            can be null
	 * @return
	 */
	public static Entry createTag(String label, String lang_,
			String urlPattern, ModelSet _model) {
		if (label == null || "".equals(label))
			return null;

		Entry result = null;

		Context tagging = ContextsManager.getTagContext(lang_);
		
		String lang = tagging.getLocale();
		URI uri = (urlPattern != null || _model != null) ? createURI(label,
				lang, urlPattern, _model, tagging) : createURI(label, lang,
				tagging);
		Storage storage = (_model != null) ? Storage.createStorage(_model)
				: tagging.getStorage();

		if (!storage.checkForInstance(uri)) {// --if you did not found a tag
												// in this context - create
												// it
			ModelSet model = storage.getModel();
			model.addStatement(tagging.getNamespaceURI(), uri, RDF.type,
					Concepts.TAG.get());
			model.addStatement(tagging.getNamespaceURI(), uri,
					Concepts.SKOS_PREFLABEL.get(), model
							.createLanguageTagLiteral(label, lang));
			model
					.addStatement(tagging.getNamespaceURI(), uri,
							Concepts.SKOS_INSCHEME.get(), tagging
									.getNamespaceURI());

			// --- index it in the lucene

			Map<String, String[]> map = new HashMap<String, String[]>();

			map.put("lang", new String[] { lang });
			map.put("inSchemeLabel", new String[] { tagging
					.getNamespaceAbbr() });

			try {
				FulltextIndex.getInstance().add(uri, label, label,
						new String[] { "tag", "word" }, map, false,
						tagging.getNamespaceURI().toString(), null); 
			} catch (CorruptIndexException e) {
				logger.error(
						"Problem with indexing a tag - index is corrupted",
						e);
			} catch (IOException e) {
				logger
						.error(
								"Problem with indexing a tag - some I/O problem",
								e);
			}
		}

		result = new EntryEntity(uri, storage.getModel());

		return result;
	}

	/**
	 * 
	 * @param sentries
	 * @return
	 */
	public static Collection<Entry> extractEntries(String sentries, String lang, String pattern, ModelSet model) {
		Collection<Entry> result = new HashSet<Entry>();

		for (String[] akey : explodeEntries(sentries))
			result.add(TagsManager.createTag(akey[1], lang, pattern, model));

		return result;
	}

	/**
	 * Extracts single entries as a collection of {@link String}s
	 * 
	 * @param sentries
	 * @return
	 */
	public static Collection<String[]> explodeEntries(String sentries) {
		Collection<String[]> result = new HashSet<String[]>();
		EntryParsers parser = null;

		if (sentries.indexOf('"') > 0 || sentries.indexOf("'") > 0)
			parser = EntryParsers.QUOTE;
		else if (sentries.contains(","))
			parser = EntryParsers.COMA;
		else
			parser = EntryParsers.SIMPLE;

		//logger.warn(parser.toString());

		Matcher m = parser.getPattern().matcher(sentries);
		String[] aresult = null;

		while (m.find()) {
			switch (parser) {
			case QUOTE:
				String key = m.group(2);
				if (key == null)
					key = m.group(3);
				if (key == null)
					key = m.group(4);
				aresult = new String[] { m.group(1), key };
				break;
			case COMA:
			case SIMPLE:
				aresult = new String[] { m.group(1), m.group(2) };
				break;
			}

			result.add(aresult);
		}

		return result;
	}

	public static final Logger logger = LoggerFactory
			.getLogger(TagsManager.class);

	private static final String ALLOWED_PUNKT = "[.]|[#]|[$]|[-]|[+]"; 
	private static final String ALLOWED_CHAR = "[^\\s\\p{Punct}]";
	
	/**
	 * @author sebastiankruk List of entry parsers used when exploding terms
	 */
	enum EntryParsers {
		/**
		 * {@code ((?:\\w[:])?\\w+)\\b}
		 */
		SIMPLE("(?:("+ALLOWED_CHAR+")[:]\\s*)?((?:"+ALLOWED_CHAR+"|"+ALLOWED_PUNKT+")+\\b(?:"+ALLOWED_PUNKT+")*)"),
		/**
		 * {@code ((?:\\w[:])?+\\w+(?:\\s+(?:\\w[:])?+\\w+)*)(?:[,;]|\\s+|\\z)}
		 */
		COMA(
				"(?:("+ALLOWED_CHAR+")[:]\\s*)?((?:"+ALLOWED_CHAR+"|"+ALLOWED_PUNKT+")+(?:\\s+(?:"+ALLOWED_CHAR+"|"+ALLOWED_PUNKT+")+)*)(?:[,;]|\\s+|\\z)"),
		/**
		 * {@code (\\w[:])?(?:(\\w+)\\b|(?:[\"](\\w+(?:\\s+\\w+)*)[\
		 * "])|(?:['](\\w+(?:\\s+\\w+)*)[']))}
		 */
		QUOTE(
				"(?:("+ALLOWED_CHAR+")[:]\\s*)?(?:((?:"+ALLOWED_CHAR+"|"+ALLOWED_PUNKT+")+)\\b|(?:[\"]((?:"+ALLOWED_CHAR+"|"+ALLOWED_PUNKT+")+(?:\\s+(?:"+ALLOWED_CHAR+"|"+ALLOWED_PUNKT+")+)*)[\"])|(?:[']((?:"+ALLOWED_CHAR+"|"+ALLOWED_PUNKT+")+(?:\\s+(?:"+ALLOWED_CHAR+"|"+ALLOWED_PUNKT+")+)*)[']))");

		private Pattern pattern;
		

		private EntryParsers(String spttern) {
			this.pattern = Pattern.compile(spttern);
		}

		public Pattern getPattern() {
			return pattern;
		}
	}

}
