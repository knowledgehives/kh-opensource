/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.managers;

import java.lang.reflect.Constructor;

import org.ontoware.rdf2go.model.ModelSet;
import org.ontoware.rdf2go.model.node.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kh.ov.entities.ConceptEntity;
import com.kh.ov.entities.EntryEntity;
import com.kh.ov.entities.TaxonomyEntryEntity;
import com.kh.ov.model.Context;
import com.kh.ov.model.ContextTypes;
import com.kh.ov.model.Entry;
import com.kh.ov.model.ThesaurusEntry;
import com.kh.ov.model.wordnet.WordNetTypes;
import com.kh.ov.persistance.Concepts;
import com.kh.ov.persistance.Repository;
import com.kh.ov.persistance.Storage;

/**
 * @author sebastiankruk
 *
 */
public class EntriesManager {

	/**
	 * Static method to get a KOS Entry by given URI
	 * <br/> previously: <code>KOSContext.getEntry(new URIImpl(uri), null);</code>
	 * @param _uri
	 * @param _model
	 * @return
	 */
	public static Entry getEntry(URI _uri, ModelSet _model){
		return getEntryByURI(_uri, ContextsManager.findInWhichContext(_uri, _model), _model);
	}

	/* (non-Javadoc)
	 * @see com.kh.ov.model.Context#getEntryByUri(java.lang.String, org.ontoware.rdf2go.model.Model)
	 */
	public static Entry getEntryByURI(URI uri, Context context, ModelSet _model){
		ModelSet model = (_model != null)?_model:Repository.getInstance().getModel();
		Storage _storage = (_model != null) ? Storage.createStorage(_model) : ((context != null) ?  context.getStorage() : Storage.createStorage(model));
		Entry result = null;

		if(_storage.checkForInstance(uri)) {
			Class<? extends ThesaurusEntry> klass = null;
			boolean istag = context != null && context.getType() == ContextTypes.TAGGING;
			boolean isconcept = false;
			boolean istaxonomy = context != null && context.getType() == ContextTypes.TAXONOMY;
			
			Context ctx = (context == null) ? ContextsManager.findInWhichContext(uri, model) : context;
			
			
			if ( !istag && !istaxonomy )
			{
				for ( URI _uri : _storage.getType(uri) ) 
				{
					//-- detecting tagging
					if ( Concepts.TAG.get().equals(_uri) ) 
					{
						istag = true;
						break;
					}
					else if ( Concepts.TAXONOMYCONCEPT.get().equals(_uri) )
					{
						istaxonomy = true;
						break;
					}
					//-- detecting abstract concepts (e.g. from OpenThesaurus)
					else if ( Concepts.ABSTRACTCONCEPT.get().equals(_uri) ) 
					{
						isconcept = true;
						break;
					}
					else 
					{
					//-- detecting wordnet types
						WordNetTypes type = WordNetTypes.get(_uri);
						
						if (type != null) 
						{
							klass = type.getKlass();
							break;
						}
					}// if -else-else-ese
					
				}
			} // -- if not tag and not taxonomy
			
			if (isconcept) // -- if abstract concept
			{
				result = new ConceptEntity(ctx, uri, model);
			}
			else if (istag) // -- if a tagging
			{
				result = new EntryEntity(uri, model);
			}
			else if (istaxonomy)
			{
				result = new TaxonomyEntryEntity(ctx, uri, model);
			}
			else if (klass != null)
			{
				try 
				{
					Constructor<? extends ThesaurusEntry> constr = klass.getConstructor(URI.class, ModelSet.class);
					result = constr.newInstance(uri, model);
				} 
				catch (Exception e) 
				{
					logger.warn("Problem creating new WordNetEntry", e);
				}
			}
			else
			{
				result = new EntryEntity(ctx, uri, model);
			}
			
		}//--- if in storage
		
		return result;
	}
	
	private static final Logger logger = LoggerFactory.getLogger(EntriesManager.class);
	
}
