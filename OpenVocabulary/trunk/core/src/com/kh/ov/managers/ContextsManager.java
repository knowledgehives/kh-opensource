/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.managers;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.ontoware.aifbcommons.collection.ClosableIterator;
import org.ontoware.rdf2go.model.ModelSet;
import org.ontoware.rdf2go.model.Statement;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.Variable;
import org.ontoware.rdf2go.model.node.impl.URIImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kh.ov.entities.ContextEntity;
import com.kh.ov.model.Context;
import com.kh.ov.model.ContextTypes;
import com.kh.ov.model.Entry;
import com.kh.ov.persistance.Concepts;
import com.kh.ov.persistance.Repository;
import com.kh.ov.persistance.Storage;
import com.kh.ov.utils.SmartProperties;

/**
 * @author sebastiankruk
 *
 */
public class ContextsManager {

	
	/**
	 * Adds new contexts to the global list of KOS Contexts
	 * @param suri
	 * @param context
	 */
	public static void addContext(String suri, Context context){
		synchronized (contexts) {
			contexts.put(suri, context);
		}
	}

	/**
	 * @param uri
	 * @return A context for the given URI
	 */
	public static Context findContext(URI uri){
		synchronized(contexts){
			if (contexts.size() == 0) 
			{
				loadContexts();
			}
			return contexts.get(uri.toString());
		}
	}

	/**
	 * Tells in which contexts is given {@link uri} 
	 * @param _uri
	 * @param _model
	 * @return
	 */
	public static Context findInWhichContext(URI _uri, ModelSet _model){
		ModelSet model = (_model != null)?_model:Repository.getInstance().getModel();
		Context context = null;
		
		ClosableIterator<Statement> it = model.findStatements(Variable.ANY, _uri, Concepts.SKOS_INSCHEME.get(), Variable.ANY);
		URI ucontext = null;
		
		if(it.hasNext()){
			Statement st = it.next();
			ucontext = (URI) st.getObject();
		}
		
		it.close();
		
		if(ucontext != null)
			context = findContext(ucontext);
		
		if(context == null){
			context = new ContextEntity((ucontext!=null)?ucontext:_uri);
			synchronized (contexts) {
				if(contexts.size() == 0)
					loadContexts();
				contexts.put(((ucontext!=null)?ucontext:_uri).toString(), context);
			}
		}
		
		return context;
	}
	
	
	/**
	 * Returns singleton object of DDC Context
	 * 
	 * @return
	 * @throws IOException 
	 */
	public static Context createContext(String propsName) throws IOException{
		
		synchronized(contexts){
			Context context = contexts.get(propsName);
			
			if(context == null){
				try{
					SmartProperties prop = SmartProperties.load("com/kh/ov/conf/"+propsName, "com.kh.ov.conf."+propsName, Context.class);
			
					String label = prop.getProperty("ov_label");
					String description = prop.getProperty("ov_description");
					String namespace = prop.getProperty("ov_namespace_uri");
					String namespaceabbr = prop.getProperty("ov_namespace_abbr");
					String locale = prop.getProperty("ov_context_locale");
					ContextTypes type = ContextTypes.valueOf(prop.getProperty("ov_context_type"));
			
					context = new ContextEntity(label, description, namespace, namespaceabbr, locale, type, Storage.getInstance());
				
					// -- read roots and NO RDF read-in !!!
					switch(type){
					case TAGGING:
						context.setUrlPattern(prop.getProperty("ov_url_pattern"));
						if(defaultTagContext == null)
							defaultTagContext = context;
						break;
					case TAXONOMY:
						String[] lstRoots = prop.getAsStringArray("ov_root_concepts");
						for(String root : lstRoots){
							Entry entry = EntriesManager.getEntryByURI(new URIImpl(root), context, null);
							if(entry != null)
								context.addRoot(entry);
						}
						String[] lstTrees = prop.getAsStringArray("ov_main_tree_props");
						context.addTreeProperties(lstTrees);
						break;
					}
				}catch(NullPointerException ex){
					logger.error("Problem when loading context: "+propsName, ex);
				}
			}
			return context;
		}
		
	}	
	
	/**
	 * Loads all contexts in the system.
	 * 
	 * <big>IMPORTANT</big> Call this methods FIRST !
	 */
	public static Collection<Context> loadContexts() {
		SmartProperties props = SmartProperties.load("com/kh/ov/conf/contexts", "com.kh.ov.conf.contexts", Context.class);
        
        synchronized(contexts){
        	tagContexts = new HashMap<String, Context>();
        	Set<Context> taxonomies = new HashSet<Context>();
        	Set<Context> thesauri = new HashSet<Context>();
        	
	        for(String scontext : props.getAsStringArray("ov_contexts")){
	        	Context context;
				try {
					context = createContext(scontext);
		        	contexts.put(context.getNamespaceURI().toString(), context);
		        	
		        	switch(context.getType()){
		        	case TAGGING:
		        		tagContexts.put(context.getLocale(), context);
		        		break;
		        	case TAXONOMY:
		        		taxonomies.add(context);
		        		break;
		        	case THESAURUS:
		        		thesauri.add(context);
		        		break;
		        	}
		        		
				} catch (Exception e) {
					logger.error("problem when loading context "+scontext, e);
				}
	        }
	        taxonomyContexts = Collections.unmodifiableSet(taxonomies);
	        thesaurusContexts = Collections.unmodifiableSet(thesauri);
	        contextsView = Collections.unmodifiableCollection(contexts.values());
        }
        
        return contextsView;
    }
    
    /**
     * Adds a new TaxonomyContext to the available collection.
     * @param context new TaxonomyContext to be added to the available collection
     */
	public static void addContext(Context context) {
		synchronized(contexts){
			contexts.put(context.getNamespaceURI().toString(), context);
		}
    }
    
    /**
     * Returns unmodifiable view of the available contexts collection.
     * @return unmodifiable view of the available contexts collection
     */
	public static Collection<Context> getContexts() {
        return contextsView;
    }
    
    public static Context getContext(String uri) {
    	return contexts.get(uri);
    }
    
    public static Map<String, Context> getTagContexts() {
        synchronized(contexts){
    	if(tagContexts == null)
    		loadContexts();  
		return tagContexts;
        }
	}
    
    public static Context getTagContext(String locale) {
        synchronized(contexts){
    	if(tagContexts == null)
    		loadContexts();
    	
    	Context c = null;
    	
    	if (locale != null) {
    		c = tagContexts.get(locale);
    		
    		if (c == null){  // create new context for this locale
    			c = new ContextEntity("OpenVocabulary Tags ("+locale+")", 
    								  defaultTagContext.getDescription(), 
    								  defaultTagContext.getNamespaceURI().toString()+locale+"/", 
    								  "ovtag"+locale, 
    								  locale, 
    								  ContextTypes.TAGGING, 
    								  Storage.getInstance());
    			c.setUrlPattern(defaultTagContext.getUrlPattern());
    			
    			tagContexts.put(locale, c);//XXX ok since we are in a synchronized block
    			contexts.put(c.getNamespaceURI().toString(), c);
    		}
    	}

    	if (c == null)
    		c = defaultTagContext;
    		
		return c;
        }
	}

    public static Collection<Context> getThesaurusContexts() {
        synchronized(contexts){
        if(thesaurusContexts == null)
    		loadContexts();
		return thesaurusContexts;
        }
	}
    
    public static Collection<Context> getTaxonomyContexts() {
        synchronized(contexts){
    	if(taxonomyContexts == null)
    		loadContexts();
		return taxonomyContexts;
        }
	}


	//========================== statics =========================
	
	
	/**
	 * Map with all registered contexts 
	 */
	protected static final Map<String, Context> contexts = new HashMap<String, Context>();
    /**
     * Unmodifiable view of the contexts collection that is returned to the user.
     */
	private static Collection<Context> contextsView = null;
	/**
	 * Will keep the defined tag contexts
	 */
	private static Map<String, Context> tagContexts = null;
	/**
	 * Will keep the defined taxonomy contexts
	 */
	private static Collection<Context> taxonomyContexts = null;
	/**
	 * Will keep the defined thesaurus contexts
	 */
	private static Collection<Context> thesaurusContexts = null;
	/**
	 * Default tag context when lang is unknown
	 */
	private static Context defaultTagContext = null;
	

	public static final Logger logger = LoggerFactory.getLogger(ContextsManager.class);
}
