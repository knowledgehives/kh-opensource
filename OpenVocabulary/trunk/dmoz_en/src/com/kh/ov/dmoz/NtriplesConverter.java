/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */

package com.kh.ov.dmoz;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Auxiliary class containing static functions for converting text file with DMoz
 * taxonomy encoded in RDF/N-Triples format. The file has to be previously
 * parsed by narrowing.pl and unmangling.pl Perl scripts available in this
 * package. It should also be cleared of any additional content - HTML markup,
 * comments etc.
 */
public class NtriplesConverter {

	public NtriplesConverter() {
		/**
		 * empty constructor
		 */
	}

	/**
	 * Removes all lines from the given file (in ntriples rdf format) that
	 * represents topics from too deep depth level
	 * 
	 * @param filein
	 *            Input file in RDF N-Triples format
	 * @param fileout
	 *            Output file in RDF N-Triples format without topics from too
	 *            large depth level
	 * @param levels
	 *            Number of depth levels
	 */
	public static void trimFile(String filein, String fileout, int levels) {
		String line;
		String regexp = "^.*http://dmoz.org/Top";
		Pattern pattern, pattern2;
		int counter = levels;
		
		// adding as many regular expressions defining one depth level as given
		// 'levels'
		while (counter-- > 0) {
			regexp = regexp.concat("/[^/>]*");
		}
		regexp = regexp.concat("/.*$");

		pattern = Pattern.compile(regexp);
		// additional pattern needed to remove some unnecessary lines
		pattern2 = Pattern.compile("^.*<http://www.w3.org/.*$");

		try {
			BufferedReader in = new BufferedReader(new FileReader(filein));
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter(fileout)));

			while ((line = in.readLine()) != null) {
				// if the given line doesn't contain n-th level topics - it's
				// printed out
				if (pattern.matcher(line).matches() == false
						&& pattern2.matcher(line).matches() == false)
					out.println(line);
				else {
					counter++;
					if (counter % 100000 == 0)
						System.out.println("" + counter
								+ " statements removed so far");
				}
			}
			out.close();

			// for testing purpose only
			System.out.println("" + counter + " statements removed");
		} catch (FileNotFoundException e) {
			System.err.println("File opening error");
		} catch (IOException e) {
			System.err.println("File reading error");
		}
	}

	/**
	 * Function used to sort already trimmed rdf file with dmoz taxonomy in
	 * N-Triples format.
	 * 
	 * @param filein
	 * @param fileout
	 */
	public static void sortFile(String filein, String fileout) {
		String line;
		String regexp = "^.*narrow[12].*$", regexp2 = "narrow[12]";
		Pattern pattern = Pattern.compile(regexp);
		Pattern pattern2 = Pattern.compile(regexp2);
		// A set to keep our taxonomy, sorted according to given comparator
		SortedSet<String> set = new TreeSet<String>(new StringComparator());
		int counter = 0;

		try {
			BufferedReader in = new BufferedReader(new FileReader(filein));
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter(fileout)));

			while ((line = in.readLine()) != null) {
				if (pattern.matcher(line).matches() == true)
					// replacing 'narrow[12] to 'narrow' to avoid sorting
					// problems
					line = pattern2.matcher(line).replaceAll("narrow");
				// simply adding lines to SortedSet - it will preserve correct
				// order
				set.add(line);
			}

			Iterator<String> i = set.iterator();
			while (i.hasNext()) {
				out.println(i.next());
				counter++;
				if (counter % 1000 == 0)
					System.out.println("" + counter);
			}
			out.close();

		} catch (FileNotFoundException e) {
			System.err.println("File opening error");
		} catch (IOException e) {
			System.err.println("File reading error");
		}
	}
	
	public static void slashUToPercent(String filein, String fileout)  {
		String line;
		String regexp2 = "\\\\u([A-F0-9]{2})([A-F0-9]{2})";
		String regexp = "<[^<]+>";
		Pattern pattern1 = Pattern.compile(regexp);
		Pattern pattern2 = Pattern.compile(regexp2);

		String[] letters = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r",
				"s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M",
				"N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
		
		try {
			BufferedReader in = new BufferedReader(new FileReader(filein));
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter(fileout)));
			Matcher m1, m2;
			while ((line = in.readLine()) != null) {
				m1 = pattern1.matcher(line);
				
				while (m1.find()){
					String s1 = m1.group();
					m2 = pattern2.matcher(s1);
					while(m2.find()){
						String s2 = m2.group();
						s2 = s2.substring(2);
						int value = Integer.parseInt(s2, 16);
						value %= 52;
						String s3 = s1.replace(m2.group(), letters[value]);
						line = line.replace(s1, s3);
						s1 = s3;
						//System.out.println(line);
					}					
				}	
				out.println(line);
			}
		} catch (FileNotFoundException e) {
			System.err.println("File opening error");
		} catch (IOException e) {
			System.err.println("File reading error");
		}
	}
	
	public static void underlineToSpace(String filein, String fileout)  {
		String line;
		String regexp = "\".*_.*\"";
		Pattern pattern1 = Pattern.compile(regexp);
		
		try {
			BufferedReader in = new BufferedReader(new FileReader(filein));
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter(fileout)));
			Matcher m1;
			while ((line = in.readLine()) != null) {
				m1 = pattern1.matcher(line);				
				if (m1.find()){
					String s1 = m1.group();					
					String s2 = s1.replaceAll("_", " ");		
					line = line.replaceAll(s1, s2);
				}					
				out.println(line);
			}
		} catch (FileNotFoundException e) {
			System.err.println("File opening error");
		} catch (IOException e) {
			System.err.println("File reading error");
		}
	}

	
	/* Below is the code used for creating a list of dmoz concepts for usage in TTM
	//getting rid of unnecessary lines
	public static void ttmConverter(String filein, String fileout)  {
		String line;
//		String regexp = "<Topic|<link|<rss";
		String regexp = "(<Topic r:id=\")([^\"]+)(\">)";
		String regexp2 = "<link|<rss";

		Pattern pattern = Pattern.compile(regexp);
		Pattern pattern2 = Pattern.compile(regexp2);
		
		BufferedReader in = null;
		PrintWriter out = null;
		try {
			in = new BufferedReader(new FileReader(filein));
			out = new PrintWriter(new BufferedWriter(
					new FileWriter(fileout)));
			Matcher m1, m2;
			long counter = 0;
			while ((line = in.readLine()) != null) {
				in.mark(1000000);
				m1 = pattern.matcher(line);				
				if (m1.matches()) {
					out.print(m1.group(2));
					int items = 0;
					while ((line = in.readLine()) != null) {
						m2 = pattern2.matcher(line);
						if(m2.find())
							items++;
						else {
							in.reset();
							break;
						}						
					}
					out.println(" "+items);
				}
				
				
				if(counter % 100000 == 0)
					System.out.println(""+counter+" lines processed so far");
				counter++;
				
//				if(counter==1000)
//					return;

			}
		} catch (FileNotFoundException e) {
			System.err.println("File opening error");
		} catch (IOException e) {
			System.err.println("File reading error");
		} finally {
			try {
				in.close();
				out.close();
			} catch (IOException e) {
				//null
			}
			
		}
	}
	
	//summing up the resources in the hierarchies
	public static void ttmConverter2(String filein, String fileout)  {
		String line;
		
		LinkedList<Tag> stack = new LinkedList<Tag>();
		
		BufferedReader in = null;
		PrintWriter out = null;
		try {
			in = new BufferedReader(new FileReader(filein));
			out = new PrintWriter(new BufferedWriter(
					new FileWriter(fileout)));
			
			int curDepth = 1, counter = 0;
			
			while ((line = in.readLine()) != null) {
				
				String[] tab = line.split(" ");
				char[] tab2 = tab[0].toCharArray();
				curDepth = 1;
				for(char c : tab2) {
					if(c == '/')
						curDepth++;
				}

				Tag tag = new Tag();
				tag.setTag(tab[0]);
				tag.setSize(Long.valueOf(tab[1]));
				
				if(curDepth > stack.size()) { //we simply push new tag on the stack
					for(int i = 0; i<stack.size(); i++) {	//now we have to add current size to all tag that are higher in the hierarchy
						Tag tmp = stack.get(i);
						tmp.setSize(tmp.getSize()+tag.getSize());
						stack.set(i, tmp);
					}
					stack.add(tag);
				}
				else {
					for(int i = stack.size()-1; i >= curDepth-1; i--) {	//we remove and print all tags that are lower in the hierarchy
						Tag toremove = stack.pollLast();
						out.println(toremove.getTag()+" "+toremove.getSize());
					}
					for(int i = 0; i<stack.size(); i++) {	//now we have to add current size to all tag that are higher in the hierarchy
						Tag tmp = stack.get(i);
						tmp.setSize(tmp.getSize()+tag.getSize());
						stack.set(i, tmp);
					}
					stack.add(tag);
				}
				if(counter % 100000 == 0)
					System.out.println(""+counter+" lines processed so far");
				counter++;
			}
		} catch (FileNotFoundException e) {
			System.err.println("File opening error");
		} catch (IOException e) {
			System.err.println("File reading error");
		} finally {
			try {
				in.close();
				out.close();
			} catch (IOException e) {
				//null
			}
		}
	}
	
	//getting rid of tags with size 0 and with "strange" alphabets
	public static void ttmConverter3(String filein, String fileout)  {
		String line;
		
		BufferedReader in = null;
		PrintWriter out = null;
		try {
			in = new BufferedReader(new FileReader(filein));
			out = new PrintWriter(new BufferedWriter(
					new FileWriter(fileout)));
			
			int counter = 0;
			int counter2 = 0, counter3 = 0;
			
			while ((line = in.readLine()) != null) {
				
				if(counter % 100000 == 0)
					System.out.println(""+counter+" lines processed so far");
				counter++;
				
				String[] tab = line.split(" ");
				if(Integer.parseInt(tab[1]) == 0) {
					counter3++;
					continue;
				}
				
				char[] tab2 = tab[0].toCharArray();
				boolean stop = false;
				for(char c : tab2) {
					if(c  > 160) {
						counter2++;
						stop = true;
						break;
					}
				}
				if(stop) {
					continue;
				}
				
				out.println(line);
			}
			System.out.println(counter);
			System.out.println(counter2);
			System.out.println(counter3);
		} catch (FileNotFoundException e) {
			System.err.println("File opening error");
		} catch (IOException e) {
			System.err.println("File reading error");
		} finally {
			try {
				in.close();
				out.close();
			} catch (IOException e) {
				//null
			}
		}
	}
	
	//extracting category's name and parent, and putting them into the result string
	public static void ttmConverter4(String filein, String fileout)  {
		String line;
		
		BufferedReader in = null;
		PrintWriter out = null;
		try {
			in = new BufferedReader(new FileReader(filein));
			out = new PrintWriter(new BufferedWriter(
					new FileWriter(fileout)));
			
			int counter = 0;
			
			while ((line = in.readLine()) != null) {
				
				if(counter % 100000 == 0)
					System.out.println(""+counter+" lines processed so far");
				counter++;
				
				String[] tab = line.split(" ");

				String path = tab[0];
				int number = Integer.parseInt(tab[1]);
				
				tab = path.split("/");
				
				String parent = "", category = "";
				if(tab.length == 2) {
					parent = tab[0];
					category = tab[1];
				} else if(tab.length > 2) {
					parent = tab[tab.length-2];
					category = tab[tab.length-1];
				}
					
				
				out.println(path+" "+parent+" "+category+" "+number);
			}
			System.out.println(counter);
		} catch (FileNotFoundException e) {
			System.err.println("File opening error");
		} catch (IOException e) {
			System.err.println("File reading error");
		} finally {
			try {
				in.close();
				out.close();
			} catch (IOException e) {
				//null
			}
		}
	}
	
	public static void ttmConverter5(String filein, String fileout) {
		String line;
		String regexp = "^Top(/[^/]+){4,}";
		Pattern pattern = Pattern.compile(regexp);
		int counter = 0;

		try {
			BufferedReader in = new BufferedReader(new FileReader(filein));
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new FileWriter(fileout)));

			while ((line = in.readLine()) != null) {

				if(counter % 100000 == 0)
					System.out.println(""+counter+" lines processed so far");
				counter++;
				
				if (pattern.matcher(line).matches() == true)
					continue;
				
				out.println(line);
			}
			
			// for testing purpose only
			System.out.println("" + counter + " statements removed");
		} catch (FileNotFoundException e) {
			System.err.println("File opening error");
		} catch (IOException e) {
			System.err.println("File reading error");
		}
	}
	*/
	
	
	/**
	 * For testing purposes only
	 * 
	 * @param args
	 * @throws UnsupportedEncodingException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	public static void main(String[] args) {
		//ttmConverter5("c:/Workspace/JOnto_dmoz/src/test4.txt", "c:/Workspace/JOnto_dmoz/src/test5b.txt");
	}

}

class StringComparator implements Comparator<String> {

	public int compare(String o1, String o2) {
		String[] t1 = new String[4];
		String[] t2 = new String[4];
		t1 = o1.split("<");
		t2 = o2.split("<");

		if (t1[1].split("/").length > t2[1].split("/").length)
			return 1;
		else if (t1[1].split("/").length < t2[1].split("/").length)
			return -1;
		else if (t1[2].contains("http://purl.org/dc/elements/1.0/Title"))
			return -1;
		else if (t2[2].contains("http://purl.org/dc/elements/1.0/Title"))
			return 1;
		else if (t1[2].compareTo(t2[2]) > 0)
			return 1;
		else if (t1[2].compareTo(t2[2]) < 0)
			return -1;
		else if (o1.compareTo(o2) > 0)
			return 1;
		else if (o1.compareTo(o2) < 0)
			return -1;
		else
			return 0;

	}
}

class Tag {
	String tag;
	Long size;
	public String getTag() {
		return tag;
	}
	public void setTag(String _tag) {
		this.tag = _tag;
	}
	public Long getSize() {
		return size;
	}
	public void setSize(Long _size) {
		this.size = _size;
	}
	public Tag(String _tag, Long _size) {
		super();
		this.tag = _tag;
		this.size = _size;
	}		
	public Tag() {
		this.tag = null;
		this.size = null;
	}
}
