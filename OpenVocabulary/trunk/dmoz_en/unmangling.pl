#!/usr/bin/perl


# dmoz rdf unmangler

# krychu



use warnings;

use strict;



open(IN, '<structure.rdf.u8') or die 'can not open the file';

open(OUT, '>structure2.rdf.u8') or die 'can not create the file';



binmode(IN); # IN, ':uft8'?

binmode(OUT);



my $n=1;

my @signs=('\\', '|', '/', '-', '\\', '|', '/', '-');

my $nsigns = $#signs+1;

my $sign;



while (<IN>) {

    if (m/xmlns:r/) {

	print OUT "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n";

    } elsif (m/xmlns:d/) {

	print OUT "\txmlns:d=\"http://purl.org/dc/elements/1.0/\"\n";

    } elsif (m/xmlns=/) {
	
	print OUT "\txmlns=\"http://dmoz.org/rdf/\">\n";

    } elsif (m/<Topic r:id="(.*?)"/) {

	print OUT "<rdf:Description rdf:about=\"http://dmoz.org/$1\">\n";

	print OUT "\t<rdf:type rdf:resource=\"http://dmoz.org/rdf/Topic\"/>\n";

    } elsif (m{</Topic>}) {

	print OUT "</rdf:Description>\n";

    } elsif (m/<Alias r:id="(.*?)">/) {

	print OUT "<rdf:Description rdf:about=\"http://dmoz.org/$1\">\n";

	print OUT "<rdf:type rdf:resource=\"http://dmoz.org/rdf/Alias\"/>\n";

    } elsif (m{</Alias>}) {

	print OUT "</rdf:Description>\n";

    } elsif (m/r:resource="(.*?)"/) {

	s|r:resource="(.*?)"|rdf:resource="http://dmoz.org/$1"|g;

	print OUT $_;

    } elsif (m{</RDF>}) {

	print OUT "</rdf:RDF>\n";

#    } elsif (m/<d:.*?>/) {

#	s/d:/dc:/g;

#	print OUT $_;

#    } elsif (m/<\/d:.*?>/) {

#	s/d:/dc:/;

#	print OUT $_;

    } else {

	print OUT $_;

    }



    $sign = $signs[$n % $nsigns];

    print "\runmangling ... $sign";

    $n++;

}



print "\ndone\n";



close OUT;

close IN;

