#!/usr/bin/perl

# dmoz rdf unmangler

# krychu



use warnings;

use strict;



open(IN, '<structure2.rdf.u8') or die 'can not open the file';

open(OUT, '>structure3.rdf.u8') or die 'can not create the file';



binmode(IN); # IN, ':uft8'?

binmode(OUT);



my $n=1;

my @signs=('\\', '|', '/', '-', '\\', '|', '/', '-');

my $nsigns = $#signs+1;

my $sign;

my $state = 0;

while (<IN>) {

    if (m/xmlns:r/) {

	print OUT "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n";

    } elsif (m/xmlns:d/) {

	print OUT "\txmlns:d=\"http://purl.org/dc/elements/1.0/\"\n";

    } elsif (m/xmlns=/) {
	
	print OUT "\txmlns=\"http://dmoz.org/rdf/\">\n";

#    } elsif (m/<Topic r:id="(.*?)"/) {
    } elsif (m/rdf:Description rdf:about="http:\/\/dmoz.org\/([^:]*?)"/) {
	print OUT "<rdf:Description rdf:about=\"http://dmoz.org/$1\">\n";


	$state = 1;

    } elsif ($state == 1 && m/rdf:type/) {

	print OUT "\t<rdf:type rdf:resource=\"http://dmoz.org/rdf/Topic\"/>\n";

#    } elsif (m{</Topic>}) {
    } elsif($state == 1 && m/<\/rdf:Description>/) {

	print OUT "</rdf:Description>\n";

	$state = 0;

#    } elsif (m/<Alias r:id="(.*?)">/) {

#	$state = 0;

#	print OUT "<rdf:Description rdf:about=\"http://dmoz.org/$1\">\n";

#	print OUT "<rdf:type rdf:resource=\"http://dmoz.org/rdf/Alias\"/>\n";

 #   } elsif (m{</Alias>}) {

#	$state = 1;

#	print OUT "</rdf:Description>\n";

    } elsif ($state == 1 && m/<narrow([12]?) rdf:resource="(.*?)"\/>/) {

#	s|r:resource="(.*?)"|rdf:resource="http://dmoz.org/$1"|g;

	print OUT $_;

    } elsif (m{</rdf:RDF>}) {

	print OUT "</rdf:RDF>\n";

    } elsif ($state == 1 && m/<d:Title>(.*?)<\/d:Title>/) {

#	s/d:/dc:/g;

	print OUT $_;

#    } elsif (m/<\/d:.*?>/) {

#	s/d:/dc:/;

#	print OUT $_;

#    } elsif (m/<narrow[12]?.*?>


#    } else {

#	print OUT $_;

    }



    $sign = $signs[$n % $nsigns];

    print "\rnarrowing ... $sign";

    $n++;

}



print "\ndone\n";



close OUT;

close IN;

