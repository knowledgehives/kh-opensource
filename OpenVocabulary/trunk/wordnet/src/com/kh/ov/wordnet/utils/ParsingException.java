/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.wordnet.utils;

import java.io.IOException;

/**
 * @author sebastiankruk
 *
 */
public class ParsingException extends IOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3804600521306886826L;
	/**
	 * Message to be shown as the reason of the exception
	 */
	String message;
	/**
	 * Actual cause with stack trace as reported by the process
	 */
	Throwable cause;
	
	public ParsingException(String filename, Throwable e) {
		this.message = filename;
		this.cause = e;
		this.setStackTrace(e.getStackTrace());
	}
	
	@Override
	public String toString() {
		return this.message+"["+this.cause.toString()+"]";
	}

}
