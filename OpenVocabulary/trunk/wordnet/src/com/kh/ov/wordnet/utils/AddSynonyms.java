/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.wordnet.utils;

import java.util.Iterator;
import java.util.logging.Logger;

import org.ontoware.rdf2go.model.ModelSet;
import org.ontoware.rdf2go.model.Statement;

import com.kh.ov.persistance.Repository;


/**
 * @author sebastiankruk
 *
 */
public class AddSynonyms {

	
	/**
	 * Logger
	 */
	protected static Logger logger = Logger.getLogger("com.kh.jonto.openthesaurus.serialize");
	
	/**
	 * access to sesame 
	 */
	ModelSet model;
	
	/**
	 * <pre>PREFIX skos: &lt;http://www.w3.org/2004/02/skos/core#>
PREFIX wns:  &lt;http://www.w3.org/2006/03/wn/wn20/schema/>
CONSTRUCT  { ?meaningA skos:related          ?meaningB }
WHERE      { ?synset   wns:containsWordSense ?meaningA .
             ?synset   wns:containsWordSense ?meaningB .
             FILTER ( ?meaningA != ?meaningB ) }</pre>
	 */
	protected static final String MATCH_SYNONYMS = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" + 
			"PREFIX wns:  <http://www.w3.org/2006/03/wn/wn20/schema/>\n" + 
			"CONSTRUCT  { ?meaningA skos:related          ?meaningB }\n" + 
			"WHERE      { ?synset   wns:containsWordSense ?meaningA .\n" + 
			"             ?synset   wns:containsWordSense ?meaningB .\n" + 
			"             FILTER ( ?meaningA != ?meaningB ) }";	
	
	/**
	 * <pre>PREFIX skos: &lt;http://www.w3.org/2004/02/skos/core#>
PREFIX wns:  &lt;http://www.w3.org/2006/03/wn/wn20/schema/>
CONSTRUCT  { ?synA skos:broader  ?synB .
             ?synB skos:narrower ?synA }
WHERE      { ?synA wns:hyponymOf ?synB }</pre>
	 */
	protected static final String MATCH_HYPONYMS = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" + 
			"PREFIX wns:  <http://www.w3.org/2006/03/wn/wn20/schema/>\n" + 
			"CONSTRUCT  { ?synA skos:broader  ?synB .\n" + 
			"             ?synB skos:narrower ?synA }\n" + 
			"WHERE      { ?synA wns:hyponymOf ?synB }";
	
	/**
	 * <pre>PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX wns:  <http://www.w3.org/2006/03/wn/wn20/schema/>
PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
CONSTRUCT  { ?res skos:inScheme <http://www.w3.org/2006/03/wn/wn20/> }
WHERE      { ?res rdf:type ?atype .
             FILTER  regex( STR(?res), "^http[:]//www[.]w3[.]org/2006/03/wn/wn20/instances/.*" ) }</pre>
	 */
	protected static final String ADD_INSCHEME = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" + 
			"PREFIX wns:  <http://www.w3.org/2006/03/wn/wn20/schema/>\n" + 
			"PREFIX rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n" + 
			"CONSTRUCT  { ?res skos:inScheme <http://www.w3.org/2006/03/wn/wn20/> }\n" + 
			"WHERE      { ?res rdf:type ?atype .\n" + 
			"             FILTER  regex( STR(?res), \"^http[:]//www[.]w3[.]org/2006/03/wn/wn20/instances/.*\" ) }";

	public AddSynonyms() {
		this.model = Repository.getInstance().getModel();
		
	}
	
	public void match()  {
		Iterator<Statement> stmtit = model.queryConstruct(MATCH_SYNONYMS, "SPARQL").iterator();

		model.addAll(stmtit);
		logger.info("[ SKOS:SYNONYMS MAPPING CREATED ]");

		stmtit = model.queryConstruct(MATCH_HYPONYMS, "SPARQL").iterator();
		
		model.addAll(stmtit);
		logger.info("[ SKOS:HYPONYMS MAPPING CREATED ]");
	}
	
	
}

