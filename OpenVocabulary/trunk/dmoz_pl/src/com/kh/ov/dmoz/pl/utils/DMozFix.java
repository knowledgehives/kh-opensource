/**
Copyright (c) 2011, KnowledgeHives sp. z o.o

This file is part of OpenVocabulary.

OpenVocabulary has been invented by Sebastian Ryszard Kruk (based on previous work on JOnto project) 

OpenVocabulary is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenVocabulary is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.

IMPORTANT:
1) In addition to the terms and conditions defined in the GNU Affero 
General Public License you agree to use this software to provide access 
to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
a generally available end-point.
2) You will also notify the copyright owners, i.e., Knowledge Hives 
sp. z o.o., via email at info@knowledgehives.com, about the address 
of end-point you have setup using this software.
3) Finally, you need to ensure that the vocabularies managed using this 
software are correctly indexed by the Sindice semantic index service; 
we suggest using semantic sitemap protocol in oder to do so.

See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
for more information
 */
package com.kh.ov.dmoz.pl.utils;

import java.util.Collection;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.corrib.openrdf.OpenRDF;
import org.ontoware.aifbcommons.collection.ClosableIterator;
import org.ontoware.rdf2go.model.ModelSet;
import org.ontoware.rdf2go.model.QueryResultTable;
import org.ontoware.rdf2go.model.QueryRow;
import org.ontoware.rdf2go.model.Statement;
import org.ontoware.rdf2go.model.node.Node;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.Variable;
import org.ontoware.rdf2go.model.node.impl.URIImpl;
import org.ontoware.rdf2go.vocabulary.RDFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kh.ov.persistance.Concepts;
import com.kh.ov.utils.Latinize;

/**
 * @author sebastiankruk
 *
 */
public class DMozFix {
	
	/**
	 * Extracts polish path from the URI: <code>http[:][/][/]dmoz.org([/]Top[/]World[/]Polska(?:[/].*)?)$</code>
	 */
	static final Pattern extractPolish = Pattern.compile("^http[:][/][/]dmoz.org([/]Top[/]World[/]Polska(?:[/].*)?)$");
	/**
	 * Creates new URI in OV space <code>http://www.openvocabulary.info/taxonomies/dmoz/pl%1$s</code>
	 */
	static final String newOVPath =  "http://www.openvocabulary.info/taxonomies/dmoz/pl%1$s";
	
	static final Logger logger = LoggerFactory.getLogger(DMozFix.class);
	
	static final String context = "http://www.openvocabulary.info/taxonomies/dmoz/pl/";
	

	public void fix(){
		ModelSet model =  OpenRDF.getInstance().getRemoteModelSet("http://localhost:8180/openrdf-sesame", "DMOZ_PL");
		model.open();
		this.updatePath(model);
		this.fixProperties(model);
		this.addToScheme(model);
		model.close();
	}
	
	
	/**
	 * Translates all Topics from
	 * <code>http://dmoz.org/Top/World/Polska/</code> prefix to <code>http://www.openvocabulary.info/taxonomy/dmoz/pl/</code> prefix in the given model.
	 * <br/>
	 * Uses {@link FixingQueries#LIST_POLISH_TOPICS} query.
	 * <br/>
	 * Sets original URIs as see also (<code>http://www.w3.org/2000/01/rdf-schema#seeAlso</code>)
	 * @param model
	 */
	protected void updatePath(ModelSet model){
		QueryResultTable results = model.sparqlSelect(FixingQueries.LIST_POLISH_TOPICS.getQuery());
//		ModelSet tmpmodel = RDF2Go.getModelFactory().createModelSet();
//		tmpmodel.open();
		
		ClosableIterator<QueryRow> rows = results.iterator();
		Collection<Node> toremove = new HashSet<Node>();
		
		URI ucontext = new URIImpl(context);
		
		for( ; rows.hasNext();  ){ // for all topics
			QueryRow row = rows.next();
			Node n = row.getValue("topic");
			URI usubject = null;

			//--extract topic
			String ssubject = n.toString();
			String nsubject = null;
			
			//--convert topic to OV path
			Matcher m = extractPolish.matcher(ssubject);
			if(m.find()){
				String path = m.group(1);
				nsubject = String.format(newOVPath, Latinize.latinize(path, true)); // point 1. translation and 2. - latinization
				usubject = new URIImpl(nsubject);
			}

			if(usubject != null){
				logger.info("Convert from: "+n+" to: "+usubject);
				//---create new triples in the tmp model
				ClosableIterator<Statement> cit = model.findStatements(Variable.ANY, n.asResource(), Variable.ANY, Variable.ANY);
				for( ; cit.hasNext(); ){
					Statement st = cit.next();
					
					if(st.getObject() instanceof URI){
						URI u = st.getObject().asURI();
						String su = u.toString();
						
						Matcher mu = extractPolish.matcher(su);
						if(mu.find()){
							String path = m.group(1);
							su = String.format(newOVPath, Latinize.latinize(path, true)); // point 1. translation and 2. - latinization
							u = new URIImpl(su);
						}
						
						model.addStatement(ucontext, usubject, st.getPredicate(), u);
					}else
						model.addStatement(ucontext, usubject, st.getPredicate(), st.getObject());
				}
				cit.close();
				
				//--add seeAlso triple
				model.addStatement(ucontext, usubject, RDFS.seeAlso, n);
			}
			
			toremove.add(n);
			
		}
		rows.close();
		
		//--remove those triples that should be removed
		for(Node n : toremove)
			model.removeStatements(Variable.ANY, n.asResource(), Variable.ANY, Variable.ANY);
		 
		//--copy models over to the original model
		//model.addAll(tmpmodel.iterator());
		
		
//		tmpmodel.close();		
		
	}
	
	
	/**
	 * Fixes properties in the graph:
	 * <ul>
	 * <li><code>http://purl.org/dc/elements/1.0/Title</code> to skos:prefLabel <code>http://www.w3.org/2004/02/skos/core#prefLabel</code></li>
	 * <li><code>http://purl.org/dc/elements/1.0/Description</code> to skos:definition <code>http://www.w3.org/2004/02/skos/core#definition</code></li>
	 * <li><code>http://dmoz.org/rdf/related</code> to skos:related <code>http://www.w3.org/2004/02/skos/core#related</code></li>
	 * <li><code>http://dmoz.org/rdf/narrow</code>, <code>http://dmoz.org/rdf/narrow1</code> and <code>http://dmoz.org/rdf/narrow2</code> to <code>http://www.w3.org/2004/02/skos/core#narrower</code></li>
	 * <li><code>http://dmoz.org/rdf/symbolic</code>, <code>http://dmoz.org/rdf/symbolic1</code> and <code>http://dmoz.org/rdf/symbolic2</code> to <code>http://www.w3.org/2000/01/rdf-schema#seeAlso</code></li>
	 * <li>provide inverted values for skos:narrower <code>http://www.w3.org/2004/02/skos/core#broader</code></li>
	 * </ul>
	 * @param model
	 */
	public void fixProperties(ModelSet model){
		ClosableIterator<Statement> stit;
		
		//<li><code>http://purl.org/dc/elements/1.0/Title</code> to skos:prefLabel <code>http://www.w3.org/2004/02/skos/core#prefLabel</code></li>
		stit = model.findStatements(Variable.ANY, Variable.ANY, new URIImpl("http://purl.org/dc/elements/1.0/Title"), Variable.ANY);
		for( ; stit.hasNext(); ){
			Statement st = stit.next();
			model.addStatement(st.getContext(), st.getSubject(), Concepts.SKOS_PREFLABEL.get(), st.getObject());
			logger.info("Title: "+st.getSubject());
		}
		stit.close();
		
		//<li><code>http://purl.org/dc/elements/1.0/Description</code> to skos:definition <code>http://www.w3.org/2004/02/skos/core#definition</code></li>
		stit = model.findStatements(Variable.ANY, Variable.ANY, new URIImpl("http://purl.org/dc/elements/1.0/Description"), Variable.ANY);
		for( ; stit.hasNext(); ){
			Statement st = stit.next();
			model.addStatement(st.getContext(), st.getSubject(), Concepts.SKOS_DEFINITION.get(), st.getObject());
			logger.info("Description: "+st.getSubject());
		}
		stit.close();
		
		//<li><code>http://dmoz.org/rdf/related</code> to skos:related <code>http://www.w3.org/2004/02/skos/core#related</code></li>
		stit = model.findStatements(Variable.ANY, Variable.ANY, new URIImpl("http://dmoz.org/rdf/related"), Variable.ANY);
		for( ; stit.hasNext(); ){
			Statement st = stit.next();
			model.addStatement(st.getContext(), st.getSubject(), Concepts.SKOS_RELATED.get(), st.getObject());
			logger.info("Related: "+st.getSubject());
		}
		stit.close();
		
		//<li><code>http://dmoz.org/rdf/narrow</code>, <code>http://dmoz.org/rdf/narrow1</code> and <code>http://dmoz.org/rdf/narrow2</code> to <code>http://www.w3.org/2004/02/skos/core#narrower</code></li>
		stit = model.findStatements(Variable.ANY, Variable.ANY, new URIImpl("http://dmoz.org/rdf/narrow"), Variable.ANY);
		for( ; stit.hasNext(); ){
			Statement st = stit.next();
			model.addStatement(st.getContext(), st.getSubject(), Concepts.SKOS_NARROWER.get(), st.getObject());
			model.addStatement(st.getContext(), st.getObject().asResource(), Concepts.SKOS_BROADER.get(), st.getSubject());
			logger.info("Narrower: "+st.getSubject());
		}
		stit.close();
		stit = model.findStatements(Variable.ANY, Variable.ANY, new URIImpl("http://dmoz.org/rdf/narrow1"), Variable.ANY);
		for( ; stit.hasNext(); ){
			Statement st = stit.next();
			model.addStatement(st.getContext(), st.getSubject(), Concepts.SKOS_NARROWER.get(), st.getObject());
			model.addStatement(st.getContext(), st.getObject().asResource(), Concepts.SKOS_BROADER.get(), st.getSubject());
			logger.info("Narrower: "+st.getSubject());
		}
		stit.close();
		stit = model.findStatements(Variable.ANY, Variable.ANY, new URIImpl("http://dmoz.org/rdf/narrow2"), Variable.ANY);
		for( ; stit.hasNext(); ){
			Statement st = stit.next();
			model.addStatement(st.getContext(), st.getSubject(), Concepts.SKOS_NARROWER.get(), st.getObject());
			model.addStatement(st.getContext(), st.getObject().asResource(), Concepts.SKOS_BROADER.get(), st.getSubject());
			logger.info("Narrower: "+st.getSubject());
		}
		stit.close();
		
		//<li><code>http://dmoz.org/rdf/symbolic</code>, <code>http://dmoz.org/rdf/symbolic1</code> and <code>http://dmoz.org/rdf/symbolic2</code> to <code>http://www.w3.org/2000/01/rdf-schema#seeAlso</code></li>
		stit = model.findStatements(Variable.ANY, Variable.ANY, new URIImpl("http://dmoz.org/rdf/symbolic"), Variable.ANY);
		for( ; stit.hasNext(); ){
			Statement st = stit.next();
			model.addStatement(st.getContext(), st.getSubject(), RDFS.seeAlso, st.getObject());
			logger.info("Symbolic: "+st.getSubject());
		}
		stit.close();
		stit = model.findStatements(Variable.ANY, Variable.ANY, new URIImpl("http://dmoz.org/rdf/symbolic1"), Variable.ANY);
		for( ; stit.hasNext(); ){
			Statement st = stit.next();
			model.addStatement(st.getContext(), st.getSubject(), RDFS.seeAlso, st.getObject());
			logger.info("Symbolic: "+st.getSubject());
		}
		stit.close();
		stit = model.findStatements(Variable.ANY, Variable.ANY, new URIImpl("http://dmoz.org/rdf/symbolic2"), Variable.ANY);
		for( ; stit.hasNext(); ){
			Statement st = stit.next();
			model.addStatement(st.getContext(), st.getSubject(), RDFS.seeAlso, st.getObject());
			logger.info("Symbolic: "+st.getSubject());
		}
		stit.close();
		
		
	}
	
	/**
	 * Adding each Topic to the scheme using skos:inScheme
	 * @param model
	 */
	public void addToScheme(ModelSet model){
		ClosableIterator<QueryRow> it =	model.sparqlSelect(FixingQueries.LIST_TOPICS.getQuery()).iterator();
		URI ucontext = new URIImpl(context);
		
		for( ; it.hasNext(); ){
			Node n = it.next().getValue("topic");
			model.addStatement(ucontext, n.asResource(), Concepts.SKOS_INSCHEME.get(), ucontext);
		}
		
		
		it.close();
	}
	
	public static void main(String[] args){
		new DMozFix().fix();
	}
}
