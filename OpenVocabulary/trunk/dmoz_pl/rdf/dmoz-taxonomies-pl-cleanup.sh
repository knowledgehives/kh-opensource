#!/bin/bash

cat export.nt | awk ' \
  BEGIN { skip = 0 } \
  /^.*$/ { \
     skip = 0; \
  } \
  skip == 0 && /seeAlso> <http:\/\/dmoz.org\/Top\//{ \
     print $0; \
     skip = 1; \
  } \
  skip == 0 && /<.+> <.+[^o]> <http:\/\/dmoz.org\/Top\/.+> [.]/ { str = 3; \
     gsub(/<http:\/\/dmoz.org\/Top\//, "<http://www.openvocabulary.info/taxonomies/dmoz/pl/Top/", $str ); \
     print $1,$2,$str,".";  \
     skip = 1 \
   }  \
  skip == 0 { \
     print $0 \
  } \
' | awk ' \
  BEGIN { skip = 0 } \
  /^.*$/ { \
     skip = 0; \
  } \
  skip == 0 && /<http:\/\/dmoz.org\/Top\/.+> <.+> .+ [.]/ { str = 1; \
     gsub(/<http:\/\/dmoz.org\/Top\//, "<http://www.openvocabulary.info/taxonomies/dmoz/pl/Top/", $str ); \
     print $str,$2,$3,".";  \
     skip = 1 \
   }  \
  skip == 0 { \
     print $0 \
  } \
' | sed -e "s/<http:\/\/www.openvocabulary.info\/taxonomy\/dmoz\/pl\//<http:\/\/www.openvocabulary.info\/taxonomies\/dmoz\/pl\//g"

