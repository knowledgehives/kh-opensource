import sys, os

path = os.path.dirname(__file__).rstrip("apache")

sys.path.append(path+"/ov_django")
os.environ["DJANGO_SETTINGS_MODULE"] = "ov_django.settings"

import ov_django.settings as settings
import django.core.management
django.core.management.setup_environ(settings)
utility = django.core.management.ManagementUtility()
command = utility.fetch_command('runserver')
command.validate()

import django.conf
import django.utils
django.utils.translation.activate(django.conf.settings.LANGUAGE_CODE)


import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()