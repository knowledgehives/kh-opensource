#fixuri.py
#
#Created by Sebastian Kruk.
#Copyright (c) 2011, KnowledgeHives sp. z o.o
#
#This file is part of OpenVocabulary.
#
#OpenVocabulary is free software: you can redistribute it and/or modify
#it under the terms of the GNU Affero General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#OpenVocabulary is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU Affero General Public License for more details.
#
#You should have received a copy of the GNU Affero General Public License
#along with OpenVocabulary.  If not, see <http://www.gnu.org/licenses/>.
#
#IMPORTANT:
#1) In addition to the terms and conditions defined in the GNU Affero 
#General Public License you agree to use this software to provide access 
#to vocabularies, i.e., thesauri, taxonomies, and such, ONLY through 
#a generally available end-point.
#2) You will also notify the copyright owners, i.e., Knowledge Hives 
#sp. z o.o., via email at info@knowledgehives.com, about the address 
#of end-point you have setup using this software.
#3) Finally, you need to ensure that the vocabularies managed using this 
#software are correctly indexed by the Sindice semantic index service; 
#we suggest using semantic sitemap protocol in oder to do so.
#
#See http://opensource.knowledgehives.com/wiki/OpenVocabulary 
#for more information

import sys
import gc
import re
import getopt
import time
import datetime

from django.core.management import setup_environ
from django.conf import settings
settings.DEBUG = False
setup_environ(settings)

from django.db import connection
assert not connection.queries, 'settings.DEBUG=True?'

from ov.models import *
from ov.importer import *

name_pat = re.compile(r"^(?:.+)[/](?P<label>[^/]+)[/]?$")
words_pat = re.compile(r"^(?:[A-Z][a-z]+){2,}$")
word_pat = re.compile(r"(?P<word>[A-Z][a-z]+)")

gc.enable()


print "will fix labels now (%s)"%sys.getfilesystemencoding()
date = time.mktime(datetime.datetime.utcnow().timetuple())
i=0
for uri in URI.objects.iterator():
    print "%d | %s" % (i, str(uri))
    i+=1
    # retrieve label from URI
    if not uri.label:
        m = name_pat.match(uri.uri)
        if m:
            gdict = m.groupdict()
            label = gdict['label']
            if label:
                if words_pat.match(label):
                    l = ""
                    for m in word_pat.finditer(label):
                        if m.start() > 0:
                            l += m.group().lower()+" "
                        else:
                            l += m.group()+" "
                    label = l.strip()
                # continue                
                uri.label = label.replace('_', ' ')
                uri.save()
                print "extracted label for %s" % str(uri)
            else:
                print u"Did not find label for %s " % str(uri)
        else:
            print u"Could not get label for %s " % str(uri)
            
    if not i%100:
        gc.collect()
    
print u"Completed in ", time.mktime(datetime.datetime.utcnow().timetuple()) - date

