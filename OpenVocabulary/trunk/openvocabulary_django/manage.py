#!/usr/bin/env python
import os, sys

#following lines are required by PyDev's debugger; feel free to remove if you don't use it
#sys.path.append("/home/mateusz/eclipse/plugins/org.python.pydev_2.6.0.2012062818/pysrc/")
#import pydevd
#pydevd.patch_django_autoreload()

if __name__ == "__main__":
    os.environ["DJANGO_SETTINGS_MODULE"] = "ov_django.settings"

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
