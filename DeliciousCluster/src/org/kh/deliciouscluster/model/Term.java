package org.kh.deliciouscluster.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.kh.deliciouscluster.engine.Engine;

import uk.ac.shef.wit.simmetrics.similaritymetrics.Levenshtein;

/**
 * This class extends the concepts of mappable tag with ability to add "child"
 * tags
 * 
 * @author sebastiankruk
 * 
 */
public class Term {

	/**
	 * This field will hold the sub-tags
	 */
	private List<Term> children;
	/**
	 * Yellow pages for all registered tags
	 */
	private static final Map<String, Term> TERMS = new HashMap<String, Term>();
	/**
	 * Depth of the tag in a hierarchy of taxonomy
	 */
	private int depth;
	/**
	 * A unique ID of the tag
	 */
	private String id;
	/**
	 * Similarity metric used by this object
	 */
	private Levenshtein metric;
	/**
	 * Order of the tag - usually alphabetic
	 */
	private int order_;
	/**
	 * size of the tag - how many times is used
	 */
	protected Double size_;
	/**
	 * This is additional information (compared to Mappable interface)
	 */
	protected String label_;
	/**
	 * lowercase label
	 */
	protected String value_;
	/**
	 * similarity threshold value
	 */
	private Float threshold_;

	public Term(String _tag, Double _size, Term... mappableTags) {
		this(_tag, _size);
		this.addChildren(mappableTags);
	}

	/**
	 * Clone given generic <code>TreeMappableTag</code> to this type
	 * 
	 * @param label_
	 */
	public Term(Term term) {
		this(term.getTerm(), term.getSize());
		this.setOrder(term.getOrder());
		this.setDepth(term.getDepth());
		this.setThreshold(term.threshold_);

		for (Term child : term.getChildren())
			this.children.add(new Term(child));

	}

	public Term(String _tag, Double _size) {
		this.label_ = _tag;
		this.size_ = _size;
		this.order_ = 0;
		this.depth = 0;
		this.threshold_ = 1.0F;

		this.value_ = (this.label_ != null) ? this.label_.toLowerCase() : null;

		this.metric = new Levenshtein();
		this.children = new ArrayList<Term>();

		synchronized (TERMS) {
			String safename = _tag.replaceAll("[&\"\\s]+", "_");
			String _id = safename + (new Date().getTime())
					+ Engine.random.nextInt(100000);
			while (TERMS.containsKey(_id)) {
				_id = safename + (new Date().getTime())
						+ Engine.random.nextInt(100000);
			}
			this.id = _id;
			TERMS.put(this.id, this);
		}
	}

	/**
	 * Performs batch operation on a set of mappable tags converting it to a
	 * list of <code>{@link Term}</code>
	 * 
	 * @param terms
	 * @return
	 */
	public static final List<Term> get(Term... terms) {
		List<Term> result = new ArrayList<Term>();

		for (Term term : terms)
			result.add(term);

		return result;
	}

	public Collection<String> getLabels() {
		Collection<String> result = new HashSet<String>();

		for (Term term : this.children)
			result.addAll(term.getLabels());

		if (!"".equals(this.getTerm())) //$NON-NLS-1$
			result.add(this.getTerm());

		return result;
	}

	/**
	 * Adds one or more mappable tags. Sets their depth to this.depth + 1
	 * 
	 * @param terms
	 */
	public void addChildren(Term... terms) {
		for (Term term : terms) {
			if (term != null) {
				Term child = term;
				child.setDepth(this.getDepth() + 1);
				this.children.add(child);
			}
		}
	}
	
	/**
	 * Caches list of posts associated with this tag
	 * @param _posts
	 */
	public List<Post> getPosts(Engine _delicious) {
		List<Post> result;
		Set<Post> _result = new HashSet<Post>();
		Collection<String> _tags = this.getTags();
		
		for(String _tag : _tags) 
			_result.addAll(_delicious.getPostsPerTag(_tag));

		result = new ArrayList<Post>(_result);
		
		return result;
	}

	public Collection<String> getTags() {
		Collection<String> result = new HashSet<String>();
		
		for(Term _tag : this.children)
			result.addAll(_tag.getTags());
		
		if(!"".equals(this.getTerm())) //$NON-NLS-1$
			result.add(this.getTerm());
		
		return result;
	}
	

	/**
	 * Returns a not-null, unmidifiable list of children tags
	 * 
	 * @return
	 */
	public List<Term> getChildren() {
		return Collections.unmodifiableList(this.children);
	}

	/**
	 * Removes a batch of children tags
	 * 
	 * @param _children
	 */
	public void removeChildren(Term... _children) {
		this.children.removeAll(Arrays.asList(_children));
	}

	/**
	 * @param otherObject
	 *            please note that this parameter should be
	 *            {@link StringSimilar}
	 * @see org.kh.deliciouscluster.model.kh.digime.berryimporter.delicious.model.Term#equals(com.kh.digime.berryimporter.delicious.similar.Similar)
	 */
	public boolean similar(Term otherObject) {
		boolean result;

		if ("".equals(this.getTerm()) && this.getChildrenSize() > 0) //$NON-NLS-1$
			result = true; // in this implementation it is allowed that the tag
		// name is not set up - which means that it is a
		// cluster without a name selected yet
		else {
			float sim = (this.getValue().equalsIgnoreCase(otherObject
					.getValue())) ? 1.0F : this.metric.getSimilarity(this
					.getValue(), otherObject.getValue());
			result = sim >= this.threshold_;
		}

		// -- check all children for similarity
		if (result)
			for (Term child : this.getChildren()) {
				result &= child.similar(otherObject);
				if (!result)
					break;
			}

		return result;
	}

	public float similarity(Term term) {
		return (this.value_.equalsIgnoreCase(term.value_)) ? 1.0F : this.metric
				.getSimilarity(this.value_, term.value_);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kh.digime.berryimporter.utils.TreeMappableTag#toString()
	 */
	@Override
	public String toString() {
		return this.getId()
				+ "(" + this.getSize() + ")" + ((this.getChildrenSize() > 0) ? " " + this.getChildren() : ""); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	}

	/**
	 * sets depth and propogates the value to children
	 * 
	 * @see org.kh.deliciouscluster.model.kh.digime.berryimporter.delicious.model.Term#setDepth(int)
	 */
	public void setDepth(int _depth) {
		this.depth = _depth;

		for (Term child : this.children) {
			child.setDepth(this.getDepth() + 1);
		}
	}

	public double getSize() {
		if (this.size_ == null) {
			double sz = 0;
			for (Term child : this.children)
				sz += child.getSize();

			this.setSize(sz);
		}

		return this.size_;
	}

	/**
	 * Remove all children
	 */
	public void clearChildren() {
		this.children.clear();
	}

	/**
	 * Returns >=0 size of the list of children
	 * 
	 * @return
	 */
	public int getChildrenSize() {
		return (this.children == null) ? 0 : this.children.size();
	}

	/**
	 * Returns a TreeMappableTag object cached under given ID
	 * 
	 * @param id
	 * @return
	 */
	public static final Term getById(String id) {
		synchronized (TERMS) {
			return TERMS.get(id);
		}
	}

	@Override
	protected void finalize() throws Throwable {
		synchronized (TERMS) {
			TERMS.remove(this.id);
		}
		super.finalize();
	}

	public int getDepth() {
		return this.depth;
	}

	public String getId() {
		return id;
	}

	public int getOrder() {
		return this.order_;
	}

	/**
	 * Returns a string representing this tag
	 * 
	 * @return
	 */
	public String getTerm() {
		return this.label_;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.kh.digime.berryimporter.delicious.similar.StringSimilar#getValue()
	 */
	public String getValue() {
		return this.value_;
	}

	public void setOrder(int _order) {
		this.order_ = _order;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.umd.cs.treemap.Mappable#setSize(double)
	 */

	public void setSize(double _size) {
		this.size_ = _size;
	}

	/**
	 * Sets the string description of this tag. <b>NOTE</b> if {@value _tag} ==
	 * <code>""</code> then the {@link Term.size} is <code>null</code>ed - to
	 * indicate that that tag is a parent tag in the tree
	 * 
	 * @param _tag
	 */
	public void setTag(String _tag) {
		this.label_ = _tag;
		this.value_ = (this.label_ != null) ? this.label_.toLowerCase() : null;
		if ("".equals(_tag)) //$NON-NLS-1$
			this.size_ = null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.kh.digime.berryimporter.delicious.similar.Similar#setThreshold(java
	 * .lang.Float)
	 */
	public void setThreshold(Float threshold) {
		this.threshold_ = (threshold != null) ? threshold : 1.0F;
	}

	public Float getThreshold() {
		return threshold_;
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(obj);
	}

}
