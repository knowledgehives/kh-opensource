package org.kh.deliciouscluster.model;

import java.util.Date;

/**
 * Post
 * 
 * @author David Czarnecki, skruk
 * @version $Id: Post.java,v 1.2 2007/04/18 09:14:23 skruk Exp $
 * @since 1.0
 */
public class Post {

	private String href;
	private String description;
	private String[] tags;
	private String extended;
	private Date time;
	private boolean shared;
	private String additionalType;

	/**
	 * Construct a new Post
	 * 
	 * @param _href
	 *            Link
	 * @param _description
	 *            Description of link
	 * @param _hash
	 *            Hash of link
	 * @param _tag
	 *            Space-delimited set of tags
	 * @param _time
	 *            Time when link added
	 * @param _shared
	 *            Whether or not the post is shared
	 */
	public Post(String _href, String _description, String _extended,
			Date _time, boolean _shared, String... _tags) {
		this.href = _href;
		this.description = _description;
		this.extended = _extended;
		this.tags = _tags;
		this.shared = _shared;
		this.time = _time;
	}

	/**
	 * Construct a new Post
	 * 
	 * @param _href
	 *            Link
	 * @param _description
	 *            Description of link
	 * @param _hash
	 *            Hash of link
	 * @param _tag
	 *            Space-delimited set of tags
	 * @param _time
	 *            Time when link added
	 * @param _shared
	 *            Whether or not the post is shared
	 */
	public Post(String _href, String _description, String _extended,
			Date _time, boolean _shared, String _additionalType,
			String... _tags) {
		this.href = _href;
		this.description = _description;
		this.extended = _extended;
		this.tags = _tags;
		this.shared = _shared;
		this.time = _time;
		this.additionalType = _additionalType;
	}

	/**
	 * @return the href
	 */
	public String getHref() {
		return href;
	}

	/**
	 * @param _href
	 *            the href to set
	 */
	public void setHref(String _href) {
		this.href = _href;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param _description
	 *            the description to set
	 */
	public void setDescription(String _description) {
		this.description = _description;
	}

	/**
	 * @return the tags
	 */
	public String[] getTags() {
		return tags;
	}

	/**
	 * @param _tags
	 *            the tags to set
	 */
	public void setTags(String... _tags) {
		this.tags = _tags;
	}

	/**
	 * @return the extended
	 */
	public String getExtended() {
		return extended;
	}

	/**
	 * @param _extended
	 *            the extended to set
	 */
	public void setExtended(String _extended) {
		this.extended = _extended;
	}

	/**
	 * @return the shared
	 */
	public boolean isShared() {
		return shared;
	}

	/**
	 * @param _shared
	 *            the shared to set
	 */
	public void setShared(boolean _shared) {
		this.shared = _shared;
	}

	/**
	 * @return the time
	 */
	public Date getTime() {
		return time;
	}

	/**
	 * @param _time
	 *            the time to set
	 */
	public void setTime(Date _time) {
		this.time = _time;
	}

	/**
	 * @return the additionalType
	 */
	public String getAdditionalType() {
		return additionalType;
	}

	/**
	 * @param additionalType
	 *            the additionalType to set
	 */
	public void setAdditionalType(String _additionalType) {
		this.additionalType = _additionalType;
	}

	@Override
	public String toString() {
		return href + '\n';
	}

	/**
	 * two Posts are considered equal if they point to the same URL have the
	 * same description and same tags
	 */
	@Override
	public boolean equals(Object o) {
		if (o instanceof Post) {
			Post post = (Post) o;
			if (getHref().equals(post.getHref())
					&& getDescription().equals(post.getDescription())
					&& compareTags(post.getTags()))
				return true;
		}
		return false;
	}

	/**
	 * Compares this.tags with the given tags
	 * 
	 * @param tags2
	 * @return <tt>true</tt> If all tags are the same then <br/> if at least one
	 *         is different the return false;
	 */
	private boolean compareTags(String... tags2) {

		if (tags.length != tags2.length)
			return false;
		for (String tag : tags) {
			boolean match = false;
			for (String tag2 : tags2) {
				if (tag.equals(tag2)) {
					match = true;
					break;
				}
			}
			if (!match) {
				return false;
			}
		}
		return true;
	}

}