package org.kh.deliciouscluster.model;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Tag
 * 
 * based on:
 * 
 * @author David Czarnecki, skruk
 * @version $Id: Tag.java,v 1.2 2007/04/18 09:14:23 skruk Exp $
 * @since 1.0
 */
public class Tag implements Comparable<Tag> {

	private int count;
	private String tag;

	/**
	 * Construct a new tag
	 * 
	 * @param _count
	 *            Number of links for tag
	 * @param _tag
	 *            Tag name
	 */
	public Tag(int _count, String _tag) {
		this.count = _count;
		this.tag = _tag;
	}

	/**
	 * Increments internal count by 1
	 * 
	 * @return
	 */
	public int incCount() {
		return ++count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kh.digime.berryimporter.delicious.model.CloudTag#getCount()
	 */
	public int getCount() {
		return count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kh.digime.berryimporter.delicious.model.CloudTag#setCount(int)
	 */
	public void setCount(int _count) {
		this.count = _count;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kh.digime.berryimporter.delicious.model.CloudTag#getTag()
	 */
	public String getTag() {
		return tag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.kh.digime.berryimporter.delicious.model.CloudTag#setTag(java.lang
	 * .String)
	 */
	public void setTag(String _tag) {
		this.tag = _tag;
	}

	/**
	 * Object as count:tag string
	 * 
	 * @return count:tag
	 */
	@Override
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append(count).append(':').append(tag);

		return result.toString();
	}

	/**
	 * Changes tags encoded in a form of spaces seperated list into tags
	 * 
	 * @param _tag
	 * @return
	 */
	public static final String[] explode(String _tag) {
		Set<String> res = new HashSet<String>();
		String tags = _tag;

		while (tags != null) {
			tags = getFirstTag(tags, res);
		}

		return res.toArray(new String[res.size()]);
	}

	/**
	 * Changes given tags into a space separated string
	 * 
	 * @param _tags
	 * @return
	 */
	public static final String implode(String... _tags) {
		StringBuilder sb = new StringBuilder();
		for (String tag : _tags) {
			sb.append(tag);
			sb.append(' ');
		}
		return sb.toString();
	}

	/**
	 * Puts first tag to the set, and retuns the rest
	 * 
	 * @param _tags
	 * @param stags
	 * @return
	 */
	protected static final String getFirstTag(String _tags, Set<String> stags) {
		String res = null;

		if (_tags.matches(patExplodeTags.toString())) {
			Matcher mv = patExplodeTags.matcher(_tags);

			if (mv.find()) {
				if (mv.groupCount() > 0)
					stags.add(mv.group(1));
				if (mv.groupCount() > 1)
					res = mv.group(2);
			}
		}

		return res;
	}

	/**
	 * Allows to extract tags:
	 * 
	 * <pre>
	 * &circ;(?:([&circ; &quot;]+|(?:[&quot;][&circ;\&quot;]+[&quot;]))(?:[, ]+(.*))?)+$
	 * </pre>
	 */
	protected static final Pattern patExplodeTags = Pattern
			.compile("^(?:([^ \"]+|(?:[\"][^\"]+[\"]))(?:[, ]+(.*))?)+$"); //$NON-NLS-1$

	public int compareTo(Tag arg0) {
		return this.getTag().compareTo(arg0.getTag());
	}

}