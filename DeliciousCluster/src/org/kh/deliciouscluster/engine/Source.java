package org.kh.deliciouscluster.engine;

import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.List;

import org.kh.deliciouscluster.model.Bundle;
import org.kh.deliciouscluster.model.Post;
import org.kh.deliciouscluster.model.Tag;

import del.icio.us.Delicious;
import del.icio.us.DeliciousNotAuthorizedException;
import del.icio.us.DeliciousNotAvailableException;

/**
 * Wraps operations on the Delicious tagging source
 * 
 * @author sebastiankruk
 */
public class Source {

	private Delicious delicious_;

	public Source() {
		this.delicious_ = null;
	}

	public List<Post> getAllPosts() throws DeliciousNotAuthorizedException,
			DeliciousNotAvailableException {
		List<Post> allposts = new ArrayList<Post>();
		List<del.icio.us.beans.Post> delposts = null;

		try {
			delposts = this.delicious_.getAllPosts();
		} catch (DeliciousNotAuthorizedException e) {
			throw new DeliciousNotAuthorizedException(e.toString());
		} catch (DeliciousNotAvailableException e) {
			throw new DeliciousNotAvailableException(e.toString());
		} finally {
			if (delposts != null)
				for (del.icio.us.beans.Post post : delposts) {
					allposts.add(new Post(post.getHref(),
							post.getDescription(), post.getExtended(), post
									.getTimeAsDate(), post.isShared(), post
									.getTagsAsArray(" "))); //$NON-NLS-1$
				}
		}

		return allposts;
	}

	public List<Bundle> getBundles() throws DeliciousNotAuthorizedException {
		List<Bundle> bundles = new ArrayList<Bundle>();
		List<del.icio.us.beans.Bundle> delbundle = null;

		try {
			delbundle = this.delicious_.getBundles();
		} catch (DeliciousNotAuthorizedException e) {
			throw new DeliciousNotAuthorizedException(e.toString());
		} catch (DeliciousNotAvailableException e) {
			throw new DeliciousNotAuthorizedException(e.toString());
		} finally {
			if (delbundle != null)
				for (del.icio.us.beans.Bundle bundle : delbundle) {
					bundles.add(new Bundle(bundle.getName(), bundle
							.getTagsAsArray()));
				}
		}

		return bundles;
	}

	public List<Tag> getTags() throws DeliciousNotAuthorizedException {
		List<Tag> tags = new ArrayList<Tag>();
		List<del.icio.us.beans.Tag> deltags = null;

		try {
			deltags = this.delicious_.getTags();
		} catch (DeliciousNotAuthorizedException e) {
			throw new DeliciousNotAuthorizedException(e.toString());
		} catch (DeliciousNotAvailableException e) {
			throw new DeliciousNotAuthorizedException(e.toString());
		} finally {
			if (deltags != null)
				for (del.icio.us.beans.Tag tag : deltags) {
					tags.add(new Tag(tag.getCount(), tag.getTag()));
				}
		}

		return tags;
	}

	public void setCredentials(String login, String pass) {
		if (login == null || pass == null || "".equals(login.trim())
				|| "".equals(pass.trim()))
			throw new IllegalArgumentException(
					"login and password cannot be empty");

		this.delicious_ = new Delicious(login, pass);

		int credCheck = 0;

		try {
			credCheck = this.delicious_.checkCredentials();
		} catch (Throwable e) {
			Engine.logger.warn(e.toString());
		} finally {
			if (credCheck != 200) {
				this.delicious_ = null;
				throw new AccessControlException(
						"could not login to del.icio.us");
			}
		}
	}
}
