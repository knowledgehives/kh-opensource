package org.kh.deliciouscluster.process;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.kh.deliciouscluster.model.Term;

/**
 * @author sebastiankruk
 * 
 */
public class ClusterNaming {

	/**
	 * This is a threshold to be used to recognize multiclusters
	 */
	private static final double MULTI_THRESHOLD = 0.01F;

	/**
	 * Please note that this method requires and retuns
	 * <code>List<? extends TreeMappableTag></code>.
	 * 
	 * @see com.kh.digime.berryimporter.delicious.preprocess.ClusterPreProcessor#process(java.util.List,
	 *      java.util.Map, List, com.kh.digime.berryimporter.ExecuteEngine)
	 */
	public List<Term> process(List<Term> tags) {

		List<Term> result = new ArrayList<Term>();

		for (Term tag : tags) {
			if ("".equals(tag.getTerm())) //$NON-NLS-1$
				nameCluster(tag);

			result.add(tag);
		}

		return result;
	}

	/**
	 * Tries to figure out a name for the cluster
	 * 
	 * @param cluster
	 */
	protected void nameCluster(Term cluster) {
		List<Term> children = cluster.getChildren();
		int size = children.size();

		if (size == 0)
			return; // bug somewhere above creates cluster with no children ?
		if (size == 1) // -- what kind of a cluster it is btw ?! - this should
						// not happen
			cluster.setTag(children.get(0).getTerm());
		else if (size == 2
				&& children.get(0).similarity(children.get(1)) == 1.0F)
			cluster.setTag(children.get(0).getValue());
		else {
			for (Term subcluster : children)
				if ("".equals(subcluster.getTerm())) //$NON-NLS-1$
					nameCluster(subcluster);

			// -- calculate average values
			// TODO include importance of the tags in this calculations
			float[][] aasim = new float[size][size];
			float[] aavg = new float[size];

			for (int i = 0; i < size - 1; i++) {
				aasim[i][i] = 1.0F;
				aavg[i] += 1.0F / size;

				for (int j = i + 1; j < size; j++) {
					aasim[i][j] = children.get(i).similarity(children.get(j));
					aasim[j][i] = aasim[i][j];
					aavg[i] += aasim[i][j] / size;
					aavg[j] += aasim[i][j] / size;
				}// 2nd for
			}
			aavg[size - 1] += 1.0F / size;

			float avg = 0.0F;
			double dev = 0.0;
			for (float f : aavg)
				avg += f / size;

			if (size > 3) {
				for (float f : aavg)
					dev += (f - avg) * (f - avg);
				dev /= size;
				dev = Math.sqrt(dev);
			}

			if (dev < MULTI_THRESHOLD && size > 3) {
				// -- there are more then one distinct sub-clusters
				// TODO
				Set<Integer> sused = new HashSet<Integer>();
				StringBuilder name = new StringBuilder();

				for (int i = 0; i < size - 1; i++)
					if (!sused.contains(i)) {

						float max = Float.MIN_VALUE;
						int imax = -1;

						for (int j = i + 1; j < size; j++)
							if (aasim[i][j] >= avg) {
								sused.add(j);

								if (aasim[i][j] > max) {
									max = aasim[i][j];
									imax = j;
								}
							}// for each matching/similar tag

						if (imax > -1)
							name.append(
									children.get(imax).getTerm().toLowerCase())
									.append('&');
					}// for each row

				if (name.length() > 1)
					name.delete(name.length() - 1, name.length());

				cluster.setTag(name.toString());
			} else {
				// -- there seems to be one leading cluster ide - let's select
				// the most popular concept
				float max = aavg[0];
				int elected = 0;

				for (int i = 1; i < size; i++)
					if (max < aavg[i]) {
						max = aavg[i];
						elected = i;
					}

				cluster.setTag(children.get(elected).getTerm().toLowerCase());
			} // -- not multi-cluster

		}// -- else

	}

}
