package business.delegates
{

    import model.ModelLocator;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
import mx.rpc.events.FaultEvent;
import mx.rpc.events.ResultEvent;
import mx.rpc.http.HTTPService;
import mx.utils.StringUtil;

public class TagDelegate
	{
	    //private var __locator:ServiceLocator = ServiceLocator.getInstance();
		private var __service:HTTPService;
		//private var __responder:IResponder;
		private var __model:ModelLocator = ModelLocator.getInstance();
        private var __onResults_searchTag;
        private var __onFaults_searchTag;

		public function TagDelegate(onResults_searchTag, onFaults_searchTag) // responder:IResponder)
		{
            __onResults_searchTag = onResults_searchTag;
            __onFaults_searchTag = onFaults_searchTag;

        }
		
		public function searchTags(searchString:String, page:String):void
		{
            this.__service = new HTTPService(); //__locator.getHTTPService("searchTags");
            this.__service.useProxy = false;
            //__responder = responder;
            this.__service.resultFormat = "text";
            this.__service.contentType = "application/x-www-form-urlencoded";
            this.__service.method = "GET";
//            this.__service.headers = "Content-Type: text/javascript";
            this.__service.addEventListener(FaultEvent.FAULT, __onFaults_searchTag);
            this.__service.addEventListener(ResultEvent.RESULT, __onResults_searchTag);

            var url:String = StringUtil.substitute(__model.searchTagServiceAddress, searchString );
            trace("CALLING url = "+url);
			this.__service.url = url

//			var query:XML = <query xmlns="http://openlinksw.com/services/facets/1.0" inference="" same-as=""><text>{searchString}</text></query>;
//			var view:XML = <view type="text" limit="10" offset=""/>;
//			view.@offset = Number(page) * __model.tagsPerSearch;
//			query.appendChild(view);

			var token:AsyncToken = __service.send();
			//token.addResponder(__responder);
		}
	}
}