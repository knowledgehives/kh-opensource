package business.delegates
{
	import com.adobe.cairngorm.business.ServiceLocator;
	
	import model.ModelLocator;
	
	import mx.rpc.AsyncToken;
	import mx.rpc.IResponder;
	import mx.rpc.http.HTTPService;
	
	import valueObjects.VideoTag;

	public class TaggingDelegate
	{
		private var __locator:ServiceLocator = ServiceLocator.getInstance();
		private var __loadService:HTTPService;
		private var __saveService:HTTPService;
		private var __responder:IResponder;
		private var __model:ModelLocator = ModelLocator.getInstance();

		public function TaggingDelegate(responder:IResponder)
		{
			__loadService = __locator.getHTTPService("tagsIn");
			__saveService = __locator.getHTTPService("storeTag");
			__responder = responder;
		}
		
		public function loadTaggings():void
		{
			__loadService.url = __model.baseServerAddress + __model.taggingServiceName;
			var token:AsyncToken = __loadService.send({documentURI:__model.documentURI, responseFormat:"xml"});
			token.addResponder(__responder);
		}
		
		public function saveTagging(videoTag:VideoTag):void
		{
			var postObject:Object = videoTag.toPostObject();
			__saveService.url = __model.baseServerAddress + __model.taggingServiceName;
			__saveService.method = "POST";
			var token:AsyncToken = __saveService.send(postObject);
			token.addResponder(__responder);
		}

		public function loadTaggings_proxy():void
		{
			__loadService.url = "assets/tags.xml";
			var token:AsyncToken = __loadService.send({documentURI:__model.documentURI, responseFormat:"xml"});
			token.addResponder(__responder);
		}
		
		public function saveTagging_proxy(videoTag:VideoTag):void
		{
			var newTagging:XML = videoTag.toXMLObject();
			__model.xmlTags.appendChild(newTagging);
			__responder.result(null);
		}
	}
}