package business
{
	import business.commands.AcquireParametersCommand;
	import business.commands.AddTaggingCommand;
	import business.commands.LoadTaggingsCommand;
	import business.commands.PlayVideoCommand;
	import business.commands.SaveTaggingCommand;
	import business.commands.SearchTagCommand;
	import business.commands.SeekToTimeCommand;
	import business.commands.ShowTaggingCommand;
	import business.events.AcquireParametersEvent;
	import business.events.AddTaggingEvent;
	import business.events.LoadTaggingsEvent;
	import business.events.PlayVideoEvent;
	import business.events.SaveTaggingEvent;
	import business.events.SearchTagEvent;
	import business.events.SeekToTimeEvent;
	import business.events.ShowTaggingEvent;
	
	import com.adobe.cairngorm.control.FrontController;

	public class JDLController extends FrontController
	{
		public function JDLController()
		{
			super();
			addCommand(AcquireParametersEvent.EVENT_ID,AcquireParametersCommand);
			addCommand(LoadTaggingsEvent.EVENT_ID,LoadTaggingsCommand);
			addCommand(PlayVideoEvent.EVENT_ID,PlayVideoCommand);
			addCommand(ShowTaggingEvent.EVENT_ID,ShowTaggingCommand);
			addCommand(AddTaggingEvent.EVENT_ID,AddTaggingCommand);
			addCommand(SeekToTimeEvent.EVENT_ID,SeekToTimeCommand);
			addCommand(SaveTaggingEvent.EVENT_ID,SaveTaggingCommand);
			addCommand(SearchTagEvent.EVENT_ID,SearchTagCommand);
		}
	}
}
