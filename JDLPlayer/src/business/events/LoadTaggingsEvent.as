package business.events
{
	import com.adobe.cairngorm.control.CairngormEvent;
	
	public class LoadTaggingsEvent extends CairngormEvent
	{
		static public var EVENT_ID:String = "loadTags";

		public function LoadTaggingsEvent()
		{
			super(EVENT_ID);
		}
	}
}