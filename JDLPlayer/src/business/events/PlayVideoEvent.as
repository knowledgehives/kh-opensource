package business.events
{
	import com.adobe.cairngorm.control.CairngormEvent;
	
	public class PlayVideoEvent extends CairngormEvent
	{
		static public var EVENT_ID:String = "playVideo";

		public function PlayVideoEvent()
		{
			super(EVENT_ID);
		}
	}
}