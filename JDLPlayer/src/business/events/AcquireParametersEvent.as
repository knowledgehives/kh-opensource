package business.events
{
	import com.adobe.cairngorm.control.CairngormEvent;
	
	public class AcquireParametersEvent extends CairngormEvent
	{
		static public var EVENT_ID:String = "acquireParameters";
		
		public function AcquireParametersEvent()
		{
			super(EVENT_ID);
		}
	}
}