package business.events
{
	import com.adobe.cairngorm.control.CairngormEvent;
	
	import mx.core.UIComponent;
	
	public class SearchTagEvent extends CairngormEvent
	{
		static public var EVENT_ID:String = "searchTag";
		public var searchString:String;
		public var page:String;
		public var component:UIComponent;
		
		public function SearchTagEvent(component:UIComponent,searchString:String,page:String = "0")
		{
			this.page = page;
			this.searchString = searchString;
			this.component = component;
			super(EVENT_ID);
		}
	}
}