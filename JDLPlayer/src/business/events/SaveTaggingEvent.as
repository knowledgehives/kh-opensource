package business.events
{
	import com.adobe.cairngorm.control.CairngormEvent;
	
	import valueObjects.VideoTag;
	
	public class SaveTaggingEvent extends CairngormEvent
	{
		static public var EVENT_ID:String = "saveTag";
		public var videoTag:VideoTag;

		public function SaveTaggingEvent(videoTag:VideoTag)
		{
			this.videoTag = videoTag;
			super(EVENT_ID);
		}
	}
}