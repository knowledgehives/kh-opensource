package business.events
{
	import com.adobe.cairngorm.control.CairngormEvent;
	
	public class SeekToTimeEvent extends CairngormEvent
	{
		static public var EVENT_ID:String = "seekToTime";
		
		public var time:int;

		public function SeekToTimeEvent(time:int)
		{
			this.time = time;
			super(EVENT_ID);
		}
	}
}