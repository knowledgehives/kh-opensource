package business.events
{
	import com.adobe.cairngorm.control.CairngormEvent;
	
	public class AddTaggingEvent extends CairngormEvent
	{
		static public var EVENT_ID:String = "addTag";

		public function AddTaggingEvent()
		{
			super(EVENT_ID);
		}
	}
}