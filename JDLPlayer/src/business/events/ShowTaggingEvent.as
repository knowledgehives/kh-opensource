package business.events
{
	import com.adobe.cairngorm.control.CairngormEvent;
	
	import renderers.TaggingsRenderer;
	
	public class ShowTaggingEvent extends CairngormEvent
	{
		static public var EVENT_ID:String = "showTag";
		public var tagsRenderer:TaggingsRenderer;
		public var hide:Boolean;

		public function ShowTaggingEvent(tagsRenderer:TaggingsRenderer,hide:Boolean = false)
		{
			super(EVENT_ID);
			this.tagsRenderer = tagsRenderer;
			this.hide = hide;
		}
	}
}