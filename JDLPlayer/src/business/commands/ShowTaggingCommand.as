package business.commands
{
	import business.events.ShowTaggingEvent;
	
	import com.adobe.cairngorm.commands.ICommand;
	import com.adobe.cairngorm.control.CairngormEvent;
	
	import renderers.TaggingsRenderer;
	import components.TooltipPopup;
	
	import flash.geom.Point;
	
	import model.ModelLocator;
	
	import mx.core.FlexGlobals;
	import mx.managers.PopUpManager;
	import mx.managers.ToolTipManager;
	
	import valueObjects.VideoTag;
	
	
	public class ShowTaggingCommand implements ICommand
	{
		private var __model:ModelLocator = ModelLocator.getInstance();
		
		public function execute(event:CairngormEvent):void
		{
			var tagsRenderer:TaggingsRenderer = (event as ShowTaggingEvent).tagsRenderer;
			var hide:Boolean = (event as ShowTaggingEvent).hide;
			var sameTag:Boolean = true;
			var traceStr:String = "title: " + (tagsRenderer.data as VideoTag).title + " hide: " + hide;
			if(__model.tooltipPopup)
			{
				traceStr += " curr title: " + __model.tooltipPopup.videoTag.title;
			}
			trace(traceStr);
			if(__model.tooltipPopup != null)
			{
				if(hide)
				{
					sameTag = Boolean(__model.tooltipPopup.videoTag == tagsRenderer.data as VideoTag);
				}
				if(!sameTag && hide)
				{
					return;
				}
				PopUpManager.removePopUp(__model.tooltipPopup);
			}
			if(!hide)
			{
				__model.tooltipPopup = new TooltipPopup();
				__model.tooltipPopup.videoTag = tagsRenderer.data as VideoTag;
				var pt:Point = new Point(tagsRenderer.tagBody.x,tagsRenderer.tagBody.y);
				pt = tagsRenderer.contentToGlobal(pt);
				PopUpManager.addPopUp(__model.tooltipPopup,tagsRenderer);
				__model.tooltipPopup.x = pt.x;
				__model.tooltipPopup.y = pt.y;
			}
		}
	}
}