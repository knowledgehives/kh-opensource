package business.commands
{
	import com.adobe.cairngorm.commands.ICommand;
	import com.adobe.cairngorm.control.CairngormEvent;
	
	import model.ModelLocator;
	
	import mx.core.FlexGlobals;
	
	public class PlayVideoCommand implements ICommand
	{
		private var __model:ModelLocator = ModelLocator.getInstance();
		
		public function execute(event:CairngormEvent):void
		{
			FlexGlobals.topLevelApplication.videoPlayer.source = __model.videoAddress;
		}
	}
}