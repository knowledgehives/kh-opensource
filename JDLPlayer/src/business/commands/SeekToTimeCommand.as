package business.commands
{
	import business.events.SeekToTimeEvent;
	
	import com.adobe.cairngorm.commands.ICommand;
	import com.adobe.cairngorm.control.CairngormEvent;
	
	import mx.core.FlexGlobals;
	
	public class SeekToTimeCommand implements ICommand
	{
		
		public function execute(event:CairngormEvent):void
		{
			var time:int = (event as SeekToTimeEvent).time;
			FlexGlobals.topLevelApplication.videoPlayer.seek(time);
		}
	}
}