package business.commands
{
	import com.adobe.cairngorm.commands.ICommand;
	import com.adobe.cairngorm.control.CairngormEvent;
	
	import components.AddTaggingPopup;
	
	import flash.display.DisplayObject;
	
	import model.ModelLocator;
	
	import mx.collections.ArrayCollection;
	import mx.core.FlexGlobals;
	import mx.managers.PopUpManager;
	
	public class AddTaggingCommand implements ICommand
	{
		
		private var __model:ModelLocator = ModelLocator.getInstance();

		public function execute(event:CairngormEvent):void
		{
			if(__model.addTagPopup != null)
			{
				PopUpManager.removePopUp(__model.addTagPopup);
			}
			__model.addedTags = new ArrayCollection();
			__model.addTagPopup = new AddTaggingPopup();
			__model.addTagPopup.videoPlayer = FlexGlobals.topLevelApplication.videoPlayer;
			PopUpManager.addPopUp(__model.addTagPopup,DisplayObject(FlexGlobals.topLevelApplication),true);
			PopUpManager.centerPopUp(__model.addTagPopup);
		}
	}
}