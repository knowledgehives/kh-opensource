package business.commands
{
	import com.adobe.cairngorm.commands.ICommand;
	import com.adobe.cairngorm.control.CairngormEvent;
	
	import model.ModelLocator;
	
	import mx.core.Application;
	import mx.core.FlexGlobals;
	
	public class AcquireParametersCommand implements ICommand
	{
		private var __model:ModelLocator = ModelLocator.getInstance();
		private static const DEBUG:Boolean = true;
		
		public function execute(event:CairngormEvent):void
		{
			if(FlexGlobals.topLevelApplication.parameters.srv == null) {
				if(DEBUG) {
					__model.baseServerAddress = "http://5.90.150.72:7390/mmt/";
				}
			} else {
				__model.baseServerAddress = FlexGlobals.topLevelApplication.parameters.srv;
			}
            if(FlexGlobals.topLevelApplication.parameters.movie == null) {
         		if(DEBUG) {
         			__model.videoAddress = "assets/szczepanirenka.flv";
                }
         	} else {
         		__model.videoAddress = FlexGlobals.topLevelApplication.parameters.movie;
         	}

            trace("video = "+__model.videoAddress);

			__model.documentURI = __model.baseServerAddress + __model.videoAddress;
            FlexGlobals.topLevelApplication.videoPlayer.source = __model.videoAddress;

			if(FlexGlobals.topLevelApplication.parameters.tagger == null)
			{
				if(DEBUG)
				{
					__model.taggerURI = "http://jeromedl.test";
				}
			}
			else
			{
				__model.taggerURI = FlexGlobals.topLevelApplication.parameters.tagger;
			}
		}
	}
}