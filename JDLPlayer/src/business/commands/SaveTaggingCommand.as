package business.commands
{
	import business.delegates.TaggingDelegate;
	import business.events.LoadTaggingsEvent;
	import business.events.SaveTaggingEvent;
	
	import com.adobe.cairngorm.commands.ICommand;
	import com.adobe.cairngorm.control.CairngormEvent;
	
	import mx.controls.Alert;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import valueObjects.VideoTag;
	
	public class SaveTaggingCommand implements ICommand
	{
		public function execute(event:CairngormEvent):void
		{
			var videoTag:VideoTag = (event as SaveTaggingEvent).videoTag;
			var responder:Responder = new Responder(onResults_saveTag,onFaults_saveTag);
			var delegate:TaggingDelegate = new TaggingDelegate(responder);
			delegate.saveTagging(videoTag);
		}
		
		private function onResults_saveTag(event:ResultEvent):void
		{
			var loadTagsEvent:LoadTaggingsEvent = new LoadTaggingsEvent();
			loadTagsEvent.dispatch();
		}
		
		private function onFaults_saveTag(event:FaultEvent):void
		{
			Alert.show("Problem while saving tag: " + event.fault.faultCode,"Error");
		}
	}
}