package business.commands
{
	import business.delegates.TaggingDelegate;
	
	import com.adobe.cairngorm.commands.ICommand;
	import com.adobe.cairngorm.control.CairngormEvent;
	
	import model.Defaults;
	import model.ModelLocator;
	
	import mx.collections.ArrayCollection;
	import mx.collections.Sort;
	import mx.collections.SortField;
	import mx.controls.Alert;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	
	import org.osmf.layout.AbsoluteLayoutFacet;
	
	import valueObjects.VideoTag;
	
	public class LoadTaggingsCommand implements ICommand
	{

		private var __model:ModelLocator = ModelLocator.getInstance();

		public function execute(event:CairngormEvent):void
		{
			var responder:Responder = new Responder(onResults_loadTags,onFaults_loadTags);
			var delegate:TaggingDelegate = new TaggingDelegate(responder);
			delegate.loadTaggings();
		}
		
		public function execute_proxy(event:CairngormEvent):void
		{
			if(__model.xmlTags == null)
			{
				var responder:Responder = new Responder(onResults_loadTags,onFaults_loadTags);
				var delegate:TaggingDelegate = new TaggingDelegate(responder);
				delegate.loadTaggings();
			}
			else
			{
				parseTags(__model.xmlTags);
			}
			
		}
		
		private function onResults_loadTags(event:ResultEvent):void
		{
			__model.xmlTags = new XML(event.result);
			parseTags(__model.xmlTags);
		}
		
		private function parseTags(xmlSource:XML):void
		{
			var tagLayout:ArrayCollection = new ArrayCollection();
			for(var iterator:* in xmlSource.tagging)
			{
				var xmlTaggingDef:XML = xmlSource.tagging[iterator];
				var videoTag:VideoTag = new VideoTag();
				if(xmlTaggingDef.clipping.length() > 0 && xmlTaggingDef.clipping[0].hasComplexContent())
				{
					var xmlClippingDef:XML = xmlTaggingDef.clipping[0];
					videoTag.timeStart = int(xmlClippingDef.startPoint[0].text()[0]);
					videoTag.timeEnd = videoTag.timeStart + int(xmlClippingDef.length[0].text()[0]);
					videoTag.title = String(xmlTaggingDef.title[0].text()[0] || Defaults.TAG_TITLE);
					videoTag.description = String(xmlTaggingDef.description[0].text()[0] || '');
					videoTag.documentURI = String(xmlTaggingDef.documentURI[0].text()[0]);
					videoTag.taggerURI = String(xmlTaggingDef.taggerURI[0].text()[0]);
					videoTag.taggingURI = String(xmlTaggingDef.@uri);
					videoTag.clippingURI = String(xmlClippingDef.@uri);
					videoTag.links = null;
					videoTag.crossReferences = null;
					videoTag.terms = null;
					if(xmlTaggingDef.links.length() > 0 && xmlTaggingDef.links[0].hasComplexContent())
					{
						videoTag.links = new ArrayCollection();
						for(var iLink:* in xmlTaggingDef.links.link)
						{
							videoTag.links.addItem(String(xmlTaggingDef.links.link[iLink].text()[0]));
						}
						
					}
					if(xmlTaggingDef.crossReferences.length() > 0 && xmlTaggingDef.crossReferences[0].hasComplexContent())
					{
						videoTag.crossReferences = new ArrayCollection();
						for(var iReference:* in xmlTaggingDef.crossReferences.crossReference)
						{
							videoTag.crossReferences.addItem(String(xmlTaggingDef.crossReferences.crossReference[iReference].@value));
						}
						
					}
					if(xmlTaggingDef.terms.length() > 0 && xmlTaggingDef.terms[0].hasComplexContent())
					{
						videoTag.terms = new ArrayCollection();
						var newTerm:Object;
						for(var iTerm:* in xmlTaggingDef.terms.term)
						{
							newTerm = new Object();
							newTerm.label = String(xmlTaggingDef.terms.term[iTerm].tag[0].text()[0]);
							newTerm.uri = String(xmlTaggingDef.terms.term[iTerm].@uri);
							videoTag.terms.addItem(newTerm);
						}
						
					}
					var iLevel:int = 0;
					while(iLevel < tagLayout.length && tagLayout[iLevel] != undefined)
					{
						if(tagLayout[iLevel][0].timeStart > videoTag.timeStart) {
							if(tagLayout[iLevel][0].timeStart > videoTag.timeEnd) {
								break;
							} else {
								iLevel++;
							}
						} else {
							for(var i:int = 0; i < tagLayout[iLevel].length && videoTag.timeStart > tagLayout[iLevel][i].timeStart; i++) {
							}
							if((i >= tagLayout[iLevel].length || videoTag.timeEnd < tagLayout[iLevel][i].timeStart) && videoTag.timeStart > tagLayout[iLevel][i-1].timeEnd) {
								break;
							} else {
								iLevel++;
							}
						}
					}
					if(iLevel >= tagLayout.length)
					{
						tagLayout.addItem(new ArrayCollection());
					}
					tagLayout[iLevel].addItem(videoTag);
					var sortField:SortField = new SortField();
					sortField.name = "timeStart";
					sortField.numeric = true;
					var sort:Sort = new Sort();
					sort.fields = [sortField];
					var level:ArrayCollection = tagLayout[iLevel] as ArrayCollection;
					level.sort = sort;
					level.refresh();
				}
			}
			__model.tagLayout = tagLayout;
		}
		
		private function onFaults_loadTags(event:FaultEvent):void
		{
			Alert.show("Problem while acquiring tags: " + event.fault.faultCode,"Error");
		}
		
		private function sortLayout(x:VideoTag,y:VideoTag):int
		{
			if(x.timeStart == y.timeStart) return 0;
			return x.timeStart < y.timeStart ? -1 : 1;
		}
	}
}