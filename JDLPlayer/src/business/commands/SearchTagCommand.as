package business.commands
{
	import business.delegates.TagDelegate;
	import business.events.SearchTagEvent;
	
	import com.adobe.cairngorm.commands.ICommand;
	import com.adobe.cairngorm.control.CairngormEvent;
	
	import flash.geom.Point;
	
	import model.ModelLocator;
	
	import mx.collections.ArrayCollection;
	import mx.controls.Alert;
	import mx.controls.Menu;
	import mx.core.UIComponent;
	import mx.events.MenuEvent;
	import mx.rpc.Responder;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;

    import com.adobe.serialization.json.JSONDecoder;

	public class SearchTagCommand implements ICommand
	{
		private var searchString:String;
		private var page:String;
		private var component:UIComponent;
		private var __model:ModelLocator = ModelLocator.getInstance();
		
		public function execute(event:CairngormEvent):void
		{
			this.searchString = (event as SearchTagEvent).searchString;
			this.page = (event as SearchTagEvent).page;
			this.component = (event as SearchTagEvent).component;
			doSearch();
		}
		
		private function doSearch():void
		{
			//var responder:Responder = new Responder(onResults_searchTag, onFaults_searchTag);
			var tagDelegate:TagDelegate = new TagDelegate(onResults_searchTag, onFaults_searchTag);
			tagDelegate.searchTags(this.searchString,this.page);
			__model.loadingIconVisible = true;
		}
		
		//namespace fct = "http://openlinksw.com/services/facets/1.0/";
		private function onResults_searchTag(event:ResultEvent):void
		{
			__model.loadingIconVisible = false;

            var data:String = event.result.toString();
           	var jd:JSONDecoder = new JSONDecoder( data, false );
            var resultRows = jd.getValue();
//			var xmlResult:XML = new XML(event.result);
//			use namespace fct;
//			var resultRows:XMLList = xmlResult..row;
			var menuData:ArrayCollection = new ArrayCollection();
			var menuObject:Object;
			for(var i:* in resultRows)
			{
                var resultRow = resultRows[i]; 
                var gloss:String = resultRow.gloss;
                trace(gloss);
				menuObject = new Object();
				menuObject.label = String(resultRow.label+ ": " + gloss.substring(1, Math.min(30, gloss.length)));
				menuObject.data = String(resultRow.uri);
				menuData.addItem(menuObject);
			}
			//menuData.addItem({label:"------", enabled:false});
			//menuData.addItem({label:"Previous page", data:-1, enabled:(Number(this.page) > 0)});
			//menuData.addItem({label:"Next page", data:1});
			var menu:Menu = Menu.createMenu(null,menuData);
			menu.addEventListener(MenuEvent.ITEM_CLICK,onClick_contextMenuItem);
			var pt:Point = new Point(this.component.x, this.component.y + this.component.height);
			var componentParent:UIComponent = this.component.parent as UIComponent;
			pt = componentParent.contentToGlobal(pt);
			menu.show(pt.x,pt.y);
			pt.y -= menu.height;
			menu.hide();
			menu.show(pt.x,pt.y);
		}
		
		private function onFaults_searchTag(event:FaultEvent):void
		{
			__model.loadingIconVisible = false;
			Alert.show("Problem while searching for tags: " + event.fault.faultCode,"Error");
		}
		
		private function onClick_contextMenuItem(event:MenuEvent):void
		{
			if(event.item.data == 1 || event.item.data == -1)
			{
				if(event.item.data == -1 && Number(this.page) <= 0)
				{
					this.page = "0";
				}
				else
				{
					this.page = String(Number(this.page) + event.item.data);
				}
				doSearch();
				return;
			}
			if(__model.addedTags == null)
			{
				__model.addedTags = new ArrayCollection();
			}
			__model.addedTags.addItem(event.item);
		}
		
	}
}