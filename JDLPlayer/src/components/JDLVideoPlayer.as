package components
{
	import business.events.AddTaggingEvent;
	
	import flash.events.MouseEvent;
	
	import spark.components.Button;
	import spark.components.Group;
	import spark.components.VideoPlayer;
	
	public class JDLVideoPlayer extends VideoPlayer
	{
		public function JDLVideoPlayer()
		{
			super();
		}
		
		[SkinPart(required="false")]
		public var controlGroup:Group;
		
		[SkinPart(required="false")]
		public var addTagButton:Button;
		
		override protected function partAdded(partName:String, instance:Object):void
		{
			super.partAdded(partName, instance);
			if(instance == controlGroup)
			{
				controlGroup.addEventListener(MouseEvent.CLICK,controlGroup_clickHandler);
			}
			else if (instance == addTagButton)
			{
				addTagButton.addEventListener(MouseEvent.CLICK,addTagButton_clickHandler);
			}
		}

		override protected function partRemoved(partName:String, instance:Object):void
		{
			super.partRemoved(partName,instance);
			if(instance == controlGroup)
			{
				controlGroup.removeEventListener(MouseEvent.CLICK,controlGroup_clickHandler);
			}
			else if (instance == addTagButton)
			{
				addTagButton.removeEventListener(MouseEvent.CLICK,addTagButton_clickHandler);
			}
		}

		private function controlGroup_clickHandler(event:MouseEvent):void
		{
			if (playing)
				pause();
			else
				play();
			
			// need to synch up to what we've actually got because sometimes 
			// the play() didn't actually play() because there's no source 
			// or we're in an error state
			playPauseButton.selected = playing;
		}
		
		private function addTagButton_clickHandler(event:MouseEvent):void
		{
			var addTagEvent:AddTaggingEvent = new AddTaggingEvent();
			addTagEvent.dispatch();
		}

	}

}