package components
{
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import mx.core.IDataRenderer;
	import mx.core.UIComponent;
	import mx.formatters.NumberFormatter;
	
	import spark.components.Button;
	import spark.components.HSlider;
	
	
	public class TwoThumbSlider extends HSlider
	{
		public function TwoThumbSlider()
		{
			super();
		}
		
		[SkinPart(required="true")]
		public var thumb2:Button;
		
		//--------------------------------------------------------------------------
		//
		//  Overridden properties: Range
		//
		//--------------------------------------------------------------------------
		
		//---------------------------------
		// value1
		//---------------------------------     
		
		[Binable]
		public function get value1():Number
		{
			return value;
		}
		
		public function set value1(newValue:Number):void
		{
			value = newValue;
		}
		
		//---------------------------------
		// value2
		//---------------------------------     
		
		private var _value2:Number = 0;
		
		[Binable]
		public function get value2():Number
		{
			return _value2;
		}
		
		public function set value2(newValue:Number):void
		{
			newValue = nearestValidValue(newValue, snapInterval);
			newValue = Math.min(maximum, Math.max(value1+1, newValue));
			if (newValue == _value2)
				return;
			
			_value2 = newValue;
			invalidateDisplayList();
		}
		
		//---------------------------------
		// Methods
		//---------------------------------
		
		override protected function partAdded(partName:String, instance:Object):void
		{
			super.partAdded(partName, instance);
			if (instance == thumb2)
				thumb2.addEventListener(MouseEvent.MOUSE_DOWN, thumb_mouseDownHandler);
		}
		
		override protected function partRemoved(partName:String, instance:Object):void
		{
			super.partRemoved(partName, instance);
			if (instance == thumb2)
				thumb2.removeEventListener(MouseEvent.MOUSE_DOWN, thumb_mouseDownHandler);
		}
		
		override protected function updateSkinDisplayList():void
		{
			super.updateSkinDisplayList();
			var thumbRange:Number = track.getLayoutBoundsWidth() - thumb2.getLayoutBoundsWidth();
			var range:Number = maximum - minimum;
			var thumbPos:Number = (range > 0) ? ((value2 - minimum) / range) * thumbRange : 0;
			thumb2.setLayoutBoundsPosition(Math.round(track.getLayoutBoundsX() + thumbPos), thumb2.getLayoutBoundsY());
		}
		
		override protected function setValue(newValue:Number):void
		{
			super.setValue(Math.max(minimum, Math.min(newValue, value2 - 1)));
		}
		
		override protected function set pendingValue(newValue:Number):void
		{
			super.pendingValue = Math.max(minimum, Math.min(newValue, value2 - 1));
		}
		
		//---------------------------------
		// Thumb drag handlers
		//---------------------------------
		
		private var clickOffset:Point;
		private var draggingThumb2:Boolean = false;
		private var dataTipInitialPosition:Point = null;
		private var dataTipInstance:IDataRenderer;
		private var dataTipFormatter:NumberFormatter = new NumberFormatter();
		
		/**
		 *  @private
		 *  Handle mouse-down events on the scroll thumb. Records 
		 *  the mouse down point in clickOffset.
		 */
		override protected function thumb_mouseDownHandler(event:MouseEvent):void
		{
			if (event.currentTarget == thumb2)
			{
				draggingThumb2 = true;
				clickOffset = thumb2.globalToLocal(new Point(event.stageX, event.stageY));
			}
			super.thumb_mouseDownHandler(event);
		}
		
		override protected function system_mouseMoveHandler(event:MouseEvent):void
		{
			if (!draggingThumb2)
			{
				super.system_mouseMoveHandler(event);
				return;
			}
			
			var p:Point = track.globalToLocal(new Point(event.stageX, event.stageY));
			var newValue:Number = pointToValue(p.x - clickOffset.x, p.y - clickOffset.y);
			value2 = newValue;
			
			if (dataTipInstance && showDataTip)
			{ 
				dataTipInstance.data = dataTipFormatter.format(value2);
				
				// Force the dataTip to render so that we have the correct size since
				// updateDataTip might need the size
				var tipAsUIComponent:UIComponent = dataTipInstance as UIComponent; 
				if (tipAsUIComponent)
				{
					tipAsUIComponent.validateNow();
					tipAsUIComponent.setActualSize(tipAsUIComponent.getExplicitOrMeasuredWidth(),tipAsUIComponent.getExplicitOrMeasuredHeight());
				}
				
				updateDataTip(dataTipInstance, dataTipInitialPosition);
			}
			
			event.updateAfterEvent();
		}
		
		override protected function system_mouseUpHandler(event:Event):void
		{
			super.system_mouseUpHandler(event);
			draggingThumb2 = false;
		}
		
		
		override protected function updateDataTip(tipInstance:IDataRenderer, initialPosition:Point):void
		{
			if (!draggingThumb2)
			{
				super.updateDataTip(tipInstance, initialPosition);
				return;
			}
			
			var tipAsDisplayObject:DisplayObject = tipInstance as DisplayObject;
			if (tipAsDisplayObject && thumb2)
			{
				var relX:Number = thumb2.getLayoutBoundsX() - 
					(tipAsDisplayObject.width - thumb2.getLayoutBoundsWidth()) / 2;
				var o:Point = new Point(relX, initialPosition.y);
				var r:Point = localToGlobal(o);     
				
				// Get the screen bounds
				var screenBounds:Rectangle = systemManager.getVisibleApplicationRect();
				// Get the tips bounds. We only care about the dimensions.
				var tipBounds:Rectangle = tipAsDisplayObject.getBounds(tipAsDisplayObject.parent);
				
				// Make sure the tip doesn't exceed the bounds of the screen
				r.x = Math.floor( Math.max(screenBounds.left, 
					Math.min(screenBounds.right - tipBounds.width, r.x)));
				r.y = Math.floor( Math.max(screenBounds.top, 
					Math.min(screenBounds.bottom - tipBounds.height, r.y)));
				
				r = tipAsDisplayObject.parent.globalToLocal(r);
				
				tipAsDisplayObject.x = r.x;
				tipAsDisplayObject.y = r.y;
				
				// Called at mouse down time with values for tipInstance and initialPosition
				// computed by Slider/thumb_mouseDownHandler().  Subsequent calls from the
				// draggingThumb2 clause in system_mouseMoveHandler() just pass these values along.
				dataTipInstance = tipInstance;
				dataTipInitialPosition = initialPosition;
			}
		}
		
		//---------------------------------
		// Click on the track
		//---------------------------------
		
		override protected function track_mouseDownHandler(event:MouseEvent):void
		{
			var p:Point = track.globalToLocal(new Point(event.stageX, event.stageY));
			if (getNearestThumb(p) == thumb)
			{
				super.track_mouseDownHandler(event);
				return;
			}
			
			// Offset the track-relative coordinates of this event so that
			// the thumb will end up centered over the mouse down location.
			p.x -= thumb2.getLayoutBoundsWidth() / 2;
			p.y -= thumb2.getLayoutBoundsHeight() / 2;
			value2 = pointToValue(p.x, p.y);                
			
			event.updateAfterEvent();
		}
		
		private function getNearestThumb(pt:Point):Button 
		{
			var xThumb1:Number = Math.abs(pt.x - thumb.x);
			var xThumb2:Number = Math.abs(pt.x - thumb2.x);
			return xThumb1 < xThumb2 ? thumb : thumb2;
		}
		
	}
}