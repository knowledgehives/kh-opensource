package valueObjects
{
	import mx.collections.ArrayCollection;

	[Bindable]
	public class VideoTag
	{
		
		public var timeStart:int;
		public var timeEnd:int;
		public var title:String;
		public var description:String;
		public var documentURI:String;
		public var taggerURI:String;
		public var taggingURI:String;
		public var clippingURI:String;
		public var links:ArrayCollection;
		public var crossReferences:ArrayCollection;
		public var terms:ArrayCollection;
		
		public function VideoTag()
		{
		}
		
		public function toPostObject():Object
		{
			var retObj:Object = new Object();
			retObj.responseFormat = "xml";
			retObj.documentURI = this.documentURI;
			retObj.taggerURI = this.taggerURI;
			retObj.title = this.title;
			retObj.description = this.description;
			retObj.links = this.links.toArray().join("||");
			retObj.crossReferences = this.crossReferences.toArray().join("||");
			
			var termObject:Object;
			var postTerms:Array = new Array();
			for(var i:int = 0; i < this.terms.length; i++)
			{
				termObject = this.terms.getItemAt(i);
				postTerms.push(termObject.data + "|http://s3b.corrib.org/tagging/hasActionTerm|" + termObject.label);
			}
			retObj.terms = postTerms.join("||");
			retObj.clipping = "||http://s3b.corrib.org/tagging/Excerpt||" + this.timeStart + "||" + (this.timeEnd - this.timeStart);
			
			return retObj;
		}
		
		public function toXMLObject():XML
		{
			var newTagging:XML = <tagging/>;
			newTagging.appendChild(<title>{this.title}</title>);
			newTagging.appendChild(<documentURI>{this.documentURI}</documentURI>);
			newTagging.appendChild(<description>{this.description}</description>);
			newTagging.appendChild(<taggerURI>{this.taggerURI}</taggerURI>);
			
			var links:XML = <links/>;
			var i:* = null;
			for(i in this.links)
			{
				links.appendChild(<link>{this.links.getItemAt(i)}</link>);
			}
			newTagging.appendChild(links);
			
			var crossReferences:XML = <crossReferences/>;
			i = null;
			for(i in this.crossReferences)
			{
				crossReferences.appendChild(<crossReference>{Math.floor(Number(this.crossReferences.getItemAt(i))*1000)}</crossReference>);
			}
			newTagging.appendChild(crossReferences);
			
			var terms:XML = <terms/>;
			i = null;
			var newTerm:XML;
			for(i in this.terms)
			{
				newTerm = <term></term>;
				newTerm.@uri = this.terms.getItemAt(i).data;
				newTerm.appendChild(<tag>{this.terms.getItemAt(i).label}</tag>);
				terms.appendChild(newTerm);
			}
			newTagging.appendChild(terms);
			
			var clipping:XML = <clipping type="http://s3b.corrib.org/tagging/Excerpt"/>;
			clipping.appendChild(<startPoint>{this.timeStart}</startPoint>);
			clipping.appendChild(<length>{this.timeEnd - this.timeStart}</length>);
			clipping.appendChild(<taggingURI></taggingURI>);
			newTagging.appendChild(clipping);
			
			return newTagging;
		}
	}
}