package model
{
	import components.AddTaggingPopup;
	import components.TooltipPopup;
	
	import mx.collections.ArrayCollection;
	
	import valueObjects.VideoTag;
	
	[Bindable]
	public class ModelLocator
	{
		static private var __instance:ModelLocator = null;
		public var baseServerAddress:String;
		public var taggerURI:String;
		public var documentURI:String;
		public var taggingServiceName:String = "Tagging";
		public var videoAddress:String;
		public var tagLayout:ArrayCollection;
		public var tooltipPopup:TooltipPopup = null;
		public var addTagPopup:AddTaggingPopup = null;
		public var searchTagServiceAddress:String = "http://www.openvocabulary.info/search/allsynsets/{0}?lang=en" // "http://lod.openlinksw.com/fct/service";
		public var addedTags:ArrayCollection = null;
		public var tagsPerSearch:Number = 10;
		public var loadingIconVisible:Boolean = false;
		
		// for proxy only!
		public var xmlTags:XML = null;
		
//		public var photoData:ArrayCollection = new ArrayCollection();
//		public var purchasedPhotos:ArrayCollection = new ArrayCollection();
		
		static public function getInstance():ModelLocator
		{
			if(__instance == null)
			{
				__instance = new ModelLocator();
			}
			return __instance;
		}
	}
}