/**
 * 
 */
package org.corrib.jonto.tagging;

import org.apache.log4j.helpers.ISO8601DateFormat;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class with global constants like date formats etc
 * and methods to load properties from property file
 * @author Jakub Demczuk
 *
 */
public class GlobalTools {
	
	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.GlobalTools");
	private static final String REPOSITORY_PATH_KEY = "storageDir";
//	private static final String TEST_REPOSITORY_PATH_KEY = "testStorageDir";
	public static DateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	
	private String repositoryPath;
//	private String testRepositoryPath;
	private Properties props;
	protected GlobalTools(){
		try {
			props = new Properties();
			InputStream bis = GlobalTools.class.getResourceAsStream("/tagging.properties");
			props.load(bis);
			repositoryPath = props.getProperty(REPOSITORY_PATH_KEY);
//			testRepositoryPath = props.getProperty(TEST_REPOSITORY_PATH_KEY);
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			throw new RuntimeException(e);
		}
		

	}
	
	private static class GlobalToolsHolder{
		private static final GlobalTools INSTANCE = new GlobalTools(); 
	}
	
	public static GlobalTools getInstance(){
		return GlobalToolsHolder.INSTANCE;
	}
	
	
	public String getRespositoryPath(){
		return repositoryPath;
	}
	
//	public String getTestRepositoryPath(){
//		return testRepositoryPath;
//	}
}
