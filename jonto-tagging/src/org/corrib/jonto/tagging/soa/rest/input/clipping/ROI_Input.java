/**
 * 
 */
package org.corrib.jonto.tagging.soa.rest.input.clipping;

import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.ROI;

/**
 * @author Jakub Demczuk
 *
 */
public class ROI_Input extends ClippingInput {

	/**
	 * Sets the ROI coordinates property based on the input properties given<br/>
	 * <b>The xCoordinate must be the third(index=2) element in the given parameters</b><br/>
	 * <b>The yCoordinate must be the forth(index=3) element in the given parameters</b>
	 * @param roi <tt>ROI</tt> roi for which to set coordinates
	 * @param properties <tt>String...</tt> properties to use
	 */
	public void setProperties(Clipping clipping, String... properties){
		ROI roi = (ROI)clipping;
		if(properties.length >= 4 && properties[2] != null && properties[3] != null){
			int xCoordinate = Integer.parseInt(properties[2]);
			roi.setXCoordinate(xCoordinate);
			int yCoordinate = Integer.parseInt(properties[3]);
			roi.setYCoordinate(yCoordinate);
			
			ROI_InputFactory.getROI_Input(roi).setProperties(roi,properties);
		}
	}
}
