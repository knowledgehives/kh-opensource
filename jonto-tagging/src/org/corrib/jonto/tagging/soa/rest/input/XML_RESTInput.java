/**
 * 
 */
package org.corrib.jonto.tagging.soa.rest.input;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.beans.CircleROI;
import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Excerpt;
import org.corrib.jonto.tagging.beans.ROI;
import org.corrib.jonto.tagging.beans.RectangleROI;
import org.corrib.jonto.tagging.beans.Tagging;
import org.corrib.jonto.tagging.beans.Term;
import org.corrib.jonto.tagging.beans.TermType;
import org.corrib.jonto.tagging.beans.factories.ClippingFactory;
import org.corrib.jonto.tagging.soa.rest.input.exceptions.MalformedStringException;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

/**
 * @author Jakub Demczuk
 *
 */
public class XML_RESTInput{

	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.soa.rest.input.XML_RESTInput");
	
	/**
	 * Creates clipping from the given xml input string
	 * @param clippingString <tt>String</tt> xml description of the clipping
	 * @return <tt>Clipping</tt> clipping created from xml
	 * @throws MalformedStringException
	 */
	public Clipping parseClipping(String clippingString) throws MalformedStringException{
		Clipping clipping = null;
		try {
			SAXBuilder saxBuilder = new SAXBuilder();
			StringReader sr = new StringReader(clippingString);
			Document document = saxBuilder.build(sr);
			Element root = document.getRootElement();
			clipping = parseClippingProperties(root);
		} catch(MalformedStringException e){
			throw new MalformedStringException(e.getMessage() + "\n" + clippingString,e);
		} catch (JDOMException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			throw new MalformedStringException(e);
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			throw new MalformedStringException(e);
		}
		return clipping;
	}

	/**
	 * Creates tagging from the given xml input string
	 * @param taggingString <tt>String</tt> xml description of the tagging
	 * @return <tt>Tagging</tt> tagging created from xml
	 * @throws MalformedStringException
	 */
	public Tagging parseTagging(String taggingString) throws MalformedStringException {
		try {
			SAXBuilder saxBuilder = new SAXBuilder();
			StringReader sr = new StringReader(taggingString);
			Document document = saxBuilder.build(sr);
			Element root = document.getRootElement();
			
			Tagging tagging = parseTaggingProperties(root);
			return tagging;
		} catch(MalformedStringException e){
			throw new MalformedStringException(e.getMessage() + "\n" + taggingString,e);
		} catch (JDOMException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			throw new MalformedStringException(e);
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			throw new MalformedStringException(e);
		}
		
	}

	
	/**
	 * Creates a term from the given xml input string
	 * @param termString <tt>String</tt> xml description of the term
	 * @return <tt>Term</tt> term created from xml
	 * @throws MalformedStringException
	 */
	public Term parseTerm(String termString) throws MalformedStringException {
		try {
			SAXBuilder saxBuilder = new SAXBuilder();
			StringReader sr = new StringReader(termString);
			Document document = saxBuilder.build(sr);
			Element root = document.getRootElement();
			
			Term term = parseTermProperties(root);
			return term;
		} catch(MalformedStringException e){
			throw new MalformedStringException(e.getMessage() + "\n" + termString,e);
		} catch (JDOMException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			throw new MalformedStringException(e);
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			throw new MalformedStringException(e);
		}
	}


	/**
	 * Reads clipping properties for the given node
	 * @param rootClippingElement <tt>Element</tt> root element containing clipping properties
	 * @throws MalformedStringException when an URI or type attribute is missing
	 */
	@SuppressWarnings("unchecked")
	private Clipping parseClippingProperties(Element rootClippingElement) throws MalformedStringException {
		if(rootClippingElement.getAttribute("type") == null )
			throw new MalformedStringException("Given clippingString does not containt type attribute : ");
		String clippingURI ="";
		Element e;
		if(rootClippingElement.getAttribute("uri") != null) {
			clippingURI = rootClippingElement.getAttribute("uri").getValue();
		}
		String clippingType = rootClippingElement.getAttribute("type").getValue();
		Clipping clipping = ClippingFactory.getClipping(clippingURI, clippingType);
		if(rootClippingElement.getChild("taggingURI") != null){
			e = rootClippingElement.getChild("taggingURI");
			clipping.setTaggingURI(e.getValue());
		}
		if(rootClippingElement.getChild("xCoordinate") != null){
			e = rootClippingElement.getChild("xCoordinate");
			ROI roi = (ROI)clipping;
			roi.setXCoordinate(Integer.parseInt(e.getValue()));
		}
		if(rootClippingElement.getChild("yCoordinate") != null){
			e = rootClippingElement.getChild("yCoordinate");
			ROI roi = (ROI)clipping;
			roi.setYCoordinate(Integer.parseInt(e.getValue()));
		}
		if(rootClippingElement.getChild("radius") != null){
			e = rootClippingElement.getChild("radius");
			CircleROI circle = (CircleROI)clipping;
			circle.setRadius(Integer.parseInt(e.getValue()));
		}
		if(rootClippingElement.getChild("width") != null){
			e = rootClippingElement.getChild("width");
			RectangleROI rec = (RectangleROI)clipping;
			rec.setWidth(Integer.parseInt(e.getValue()));
		}
		
		if(rootClippingElement.getChild("height") != null){
			e = rootClippingElement.getChild("height");
			RectangleROI rec = (RectangleROI)clipping;
			rec.setHeight(Integer.parseInt(e.getValue()));
		}
		if(rootClippingElement.getChild("startPoint") != null){
			e = rootClippingElement.getChild("startPoint");
			Excerpt excerpt = (Excerpt)clipping;
			excerpt.setStartPoint(Long.parseLong(e.getValue()));
		}
		if(rootClippingElement.getChild("length") != null){
			e = rootClippingElement.getChild("length");
			Excerpt excerpt = (Excerpt)clipping;
			excerpt.setLength(Long.parseLong(e.getValue()));
		}
		

		return clipping;
	}
	
	/**
	 * Reads tagging properties for the given node
	 * @param rootTaggingElement <tt>Element</tt> root element containing tagging properties
	 * @throws MalformedStringException when an URI attribute is missing
	 */
	@SuppressWarnings("unchecked")
	private Tagging parseTaggingProperties(Element rootTaggingElement) throws MalformedStringException {
		Tagging tagging;
			
		tagging = new Tagging();
		if(rootTaggingElement.getAttributeValue("uri") != null){
			tagging.setURI(rootTaggingElement.getAttributeValue("uri"));
		}
		Element e;

		if(rootTaggingElement.getChild("documentURI") != null){
			e = rootTaggingElement.getChild("documentURI");
			tagging.setDocumentURI(e.getValue());
		}

		if(rootTaggingElement.getChild("taggerURI") != null){
			e = rootTaggingElement.getChild("taggerURI");
			tagging.setTaggerURI(e.getValue());
		}

		if(rootTaggingElement.getChild("title") != null){
			e = rootTaggingElement.getChild("title");
			tagging.setTitle(e.getValue());
		}

		if(rootTaggingElement.getChild("description") != null){
			e = rootTaggingElement.getChild("description");
			tagging.setDescription(e.getValue());
		}

		if(rootTaggingElement.getChild("clipping") != null){
			e = rootTaggingElement.getChild("clipping");
			Clipping clipping = parseClippingProperties(e);
			tagging.setClipping(clipping);
		}
		
		if(rootTaggingElement.getChild("terms") != null){
			e = rootTaggingElement.getChild("terms");
			List<Term> terms = new ArrayList<Term>(); 
			for(Element termElement : (List<Element>)e.getChildren()){
				Term term = parseTermProperties(termElement);
				terms.add(term);
			}
			tagging.addTerms(terms);
		}
		
		if(rootTaggingElement.getChild("links") != null){
			e = rootTaggingElement.getChild("links");
			for(Element link : (List<Element>)e.getChildren()){
				tagging.addLink(link.getValue());
			}
		}
		
		if(rootTaggingElement.getChild("crossReferences") != null){
			e = rootTaggingElement.getChild("crossReferences");
			for(Element crossReference : (List<Element>)e.getChildren()){
				tagging.addCrossReference(crossReference.getValue());
			}
		}
		return tagging;
	}
	
	/**
	 * Reads term properties for the given node
	 * @param rootTermElement <tt>Element</tt> root element containing term properties
	 * @throws MalformedStringException when an URI or type attribute is missing
	 */
	private Term parseTermProperties(Element rootTermElement) throws MalformedStringException {
		if(rootTermElement.getAttribute("type") == null )
			throw new MalformedStringException("Given termString does not containt type attribute : ");
		String termType = rootTermElement.getAttribute("type").getValue();
		Term term = new Term();
		if(rootTermElement.getAttribute("uri") != null)
			term.setURI(rootTermElement.getAttribute("uri").getValue());
		term.setTermType(TermType.getTermType(termType));
		Element e;
		if(rootTermElement.getChild("taggingURI") != null){
			e = rootTermElement.getChild("taggingURI");
			term.setTaggingURI(e.getValue());
		}
		if(rootTermElement.getChild("tag") != null){
			e = rootTermElement.getChild("tag");
			term.setTag(e.getValue());
		}
		return term;
	}

	

}
