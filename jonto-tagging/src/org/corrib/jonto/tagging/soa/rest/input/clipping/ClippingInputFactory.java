/**
 * 
 */
package org.corrib.jonto.tagging.soa.rest.input.clipping;

import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Excerpt;
import org.corrib.jonto.tagging.beans.ROI;

/**
 * @author Jakub Demczuk
 *
 */
public class ClippingInputFactory {

	public static ClippingInput getClipping(Clipping clipping) {
		if(clipping instanceof ROI){
			return new ROI_Input();
		}else if (clipping instanceof Excerpt)
			return new ExcerptInput();
		else
			throw new IllegalArgumentException("ClippingInput not yet supported");
	}

}
