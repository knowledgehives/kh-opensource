/**
 * 
 */
package org.corrib.jonto.tagging.soa.rest.input;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.corrib.jonto.tagging.beans.CircleROI;
import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Excerpt;
import org.corrib.jonto.tagging.beans.ROI;
import org.corrib.jonto.tagging.beans.RectangleROI;
import org.corrib.jonto.tagging.beans.Tagging;
import org.corrib.jonto.tagging.beans.Term;
import org.corrib.jonto.tagging.beans.TermType;
import org.corrib.jonto.tagging.beans.factories.ClippingFactory;
import org.corrib.jonto.tagging.soa.rest.input.clipping.ClippingInput;
import org.corrib.jonto.tagging.soa.rest.input.exceptions.MalformedRequestException;

/**
 */
public class RequestParams_RESTInput {

	
	/**
	 * Creates a tagging object from the given parameter map
	 * @param parameterMap <tt>Map</tt> parameter map from request
	 * @return <tt>Tagging</tt> tagging created from the given map 
	 * @throws MalformedRequestException 
	 */
	@SuppressWarnings("unchecked")

	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.soa.rest.input.RequestParams_RESTInput");

	public Tagging parseTagging(Map parameterMap) throws MalformedRequestException {
		Tagging tagging = new Tagging();
		
		if(parameterMap.get("uri") != null) {
			String[] sURI = (String[])parameterMap.get("uri");
			tagging.setURI(sURI[0]);
		}
		
		if(parameterMap.get("documentURI") != null){
			String[] sDocURI = (String[]) parameterMap.get("documentURI");
			logger.log(Level.INFO,"documentURI post: " + sDocURI[0]);
			if(!sDocURI[0].equals(""))
			{
				tagging.setDocumentURI(sDocURI[0]);
			}
		}
		
		if(parameterMap.get("taggerURI") != null){
			String[] sTaggerURI = (String[]) parameterMap.get("taggerURI");
			if(!sTaggerURI[0].equals(""))
			{
				tagging.setTaggerURI(sTaggerURI[0]);
			}
		}
		
		if(parameterMap.get("title") != null){
			String[] sTitle = (String[]) parameterMap.get("title");
			if(!sTitle[0].equals(""))
			{
				tagging.setTitle(sTitle[0]);
			}
		}
		
		if(parameterMap.get("description") != null){
			String[] sDesc = (String[]) parameterMap.get("description");
			if(!sDesc[0].equals(""))
			{
				tagging.setDescription(sDesc[0]);
			}
		}
		
		if(parameterMap.get("shared") != null){
			String[] sShared = (String[]) parameterMap.get("shared");
			if(!sShared[0].equals(""))
			{
				tagging.setShared(Boolean.parseBoolean(sShared[0]));
			}
		}
		
		Pattern pattern = Pattern.compile("http://s3b.corrib.org/tagging/[a-zA-Z]*?");
		if(parameterMap.get("clipping") != null){
			String[] saClipping = (String[]) parameterMap.get("clipping");
			String sClipping = saClipping[0];
			if(!sClipping.equals(""))
			{
				String[] clippingParts = sClipping.split("\\|\\|");
				for(int i=0; i < clippingParts.length; i++)
				{
					logger.log(Level.INFO, "clippingParts["+ i +"]: '" + (clippingParts[i].trim().equals("null") ? "null" : clippingParts[i])+"'");
					
				}
				
				Matcher matcher = pattern.matcher(clippingParts[1]);
				if (!matcher.find())
					throw new MalformedRequestException("Given clipping type is not valid");
				
				String clippingType = clippingParts[1];
				String clippingURI = clippingParts[0].trim();
				if("".equals(clippingURI) || clippingURI == null){
					clippingURI = null;
					clippingParts[0] = clippingURI;
				}
				Clipping clipping = ClippingFactory.getClipping(clippingURI, clippingType);
				
				ClippingInput ci = new ClippingInput();
				ci.setProperties(clipping, clippingParts);
				tagging.setClipping(clipping);
			}
		}
		
		
		if(parameterMap.get("terms") != null){
			String[] terms = (String[]) parameterMap.get("terms");
			String termsParam = terms[0];
			if(!termsParam.equals(""))
			{
				String[] termDefs = termsParam.split("\\|\\|");
				for(String termDef : termDefs){
					String[] termParts = termDef.split("\\|");
					if (termParts.length < 2 )
						throw new MalformedRequestException("One of the given terms is not valid (number of args less than 2) : " + termDef);
					Matcher matcher = pattern.matcher(termParts[1]);
					if(!matcher.find())
						throw new MalformedRequestException("One of the given terms is not valid (term type URI does not match) : " + termDef);
					
					String termURI = termParts[0].trim();
					if("".equals(termURI) || termURI == null) 
						termURI = null;
					
					TermType termType = TermType.getTermType(termParts[1]);
					Term term = new Term();
					if(termURI != null)
						term.setURI(termURI);

//					if(true)
//						throw new MalformedRequestException("WTF! : " + term.getURI());
					term.setTermType(termType);
					term.setTag(termParts[termParts.length-1]);
					tagging.addTerm(term);
				}
			}
		}
			
		if(parameterMap.get("links") != null){
			String[] links = (String[]) parameterMap.get("links");
			String linksParam = links[0];
			if(!linksParam.equals(""))
			{
				String[] linksArray = linksParam.split("\\|\\|");
				tagging.addLinksFromStrings(linksArray);
			}
		}
		if(parameterMap.get("crossReferences") != null){
			String[] crossReferences = (String[]) parameterMap.get("crossReferences");
			String crParam = crossReferences[0];
			if(!crParam.equals(""))
			{
				String[] crArray = crParam.split("\\|\\|");
				tagging.addCrossReferencesFromStrings(crArray);
			}
		}
		return tagging;
	}

	
		


	/**
	 * Creates a clipping object from the given parameter map
	 * @param parameterMap <tt>Map</tt> parameter map from request
	 * @return <tt>Clipping</tt> clipping created from the given map 
	 * @throws MalformedRequestException 
	 */
	@SuppressWarnings("unchecked")
	public Clipping parseClipping(Map parameterMap) throws MalformedRequestException {
		String clippingType = null;
		if (parameterMap.get("type") == null){
			throw new MalformedRequestException("Given request does not contain required type attribute for clipping");
		}
		
		String uri = null;
		if(parameterMap.get("uri") != null){
			String[] sURI = (String[]) parameterMap.get("uri");
			uri = sURI[0];
		}
		String[] sType = (String[]) parameterMap.get("type");
		clippingType = sType[0];
		Clipping clipping = ClippingFactory.getClipping(uri, clippingType);
		
		
		
		String value;
		if(parameterMap.get("taggingURI") != null){
			String[] sTaggingURI = (String[]) parameterMap.get("taggingURI");
			value = sTaggingURI[0];
			clipping.setTaggingURI(value);
		}
		if(parameterMap.get("xCoordinate") != null){
			String[] sXCoord = (String[]) parameterMap.get("xCoordinate");
			value = sXCoord[0];
			ROI roi = (ROI)clipping;
			roi.setXCoordinate(Integer.parseInt(value));
		}
		if(parameterMap.get("yCoordinate") != null){
			String[] sYCoord = (String[]) parameterMap.get("yCoordinate");
			value = sYCoord[0];
			ROI roi = (ROI)clipping;
			roi.setYCoordinate(Integer.parseInt(value));
		}
		if(parameterMap.get("radius") != null){
			String[] sRadius = (String[]) parameterMap.get("radius");
			value = sRadius[0];
			CircleROI circle = (CircleROI)clipping;
			circle.setRadius(Integer.parseInt(value));
		}
		if(parameterMap.get("width") != null){
			String[] sWidth = (String[]) parameterMap.get("width");
			value = sWidth[0];
			RectangleROI rec = (RectangleROI)clipping;
			rec.setWidth(Integer.parseInt(value));
		}
		
		if(parameterMap.get("height") != null){
			String[] sHeight = (String[]) parameterMap.get("height");
			value = sHeight[0];
			RectangleROI rec = (RectangleROI)clipping;
			rec.setHeight(Integer.parseInt(value));
		}
		if(parameterMap.get("startPoint") != null){
			String[] sStartPoint = (String[]) parameterMap.get("startPoint");
			value = sStartPoint[0];
			Excerpt excerpt = (Excerpt)clipping;
			excerpt.setStartPoint(Long.parseLong(value));
		}
		if(parameterMap.get("length") != null){
			String[] sLength = (String[]) parameterMap.get("length");
			value = sLength[0];
			Excerpt excerpt = (Excerpt)clipping;
			excerpt.setLength(Long.parseLong(value));
		}
		return clipping;
	}

	/**
	 * Creates a term object from the given parameter map
	 * @param parameterMap <tt>Map</tt> parameter map from request
	 * @return <tt>Term</tt> term created from the given map 
	 * @throws MalformedRequestException 
	 */
	@SuppressWarnings("unchecked")
	public Term parseTerm(Map parameterMap) throws MalformedRequestException {
		if(parameterMap.get("type") == null )
			throw new MalformedRequestException("Given request does not contain required type attribute for term");
		String[] sType = (String[]) parameterMap.get("type");
		String termType = sType[0];
		Term term = new Term();
		String uri = null;
		if(parameterMap.get("uri") != null){
			String[] sURI = (String[]) parameterMap.get("uri");
			uri = sURI[0];
			term.setURI(uri);
		}
		term.setTermType(TermType.getTermType(termType));
		
		String value;
		if(parameterMap.get("taggingURI") != null){
			String[] sTaggingURI = (String[]) parameterMap.get("taggingURI");
			value = sTaggingURI[0];
			term.setTaggingURI(value);
		}
		if(parameterMap.get("tag") != null){
			String[] sTag = (String[]) parameterMap.get("tag");
			value = sTag[0];
			term.setTag(value);
		}
		return term;
	}

}
