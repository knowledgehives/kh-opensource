/**
 * 
 */
package org.corrib.jonto.tagging.soa.rest.input.clipping;

import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Excerpt;


/**
 * @author Jakub Demczuk
 *
 */
public class ExcerptInput extends ClippingInput {
	/**
	 * Sets the startPoin/length properties based on the input properties given<br/>
	 * <b>The startPoint must be the third(index=4) element in the given parameters</b><br/>
	 * <b>The length must be the forth(index=5) element in the given parameters</b>
	 * @param clipping <tt>Clipping</tt> clipping that's an Excerpt
	 * @param properties <tt>String...</tt> properties to use
	 */
	public void setProperties(Clipping clipping, String... properties){
		Excerpt excerpt = (Excerpt) clipping;
		if(properties.length >= 4 && properties[2] != null && properties[3] != null){
			long startPoint = Long.parseLong(properties[2]);
			excerpt.setStartPoint(startPoint);
			long length = Long.parseLong(properties[3]);
			excerpt.setLength(length);
		}
	}
}
