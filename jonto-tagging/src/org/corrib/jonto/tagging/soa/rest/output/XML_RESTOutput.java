/**
 * 
 */
package org.corrib.jonto.tagging.soa.rest.output;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Outputable;
import org.corrib.jonto.tagging.beans.Tagging;
import org.corrib.jonto.tagging.beans.Term;
import org.corrib.jonto.tagging.beans.factories.ClippingFactory;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.ontoware.rdf2go.model.node.URI;


/**
 * This class prepares response in an xml format
 * @author Jakub Demczuk
 *
 */
public class XML_RESTOutput implements RESTOutput {

	private static final String TERM = "term";
	private static final String TERMS = "terms";
	private static final String TAGGING = "tagging";
	private static final String TAGGINGS = "taggings";
	private static final String CLIPPING = "clipping";
	private static final String CLIPPINGS = "clippings";
	private static final String RESULTS = "results";
	
	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.soa.rest.output.XML_RESTOutput");
	
	public XML_RESTOutput() {
		//default empty constructor
	}
	
	
	public String getOutputText(Outputable object){
		Document result = doGetOutputText(null,object);
		return generateOutputText(result);
	}
	
	public String getOutputText(List<Outputable> outputables){
		Document document = new Document();
		Element root = new Element(RESULTS);
		document.addContent(root);
		for(Outputable object : outputables){
			document = doGetOutputText(document, object);
		}
		return generateOutputText(document);
	}
	
	public String getOutputTextForClippings(List<Clipping> clippings) {
		Document document = new Document();
		Element root = new Element(CLIPPINGS);
		document.addContent(root);
		for(Clipping clipping : clippings){
			document = doGetOutputText(document, clipping);
		}
		return generateOutputText(document);
	}


	public String getOutputTextForTaggings(List<Tagging> taggings) {
		Document document = new Document();
		Element root = new Element(TAGGINGS);
		document.addContent(root);
		for(Tagging tagging : taggings){
			document = doGetOutputText(document, tagging);
		}
		return generateOutputText(document);
	}


	public String getOutputTextForTerms(List<Term> terms) {
		Document document = new Document();
		Element root = new Element(TERMS);
		document.addContent(root);
		for(Term term : terms){
			document = doGetOutputText(document, term);
		}
		return generateOutputText(document);
	}

	public String getErrorOutputText(String errorMessage){
		StringBuilder sb = new StringBuilder();
		sb.append("<error>");
		sb.append(errorMessage);
		sb.append("</error>");
		return sb.toString();
	}
	
	/**
	 * Appends given or creates new Document with the given object as child
	 * @param document <tt>Document</tt> document to append, if null is given then it will be created
	 * @param object <tt>Outputable</tt> outputable for which to generate xml 
	 * @return <tt>Document</tt> document with outputable as a child
	 */
	protected Document doGetOutputText(Document document, Outputable object){
		if(object instanceof Tagging){
			return appendDOM(document, (Tagging) object); 
		}else if (object instanceof Clipping){
			return appendDOM(document, (Clipping) object);
		}else if (object instanceof Term){
			return appendDOM(document, (Term) object); 
		}else
			throw new IllegalArgumentException("Outputable class not yet supported : " + object.getClass());
	}
	
	/**
	 * Appends or creates new Document with the given tagging as child
	 * @param newTaggingDOM <tt>Document</tt> if <tt>not null</tt> then the given tagging is appended to the existing document <br/>
	 * otherwise new Document is generated with only the tagging given  
	 * @param tagging <tt>Tagging</tt> tagging for which to generate xml
	 * @return <tt>Document</tt> document with appended tagging as a child
	 */
	protected Document appendDOM(Document taggingDOM,Tagging tagging) {
		Document newTaggingDOM = taggingDOM;
		Map<String, Object> properties = tagging.getPropertiesMap();
		DOMData domData = new DOMData(TAGGING, tagging.getURI().toString(), properties);
		Element nodeToAppend;
		if(newTaggingDOM == null){
			newTaggingDOM = generateDOM(domData);
			nodeToAppend = newTaggingDOM.getRootElement();
		}else{
			nodeToAppend = appendDOM(newTaggingDOM, newTaggingDOM.getRootElement(), domData);
		}

		//append tagging DOM with list of links
		appendDOM(newTaggingDOM,nodeToAppend,"links","link",tagging.getLinks());
		//append tagging DOM with list of cross references
		appendDOMDual(newTaggingDOM,nodeToAppend,"crossReferences","crossReference",tagging.getCrossReferences(),tagging.getCrossReferenceStrings());

		//append tagging DOM with clipping if it exists
		if(tagging.getClipping() != null){
			properties = tagging.getClipping().getPropertiesMap();
			String clippingType = ClippingFactory.getClippingType(tagging.getClipping());
			domData = new DOMData(CLIPPING,tagging.getClipping().getURI().toString(),clippingType,properties);
			appendDOM(newTaggingDOM,nodeToAppend,domData);
		}

		//append tagging DOM with list of terms from this clipping
		List<DOMData> termsData = new ArrayList<DOMData>();
		for(Term term : tagging.getTerms()){
			properties = term.getPropertiesMap();
			domData = new DOMData(TERM, term.getURI().toString(),term.getTermType().getTermLabel(), properties);
			termsData.add(domData);
		}
		appendDOM(newTaggingDOM,nodeToAppend,"terms",termsData);
		return newTaggingDOM;
	}

	/**
	 * Appends given or creates new Document with the given clipping as child
	 * @param clippingDOM <tt>Document</tt> if <tt>not null</tt> then the given clipping is appended to the existing document <br/>
	 * @param clipping<tt>Clipping</tt> clipping for which to generate xml
	 * otherwise new Document is generated with only the clipping given
	 * @return <tt>Document</tt> document with appended clipping as a child 
	 */
	protected Document appendDOM(Document clippingDOM, Clipping clipping) {
		Document newClippingDOM = clippingDOM;
		Map<String,Object> properties = clipping.getPropertiesMap();
		String clippingType = ClippingFactory.getClippingType(clipping);
		DOMData domData = new DOMData(CLIPPING,clipping.getURI().toString(),clippingType,properties);
		if(newClippingDOM == null){
			newClippingDOM = generateDOM(domData);
		}else{
			appendDOM(newClippingDOM, newClippingDOM.getRootElement(), domData);
		}
		return newClippingDOM;
	}

	/**
	 * Appends given or creates new Document with the given term as child 
	 * @param termDOM <tt>Document</tt> if <tt>not null</tt> then the given term is appended to the existing document <br/>
	 * otherwise new Document is generated with only the term given
	 * @param term <tt>Term</tt> term for which to generate xml
	 * @return <tt>Document</tt> document with appended term as a child 
	 */
	protected Document appendDOM(Document termDOM, Term term) {
		Document newTermDOM = termDOM;
		Map<String,Object> properties = term.getPropertiesMap();
		DOMData domData = new DOMData(TERM, term.getURI().toString(),term.getTermType().getTermLabel(), properties);
		if(newTermDOM == null){
			newTermDOM = generateDOM(domData);
		}else{
			appendDOM(newTermDOM, newTermDOM.getRootElement(), domData);
		}
		return newTermDOM;
	}
	
	/**
	 * Generates xml string from the given document
	 * @param xmlDoc <tt>Document</tt> document from which to generate string
	 * @return <tt>String</tt> string representation of the given document
	 */
	protected String generateOutputText(Document xmlDoc){
		try {
			StringWriter sw = new StringWriter();
			XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
			xmlOutputter.output(xmlDoc, sw);
			return sw.toString();
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
		}
		return "";
	}
	
	/**
	 * Generates DOM Document object from the given properties <br/>
	 * 		{@literal<rootNodeName uri="rootNodeId" type="type">} <br/>
	 * 			{@literal <properties.key> properties.value </properties.key> }<br/>
	 * 			{@literal <properties.key> properties.value </properties.key> }<br/>
	 * 			.... <br/>
	 * 		{@literal</rootNodeName>} <br/>
	 * @param rootNodeName <tt>String</tt> name of the root node specifying type of the returned xml (tagging, clipping, term) 
	 * @param rootNodeId <tt>String</tt> [optional] uri of the root node 
	 * @param type <tt>String</tt> [optional] type property specifying termType or clipping type(circleROI,rectangleROI,excerpt etc) 
	 * @param properties <tt>Map{@literal<String,Object>}</tt> map [field -> value] for the object to generate  
	 * @return <tt>Document</tt> DOM document for the given object properties
	 */
	private Document generateDOM(String rootNodeName, String rootNodeId, String type, Map<String,Object> properties) {
		Document doc = new Document();
		Element root = new Element(rootNodeName);
		if(rootNodeId != null && !"".equals(rootNodeId)){
			root.setAttribute("uri", rootNodeId);
		}
		if(type != null && !"".equals(type)){
			root.setAttribute("type", type);
		}
		for(String property : properties.keySet()){
			Element child = new Element(property);
			if(properties.get(property) != null){
				child.setText(properties.get(property).toString());
			}
			root.addContent(child);
		}
		doc.addContent(root);
		return doc;
	}
	
	/**
	 * Generates DOM document for the given domData
	 * 		{@literal<domData.rootNodeName uri="domData.rootNodeId" type="domData.type">} <br/>
	 * 			{@literal <domData.properties.key> domData.properties.value </domData.properties.key> }<br/>
	 * 			{@literal <domData.properties.key> domData.properties.value </domData.properties.key> }<br/>
	 * 			.... <br/>
	 * 		{@literal</domData.rootNodeName>} <br/>
	 * @param listRootElemName <tt>String</tt> name of the root element of the generated nodes list
	 * @param listElemName <tt>String</tt> name of each element on the generated list
	 */
	protected Document generateDOM(DOMData domData){
		return generateDOM(domData.getRootNodeName(), domData.getRootNodeId(), domData.getType(), domData.getProperties());
	}
	
	/**
	 * Appends given DOM object with nodes generated for the given list <br/>
	 * {@literal<nodeToAppend>} </br>
	 * 		{@literal<listRootElemName>} </br>
	 * 			{@literal<listElemName>} values.value {@literal</listElemName>} <br/>
	 * 			.... <br/>
	 * 		{@literal</listRootElemName>}
	 * {@literal</nodeToAppend>} </br>
	 * @param parentDocument <tt>Document</tt> document which is modified
	 * @param nodeToAppend <tt>Node</tt> node to which add new child nodes
	 * @param listRootElemName <tt>String</tt> name of the root element of the generated nodes list
	 * @param listElemName <tt>String</tt> name of each element on the generated list
	 * @param values <tt>List{@literal<URI>}</tt> list of URIs to use as values for newly generated nodes
	 * @return <tt>Element</tt> newly created element (root of the list)
	 */
	protected Element appendDOM(Document parentDocument, Element nodeToAppend, String listRootElemName, String listElemName, List<URI> values) {
		Element root = new Element(listRootElemName);
		for(URI value : values){
			Element child = new Element(listElemName);
			child.setText(value.toString());
			root.addContent(child);
		}
		nodeToAppend.addContent(root);
		return root;
	}
	
	protected Element appendDOMDual(Document parentDocument, Element nodeToAppend, String listRootElemName, String listElemName, List<URI> values,List<String> values2) {
		Element root = new Element(listRootElemName);
		int i = 0;
		for(URI value : values){
			Element child = new Element(listElemName);
			child.setText(value.toString());
			if(i < values2.size())
			{
//				child.setAttribute("value", values2.toString());
				child.setAttribute("value", values2.get(i));
			}
			root.addContent(child);
			i++;
		}
		nodeToAppend.addContent(root);
		return root;
	}
	
	/**
	 * Appends given node with a child element from the given domData <br/>
	 * {@literal<nodeToAppend>} <br/>
	 * 		{@literal<domData.rootNodeName uri="domData.rootNodeId" type="domData.type">} <br/>
	 * 			{@literal <domData.properties.key> domData.properties.value </domData.properties.key> }<br/>
	 * 			{@literal <domData.properties.key> domData.properties.value </domData.properties.key> }<br/>
	 * 			.... <br/>
	 * 		{@literal</domData.rootNodeName>} <br/>
	 * {@literal</nodeToAppend>} <br/>
	 * @param parentDocument <tt>Document</tt> document which is modified
	 * @param nodeToAppend <tt>Node</tt> node to which add new child nodes
	 * @return <tt>Element</tt> newly created element
	 */
	protected Element appendDOM(Document parentDocument, Element nodeToAppend, DOMData domData) {
		Element newNode = new Element(domData.getRootNodeName());
		if(domData.getRootNodeId() != null && !"".equals(domData.getRootNodeId())){
			newNode.setAttribute("uri", domData.getRootNodeId());
		}
		if(domData.getType() != null && !"".equals(domData.getType())){
			newNode.setAttribute("type", domData.getType());
		}
		for(String property : domData.getProperties().keySet()){
			Element child = new Element(property);
			if(domData.getProperties().get(property) != null){
				child.setText(domData.getProperties().get(property).toString());
			}
			newNode.addContent(child);
		}
		nodeToAppend.addContent(newNode);
		return newNode;
	}
	
	/**
	 * Appends given node with a child element that contains elements from the given list of domDataList<br/>
	 * {@literal<nodeToAppend>} <br/>
	 * 		{@literal<listRootElemName>} </br>
	 * 			{@literal<domData.rootNodeName uri="domData.rootNodeId" type="domData.type">} <br/>
	 * 				{@literal <domData.properties.key> domData.properties.value </domData.properties.key> }<br/>
	 * 				{@literal <domData.properties.key> domData.properties.value </domData.properties.key> }<br/>
	 * 				.... <br/>
	 * 			{@literal</domData.rootNodeName>} <br/>
	 * 
	 * 			{@literal<domData.rootNodeName uri="domData.rootNodeId" type="domData.type">} <br/>
	 * 				{@literal <domData.properties.key> domData.properties.value </domData.properties.key> }<br/>
	 * 				{@literal <domData.properties.key> domData.properties.value </domData.properties.key> }<br/>
	 * 				.... <br/>
	 * 			{@literal</domData.rootNodeName>} <br/>
	 *			...
	 * 		{@literal</listRootElemName>} <br/>
	 * {@literal</nodeToAppend>} <br/>
	 * @param parentDocument <tt>Document</tt> document which is modified
	 * @param nodeToAppend <tt>Node</tt> node to which add new child nodes
	 * @param listRootElemName <tt>String</tt> name of the root element of the generated nodes list
	 * @param listElemName <tt>String</tt> name of each element on the generated list
	 * @param domDataList<tt>List{@literal<DOMData>}</tt> list of domData object that should be added
	 */
	protected void appendDOM(Document parentDocument,Element nodeToAppend, String listRootElemName, List<DOMData> domDataList) {
		Element termNodes = new Element(listRootElemName);
		for(DOMData domData : domDataList){
			appendDOM(parentDocument, termNodes, domData);
		}
		nodeToAppend.addContent(termNodes);
	}

	/**
	 * Helper class for storing properties used when generating DOM documents
	 * @author Jakub Demczuk
	 */
	protected class DOMData{
		private String rootNodeName;
		private String rootNodeId;
		private String type;
		private Map<String,Object> properties;
		
		/**
		 * Use when generating lists of objects like Tagging, Clipping or Term
		 * @param _rootNodeName <tt>String</tt> name of the root node specifying type of the returned xml (tagging, clipping, term)
		 */
		protected DOMData(String _rootNodeName) {
			this.rootNodeName = _rootNodeName;
		}

		/**
		 * @param _rootNodeName <tt>String</tt> name of the root node specifying type of the returned xml (tagging, clipping, term) 
		 * @param _rootNodeId <tt>String</tt> uri of the root node
		 * @param _type <tt>String</tt> [optional] type property specifying termType or clipping type(circleROI,rectangleROI,excerpt etc) 
		 * @param _properties <tt>Map{@literal<String,Object>}</tt> map [field -> value] for the object to generate
		 */
		protected DOMData(String _rootNodeName, String _rootNodeId, String _type, Map<String, Object> _properties) {
			this.rootNodeName = _rootNodeName;
			this.rootNodeId = _rootNodeId;
			this.type = _type;
			this.properties = _properties;
		}

		/**
		 * @param _rootNodeName <tt>String</tt> name of the root node specifying type of the returned xml (tagging, clipping, term) 
		 * @param _rootNodeId <tt>String</tt> uri of the root node
		 * @param _properties <tt>Map{@literal<String,Object>}</tt> map [field -> value] for the object to generate
		 */
		protected DOMData(String _rootNodeName, String _rootNodeId, Map<String, Object> _properties) {
			this.rootNodeName = _rootNodeName;
			this.rootNodeId = _rootNodeId;
			this.properties = _properties;
			type = null;
		}

		public String getRootNodeName() {
			return rootNodeName;
		}

		public void setRootNodeName(String _rootNodeName) {
			this.rootNodeName = _rootNodeName;
		}

		public String getRootNodeId() {
			return rootNodeId;
		}

		public void setRootNodeId(String _rootNodeId) {
			this.rootNodeId = _rootNodeId;
		}

		public String getType() {
			return type;
		}

		public void setType(String _type) {
			this.type = _type;
		}

		public Map<String, Object> getProperties() {
			return properties;
		}

		public void setProperties(Map<String, Object> _properties) {
			this.properties = _properties;
		}
	}

}
