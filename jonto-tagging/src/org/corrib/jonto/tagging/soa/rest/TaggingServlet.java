package org.corrib.jonto.tagging.soa.rest;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hp.hpl.jena.query.Syntax;
import org.corrib.jonto.tagging.beans.Tagging;
import org.corrib.jonto.tagging.db.exceptions.IllegalTypeException;
import org.corrib.jonto.tagging.db.exceptions.ResourceNotFoundException;
import org.corrib.jonto.tagging.db.logic.TaggingDbLogic;
import org.corrib.jonto.tagging.soa.rest.input.RequestParams_RESTInput;
import org.corrib.jonto.tagging.soa.rest.input.XML_RESTInput;
import org.corrib.jonto.tagging.soa.rest.input.exceptions.MalformedRequestException;
import org.corrib.jonto.tagging.soa.rest.input.exceptions.MalformedStringException;
import org.corrib.jonto.tagging.soa.rest.output.RESTOutputFactory;
import org.corrib.jonto.tagging.soa.tools.RequestParameter;
import org.corrib.jonto.tagging.soa.tools.RequestResponseFormat;
import org.corrib.jonto.tagging.soa.tools.ResponseSender;
import org.corrib.jonto.tagging.soa.tools.ServletTools;
import org.ontoware.rdf2go.model.Model;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;

/**
 * Servlet implementation class for Servlet: TaggingServlet
 * 
 */
public class TaggingServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
	static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.soa.rest.TaggingServlet");

	/*
	 * (non-Java-doc)
	 * 
	 * @see javax.servlet.http.HttpServlet#HttpServlet()
	 */
	public TaggingServlet() {
		super();
	}

	/**
	 * GET calls are used for finding Taggings with the given parameters
	 * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletTools st = new ServletTools();
		ResponseSender rs = new ResponseSender(response);
		
		RequestResponseFormat responseFormat = st.getRequestResponseFormat(request,RequestParameter.RESPONSE_FORMAT, rs);
		//if null then errorResponse have already been send and can return
		if(responseFormat == null) 
			return;

		TaggingDbLogic taggingDbLogic = new TaggingDbLogic();

		// check given parameters and if any was given
		// perform search based on them
		String sTaggerURI = request.getParameter(RequestParameter.TAGGER_URI.toString());
		String sDocumentURI = request.getParameter(RequestParameter.DOCUMENT_URI.toString());
		// search for taggings with documentURI and taggerURI
		if (sDocumentURI != null && sTaggerURI != null) {
			try {
				URI taggerURI = new URIImpl(sTaggerURI);
				URI documentURI = new URIImpl(sDocumentURI);
				List<Tagging> taggings = taggingDbLogic.getTaggingsForDocumentTagger(documentURI, taggerURI);
				String responseText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputTextForTaggings(taggings);
				rs.sendResponse(responseText);
				return;
				// IllegalArgumentException is thrown by URIImpl constructor when URI validation fails
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Malformed request. Either TaggerURI or documentURI is not a valid URI");
				return;
			}
			// search for taggings with documentURI only
		} else if (sDocumentURI != null) {
			try {
				URI documentURI = new URIImpl(sDocumentURI);
                if (responseFormat == RequestResponseFormat.RDF) {
                    response.setContentType("text/turtle;charset=UTF-8");
                    response.setHeader("Cache-Control", "no-cache");
                    response.setStatus(HttpServletResponse.SC_OK);

                    Model model = taggingDbLogic.getTaggingsForResource(documentURI);
                          model.open();
                          model.writeTo(response.getWriter(), org.ontoware.rdf2go.model.Syntax.Turtle);
                          model.close();

                } else {
                    List<Tagging> taggings = taggingDbLogic.getTaggingsForDocument(documentURI);
                    String responseText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputTextForTaggings(taggings);
                    rs.sendResponse(responseText);
                }

				return;
				// IllegalArgumentException is thrown by URIImpl constructor when URI validation fails
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Malformed request. DocumentURI is not a valid URI: " + sDocumentURI);
				return;
			}
			// search for taggings with taggerURI only
		} else if (sTaggerURI != null) {
			try {
				URI taggerURI = new URIImpl(sTaggerURI);
				List<Tagging> taggings = taggingDbLogic.getTaggingsForTagger(taggerURI);
				String responseText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputTextForTaggings(taggings);
				rs.sendResponse(responseText);
				return;
				// IllegalArgumentException is thrown by URIImpl constructor when URI validation fails
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Malformed request. TaggerURI is not a valid URI");
				return;
			}
		}
		// no parameters were given, then it is a call
		// to get just a signle tagging with a given uri
		String delete = request.getParameter(RequestParameter.DELETE.toString());
//		URI taggingURI = st.extractResourceURI(request);
		String sTaggingURI = request.getParameter(RequestParameter.TAGGING_URI.toString());
		if(sTaggingURI == null || sTaggingURI.equals(""))
		{
			rs.sendErrorResponse(responseFormat, "TaggingURI not given!");
			return;
		}
		URI taggingURI = new URIImpl(sTaggingURI);

		if (taggingURI != null) {
			// check uri
			try {
				Tagging tagging = taggingDbLogic.getTagging(taggingURI);
				if(true) {
//					rs.sendResponse(delete);
//					return;
				}
				if(delete != null && delete.equals("true")) {
					taggingDbLogic.deleteTagging(tagging);
					rs.sendResponse("<result>OK</result>");
					return;
				}
				String responseText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputText(tagging);
				rs.sendResponse(responseText);
				return;
				// IllegalArgumentException is thrown by URIImpl constructor when URI validation fails
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Malformed request. TaggingURI is not a valid URI "+taggingURI);
				return;
			} catch (ResourceNotFoundException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Given taggingURI was not found in the storage");
				return;
			} catch (IllegalTypeException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Given taggingURI is not a valid URI for tagging resources");
				return;
			}
		}
		rs.sendErrorResponse(responseFormat, "Malformed request. Neither TaggingURI nor DocumentURI nor TaggerURI was given. At least one of them is required");
		return;
	}

	/**
	 * Post method handles adding new Taggings to the storage
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletTools st = new ServletTools();
		ResponseSender rs = new ResponseSender(response);
		TaggingDbLogic taggingDbLogic = new TaggingDbLogic();
		RequestResponseFormat requestFormat = st.getRequestResponseFormat(request, RequestParameter.REQUEST_FORMAT, rs);
		//if null then errorResponse have already been send and can return
//		if(requestFormat == null) 
//			return;
		RequestResponseFormat responseFormat = st.getRequestResponseFormat(request, RequestParameter.RESPONSE_FORMAT, rs);
		//if null then errorResponse have already been send and can return
		if(responseFormat == null) 
			return;
		
		String taggingString = request.getParameter("tagging");

		try {
			//check if param required with xml call was given
			if(taggingString == null && RequestResponseFormat.XML.equals(requestFormat)){
				rs.sendErrorResponse(responseFormat, "No tagging was given as a param for this call");
			}else if(taggingString == null){
				//that's not an xml request, so parse params given
				Tagging tagging = new RequestParams_RESTInput().parseTagging(request.getParameterMap());
				handleTagging(taggingDbLogic, tagging,st.getServiceAddr(request));
				String outputText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputText(tagging);
				rs.sendResponse(outputText);
			}else{
				//xml request send, and is valid
				Tagging tagging = new XML_RESTInput().parseTagging(taggingString);
				handleTagging(taggingDbLogic, tagging,st.getServiceAddr(request));
				String outputText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputText(tagging);
				rs.sendResponse(outputText);
			}
		} catch (MalformedStringException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			rs.sendErrorResponse(responseFormat,"Malformed tagging send : " + e.getMessage());
			return;
		} catch (ResourceNotFoundException e) {
			logger.log(Level.WARNING, e.getMessage());
			rs.sendErrorResponse(responseFormat, "Given taggingURI was not found in the storage");
			return;
		} catch (IllegalTypeException e) {
			logger.log(Level.WARNING, e.getMessage());
			rs.sendErrorResponse(responseFormat, "Given taggingURI is not a valid URI for tagging resources");
			return;
		} catch (MalformedRequestException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			rs.sendErrorResponse(responseFormat,"Malformed tagging send : " + e.getMessage());
			return;
		}
			
	}
	
	/**
	 * Delete method handles removing  Taggings from the storage
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ServletTools st = new ServletTools();
		ResponseSender rs = new ResponseSender(response);

		TaggingDbLogic taggingDbLogic = new TaggingDbLogic();
		RequestResponseFormat responseFormat = st.getRequestResponseFormat(request,RequestParameter.RESPONSE_FORMAT, rs);
		//if null then errorResponse have already been send and can return
		if(responseFormat == null) 
			return;
		
		URI taggingURI = st.extractResourceURI(request);
		if (taggingURI != null) {
			// check uri
			try {
				Tagging tagging = taggingDbLogic.getTagging(taggingURI);
				taggingDbLogic.deleteTagging(tagging);
				rs.sendResponse("OK");
				return;
				// IllegalArgumentException is thrown by URIImpl constructor when URI validation fails
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Malformed request. TaggingURI is not a valid URI");
				return;
			} catch (ResourceNotFoundException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Given taggingURI was not found in the storage");
				return;
			} catch (IllegalTypeException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Given taggingURI is not a valid URI for tagging resources");
				return;
			}
		}
		rs.sendErrorResponse(responseFormat, "Malformed request. TaggingURI not given and is required with this call");
		return;
	}
	
	
	/**
	 * @param taggingDbLogic
	 * @param tagging
	 * @param serviceAddress
	 * @throws ResourceNotFoundException
	 * @throws IllegalTypeException
	 */
	private void handleTagging(TaggingDbLogic taggingDbLogic, Tagging tagging, String serviceAddress) throws ResourceNotFoundException,
			IllegalTypeException {
		//new tagging was given, then add it
		if(tagging.getURI() == null || "".equals(tagging.getURI().toString())){
			taggingDbLogic.addTagging(tagging, serviceAddress);
		//otherwise do update
		}else{
			taggingDbLogic.updateTagging(tagging, serviceAddress);
		}
	}
	
}