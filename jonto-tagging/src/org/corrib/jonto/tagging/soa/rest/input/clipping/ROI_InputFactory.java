package org.corrib.jonto.tagging.soa.rest.input.clipping;

import org.corrib.jonto.tagging.beans.CircleROI;
import org.corrib.jonto.tagging.beans.ROI;
import org.corrib.jonto.tagging.beans.RectangleROI;

/**
 * Factory class for ROI_Inputs
 * @author Jakub Demczuk
 *
 */
public class ROI_InputFactory {

	/**
	 * Gets ROI_Input specific for the GivenROI
	 * @param roi <tt>ROI</tt> roi for which to return ROI_Input
	 * @return <tt>ROI_Input</tt> for the given ROI class
	 * @throws IllegalArgumentException if the given ROI is not supported
	 */
	public static ROI_Input getROI_Input(ROI roi) {
		if(roi instanceof CircleROI){
			return new CircleROI_Input();
		}else if (roi instanceof RectangleROI)
			return new RectangleROI_Input();
		else 
			throw new IllegalArgumentException("ROIInput not yet supported");
	}
}
