package org.corrib.jonto.tagging.soa.rest;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.db.exceptions.IllegalTypeException;
import org.corrib.jonto.tagging.db.exceptions.ResourceNotFoundException;
import org.corrib.jonto.tagging.db.logic.ClippingDbLogic;
import org.corrib.jonto.tagging.soa.rest.input.RequestParams_RESTInput;
import org.corrib.jonto.tagging.soa.rest.input.XML_RESTInput;
import org.corrib.jonto.tagging.soa.rest.input.exceptions.MalformedRequestException;
import org.corrib.jonto.tagging.soa.rest.input.exceptions.MalformedStringException;
import org.corrib.jonto.tagging.soa.rest.output.RESTOutputFactory;
import org.corrib.jonto.tagging.soa.tools.RequestParameter;
import org.corrib.jonto.tagging.soa.tools.RequestResponseFormat;
import org.corrib.jonto.tagging.soa.tools.ResponseSender;
import org.corrib.jonto.tagging.soa.tools.ServletTools;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;

/**
 * Servlet implementation class for Servlet: ClippingServlet
 *
 */
 public class ClippingServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
   static final long serialVersionUID = 1L;
   private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.soa.rest.ClippingServlet");
    /* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#HttpServlet()
	 */
	public ClippingServlet() {
		super();
	}  
	
	/**
	 * GET calls are used for finding Clippings with the given parameters
	 * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ServletTools st = new ServletTools();
		ResponseSender rs = new ResponseSender(response);
		
		RequestResponseFormat responseFormat = st.getRequestResponseFormat(request,RequestParameter.RESPONSE_FORMAT, rs);
		//if null then errorResponse have already been send and can return
		if(responseFormat == null) 
			return;

		ClippingDbLogic clippingDbLogic = new ClippingDbLogic();

		// check given parameters and if any was given
		// perform search based on them
		String sTaggerURI = request.getParameter(RequestParameter.TAGGER_URI.toString());
		String sDocumentURI = request.getParameter(RequestParameter.DOCUMENT_URI.toString());
		// search for clippings with documentURI and taggerURI
		if (sDocumentURI != null && sTaggerURI != null) {
			try {
				URI taggerURI = new URIImpl(sTaggerURI);
				URI documentURI = new URIImpl(sDocumentURI);
				List<Clipping> clippings = clippingDbLogic.getClippingsForDocumentTagger(documentURI, taggerURI);
				String responseText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputTextForClippings(clippings);
				rs.sendResponse(responseText);
				return;
				// IllegalArgumentException is thrown by URIImpl constructor when URI validation fails
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Malformed request. Either TaggerURI or documentURI is not a valid URI");
				return;
			}
			// search for clippings with documentURI only
		} else if (sDocumentURI != null) {
			try {
				URI documentURI = new URIImpl(sDocumentURI);
				List<Clipping> clippings = clippingDbLogic.getClippingsForDocument(documentURI);
				String responseText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputTextForClippings(clippings);
				rs.sendResponse(responseText);
				return;
				// IllegalArgumentException is thrown by URIImpl constructor when URI validation fails
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Malformed request. DocumentURI is not a valid URI");
				return;
			}
			// search for clippings with taggerURI only
		} else if (sTaggerURI != null) {
			try {
				URI taggerURI = new URIImpl(sTaggerURI);
				List<Clipping> clippings = clippingDbLogic.getClippingsForTagger(taggerURI);
				String responseText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputTextForClippings(clippings);
				rs.sendResponse(responseText);
				return;
				// IllegalArgumentException is thrown by URIImpl constructor when URI validation fails
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Malformed request. TaggerURI is not a valid URI");
				return;
			}
		}
		// no parameters were given, then it is a call
		// to get just a signle clipping with a given uri
//		URI clippingURI = st.extractResourceURI(request);
		URI clippingURI = new URIImpl(request.getParameter("clippingUri"));

		if (clippingURI != null) {
			// check uri
			try {
				Clipping clipping = clippingDbLogic.getClipping(clippingURI);
				String responseText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputText(clipping);
				rs.sendResponse(responseText);
				return;
				// IllegalArgumentException is thrown by URIImpl constructor when URI validation fails
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Malformed request. ClippingURI is not a valid URI: " + clippingURI);
				return;
			} catch (ResourceNotFoundException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Given clippingURI was not found in the storage");
				return;
			} catch (IllegalTypeException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Given clippingURI is not a valid URI for clipping resources");
				return;
			}
		}
		rs.sendErrorResponse(responseFormat, "Malformed request. Neither ClippingURI nor DocumentURI nor TaggerURI was given. At least one of them is required");
		return;
	}

	/**
	 * Post method handles adding new Clippings to the storage
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ServletTools st = new ServletTools();
		ResponseSender rs = new ResponseSender(response);
		ClippingDbLogic clippingDbLogic = new ClippingDbLogic();
		
		RequestResponseFormat requestFormat = st.getRequestResponseFormat(request, RequestParameter.REQUEST_FORMAT, rs);
		RequestResponseFormat responseFormat = st.getRequestResponseFormat(request, RequestParameter.RESPONSE_FORMAT, rs);
		//if null then errorResponse have already been send and can return
		if(responseFormat == null) 
			return;
		
		String clippingString = request.getParameter("clipping");
		try {
			//check if param required with xml call was given
			if(clippingString == null && RequestResponseFormat.XML.equals(requestFormat)){
				rs.sendErrorResponse(responseFormat, "No clipping was given as a param for this call");
			}else if(clippingString == null){
				//that's not an xml request, so parse params given
				Clipping clipping = new RequestParams_RESTInput().parseClipping(request.getParameterMap());
				handleClipping(clippingDbLogic, clipping,st.getServiceAddr(request));
				String outputText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputText(clipping);
				rs.sendResponse(outputText);
			}else{
				//xml request send, and is valid
				Clipping clipping = new XML_RESTInput().parseClipping(clippingString);
				handleClipping(clippingDbLogic, clipping,st.getServiceAddr(request));
				String outputText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputText(clipping);
				rs.sendResponse(outputText);
			}
		} catch (MalformedStringException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			rs.sendErrorResponse(responseFormat,"Malformed clipping send : " + e.getMessage());
			return;
		} catch (ResourceNotFoundException e) {
			logger.log(Level.WARNING, e.getMessage());
			rs.sendErrorResponse(responseFormat, "Given clippingURI was not found in the storage");
			return;
		} catch (IllegalTypeException e) {
			logger.log(Level.WARNING, e.getMessage());
			rs.sendErrorResponse(responseFormat, "Given clippingURI is not a valid URI for clipping resources");
			return;
		} catch (MalformedRequestException e) {
			logger.log(Level.WARNING, e.getMessage());
			rs.sendErrorResponse(responseFormat,"Malformed clipping send : " + e.getMessage());
			return;
		}
			
	}
	
	/**
	 * Delete method handles removing  Clippings from the storage
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ServletTools st = new ServletTools();
		ResponseSender rs = new ResponseSender(response);
		ClippingDbLogic clippingDbLogic = new ClippingDbLogic();
		RequestResponseFormat responseFormat = st.getRequestResponseFormat(request,RequestParameter.RESPONSE_FORMAT, rs);
		//if null then errorResponse have already been send and can return
		if(responseFormat == null) 
			return;
		
//		URI clippingURI = st.extractResourceURI(request);
		URI clippingURI = new URIImpl(request.getParameter("clippingUri"));
		if (clippingURI != null) {
			// check uri
			try {
				Clipping clipping = clippingDbLogic.getClipping(clippingURI);
				clippingDbLogic.deleteClipping(clipping);
				rs.sendResponse("OK");
				return;
				// IllegalArgumentException is thrown by URIImpl constructor when URI validation fails
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Malformed request. ClippingURI is not a valid URI");
				return;
			} catch (ResourceNotFoundException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Given clippingURI was not found in the storage");
				return;
			} catch (IllegalTypeException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Given clippingURI is not a valid URI for clipping resources");
				return;
			}
		}
		rs.sendErrorResponse(responseFormat, "Malformed request. ClippingURI not given and is required with this call");
		return;
	}    
	
	/**
	 * @param clippingDbLogic
	 * @param clipping
	 * @param serviceAddress
	 * @throws ResourceNotFoundException
	 * @throws IllegalTypeException
	 */
	private void handleClipping(ClippingDbLogic clippingDbLogic, Clipping clipping, String serviceAddress) throws ResourceNotFoundException,
			IllegalTypeException {
		//new clipping was given, then add it
		if(clipping.getURI() == null || "".equals(clipping.getURI().toString())){
			clippingDbLogic.addClipping(clipping, serviceAddress);
		//otherwise do update
		}else{
			clippingDbLogic.updateClipping(clipping);
		}
	}
}