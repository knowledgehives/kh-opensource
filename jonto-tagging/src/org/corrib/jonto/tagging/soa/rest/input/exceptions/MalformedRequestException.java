/**
 * 
 */
package org.corrib.jonto.tagging.soa.rest.input.exceptions;

/**
 * @author Jakub Demczuk
 *
 */
public class MalformedRequestException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4454564893091808948L;

	/**
	 * 
	 */
	public MalformedRequestException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public MalformedRequestException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public MalformedRequestException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public MalformedRequestException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

}
