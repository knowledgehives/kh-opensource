/**
 * 
 */
package org.corrib.jonto.tagging.soa.rest.input.clipping;

import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.RectangleROI;



/**
 *
 */
public class RectangleROI_Input extends ROI_Input {
	/**
	 * Sets the width/height properties based on the input properties given<br/>
	 * <b>The width must be the third(index=4) element in the given parameters</b><br/>
	 * <b>The height must be the forth(index=5) element in the given parameters</b>
	 * @param clipping <tt>Clipping</tt> clipping that's a rectangle for which to set width/height
	 * @param properties <tt>String...</tt> properties to use
	 */
	public void setProperties(Clipping clipping, String... properties){
		RectangleROI rect = (RectangleROI) clipping;
		if(properties.length >= 6 && properties[4] != null && properties[5] != null){
			int width = Integer.parseInt(properties[4]);
			rect.setWidth(width);
			int height = Integer.parseInt(properties[5]);
			rect.setHeight(height);
		}
	}
}
