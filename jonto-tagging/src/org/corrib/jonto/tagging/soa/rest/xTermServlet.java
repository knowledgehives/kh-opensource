package org.corrib.jonto.tagging.soa.rest;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.corrib.jonto.tagging.beans.Term;
import org.corrib.jonto.tagging.db.exceptions.IllegalTypeException;
import org.corrib.jonto.tagging.db.exceptions.ResourceNotFoundException;
import org.corrib.jonto.tagging.db.logic.TermDbLogic;
import org.corrib.jonto.tagging.soa.rest.input.RequestParams_RESTInput;
import org.corrib.jonto.tagging.soa.rest.input.XML_RESTInput;
import org.corrib.jonto.tagging.soa.rest.input.exceptions.MalformedRequestException;
import org.corrib.jonto.tagging.soa.rest.input.exceptions.MalformedStringException;
import org.corrib.jonto.tagging.soa.rest.output.RESTOutputFactory;
import org.corrib.jonto.tagging.soa.tools.RequestParameter;
import org.corrib.jonto.tagging.soa.tools.RequestResponseFormat;
import org.corrib.jonto.tagging.soa.tools.ResponseSender;
import org.corrib.jonto.tagging.soa.tools.ServletTools;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;

/**
 * Servlet implementation class for Servlet: TermServlet
 *
 */
 public class xTermServlet extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
   static final long serialVersionUID = 1L;
   private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.soa.rest.TermServlet");
    /* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#HttpServlet()
	 */
	public xTermServlet() {
		super();
	}   	
	
	/**
	 * GET calls are used for finding Terms with the given parameters
	 * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.log(Level.INFO, "termServlet doGet");
		ServletTools st = new ServletTools();
		ResponseSender rs = new ResponseSender(response);
		
		RequestResponseFormat responseFormat = st.getRequestResponseFormat(request,RequestParameter.RESPONSE_FORMAT, rs);
		//if null then errorResponse have already been send and can return
		logger.log(Level.INFO, "termServlet test" + responseFormat.toString());
		if(responseFormat == null) 
			return;

		TermDbLogic termDbLogic = new TermDbLogic();

		// check given parameters and if any was given
		// perform search based on them
		String sTaggerURI = request.getParameter(RequestParameter.TAGGER_URI.toString());
		String sDocumentURI = request.getParameter(RequestParameter.DOCUMENT_URI.toString());
		// search for terms with documentURI and taggerURI
		logger.log(Level.INFO, "termServlet doGet-PARAMS ok");
		if (sDocumentURI != null && sTaggerURI != null) {
			try {
				URI taggerURI = new URIImpl(sTaggerURI);
				URI documentURI = new URIImpl(sDocumentURI);
				List<Term> terms = termDbLogic.getTermsForDocumentTagger(documentURI, taggerURI);
				String responseText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputTextForTerms(terms);
				rs.sendResponse(responseText);
				return;
				// IllegalArgumentException is thrown by URIImpl constructor when URI validation fails
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Malformed request. Either TaggerURI or documentURI is not a valid URI");
				return;
			}
			// search for terms with documentURI only
		} else if (sDocumentURI != null) {
			try {
				URI documentURI = new URIImpl(sDocumentURI);
				List<Term> terms = termDbLogic.getTermsForDocument(documentURI);
				String responseText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputTextForTerms(terms);
				rs.sendResponse(responseText);
				return;
				// IllegalArgumentException is thrown by URIImpl constructor when URI validation fails
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Malformed request. DocumentURI is not a valid URI");
				return;
			}
			// search for terms with taggerURI only
		} else if (sTaggerURI != null) {
			try {
				URI taggerURI = new URIImpl(sTaggerURI);
				List<Term> terms = termDbLogic.getTermsForTagger(taggerURI);
				String responseText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputTextForTerms(terms);
				rs.sendResponse(responseText);
				return;
				// IllegalArgumentException is thrown by URIImpl constructor when URI validation fails
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Malformed request. TaggerURI is not a valid URI");
				return;
			}
		}
		// no parameters were given, then it is a call
		// to get just a signle term with a given uri
		URI termURI = st.extractResourceURI(request);
		if (termURI != null) {
			// check uri
			try {
				Term term = termDbLogic.getTerm(termURI);
				String responseText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputText(term);
				rs.sendResponse(responseText);
				return;
				// IllegalArgumentException is thrown by URIImpl constructor when URI validation fails
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Malformed request. TermURI is not a valid URI");
				return;
			} catch (ResourceNotFoundException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Given termURI was not found in the storage");
				return;
			} catch (IllegalTypeException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Given termURI is not a valid URI for term resources");
				return;
			}
		}
		rs.sendErrorResponse(responseFormat, "Malformed request. Neither TermURI nor DocumentURI nor TaggerURI was given. At least one of them is required");
		return;
	}

	/**
	 * Post method handles adding new Terms to the storage
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		logger.log(Level.INFO, "termServlet doPost_");
		ServletTools st = new ServletTools();
		ResponseSender rs = new ResponseSender(response);
		TermDbLogic termDbLogic = new TermDbLogic();
		
		RequestResponseFormat requestFormat = st.getRequestResponseFormat(request, RequestParameter.REQUEST_FORMAT, rs);
		RequestResponseFormat responseFormat = st.getRequestResponseFormat(request, RequestParameter.RESPONSE_FORMAT, rs);
		//if null then errorResponse have already been send and can return
		if(responseFormat == null) 
			return;
		
		String termString = request.getParameter("term");
		try {
			//check if param required with xml call was given
			if(termString == null && RequestResponseFormat.XML.equals(requestFormat)){
				rs.sendErrorResponse(responseFormat, "No term was given as a param for this call");
			}else if(termString == null){
				//that's not an xml request, so parse params given
				logger.log(Level.INFO, "post params");
				Term term = new RequestParams_RESTInput().parseTerm(request.getParameterMap());
				logger.log(Level.INFO, term.toString());
				handleClipping(termDbLogic, term,st.getServiceAddr(request));
				String outputText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputText(term);
				logger.log(Level.INFO, "qrf, response!: " + outputText);
				rs.sendResponse(outputText);
			}else{
				//xml request send, and is valid
				logger.log(Level.INFO, "xml params");
				Term term = new XML_RESTInput().parseTerm(termString);
				handleClipping(termDbLogic, term,st.getServiceAddr(request));
				String outputText = RESTOutputFactory.getRESTOutput(responseFormat).getOutputText(term);
				rs.sendResponse(outputText);
			}
		} catch (MalformedStringException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			rs.sendErrorResponse(responseFormat,"Malformed term send : " + e.getMessage());
			return;
		} catch (ResourceNotFoundException e) {
			logger.log(Level.WARNING, e.getMessage());
			rs.sendErrorResponse(responseFormat, "Given termURI was not found in the storage");
			return;
		} catch (IllegalTypeException e) {
			logger.log(Level.WARNING, e.getMessage());
			rs.sendErrorResponse(responseFormat, "Given termURI is not a valid URI for term resources");
			return;
		} catch (MalformedRequestException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			rs.sendErrorResponse(responseFormat,"Malformed term send : " + e.getMessage());
			return;
		}
			
	}
	
	/**
	 * Delete method handles removing  Terms from the storage
	 * 
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ServletTools st = new ServletTools();
		ResponseSender rs = new ResponseSender(response);
		TermDbLogic termDbLogic = new TermDbLogic();
		RequestResponseFormat responseFormat = st.getRequestResponseFormat(request,RequestParameter.RESPONSE_FORMAT, rs);
		//if null then errorResponse have already been send and can return
		if(responseFormat == null) 
			return;
		
		URI termURI = st.extractResourceURI(request);
		if (termURI != null) {
			// check uri
			try {
				Term term = termDbLogic.getTerm(termURI);
				termDbLogic.deleteTerm(term);
				rs.sendResponse("OK");
				return;
				// IllegalArgumentException is thrown by URIImpl constructor when URI validation fails
			} catch (IllegalArgumentException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Malformed request. TermURI is not a valid URI");
				return;
			} catch (ResourceNotFoundException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Given termURI was not found in the storage");
				return;
			} catch (IllegalTypeException e) {
				logger.log(Level.WARNING, e.getMessage());
				rs.sendErrorResponse(responseFormat, "Given termURI is not a valid URI for term resources");
				return;
			}
		}
		rs.sendErrorResponse(responseFormat, "Malformed request. TermURI not given and is required with this call");
		return;
	}
	
	/**
	 * @param termDbLogic
	 * @param term
	 * @param serviceAddress
	 * @throws ResourceNotFoundException
	 * @throws IllegalTypeException
	 */
	private void handleClipping(TermDbLogic termDbLogic, Term term, String serviceAddress) throws ResourceNotFoundException,
			IllegalTypeException {
		//new term was given, then add it
		if(term.getURI() == null || "".equals(term.getURI().toString())){
			termDbLogic.addTerm(term, serviceAddress);
		//otherwise do update
		}else{
			termDbLogic.updateTerm(term);
		}
	}
}