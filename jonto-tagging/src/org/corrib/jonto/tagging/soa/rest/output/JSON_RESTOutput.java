package org.corrib.jonto.tagging.soa.rest.output;

import java.util.List;

import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Outputable;
import org.corrib.jonto.tagging.beans.Tagging;
import org.corrib.jonto.tagging.beans.Term;
import org.json.jdk5.simple.JSONArray;
import org.json.jdk5.simple.JSONObject;

public class JSON_RESTOutput implements RESTOutput {

	public String getErrorOutputText(String errorMessage) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("error", errorMessage);
		return jsonObject.toString();
	}

	public String getOutputText(List<Outputable> outputables) {
		JSONArray jsonArray = new JSONArray();
		JSONObject json = new JSONObject();
		for(Outputable outputable : outputables){
			JSONObject jsonElement = new JSONObject();
			jsonElement.put(outputable.getJSONKey(), outputable.toJSON());
			jsonArray.add(jsonElement);
		}
		json.put("outputables", jsonArray);
		return json.toString();
	}

	public String getOutputText(Outputable outputable) {
		JSONObject json = new JSONObject();
		json.put(outputable.getJSONKey(), outputable.toJSON());
		return json.toString();
	}

	public String getOutputTextForClippings(List<Clipping> clippings) {
		JSONObject json = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		for(Clipping clipping : clippings){
			jsonArray.add(clipping.toJSON());
		}
		json.put("clippings", jsonArray);
		return json.toString();
	}

	public String getOutputTextForTaggings(List<Tagging> taggings) {
		JSONObject json = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		for(Tagging tagging : taggings){
			jsonArray.add(tagging.toJSON());
		}
		json.put("taggings", jsonArray);
		return json.toString();
	}

	public String getOutputTextForTerms(List<Term> terms) {
		JSONObject json = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		for(Term term : terms){
			jsonArray.add(term.toJSON());
		}
		json.put("terms", jsonArray);
		return json.toString();
	}

}
