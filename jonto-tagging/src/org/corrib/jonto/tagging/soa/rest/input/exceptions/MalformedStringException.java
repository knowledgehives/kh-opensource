/**
 * 
 */
package org.corrib.jonto.tagging.soa.rest.input.exceptions;

/**
 * @author Jakub Demczuk
 *
 */
public class MalformedStringException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8239545386978928849L;

	/**
	 * 
	 */
	public MalformedStringException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public MalformedStringException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public MalformedStringException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public MalformedStringException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

}
