/**
 * 
 */
package org.corrib.jonto.tagging.soa.rest.input.clipping;

import org.corrib.jonto.tagging.beans.Clipping;

/**
 * @author Jakub Demczuk
 *
 */
public class ClippingInput {
	
	/**
	 * Sets the URI property based on the input properties given
	 * <b>The URI must be the first(index=0) element in the given parameters</b>
	 * @param clipping <tt>Clipping</tt> clipping for which to set URI
	 * @param properties <tt>String...</tt> properties to use
	 */
	public void setProperties(Clipping clipping, String... properties){
		if(properties[0] != null)
			clipping.setURI(properties[0]);
		
		ClippingInputFactory.getClipping(clipping).setProperties(clipping,properties);
	}
}
