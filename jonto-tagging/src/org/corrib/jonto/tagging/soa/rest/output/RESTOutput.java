/**
 * 
 */
package org.corrib.jonto.tagging.soa.rest.output;

import java.util.List;

import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Outputable;
import org.corrib.jonto.tagging.beans.Tagging;
import org.corrib.jonto.tagging.beans.Term;

/**
 * @author Jakub Demczuk
 *
 */
public interface RESTOutput {

	/**
	 * Returns text response to be send in a response for the given Taggings
	 * @param taggings <tt>List{@literal<Taggings>}</tt> taggings for which to generate response
	 * @return <tt>String</tt> string to include in servlet response
	 */
	String getOutputTextForTaggings(List<Tagging> taggings);
	
	/**
	 * Returns text response to be send in a response for the given Outputable objects
	 * @param clippings <tt>List{@literal<Clipping>}</tt> clippings for which to generate response
	 * @return <tt>String</tt> string to include in servlet response
	 */
	String getOutputTextForClippings(List<Clipping> clippings);
	
	/**
	 * Returns text response to be send in a response for the given Clippings
	 * @param terms <tt>List{@literal<Term>}</tt> terms for which to generate response
	 * @return <tt>String</tt> string to include in servlet response
	 */
	String getOutputTextForTerms(List<Term> terms);
	
	/**
	 * Returns text response to be send in a response for the given Terms
	 * <b>NOTE:</b>It may contain various objects (like Tagging, Clipping, Term for eg. )
	 * @param outputables <tt>List{@literal<Outputable>}</tt> outputables for which to generate response
	 * @return <tt>String</tt> string to include in servlet response
	 */
	String getOutputText(List<Outputable> outputables);
	
	/**
	 * Returns text response to be send in a response for the given Outputable object <br/>
	 * @param object <tt>Outputable</tt> outputable instance for with to prepare response
	 * @return <tt>String</tt> string to include in servlet response
	 */
	String getOutputText(Outputable object);
	
	/**
	 * Returns text response for error messages
	 * @param errorMessage<tt>String</tt> message string describing error
	 */
	String getErrorOutputText(String errorMessage);
}
