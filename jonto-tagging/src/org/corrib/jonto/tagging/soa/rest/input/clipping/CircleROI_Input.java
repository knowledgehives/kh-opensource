package org.corrib.jonto.tagging.soa.rest.input.clipping;

import org.corrib.jonto.tagging.beans.CircleROI;
import org.corrib.jonto.tagging.beans.Clipping;

public class CircleROI_Input extends ROI_Input {
	/**
	 * Sets the radius property based on the input properties given<br/>
	 * <b>The radius must be the third(index=4) element in the given parameters</b><br/>
	 * @param clipping <tt>Clipping</tt> clipping that's a circle
	 * @param properties <tt>String...</tt> properties to use
	 */
	public void setProperties(Clipping clipping, String... properties){
		CircleROI circle = (CircleROI)clipping;
		if(properties.length >= 5 && properties[4] != null){
			int radius = Integer.parseInt(properties[4]);
			circle.setRadius(radius);
		}
	}
}
