/**
 * 
 */
package org.corrib.jonto.tagging.soa.rest.output;

import org.corrib.jonto.tagging.soa.tools.RequestResponseFormat;

/**
 * @author Jakub Demczuk
 *
 */
public class RESTOutputFactory {

	public static RESTOutput getRESTOutput(RequestResponseFormat outputFormat){
		if(RequestResponseFormat.XML.equals(outputFormat))
			return new XML_RESTOutput();
		else if (RequestResponseFormat.JSON.equals(outputFormat)){
			return new JSON_RESTOutput();
		}
		throw new IllegalArgumentException("OutputFormat not yet supported : " + outputFormat.toString());
	}
}
