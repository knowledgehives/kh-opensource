/**
 * 
 */
package org.corrib.jonto.tagging.soa.tools;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.corrib.jonto.tagging.soa.rest.output.RESTOutputFactory;

/**
 * @author Jakub Demczuk
 *
 */
public class ResponseSender {

	private HttpServletResponse response;
	
	public ResponseSender(HttpServletResponse _response){
		response = _response;
	}
	
	public void sendResponse(String responseText) throws IOException{
		response.setContentType("text/xml;charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		response.setStatus(HttpServletResponse.SC_OK);
		response.getWriter().write(responseText);
	}
	
	public void sendErrorResponse(RequestResponseFormat responseFormat, String responseText) throws IOException{
		response.setContentType("text/xml;charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		String message = RESTOutputFactory.getRESTOutput(responseFormat).getErrorOutputText(responseText);
		response.getWriter().write(message);
	}
	
	public void sendErrorResponse(String responseText) throws IOException{
		response.setContentType("text/xml;charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		response.getWriter().write(responseText);
	}
}
