/**
 * 
 */
package org.corrib.jonto.tagging.soa.tools;

/**
 * @author Jakub Demczuk
 *
 */
public enum RequestParameter {
	REQUEST_FORMAT("requestFormat"),
	RESPONSE_FORMAT("responseFormat"),
	DOCUMENT_URI("documentURI"),
	TAGGER_URI("taggerURI"),
	TAGGING_URI("taggingURI"),
	DELETE("delete");
	
	private String paramName;
	
	private RequestParameter(String _paramName){
		paramName = _paramName;
	}
	
	public String toString(){
		return paramName;
	}
	
}
