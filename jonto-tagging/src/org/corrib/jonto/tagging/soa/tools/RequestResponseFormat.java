/**
 * 
 */
package org.corrib.jonto.tagging.soa.tools;
import java.lang.Enum;


/**
 * @author Jakub Demczuk
 *
 */
public enum RequestResponseFormat {
	XML("xml"),
	RDF("rdf"),
	JSON("json");
	
	private String type;
	
	private RequestResponseFormat(String _type){
		type = _type;
	}
	
	/**
	 * Get RequestResponseFormat for the given _type String
	 * @param _type <tt>String</tt> string representation of type
	 * @return <tt>RequestResponseFormat</tt> for the given string
	 */
	public static RequestResponseFormat getFromString(String _type){
		if(XML.type.equals(_type))
			return XML;
		else if(RDF.type.equals(_type))
			return RDF;
		else if (JSON.type.equals(_type))
			return JSON;
		else
			throw new IllegalArgumentException("Format not yet supported");
	}
	
	public String toString(){
		return type;
	}
}
