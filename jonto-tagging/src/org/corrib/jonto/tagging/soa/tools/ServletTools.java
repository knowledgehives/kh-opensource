/**
 * 
 */
package org.corrib.jonto.tagging.soa.tools;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;

import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;

/**
 * @author Jakub Demczuk
 */
public class ServletTools {
	
	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.soa.rest.ServletTools");
	public ServletTools(){
		//default empty constructor
	}
	/**
	 * Gets response/request format from the given request based on the parameter
	 * @param request <tt>HttpServletRequest</tt> request with responseFormat parameter
	 * @param parameter <tt>RequestParameter</tt> parameter from request which value corresponds to format 
	 * @param rs <tt>ResponseSender</tt> to use when sending error response (param not valid or not found)
	 * @return <tt>RequestResponseFormat</tt> requested response format
	 * @throws IOException
	 */
	public RequestResponseFormat getRequestResponseFormat(HttpServletRequest request,RequestParameter parameter,ResponseSender rs) throws IOException{
		String sResponeFormat = request.getParameter(parameter.toString());
		RequestResponseFormat responseFormat = null;
		// check if valid responseFormat was given
		if (sResponeFormat != null && !"".equals(sResponeFormat)) {
			try{
				responseFormat = RequestResponseFormat.getFromString(sResponeFormat);
				return responseFormat;
			}catch(IllegalArgumentException e){
//				rs.sendErrorResponse(e.getMessage());
				return null;
			}
		} else if(RequestParameter.RESPONSE_FORMAT.equals(parameter)){
			rs.sendErrorResponse("Malformed request. responseFormat is required with this call but none was given");
		}
		return null;
	}
	
	/**
	 * @param request <tt>HttpServletRequest</tt> request with the resource uri to extract from
	 * @return <tt>URI</tt> URI of resource
	 * @throws IllegalArgumentException is thrown when the string URI from the request is not valid
	 */
	public URI extractResourceURI(HttpServletRequest request) throws IllegalArgumentException{
		
		String requestURI = request.getRequestURI();
		String sResourceURI = null;
		logger.log(Level.INFO, requestURI);
		if (requestURI != null && !"/".equals(requestURI)) {
			String[] URIparts = requestURI.split("/");
			sResourceURI = URIparts[URIparts.length-1];
			try {
				sResourceURI = URLDecoder.decode(sResourceURI, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				//this should never happen, as encoding is set by hand
			}
			// check uri
			URI taggingURI = new URIImpl(sResourceURI);
			return taggingURI;
		}
		return null;
			
	}
	
	public String getServiceAddr(HttpServletRequest request)
	{
		StringBuffer servAddr = new StringBuffer(); 
		
		servAddr.append("http://");
		servAddr.append(request.getLocalName());
		if(request.getLocalPort()!=0&&request.getLocalPort()!=80)
		{
			servAddr.append(":");
			servAddr.append(request.getLocalPort());
		}
		if(request.getContextPath()!=null&&!"".equals(request.getContextPath()))
		{
			servAddr.append(request.getContextPath());
		}
		//servAddr.append("/");
		
	
		return servAddr.toString();
	}
}
