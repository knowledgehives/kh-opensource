/**
 * 
 */
package org.corrib.jonto.tagging.beans;

import org.ontoware.rdf2go.model.node.URI;



/**
 * Interface for clipping types
 * @author Jakub Demczuk
 */
public interface Clipping extends Outputable{

	/**
	 * Returns URI of this clipping
	 * @return <tt>URI</tt> uri of clipping
	 */
	URI getURI();

	/**
	 * Sets URI of the clipping
	 * @param _uri <tt>URI</tt> new URI to set
	 */
	void setURI(URI _uri);
	
	/**
	 * Sets URI of the clipping
	 * @param _uri <tt>String</tt> string representation of the new URI
	 */
	void setURI(String _uri);

	/**
	 * Gets URI of the tagging that this clipping belongs to
	 * @return <tt>URI</tt> tagger URI
	 */
	URI getTaggingURI();

	/**
	 * Sets the tagging URI (tagging that this clipping belongs to)
	 * @param _taggingURI <tt>URI</tt> tagging uri for this clipping
	 */
	void setTaggingURI(URI _taggingURI);
	
	/**
	 * Sets the tagging URI (tagging that this clipping belongs to)
	 * @param _taggingURI <tt>String</tt> string representation of tagging uri for this clipping
	 */
	void setTaggingURI(String _taggingURI);
	
}
