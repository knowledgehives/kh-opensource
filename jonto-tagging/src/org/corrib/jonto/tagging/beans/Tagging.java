package org.corrib.jonto.tagging.beans;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.corrib.jonto.tagging.GlobalTools;
import org.corrib.jonto.tagging.S3B_MULTIMEDIATAGGING;
import org.corrib.jonto.tagging.db.UriGenerator;
import org.json.jdk5.simple.JSONArray;
import org.json.jdk5.simple.JSONObject;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;

/**
 * This class represents a tagging of a document by a tagger <br/>
 * @author Jakub Demczuk
 *
 */
public class Tagging implements Outputable{
	
	/**
	 * URI of this tagging;
	 */
	private URI uri;
	
	/**
	 * URI of the document that has been tagged by this tagging 
	 */
	private URI documentURI;
	
	/**
	 * URI of an Agent that created this tagging (eg. FOAF Person)
	 */
	private URI taggerURI;
	
	/**
	 * A clipping for this tagging (defining it's area/time-span)
	 * may be null if it's not a ROI or Excerpt tagging
	 */
	private Clipping clipping;
	
	/**
	 * A list of terms describing this tagging 
	 */
	private List<Term> terms;
	
	/**
	 * Creation time of this tagging
	 */
	private Date creationTime;
	
	/**
	 * A title of this tagging 
	 */
	private String title;
	
	/**
	 * Longer description of this tagging, comment
	 */
	private String description;
	
	/**
	 * Indicates whether this tagging is publicly available
	 */
	private boolean shared;
	
	
	/**
	 * List of external resources this tagging links to
	 */
	private List<URI> links;
	
	/**
	 * List of internal resources connected to this tagging
	 */
	private List<URI> crossReferences;
	
	private List<String> crStrings;
	
	/**
	 * Default constructor
	 */
	public Tagging(){
		links = new ArrayList<URI>();
		crossReferences = new ArrayList<URI>();
		terms = new ArrayList<Term>();
		crStrings = new ArrayList<String>();
	}
	
	/**
	 * @param _uri <tt>URI</tt> uri of tagging to create
	 */
	public Tagging(URI _uri){
		this();
		uri = _uri;
	}
	
	/**
	 * @param _uri <tt>String</tt> uri of tagging to create
	 */
	public Tagging(String _uri){
		this();
		uri = new URIImpl(_uri);
	}
	
	/**
	 * Creates tagging for the given document
	 * @param _documentURI <tt>URI</tt> URI of the document this tagging was made for
	 * @param _taggerURI <tt>URI</tt> URI of the tagger that made this tagging
	 */
	public Tagging(URI _documentURI, URI _taggerURI){
		this();
		documentURI = _documentURI;
		taggerURI = _taggerURI;
	}
	
	
	/**
	 * Creates tagging for the given document
	 * @param _documentURI <tt>String</tt> URI of the document this tagging was made for
	 * @param _taggerURI <tt>String</tt> URI of the tagger that made this tagging
	 */
	public Tagging(String _documentURI, String _taggerURI){
		this();
		documentURI = new URIImpl(_documentURI);
		taggerURI = new URIImpl(_taggerURI);
	}
	
	

	public URI getURI() {
		return uri;
	}
	
	public String encodeURI(URI _uriToEncode){
		try {
			String encode = URLEncoder.encode(_uriToEncode.toString(),"UTF-8");
			return encode;
		} catch (UnsupportedEncodingException e) {
			//cannot happen
		}
		return null;
	}

	public void setURI(URI _uri) {
		this.uri = _uri;
	}
	public void setURI(String _uri) {
		this.uri = new URIImpl(_uri);
	}

	public URI getDocumentURI() {
		return documentURI;
	}

	public void setDocumentURI(URI _documentURI) {
		this.documentURI = _documentURI;
	}
	
	public void setDocumentURI(String _documentURI) {
		this.documentURI = new URIImpl(_documentURI);
	}

	public URI getTaggerURI() {
		return taggerURI;
	}

	public void setTaggerURI(URI _taggerURI) {
		this.taggerURI = _taggerURI;
	}
	
	public void setTaggerURI(String _taggerURI) {
		this.taggerURI = new URIImpl(_taggerURI);
	}

	public Clipping getClipping() {
		return clipping;
	}

	public void setClipping(Clipping _clipping) {
		if(_clipping != null)
			_clipping.setTaggingURI(uri);
		this.clipping = _clipping;
	}

	/**
	 * Returns a list of terms for this tagging <br/>
	 * Never returns null, empty list instead
	 * @return <tt>List{@literal<Term>}</tt> list of terms
	 */
	public List<Term> getTerms() {
		if(terms == null)
			terms = new ArrayList<Term>();
		return terms;
	}
	
	public void setTerms(List<Term> _terms){
		if(_terms == null){
			terms = new ArrayList<Term>();
			return;
		}
		this.terms = _terms;
	}
	
	public void addTerm(Term term){
		term.setTaggingURI(uri);
		getTerms().add(term);
	}
	
	public void addTerms(List<Term> _terms){
		for(Term term : _terms){
			addTerm(term);
		}
	}

	public Date getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Date _creationTime) {
		this.creationTime = _creationTime;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String _title) {
		this.title = _title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String _description) {
		this.description = _description;
	}

	
	public boolean isShared() {
		return shared;
	}

	public void setShared(boolean isShared) {
		this.shared = isShared;
	}
	
	
	public List<URI> getLinks() {
		if(links == null)
			links = new ArrayList<URI>();
		return links;
	}
	
	public void setLinks(List<URI> _links) {
		this.links = _links;
	}
	
	public void addLink(URI link){
		getLinks().add(link);
	}
	
	public void addLink(String link){
		getLinks().add(new URIImpl(link));
	}
	
	public void addLinks(List<URI> _links){
		getLinks().addAll(_links);
	}
	
	public void addLinksFromStrings(String... _links){
		for(String link : _links){
			getLinks().add(new URIImpl(link));
		}
	}

	public List<URI> getCrossReferences() {
		if(crossReferences == null)
			crossReferences = new ArrayList<URI>();
		return crossReferences;
	}

	public List<String> getCrossReferenceStrings() {
		if(crStrings == null)
			crStrings = new ArrayList<String>();
		return crStrings;
	}

	public void setCrossReferences(List<URI> _crossReferences) {
		this.crossReferences = _crossReferences;
	}
	
	public void setCrossReferenceStrings(List<String> _crStrings)
	{
		this.crStrings = _crStrings;
	}
	
	public void addCrossReference(URI _crossReference){
		getCrossReferences().add(_crossReference);
	}
	
	public void addCrossReference(String _crossReference){
		getCrossReferences().add(new URIImpl(_crossReference));
	}
	
	public void addCrossReferences(List<URI> _crossReferences){
		getCrossReferences().addAll(_crossReferences);
	}
	
	public void addCrossReferencesFromStrings(String... _crossReferences){
		for(String crossReference : _crossReferences){
			URIImpl crURI = UriGenerator.getUriForCrossReference(this, crossReference);
			getCrossReferenceStrings().add(crossReference);
			getCrossReferences().add(crURI);
		}
	}

	public Map<String, Object> getPropertiesMap() {
		Map<String, Object> propertiesMap = new HashMap<String, Object>();
		propertiesMap.put("documentURI", this.getDocumentURI());
		propertiesMap.put("taggerURI", this.getTaggerURI());
		propertiesMap.put("title", this.getTitle());
		propertiesMap.put("description", this.getDescription());
		propertiesMap.put("documentURI", this.getDocumentURI());
		propertiesMap.put("shared",isShared());
		DateFormat sdf = GlobalTools.DATE_TIME_FORMAT;
		if(getCreationTime() != null){
			propertiesMap.put("creationTime", sdf.format(getCreationTime()));
		}
		return propertiesMap;
	}
	
	public String getJSONKey() {
		return encodeURI(S3B_MULTIMEDIATAGGING.TAGGING.asURI());
	}
	
	public JSONObject toJSON(){
		JSONObject json = new JSONObject();
		
		json.put("uri", encodeURI(uri));
		json.put("documentURI", encodeURI(this.getDocumentURI()));
		json.put("taggerURI", encodeURI(this.getTaggerURI()));
		json.put("title", this.getTitle());
		json.put("description", this.getDescription());
		json.put("clipping", clipping.toJSON());
		json.put("shared",isShared());
		DateFormat sdf = GlobalTools.DATE_TIME_FORMAT;
		if(getCreationTime() != null){
			json.put("creationTime", sdf.format(getCreationTime()));
		}
		
		JSONArray jsonTerms = new JSONArray();
		for(Term term : terms){
			jsonTerms.add(term.toJSON());
		}
		json.put("terms", jsonTerms);
		
		JSONArray jsonLinks = new JSONArray();
		for(URI link : links){
			jsonLinks.add(encodeURI(link));
		}
		json.put("links",jsonLinks);
		
		JSONArray jsonCrossReferences = new JSONArray();
		for(URI crossReference : crossReferences){
			jsonCrossReferences.add(encodeURI(crossReference));
		}
		json.put("crossReferences",jsonCrossReferences);
		return json;
	}
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(uri);
		sb.append(" : doc = ");
		sb.append(documentURI);
		sb.append(" , tagger = ");
		sb.append(taggerURI);
		return sb.toString();
	}
	
	
	public boolean equals(Object o){
		if (o instanceof Tagging) {
			Tagging obj = (Tagging) o;
			if( ((uri != null) && uri.equals(obj.getURI())) &&
					( (documentURI == null && obj.getDocumentURI() == null ) || documentURI.equals(obj.getDocumentURI()) )&&
					( (taggerURI == null  && obj.getTaggerURI() == null) || taggerURI.equals(obj.getTaggerURI()) )&&
					( (title == null && obj.getTitle() == null) || title.equals(obj.getTitle()) ) &&
					( (description == null && obj.getDescription() == null) || description.equals(obj.getDescription()) )&&
					( (creationTime == null && obj.getCreationTime() == null) || creationTime.equals(obj.getCreationTime()) )&&
					( (links == null && obj.getLinks() == null) || links.equals(obj.getLinks()) )&&
					( (crossReferences == null && obj.getCrossReferences() == null) || crossReferences.equals(obj.getCrossReferences()) )&&
					shared == obj.isShared() &&
					( (clipping == null && obj.getClipping() == null) || clipping.equals(obj.getClipping()) )&&
					( (terms == null && obj.getTerms() == null) || terms.equals(obj.getTerms()))){
				return true;
			}
		}
		return false;
	}

	

}
