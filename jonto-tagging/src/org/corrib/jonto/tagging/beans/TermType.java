/**
 * 
 */
package org.corrib.jonto.tagging.beans;

import org.corrib.jonto.tagging.S3B_MULTIMEDIATAGGING;
import org.ontoware.rdf2go.model.node.URI;

/**
 * Enum representing various Term types used in the multimedia tagging ontology (like setting term, action term)
 * @author Jakub Demczuk
 *
 */
public enum TermType {
	
	/**
	 * http://s3b.corrib.org/tagging/hasActionTerm
	 */
	ACTION_TERM(S3B_MULTIMEDIATAGGING.HAS_ACTION_TERM.asString(),S3B_MULTIMEDIATAGGING.HAS_ACTION_TERM.asURI()),
	
	/**
	 * http://s3b.corrib.org/tagging/hasAgentTerm
	 */
	AGENT_TERM(S3B_MULTIMEDIATAGGING.HAS_AGENT_TERM.asString(),S3B_MULTIMEDIATAGGING.HAS_AGENT_TERM.asURI()),
	
	/**
	 * http://s3b.corrib.org/tagging/hasObjectTerm
	 */
	OBJECT_TERM(S3B_MULTIMEDIATAGGING.HAS_OBJECT_TERM.asString(),S3B_MULTIMEDIATAGGING.HAS_OBJECT_TERM.asURI()),
	
	/**
	 * http://s3b.corrib.org/tagging/hasSettingTerm
	 */
	SETTING_TERM(S3B_MULTIMEDIATAGGING.HAS_SETTING_TERM.asString(),S3B_MULTIMEDIATAGGING.HAS_SETTING_TERM.asURI()),
	
	/**
	 * http://s3b.corrib.org/tagging/hasTerm
	 */
	SIMPLE_TERM(S3B_MULTIMEDIATAGGING.HAS_TERM.asString(),S3B_MULTIMEDIATAGGING.HAS_TERM.asURI());
	
	private URI termPredicate;
	private final String termLabel;
	
	private TermType(String _termLabel, URI _termPredicate){
		termLabel = _termLabel;
		termPredicate = _termPredicate;
	}

	/**
	 * Gets label that is associated with the chosen term
	 * @return <tt>String</tt> string representing this term
	 */
	public String getTermLabel() {
		return termLabel;
	}

	/**
	 * Gets the predicate that is associated with the chosen term
	 * @return <tt>URI</tt> predicate value of this term type
	 */
	public URI getTermPredicate() {
		return termPredicate;
	}
	
	public String toString(){
		return termLabel;
	}
	
	/**
	 * Returns term type based on the label given
	 * @param termLabel <tt>String</tt> 
	 * @return
	 */
	public static TermType getTermType(String termLabel){
		if(ACTION_TERM.getTermLabel().equals(termLabel))
			return ACTION_TERM;
		else if(AGENT_TERM.getTermLabel().equals(termLabel))
			return AGENT_TERM;
		else if(OBJECT_TERM.getTermLabel().equals(termLabel))
			return OBJECT_TERM;
		else if(SETTING_TERM.getTermLabel().equals(termLabel))
			return SETTING_TERM;
		else
			return SIMPLE_TERM;
	}
}
