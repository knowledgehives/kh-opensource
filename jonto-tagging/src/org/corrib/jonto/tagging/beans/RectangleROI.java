/**
 * 
 */
package org.corrib.jonto.tagging.beans;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.S3B_MULTIMEDIATAGGING;
import org.json.jdk5.simple.JSONObject;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;

/**
 * @author Jakub Demczuk
 *
 */
public class RectangleROI implements ROI {
	
	
	private int width;
	private int height;
	private URI uri;
	private URI taggingURI;
	private int xCoordinate;
	private int yCoordinate;
	
	
	/**
	 * Default constructor
	 */
	public RectangleROI(){
		//empty default constructor
	}
	/**
	 * @param _uri
	 */
	public RectangleROI(URI _uri) {
		this.uri = _uri;
	}

	public RectangleROI(String _uri){
		this(new URIImpl(_uri));
	}

	/**
	 * @param _xCoordinate
	 * @param _yCoordinate
	 * @param _width
	 * @param _height
	 */
	public RectangleROI(int _xCoordinate, int _yCoordinate, int _width, int _height) {
		xCoordinate = _xCoordinate;
		yCoordinate = _yCoordinate;
		width = _width;
		height = _height;
	}
	
	public URI getURI() {
		return uri;
	}
	
	public String encodeURI(URI _uriToEncode){
		try {
			String encode = URLEncoder.encode(_uriToEncode.toString(),"UTF-8");
			return encode;
		} catch (UnsupportedEncodingException e) {
			//cannot happen
		}
		return null;
	}
	
	public void setURI(URI _uri) {
		uri = _uri;
	}

	public void setURI(String _uri) {
		uri = new URIImpl(_uri);
	}

	public URI getTaggingURI() {
		return taggingURI;
	}
	
	public void setTaggingURI(URI _taggingURI) {
		taggingURI = _taggingURI;
	}

	public void setTaggingURI(String _taggingURI) {
		taggingURI = new URIImpl(_taggingURI);
	}
	
	public int getXCoordinate() {
		return xCoordinate;
	}

	public int getYCoordinate() {
		return yCoordinate;
	}

	public void setXCoordinate(int _xCoordinate) {
		xCoordinate = _xCoordinate;
	}

	public void setYCoordinate(int _yCoordinate) {
		yCoordinate = _yCoordinate;
	}
	
	public int getWidth() {
		return width;
	}
	public void setWidth(int _width) {
		this.width = _width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int _height) {
		this.height = _height;
	}
	
	public Map<String, Object> getPropertiesMap() {
		Map<String, Object> propertiesMap = new HashMap<String, Object>();
		Field[] fields = this.getClass().getDeclaredFields();
		for(Field field : fields){
			String name = field.getName();
			if(name.equals("uri"))
				continue;
			Object value;
			try {
				value = field.get(this);
				propertiesMap.put(name, value);
			} catch (IllegalArgumentException e) {
				Logger.getLogger("org.corrib.jonto.tagging.beans.RectangleROI").log(Level.SEVERE, e.getMessage(), e);
			} catch (IllegalAccessException e) {
				Logger.getLogger("org.corrib.jonto.tagging.beans.RectangleROI").log(Level.SEVERE, e.getMessage(), e);
			}
		}
		return propertiesMap;
	}
	
	public JSONObject toJSON(){
		JSONObject json = new JSONObject();
		
		Field[] fields = this.getClass().getDeclaredFields();
		for(Field field : fields){
			String name = field.getName();
			Object value;
			try {
				if(name.contains("uri") || name.contains("URI")){
					value = encodeURI((URI) field.get(this));
				}else{
					value = field.get(this);
				}
				json.put(name, value);
			} catch (IllegalArgumentException e) {
				Logger.getLogger("org.corrib.jonto.tagging.beans.RectangleROI").log(Level.SEVERE, e.getMessage(), e);
			} catch (IllegalAccessException e) {
				Logger.getLogger("org.corrib.jonto.tagging.beans.RectangleROI").log(Level.SEVERE, e.getMessage(), e);
			}
		}
//		json.put("uri", (uri != null) ? uri.toString() : null);
//		json.put("taggingURI",(taggingURI != null) ? taggingURI.toString() : null);
//		json.put("xCoordinate", xCoordinate);
//		json.put("yCoordinate", yCoordinate);
//		json.put("width", width);
//		json.put("height", height);
		return json;
	}
	public String getJSONKey() {
		return encodeURI(S3B_MULTIMEDIATAGGING.RECTANGLE_ROI.asURI());
	}
}
