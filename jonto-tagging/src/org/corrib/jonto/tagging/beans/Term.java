/**
 * 
 */
package org.corrib.jonto.tagging.beans;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.S3B_MULTIMEDIATAGGING;
import org.json.jdk5.simple.JSONObject;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;


/**
 * Class representing the Term concept from tagging ontology
 * @author Jakub Demczuk
 *
 */
public class Term implements Outputable{
	
	
	private URI uri;
	private URI taggingURI;
	private String tag;
	private TermType termType;
	
	
	/**
	 * Create an instance of term with only uri set
	 * @param _uri <tt>URI</tt> uri of the term
	 */
	public Term(URI _uri){
		uri = _uri;
	}
	
	/**
	 * Create an instance of term with only uri set
	 * @param _uri <tt>String</tt> uri of the term
	 */
	public Term(String _uri){
		uri = new URIImpl(_uri);
	}
	
	/**
	 * @param _taggingURI <tt>URI</tt> string uri of the tagging this term applies to
	 * @param _termType <tt>TermType</tt> type of this term
	 * @param _tag <tt>String</tt> tag describing this term
	 */
	public Term(URI _taggingURI, TermType _termType,String _tag) {
		taggingURI = _taggingURI;
		tag = _tag;
		termType = _termType;
	}
	
	/**
	 * Creates a term of SIMPLE_TERM type with the given tags
	 * @param _taggingURI <tt>URI</tt> string uri of the tagging this term applies to
	 * @param _tag <tt>String</tt> tags for this term
	 */
	public Term(URI _taggingURI,String _tag) {
		taggingURI = _taggingURI;
		tag = _tag;
		termType = TermType.SIMPLE_TERM;
	}
	
	/**
	 * Default empty constructor
	 */
	public Term(){
		//empty default constructor
	}

	public URI getURI() {
		return uri;
	}
	
	public String encodeURI(URI _uriToEncode){
		try {
			String encode = URLEncoder.encode(_uriToEncode.toString(),"UTF-8");
			return encode;
		} catch (UnsupportedEncodingException e) {
			//cannot happen
		}
		return null;
	}

	public void setURI(URI _uri) {
		uri = _uri;
	}
	
	public void setURI(String _uri){
		uri = new URIImpl(_uri);
	}

	public String getTag() {
		return tag;
	}
	public void setTag(String _tag) {
		tag = _tag;
	}
	public TermType getTermType() {
		return termType;
	}
	public void setTermType(TermType _termType) {
		this.termType = _termType;
	}

	public URI getTaggingURI() {
		return taggingURI;
	}

	public void setTaggingURI(URI _taggingURI) {
		taggingURI = _taggingURI;
	}
	
	public void setTaggingURI(String _taggingURI) {
		taggingURI = new URIImpl(_taggingURI);
	}

	
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(taggingURI);
		sb.append(" : ");
		sb.append(termType.getTermPredicate());
		sb.append(" : ");
		sb.append(tag);
		return sb.toString();
		
	}
	
	public boolean equals(Object o){
		if(o instanceof Term){
			Term term2 = (Term)o;
			if(this.getTaggingURI().equals(term2.getTaggingURI()) &&
					this.getTermType().equals(term2.getTermType()) &&
					this.tag.equals(term2.getTag())){
				
				return true;
			}
		}
		return false;
	}
	
	public Map<String, Object> getPropertiesMap() {
		Map<String, Object> propertiesMap = new HashMap<String, Object>();
		Field[] fields = this.getClass().getDeclaredFields();
		for(Field field : fields){
			String name = field.getName();
			if(name.equals("uri") || name.equals("termType"))
				continue;
			Object value;
			try {
				value = field.get(this);
				propertiesMap.put(name, value);
			} catch (IllegalArgumentException e) {
				 Logger.getLogger("org.corrib.jonto.tagging.beans.Term").log(Level.SEVERE, e.getMessage(), e);
			} catch (IllegalAccessException e) {
				 Logger.getLogger("org.corrib.jonto.tagging.beans.Term").log(Level.SEVERE, e.getMessage(), e);
			}
		}
		return propertiesMap;
	}
	
	
	public JSONObject toJSON(){
		JSONObject json = new JSONObject();
		Field[] fields = this.getClass().getDeclaredFields();
		for(Field field : fields){
			String name = field.getName();
			Object value;
			try {
				//special case when value has to be encoded by hand
				if ("termType".equals(name)){
					value = encodeURI(termType.getTermPredicate());
				}else if(name.contains("uri") || name.contains("URI")){
					value = encodeURI((URI) field.get(this));
				}else{
					value = field.get(this);
				}
				json.put(name, value);
			} catch (IllegalArgumentException e) {
				Logger.getLogger("org.corrib.jonto.tagging.beans.Term").log(Level.SEVERE, e.getMessage(), e);
			} catch (IllegalAccessException e) {
				Logger.getLogger("org.corrib.jonto.tagging.beans.Term").log(Level.SEVERE, e.getMessage(), e);
			}
		}
		return json;
	}

	public String getJSONKey() {
		return encodeURI(S3B_MULTIMEDIATAGGING.TERM.asURI());
	}
}
