/**
 * 
 */
package org.corrib.jonto.tagging.beans;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.S3B_MULTIMEDIATAGGING;
import org.json.jdk5.simple.JSONObject;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;

/**
 * Entity class for Excert type objects
 * @author Jakub Demczuk
 *
 */
public class Excerpt implements Clipping {
	
	private URI uri;
	private URI taggingURI;
	private long startPoint;
	private long length;

	
	/**
	 * Default constructor
	 */
	public Excerpt(){
		//empty default constructor
	}
	/**
	 * @param _uri
	 */
	public Excerpt(URI _uri) {
		this.uri = _uri;
	}

	public Excerpt(String _uri){
		this(new URIImpl(_uri));
	}
	
	/**
	 * @param _uri
	 * @param _taggingURI
	 * @param _startPoint
	 * @param _length
	 */
	public Excerpt(URI _uri, URI _taggingURI, long _startPoint, long _length) {
		this.uri = _uri;
		this.taggingURI = _taggingURI;
		this.startPoint = _startPoint;
		this.length = _length;
	}

	public URI getURI() {
		return uri;
	}
	
	public String encodeURI(URI _uriToEncode){
		try {
			String encode = URLEncoder.encode(_uriToEncode.toString(),"UTF-8");
			return encode;
		} catch (UnsupportedEncodingException e) {
			//cannot happen
		}
		return null;
	}
	
	public void setURI(URI _uri) {
		uri = _uri;
	}
	public void setURI(String _uri) {
		uri = new URIImpl(_uri);
	}
	public URI getTaggingURI() {
		return taggingURI;
	}
	public void setTaggingURI(URI _taggingURI) {
		taggingURI = _taggingURI;
	}
	
	public void setTaggingURI(String _taggingURI) {
		taggingURI = new URIImpl(_taggingURI);
	}
	
	public long getStartPoint() {
		return startPoint;
	}
	
	public void setStartPoint(long _startPoint) {
		this.startPoint = _startPoint;
	}
	
	public long getLength() {
		return length;
	}
	
	public void setLength(long _length) {
		this.length = _length;
	}
	
	public Map<String, Object> getPropertiesMap() {
		Map<String, Object> propertiesMap = new HashMap<String, Object>();
		Field[] fields = this.getClass().getDeclaredFields();
		for(Field field : fields){
			String name = field.getName();
			if(name.equals("uri"))
				continue;
			Object value;
			try {
				value = field.get(this);
				propertiesMap.put(name, value);
			} catch (IllegalArgumentException e) {
				Logger.getLogger("org.corrib.jonto.tagging.beans.Excerpt").log(Level.SEVERE, e.getMessage(), e);
			} catch (IllegalAccessException e) {
				Logger.getLogger("org.corrib.jonto.tagging.beans.Excerpt").log(Level.SEVERE, e.getMessage(), e);
			}
		}
		return propertiesMap;
	}
	
	public JSONObject toJSON(){
		JSONObject json = new JSONObject();
		Field[] fields = this.getClass().getDeclaredFields();
		for(Field field : fields){
			String name = field.getName();
			Object value;
			try {
				if(name.contains("uri") || name.contains("URI")){
					value = encodeURI((URI) field.get(this));
				}else{
					value = field.get(this);
				}
				json.put(name, value);
			} catch (IllegalArgumentException e) {
				Logger.getLogger("org.corrib.jonto.tagging.beans.Excerpt").log(Level.SEVERE, e.getMessage(), e);
			} catch (IllegalAccessException e) {
				Logger.getLogger("org.corrib.jonto.tagging.beans.Excerpt").log(Level.SEVERE, e.getMessage(), e);
			}
		}
		return json;
	}
	public String getJSONKey() {
		return encodeURI(S3B_MULTIMEDIATAGGING.EXCERPT.asURI());
	}
	
}
