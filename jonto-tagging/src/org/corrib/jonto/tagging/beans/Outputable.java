/**
 * 
 */
package org.corrib.jonto.tagging.beans;

import java.util.Map;

import org.json.jdk5.simple.JSONObject;
import org.ontoware.rdf2go.model.node.URI;

/**
 * Interface for preparing data for output
 * @author Jakub Demczuk
 *
 */
public interface Outputable {
	/**
	 * Prepares map: [fieldName -> value] 
	 * @return <tt>Map{@literal<String,Object>}</tt> map with [field,value] pairs for this instance
	 */
	Map<String,Object> getPropertiesMap();
	
	/**
	 * Prepares JSON object string properties of the implemantor
	 * @return <tt>JSONObject</tt> JSON object describing the instance
	 */
	JSONObject toJSON();
	
	/**
	 * Gets the string key used for this object in JSON
	 * If it's an URI the it should be URL Encoded
	 * @return <tt>String</tt> key to use when putting object in JSON
	 */
	String getJSONKey();
	
	
	/**
	 * Creates an encoded String from the given URI
	 * @param _uriToEncode <tt>URI</tt> URI to encode
	 * @return <tt>String</tt> encoded URI
	 */
	String encodeURI(URI _uriToEncode);
}
