/**
 * 
 */
package org.corrib.jonto.tagging.beans.factories;

import org.corrib.jonto.tagging.S3B_MULTIMEDIATAGGING;
import org.corrib.jonto.tagging.beans.CircleROI;
import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Excerpt;
import org.corrib.jonto.tagging.beans.RectangleROI;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;

/**
 * @author Jakub Demczuk
 *
 */
public class ClippingFactory {
	
	/**
	 * Gets a new instance of clipping based on the given rdfType property
	 * @param clippingURI <tt>URI</tt> URI to set for the returned clipping
	 * @param rdfType <tt>URI</tt> s3b_multimediatagging ontology Type 
	 * @return <tt>Clipping</tt> instance of clipping for the given type
	 */
	public static Clipping getClipping(URI clippingURI, URI rdfType){
		if(S3B_MULTIMEDIATAGGING.CIRCLE_ROI.asURI().equals(rdfType)){
			return new CircleROI(clippingURI);
		}else if(S3B_MULTIMEDIATAGGING.RECTANGLE_ROI.asURI().equals(rdfType)){
			return new RectangleROI(clippingURI);
		}else if(S3B_MULTIMEDIATAGGING.EXCERPT.asURI().equals(rdfType)){
			return new Excerpt(clippingURI);
		}else
			throw new IllegalArgumentException("Clipping type not yet implemented or the given type is not a clipping: " + rdfType);
	}
	/**
	 * Gets a new instance of clipping based on the given rdfType property
	 * @param clippingURI <tt>String</tt> String uri to set for the returned clipping
	 * @param rdfType <tt>String</tt> s3b_multimediatagging ontology Type 
	 * @return <tt>Clipping</tt> instance of clipping for the given type
	 */
	public static Clipping getClipping(String clippingURI, String rdfType){
		if(clippingURI != null)
			return getClipping(new URIImpl(clippingURI), new URIImpl(rdfType));
		return getClipping(null, new URIImpl(rdfType));
	}
	/**
	 * Returns the rdfType property for the given clipping
	 * @param clipping <tt>Clipping</tt> clipping to get type for
	 * @return <tt>String</tt> rdfType property for the given clipping
	 */
	public static String getClippingType(Clipping clipping) {
		if(clipping instanceof CircleROI)
			return S3B_MULTIMEDIATAGGING.CIRCLE_ROI.asString();
		else if(clipping instanceof RectangleROI)
			return S3B_MULTIMEDIATAGGING.RECTANGLE_ROI.asString();
		else if(clipping instanceof Excerpt)
			return S3B_MULTIMEDIATAGGING.EXCERPT.asString();
		else
			throw new IllegalArgumentException("Clipping type not yes supported : " + clipping.getClass());
	}
}
