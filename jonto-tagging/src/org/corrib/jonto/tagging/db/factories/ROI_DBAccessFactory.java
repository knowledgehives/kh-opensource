/**
 * 
 */
package org.corrib.jonto.tagging.db.factories;

import org.corrib.jonto.tagging.beans.CircleROI;
import org.corrib.jonto.tagging.beans.ROI;
import org.corrib.jonto.tagging.beans.RectangleROI;
import org.corrib.jonto.tagging.db.access.CircleROI_DBAccess;
import org.corrib.jonto.tagging.db.access.ROI_DBAccess;
import org.corrib.jonto.tagging.db.access.RectangleROI_DBAccess;

/**
 * @author Jakub Demczuk
 *
 */
public class ROI_DBAccessFactory {
	public static ROI_DBAccess getROI_DBAccess(ROI roi){
		if(roi instanceof CircleROI){
			return new CircleROI_DBAccess();
		}else if (roi instanceof RectangleROI){
			return new RectangleROI_DBAccess();
		}else
			throw new IllegalArgumentException("ROI type not supported: " + roi.getClass());
	}
}
