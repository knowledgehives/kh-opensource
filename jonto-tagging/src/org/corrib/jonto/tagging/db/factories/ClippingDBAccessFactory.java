/**
 * 
 */
package org.corrib.jonto.tagging.db.factories;

import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Excerpt;
import org.corrib.jonto.tagging.beans.ROI;
import org.corrib.jonto.tagging.db.access.ClippingDbAccess;
import org.corrib.jonto.tagging.db.access.ExcerptDBAccess;
import org.corrib.jonto.tagging.db.access.ROI_DBAccess;

/**
 * @author Jakub Demczuk
 *
 */
public class ClippingDBAccessFactory {

	public static ClippingDbAccess getClippingDBAccess(Clipping clipping){
		if(clipping instanceof ROI){
			return new ROI_DBAccess();
		}else if (clipping instanceof Excerpt){
			return new ExcerptDBAccess();
		}else
			throw new IllegalArgumentException("Clipping type not supported: " + clipping.getClass());
	}
}
