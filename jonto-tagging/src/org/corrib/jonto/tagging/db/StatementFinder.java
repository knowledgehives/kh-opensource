/**
 * 
 */
package org.corrib.jonto.tagging.db;

import java.util.logging.Logger;

import org.ontoware.aifbcommons.collection.ClosableIterator;
import org.ontoware.rdf2go.model.Model;
import org.ontoware.rdf2go.model.Statement;
import org.ontoware.rdf2go.model.node.Resource;
import org.ontoware.rdf2go.model.node.URI;

/**
 * @author Jakub Demczuk
 *
 */
public class StatementFinder {

	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.db.StatementFinder");
	
	public static Statement findStatement(Model model, Resource subject, URI predicate, Resource object){
		ClosableIterator<Statement> statements = model.findStatements(subject, predicate, object);
		Statement statement = null;
		int statementCounter = 0;
		while(statements.hasNext()){
			if(statement == null){
				statement = statements.next();
			}else{
				logger.warning("MORE THAN ONE :" + statements.next().toString());
			}
			statementCounter++;
			
		}
		if(statementCounter > 1){
			logger.warning("More that one statement was found : \n" + subject + " : " + predicate + " : " + object);
		}
		return statement;
	}
}
