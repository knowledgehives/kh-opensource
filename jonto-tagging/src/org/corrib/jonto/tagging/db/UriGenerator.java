package org.corrib.jonto.tagging.db;

import org.corrib.jonto.tagging.beans.CircleROI;
import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Excerpt;
import org.corrib.jonto.tagging.beans.ROI;
import org.corrib.jonto.tagging.beans.RectangleROI;
import org.corrib.jonto.tagging.beans.Tagging;
import org.corrib.jonto.tagging.beans.Term;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;

/**
 * Class with static methods to generate IDs for 
 * new objects that are stored in repository
 * @author Jakub Demczuk
 *
 */
public class UriGenerator {
	
	private static String TAGGING = "tagging/";
	private static String CLIPPING = "clipping/";
	private static String TERM = "term/";
	
	/**
	 * Creates new id for a tagging to store
	 * @param tagging <tt>Tagging</tt> tagging for which to generate id
	 * @param serviceAddress <tt>String</tt> address of the service sending request
	 * @return <tt>URI</tt> string uri of the tagging
	 */
	public static URI getUri(Tagging tagging, String serviceAddress){
		StringBuilder sb = new StringBuilder();
		sb.append(tagging.getCreationTime());
		sb.append(tagging.getDocumentURI());
		sb.append(tagging.getTaggerURI());
		sb.append(tagging.getClipping());
		sb.append(tagging.getTerms());
		String hash = Sha1sum.getInstance().calc(sb.toString());
		String fixedServiceAddress = fixServiceAddress(serviceAddress,TAGGING);
		String uri = fixedServiceAddress + hash;
		return new URIImpl(uri);
	}
	
	/**
	 * Creates new id for a clipping to store
	 * @param clipping <tt>Clipping</tt> clipping for which to generate id
	 * @param serviceAddress <tt>String</tt> address of the service sending request
	 * @return <tt>URI</tt> string uri of the clipping
	 */
	public static URI getUri(Clipping clipping, String serviceAddress){
		StringBuilder sb = new StringBuilder();
		sb.append(clipping.getTaggingURI());
		
		if (clipping instanceof ROI) {
			ROI roi = (ROI)clipping;
			sb.append(roi.getXCoordinate());
			sb.append(roi.getYCoordinate());
			if(roi instanceof CircleROI){
				CircleROI circle = (CircleROI) roi;
				sb.append(circle.getRadius());
			}
			if(roi instanceof RectangleROI){
				RectangleROI rect = (RectangleROI)roi;
				sb.append(rect.getWidth());
				sb.append(rect.getHeight());
			}
		}else if (clipping instanceof Excerpt){
			Excerpt excerpt = (Excerpt) clipping;
			sb.append(excerpt.getStartPoint());
			sb.append(excerpt.getLength());
		}
		String hash = Sha1sum.getInstance().calc(sb.toString());
		String fixedServiceAddress = fixServiceAddress(serviceAddress, CLIPPING);
		String uri = fixedServiceAddress + hash;
		return new URIImpl(uri);
	}
	/**
	 * Creates new id for a term to store
	 * @param term <tt>Term</tt> term for which to generate id
	 * @param serviceAddress <tt>String</tt> address of the service sending request
	 * @return <tt>String</tt> string uri of the term
	 */
	public static URI getUri(Term term, String serviceAddress){
		StringBuilder sb = new StringBuilder();
		sb.append(term.getTaggingURI());
		sb.append(term.getTag());
		sb.append(term.getTermType().getTermPredicate());
		String hash = Sha1sum.getInstance().calc(sb.toString());
		String fixedServiceAddress = fixServiceAddress(serviceAddress,TERM);
		String uri = fixedServiceAddress + hash;
		return new URIImpl(uri);
	}
	
	public static URIImpl getUriForCrossReference(Tagging tagging, String crString){
		StringBuilder sb = new StringBuilder();
		sb.append(crString);
		String hash = Sha1sum.getInstance().calc(sb.toString());
		String uri = tagging.getDocumentURI() + "#" + hash;
		return new URIImpl(uri);
	}

	/**
	 * Adds postfix  to the end of the service address
	 * @param serviceAddress <tt>String</tt> address of the service sending request
	 * @param postfix <tt>String</tt> one of the postfixes to append (TAGGING, CLIPPING, TERM)
	 * @return <tt>String</tt> serviceAddress with the given postfix at the end
	 * 
	 */
	private static String fixServiceAddress(String serviceAddress, String postfix) {
		String fixedServiceAddress = "";
		if(!serviceAddress.endsWith("/")){
			fixedServiceAddress = serviceAddress.concat("/");
		}else
			fixedServiceAddress = serviceAddress;
		fixedServiceAddress = fixedServiceAddress.concat(postfix);
		System.out.println(fixedServiceAddress);
		return fixedServiceAddress;
	}
}
