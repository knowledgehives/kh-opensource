/**
 * 
 */
package org.corrib.jonto.tagging.db.access;

import org.corrib.jonto.tagging.S3B_MULTIMEDIATAGGING;

/**
 * @author Jakub Demczuk
 *
 */
public enum SparqlQuery {
	
	/**
	 * {@literal select ?uri }<br/>
		{@literal	 where { }<br/>
				{@literal ?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/Term>.}<br/>
				{@literal ?uri <http://rdfs.org/sioc/ns#related_to> <%taggingURI>} <br/>
			}}
	 */
	SELECT_TERMS_BY_TAGGING(
			"select ?uri \n"
			+ " where { \n"
			+ 	"	?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.TERM.asString() + ">. \n"
			+ 	"	?uri <" + S3B_MULTIMEDIATAGGING.RELATED_TO.asString() + "> <%s> \n"
			+ "}"), 
	/**
	 * {@literal select ?uri }<br/>
		{@literal	 where { }<br/>
				{@literal ?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/Term>.}<br/>
				{@literal ?tagging <http://rdfs.org/sioc/ns#related_to> <%documentURI>} <br/>
				{@literal ?tagging <http://rdfs.org/sioc/ns#has_creator> <%taggerURI>.} <br/>
			   			{ <br/>
			{@literal   		?tagging <http://s3b.corrib.org/tagging/hasTerm> ?uri. }<br/>
			 			} UNION { </br>
			{@literal 		?tagging <http://s3b.corrib.org/tagging/hasActionTerm> ?uri.} <br/>
			 			} UNION { <br/>
			{@literal 		?tagging <http://s3b.corrib.org/tagging/hasAgentTerm> ?uri.} <br/>
						} UNION { <br/>
			{@literal 		?tagging <http://s3b.corrib.org/tagging/hasObjectTerm> ?uri.} <br/>
			 			} UNION { <br/>
			{@literal 		?tagging <http://s3b.corrib.org/tagging/hasSettingTerm> ?uri.} <br/>
			   			} <br/>
				}, 
	 */
	SELECT_TERMS_BY_DOCUMENT_AND_TAGGER(
			"select ?uri \n"
			+ "where { \n"
			+	"	?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.TERM.asString() + ">. \n"
			+	"	?tagging <" + S3B_MULTIMEDIATAGGING.RELATED_TO.asString() + "> <%1$s>. \n"
			+	"	?tagging <" + S3B_MULTIMEDIATAGGING.HAS_CREATOR.asString() + "> <%2$s>. \n"
			+ 	"  	{ \n"
			+ 	"  		?tagging <" + S3B_MULTIMEDIATAGGING.HAS_TERM.asString() + "> ?uri. \n"
			+	"	} UNION { \n"
			+	"		?tagging <" + S3B_MULTIMEDIATAGGING.HAS_ACTION_TERM.asString() + "> ?uri. \n"
			+	"	} UNION { \n"
			+	"		?tagging <" + S3B_MULTIMEDIATAGGING.HAS_AGENT_TERM.asString() + "> ?uri. \n"
			+	"	} UNION { \n"
			+	"		?tagging <" + S3B_MULTIMEDIATAGGING.HAS_OBJECT_TERM.asString() + "> ?uri. \n"
			+	"	} UNION { \n"
			+	"		?tagging <" + S3B_MULTIMEDIATAGGING.HAS_SETTING_TERM.asString() + "> ?uri. \n"
			+ 	"  	} \n"
			+ "}"),

	/**
	 * {@literal select ?uri }<br/>
		{@literal	 where { }<br/>
				{@literal ?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/Term>.}<br/>
				{@literal ?tagging <http://rdfs.org/sioc/ns#related_to> <%documentURI>} <br/>
			   			{ <br/>
			{@literal   		?tagging <http://s3b.corrib.org/tagging/hasTerm> ?uri. }<br/>
			 			} UNION { </br>
			{@literal 		?tagging <http://s3b.corrib.org/tagging/hasActionTerm> ?uri.} <br/>
			 			} UNION { <br/>
			{@literal 		?tagging <http://s3b.corrib.org/tagging/hasAgentTerm> ?uri.} <br/>
						} UNION { <br/>
			{@literal 		?tagging <http://s3b.corrib.org/tagging/hasObjectTerm> ?uri.} <br/>
			 			} UNION { <br/>
			{@literal 		?tagging <http://s3b.corrib.org/tagging/hasSettingTerm> ?uri.} <br/>
			   			} <br/>
				}, 
	 */			
	SELECT_TERMS_BY_DOCUMENT(
			"select ?uri \n"
			+ "where {\n"
			+	"	?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.TERM.asString() + ">. \n"
			+	"	?tagging <" + S3B_MULTIMEDIATAGGING.RELATED_TO.asString() + "> <%s>. \n"
			+ 	"  	{ \n"
			+ 	"  		?tagging <" + S3B_MULTIMEDIATAGGING.HAS_TERM.asString() + "> ?uri. \n"
			+	"	} UNION { \n"
			+	"		?tagging <" + S3B_MULTIMEDIATAGGING.HAS_ACTION_TERM.asString() + "> ?uri. \n"
			+	"	} UNION { \n"
			+	"		?tagging <" + S3B_MULTIMEDIATAGGING.HAS_AGENT_TERM.asString() + "> ?uri. \n"
			+	"	} UNION { \n"
			+	"		?tagging <" + S3B_MULTIMEDIATAGGING.HAS_OBJECT_TERM.asString() + "> ?uri. \n"
			+	"	} UNION { \n"
			+	"		?tagging <" + S3B_MULTIMEDIATAGGING.HAS_SETTING_TERM.asString() + "> ?uri. \n"
			+ 	"  	} \n"
			+ "}"),
	/**
	 * {@literal select ?uri }<br/>
		{@literal	 where { }<br/>
				{@literal ?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/Term>.}<br/>
				{@literal ?tagging <http://rdfs.org/sioc/ns#has_creator> <%taggerURI>.} <br/>
			   			{ <br/>
			{@literal   		?tagging <http://s3b.corrib.org/tagging/hasTerm> ?uri. }<br/>
			 			} UNION { </br>
			{@literal 		?tagging <http://s3b.corrib.org/tagging/hasActionTerm> ?uri.} <br/>
			 			} UNION { <br/>
			{@literal 		?tagging <http://s3b.corrib.org/tagging/hasAgentTerm> ?uri.} <br/>
						} UNION { <br/>
			{@literal 		?tagging <http://s3b.corrib.org/tagging/hasObjectTerm> ?uri.} <br/>
			 			} UNION { <br/>
			{@literal 		?tagging <http://s3b.corrib.org/tagging/hasSettingTerm> ?uri.} <br/>
			   			} <br/>
				}, 
	 */
	SELECT_TERMS_BY_TAGGER(
			"select ?uri \n"
			+ "where {\n"
			+	"	?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.TERM.asString() + ">. \n"
			+	"	?tagging <" + S3B_MULTIMEDIATAGGING.HAS_CREATOR.asString() + "> <%s>. \n"
			+ 	"  	{ \n"
			+ 	"  		?tagging <" + S3B_MULTIMEDIATAGGING.HAS_TERM.asString() + "> ?uri. \n"
			+	"	} UNION { \n"
			+	"		?tagging <" + S3B_MULTIMEDIATAGGING.HAS_ACTION_TERM.asString() + "> ?uri. \n"
			+	"	} UNION { \n"
			+	"		?tagging <" + S3B_MULTIMEDIATAGGING.HAS_AGENT_TERM.asString() + "> ?uri. \n"
			+	"	} UNION { \n"
			+	"		?tagging <" + S3B_MULTIMEDIATAGGING.HAS_OBJECT_TERM.asString() + "> ?uri. \n"
			+	"	} UNION { \n"
			+	"		?tagging <" + S3B_MULTIMEDIATAGGING.HAS_SETTING_TERM.asString() + "> ?uri. \n"
			+ 	"  	} \n"
			+ "}"),
			
			
	/**
	 * {@literal select ?uri }<br/>
		{@literal	 where { }<br/>
				{@literal ?uri <http://rdfs.org/sioc/ns#related_to> <%taggingURI>} <br/>
			   			{ <br/>
			{@literal   	?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/Clipping>. }<br/>
			 			} UNION { </br>
			{@literal 		?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/CircleROI>.} <br/>
			 			} UNION { <br/>
			{@literal 		?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/RectangleROI>.} <br/>
						} UNION { <br/>
			{@literal 		?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/Excerpt>.} <br/>
			   			} <br/>
				}, 
	 */			
	SELECT_CLIPPING_BY_TAGGING(
			"select ?uri \n"
			+ " where { \n"
			+ 	"	?uri <" + S3B_MULTIMEDIATAGGING.RELATED_TO.asString() + "> <%s>. \n"
			+ 	"  	{\n " 
			+ 	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.CLIPPING.asString() + ">. \n"
			+	"  	} UNION {\n "
			+	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.CIRCLE_ROI.asString() + ">. \n"
			+ 	"  	} UNION {\n "
			+	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.RECTANGLE_ROI.asString() + ">. \n"
			+ 	"  	} UNION {\n "
			+	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.EXCERPT.asString() + ">. \n"
			+ 	"  	} \n"
			+ "}"),
	/**
	 * {@literal select ?uri }<br/>
		{@literal	 where { }<br/>
			{@literal 	?tagging <http://s3b.corrib.org/tagging/hasClipping> ?uri.}<br/>
			{@literal	?tagging <http://rdfs.org/sioc/ns#related_to> <%documentURI>. }<br/>
			{@literal 	?tagging <http://rdfs.org/sioc/ns#has_creator> <%taggerURI>.}<br/>
			   			{ <br/>
			{@literal   	?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/Clipping>. }<br/>
			 			} UNION { </br>
			{@literal 		?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/CircleROI>.} <br/>
			 			} UNION { <br/>
			{@literal 		?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/RectangleROI>.} <br/>
						} UNION { <br/>
			{@literal 		?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/Excerpt>.} <br/>
			   			} <br/>
				}, 
	 */					
	SELECT_CLIPPINGS_BY_DOCUMENT_AND_TAGGER(
			"select ?uri \n"
			+ "where { \n"
			+	"	?tagging <" + S3B_MULTIMEDIATAGGING.HAS_CLIPPING.asString() + "> ?uri. \n"
			+	"	?tagging <" + S3B_MULTIMEDIATAGGING.RELATED_TO.asString() + "> <%1$s>. \n"
			+	"	?tagging <" + S3B_MULTIMEDIATAGGING.HAS_CREATOR.asString() + "> <%2$s>. \n"
			+ 	"  	{\n " 
			+ 	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.CLIPPING.asString() + ">. \n"
			+	"  	} UNION {\n "
			+	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.CIRCLE_ROI.asString() + ">. \n"
			+ 	"  	} UNION {\n "
			+	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.RECTANGLE_ROI.asString() + ">. \n"
			+ 	"  	} UNION {\n "
			+	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.EXCERPT.asString() + ">. \n"
			+ 	"  	} \n"
			+ "}"),
	/**
	 * {@literal select ?uri }<br/>
		{@literal	 where { }<br/>
			{@literal 	?tagging <http://s3b.corrib.org/tagging/hasClipping> ?uri.}<br/>
			{@literal	?tagging <http://rdfs.org/sioc/ns#related_to> <%documentURI>. }<br/>
			   			{ <br/>
			{@literal   	?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/Clipping>. }<br/>
			 			} UNION { </br>
			{@literal 		?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/CircleROI>.} <br/>
			 			} UNION { <br/>
			{@literal 		?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/RectangleROI>.} <br/>
						} UNION { <br/>
			{@literal 		?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/Excerpt>.} <br/>
			   			} <br/>
				}, 
	 */				
	SELECT_CLIPPINGS_BY_DOCUMENT(
			"select ?uri \n"
			+ "where {\n"
			+ 	"	?tagging <" + S3B_MULTIMEDIATAGGING.HAS_CLIPPING.asString() + "> ?uri. \n"
			+	"	?tagging <" + S3B_MULTIMEDIATAGGING.RELATED_TO.asString() + "> <%s>. \n"
			+ 	"  	{\n " 
			+ 	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.CLIPPING.asString() + ">. \n"
			+	"  	} UNION {\n "
			+	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.CIRCLE_ROI.asString() + ">. \n"
			+ 	"  	} UNION {\n "
			+	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.RECTANGLE_ROI.asString() + ">. \n"
			+ 	"  	} UNION {\n "
			+	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.EXCERPT.asString() + ">. \n"
			+ 	"  	} \n"
			+ "}"),
	/**
	 * {@literal select ?uri }<br/>
		{@literal	 where { }<br/>
			{@literal 	?tagging <http://s3b.corrib.org/tagging/hasClipping> ?uri.}<br/>
			{@literal 	?tagging <http://rdfs.org/sioc/ns#has_creator> <%taggerURI>.}<br/>
			   			{ <br/>
			{@literal   	?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/Clipping>. }<br/>
			 			} UNION { </br>
			{@literal 		?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/CircleROI>.} <br/>
			 			} UNION { <br/>
			{@literal 		?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/RectangleROI>.} <br/>
						} UNION { <br/>
			{@literal 		?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/Excerpt>.} <br/>
			   			} <br/>
				}, 
	 */				
	SELECT_CLIPPINGS_BY_TAGGER(
			"select ?uri \n"
			+ "where {\n"
			+ 	"	?tagging <" + S3B_MULTIMEDIATAGGING.HAS_CLIPPING.asString() + "> ?uri. \n"
			+	"	?tagging <" + S3B_MULTIMEDIATAGGING.HAS_CREATOR.asString() + "> <%s>. \n"
			+ 	"  	{\n " 
			+ 	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.CLIPPING.asString() + ">. \n"
			+	"  	} UNION {\n "
			+	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.CIRCLE_ROI.asString() + ">. \n"
			+ 	"  	} UNION {\n "
			+	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.RECTANGLE_ROI.asString() + ">. \n"
			+ 	"  	} UNION {\n "
			+	"		?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.EXCERPT.asString() + ">. \n"
			+ 	"  	} \n"
			+ "}"), 
			
			
	/**
	 * {@literal select ?uri }<br/>
		{@literal	 where { }<br/>
			{@literal 	?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/Tagging>.}<br/>
			{@literal	<%documentURI> <http://s3b.corrib.org/tagging/hasTagging> ?uri. }<br/>
			{@literal 	?uri <http://rdfs.org/sioc/ns#related_to> <%documentURI>.}<br/>
			{@literal 	?uri <http://rdfs.org/sioc/ns#has_creator> <%taggerURI>.}<br/>
				},
	 */
	SELECT_TAGGINGS_BY_DOCUMENT_AND_TAGGER(
			"select ?uri \n"
			+ "where { \n"
			+	"	?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.TAGGING.asString() + ">. \n"
			+	"	<%1$s> <" + S3B_MULTIMEDIATAGGING.HAS_TAGGING.asString() + "> ?uri. \n"
			+	"	?uri <" + S3B_MULTIMEDIATAGGING.RELATED_TO.asString() + "> <%1$s>. \n"
			+	"	?uri <" + S3B_MULTIMEDIATAGGING.HAS_CREATOR.asString() + "> <%2$s>. \n"
			+ "}"),
	/**
	 * {@literal select ?uri }<br/>
		{@literal	 where { }<br/>
			{@literal 	?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/Tagging>.}<br/>
			{@literal	<%documentURI> <http://s3b.corrib.org/tagging/hasTagging> ?uri. }<br/>
			{@literal 	?uri <http://rdfs.org/sioc/ns#related_to> <%documentURI>.}<br/>
				},
	 */
	SELECT_TAGGINGS_BY_DOCUMENT(
			"select ?uri \n"
			+ "where {\n"
			+	"	?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.TAGGING.asString() + ">. \n"
			+ 	"	<%1$s> <" + S3B_MULTIMEDIATAGGING.HAS_TAGGING.asString() + "> ?uri. \n"
			+	"	?uri <" + S3B_MULTIMEDIATAGGING.RELATED_TO.asString() + "> <%1$s>. \n"
			+ "}"),
	/**
	 * {@literal select ?uri }<br/>
		{@literal	 where { }<br/>
			{@literal 	?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://s3b.corrib.org/tagging/Tagging>.}<br/>
			{@literal	<%documentURI> <http://s3b.corrib.org/tagging/hasTagging> ?uri. }<br/>
			{@literal 	?uri <http://rdfs.org/sioc/ns#has_creator> <%taggerURI>.}<br/>
				},
	 */
	SELECT_TAGGINGS_BY_TAGGER(
			"select ?uri \n"
			+ "where {\n"
			+	"	?uri <" + S3B_MULTIMEDIATAGGING.TYPE.asString() + "> <" + S3B_MULTIMEDIATAGGING.TAGGING.asString() + ">. \n"
			+	"	?uri <" + S3B_MULTIMEDIATAGGING.HAS_CREATOR.asString() + "> <%s>. \n"
			+ "}"),

    DESCRIBE_TAGGING("DESCRIBE ?t WHERE { ?t <"+S3B_MULTIMEDIATAGGING.RELATED_TO.asString()+"> <%s>. }");



	private String query;
	SparqlQuery(String _query){
		query = _query;
	}

	/**
	 * Returns query string without setting arguments of the query if there are any
	 * @return <tt>String</tt> query string
	 */
	public String getQuery(){
		return query;
	}

	public String getFormatedQuery(String... params){
		return String.format(query, (Object[])params);
	}
}
