/**
 * 
 */
package org.corrib.jonto.tagging.db.access;

import org.corrib.jonto.tagging.S3B_MULTIMEDIATAGGING;
import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Excerpt;
import org.corrib.jonto.tagging.db.StatementFinder;
import org.ontoware.rdf2go.model.Model;
import org.ontoware.rdf2go.model.Statement;
import org.ontoware.rdf2go.model.node.Resource;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.vocabulary.XSD;

/**
 * @author Jakub Demczuk
 *
 */
public class ExcerptDBAccess extends ClippingDbAccess {

	public ExcerptDBAccess(){
		//default empty constructor
	}

	/**
	 * Method loads startPoint and length properties of Excerpt objects
	 * @see org.corrib.jonto.tagging.db.access.ClippingDbAccess#fillClipping(org.ontoware.rdf2go.model.Model, org.corrib.jonto.tagging.beans.Clipping)
	 */
	@Override
	protected void performGet(Model model,Clipping clipping) {
		Excerpt excerpt = (Excerpt)clipping;
		
		Resource subject = clipping.getURI();
		URI predicate = S3B_MULTIMEDIATAGGING.HAS_EXCERPT_START.asURI();
		
		Statement statement = StatementFinder.findStatement(model, subject, predicate, null);
		if(statement != null){
			excerpt.setStartPoint(Long.parseLong(statement.getObject().asDatatypeLiteral().getValue()));
		}
		
		predicate = S3B_MULTIMEDIATAGGING.HAS_EXCERPT_LENGTH.asURI();
		
		statement = StatementFinder.findStatement(model, subject, predicate, null);
		if(statement != null){
			excerpt.setLength(Long.parseLong(statement.getObject().asDatatypeLiteral().getValue()));
		}
	}
	
	/**
	 * Method stores excerptStart and excerptLength properties of Excerpts
	 * @see org.corrib.jonto.tagging.db.access.ClippingDbAccess#storeClipping(org.ontoware.rdf2go.model.Model, org.corrib.jonto.tagging.beans.Clipping)
	 */
	@Override
	protected void performAdd(Model model, Clipping clipping) {
		Excerpt excerpt = (Excerpt)clipping;
		URI clippingURI = excerpt.getURI();
		//add type property
		URI predicate = S3B_MULTIMEDIATAGGING.TYPE.asURI();
		model.addStatement(clippingURI,predicate,S3B_MULTIMEDIATAGGING.EXCERPT.asURI());
		String value;
		
		//start point
		predicate = S3B_MULTIMEDIATAGGING.HAS_EXCERPT_START.asURI();
		value = Long.toString(excerpt.getStartPoint());
		model.addStatement(clippingURI,predicate,model.createDatatypeLiteral(value, XSD._long));
		//length
		predicate = S3B_MULTIMEDIATAGGING.HAS_EXCERPT_LENGTH.asURI();
		value = Long.toString(excerpt.getLength());
		model.addStatement(clippingURI,predicate,model.createDatatypeLiteral(value, XSD._long));
	}
}
