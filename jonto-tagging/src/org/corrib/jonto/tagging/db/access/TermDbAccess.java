package org.corrib.jonto.tagging.db.access;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.S3B_MULTIMEDIATAGGING;
import org.corrib.jonto.tagging.beans.Tagging;
import org.corrib.jonto.tagging.beans.Term;
import org.corrib.jonto.tagging.beans.TermType;
import org.corrib.jonto.tagging.db.StatementFinder;
import org.corrib.jonto.tagging.db.exceptions.IllegalTypeException;
import org.corrib.jonto.tagging.db.exceptions.ResourceNotFoundException;
import org.ontoware.aifbcommons.collection.ClosableIterator;
import org.ontoware.rdf2go.model.Model;
import org.ontoware.rdf2go.model.QueryRow;
import org.ontoware.rdf2go.model.Statement;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;
import org.ontoware.rdf2go.vocabulary.XSD;


/**
 * Singleton class used to perform db operations on Term objects
 * @author Jakub Demczuk
 *
 */
public class TermDbAccess {
	
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.db.TermDbAccess");
	
	public TermDbAccess(){
		//default empty constructor
	}
	/**
	 * Loads an existing Terms for the given termURI
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param termURI <tt>URI</tt> uri of the term to load
	 * @return <tt>Term</tt> loaded instance from the database
	 * @throws ResourceNotFoundException when term with the given uri does not exists in the store
	 * @throws IllegalTypeException when an uri that is not a term type have been given
	 */
	public Term getTerm(Model model, URI termURI) throws ResourceNotFoundException, IllegalTypeException{
		Term term = new Term(termURI);
//		ClosableIterator<Statement> statements = model.findStatements(termURI,null,null);
//		if(!statements.hasNext())
//			throw new ResourceNotFoundException("Term not found: " + termURI);
//		while (statements.hasNext()) {
//			Statement statement = statements.next();
//			//check if type of this resource is ok
//			if (S3B_MULTIMEDIATAGGING.type.equals(statement.getPredicate().asURI().toString())){
//				if(!S3B_MULTIMEDIATAGGING.Term.equals(statement.getObject().asURI().toString())){
//					throw new IllegalTypeException("Given URI is not a term : " + termURI);
//				}
//				//label found
//			}else if(S3B_MULTIMEDIATAGGING.prefLabel.equals(statement.getPredicate().asURI().toString())){
//				term.setTag(statement.getObject().asLiteral().toString());
//			//info about tagging this term relates to
//			}else if (S3B_MULTIMEDIATAGGING.related_to.equals(statement.getPredicate().asURI().toString())){
//				URI taggingURI = statement.getObject().asURI();
//				term.setTaggingURI(taggingURI);
//				//check what type that term exactly is
//				ClosableIterator<Statement> termTypeStatements = model.findStatements(taggingURI, null, termURI);
//				while(termTypeStatements.hasNext()){
//					Statement typeStatement = termTypeStatements.next();
//					if(S3B_MULTIMEDIATAGGING.hasTerm.equals(typeStatement.getPredicate().toString())){
//						term.setTermType(TermType.SIMPLE_TERM);
//					}else if(S3B_MULTIMEDIATAGGING.hasActionTerm.equals(typeStatement.getPredicate().toString())){
//						term.setTermType(TermType.ACTION_TERM);
//					}else if(S3B_MULTIMEDIATAGGING.hasAgentTerm.equals(typeStatement.getPredicate().toString())){
//						term.setTermType(TermType.AGENT_TERM);
//					}else if(S3B_MULTIMEDIATAGGING.hasObjectTerm.equals(typeStatement.getPredicate().toString())){
//						term.setTermType(TermType.OBJECT_TERM);
//					}else if(S3B_MULTIMEDIATAGGING.hasSettingTerm.equals(typeStatement.getPredicate().toString())){
//						term.setTermType(TermType.SETTING_TERM);
//					}
//				}
//				termTypeStatements.close();
//			}
//		}
//		statements.close();
		
		//type
		URI predicate = S3B_MULTIMEDIATAGGING.TYPE.asURI();
		Statement statement = StatementFinder.findStatement(model, termURI, predicate, null);
		if(statement != null){
			if(!S3B_MULTIMEDIATAGGING.TERM.asURI().equals(statement.getObject().asURI())){
				throw new IllegalTypeException("Given URI is not a term : " + termURI);
			}
		}else{
			throw new ResourceNotFoundException("Term not found: " + termURI);
		}
		
		//prefLabel
		predicate = S3B_MULTIMEDIATAGGING.PREF_LABEL.asURI();
		statement = StatementFinder.findStatement(model, termURI, predicate, null);
		if(statement != null){
			term.setTag(statement.getObject().asDatatypeLiteral().getValue());
		}
		
		//relatedTo and termType
		predicate = S3B_MULTIMEDIATAGGING.RELATED_TO.asURI();
		statement = StatementFinder.findStatement(model, termURI, predicate, null);
		if(statement != null){
			URI taggingURI = statement.getObject().asURI();
			term.setTaggingURI(taggingURI);
			
			statement = StatementFinder.findStatement(model, taggingURI, null, termURI);
			if(statement != null){
				if(S3B_MULTIMEDIATAGGING.HAS_TERM.asURI().equals(statement.getPredicate().asURI())){
					term.setTermType(TermType.SIMPLE_TERM);
				}else if(S3B_MULTIMEDIATAGGING.HAS_ACTION_TERM.asURI().equals(statement.getPredicate().asURI())){
					term.setTermType(TermType.ACTION_TERM);
				}else if(S3B_MULTIMEDIATAGGING.HAS_AGENT_TERM.asURI().equals(statement.getPredicate().asURI())){
					term.setTermType(TermType.AGENT_TERM);
				}else if(S3B_MULTIMEDIATAGGING.HAS_OBJECT_TERM.asURI().equals(statement.getPredicate().asURI())){
					term.setTermType(TermType.OBJECT_TERM);
				}else if(S3B_MULTIMEDIATAGGING.HAS_SETTING_TERM.asURI().equals(statement.getPredicate().asURI())){
					term.setTermType(TermType.SETTING_TERM);
				}
			}
		}
		return term;
	}
	
	/**
	 * Loads all existing Terms for the given tagging from the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param taggingURI <tt>URI</tt> uri of the tagging for which to get terms
	 * @return <tt>List{@literal<Term>}</tt> list of terms for the given tagging
	 */
	public List<Term> getTermsForTagging(Model model, URI taggingURI){
		List<Term> terms = new ArrayList<Term>();
		String query = SparqlQuery.SELECT_TERMS_BY_TAGGING.getFormatedQuery(taggingURI.toString());
		logger.info("" + query);
		terms.addAll(loadTermsForQuery(model, query));
		return terms;
	}
	
	/**
	 * Loads all existing terms for the given  document and tagger from the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param documentURI <tt>URI</tt> uri of the document for which to load terms
	 * @param taggerURI <tt>URI</tt> uri of the tagger for whom to load terms
	 * @return <tt>List{@literal<Term>}</tt> list of terms for the given document created by the given tagger
	 */
	public List<Term> getTermsForDocumentTagger(Model model, URI documentURI, URI taggerURI){
		List<Term> terms = new ArrayList<Term>();
		String query = SparqlQuery.SELECT_TERMS_BY_DOCUMENT_AND_TAGGER.getFormatedQuery(documentURI.toString(),taggerURI.toString());
		logger.info("" + query);
		terms.addAll(loadTermsForQuery(model,query));
		return terms;
	}
	
	/**
	 * Loads all existing terms for the given document from the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param documentURI <tt>URI</tt> uri of the document for which to load terms
	 * @return <tt>List{@literal<Term>}</tt> list of terms for the given document 
	 */
	public List<Term> getTermsForDocument(Model model, URI documentURI){
		List<Term> terms = new ArrayList<Term>();
		String query = SparqlQuery.SELECT_TERMS_BY_DOCUMENT.getFormatedQuery(documentURI.toString());
		logger.info("" + query);
		terms.addAll(loadTermsForQuery(model, query));
		return terms;
	}
	
	/**
	 * Loads all existing terms for the given tagger from the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param taggerURI <tt>URI</tt> uri of the tagger for whom to load terms
	 * @return <tt>List{@literal<Term>}</tt> list of terms for the given document 
	 */
	public List<Term> getTermsForTagger(Model model, URI taggerURI){
		List<Term> terms = new ArrayList<Term>();
		String query = SparqlQuery.SELECT_TERMS_BY_TAGGER.getFormatedQuery(taggerURI.toString());
		logger.info("" + query);
		terms.addAll(loadTermsForQuery(model, query));
		return terms;
	}
	
	/**
	 * Adds new Term to the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param term <tt>Term</tt> instance of Term to store
	 * @return <tt>URI</tt> uri of the newly created Term
	 */
	public URI addTerm(Model model, Term term){
		URI termURI = term.getURI();
		URI taggingURI = term.getTaggingURI();
		//add type property
		URI predicate = S3B_MULTIMEDIATAGGING.TYPE.asURI();
		model.addStatement(termURI,predicate,S3B_MULTIMEDIATAGGING.TERM.asURI());
		
		//add tagging this term relates to
		predicate = S3B_MULTIMEDIATAGGING.RELATED_TO.asURI();
		model.addStatement(termURI,predicate,taggingURI);
		
		//add info that tagging has a term of specific type
		predicate = term.getTermType().getTermPredicate();
		model.addStatement(taggingURI,predicate,termURI);
		
		//add tag(label) property
		predicate = S3B_MULTIMEDIATAGGING.PREF_LABEL.asURI();
		model.addStatement(termURI,predicate,model.createDatatypeLiteral(term.getTag(), XSD._string));
		
		return term.getURI();
	}
	
	/**
	 * Updates Term properties in the database. Only type and label of term are updated
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param term <tt>Term</tt> instance of Term to update
	 * @return <tt>URI</tt> uri of the updated term
	 * @throws ResourceNotFoundException when term with the given uri does not exists in the store
	 * @throws IllegalTypeException when an uri that is not a term type have been given
	 */
	public URI updateTerm(Model model, Term term) throws ResourceNotFoundException, IllegalTypeException{
		//check if this term exists
		URI predicate = S3B_MULTIMEDIATAGGING.TYPE.asURI();
		Statement statement = StatementFinder.findStatement(model, term.getURI(), predicate, null);
		if(statement != null){
			if(!S3B_MULTIMEDIATAGGING.TERM.asURI().equals(statement.getObject().asURI())){
				throw new IllegalTypeException("Given URI is not a term : " + term.getURI());
			}
		}else{
			throw new ResourceNotFoundException("Term not found: " + term.getURI());
		}
		//delete term
		deleteTerm(model, term);
		//and add it again
		addTerm(model, term);
		return term.getURI();
	}
	
	/**
	 * Updates terms for the given tagging
	 * @param model <tt>Model</tt> model to use for performing operations 
	 * @param tagging <tt>Tagging</tt> tagging which terms to update
	 * @throws ResourceNotFoundException when term with the given uri does not exists in the store
	 * @throws IllegalTypeException when an uri that is not a term type have been given
	 */
	public void updateTermsForTagging(Model model, Tagging tagging) throws ResourceNotFoundException, IllegalTypeException {
		List<Term> termsFromStorage = getTermsForTagging(model, tagging.getURI());
		for(Term term : tagging.getTerms()){
			boolean newTerm = true;
			for(Term termFromStorage : termsFromStorage){
					//if term exists in database then there's a matching and it is not a new Term
					if(term.getURI() != null && term.getURI().equals(termFromStorage.getURI())){
						newTerm = false;
						//if prefLabel has changed update it
						if(!term.getTag().equals(termFromStorage.getTag())){
							updateTerm(model, term);
						}
						break;
					}
			}
			//if no existing term with the same uri was found then that's a new term
			if(newTerm){
				addTerm(model, term);
			}
		}
		//delete terms that were stored but weren't given in the update list
		for(Term termFromStorage : termsFromStorage){
			boolean doDelete = true;
			for(Term updatedTerm : tagging.getTerms()){
				//if there is a matching for a stored term and the updated one
				if(termFromStorage.getURI().equals(updatedTerm.getURI())){
					doDelete = false;
					break;
				}
			}
			//if no matching was found, then this Term should be removed
			if(doDelete){
				deleteTerm(model, termFromStorage);
			}
		}
	}
	
	/**
	 * Deletes existing Term from the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param term <tt>Term</tt> instance of Term to delete
	 */
	public void deleteTerm(Model model, Term term){
		//remove all statements with this term as subject
		model.removeStatements(term.getURI(), null, null);
		
		//also remove all statements with this term as object
		// hasTerm, hasActionTerm, hasAgentTerm etc.
		model.removeStatements(null,null,term.getURI());
	}
	
	/**
	 * Deletes all existing terms for the given tagging
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param taggingURI <tt>URI</tt> instance of Term to delete
	 */
	public void deleteTermsForTagging(Model model, URI taggingURI){
		
		List<Term> terms = getTermsForTagging(model, taggingURI);
		for(Term term : terms){
			deleteTerm(model, term);
		}
	}
	
	
	/**
	 * Performs select query and loads terms based on the results from the query
	 * @param model <tt>Model</tt> model to use for performing query
	 * @param sparqlQuery <tt>String</tt> sparql query to use
	 * @return <tt>List{@literal<Term>}</tt> list of terms loaded from the storage
	 */
	private List<Term> loadTermsForQuery(Model model, String sparqlQuery) {
		List<Term> terms = new ArrayList<Term>();
		ClosableIterator<QueryRow> rows = model.sparqlSelect(sparqlQuery).iterator();
		while(rows.hasNext()){
			QueryRow row = rows.next();
			String sURI = row.getValue("uri").toString();
			URI termURI = new URIImpl(sURI);
			try {
				terms.add(getTerm(model, termURI));
			} catch (ResourceNotFoundException e) {
				logger.severe(e.getMessage());
				throw new RuntimeException("SHOULD NOT HAPPEN !! TermUri from the queryResult does not exist in the storage !! MESS");
			} catch (IllegalTypeException e) {
				logger.severe(e.getMessage());
				throw new RuntimeException("SHOULD NOT HAPPEN !! TermUri from the queryResult is not a term in the storage !! MESS");
			}
		}
		rows.close();
		return terms;
	}
}
