/**
 * 
 */
package org.corrib.jonto.tagging.db.access;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.S3B_MULTIMEDIATAGGING;
import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Tagging;
import org.corrib.jonto.tagging.beans.factories.ClippingFactory;
import org.corrib.jonto.tagging.db.StatementFinder;
import org.corrib.jonto.tagging.db.exceptions.IllegalTypeException;
import org.corrib.jonto.tagging.db.exceptions.ResourceNotFoundException;
import org.corrib.jonto.tagging.db.factories.ClippingDBAccessFactory;
import org.ontoware.aifbcommons.collection.ClosableIterator;
import org.ontoware.rdf2go.model.Model;
import org.ontoware.rdf2go.model.QueryRow;
import org.ontoware.rdf2go.model.Statement;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;


/**
 * General class for accessing Clipping properties
 * @author Jakub Demczuk
 *
 */
public class ClippingDbAccess{
	
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.db.access.ClippingDbAccess");
	
	public ClippingDbAccess(){
		//default empty constructor
	}
	
	/**
	 * Loads an existing clipping from the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param clippingURI <tt>URI</tt> uri of the clipping to load
	 * @return <tt>Clipping</tt> loaded instance from the database
	 * @throws ResourceNotFoundException when clipping with the given uri does not exists in the store 
	 * @throws IllegalTypeException when an uri that is not a clipping type have been given
	 */
	public Clipping getClipping(Model model, URI clippingURI) throws ResourceNotFoundException, IllegalTypeException{
		Statement statement = StatementFinder.findStatement(model, clippingURI, S3B_MULTIMEDIATAGGING.TYPE.asURI(), null);
		if(statement != null){
			if(!checkValidType(statement.getObject().asURI())){
				throw new IllegalTypeException("Given URI is not a term : " + clippingURI);
			}
		}else{
			throw new ResourceNotFoundException("Clipping not found: " + clippingURI);
		}
		Clipping clipping = null;
		URI type = statement.getObject().asURI();
		clipping = ClippingFactory.getClipping(clippingURI, type);
		//fill clipping with the top level properties
		performGet(model, clipping);
		
		//then get proper DBAccess for the clipping type and fill clipping with more specific properties
		ClippingDBAccessFactory.getClippingDBAccess(clipping).performGet(model,clipping);
		return clipping;
	}
	
	private boolean checkValidType(URI typeURI) {
		if(S3B_MULTIMEDIATAGGING.CIRCLE_ROI.asURI().equals(typeURI) ||
				S3B_MULTIMEDIATAGGING.RECTANGLE_ROI.asURI().equals(typeURI) ||
				S3B_MULTIMEDIATAGGING.EXCERPT.asURI().equals(typeURI) ||
				S3B_MULTIMEDIATAGGING.CLIPPING.asURI().equals(typeURI) ||
				S3B_MULTIMEDIATAGGING.ROI.asURI().equals(typeURI))
			return true;
		return false;
	}

	/**
	 * Method sets the tagging uri for the clipping property of the instance of this class
	 * from the value found in the storage
	 * @param model <tt>Model</tt> model in which to look for property
	 */
	protected void performGet(Model model,Clipping clipping){
		Statement statement = StatementFinder.findStatement(model,clipping.getURI(), S3B_MULTIMEDIATAGGING.RELATED_TO.asURI(), null);
		if(statement != null){
			clipping.setTaggingURI(statement.getObject().asURI());
		}
	}
	
	/**
	 * Loads clipping for the given tagging from the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param taggingURI <tt>URI</tt> uri of the tagging for which to get clippings
	 * @return <tt>Clipping</tt> clipping for the given tagging or <tt>null</tt> if this tagging has no clipping
	 */
	public Clipping getClippingForTagging(Model model, URI taggingURI){
		List<Clipping> clippings = new ArrayList<Clipping>();
		String query = SparqlQuery.SELECT_CLIPPING_BY_TAGGING.getFormatedQuery(taggingURI.toString());
		logger.info("" + query);
		clippings.addAll(loadClippingsForQuery(model, query));
		if(clippings.size() == 1)
			return clippings.get(0);
		return null;
	}
	
	/**
	 * Loads all clippings for the given document and given tagger from database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param documentURI <tt>URI</tt> uri of the document for which to load clippings
	 * @param taggerURI <tt>URI</tt> uri of the tagger for whom to load clippings
	 * @return <tt>List{@literal<Clipping>}</tt> list of all clippings for the given document
	 */
	public List<Clipping> getClippingsForDocumentTagger(Model model, URI documentURI, URI taggerURI){
		List<Clipping> clippings = new ArrayList<Clipping>();
		String query = SparqlQuery.SELECT_CLIPPINGS_BY_DOCUMENT_AND_TAGGER.getFormatedQuery(documentURI.toString(),taggerURI.toString());
		logger.info("" + query);
		clippings.addAll(loadClippingsForQuery(model, query));
		return clippings;
	}
	
	/**
	 * Loads all clippings for the given document from database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param documentURI <tt>URI</tt> uri of the document for which to load clippings
	 * @return <tt>List{@literal<Clipping>}</tt> list of all clippings for the given document
	 */
	public List<Clipping> getClippingsForDocument(Model model, URI documentURI){
		List<Clipping> clippings = new ArrayList<Clipping>();
		String query = SparqlQuery.SELECT_CLIPPINGS_BY_DOCUMENT.getFormatedQuery(documentURI.toString());
		logger.info("" + query);
		clippings.addAll(loadClippingsForQuery(model, query));
		return clippings;
	}
	/**
	 * Loads all clippings for the given tagger from database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param taggerURI <tt>URI</tt> uri of the tagger for whom to load clippings
	 * @return <tt>List{@literal<Clipping>}</tt> list of all clippings for the given document
	 */
	public List<Clipping> getClippingsForTagger(Model model, URI taggerURI){
		List<Clipping> clippings = new ArrayList<Clipping>();
		String query = SparqlQuery.SELECT_CLIPPINGS_BY_TAGGER.getFormatedQuery(taggerURI.toString());
		logger.info("" + query);
		clippings.addAll(loadClippingsForQuery(model, query));
		return clippings;
	}
	
	
	/**
	 * Adds new clipping to the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param clipping <tt>Clipping</tt> instance of clipping to store
	 * @return <tt>URI</tt> uri of the newly created clipping
	 */
	public URI addClipping(Model model, Clipping clipping){
		//add general clipping properties
		performAdd(model, clipping);
		
		//then get proper DBAccess from factory and add more specific properties
		ClippingDBAccessFactory.getClippingDBAccess(clipping).performAdd(model, clipping);
		return clipping.getURI();
	}

	/**
	 * Stores clipping property: related_to and sets the hasClipping property for its tagging
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param clipping <tt>Clipping</tt> instance of clipping to store
	 * @return
	 */
	protected void performAdd(Model model, Clipping clipping) {
		URI clippingURI = clipping.getURI();
		URI taggingURI = clipping.getTaggingURI();
		URI predicate;
		
		
		//add tagging this clipping relates to
		predicate = S3B_MULTIMEDIATAGGING.RELATED_TO.asURI();
		model.addStatement(clippingURI,predicate,taggingURI);
		
		//add this clipping as a tagging property
		predicate = S3B_MULTIMEDIATAGGING.HAS_CLIPPING.asURI();
		model.addStatement(taggingURI,predicate,clippingURI);
	}
	
	/**
	 * Updates clipping properties in the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param clipping <tt>Clipping</tt> instance of clipping to update
	 * @return <tt>URI</tt> uri of the newly created clipping
	 * @throws ResourceNotFoundException when clipping with the given uri does not exists in the store 
	 * @throws IllegalTypeException when an uri that is not a clipping type have been given
	 */
	public URI updateClipping(Model model, Clipping clipping) throws ResourceNotFoundException, IllegalTypeException {
		//check if clipping exists
		Statement statement = StatementFinder.findStatement(model, clipping.getURI(), S3B_MULTIMEDIATAGGING.TYPE.asURI(), null);
		if(statement != null){
			if(!checkValidType(statement.getObject().asURI())){
				throw new IllegalTypeException("Given URI is not a term : " + clipping.getURI());
			}
		}else{
			throw new ResourceNotFoundException("Clipping not found: " + clipping.getURI());
		}
		//delete all info about the clipping
		deleteClipping(model, clipping);
		//add it once again
		addClipping(model, clipping);
		return clipping.getURI();
	}
	
	/**
	 * Updates clipping for the given tagging
	 * @param model
	 * @param tagging
	 * @throws ResourceNotFoundException when clipping with the given uri does not exists in the store 
	 * @throws IllegalTypeException when an uri that is not a clipping type have been given
	 */
	public void updateClippingForTagging(Model model, Tagging tagging) throws ResourceNotFoundException, IllegalTypeException {
		Clipping clipping = getClippingForTagging(model, tagging.getURI());
		if(tagging.getClipping() != null){
			//if new clipping was given then remove old clipping (if it exists) and store the new one
			if(tagging.getClipping().getURI() == null){
				if(clipping != null)
					deleteClipping(model, clipping);
				addClipping(model, tagging.getClipping());
			//if clipping already exists for this tagging and has the same uri as the clipping in tagging object
			//then update
			}else if(clipping != null && tagging.getClipping().getURI().equals(clipping.getURI())){
				updateClipping(model, tagging.getClipping());
			}
		//no clipping given then remove the old one (if it exists)
		}else{
			if(clipping != null){
				deleteClipping(model, clipping);
			}
		}
	}

	
	/**
	 * Deletes existing clipping from the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param clipping <tt>Clipping</tt> instance of clipping to delete
	 */
	public void deleteClipping(Model model, Clipping clipping){
		//remove all statements with this clipping as subject
		model.removeStatements(clipping.getURI(), null, null);
		
		//also remove all statements with this clipping as object
		//that is all hasClipping
		model.removeStatements(null,null,clipping.getURI());
	}
	
	public void deleteClippingForTagging(Model model, URI taggingURI){
		Clipping clipping = getClippingForTagging(model, taggingURI);
		if(clipping != null)
			deleteClipping(model, clipping);
	}
	
	/**
	 * Performs select query and loads clippings based on the results from the query
	 * @param model <tt>Model</tt> model to use for performing query
	 * @param sparqlQuery <tt>String</tt> sparql query to use
	 * @return <tt>List{@literal<Clipping>}</tt> list of clippings loaded from the storage
	 */
	private List<Clipping> loadClippingsForQuery(Model model, String sparqlQuery) {
		List<Clipping> clippings = new ArrayList<Clipping>();
		ClosableIterator<QueryRow> rows = model.sparqlSelect(sparqlQuery).iterator();
		while(rows.hasNext()){
			QueryRow row = rows.next();
			String sURI = row.getValue("uri").toString();
			URI clippingURI = new URIImpl(sURI);
			try {
				clippings.add(getClipping(model, clippingURI));
			} catch (ResourceNotFoundException e) {
				logger.severe(e.getMessage());
				throw new RuntimeException("SHOULD NOT HAPPEN !! ClippingUri from the queryResult does not exist in the storage !! MESS");
			} catch (IllegalTypeException e) {
				logger.severe(e.getMessage());
				throw new RuntimeException("SHOULD NOT HAPPEN !! ClippingUri from the queryResult is not a clipping in the storage !! MESS");
			}
		}
		rows.close();
		return clippings;
	}
}
