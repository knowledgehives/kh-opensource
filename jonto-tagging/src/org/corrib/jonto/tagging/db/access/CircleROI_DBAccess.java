/**
 * 
 */
package org.corrib.jonto.tagging.db.access;

import org.corrib.jonto.tagging.S3B_MULTIMEDIATAGGING;
import org.corrib.jonto.tagging.beans.CircleROI;
import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.db.StatementFinder;
import org.ontoware.rdf2go.model.Model;
import org.ontoware.rdf2go.model.Statement;
import org.ontoware.rdf2go.model.node.Resource;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.vocabulary.XSD;

/**
 * Class for accessing CircleROI specific properties
 * @author Jakub Demczuk
 *
 */
public class CircleROI_DBAccess extends ROI_DBAccess {

	public CircleROI_DBAccess() {
		//default empty constructor
	}

	/**
	 * Method loads radius property of CircleROI objects
	 * @see org.corrib.jonto.tagging.db.access.ROI_DBAccess#fillClipping(org.ontoware.rdf2go.model.Model, org.corrib.jonto.tagging.beans.Clipping)
	 */
	@Override
	protected void performGet(Model model,Clipping clipping) {
		CircleROI circle = (CircleROI)clipping;
		
		Resource subject = clipping.getURI();
		URI predicate = S3B_MULTIMEDIATAGGING.HAS_RADIUS.asURI();
		
		Statement statement = StatementFinder.findStatement(model, subject, predicate, null);
		if(statement != null){
			circle.setRadius(Integer.parseInt(statement.getObject().asDatatypeLiteral().getValue()));
		}
	}
	
	
	/**
	 * Method stores radius property of CircleROIs
	 * @see org.corrib.jonto.tagging.db.access.ROI_DBAccess#storeClipping(org.ontoware.rdf2go.model.Model, org.corrib.jonto.tagging.beans.Clipping)
	 */
	@Override
	protected void performAdd(Model model, Clipping clipping) {
		CircleROI circle = (CircleROI)clipping;
		URI clippingURI = circle.getURI();
		URI predicate = S3B_MULTIMEDIATAGGING.TYPE.asURI();
		model.addStatement(clippingURI,predicate,S3B_MULTIMEDIATAGGING.CIRCLE_ROI.asURI());
		//radius
		predicate = S3B_MULTIMEDIATAGGING.HAS_RADIUS.asURI();
		String value = Integer.toString(circle.getRadius());
		model.addStatement(clippingURI,predicate,model.createDatatypeLiteral(value, XSD._integer));
	}
}
