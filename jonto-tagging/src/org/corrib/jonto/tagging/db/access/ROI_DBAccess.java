/**
 * 
 */
package org.corrib.jonto.tagging.db.access;

import org.corrib.jonto.tagging.S3B_MULTIMEDIATAGGING;
import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.ROI;
import org.corrib.jonto.tagging.db.StatementFinder;
import org.corrib.jonto.tagging.db.factories.ROI_DBAccessFactory;
import org.ontoware.rdf2go.model.Model;
import org.ontoware.rdf2go.model.Statement;
import org.ontoware.rdf2go.model.node.Resource;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.vocabulary.XSD;

/**
 * Class for accessing ROI general properties
 * @author Jakub Demczuk
 *
 */
public class ROI_DBAccess extends ClippingDbAccess {
	
	
	public ROI_DBAccess() {
		//default empty constructor
	}
	/**
	 *  Method loads xCoordinate and yCoordinate properties of the ROI type clippings
	 * @see org.corrib.jonto.tagging.db.access.ClippingDbAccess#fillClipping(org.ontoware.rdf2go.model.Model, org.corrib.jonto.tagging.beans.Clipping)
	 */
	@Override
	protected void performGet(Model model,Clipping clipping) {
		ROI roi = (ROI)clipping;
		Resource subject = clipping.getURI();
		URI predicate = S3B_MULTIMEDIATAGGING.HAS_X_COORDINATE.asURI();
		
		//fill xCoordinate
		Statement statement = StatementFinder.findStatement(model, subject, predicate, null);
		if(statement != null){
			roi.setXCoordinate(Integer.parseInt(statement.getObject().asDatatypeLiteral().getValue()));
		}
		
		//fill yCoordinate 
		predicate = S3B_MULTIMEDIATAGGING.HAS_Y_COORDINATE.asURI();
		statement = StatementFinder.findStatement(model, subject, predicate, null);
		if(statement != null){
			roi.setYCoordinate(Integer.parseInt(statement.getObject().asDatatypeLiteral().getValue()));
		}
		
		//get proper DBAccess for the roi type and fill roi with more specific properties
		ROI_DBAccessFactory.getROI_DBAccess(roi).performGet(model, clipping);
	}
	/**
	 * Method stores xCoordinate and yCoordinate properties of the ROIs
	 * @see org.corrib.jonto.tagging.db.access.ClippingDbAccess#storeClipping(org.ontoware.rdf2go.model.Model, org.corrib.jonto.tagging.beans.Clipping)
	 */
	@Override
	protected void performAdd(Model model, Clipping clipping){
		ROI roi = (ROI)clipping;
		URI clippingURI = roi.getURI();
		//add x,y coordinates
		URI predicate = S3B_MULTIMEDIATAGGING.HAS_X_COORDINATE.asURI();
		String value = Integer.toString(roi.getXCoordinate());
		model.addStatement(clippingURI,predicate,model.createDatatypeLiteral(value, XSD._integer));
		
		predicate = S3B_MULTIMEDIATAGGING.HAS_Y_COORDINATE.asURI();
		value = Integer.toString(roi.getYCoordinate());
		model.addStatement(clippingURI,predicate,model.createDatatypeLiteral(value, XSD._integer));
		
		//get proper DBAccess for the roi type and store more specific properties
		ROI_DBAccessFactory.getROI_DBAccess(roi).performAdd(model, clipping);
	}
}
