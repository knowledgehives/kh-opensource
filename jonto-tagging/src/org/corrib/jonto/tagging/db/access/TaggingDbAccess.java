package org.corrib.jonto.tagging.db.access;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.GlobalTools;
import org.corrib.jonto.tagging.S3B_MULTIMEDIATAGGING;
import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Tagging;
import org.corrib.jonto.tagging.beans.Term;
import org.corrib.jonto.tagging.db.StatementFinder;
import org.corrib.jonto.tagging.db.exceptions.IllegalTypeException;
import org.corrib.jonto.tagging.db.exceptions.ResourceNotFoundException;
import org.ontoware.aifbcommons.collection.ClosableIterable;
import org.ontoware.aifbcommons.collection.ClosableIterator;
import org.ontoware.rdf2go.impl.jena24.ModelFactoryImpl;
import org.ontoware.rdf2go.model.Model;
import org.ontoware.rdf2go.model.ModelIO;
import org.ontoware.rdf2go.model.QueryRow;
import org.ontoware.rdf2go.model.Statement;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;
import org.ontoware.rdf2go.vocabulary.XSD;


/**
 * Class used to perform db operations on Tagging objects
 * @author Jakub Demczuk
 *
 */
public class TaggingDbAccess {
	
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.db.access.TaggingDbAccess");
	private DateFormat sdf = GlobalTools.DATE_TIME_FORMAT;
	
	public TaggingDbAccess(){
		//default empty constructor
	}
	
	/**
	 * Loads an existing clipping from the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param taggingURI <tt>URI</tt> uri of the clipping to load
	 * @return <tt>Tagging</tt> loaded instance from the database
	 * @throws ResourceNotFoundException when tagging with the given uri does not exists in the store 
	 * @throws IllegalTypeException when an uri that is not a tagging type have been given
	 */
	public Tagging getTagging(Model model, URI taggingURI) throws ResourceNotFoundException, IllegalTypeException{
		
//		ClosableIterator<Statement> statements = model.findStatements(taggingURI, null, null);
//		if(!statements.hasNext()){
//			throw new ResourceNotFoundException("Tagging not found: " + taggingURI);
//		}
//		while(statements.hasNext()){
//			Statement statement = statements.next();
//			
//			if (S3B_MULTIMEDIATAGGING.type.equals(statement.getPredicate().asURI().toString())){
//				if(!S3B_MULTIMEDIATAGGING.Tagging.equals(statement.getObject().asURI().toString())){
//					throw new IllegalTypeException("Given URI is not a tagging : " + taggingURI);
//				}
//			}else if(S3B_MULTIMEDIATAGGING.related_to.equals(statement.getPredicate().toString())){
//				tagging.setDocumentURI(statement.getObject().asURI().toString());
//			}else if(S3B_MULTIMEDIATAGGING.hasTime.equals(statement.getPredicate().toString())){
//				long lTime = Long.parseLong(statement.getObject().asLiteral().toString());
//				tagging.setCreationTime(new Date(lTime));
//			}else if(S3B_MULTIMEDIATAGGING.title.equals(statement.getPredicate().toString())){
//				tagging.setTitle(statement.getObject().asLiteral().toString());
//			}else if(S3B_MULTIMEDIATAGGING.description.equals(statement.getPredicate().toString())){
//				tagging.setDescription(statement.getObject().asLiteral().toString());
//			}else if(S3B_MULTIMEDIATAGGING.has_creator.equals(statement.getPredicate().toString())){
//				tagging.setTaggerURI(statement.getObject().asURI().toString());
//			}else if(S3B_MULTIMEDIATAGGING.isShared.equals(statement.getPredicate().toString())){
//				boolean isShared = Boolean.parseBoolean(statement.getObject().asLiteral().toString());
//				tagging.setShared(isShared);
//			}else if(S3B_MULTIMEDIATAGGING.hasClipping.equals(statement.getPredicate().toString())){
//				String sUri = statement.getObject().asURI().toString();
//				URI clippingURI = new URIImpl(sUri);
//				Clipping clipping = ClippingDbAccess.getInstance().getClipping(model, clippingURI);
//				tagging.setClipping(clipping);
//			}else if(checkValidTerm(statement.getPredicate().toString())){
//				String sUri = statement.getObject().asURI().toString();
//				URI termURI = new URIImpl(sUri);
//				Term term = TermDbAccess.getInstance().getTerm(model, termURI);
//				terms.add(term);
//			}else if(S3B_MULTIMEDIATAGGING.linksTo.equals(statement.getPredicate().toString())){
//				links.add(statement.getObject().asURI());
//			}else if(S3B_MULTIMEDIATAGGING.hasCrossReference.equals(statement.getPredicate().toString())){
//				crossReferences.add(statement.getObject().asURI());
//			}
//		}
//		statements.close();
		
		Tagging tagging = new Tagging(taggingURI);
		List<URI> links = new ArrayList<URI>();
		List<URI> crossReferences = new ArrayList<URI>();
		List<Term> terms = new ArrayList<Term>();
		List<String> crStrings = new ArrayList<String>();
		
		//type
		URI predicate = S3B_MULTIMEDIATAGGING.TYPE.asURI();
		Statement statement = StatementFinder.findStatement(model, taggingURI, predicate, null);
		if(statement != null){
			if(!S3B_MULTIMEDIATAGGING.TAGGING.asURI().equals(statement.getObject().asURI())){
				throw new IllegalTypeException("Given URI is not a tagging : " + taggingURI);
			}
		}else{
			throw new ResourceNotFoundException("Tagging not found : " + taggingURI);
		}
		
		//related_to
		predicate = S3B_MULTIMEDIATAGGING.RELATED_TO.asURI();
		statement = StatementFinder.findStatement(model, taggingURI, predicate, null);
		if(statement != null)
			tagging.setDocumentURI(statement.getObject().asURI());
		
		//title
		predicate = S3B_MULTIMEDIATAGGING.TITLE.asURI();
		statement = StatementFinder.findStatement(model, taggingURI, predicate, null);
		if(statement != null)
			tagging.setTitle(statement.getObject().asDatatypeLiteral().getValue());
		
		//description
		predicate = S3B_MULTIMEDIATAGGING.DESCRIPTION.asURI();
		statement = StatementFinder.findStatement(model, taggingURI, predicate, null);
		if(statement != null)
			tagging.setDescription(statement.getObject().asDatatypeLiteral().getValue());
		
		//has_creator
		predicate = S3B_MULTIMEDIATAGGING.HAS_CREATOR.asURI();
		statement = StatementFinder.findStatement(model, taggingURI, predicate, null);
		if(statement != null)
			tagging.setTaggerURI(statement.getObject().asURI());
		
		//hasTime
		predicate = S3B_MULTIMEDIATAGGING.HAS_TIME.asURI();
		statement = StatementFinder.findStatement(model, taggingURI, predicate, null);
		if(statement != null){
			String value = statement.getObject().asDatatypeLiteral().getValue();
			try {
				tagging.setCreationTime(sdf.parse(value));
			} catch (ParseException e) {
				logger.severe(e.getClass() + "\n" + e.getMessage());
			}
		}
		
		//isShared
		predicate = S3B_MULTIMEDIATAGGING.IS_SHARED.asURI();
		statement = StatementFinder.findStatement(model, taggingURI, predicate, null);
		if(statement != null){
			String value = statement.getObject().asDatatypeLiteral().getValue();
			boolean isShared = Boolean.parseBoolean(value);
			tagging.setShared(isShared);
		}
		
		//links
		predicate = S3B_MULTIMEDIATAGGING.LINKS_TO.asURI();
		ClosableIterator<Statement> statements = model.findStatements(taggingURI, predicate, null);
		while(statements.hasNext()){
			statement = statements.next();
			links.add(statement.getObject().asURI());
		}
		statements.close();
		
		//crossReferences
		predicate = S3B_MULTIMEDIATAGGING.HAS_CROSSREFERENCE.asURI();
		statements = model.findStatements(taggingURI, predicate, null);
		while(statements.hasNext()){
			statement = statements.next();
			URI crURI = statement.getObject().asURI();
			crossReferences.add(crURI);

			predicate = S3B_MULTIMEDIATAGGING.TIMESTAMP_AT.asURI();
			ClosableIterator<Statement> crStatements = model.findStatements(crURI, predicate, null);
			while(crStatements.hasNext())
			{
				crStrings.add(crStatements.next().getObject().toString());
			}
			crStatements.close();
		}
		statements.close();

		
		
		//get Terms for this tagging
		TermDbAccess termDbAccess = new TermDbAccess();
		terms = termDbAccess.getTermsForTagging(model, taggingURI);
		
		//get clipping for this tagging
		ClippingDbAccess clippingDBAccess = new ClippingDbAccess();
		Clipping clipping = clippingDBAccess.getClippingForTagging(model, taggingURI);
		tagging.setClipping(clipping);
		tagging.setTerms(terms);
		
		tagging.setLinks(links);
		tagging.setCrossReferences(crossReferences);
		tagging.setCrossReferenceStrings(crStrings);
		return tagging;
	}
	
	/**
	 * Loads all taggings for the given document and given tagger from database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param documentURI <tt>URI</tt> uri of the document for which to load taggings
	 * @param taggerURI <tt>URI</tt> uri of the tagger for whom to load taggings
	 * @return <tt>List{@literal<Tagging>}</tt> list of all taggings for the given document
	 */
	public List<Tagging> getTaggingsForDocumentTagger(Model model, URI documentURI, URI taggerURI){
		List<Tagging> taggings = new ArrayList<Tagging>();
		String query = SparqlQuery.SELECT_TAGGINGS_BY_DOCUMENT_AND_TAGGER.getFormatedQuery(documentURI.toString(),taggerURI.toString());
		taggings.addAll(loadTaggingsForQuery(model, query));
		return taggings;
	}

    public Model getTaggingsForResource(Model model, URI documentURI) {
        String query = SparqlQuery.DESCRIBE_TAGGING.getFormatedQuery(documentURI.toString());
        Model modelio = new ModelFactoryImpl().createModel();
		      modelio.open();
		      modelio.setAutocommit(false);
              modelio.addAll(model.sparqlDescribe(query).iterator());
			  modelio.commit();
			  modelio.close();

        return modelio;
    }
	
	/**
	 * Loads all taggings for the given document from database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param documentURI <tt>URI</tt> uri of the document for which to load taggings
	 * @return <tt>List{@literal<Tagging>}</tt> list of all taggings for the given document
	 */
	public List<Tagging> getTaggingsForDocument(Model model, URI documentURI){
		List<Tagging> taggings = new ArrayList<Tagging>();
		String query = SparqlQuery.SELECT_TAGGINGS_BY_DOCUMENT.getFormatedQuery(documentURI.toString());
		taggings.addAll(loadTaggingsForQuery(model, query));
		return taggings;
	}
	
	/**
	 * Loads all taggings for the given tagger from database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param taggerURI <tt>URI</tt> uri of the tagger for whom to load taggings
	 * @return <tt>List{@literal<Tagging>}</tt> list of all taggings for the given document
	 */
	public List<Tagging> getTaggingsForTagger(Model model, URI taggerURI){
		List<Tagging> taggings = new ArrayList<Tagging>();
		String query = SparqlQuery.SELECT_TAGGINGS_BY_TAGGER.getFormatedQuery(taggerURI.toString());
		taggings.addAll(loadTaggingsForQuery(model, query));
		return taggings;
	}
	
	
	/**
	 * Adds new tagging to the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param tagging <tt>Tagging</tt> instance of tagging to store
	 * @return <tt>URI</tt> uri of the newly created tagging
	 */
	public URI addTagging(Model model, Tagging tagging){
		
		URI documentURI = tagging.getDocumentURI();
		URI taggerURI = tagging.getTaggerURI();
		URI taggingURI = tagging.getURI();
		
		//type info
		URI predicate = S3B_MULTIMEDIATAGGING.TYPE.asURI();
		model.addStatement(taggingURI,predicate,S3B_MULTIMEDIATAGGING.TAGGING.asURI());
		
		//add tagging to document
		predicate = S3B_MULTIMEDIATAGGING.HAS_TAGGING.asURI();
		model.addStatement(documentURI, predicate, tagging.getURI());
		
		//add document that tagging relates to
		if(tagging.getDocumentURI() != null){
			predicate = S3B_MULTIMEDIATAGGING.RELATED_TO.asURI();
			model.addStatement(taggingURI,predicate,documentURI);
		}
		
		//add tagger for this tagging
		if(tagging.getTaggerURI() != null){
			predicate = S3B_MULTIMEDIATAGGING.HAS_CREATOR.asURI();
			model.addStatement(taggingURI,predicate,taggerURI);
		}
		
		//add title
		if(tagging.getTitle() != null){
			predicate = S3B_MULTIMEDIATAGGING.TITLE.asURI();
			model.addStatement(taggingURI,predicate,model.createDatatypeLiteral(tagging.getTitle(), XSD._string));
		}
		
		//add description
		if(tagging.getDescription() != null){
			predicate = S3B_MULTIMEDIATAGGING.DESCRIPTION.asURI();
			model.addStatement(taggingURI,predicate,model.createDatatypeLiteral(tagging.getDescription(), XSD._string));
		}
		
		//add creation time
		if(tagging.getCreationTime() != null){
			predicate = S3B_MULTIMEDIATAGGING.HAS_TIME.asURI();
			String value = sdf.format(tagging.getCreationTime());
			model.addStatement(taggingURI,predicate,model.createDatatypeLiteral(value, XSD._dateTime));
		}
		
		//add is shared property
		predicate = S3B_MULTIMEDIATAGGING.IS_SHARED.asURI();
		String isShared = Boolean.toString(tagging.isShared());
		model.addStatement(taggingURI,predicate,model.createDatatypeLiteral(isShared, XSD._boolean));
		
		//add links to external resources
		if(tagging.getLinks() != null){
			predicate = S3B_MULTIMEDIATAGGING.LINKS_TO.asURI();
			for(URI link : tagging.getLinks()){
				model.addStatement(taggingURI,predicate,link);
			}
		}
		
		//add references to internal resources
		if(tagging.getCrossReferences() != null){
			int i = 0;
			String crString;
			for(URI crossReference : tagging.getCrossReferences()){
				predicate = S3B_MULTIMEDIATAGGING.HAS_CROSSREFERENCE.asURI();
				model.addStatement(taggingURI,predicate,crossReference);
				predicate = S3B_MULTIMEDIATAGGING.TIMESTAMP_AT.asURI();
				crString = tagging.getCrossReferenceStrings().get(i);
//				crString = "dupa" + i;
				model.addStatement(crossReference,predicate,crString);
				i++;
			}
		}
		return tagging.getURI();
	}
	
	/**
	 * Updates tagging properties in the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param tagging <tt>Tagging</tt> instance of tagging to update
	 * @return <tt>URI</tt> uri of the newly created tagging
	 * @throws ResourceNotFoundException when tagging with the given uri does not exists in the store 
	 * @throws IllegalTypeException when an uri that is not a tagging type have been given
	 */
	public URI updateTagging(Model model, Tagging tagging) throws ResourceNotFoundException, IllegalTypeException{
		
		//check if it exists
		URI predicate = S3B_MULTIMEDIATAGGING.TYPE.asURI();
		Statement statement = StatementFinder.findStatement(model, tagging.getURI(), predicate, null);
		if(statement != null){
			if(!S3B_MULTIMEDIATAGGING.TAGGING.asURI().equals(statement.getObject().asURI())){
				throw new IllegalTypeException("Given URI is not a tagging : " + tagging.getURI());
			}
		}else{
			throw new ResourceNotFoundException("Tagging not found : " + tagging.getURI());
		}
		
		//remove terms for this tagging
		TermDbAccess termDbAccess = new TermDbAccess();
		termDbAccess.deleteTermsForTagging(model, tagging.getURI());
		
		//remove clipping for this tagging
		ClippingDbAccess clippingDbAccess = new ClippingDbAccess();
		clippingDbAccess.deleteClippingForTagging(model, tagging.getURI());
		
		//remove tagging
		deleteTagging(model, tagging);
		
		//add the updated one to store
		addTagging(model, tagging);
		
		//add clipping
		if(tagging.getClipping() != null){
			logger.info("the uri: " + tagging.getClipping().getURI().toString());
			Clipping clipping = tagging.getClipping();
			clippingDbAccess.addClipping(model, clipping);
		}
		
		//add terms to the store
		if(tagging.getTerms() != null){
			for(Term term : tagging.getTerms()){
				termDbAccess.addTerm(model, term);
			}
		}
		
		return tagging.getURI();
	}
	
	/**
	 * Deletes existing tagging from the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param tagging <tt>Tagging</tt> instance of tagging to delete
	 */
	public void deleteTagging(Model model, Tagging tagging){
		//remove all statements with this tagging as subject
		model.removeStatements(tagging.getURI(), null, null);
		
		//also remove all statements with this tagging as object
		//all hasTagging, hasCrossReference etc;
		model.removeStatements(null,null,tagging.getURI());
		
		
		ClosableIterator<Statement> findStatements = model.findStatements(null, null, tagging.getURI());
		while(findStatements.hasNext()){
			logger.info("IN DELETE" + findStatements.next());
		}
	}
	
	/**
	 * Performs select query and loads taggings based on the results from the query
	 * @param model <tt>Model</tt> model to use for performing query
	 * @param sparqlQuery <tt>String</tt> sparql query to use
	 * @return <tt>List{@literal<Tagging>}</tt> list of taggings loaded from the storage
	 */
	private List<Tagging> loadTaggingsForQuery(Model model, String sparqlQuery) {
		List<Tagging> taggings = new ArrayList<Tagging>();
		ClosableIterator<QueryRow> rows = model.sparqlSelect(sparqlQuery).iterator();
		while(rows.hasNext()){
			QueryRow row = rows.next();
			String sURI = row.getValue("uri").toString();
			URI taggingURI = new URIImpl(sURI);
			try {
				taggings.add(getTagging(model, taggingURI));
			} catch (ResourceNotFoundException e) {
				throw new RuntimeException("SHOULD NOT HAPPEN !! TaggingUri from the queryResult does not exist in the storage !! MESS",e);
			} catch (IllegalTypeException e) {
				throw new RuntimeException("SHOULD NOT HAPPEN !! TaggingUri from the queryResult is not a tagging in the storage !! MESS",e);
			}
		}
		rows.close();
		return taggings;
	}
	
	/**
	 * Check if the given predicate is one of the hasTerm properties (or sub-properties)
	 * @param predicate <tt>String</tt> predicate to check
	 * @return <tt>true</tt> iff the given predicate is a hasTerm property(or sub-property) else <tt>false</tt>
	 */
//	private boolean checkValidTerm(String predicate) {
//		if(S3B_MULTIMEDIATAGGING.hasTerm.equals(predicate)||
//				S3B_MULTIMEDIATAGGING.hasActionTerm.equals(predicate)||
//				S3B_MULTIMEDIATAGGING.hasAgentTerm.equals(predicate)||
//				S3B_MULTIMEDIATAGGING.hasObjectTerm.equals(predicate)||
//				S3B_MULTIMEDIATAGGING.hasSettingTerm.equals(predicate)){
//			return true;
//		}
//		return false;
//	}

}
