/**
 * 
 */
package org.corrib.jonto.tagging.db.exceptions;

/**
 * @author Jakub Demczuk
 *
 */
public class IllegalTypeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1024959569152372409L;

	/**
	 * 
	 */
	public IllegalTypeException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public IllegalTypeException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public IllegalTypeException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public IllegalTypeException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

}
