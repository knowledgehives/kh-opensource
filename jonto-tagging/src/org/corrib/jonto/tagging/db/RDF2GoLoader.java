package org.corrib.jonto.tagging.db;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.GlobalTools;
import org.ontoware.rdf2go.model.Model;
import org.openrdf.rdf2go.RepositoryModel;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.http.HTTPRepository;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.sail.memory.MemoryStore;

/**
 * Class for getting rdf2go model
 * Singleton pattern
 * @author Jakub Demczuk
 *
 */
public class RDF2GoLoader {

	public static final String TAGGING_REPOSITORY = "tagging-repository";

	
	private static boolean test = true;
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.db.RDF2GoLoader");
	
	Repository repository;
	/**
	 * Singleton pattern hidden constructor
	 * Creates a repository instance to be used 
	 */
	protected RDF2GoLoader(){
		try {
			GlobalTools gt = GlobalTools.getInstance();
//			if(!test) {
                String repoPath = gt.getRespositoryPath();
                if ( repoPath.startsWith("http://") ) {
                    String[] repoSpec = repoPath.split("#");
                    repository = new HTTPRepository(repoSpec[0], repoSpec[1]);
                } else {
				    repository = new SailRepository(new MemoryStore(new File(repoPath)));
                }

//            } else {
//				File repoFile = new File(gt.getTestRepositoryPath());
//				repoFile.mkdirs();
//				repository = new SailRepository(new MemoryStore(repoFile));
//				logger.log(Level.INFO, "USING TEST REPOSITORY!!!");
//			}
			repository.initialize();
		} catch (RepositoryException e) {
			logger.severe(e.getMessage());
		}
	}
	
	private static class RDF2GoLoaderHolder{
		private static final RDF2GoLoader INSTANCE = new RDF2GoLoader();
	}
	
	/**
	 * Singleton pattern <br/>
	 * Get an instance of this class
	 * @return <tt>RDF2GoLoader</tt> instance of this class
	 */
	public static RDF2GoLoader getInstance(){
		return RDF2GoLoaderHolder.INSTANCE;
	}
	
	/**
	 * Returns a new model to work on
	 * @return
	 */
	public Model getModel(){
		Model model = new RepositoryModel(repository);
		return model;
	}


	public static void setTest(boolean _test) {
		RDF2GoLoader.test = _test;
	}
}
