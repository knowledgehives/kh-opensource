/**
 * 
 */
package org.corrib.jonto.tagging.db.logic;

import java.util.List;

import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Excerpt;
import org.corrib.jonto.tagging.db.RDF2GoLoader;
import org.corrib.jonto.tagging.db.UriGenerator;
import org.corrib.jonto.tagging.db.access.ClippingDbAccess;
import org.corrib.jonto.tagging.db.exceptions.IllegalTypeException;
import org.corrib.jonto.tagging.db.exceptions.ResourceNotFoundException;
import org.ontoware.rdf2go.model.Model;
import org.ontoware.rdf2go.model.node.URI;

/**
 * @author Jakub Demczuk
 *
 */
public class ClippingDbLogic {
	
	public ClippingDbLogic(){
		//default empty constructor
	}
	/**
	 * Loads an existing clipping
	 * @param clippingURI <tt>URI</tt> URI of the clipping to load
	 * @return <tt>Clipping</tt> loaded instance
	 * @throws ResourceNotFoundException when clipping with the given URI was not found
	 * @throws IllegalTypeException when an URI that is not a clipping type have been given
	 */
	public Clipping getClipping(URI clippingURI) throws ResourceNotFoundException, IllegalTypeException{
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		try {
			Clipping clipping = new ClippingDbAccess().getClipping(model, clippingURI);
			model.commit();
			model.close();
//			((Excerpt)clipping).setLength(890);
			return clipping;
		} catch (ResourceNotFoundException e) {
			model.close();
			throw e;
		} catch (IllegalTypeException e) {
			model.close();
			throw e;
		}
	}
	
	/**
	 * Loads clipping for the given tagging from the database
	 * @param taggingURI <tt>URI</tt> URI of the tagging for which to get clippings
	 * @return <tt>Clipping</tt> clipping for the given tagging or <tt>null</tt> if this tagging has no clipping
	 */
	public Clipping getClippingForTagging(URI taggingURI){
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		Clipping clipping = new ClippingDbAccess().getClippingForTagging(model, taggingURI);
		model.commit();
		model.close();
		return clipping;
	}
	
	/**
	 * Loads all clippings for the given document and given tagger
	 * @param documentURI <tt>URI</tt> URI of the document for which to load clippings
	 * @param taggerURI <tt>URI</tt> URI of the tagger for whom to load clippings
	 * @return <tt>List{@literal<Clipping>}</tt> list of all clippings for the given document
	 */
	public List<Clipping> getClippingsForDocumentTagger(URI documentURI, URI taggerURI){
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		List<Clipping> clippings = new ClippingDbAccess().getClippingsForDocumentTagger(model, documentURI, taggerURI);
		model.commit();
		model.close();
		return clippings;
	}
	
	/**
	 * Loads all clippings for the given document
	 * @param documentURI <tt>URI</tt> URI of the document for which to load clippings
	 * @return <tt>List{@literal<Clipping>}</tt> list of all clippings for the given document
	 */
	public List<Clipping> getClippingsForDocument(URI documentURI){
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		List<Clipping> clippings = new ClippingDbAccess().getClippingsForDocument(model, documentURI);
		model.commit();
		model.close();
		return clippings;
	}
	/**
	 * Loads all clippings for the given tagger
	 * @param taggerURI <tt>URI</tt> URI of the tagger for whom to load clippings
	 * @return <tt>List{@literal<Clipping>}</tt> list of all clippings for the given document
	 */
	public List<Clipping> getClippingsForTagger(URI taggerURI){
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		List<Clipping> clippings = new ClippingDbAccess().getClippingsForTagger(model, taggerURI);
		model.commit();
		model.close();
		return clippings;
	}
	
	/**
	 * Adds new clipping
	 * @param clipping <tt>Clipping</tt> instance of clipping to store
	 * @param serviceAddress <tt>String</tt> address of the service sending request
	 * @return <tt>URI</tt> URI of the newly created clipping
	 */
	public URI addClipping(Clipping clipping,String serviceAddress){
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		URI clippingURI = UriGenerator.getUri(clipping, serviceAddress);
		clipping.setURI(clippingURI);
		Excerpt ex = (Excerpt)clipping;
		new ClippingDbAccess().addClipping(model, clipping);
		model.commit();
		model.close();
		return clippingURI;
	}
	
	/**
	 * Updates clipping properties in the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param clipping <tt>Clipping</tt> instance of clipping to update
	 * @return <tt>URI</tt> URI of the newly created clipping
	 * @throws ResourceNotFoundException when clipping with the given uri does not exists in the store 
	 * @throws IllegalTypeException when an uri that is not a clipping type have been given
	 */
	public URI updateClipping(Clipping clipping) throws ResourceNotFoundException, IllegalTypeException{
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		URI clippingURI = new ClippingDbAccess().updateClipping(model, clipping);
		model.commit();
		model.close();
		return clippingURI;
	}
	
	/**
	 * Deletes existing clipping from the database
	 * @param clipping <tt>Clipping</tt> instance of clipping to delete
	 */
	public void deleteClipping(Clipping clipping){
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		new ClippingDbAccess().deleteClipping(model, clipping);
		model.commit();
		model.close();
	}
	
	

}
