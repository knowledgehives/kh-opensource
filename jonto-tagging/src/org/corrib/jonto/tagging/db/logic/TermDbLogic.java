package org.corrib.jonto.tagging.db.logic;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.beans.Term;
import org.corrib.jonto.tagging.db.RDF2GoLoader;
import org.corrib.jonto.tagging.db.UriGenerator;
import org.corrib.jonto.tagging.db.access.TermDbAccess;
import org.corrib.jonto.tagging.db.exceptions.IllegalTypeException;
import org.corrib.jonto.tagging.db.exceptions.ResourceNotFoundException;
import org.ontoware.rdf2go.model.Model;
import org.ontoware.rdf2go.model.node.URI;

public class TermDbLogic {
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.db.TermDbAccess");
	
	public TermDbLogic(){
		//default empty constructor
	}
	
		
	/**
	 * Loads an existing Terms for the given termURI
	 * @param termURI <tt>URI</tt> uri of the term to load
	 * @return <tt>Term</tt> loaded instance from the database
	 * @throws ResourceNotFoundException when term with the given URI was not found
	 * @throws IllegalTypeException when an uri that is not a term type have benn given
	 */
	public Term getTerm(URI termURI) throws ResourceNotFoundException, IllegalTypeException{
		TermDbAccess dbAccess = new TermDbAccess();
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		try {
			Term term = dbAccess.getTerm(model, termURI);
			model.commit();
			model.close();
			return term;
		} catch (ResourceNotFoundException e) {
			model.close();
			throw e;
		} catch (IllegalTypeException e) {
			model.close();
			throw e;
		}
	}
	
	/**
	 * Loads all existing Terms for the given tagging
	 * @param taggingURI <tt>URI</tt> uri of the tagging for which to get terms
	 * @return <tt>List{@literal<Term>}</tt> list of terms for the given tagging
	 */
	public List<Term> getTermsForTagging(URI taggingURI){
		TermDbAccess dbAccess = new TermDbAccess();
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		List<Term> terms = dbAccess.getTermsForTagging(model, taggingURI);
		model.commit();
		model.close();
		return terms;
	}
	
	/**
	 * Loads all existing terms for the given  document and tagger
	 * @param documentURI <tt>URI</tt> string uri of the document for which to load terms
	 * @param taggerURI <tt>URI</tt> string uri of the tagger for whom to load terms
	 * @return <tt>List{@literal<Term>}</tt> list of terms for the given document created by the given tagger
	 */
	public List<Term> getTermsForDocumentTagger(URI documentURI, URI taggerURI){
		TermDbAccess dbAccess = new TermDbAccess();
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		List<Term> terms = dbAccess.getTermsForDocumentTagger(model, documentURI, taggerURI);
		model.commit();
		model.close();
		return terms;
	}
	/**
	 * Loads all existing terms for the given document
	 * @param documentURI <tt>URI</tt> string uri of the document for which to load terms
	 * @return <tt>List{@literal<Term>}</tt> list of terms for the given document 
	 */
	public List<Term> getTermsForDocument(URI documentURI){
		TermDbAccess dbAccess = new TermDbAccess();
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		List<Term> terms = dbAccess.getTermsForDocument(model, documentURI);
		model.commit();
		model.close();
		return terms;
	}
	
	/**
	 * Loads all existing terms for the given tagger
	 * @param taggerURI <tt>URI</tt> string uri of the tagger for whom to load terms
	 * @return <tt>List{@literal<Term>}</tt> list of terms for the given document 
	 */
	public List<Term> getTermsForTagger(URI taggerURI){
		TermDbAccess dbAccess = new TermDbAccess();
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		List<Term> terms = dbAccess.getTermsForTagger(model, taggerURI);
		model.commit();
		model.close();
		return terms;
	}
	
	/**
	 * Adds new Term
	 * @param term <tt>Term</tt> instance of Term to store
	 * @param serviceAddress <tt>String</tt> address of the service sending request
	 * @return <tt>URI</tt> uri of the newly created Term
	 */
	public URI addTerm(Term term, String serviceAddress){
		logger.log(Level.INFO, "hello addTerm");
		TermDbAccess dbAccess = new TermDbAccess();
		URI uri = UriGenerator.getUri(term, serviceAddress);
		term.setURI(uri);
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		URI termUri = dbAccess.addTerm(model, term);
		model.commit();
		model.close();
		return termUri;
	}
	
	/**
	 * Updates Term properties in the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param term <tt>Term</tt> instance of Term to update
	 * @return <tt>URI</tt> uri of the newly created Term
	 * @throws ResourceNotFoundException when term with the given uri does not exists in the store
	 * @throws IllegalTypeException when an uri that is not a term type have been given
	 */
	public URI updateTerm(Term term) throws ResourceNotFoundException, IllegalTypeException{
		TermDbAccess dbAccess = new TermDbAccess();
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		URI termUri = dbAccess.updateTerm(model, term);
		model.commit();
		model.close();
		return termUri;
	}
	
	/**
	 * Deletes existing Term from the database
	 * @param model <tt>Model</tt> model to use for performing operations
	 * @param term <tt>Term</tt> instance of Term to delete
	 */
	public void deleteTerm(Term term){
		TermDbAccess dbAccess = new TermDbAccess();
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		dbAccess.deleteTerm(model, term);
		model.commit();
		model.close();
	}
}
