package org.corrib.jonto.tagging.db.logic;

import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Tagging;
import org.corrib.jonto.tagging.beans.Term;
import org.corrib.jonto.tagging.db.RDF2GoLoader;
import org.corrib.jonto.tagging.db.UriGenerator;
import org.corrib.jonto.tagging.db.access.ClippingDbAccess;
import org.corrib.jonto.tagging.db.access.TaggingDbAccess;
import org.corrib.jonto.tagging.db.access.TermDbAccess;
import org.corrib.jonto.tagging.db.exceptions.IllegalTypeException;
import org.corrib.jonto.tagging.db.exceptions.ResourceNotFoundException;
import org.corrib.jonto.tagging.soa.rest.input.exceptions.MalformedRequestException;
import org.ontoware.rdf2go.model.Model;
import org.ontoware.rdf2go.model.node.URI;

public class TaggingDbLogic {
	
	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.db.logic.TaggingDbLogic");
	
	public TaggingDbLogic(){
		//default empty constructor
	}
	
	/**
	 * Loads an existing clipping
	 * @param taggingURI <tt>URI</tt> uri of the clipping to load
	 * @return <tt>Tagging</tt> loaded instance from the database
	 * @throws ResourceNotFoundException when tagging with the given URI was not found
	 * @throws IllegalTypeException when an uri that is not a tagging type have been given
	 */
	public Tagging getTagging(URI taggingURI) throws ResourceNotFoundException, IllegalTypeException{
		TaggingDbAccess dbAccess = new TaggingDbAccess();
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		try {
			Tagging tagging = dbAccess.getTagging(model, taggingURI);
			model.commit();
			model.close();
			return tagging;
		} catch (ResourceNotFoundException e) {
			model.close();
			throw e;
		} catch (IllegalTypeException e) {
			model.close();
			throw e;
		}
	}
	
	/**
	 * Loads all taggings for the given document and given tagger
	 * @param documentURI <tt>URI</tt> string uri of the document for which to load taggings
	 * @param taggerURI <tt>URI</tt> string uri of the tagger for whom to load taggings
	 * @return <tt>List{@literal<Tagging>}</tt> list of all taggings for the given document
	 */
	public List<Tagging> getTaggingsForDocumentTagger(URI documentURI, URI taggerURI){
		TaggingDbAccess dbAccess = new TaggingDbAccess();
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		List<Tagging> taggings = dbAccess.getTaggingsForDocumentTagger(model, documentURI, taggerURI);
		model.commit();
		model.close();
		return taggings;
	}

    public Model getTaggingsForResource(URI documentURI) {
        TaggingDbAccess dbAccess = new TaggingDbAccess();
        Model model = RDF2GoLoader.getInstance().getModel();
        model.open();
        model.setAutocommit(false);
        Model modelio = dbAccess.getTaggingsForResource(model, documentURI);
        model.commit();
        model.close();
        return modelio;
    }
	
	/**
	 * Loads all taggings for the given document
	 * @param documentURI <tt>URI</tt> string uri of the document for which to load taggings
	 * @return <tt>List{@literal<Tagging>}</tt> list of all taggings for the given document
	 */
	public List<Tagging> getTaggingsForDocument(URI documentURI){
		TaggingDbAccess dbAccess = new TaggingDbAccess();
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		List<Tagging> taggings = dbAccess.getTaggingsForDocument(model, documentURI);
		model.commit();
		model.close();
		return taggings;
	}
	
	/**
	 * Loads all taggings for the given tagger
	 * @param taggerURI <tt>URI</tt> string uri of the tagger for whom to load taggings
	 * @return <tt>List{@literal<Tagging>}</tt> list of all taggings for the given document
	 */
	public List<Tagging> getTaggingsForTagger(URI taggerURI){
		TaggingDbAccess dbAccess = new TaggingDbAccess();
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		List<Tagging> taggings = dbAccess.getTaggingsForTagger(model, taggerURI);
		model.commit();
		model.close();
		return taggings;
	}
	
	/**
	 * Adds new tagging to the database
	 * @param tagging <tt>Tagging</tt> instance of tagging to store
	 * @param serviceAddress <tt>String</tt> address of the service sending request
	 * @return <tt>URI</tt> uri of the newly created tagging
	 */
	public URI addTagging(Tagging tagging, String serviceAddress){
		TaggingDbAccess dbAccess = new TaggingDbAccess();
		tagging.setCreationTime(new Date());
		URI uri = UriGenerator.getUri(tagging, serviceAddress);
		tagging.setURI(uri);
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		dbAccess.addTagging(model, tagging);
		if(tagging.getClipping() != null){
			Clipping clipping = tagging.getClipping();
			clipping.setTaggingURI(tagging.getURI());
			URI clippingUri = UriGenerator.getUri(clipping, serviceAddress);
			clipping.setURI(clippingUri);
			ClippingDbAccess clippingDbAccess = new ClippingDbAccess();
			clippingDbAccess.addClipping(model, clipping);
		}
		if(tagging.getTerms() != null){
			for(Term term : tagging.getTerms()){
				term.setTaggingURI(tagging.getURI());
				if(term.getURI() == null)
				{
					URI termUri = UriGenerator.getUri(term, serviceAddress);
					term.setURI(termUri);
				}
				TermDbAccess termDbAccess = new TermDbAccess();
				termDbAccess.addTerm(model, term);
			}
		}
		model.commit();
		model.close();
		return tagging.getURI();
	}
	
	/**
	 * Updates tagging properties in the database
	 * @param serviceAddress <tt>String</tt> address of the service sending request
	 * @return <tt>URI</tt> uri of the newly created tagging
	 * @throws ResourceNotFoundException when tagging with the given URI was not found
	 * @throws IllegalTypeException when an uri that is not a tagging type have been given
	 */
	public URI updateTagging(Tagging tagging, String serviceAddress) throws ResourceNotFoundException, IllegalTypeException{
		TaggingDbAccess dbAccess = new TaggingDbAccess();
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		try{
			//generate new URIs for all terms that are new and for the clipping if it's new
			for(Term term : tagging.getTerms()){
				if(term.getURI() == null){
					URI uri = UriGenerator.getUri(term, serviceAddress);
					term.setURI(uri);
					term.setTaggingURI(tagging.getURI());
				}
				logger.info("old tURI: " + term.getTaggingURI() + " : " + term.getTaggingURI());
			}
			if(tagging.getClipping() != null && tagging.getClipping().getURI() == null){
				URI uri = UriGenerator.getUri(tagging.getClipping(), serviceAddress);
				tagging.getClipping().setURI(uri);
				tagging.getClipping().setTaggingURI(tagging.getURI());
			}
			URI taggingURI = dbAccess.updateTagging(model, tagging);
			model.commit(); 
			model.close();
			return taggingURI;
		} catch (ResourceNotFoundException e) {
			model.close();
			throw e;
		} catch (IllegalTypeException e) {
			model.close();
			throw e;
		}
	}

	
	
	/**
	 * Deletes existing tagging from the database
	 * @param tagging <tt>Tagging</tt> instance of tagging to delete
	 */
	public void deleteTagging(Tagging tagging){
		TaggingDbAccess dbAccess = new TaggingDbAccess();
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		model.setAutocommit(false);
		dbAccess.deleteTagging(model, tagging);
		if(tagging.getClipping() != null){
			ClippingDbAccess clippingDbAccess = new ClippingDbAccess();
			clippingDbAccess.deleteClipping(model, tagging.getClipping());
		}
		for(Term term : tagging.getTerms()){
			TermDbAccess termDbAccess = new TermDbAccess();
			termDbAccess.deleteTerm(model, term);
		}
		model.commit();
		model.close();
	}
}
