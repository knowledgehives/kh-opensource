package org.corrib.jonto.tagging;

import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;

/**
 * Vocabulary definition
 * 
 * Presents classes and properties that creates Tagging Ontology 
 * 
 * General class hierarchy:
 * 
 * 	- Document 
 * 	- Tagging
 * 		- Clipping
 * 			- ROI
 * 				- CircleROI
 * 				- RectangleROI
 * 			-Excerpt
 *  - Term
 * 	- Tagger
 * 
 * See properties description to see to what type of classes they apply.
 * 
 * @author Jakub Demczuk
 *
 */
public enum S3B_MULTIMEDIATAGGING {

	DOCUMENT("http://s3b.corrib.org/tagging/Document"),
	TAGGING("http://s3b.corrib.org/tagging/Tagging"),
	CLIPPING("http://s3b.corrib.org/tagging/Clipping"),
	EXCERPT("http://s3b.corrib.org/tagging/Excerpt"),
	TAGGER("http://s3b.corrib.org/tagging/Tagger"),
	ROI("http://s3b.corrib.org/tagging/ROI"),
	CIRCLE_ROI("http://s3b.corrib.org/tagging/CircleROI"),
	RECTANGLE_ROI("http://s3b.corrib.org/tagging/RectangleROI"),
	TERM("http://s3b.corrib.org/tagging/Term"),
	
	HAS_CLIPPING("http://s3b.corrib.org/tagging/hasClipping"),
	HAS_CROSSREFERENCE("http://s3b.corrib.org/tagging/hasCrossReference"),
	TIMESTAMP_AT("http://s3b.corrib.org/tagging/timeStampAt"),
	HAS_TAGGING("http://s3b.corrib.org/tagging/hasTagging"),
	HAS_TERM("http://s3b.corrib.org/tagging/hasTerm"),
	HAS_ACTION_TERM("http://s3b.corrib.org/tagging/hasActionTerm"),
	HAS_AGENT_TERM("http://s3b.corrib.org/tagging/hasAgentTerm"),
	HAS_OBJECT_TERM("http://s3b.corrib.org/tagging/hasObjectTerm"),
	HAS_SETTING_TERM("http://s3b.corrib.org/tagging/hasSettingTerm"),
	HAS_EXCERPT_LENGTH("http://s3b.corrib.org/tagging/hasExcerptLength"),
	HAS_EXCERPT_START("http://s3b.corrib.org/tagging/hasExcerptStart"),
	HAS_X_COORDINATE("http://s3b.corrib.org/tagging/hasXCoordinate"),
	HAS_Y_COORDINATE("http://s3b.corrib.org/tagging/hasYCoordinate"),
	HAS_HEIGHT("http://s3b.corrib.org/tagging/hasHeight"),
	HAS_WIDTH("http://s3b.corrib.org/tagging/hasWidth"),
	HAS_RADIUS("http://s3b.corrib.org/tagging/hasRadius"),
	HAS_TIME("http://s3b.corrib.org/tagging/hasTime"),
	IS_SHARED("http://s3b.corrib.org/tagging/isShared"),
	LINKS_TO("http://s3b.corrib.org/tagging/linksTo"),
	CREATOR("http://purl.org/dc/elements/1.1/creator"),
	DESCRIPTION("http://purl.org/dc/elements/1.1/description"),
	TITLE("http://purl.org/dc/elements/1.1/title"),
	CREATED("http://purl.org/dc/terms/created"),
	HAS_CREATOR("http://rdfs.org/sioc/ns#has_creator"),
	RELATED_TO("http://rdfs.org/sioc/ns#related_to"),
	TOPIC("http://rdfs.org/sioc/ns#topic"),
	TYPE("http://www.w3.org/1999/02/22-rdf-syntax-ns#type"),
	PREF_LABEL("http://www.w3.org/2004/02/skos/core#prefLabel");
	
	
	private final String sURI;
	private URI uri;


	S3B_MULTIMEDIATAGGING(String _uri){
		sURI = _uri;
		uri = new URIImpl(_uri);
	}
	
	public String asString(){
		return sURI;
	}
	
	public URI asURI(){
		return uri;
	}
	
	/**
	 * The namespace of the vocabulary as String <br/>
	 * <tt>http://s3b.corrib.org/tagging</tt>
	 * 
	 */
	public static final String NAMESPACE = "http://s3b.corrib.org/tagging";
	
	
	
}