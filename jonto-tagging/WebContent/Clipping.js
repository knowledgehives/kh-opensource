var Clipping = Class.create();


Clipping.prototype = {
	servletAddress : "http://localhost:7390/mmt/Clipping",
	initialize : function(){
		var params = new Hash();
		params.set('type', 'http://s3b.corrib.org/tagging/RectangleROI');
		params.set('xCoordinate', 888);
		params.set('yCoordinate', 777);
		params.set('width', 912);
		params.set('height', 234);
		params.set('taggingURI','http://taggingURIForClipping.com');
		params.set('responseFormat','xml');
		var ajax = new Ajax.Request(this.servletAddress, {
			method : "post",
			parameters : params.toQueryString(),
			onSuccess : function(resp){
				alert(resp.responseText);
			},	 
			onException : function(e){
				alert(e)
			},
			onFailure : function(resp){
				alert(resp.responseText);
			}
		});
	}
}