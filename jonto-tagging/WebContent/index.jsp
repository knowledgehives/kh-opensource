<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>JeromeDL Video Tagging</title>
<script src="_assets/js/AC_OETags.js" language="javascript"></script>
<style>
body { margin: 0px; overflow:hidden }
</style>
<script language="JavaScript" type="text/javascript">
<!--
// -----------------------------------------------------------------------------
// Globals
// Major version of Flash required
var requiredMajorVersion = 10;
// Minor version of Flash required
var requiredMinorVersion = 0;
// Minor version of Flash required
var requiredRevision = 0;
// -----------------------------------------------------------------------------
// -->
</script>


<c:if test="${ not empty param.movie && not empty param.user }">
<link rel="media" type="text/turtle" title="RDF annotations for this movie" href="http://10.0.2.15:7380/mmt/Tagging/?responseFormat=rdf&documentURI=http://10.0.2.15:7380/mmt/assets/${ param.movie }"/>
</c:if>

</head>

<body scroll="no">

<h1>JeromeDL Video Tagging Demo</h1>

<form target="">
    <label for="movieid">Select example FLV video:&nbsp;</label><select id="movieid" name="movie">
        <option value="walle.flv" ${ (param.move eq 'walle.flv') ? 'selected="selected"' : '' }>Wall:E Trailer</option>
        <option value="pixar.flv" ${ (param.move eq 'pixar.flv') ? 'selected="selected"' : '' }>Birds</option>
        <option value="tennis.flv" ${ (param.move eq 'tennis.flv') ? 'selected="selected"' : '' }>Tennis</option>
    </select>
    <label for="userid">You user ID:&nbsp;</label><span>http://demo-video.jeromedl.com/users/<input type="text" value="${ (not empty param.user) ? (param.user) : ('anonymous') }" name="user" id="userid"/></span>
    <button type="submit">Load</button>
</form>

<c:if test="${ not empty param.movie && not empty param.user }">

<h2><a href="http://10.0.2.15:7380/mmt/Tagging/?responseFormat=rdf&documentURI=http://10.0.2.15:7380/mmt/assets/${ param.movie }" title="RDF annotations for this movie">RDF annotations for this movie</a></h2>

<script language="JavaScript" type="text/javascript" src="_assets/js/history.js"></script>
<script language="JavaScript" type="text/javascript">
<!--
// Version check for the Flash Player that has the ability to start Player Product Install (6.0r65)
var hasProductInstall = DetectFlashVer(6, 0, 65);

// Version check based upon the values defined in globals
var hasRequestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);


// Check to see if a player with Flash Product Install is available and the version does not meet the requirements for playback
if ( hasProductInstall && !hasRequestedVersion ) {
	// MMdoctitle is the stored document.title value used by the installation process to close the window that started the process
	// This is necessary in order to close browser windows that are still utilizing the older version of the player after installation has completed
	// DO NOT MODIFY THE FOLLOWING FOUR LINES
	// Location visited after installation is complete if installation is required
	var MMPlayerType = (isIE == true) ? "ActiveX" : "PlugIn";
	var MMredirectURL = window.location;
    document.title = document.title.slice(0, 47) + " - Flash Player Installation";
    var MMdoctitle = document.title;

	AC_FL_RunContent(
		"src", "playerProductInstall",
		"FlashVars", "MMredirectURL="+MMredirectURL+'&MMplayerType='+MMPlayerType+'&MMdoctitle='+MMdoctitle+"",
		"width", "800",
		"height", "600",
		"align", "middle",
		"id", "JDLPlayer",
		"quality", "high",
		"bgcolor", "#FFFFFF",
		"name", "JDLPlayer",
		"allowScriptAccess","sameDomain",
		"type", "application/x-shockwave-flash",
		"pluginspage", "http://www.adobe.com/go/getflashplayer"
	);
} else if (hasRequestedVersion) {
	// if we've detected an acceptable version
	// embed the Flash Content SWF when all tests are passed
	AC_FL_RunContent(
			"src", "JDLPlayer",
			"width", "800",
			"height", "600",
			"align", "middle",
			"id", "JDLPlayer",
			"quality", "high",
			"bgcolor", "#FFFFFF",
			"name", "JDLPlayer",
			"flashvars",'srv=http://10.0.2.15:7380/mmt/&movie=assets/${ param.movie }&tagger=http://demo-video.jeromedl.com/users/${ param.user }&fileField=rbt&historyUrl=_assets/html/history.htm%3F&lconid=' + lc_id + '',
			"allowScriptAccess","sameDomain",
			"type", "application/x-shockwave-flash",
			"pluginspage", "http://www.adobe.com/go/getflashplayer"
	);
  } else {  // flash is too old or we can't detect the plugin
    var alternateContent = 'Alternate HTML content should be placed here. '
  	+ 'This content requires the Adobe Flash Player. '
   	+ '<a href=http://www.adobe.comc/go/getflash/>Get Flash</a>';
    document.write(alternateContent);  // insert non-flash content
  }
// -->
</script>
<noscript>
  	<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
			id="JDLPlayer" width="800" height="600"
			codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">
			<param name="movie" value="JDLPlayer.swf" />
			<param name="quality" value="high" />
			<param name="bgcolor" value="#FFFFFF" />
			<param name="allowScriptAccess" value="sameDomain" />
			<param name="flashvars"
                   value="srv=http://10.0.2.15:7380/mmt/&movie=assets/${ param.movie }&tagger=http://demo-video.jeromedl.com/users/${ param.user }">
			<embed src="JDLPlayer.swf" quality="high" bgcolor="#FFFFFF"
				width="800" height="600" name="JDLPlayer" align="middle"
				play="true"
				loop="false"
				quality="high"
				allowScriptAccess="sameDomain"
				type="application/x-shockwave-flash"
				flashvars="srv=http://10.0.2.15:7380/mmt/&movie=assets/${ param.movie }&tagger=http://demo-video.jeromedl.com/users/${ param.user }"
				pluginspage="http://www.adobe.com/go/getflashplayer">
			</embed>
	</object>
</noscript>
<iframe name="_history" src="_assets/html/history.htm" frameborder="0" scrolling="no" width="22" height="0"></iframe>

</c:if>
</body>
</html>
