var Term = Class.create();

Term.prototype = {
	servletAddress : "http://localhost:7390/mmt/Term",
	
	initialize : function(){
		var params = new Hash();
		params.set('type', 'http://s3b.corrib.org/tagging/hasAgentTerm');
		params.set('tag','TestTagNumeroUno');
		params.set('taggingURI','http://taggingURIForTerm.com');
		params.set('responseFormat','xml');
		var ajax = new Ajax.Request(this.servletAddress, {
			method : "post",
			parameters : params.toQueryString(),
			onSuccess : function(resp){
				alert(resp.responseText);
			},	 
			onException : function(e){
				alert(e)
			},
			onFailure : function(resp){
				alert(resp.responseText);
			}
		});
	}
} 