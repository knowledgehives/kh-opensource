var Tagging = Class.create();


Tagging.prototype = {
	servletAddress : "http://localhost:7390/mmt/Tagging",
	initialize : function(){
		var params = new Hash();
		params.set('title','TITLE TEST');
		params.set('description','description');
		params.set('shared',true);
		params.set('documentURI','http://document.com');
//		params.set('taggerURI','http://tagger.com');
		var links = $A();
		links.push('http://links1.com');
		links.push('http://links2.com');
		links.push('http://links3.com');
		links.push('http://links4.com');
		params.set('links',links);
		var crossReferences = $A();
		crossReferences.push('http://crossURI1.com');
		crossReferences.push('http://crossURI2.com');
		crossReferences.push('http://crossURI3.com');
		crossReferences.push('http://crossURI4.com');
		params.set('crossReferences',crossReferences);
		
		/*
		 * CLIPPING for TAGGING
		 * clippingURI||clipingType||specificPropertiesForType 
		 * 
		 * where sprecificPropertiesForType mean:
		 * 	ROI:
		 * 		xCoordinate || yCoordinate
		 * 		RectangleROI : 
		 * 					width||heigth
		 * 		CircleROI:
		 * 					radius
		 * Excerpt:
		 * 		startPoint||length
		 */		
		var clipping =
		null +'||http://s3b.corrib.org/tagging/CircleROI||123||445||12'; 
		params.set('clipping',clipping);
		
		var terms = $A();
		
		/*
		 * TERM for TAGGING:
		 * termURI||termType||tag
		 */
		var term1 = null+"||http://s3b.corrib.org/tagging/hasTerm||tag1";		
		var term2 = null+"||http://s3b.corrib.org/tagging/hasSettingTerm||tag2";		
		var term3 = null+"||http://s3b.corrib.org/tagging/hasObjectTerm||tag3";		
		terms.push(term1);
		terms.push(term2);
		terms.push(term3);
		
		params.set('terms',terms);
		
		params.set('responseFormat','xml');
		
		var ajax = new Ajax.Request(this.servletAddress, {
			method : "get",
			parameters : params.toQueryString(),
			onSuccess : function(resp){
//				alert(resp.responseText);
				console.log(resp.responseText);
				var a = eval('(' + resp.responseText + ')');
				var b = "addd";
			},	 
			onException : function(e){
				alert(e)
			},
			onFailure : function(resp){
				alert(resp.responseText);
			}
		});
	}
}