package org.corrib.jonto.tagging;

import static org.testng.Assert.fail;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.corrib.jonto.tagging.beans.Term;
import org.corrib.jonto.tagging.soa.rest.input.XML_RESTInput;
import org.corrib.jonto.tagging.soa.rest.input.exceptions.MalformedStringException;
import org.corrib.jonto.tagging.soa.rest.output.XML_RESTOutput;


public class Testohoho {

	/**
	 * @param args
	 */
	private static final Logger logger = Logger.getLogger("org.s3b.multimediaterm.test.soa.servlet.TaggingServletTest");
	private static final String serverURL = "http://localhost:7380/mmt/Clipping/";
	private static final String responseFormat = "responseFormat=xml";
	private static final String taggingURI = "http://taggingURIasdd.com";
	private static String termXML;
	private static Term term;
	private static HttpClient httpClient = new HttpClient();
	public static void main(String[] args) {
		try{
			GetMethod get = new GetMethod(serverURL+"?clippingUri=http%3A%2F%2Flocalhost%3A7390%2Fmmt%2Fclipping%2F76f9eae7145fcc9d4cdb63cc35895eafd50b4a13&responseFormat=xml");
			PostMethod post = new PostMethod(serverURL);
			post.addParameter("responseFormat", "xml");
//			post.addParameter("requestFormat", "xml");
			XML_RESTOutput out = new XML_RESTOutput();
//			termXML = out.getOutputText(term);
//			post.addParameter("term", termXML);
			httpClient.executeMethod(get);

			String newClippingXML = get.getResponseBodyAsString();
			logger.log(Level.INFO, "" + newClippingXML);
			term = new XML_RESTInput().parseTerm(newClippingXML);
		} catch (MalformedStringException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (HttpException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		}

	}

}
