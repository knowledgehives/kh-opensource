/**
 * 
 */
package org.corrib.jonto.tagging.test.db.logic;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.beans.CircleROI;
import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Excerpt;
import org.corrib.jonto.tagging.beans.RectangleROI;
import org.corrib.jonto.tagging.db.exceptions.IllegalTypeException;
import org.corrib.jonto.tagging.db.exceptions.ResourceNotFoundException;
import org.corrib.jonto.tagging.db.logic.ClippingDbLogic;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * @author Jakub Demczuk
 *
 */
public class ClippingDbLogicTest {

	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.test.db.logic.TermDbLogicTest");
	
	private ClippingDbLogic instance;
	
	private List<Clipping> clippings;

	private String taggingURI;

	private String serviceAddress;
	
	int x,y;
	
	@BeforeClass
	public void init(){
		instance = new ClippingDbLogic();
		clippings = new ArrayList<Clipping>();
		taggingURI = "http://taggingtestx.com";
		serviceAddress = "http://localhost:7390/";
		x = 44;
		y = 99;
	}
	
	
	@Test (groups = {"creators"})
	public void storeCircleROITest(){
		int radius = 123;
		
		CircleROI circle = new CircleROI();
		circle.setRadius(radius);
		circle.setXCoordinate(x);
		circle.setYCoordinate(y);
		circle.setTaggingURI(taggingURI);
		String clippingURI = instance.addClipping(circle, serviceAddress).toString();
		assertNotNull(clippingURI);
		assertFalse("".equals(clippingURI));
		clippings.add(circle);
	}
	
	@Test (groups = {"creators"})
	public void storeRectangleROITest(){
		int width = 100;
		int height = 123;
		RectangleROI rec = new RectangleROI();
		rec.setXCoordinate(x);
		rec.setYCoordinate(y);
		rec.setHeight(height);
		rec.setWidth(width);
		rec.setTaggingURI(taggingURI);
		String clippingURI = instance.addClipping(rec, serviceAddress).toString();
		assertNotNull(clippingURI);
		assertFalse("".equals(clippingURI));
		clippings.add(rec);
	}
	
	@Test (groups = {"creators"})
	public void storeExcerptTest(){
		long startPoint = 123761924;
		long length = 1246173;
		Excerpt excerpt = new Excerpt();
		excerpt.setTaggingURI(taggingURI);
		excerpt.setStartPoint(startPoint);
		excerpt.setLength(length);
		String clippingURI = instance.addClipping(excerpt, serviceAddress).toString();
		assertNotNull(clippingURI);
		assertFalse("".equals(clippingURI));
		clippings.add(excerpt);
	}
	
	@Test (groups = {"getters"}, dependsOnGroups = "creators")
	public void getClippingTest(){
		for(Clipping clipping : clippings){
			Clipping clipping2;
			try {
				clipping2 = instance.getClipping(clipping.getURI());
				assertEquals(clipping2.getURI(), clipping.getURI());
				assertEquals(clipping2.getClass(), clipping.getClass());
			} catch (ResourceNotFoundException e) {
				fail(e.getMessage());
			} catch (IllegalTypeException e) {
				fail(e.getMessage());
			}
		}
	}
	
	@Test (groups = {"deleters"}, dependsOnGroups = {"creators","getters"})
	public void deleteClippingTest(){
		for(Clipping clipping : clippings){
			instance.deleteClipping(clipping);
			try {
				instance.getClipping(clipping.getURI());
				fail("Resource found after being removed!!");
			} catch (ResourceNotFoundException e) {
				//valid exception
			} catch (IllegalTypeException e) {
				//valid exception
			}
		}
	}
	
}
