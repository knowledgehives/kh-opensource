/**
 * 
 */
package org.corrib.jonto.tagging.test.db.logic;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.List;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.beans.Term;
import org.corrib.jonto.tagging.beans.TermType;
import org.corrib.jonto.tagging.db.exceptions.IllegalTypeException;
import org.corrib.jonto.tagging.db.exceptions.ResourceNotFoundException;
import org.corrib.jonto.tagging.db.logic.TermDbLogic;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
/**
 * @author Jakub Demczuk
 *
 */
public class TermDbLogicTest {

	
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.test.db.logic.TermDbLogicTest");
	
	private TermDbLogic instance;
	
	private Term storedTerm;

	private URI taggingURI;

	private String serviceAddress;
	
	
	@BeforeClass
	public void init(){
		instance = new TermDbLogic();
		taggingURI = new URIImpl("http://taggingtestx.com");
		serviceAddress = "http://localhost:7390/";
	}
	
	@Test (groups = {"creators"})
	public void storeTermTest(){
		String tag = "tag";
		Term term = new Term();
		term.setTag(tag);
		term.setTermType(TermType.ACTION_TERM);
		term.setTaggingURI(taggingURI);
		
		String storeTerm = instance.addTerm(term, serviceAddress).toString();
		assertNotNull(storeTerm);
		assertFalse("".equals(storeTerm));
		storedTerm = term;
	}
	
	@Test (groups = {"getters"}, dependsOnGroups = {"creators"})
	public void getTermTest(){
		Term term;
		try {
			term = instance.getTerm(storedTerm.getURI());
			assertEquals(term, storedTerm);
		} catch (ResourceNotFoundException e) {
			logger.severe(e.getMessage());
			fail(e.getMessage());
		} catch (IllegalTypeException e) {
			logger.severe(e.getMessage());
			fail(e.getMessage());
		}
	}
	
	@Test (groups = {"getters"}, dependsOnGroups = {"creators"})
	public void getTermsForTaggingTest(){
		List<Term> terms = instance.getTermsForTagging(taggingURI);
		assertNotNull(terms);
		assertTrue(terms.size() > 0);
		assertTrue(terms.size() == 1);
	}
	
	
	@Test (groups = {"deleters"}, dependsOnGroups = {"creators", "getters"})
	public void deleteTermTest(){
		instance.deleteTerm(storedTerm);
		try {
			instance.getTerm(storedTerm.getURI());
			fail("Resource found after being removed!!");
		} catch (ResourceNotFoundException e) {
			//valid exception
		} catch (IllegalTypeException e) {
			//valid exception
		}
	}
	
	
}
