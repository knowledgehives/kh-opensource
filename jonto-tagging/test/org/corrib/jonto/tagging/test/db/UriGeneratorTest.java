/**
 * 
 */
package org.corrib.jonto.tagging.test.db;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.beans.CircleROI;
import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.Excerpt;
import org.corrib.jonto.tagging.beans.RectangleROI;
import org.corrib.jonto.tagging.beans.Tagging;
import org.corrib.jonto.tagging.beans.Term;
import org.corrib.jonto.tagging.beans.TermType;
import org.corrib.jonto.tagging.db.UriGenerator;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @author Jakub Demczuk
 *
 */
public class UriGeneratorTest {
	
	String taggingUri1;
	String taggingUri2;
	String docUri1 = "http://document1.pl";
	String docUri2 = "http://document2.pl";
	String tagger1 = "mailto:admin@foafrealm.org";
	String tagger2 = "mailto:testowa@testowow.com";
	
	String serviceAddress2 = "http://localhost:8787/";
	
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.test.db.logic.TermDbLogicTest");
	
	private String serviceAddress;
	
	private String documentURI;
	int x,y;
	private Clipping clipping;

	private String description;

	private String taggerURI;

	private String title;

	private int radius;
	
	private List<Term> terms;
	
	@BeforeClass
	public void init(){
		serviceAddress = "http://localhost:7390";
		documentURI = "http://testURIdocument.com";
		description = "testing database tagging";
		taggerURI = "mailto:tagger@test.com";
		title = "Testing title";
		x = 44;
		y = 99;
		radius = 123;
		CircleROI circle = new CircleROI();
		circle.setRadius(radius);
		circle.setXCoordinate(x);
		circle.setYCoordinate(y);
		clipping = circle; 
		
		terms = new ArrayList<Term>();
		String tag = "tag";
		Term term = new Term();
		term.setTag(tag);
		term.setTermType(TermType.ACTION_TERM);
		
		Term term2 = new Term();
		term2.setTag("tag2");
		term2.setTermType(TermType.SETTING_TERM);
		terms.add(term);
		terms.add(term2);
	}
	
	
	@Test
	public void testGetUriTagging(){
		Date date = new Date();
		
		Tagging tag1 = new Tagging(docUri1,tagger1);
		tag1.setCreationTime(date);
		
		Tagging tag2 = new Tagging(docUri1,tagger2);
		tag2.setCreationTime(date);
		
		Tagging tag3 = new Tagging(docUri2,tagger1);
		tag3.setCreationTime(date);
		
		Tagging tag4 = new Tagging(docUri1,tagger1);
		Date date2 = new Date();
		long a = date2.getTime() + 123154;
		date2.setTime(a);
		tag4.setCreationTime(date2);
		String uri1 = UriGenerator.getUri(tag1, serviceAddress).toString();
		String uri2 = UriGenerator.getUri(tag2, serviceAddress).toString();
		String uri3 = UriGenerator.getUri(tag3, serviceAddress).toString();
		String uri4 = UriGenerator.getUri(tag4, serviceAddress).toString();
		String uri5 = UriGenerator.getUri(tag2, serviceAddress).toString();
		String uri6 = UriGenerator.getUri(tag1, serviceAddress2).toString();
		System.out.println(uri1);
		assertFalse(uri1.equals(uri2));
		assertFalse(uri1.equals(uri3));
		assertFalse(uri1.equals(uri4));
		assertFalse(uri2.equals(uri3));
		assertFalse(uri2.equals(uri4));
		assertFalse(uri3.equals(uri4));
		assertTrue(uri2.equals(uri5));
		assertFalse(uri1.equals(uri6));
		
		taggingUri1 = uri1;
		taggingUri2 = uri6;
	}
	
	
	@Test (dependsOnMethods = {"testGetUriTagging"})
	public void testGetUriClipping(){
		int _x = 120;
		int _y = 154;
		int _radius = 76;
		int height = 43;
		int width = 34;
		
		long length = 12;
		long start = 1231843; 
		CircleROI circle1 = new CircleROI();
		circle1.setTaggingURI(taggingUri1);
		circle1.setXCoordinate(_x);
		circle1.setYCoordinate(_y);
		circle1.setRadius(_radius);
		
		RectangleROI rec1 = new RectangleROI();
		rec1.setTaggingURI(taggingUri1);
		rec1.setHeight(height);
		rec1.setWidth(width);
		
		Excerpt excerpt1 = new Excerpt();
		excerpt1.setTaggingURI(taggingUri1);
		excerpt1.setStartPoint(start);
		excerpt1.setLength(length);
		
		
		String uri1 = UriGenerator.getUri(circle1, serviceAddress).toString();
		circle1.setRadius(_radius - 12);
		String uri2 = UriGenerator.getUri(circle1, serviceAddress).toString();
		String uri3 = UriGenerator.getUri(rec1, serviceAddress).toString();
		rec1.setHeight(height + 123);
		String uri4 = UriGenerator.getUri(rec1, serviceAddress).toString();
		
		String uri5 = UriGenerator.getUri(excerpt1, serviceAddress).toString();
		excerpt1.setStartPoint(888);
		String uri6 = UriGenerator.getUri(excerpt1, serviceAddress).toString();
		
		System.out.println(uri1);
		assertFalse(uri1.equals(uri2));
		assertFalse(uri1.equals(uri3));
		assertFalse(uri1.equals(uri5));
		assertFalse(uri3.equals(uri4));
		assertFalse(uri4.equals(uri5));
		assertFalse(uri5.equals(uri6));
		
	}
	
	@Test (dependsOnMethods = {"testGetUriTagging"})
	public void testGetUriTerm(){
		String tag1 = "tag1";
		String tag2 = "tag5";
		Term term1 = new Term();
		term1.setTaggingURI(taggingUri1);
		term1.setTag(tag1);
		term1.setTermType(TermType.SIMPLE_TERM);
		
		Term term2 = new Term();
		term2.setTaggingURI(taggingUri1);
		term2.setTag(tag2);
		term2.setTermType(TermType.SIMPLE_TERM);
		
		Term term3 = new Term();
		term3.setTaggingURI(taggingUri1);
		term3.setTag(tag1);
		term3.setTermType(TermType.AGENT_TERM);
		
		
		Term term4 = new Term();
		term4.setTaggingURI(taggingUri2);
		term4.setTag(tag2);
		term4.setTermType(TermType.SIMPLE_TERM);
		
		
		String uri1 = UriGenerator.getUri(term1, serviceAddress).toString();
		String uri2 = UriGenerator.getUri(term2, serviceAddress).toString();
		String uri3 = UriGenerator.getUri(term3, serviceAddress).toString();
		String uri4 = UriGenerator.getUri(term4, serviceAddress).toString();
		String uri5 = UriGenerator.getUri(term1, serviceAddress2).toString();
		String uri6 = UriGenerator.getUri(term2, serviceAddress2).toString();
		
		System.out.println(uri1);
		assertFalse(uri1.equals(uri2));
		assertFalse(uri1.equals(uri3));
		assertFalse(uri1.equals(uri4));
		assertFalse(uri1.equals(uri5));
		assertFalse(uri1.equals(uri6));
		assertFalse(uri2.equals(uri3));
		assertFalse(uri2.equals(uri3));
		assertFalse(uri2.equals(uri4));
		assertFalse(uri2.equals(uri5));
		assertFalse(uri2.equals(uri6));
		assertFalse(uri3.equals(uri4));
		assertFalse(uri3.equals(uri5));
		assertFalse(uri3.equals(uri6));
		assertFalse(uri4.equals(uri5));
		assertFalse(uri4.equals(uri6));
		assertFalse(uri5.equals(uri6));
		
		
	}
	
	@Test
	public void taggingURITest(){
		Tagging tagging = new Tagging();
		tagging.setCreationTime(new Date());
		tagging.setDocumentURI(documentURI);
		tagging.setDescription(description);
		tagging.setTaggerURI(taggerURI);
		tagging.setShared(true);
		tagging.setTitle(title);
		tagging.setTerms(terms);
		tagging.setClipping(clipping);
		
		String uri = UriGenerator.getUri(tagging, serviceAddress).toString();
		System.out.println(uri);
		Tagging tagging2 = new Tagging();
		tagging2.setCreationTime(new Date());
		tagging2.setDocumentURI(documentURI);
		tagging2.setDescription(description);
		tagging2.setTaggerURI(taggerURI);
		tagging2.setShared(true);
		tagging2.setTitle(title);
		String uri2 = UriGenerator.getUri(tagging2, serviceAddress).toString();
		System.out.println(uri2);
		assertFalse(uri2.equals(uri));
	}
}
