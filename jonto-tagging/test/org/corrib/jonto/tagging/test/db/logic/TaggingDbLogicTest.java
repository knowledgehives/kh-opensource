/**
 * 
 */
package org.corrib.jonto.tagging.test.db.logic;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.beans.CircleROI;
import org.corrib.jonto.tagging.beans.Clipping;
import org.corrib.jonto.tagging.beans.RectangleROI;
import org.corrib.jonto.tagging.beans.Tagging;
import org.corrib.jonto.tagging.beans.Term;
import org.corrib.jonto.tagging.beans.TermType;
import org.corrib.jonto.tagging.db.exceptions.IllegalTypeException;
import org.corrib.jonto.tagging.db.exceptions.ResourceNotFoundException;
import org.corrib.jonto.tagging.db.logic.ClippingDbLogic;
import org.corrib.jonto.tagging.db.logic.TaggingDbLogic;
import org.corrib.jonto.tagging.db.logic.TermDbLogic;
import org.ontoware.rdf2go.model.node.URI;
import org.ontoware.rdf2go.model.node.impl.URIImpl;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @author Jakub Demczuk
 *
 */
public class TaggingDbLogicTest {

	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.test.db.logic.TermDbLogicTest");
	
	private TaggingDbLogic instance;
	private ClippingDbLogic clippingInstance;
	
	private List<Tagging> taggings;

	private String serviceAddress;
	
	private URI documentURI;
	private URI taggerURI;
	private URI taggerURI2;
	
	int x,y;
	private CircleROI circle;
	private RectangleROI rec;

	private String description;


	private String title;

	private int radius;
	
	private List<Term> terms;
	
	private List<URI> links;
	private List<URI> references;

	private TermDbLogic termInstance;

	
	int taggerURITermsCounter = 0;
	int termsCounter = 0;
	int documentURITaggerURITermsCounter = 0;
	int documentURITaggings = 0;
	int taggerURITaggins = 0;
	int documentURITaggerURITaggings = 0;
	
	@BeforeClass
	public void init(){
		instance = new TaggingDbLogic();
		clippingInstance = new ClippingDbLogic();
		termInstance = new TermDbLogic();
		taggings = new ArrayList<Tagging>();
		serviceAddress = "http://localhost:7390/";
		documentURI = new URIImpl("http://testURIdocument.com");
		description = "testing database tagging";
		taggerURI = new URIImpl("mailto:tagger@test.com");
		taggerURI2 = new URIImpl("mailto:tagger2@test.com");
		title = "Testing title";
		x = 44;
		y = 99;
		radius = 123;
		circle = new CircleROI();
		circle.setRadius(radius);
		circle.setXCoordinate(x);
		circle.setYCoordinate(y);
		
		int width = 100;
		int height = 123;
		rec = new RectangleROI();
		rec.setXCoordinate(x);
		rec.setYCoordinate(y);
		rec.setHeight(height);
		rec.setWidth(width);
		
		
		
		
		
		links = new ArrayList<URI>();
		links.add(new URIImpl("http://www.wp.pl"));
		links.add(new URIImpl("http://www.wp1.pl"));
		links.add(new URIImpl("http://www.wp2.pl"));
		
		references = new ArrayList<URI>();
	}
	
	@Test (groups = {"creators"})
	public void storeTaggingTest(){
		
		terms = new ArrayList<Term>();
		String tag = "tag";
		Term term = new Term();
		term.setTag(tag);
		term.setTermType(TermType.ACTION_TERM);
		
		Term term2 = new Term();
		term2.setTag("tag2");
		term2.setTermType(TermType.SETTING_TERM);
		terms.add(term);
		terms.add(term2);
		
		
		Tagging tagging = new Tagging();
		tagging.setCreationTime(new Date());
		tagging.setDocumentURI(documentURI);
		tagging.setDescription(description);
		tagging.setTaggerURI(taggerURI);
		tagging.setShared(true);
		tagging.setTitle(title);
		tagging.addTerms(terms);
		termsCounter += terms.size();
		taggerURITermsCounter += terms.size();
		documentURITaggerURITermsCounter += terms.size();
		documentURITaggings++;
		taggerURITaggins++;
		documentURITaggerURITaggings++;
		tagging.setClipping(circle);
		tagging.setLinks(links);
		
		Tagging tagging2 = new Tagging();
		tagging2.setCreationTime(new Date());
		tagging2.setDocumentURI(documentURI);
		tagging2.setDescription(description);
		tagging2.setTaggerURI(taggerURI);
		tagging2.setShared(true);
		tagging2.setTitle(title);
		documentURITaggings++;
		taggerURITaggins++;
		documentURITaggerURITaggings++;
		
		terms = new ArrayList<Term>();
		tag = "tag";
		term = new Term();
		term.setTag(tag);
		term.setTermType(TermType.ACTION_TERM);
		term2 = new Term();
		term2.setTag("tag2");
		term2.setTermType(TermType.SETTING_TERM);
		terms.add(term);
		terms.add(term2);
		
		
		Tagging tagging3 = new Tagging();
		tagging3.setCreationTime(new Date());
		tagging3.setDocumentURI(documentURI);
		tagging3.setDescription(description);
		tagging3.setTaggerURI(taggerURI2);
		tagging3.setShared(true);
		tagging3.setTitle(title);
		tagging3.addTerms(terms);
		termsCounter += terms.size();
		tagging3.setClipping(rec);
		documentURITaggings++;
		String taggingURI = instance.addTagging(tagging, serviceAddress).toString();
		assertNotNull(taggingURI);
		assertFalse("".equals(taggingURI));
		
		
		taggingURI = instance.addTagging(tagging2, serviceAddress).toString();
		assertNotNull(taggingURI);
		assertFalse("".equals(taggingURI));
		
		references.add(tagging.getURI());
		references.add(tagging2.getURI());
		tagging3.setCrossReferences(references);
		taggingURI = instance.addTagging(tagging3, serviceAddress).toString();
		assertNotNull(taggingURI);
		assertFalse("".equals(taggingURI));
		
		
		taggings.add(tagging);
		taggings.add(tagging2);
		taggings.add(tagging3);
	}
	@Test (groups = {"creators"}, dependsOnMethods = {"storeTaggingTest"})
	public void updateTaggingTest(){
		Tagging tagging = taggings.get(0);
		tagging.setTitle("new title");
		Term termOld = terms.get(0);
		termOld.setTaggingURI(tagging.getURI());
		terms = new ArrayList<Term>();
		
		Term term = new Term();
		term.setTag("afterUpdate");
		term.setTermType(TermType.OBJECT_TERM);
		
		Term term2 = new Term();
		term2.setTag("afterUpdate2");
		term2.setTermType(TermType.SIMPLE_TERM);
		terms.add(term);
		terms.add(term2);
		terms.add(termOld);
		termsCounter += terms.size();
		taggerURITermsCounter += terms.size();
		documentURITaggerURITermsCounter += terms.size();
		
		termsCounter -= tagging.getTerms().size();
		taggerURITermsCounter -= tagging.getTerms().size();
		documentURITaggerURITermsCounter -= tagging.getTerms().size();
		tagging.setTerms(terms);
		tagging.setDescription("new desc");
		try {
			instance.updateTagging(tagging,serviceAddress);
		} catch (ResourceNotFoundException e1) {
			fail(e1.getClass() + "\n" + e1.getMessage());
		} catch (IllegalTypeException e1) {
			fail(e1.getClass() + "\n" + e1.getMessage());
		}
		
		try {
			Tagging tagging2 = instance.getTagging(tagging.getURI());
			assertEquals(tagging2.getURI(), tagging.getURI());
		} catch (ResourceNotFoundException e) {
			fail(e.getMessage());
		} catch (IllegalTypeException e) {
			fail(e.getMessage());
		}
	}
	
	@Test(groups = {"getters"}, dependsOnGroups = "creators")
	public void getTaggingTest(){
		for(Tagging tagging : taggings){
			Tagging tagging2;
			try {
				tagging2 = instance.getTagging(tagging.getURI());
				assertNotNull(tagging2);
				assertEquals(tagging2.getURI(), tagging.getURI());
				assertEquals(tagging2.getDescription(), tagging.getDescription());
				assertEquals(tagging2.getTitle(), tagging.getTitle());
			} catch (ResourceNotFoundException e) {
				fail(e.getMessage());
			} catch (IllegalTypeException e) {
				fail(e.getMessage());
			}
		}
	}
	
	@Test(groups = {"getters"}, dependsOnGroups = "creators")
	public void getClippingForTaggingTest(){
		Clipping result = clippingInstance.getClippingForTagging(taggings.get(0).getURI());
		assertNotNull(result);
		assertEquals(result.getURI(), taggings.get(0).getClipping().getURI());
		
		Clipping result2 = clippingInstance.getClippingForTagging(taggings.get(1).getURI());
		assertNull(result2);
	}
	
	@Test(groups = {"getters"}, dependsOnGroups = "creators")
	public void getClippingsForDocumentTest(){
		List<Clipping> result = clippingInstance.getClippingsForDocument(documentURI);
		assertNotNull(result);
		assertTrue(result.size() == 2);
	}
	
	@Test(groups = {"getters"}, dependsOnGroups = "creators")
	public void getClippingsForTaggerTest(){
		List<Clipping> result = clippingInstance.getClippingsForTagger(taggerURI);
		assertNotNull(result);
		assertTrue(result.size() == 1);
	}
	
	@Test(groups = {"getters"}, dependsOnGroups = "creators")
	public void getClippingsForDocumentTaggerTest(){
		List<Clipping> result = clippingInstance.getClippingsForDocumentTagger(documentURI, taggerURI);
		assertNotNull(result);
		assertTrue(result.size() == 1);
	}
	@Test(groups = {"getters"}, dependsOnGroups = "creators")
	public void getTermsForDocumentTaggerTest(){
		List<Term> result = termInstance.getTermsForDocumentTagger(documentURI, taggerURI);
		assertNotNull(result);
		assertTrue(result.size() == documentURITaggerURITermsCounter);
	}
	
	@Test(groups = {"getters"}, dependsOnGroups = "creators")
	public void getTermsForDocumentTest(){
		List<Term> result = termInstance.getTermsForDocument(documentURI);
		assertNotNull(result);
		assertTrue(result.size() == termsCounter);
	}
	
	@Test(groups = {"getters"}, dependsOnGroups = "creators")
	public void getTaggingsForDocumentTest(){
		List<Tagging> result = instance.getTaggingsForDocument(documentURI);
		assertNotNull(result);
		assertTrue(result.size() == documentURITaggings);
	}
	
	@Test(groups = {"getters"}, dependsOnGroups = "creators")
	public void getTaggingsForTaggerTest(){
		List<Tagging> result = instance.getTaggingsForTagger(taggerURI);
		assertNotNull(result);
		assertTrue(result.size() == taggerURITaggins);
	}
	
	@Test(groups = {"getters"}, dependsOnGroups = "creators")
	public void getTaggingsForDocumentTaggerTest(){
		List<Tagging> result = instance.getTaggingsForDocumentTagger(documentURI, taggerURI);
		assertNotNull(result);
		assertTrue(result.size() == documentURITaggerURITaggings);
	}
	
	@Test (groups = {"getters"}, dependsOnGroups = {"creators"})
	public void getTermsForTaggerTest(){
		List<Term> result = termInstance.getTermsForTagger(taggerURI);
		assertNotNull(result);
		assertTrue(result.size() == taggerURITermsCounter);
	}
	
	@Test (groups = {"deleters"}, dependsOnGroups = {"creators","getters"})
	public void deleteTaggingTest(){
		for(Tagging tagging : taggings){
			try {
				instance.deleteTagging(tagging);
				instance.getTagging(tagging.getURI());
				fail("Tagging found after being deleted: " + tagging.getURI());
			} catch (ResourceNotFoundException e) {
				//valid exception
			} catch (IllegalTypeException e) {
				//valid exception
			}
		}
	}
}
