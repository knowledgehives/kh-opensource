/**
 * 
 */
package org.corrib.jonto.tagging.test;

import static org.testng.Assert.assertTrue;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.corrib.jonto.tagging.GlobalTools;
import org.corrib.jonto.tagging.db.RDF2GoLoader;
import org.ontoware.aifbcommons.collection.ClosableIterator;
import org.ontoware.rdf2go.model.Model;
import org.ontoware.rdf2go.model.Statement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

/**
 * @author Jakub Demczuk
 *
 */
public class TestInit {
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.test.TestInit");
	
	@BeforeSuite(alwaysRun=true)
	public void init(){
		RDF2GoLoader.setTest(true);
	}
	
	@AfterSuite(alwaysRun=true)
	public void shutdown(){
		
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		boolean empty = model.isEmpty();
		StringBuilder sb = new StringBuilder();
		sb.append("\n");
		if(!empty){
			ClosableIterator<Statement> statements = model.findStatements(null, null, null);
			while(statements.hasNext()){
				sb.append(statements.next());
				sb.append("\n");
			}
			statements.close();
		}
		model.close();
		RDF2GoLoader.setTest(false);
//		File testDir = new File(GlobalTools.getInstance().getTestRepositoryPath());
//		for(File file : testDir.listFiles()){
//			file.delete();
//		}
//		testDir.delete();
		logger.log(Level.INFO, "REPO DUMP :\n" + sb.toString());
		assertTrue(empty, sb.toString());
	}
}
