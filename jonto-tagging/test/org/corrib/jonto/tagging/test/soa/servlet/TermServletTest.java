package org.corrib.jonto.tagging.test.soa.servlet;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.corrib.jonto.tagging.beans.Term;
import org.corrib.jonto.tagging.beans.TermType;
import org.corrib.jonto.tagging.db.RDF2GoLoader;
import org.corrib.jonto.tagging.soa.rest.input.XML_RESTInput;
import org.corrib.jonto.tagging.soa.rest.input.exceptions.MalformedStringException;
import org.corrib.jonto.tagging.soa.rest.output.XML_RESTOutput;
import org.ontoware.rdf2go.model.Model;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TermServletTest {
	private static final Logger logger = Logger.getLogger("org.s3b.multimediaterm.test.soa.servlet.TaggingServletTest");
	private HttpClient httpClient;
	private String termXML;
	private Term term;
	private static final String serverURL = "http://localhost:7390/mmt/Term/";
	private static final String responseFormat = "responseFormat=xml";
	private static final String taggingURI = "http://taggingURIasdd.com";

	@BeforeClass
	public void init(){
		httpClient = new HttpClient();
		
		term = new Term("");
		term.setTag("tahsfalsfas");
		term.setTaggingURI(taggingURI);
		term.setTermType(TermType.SETTING_TERM);
	}
	
	@Test
	public void doPostTest(){
		try{
			PostMethod post = new PostMethod(serverURL);
			post.addParameter("responseFormat", "xml");
			post.addParameter("requestFormat", "xml");
			XML_RESTOutput out = new XML_RESTOutput();
			termXML = out.getOutputText(term);
			logger.log(Level.INFO, "__ " + termXML.toString());
			post.addParameter("term", termXML);
			httpClient.executeMethod(post);

			String newClippingXML = post.getResponseBodyAsString();
			logger.log(Level.INFO, "" + newClippingXML);
			term = new XML_RESTInput().parseTerm(newClippingXML);
		} catch (MalformedStringException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (HttpException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		}
	}
	
	@Test (dependsOnMethods={"doPostTest"})
	public void doGetTest(){
		String encodedURI;
		try {
			encodedURI = URLEncoder.encode(term.getURI().toString(), "UTF-8");
			GetMethod get = new GetMethod(serverURL + encodedURI+"?"+responseFormat);
			logger.log(Level.INFO, "GET log " + serverURL + encodedURI+"?"+responseFormat);
			int status = httpClient.executeMethod(get);
			assertTrue(status == HttpServletResponse.SC_OK);
			logger.log(Level.INFO, get.getResponseBodyAsString());
		} catch (UnsupportedEncodingException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (HttpException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		}
	}
	
	@Test (dependsOnMethods={"doPostTest","doGetTest"})
	public void doPostUpdateTest(){
		try{
			PostMethod post = new PostMethod(serverURL);
			post.addParameter("responseFormat", "xml");
			post.addParameter("requestFormat", "xml");
			XML_RESTOutput out = new XML_RESTOutput();
			term.setTag("taggggggooooogggggooo");
			termXML = out.getOutputText(term);
			post.addParameter("term", termXML);
			httpClient.executeMethod(post);

			String sUpdatedTerm = post.getResponseBodyAsString();
			logger.log(Level.INFO, "" + sUpdatedTerm);
			String oldURI = term.getURI().toString();
			term = new XML_RESTInput().parseTerm(sUpdatedTerm);
			String newURI = term.getURI().toString();
			
			assertEquals(newURI, oldURI);
			String encodedURI = URLEncoder.encode(term.getURI().toString(), "UTF-8");
			GetMethod get = new GetMethod(serverURL + encodedURI+"?"+responseFormat);
			int status = httpClient.executeMethod(get);
			assertTrue(status == HttpServletResponse.SC_OK);
			String sGetTermAfterUpdate = get.getResponseBodyAsString();
			logger.log(Level.INFO, sGetTermAfterUpdate);
			
			assertEquals(sGetTermAfterUpdate, sUpdatedTerm);
		} catch (MalformedStringException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (HttpException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		}
	}
	
	@Test (dependsOnMethods={"doPostTest","doPostUpdateTest","doGetTest"})
	public void doDeleteTest(){
		String encodedURI;
		try {
			encodedURI = URLEncoder.encode(term.getURI().toString(), "UTF-8");
			DeleteMethod delete = new DeleteMethod(serverURL + encodedURI+"?"+responseFormat);
			int status = httpClient.executeMethod(delete);
			logger.log(Level.INFO, delete.getResponseBodyAsString());
			assertTrue(status == HttpServletResponse.SC_OK);
		} catch (UnsupportedEncodingException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (HttpException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		}
	}
	
	@AfterClass
	public void dumpRepo(){
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		if(model.isEmpty()){
			model.close();
		}else{
//			model.dump();
			model.close();
			fail();
		}
		
	}
}
