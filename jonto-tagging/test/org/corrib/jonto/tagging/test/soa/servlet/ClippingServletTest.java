package org.corrib.jonto.tagging.test.soa.servlet;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.corrib.jonto.tagging.beans.RectangleROI;
import org.corrib.jonto.tagging.db.RDF2GoLoader;
import org.corrib.jonto.tagging.soa.rest.input.XML_RESTInput;
import org.corrib.jonto.tagging.soa.rest.input.exceptions.MalformedStringException;
import org.corrib.jonto.tagging.soa.rest.output.XML_RESTOutput;
import org.ontoware.rdf2go.model.Model;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ClippingServletTest {
	private static final Logger logger = Logger.getLogger("org.s3b.multimediaclipping.test.soa.servlet.TaggingServletTest");
	private HttpClient httpClient;
	private String clippingXML;
	private RectangleROI clipping;
	private static final String serverURL = "http://localhost:7390/mmt/Clipping/";
	private static final String responseFormat = "responseFormat=xml";
	private static final String taggingURI = "http://taggingURIasdd.com";
	

	@BeforeClass
	public void init(){
		httpClient = new HttpClient();
		
		clipping = new RectangleROI("");
		clipping.setTaggingURI(taggingURI);
		clipping.setXCoordinate(1235);
		clipping.setYCoordinate(653);
		clipping.setWidth(235);
		clipping.setHeight(1412);
	}
	
	@Test
	public void doPostTest(){
		try{
			PostMethod post = new PostMethod(serverURL);
			post.addParameter("responseFormat", "xml");
			post.addParameter("requestFormat", "xml");
			XML_RESTOutput out = new XML_RESTOutput();
			clippingXML = out.getOutputText(clipping);
			post.addParameter("clipping", clippingXML);
			httpClient.executeMethod(post);

			String newClippingXML = post.getResponseBodyAsString();
			logger.log(Level.INFO, "" + newClippingXML);
			clipping = (RectangleROI) new XML_RESTInput().parseClipping(newClippingXML);
		} catch (MalformedStringException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (HttpException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		}
	}
	
	@Test (dependsOnMethods={"doPostTest"})
	public void doGetTest(){
		String encodedURI;
		try {
			encodedURI = URLEncoder.encode(clipping.getURI().toString(), "UTF-8");
			GetMethod get = new GetMethod(serverURL + encodedURI+"?"+responseFormat);
			int status = httpClient.executeMethod(get);
			assertTrue(status == HttpServletResponse.SC_OK);
			logger.log(Level.INFO, get.getResponseBodyAsString());
		} catch (UnsupportedEncodingException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (HttpException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		}
	}
	
	@Test (dependsOnMethods={"doPostTest","doGetTest"})
	public void doPostUpdateTest(){
		try{
			PostMethod post = new PostMethod(serverURL);
			post.addParameter("responseFormat", "xml");
			post.addParameter("requestFormat", "xml");
			XML_RESTOutput out = new XML_RESTOutput();
			clipping.setWidth(13431);
			clipping.setHeight(1933);
			clippingXML = out.getOutputText(clipping);
			post.addParameter("clipping", clippingXML);
			httpClient.executeMethod(post);

			String sUpdatedClipping = post.getResponseBodyAsString();
			logger.log(Level.INFO, "" + sUpdatedClipping);
			String oldURI = clipping.getURI().toString();
			clipping = (RectangleROI) new XML_RESTInput().parseClipping(sUpdatedClipping);
			String newURI = clipping.getURI().toString();
			
			assertEquals(newURI, oldURI);
			String encodedURI = URLEncoder.encode(clipping.getURI().toString(), "UTF-8");
			GetMethod get = new GetMethod(serverURL + encodedURI+"?"+responseFormat);
			int status = httpClient.executeMethod(get);
			assertTrue(status == HttpServletResponse.SC_OK);
			String sGetTaggingAfterUpdate = get.getResponseBodyAsString();
			logger.log(Level.INFO, sGetTaggingAfterUpdate);
			
			assertEquals(sGetTaggingAfterUpdate, sUpdatedClipping);
		} catch (MalformedStringException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (HttpException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		}
	}
	
	@Test (dependsOnMethods={"doPostTest","doPostUpdateTest","doGetTest"})
	public void doDeleteTest(){
		String encodedURI;
		try {
			encodedURI = URLEncoder.encode(clipping.getURI().toString(), "UTF-8");
			DeleteMethod delete = new DeleteMethod(serverURL + encodedURI+"?"+responseFormat);
			int status = httpClient.executeMethod(delete);
			logger.log(Level.INFO, delete.getResponseBodyAsString());
			assertTrue(status == HttpServletResponse.SC_OK);
		} catch (UnsupportedEncodingException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (HttpException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		}
	}
	
	@AfterClass
	public void dumpRepo(){
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		if(model.isEmpty()){
			model.close();
		}else{
//			model.dump();
			model.close();
			fail();
		}
		
	}
}
