/**
 * 
 */
package org.corrib.jonto.tagging.test.soa.servlet;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.corrib.jonto.tagging.beans.CircleROI;
import org.corrib.jonto.tagging.beans.Tagging;
import org.corrib.jonto.tagging.beans.Term;
import org.corrib.jonto.tagging.beans.TermType;
import org.corrib.jonto.tagging.db.RDF2GoLoader;
import org.corrib.jonto.tagging.soa.rest.input.XML_RESTInput;
import org.corrib.jonto.tagging.soa.rest.input.exceptions.MalformedStringException;
import org.corrib.jonto.tagging.soa.rest.output.XML_RESTOutput;
import org.ontoware.rdf2go.model.Model;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @author Jakub Demczuk
 *
 */
public class TaggingServletTest {

	private static final Logger logger = Logger.getLogger("org.corrib.jonto.tagging.test.soa.servlet.TaggingServletTest");
	private HttpClient httpClient;
	private Tagging tagging;
	private String taggingXML;
	private static final String serverURL = "http://localhost:7390/mmt/Tagging";
	private static final String responseFormat = "responseFormat=xml";
	

	@BeforeClass
	public void init(){
		httpClient = new HttpClient();
		tagging = new Tagging("");
		CircleROI clipping;
		clipping = new CircleROI("");
		clipping.setXCoordinate(1235);
		clipping.setYCoordinate(653);
		clipping.setRadius(235);
		
		Term term = new Term();
		term.setURI("");
		term.setTag("adsss");
		term.setTermType(TermType.SIMPLE_TERM);
		
		tagging.addTerm(term);
		tagging.addCrossReference("http://asdalsf.pl");
		tagging.addLink("http://testowy.com");
		tagging.setClipping(clipping);
		tagging.setDocumentURI("http://document.asssx");
		tagging.setTaggerURI("http:/tagger.uri.com");
		
		
	}
	
	@Test
	public void doPostTest(){
		try{
			PostMethod post = new PostMethod(serverURL);
			post.addParameter("responseFormat", "xml");
//			post.addParameter("requestFormat", "xml");
			XML_RESTOutput out = new XML_RESTOutput();
			taggingXML = out.getOutputText(tagging);
			logger.log(Level.INFO, "" + taggingXML);
			post.addParameter("tagging", taggingXML);
			httpClient.executeMethod(post);

			String newTaggingXML = post.getResponseBodyAsString();
			logger.log(Level.INFO, "" + newTaggingXML);
			tagging = new XML_RESTInput().parseTagging(newTaggingXML);
		} catch (MalformedStringException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (HttpException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		}
	}
	
	
	@Test (dependsOnMethods={"doPostTest"})
	public void doGetTest(){
		String encodedURI;
		try {
			encodedURI = URLEncoder.encode(tagging.getURI().toString(), "UTF-8");
			GetMethod get = new GetMethod(serverURL + encodedURI+"?"+responseFormat);
			int status = httpClient.executeMethod(get);
			assertTrue(status == HttpServletResponse.SC_OK);
			logger.log(Level.INFO, get.getResponseBodyAsString());
		} catch (UnsupportedEncodingException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (HttpException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		}
	}
/*	
	@Test (dependsOnMethods={"doPostTest"})
	public void doGetByDocumentTest(){
		try {
			GetMethod get = new GetMethod(serverURL + "?"+responseFormat + "&documentURI="+tagging.getDocumentURI());
			logger.log(Level.INFO, serverURL + "?"+responseFormat + "&documentURI="+tagging.getDocumentURI());
			int status = httpClient.executeMethod(get);
			assertTrue(status == HttpServletResponse.SC_OK);
			logger.log(Level.INFO, get.getResponseBodyAsString());
		} catch (UnsupportedEncodingException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (HttpException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		}
	}
*/	
	@Test //(dependsOnMethods={"doPostTest","doGetTest"})
	public void doPostUpdateTest(){
		logger.log(Level.INFO, "dupa");
		try{
			PostMethod post = new PostMethod(serverURL);
			post.addParameter("responseFormat", "xml");
			post.addParameter("requestFormat", "xml");
			XML_RESTOutput out = new XML_RESTOutput();
			tagging.setDescription("descripiton after update");
			tagging.setTitle("HAHAHAHA");
			taggingXML = out.getOutputText(tagging);
			post.addParameter("tagging", taggingXML);
			httpClient.executeMethod(post);

			String sUpdatedTagging = post.getResponseBodyAsString();
			logger.log(Level.INFO, "" + sUpdatedTagging);
			String oldURI = tagging.getURI().toString();
			tagging = new XML_RESTInput().parseTagging(sUpdatedTagging);
			String newURI = tagging.getURI().toString();
			
			assertEquals(newURI, oldURI);
			String encodedURI = URLEncoder.encode(tagging.getURI().toString(), "UTF-8");
			GetMethod get = new GetMethod(serverURL + encodedURI+"?"+responseFormat);
			int status = httpClient.executeMethod(get);
			assertTrue(status == HttpServletResponse.SC_OK);
			String sGetTaggingAfterUpdate = get.getResponseBodyAsString();
			logger.log(Level.INFO, sGetTaggingAfterUpdate);
			
			assertEquals(sGetTaggingAfterUpdate, sUpdatedTagging);
		} catch (MalformedStringException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (HttpException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		}
	}
	
	@Test //(dependsOnMethods={"doPostTest","doPostUpdateTest","doGetTest"})
	public void doDeleteTest(){
		String encodedURI;
		try {
			encodedURI = URLEncoder.encode(tagging.getURI().toString(), "UTF-8");
			DeleteMethod delete = new DeleteMethod(serverURL + encodedURI+"?"+responseFormat);
			int status = httpClient.executeMethod(delete);
			logger.log(Level.INFO, delete.getResponseBodyAsString());
			assertTrue(status == HttpServletResponse.SC_OK);
		} catch (UnsupportedEncodingException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (HttpException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		} catch (IOException e) {
			logger.log(Level.SEVERE, e.getMessage(), e);
			fail();
		}
	}
	
	@AfterClass
	public void dumpRepo(){
		Model model = RDF2GoLoader.getInstance().getModel();
		model.open();
		if(model.isEmpty()){
			model.close();
		}else{
//			model.dump();
			model.close();
			fail();
		}
		
	}
	
}
