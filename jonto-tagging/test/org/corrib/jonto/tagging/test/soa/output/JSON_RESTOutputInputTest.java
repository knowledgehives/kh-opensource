/**
 * 
 */
package org.corrib.jonto.tagging.test.soa.output;

import org.corrib.jonto.tagging.beans.CircleROI;
import org.corrib.jonto.tagging.beans.Tagging;
import org.corrib.jonto.tagging.beans.Term;
import org.corrib.jonto.tagging.beans.TermType;
import org.corrib.jonto.tagging.db.UriGenerator;
import org.corrib.jonto.tagging.soa.rest.output.JSON_RESTOutput;
import org.corrib.jonto.tagging.soa.rest.output.RESTOutput;
import org.testng.annotations.Test;

/**
 * @author Jakub Demczuk
 *
 */
public class JSON_RESTOutputInputTest {

	
	@Test
	public void jsonRESTOutputInputTest(){
		RESTOutput rOut = new JSON_RESTOutput();
//		RESTInput rIn = new XML_RESTInput();
		Tagging tagging = new Tagging();
		CircleROI clipping = new CircleROI();
		clipping.setXCoordinate(1235);
		clipping.setYCoordinate(653);
		clipping.setRadius(235);
		
		Term term = new Term();
		term.setTag("adsss");
		term.setTermType(TermType.SIMPLE_TERM);
		
		
		
		String serviceAddress = "http://localhost:7390";
		tagging.setURI(UriGenerator.getUri(tagging, serviceAddress ));
		clipping.setURI(UriGenerator.getUri(clipping, serviceAddress));
		term.setURI(UriGenerator.getUri(term, serviceAddress));
		
		tagging.addTerm(term);
		tagging.addCrossReference("http://asdalsf.pl");
		tagging.addLink("http://testowy.com");
		tagging.setClipping(clipping);
		tagging.setDocumentURI("http://document.asss");
		tagging.setTaggerURI("http://tagger.uri.com");
		
		
		String taggingOut1 = rOut.getOutputText(tagging);
		System.out.println(taggingOut1);
		String clippingOut1 = rOut.getOutputText(clipping);
		String termOut1 = rOut.getOutputText(term);
		System.out.println(clippingOut1);
		System.out.println(termOut1);
//		try {
//			Tagging parsedTagging = rIn.parseTagging(taggingOut1);
//			Clipping parsedClipping = rIn.parseClipping(clippingOut1);
//			Term parsedTerm = rIn.parseTerm(termOut1);
//			assertNotNull(parsedTagging);
//			assertNotNull(parsedClipping);
//			assertNotNull(parsedTerm);
//			
//			String taggingOut2 = rOut.getOutputText(parsedTagging);
//			System.out.println(taggingOut2);
//			String clippingOut2 = rOut.getOutputText(parsedClipping);
//			String termOut2 = rOut.getOutputText(parsedTerm);
//			assertEquals(taggingOut2, taggingOut1);
//			assertEquals(clippingOut2, clippingOut1);
//			assertEquals(termOut2, termOut1);
//		} catch (MalformedStringException e) {
//			fail(e.getMessage());
//		}
		
	}
}
