<%@ tag language="java" pageEncoding="UTF-8" description="embeds flex animation" %>

<%@ attribute name="id" type="java.lang.String" required="true"%> 
<%@ attribute name="animationName" type="java.lang.String" required="true" %> 
<%@ attribute name="width" type="java.lang.String" required="false" %>
<%@ attribute name="height" type="java.lang.String" required="false" %>

<div id="${ id }_${ animationName }" class="flexContainer" style="width:${ width }; height:${ height };" >
    <!-- need to create this way instead of <div .. />
    otherwise swfobject makes some freaky errors during replacements in DOM -->     
</div>