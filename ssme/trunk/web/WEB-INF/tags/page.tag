<%@ tag language="java" pageEncoding="UTF-8" description="main template ssme" %>

<%@ taglib prefix="ssme" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="title" %> 
<%@ attribute name="header" fragment="true" %> 
<%@ attribute name="footer" fragment="true" %> 
<%@ attribute name="left" fragment="true" %> 
<%@ attribute name="right" fragment="true" %> 
<%@ attribute name="isLeft" required="false" rtexprvalue="true" type="java.lang.Boolean" %> 
<%@ attribute name="isRight" required="false" rtexprvalue="true" type="java.lang.Boolean" %> 
<%@ attribute name="isSimple" required="false" rtexprvalue="true" type="java.lang.Boolean" %>
<%@ attribute name="isMain" required="false" rtexprvalue="true" type="java.lang.Boolean" %>	

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="${sessionScope.lang}">

	<head>
		<ssme:head />
	</head>
	<body id="page_body">
		<c:choose>
			<c:when test="${ isSimple }">
				<jsp:doBody />
			</c:when>
			<c:otherwise>
				<!-- left -->	
				<c:if test="${ isLeft }">
					<div id="left_column">	
					
						<div id="header">
							<div class="logo"></div>
							<span class="app">SmartSchedule.me</span>
						</div>						

                        <div id="left_column_content">
                            <jsp:invoke fragment="left"/>                        						
                        </div>
                        
						<div id="footer">
							Copyright 2009-2011<br/>
                            <a href="http://www.knowledgehives.com/" target="_blank">KnowledgeHives.com</a>
						</div>								
					</div>
				</c:if>
				
				<!-- content -->	
				<div id="center_column" >							
					<div id="content">
	            		<jsp:doBody />					
					</div>
					                        	
				</div>
		
				
				<!-- right -->	
				<c:if test="${ isRight }">
					<div id="right_column">			
						<jsp:invoke fragment="right"/>
					</div>
				</c:if>	
				
				<!-- 
				<div id="loggerContainer">	
					<div style="padding: 5px 5px 5px 5px; border: 1px solid black;">
						<span style="border: 2px outset black; padding: 1px 5px 1px 5px;" onClick="$('logger').innerHTML = '';">Clear Log</span>
						[Method: ${pageContext.request.method}][Query String: ${pageContext.request.queryString}][Request URI: ${pageContext.request.requestURI}]
						[Request URL: ${pageContext.request.requestURL}][Servlet Path: ${pageContext.request.servletPath}][Context Path: ${pageContext.request.contextPath}]					
					</div>
					<div id="logger" />	
				</div>
				 -->

			</c:otherwise>
		</c:choose>
	</body>


</html>