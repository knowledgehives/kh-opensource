/** 
 * JS ToggleButton class
 */
ToggleButton = Class.create();

ToggleButton.prototype = {
		
	stateIsOn : null,
	
	element : null,
	
	handlerEnabledFlag : null,	
	
	initialize: function(toggleElement){
		Logger.debug("ToggleButton.initialize()");	
		this.stateIsOn = false;
		this.element = toggleElement;
		Element.addClassName(this.element,'toggleOff');
		this.handlerEnabledFlag = true;
		
		//$(this.element).onmousedown = function(e){ return false; };			
		$(this.element).onclick = this.click_handler.bindAsEventListener(this);	

		toggleElement.toggle = this;
	},

	/**
	 * click event handling - toggling button on/off
	 */
	click_handler: function(event) {
		Logger.debug("ToggleButton.click_handler() stateIsOn:[" + this.stateIsOn + "]");	
		if(this.handlerEnabledFlag){
			handlerEnabledFlag = false;
			if(this.stateIsOn){
				//switch from ON to OFF
				Element.addClassName(this.element,'toggleOff');
				Element.removeClassName(this.element,'toggleOn');
				this.stateIsOn = false;						
				if (this.toggleOff != null){
					this.toggleOff(this);
				}									
			} else {
				//toggle from OFF to ON
				Element.addClassName(this.element,'toggleOn');
				Element.removeClassName(this.element,'toggleOff');
				this.stateIsOn = true;					
				if (this.toggleOn != null){
					this.toggleOn(this);
				}								
			}
			handlerEnabledFlag = true;
		}
	},
	
	/**
	 * on toggleOn callback function (this-object)
	 */
	toggleOn: null,	
	
	/**
	 * on toggleOff callback function (this-object)
	 */
	toggleOff: null,		
		
}

//static
ToggleButton.preload = function() {
	Logger.debug("ToggleButton.preload()");
	$$(".toggleButton").each(
		function(button){
			Logger.debug("ToggleButton.preload( ) -> "+button);			
			new ToggleButton(button);
		}
	);	
}

Event.observe(window, "load", ToggleButton.preload);
/**************************************************************************************/


















