/**
 * JS User class for UserEntity
 */
User = Class.create();

User.prototype = {
	//-------------------------------------------
	// ATTRIBUTES
	//			
	/**
	 * unique ID of this component
	 */
	id : null,
	
	/**
	 * New bookmark (no UUID was given)
	 */
	isNew : true,
	
	/**
	 * User fields were loaded already
	 */
	//isLoaded : false,
	
	/**
	 * The form that represents the user
	 */
	thisForm : null,
	
	//
	//-------------------------------------------		
	
	
	
	
	//-------------------------------------------
	// INITIALIZE
	//	
	initialize: function(formElement){
	 
		this.id = Element.identify(formElement);		
		Logger.debug("User.initialize() formElement.id:[" + this.id + "] UUID[" + $(this.id+"_uuid").value + "]");		
		this.thisForm = {
			// fields	
			uuid 		: $(this.id+"_uuid"),
			login		: $(this.id+"_login"),
			name		: $(this.id+"_name"),
			surname		: $(this.id+"_surname"),
			mail		: $(this.id+"_mail"),
			mobile		: $(this.id+"_mobile"),
			
			//buttons
			apply_button	: $(this.id+"_apply"),
			edit_button 	: $(this.id+"_edit"),
			create_button	: $(this.id+"_create"),
			change_button	: $(this.id+"_change"),
			confirm_button	: $(this.id+"_confirm")		
		};
		this.isNew = (this.thisForm.uuid.value == "");
		
		//------------------------------------------		
		
		//apply button
		if(this.thisForm.apply_button){		
			this.thisForm.apply_button.onclick = this.onApply.bindAsEventListener(this);					
		}
		
		//editable toggle button
		if(this.thisForm.edit_button){			
			this.thisForm.edit_button.toggle.toggleOn  = this.onEdit.bindAsEventListener(this, this.thisForm.edit_button, $$("#"+this.id+"_apply")[0]);		
			this.thisForm.edit_button.toggle.toggleOff = this.onEdit.bindAsEventListener(this, this.thisForm.edit_button, $$("#"+this.id+"_apply")[0]);			
		}	
		
		//------------------------------------------			
		
		//create button
		if(this.thisForm.create_button){		
			this.thisForm.create_button.onclick = this.onCreate.bindAsEventListener(this);					
		}	
		
		//confirm toggle button
		if(this.thisForm.confirm_button){		
			this.thisForm.confirm_button.onclick = this.onConfirm.bindAsEventListener(this);					
		}
		
		//change button
		if(this.thisForm.change_button){			
			this.thisForm.change_button.toggle.toggleOn  = this.onChange.bindAsEventListener(this, this.thisForm.change_button, $$("#"+this.id+"_confirm")[0]);		
			this.thisForm.change_button.toggle.toggleOff = this.onChange.bindAsEventListener(this, this.thisForm.change_button, $$("#"+this.id+"_confirm")[0]);			
		}			
		
		//------------------------------------------
		
		//group lists - details buttons
		$$("ul.groupList li").each(
			function(el){
				var liId = el.identify();
				var aElement = $$('#'+liId+' .detailsButton')[0]; 						
				aElement.onclick = this.onGroupLiClick.bindAsEventListener(this, aElement, liId);		
			}.bind(this)
		);							
	
		
		//new group button
		var aElement = $$('#'+this.id+' span.newGroupButton a')[0];	
		if(aElement){
			aElement.onclick = this.onGroupNewClick.bindAsEventListener(this, aElement);
		}
	
		//------------------------------------------		
		
		//add to instances
		User.instances.set(this.id, this);		
	},
	
	/**
	 * Catch the onsubmit event - to make sure it is not submitted :)
	 */	
	onSubmit : function(event)
	{
		return false;
	},		
	
	//
	//-------------------------------------------	
	
	
	
	//-------------------------------------------
	// LOAD
	//
	
	loadUser: function(user){
		this.thisForm.uuid.value = user.uuid;
		this.thisForm.login.value = user.login;
		this.thisForm.name.value = user.name;
		this.thisForm.surname.value = user.surname;
		this.thisForm.mail.value = user.mail;
		this.thisForm.mobile.value = user.mobile;		

		this.isNew = (this.thisForm.uuid.value == "");
	},	
	
	load: function(uuid){
		
		if(uuid){
			this.thisForm.uuid.value = uuid;
		}
			
		Logger.debug("User.load() PATH : " + CONTEXT_PATH+"/user/"+this.thisForm.uuid.value);		
		var req = new Ajax.Request(CONTEXT_PATH+"/user/"+this.thisForm.uuid.value, 
		{
			method: "get",
			onSuccess: function(resp){
				if ( resp.responseJSON === null )
				{
					resp.responseJSON = resp.responseText.evalJSON();
				}
				this.loadUser(resp.responseJSON); 
				if (this.onLoad != null)
				{
					this.onLoad(this, true);
				} 
			}.bind(this),
			onFailure: function(transport, problem){
				Logger.error("User.load.onFailure  TRANSPORT:" + Util.listAttributes(transport) + " PROBLEM:" + Util.listAttributes(problem));					
				try {  console.debug(problem); } catch (e) {};
				if (this.onLoad != null)
				{
					this.onLoad(this, true);
				} 
			}.bind(this)
		});		
		 
	},		
	
	/**
	 * on-load callback  function (this-object, wasSuccessful)
	 */
	onLoad: null,
	
	//
	//-------------------------------------------	
	
	
	
	//-------------------------------------------
	// SAVE
	//
	
	saveUser: function(user){
		 
	},	 

	
	save: function(){
		var userObject;
		if( this.isNew ){
			userObject = $H({
				uuid			: this.thisForm.uuid.value,
				login			: this.thisForm.login.value				
			});	
		} else {
			userObject = $H({
				uuid			: this.thisForm.uuid.value,
				login			: this.thisForm.login.value,		
				name			: this.thisForm.name.value,
				surname			: this.thisForm.surname.value,
				mail			: this.thisForm.mail.value,
				mobile			: this.thisForm.mobile.value			
			});				
		}
					
		Logger.debug("User.save() User.toJSON() : " + userObject.toJSON());					
		var _method = ( this.isNew ) ? "put" : "post";
		var additionalParameter = ( this.isNew ) ? "?_method=put" : "";
			
		Logger.debug("User.save() PATH : " + CONTEXT_PATH+"/user/"+this.thisForm.uuid.value+additionalParameter);
		var req = new Ajax.Request(CONTEXT_PATH+"/user/"+this.thisForm.uuid.value+additionalParameter, 
		{
			method: _method,
			contentType: "application/json",
			postBody: userObject.toJSON(),
			onSuccess: this.onSuccessSave.bind(this),
			onComplete: function(resp){
				Logger.debug("User.save.onComplete : RESP:" + Util.listAttributes(resp));
			},
			onFailure: function(transport, problem){
				Logger.error("User.save.onFailure  TRANSPORT:" + Util.listAttributes(transport) + " PROBLEM:" + Util.listAttributes(problem));					
				try {  console.debug(problem); } catch (e) {};
				if (this.onSave != null)
				{
					this.onSave(this, false);
					this.onSave = null;					
				} 
			}.bind(this)
		});		
		Logger.debug("User.save : REQ : " + Util.listAttributes(req))
	},	
	
	/**
	 * It was impossible to find any bug when it was inside the previous
	 * function.
	 */
	onSuccessSave: function(resp) {
		if ( resp.responseJSON === null )		
		{
			
			resp.responseJSON = resp.responseText.evalJSON();
		}			
		this.loadUser(resp.responseJSON); 
		if (this.onSave != null)
		{
			this.onSave(this, true);
			this.onSave = null;
		} 
	},
	
	/**
	 * on-save callback function (this-object, wasSuccessful)
	 */
	onSave: null,		
	
	//
	//-------------------------------------------	
	
	
	
	//-------------------------------------------
	// LISTENERS
	//
	
	onApply: function(event){
		 Logger.debug("User.onApply");
		 this.save();
	},
	
	onCancel: function(event){
		Logger.debug("User.onCancel");
		this.load(this.thisForm.uuid.value);
	},	
	
	onEdit: function(event, element, submit){	
		var isOn = element.toggle.stateIsOn
		Logger.debug("User.onEdit isOn["+isOn+"]");
		$$('#'+this.id+' input.editable').each(
			function(el){
				el.disabled = !isOn;
				Element.addClassName(el,(isOn)?'enabledInput':'disabledInput');
				Element.removeClassName(el,(isOn)?'disabledInput':'enabledInput');
			}.bind(this)
		);	
		if(isOn){	//enable submit
			Element.removeClassName(submit,'excludedFromLayout');
		} else {	//disable submit	
			Element.addClassName(submit,'excludedFromLayout');
			this.onCancel(event);	
		}
	},	
	
	onCreate: function(event){
		 Logger.debug("User.onCreate");
		 this.onSave = function (event, success){				
							// log user in after registration was successful
						 	if(success){
						 		Global.redirectToLogIn();	
						 	} else {
						 		
						 	}			 
						}
		 this.save();
	},
	
	onConfirm: function(event){
		 Logger.debug("User.onConfirm");
	},
	 
	onGroupLiClick: function(event, element, liId){		/* element: input, liId: UUID */
		
		Event.stop(event);			 			
		Group.instances.get('groupDetails').onLoad = function (event, success){	
			alert(event + ' : ' + success);
			facebox.buttonClick_handler( element );	// content element	
		}
		Group.instances.get('groupDetails').load(liId);
		 		 	 
	},	 
	
	onGroupNewClick: function(event, element){		/* element: <a> */
		 
		 Event.stop(event);			 
		 $$("#newGroup .editable").each(
			function(el){
				el.disabled = false;
				Element.addClassName(el,'enabledInput');
			}.bind(this)
		 );			 
		 
		 //Element.setStyle($$("#facebox .body")[0], $H({ backgroundColor: '#66FF66' }));
//		 ($$("#facebox .body")[0]).style.backgroundColor = '#66FF66';
		 facebox.buttonClick_handler(element);
//		 facebox.onClose = function (){	
//			 ($$("#facebox .body")[0]).style.backgroundColor = '';
//		 }		 
		 		 	 
	}	 	
	
	//
	//-------------------------------------------	
	
}

/**
* Collection of User instances 
*/
User.instances = $H({});

User.preload = function() {
	Logger.debug("User.preload()");
	$$("form.userForm").each(
		function(form){
			new User(form);
		}
	);	
}

Event.observe(window, "load", User.preload);


