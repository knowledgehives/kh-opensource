/**
 * JS Group class for GroupEntity
 */
Group = Class.create();

Group.prototype = {
	//-------------------------------------------
	// ATTRIBUTES
	//			
	/**
	 * unique ID of this component
	 */
	id : null,
	
	/**
	 * New bookmark (no UUID was given)
	 */
	isNew : true,
	
	/**
	 * Group fields were loaded already
	 */
	//isLoaded : false,
	
	/**
	 * The form that represents the group
	 */
	thisForm : null,	
	
	//
	//-------------------------------------------	
	
	
	
	//-------------------------------------------
	// INITIALIZE
	//		
	initialize: function(formElement){
			 
		this.id = Element.identify(formElement);		
		Logger.debug("Group.initialize() formElement.id:[" + this.id + "] UUDI[" + $(this.id+"_uuid").value + "]");				
		this.thisForm = {
			//fields				
			uuid 		: $(this.id+"_uuid"),
			name		: $(this.id+"_name"),
			description : $(this.id+"_description"),						
			owner_login	: $(this.id+"_owner_login"),
			
			//buttons
			apply_button	: $(this.id+"_apply"),
			edit_button 	: $(this.id+"_edit"),
			create_button	: $(this.id+"_create")			
		};
		this.isNew = (this.thisForm.uuid.value == "");		
		
		//submit button
		if(this.thisForm.apply_button){		
			this.thisForm.apply_button.onclick = this.onApply.bindAsEventListener(this);					
		}
		
		//save button
		if(this.thisForm.create_button){		
			this.thisForm.create_button.onclick = this.onApply.bindAsEventListener(this);					
		}		
		
		
		//editable
		if(this.thisForm.edit_button){	
			this.thisForm.edit_button.toggle.toggleOn  = this.onEdit.bindAsEventListener(this, this.thisForm.edit_button, $$("#"+this.id+"_apply")[0]);		
			this.thisForm.edit_button.toggle.toggleOff = this.onEdit.bindAsEventListener(this, this.thisForm.edit_button, $$("#"+this.id+"_apply")[0]);			
		}	
		
		//users list - details buttons
		$$("ul.userList li").each(
			function(el){
				var liId = el.identify();
				var aElement = $$('#'+liId+' .detailsButton')[0];
				aElement.onclick = this.onUserLiClick.bindAsEventListener(this, aElement, liId);				
			}.bind(this)			
		);
		
		//add to instances
		Group.instances.set(this.id, this);
	},
	/**
	 * Catch the onsubmit event - to make sure it is not submitted :)
	 */	
	onSubmit : function(event)
	{
		return false;
	},		
	
	//
	//-------------------------------------------		
	
	
	
	//-------------------------------------------
	// LOAD
	//
	
	loadGroup: function(group){	
		this.thisForm.uuid.value = group.uuid;
		this.thisForm.name.value = group.name;
		this.thisForm.description.innerHTML = group.description;
		this.thisForm.owner_login.value = group.owner.login;		
		Logger.debug("Group.loadGroup() : " + group.uuid + " | " + group.name + " | " + group.owner.login + " | " + group.owner);
		
		this.isNew = (this.thisForm.uuid.value == "");
	},	
	
	load: function(uuid){
		
		if(uuid){
			this.thisForm.uuid.value = uuid;
		}		
		
		Logger.debug("Group.load() PATH : " + CONTEXT_PATH+"/group/"+this.thisForm.uuid.value);	
		var req = new Ajax.Request(CONTEXT_PATH+"/group/"+this.thisForm.uuid.value, 
		{
			method: "get",
			onSuccess: function(resp){
				if ( resp.responseJSON === null )
				{
					resp.responseJSON = resp.responseText.evalJSON();
				}
				this.loadGroup(resp.responseJSON); 
				if (this.onLoad != null)
				{
					this.onLoad(this, true);
				} 						
			}.bind(this),
			onFailure: function(transport, problem){
				Logger.error("Group.load.onFailure  TRANSPORT:" + Util.listAttributes(transport) + " PROBLEM:" + Util.listAttributes(problem));
				try {  console.debug(problem); } catch (e) {};
				if (this.onLoad != null)
				{
					this.onLoad(this, true);
				} 
			}.bind(this)
		});		
		 
	},						
	
	/**
	 * on-load callback  function (this-object, wasSuccessful)
	 */
	onLoad: null,	
	
	//
	//-------------------------------------------	
	
	
	
	//-------------------------------------------
	// SAVE
	//
	
	saveGroup: function(group){
		 
	},	 
		
	save: function(){
		var groupObject = $H(
		{
			uuid			: this.thisForm.uuid.value,	
			name			: this.thisForm.name.value,
			description		: this.thisForm.description.value,
			owner_login		: this.thisForm.owner_login.value		
		});	
					
		Logger.debug("Group.save() Group.toJSON() : " + groupObject.toJSON());			
		
		var _method = ( this.isNew ) ? "put" : "post";
		var additionalParameter = ( this.isNew ) ? "?_method=put" : "";
			
		Logger.debug("Group.save() PATH : " + CONTEXT_PATH+"/group/"+this.thisForm.uuid.value+additionalParameter);
		var req = new Ajax.Request(CONTEXT_PATH+"/group/"+this.thisForm.uuid.value+additionalParameter, 
		{
			method: _method,
			contentType: "application/json",
			postBody: groupObject.toJSON(),
			onSuccess: this.onSuccessSave.bind(this),
			onComplete: function(resp){
				Logger.debug("Group.save.onComplete : RESP:" + resp);
			},
			onFailure: function(transport, problem){
				Logger.error("Group.save.onFailure  TRANSPORT:" + Util.listAttributes(transport) + " PROBLEM:" + Util.listAttributes(problem));					
				try {  console.debug(problem); } catch (e) {};
				if (this.onSave != null)
				{
					this.onSave(this, false);
				} 
			}.bind(this)
		});		
	},	
	
	/**
	 * It was impossible to find any bug when it was inside the previous
	 * function.
	 */
	onSuccessSave: function(resp) {
		if ( resp.responseJSON === null )		
		{			
			resp.responseJSON = resp.responseText.evalJSON();
		}			
		this.loadGroup(resp.responseJSON); 
		if (this.onSave != null)
		{
			this.onSave(this, true);
		} 
	},
	
	/**
	 * on-save callback function (this-object, wasSuccessful)
	 */
	onSave: null,		
	
	//
	//-------------------------------------------	
	
	
	
	//-------------------------------------------
	// LISTENERS
	//
	
	onApply: function(event){
		 Logger.debug("Group.onApply");
		 this.save();
	},
	
	onCancel: function(event){
		Logger.debug("Group.onCancel");
		this.load(this.thisForm.uuid.value);
	},		
	
	onEdit: function(event, element, submit){	
		var isOn = element.toggle.stateIsOn
		Logger.debug("Group.onEdit isOn["+isOn+"]");		
		$$('#'+this.id+' .editable').each(
			function(el){
				el.disabled = !isOn;
				Element.addClassName(el,(isOn)?'enabledInput':'disabledInput');
				Element.removeClassName(el,(isOn)?'disabledInput':'enabledInput');
			}.bind(this)
		);	
		if(isOn){	//enable submit
			Element.removeClassName(submit,'excludedFromLayout');
		} else {	//disable submit	
			Element.addClassName(submit,'excludedFromLayout');
			this.onCancel(event);	
		}
	},		

	onUserLiClick: function(event, element, liId){		/* element: input, liId: UUID */
		 
		Event.stop(event);				 
		User.instances.get('userDetails').onLoad = function (event, success){				 
			facebox.buttonClick_handler( element );	
		}
		User.instances.get('userDetails').load(liId);
		 		 	 
	},		
	
	//
	//-------------------------------------------		
	
}

/**
* Collection of Group instances 
*/
Group.instances = $H({});

Group.preload = function() {
	Logger.debug("Group.preload()");
	$$("form.groupForm").each(
		function(form){
			new Group(form);
		}
	);	
}

Event.observe(window, "load", Group.preload);


