<%--
  ~ SmartSchedule.me - Semantic Calendar
  ~ Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
  ~
  ~ Contribution from: Gdansk University of Technology, Poland
  ~ Invented by: Sebastian R. Kruk
  ~ Implemented by: Michał Szopiński, Mariusz Cygan
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as
  ~ published by the Free Software Foundation, either version 3 of the
  ~ License, or (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program.  If not, see <http://www.gnu.org/licenses/>.
  --%>

<%@ page language="java" contentType="text/html; UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="ssme" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="rest" uri="http://www.knowledgehives.com/ssme/jsp/rest" %>

<ssme:page isLeft="${ true }" isRight="${ true }" isSimple="${ false }" title="" >
	
	<jsp:attribute name="left" >

        <ssme:flexContainer animationName="Menu" id="calendar" width="100%" height="60%" />

	</jsp:attribute>

	<jsp:body>
	
		<ssme:flexContainer animationName="FlexClient" id="calendar" width="100%" height="90%" />					
		
	</jsp:body>

</ssme:page>