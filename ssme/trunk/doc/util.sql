--
-- (request <-> eventing <-> parts) integrity check
--
select
  ep.id,
  ep.uuid,
  ep.map,
  '-',
  e.id,
  e.uuid,
  e.state,
  e.user_id,
  '-',
  mr.id,
  mr.title,
  mr.description,
  mr.created,
  mr.uuid
from eventingpart ep, eventing e, meetingrequest mr
where
  ep.eventing_id = e.id
  and e.request_id = mr.id;

-- select ep.id, ep.uuid, ep.map, '-', e.id, e.uuid, e.state, e.user_id, '-', mr.id, mr.title, mr.description, mr.created, mr.uuid from eventingpart ep, eventing e, meetingrequest mr where ep.eventing_id = e.id and e.request_id = mr.id;



--
-- clear 'plain' events
-- clear timeframes -> clear repeattypes -> clear prototypes
--
-- TODO: fixme
delete from timeframe where prototype=0 and meetingRequest_id is null;  -- children and one that are not based on request
delete from repeattype where request_id is null;                        -- those one that are defined for timeframes not for event request
delete from timeframe where prototype=1;                                -- prototypes