/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
    import com.kh.ssme.model.entity.TimeFrameEntity;
    import com.kh.ssme.util.DateProperty;
    import com.kh.ssme.util.DateUtil;
    import com.kh.ssme.util.DayEventsFieldTypeEnum;
    import com.kh.ssme.util.HashArray;
    import com.kh.ssme.util.Logger;
    import com.kh.ssme.util.SsmeEvent;

    import flash.events.Event;

    import mx.core.UIComponent;

    public class CalendarMonthCanvas extends MonthGridCanvas implements ITimeCanvas {

        public function CalendarMonthCanvas() {
            super();
            toolTip = "";
        }

        private var oldWidth:Number, oldHeight:Number;
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{

            if(visible){    // necessary only if visible

                // call first in order to get grid drawn correctly
                super.updateDisplayList(unscaledWidth, unscaledHeight);

                // call repaint only if dimensions were changed
                if((unscaledWidth - oldWidth) != 0 || (unscaledHeight - oldHeight) != 0){
                    //there was change in width or height since last call

                    oldWidth = unscaledWidth;
                    oldHeight = unscaledHeight;
                    repaint();
                }                
    
            }
        }

        public var calendarsColorsMap:HashArray;     

        public override function set currentDay(value:Date):void{
            if(value){
                dataProvider_ = new HashArray();
                super.currentDay = value;
            }
        }
        public override function get currentDay():Date{
            return super.currentDay;
        }

        protected var dataProviderAvailable:Boolean = false;
        
        [ArrayElementType("com.kh.ssme.model.entity.TimeFrameEntity")]
        private var dataProvider_:HashArray;
        public function set dataProvider(value:HashArray):void{
            dataProviderAvailable = false;
            recreateEvents = true;            
            dataProvider_ = value;

            dataProviderAvailable = true;
            repaint();
        }
        public function get dataProvider():HashArray{
            return dataProvider_;
        }


        public override function repaint():void{
            // create grid
            // to fill it with data
            super.repaint();
            
            if(dataProviderAvailable){
                fillEvents();
                validateNow();                
            }
        }

        protected override function getMonthDisplayItemImpl():IMonthtDisplayItem {
            return new DayEventsField();
        }
        
        /**
         * Create display object for each TimeFrameEntity in dataProvider
         */
        protected function fillEvents():void{

            // in order to save time we recreate EventFields
            // only if it is necessary
            if(currentDay && eventsMap && eventsMap.size>0 && dataProvider_ && dataProvider_.size>0){

                var i:int, fromDay:int, toDay:int;
                var startDay:Date, endDay:Date
                var dataProviders:HashArray = new HashArray();
                for(i=0; i<=numberOfDays; i++)  dataProviders.put(i, new HashArray());

                // split events among dataProviders                
                for each(var timeFrame:TimeFrameEntity in dataProvider_){
                    fromDay = timeFrame.from.getDate();
                    toDay = timeFrame.to.getDate();
                    if (fromDay == toDay) {
                        // starts and ends in the same day
                        dataProviders[ fromDay ].put(timeFrame.UUID, timeFrame);
                    } else {
                        // if startDay from previous month than set it from to monthBeginDay
                        startDay = (timeFrame.from.getMonth() < currentDay.getMonth()) ? monthBeginDay.date : timeFrame.from;
                        // if endDay from next month than set it to monthEndDay
                        endDay = (timeFrame.to.getMonth() > currentDay.getMonth()) ? monthEndDay.date : timeFrame.to;

                        // splitted over more than one days, so add it to all corresponding dataProviders
                        var innerTimeFrame:TimeFrameEntity, iter:DateUtil = (new DateUtil( startDay )).dayBegin();
                        for (i = startDay.getDate(); i <= endDay.getDate(); i++, iter.add( DateProperty.DATE, 1 )) {
                            innerTimeFrame = timeFrame.clone();
                            if(i>startDay.getDate())  innerTimeFrame.from = iter.dayBegin().date;                            
                            if(i<endDay.getDate())  innerTimeFrame.to = iter.dayEnd().date;
                            dataProviders[ i ].put(innerTimeFrame.UUID, innerTimeFrame);
                        }                        
                    }
                }

                // set properties for grid items
                var ev:IMonthtDisplayItem;                
                for (i=1; i<=eventsMap.size; i++){
                    ev = eventsMap.getValueAt( i-1 );
                    ev.calendarsColorsMap = this.calendarsColorsMap;
                    ev.dataProvider = dataProviders[ i ];
                }

            }
        }         

    }

}