/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.kh.ssme.components {

    import com.kh.ssme.model.entity.meta.RequestHintsCollectionEntity;
import com.kh.ssme.util.SsmeEvent;

import flash.events.Event;

import mx.containers.VBox;

    [Event(name="ssme_itemSelected", type="com.kh.ssme.util.SsmeEvent")]

    public class HintsList extends VBox {
        public function HintsList() {
            super();
        }

        private var data_:RequestHintsCollectionEntity;
        public function set dataProvider(value:RequestHintsCollectionEntity):void{
            data_ = value;
            if( value ){
                loadItems();
            } else {
                this.removeAllChildren();                
            }
        }
        public function get dataProvider():RequestHintsCollectionEntity{
            return data_;    
        }
        
        private function loadItems():void {
            this.removeAllChildren();

            var item:HintListItem;
            for( var i:int=0; i<data_.hints.size; i++ ){
                item = new HintListItem(); 
                item.dataProvider = data_.hints.getValueAt( i );
                item.percentWidth = 100;
                item.addEventListener("ssme_itemSelected", reThrow);
                this.addChild( item );
            }
        }

        private function reThrow(event:Event):void {
            // re-dispatch
            if(event is SsmeEvent){
                dispatchEvent( new SsmeEvent(event.type, (event as SsmeEvent).eventProperties) );                   
            }
        }

    }

}