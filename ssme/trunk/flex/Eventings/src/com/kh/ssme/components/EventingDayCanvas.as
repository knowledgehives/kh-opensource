/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {

    import com.kh.ssme.model.entity.EventingPartEntity;
    import com.kh.ssme.model.enum.StateEnum;
import com.kh.ssme.util.Color;
import com.kh.ssme.util.HashArray;
import com.kh.ssme.util.Logger;
import com.kh.ssme.util.SsmeEvent;

    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Point;

    import mx.controls.Label;
    import mx.events.ToolTipEvent;
    import mx.formatters.DateFormatter;
    import mx.managers.ToolTipManager;


    [Exclude(name="activeBarColor", kind="style")]
    [Exclude(name="activeBarAlpha", kind="style")]
    public class EventingDayCanvas extends RequestDayCanvas {


        public var titleLabel:Label;


        public var stateSelector:StateSelector;


        private var dateFormatter:DateFormatter = new DateFormatter();
        
        
        public function EventingDayCanvas() {
            super();

            topVerticalMargin_ = 30;
            bottomVerticalMargin_ = 10;
            leftHorizontalMargin_ = 5;

            outerBorderColor_ = 0x000000;
            outerBorderAlpha_ = 1.0;
            outerBorderThickness_ = 1;
            innerBorderColors_ = [ 0xbbbbbb, 0x777777 ];
            innerBorderAlphas_ = [ 1.0, 1.0 ];
            innerBorderThicknesses_ = [ 1, 1 ];

            rows=[ 96, 24 ];
            columns=[ 1 ];

            visible = true;
            includeInLayout = true;
            this.setStyle("cornerRadius", 0);
            this.setStyle("backgroundColor", 0xffffff);
            this.setStyle("backgroundAlpha", 0);            

            dateFormatter.formatString = "DD-MM-YYYY";

            titleLabel = new Label();
            titleLabel.x = titleLabel.y = 0;
            titleLabel.visible = titleLabel.includeInLayout = true;
            titleLabel.setStyle("textAlign", "center");
            this.addChild( titleLabel );
            
            this.addEventListener(ToolTipEvent.TOOL_TIP_CREATE, onCreateTooltip);
            this.addEventListener(ToolTipEvent.TOOL_TIP_SHOW, onShowTooltip);
            this.addEventListener(MouseEvent.MOUSE_OVER, checkTooltipDisplay);
            this.addEventListener(MouseEvent.MOUSE_MOVE, checkTooltipDisplay);
            this.toolTip = " ";
        }

        private var tooltipImpl:EventingDayCanvasTooltip = new EventingDayCanvasTooltip();
        private function onCreateTooltip(event:ToolTipEvent):void{

            if( currentDate && timeFramesMap ){ // we got data for tooltip
                var pt:Point = contentToGlobal( new Point(0,0) );
                tooltipImpl.width = 0; 
                tooltipImpl.defaultWidth = 200;
                tooltipImpl.visible = false;    // set to 'true' automatically when shown

                // we try to force to show tooltip in specific place and with specific high
                // in order to display it aligned to time grid in this component
                //
                // tooltip height = this height + tooltip top/bottom padding - this vertical margin
                tooltipImpl.height = 0;  
                tooltipImpl.defaultHeight = height + 30 - topVerticalMargin_;
                // tooltip time grid height = this time grid height
                tooltipImpl.listHeight = height - topVerticalMargin_ - bottomVerticalMargin_ +20;
                tooltipImpl.canvasHeight = height - topVerticalMargin_ - bottomVerticalMargin_ +20;
                // tooltip y = this left-upper corner y + this date label height - tooltip top padding
                tooltipImpl.y = pt.y + topVerticalMargin_ - 20 + 4;

                // data for tooltip, if necessary - delay until we are sure that tooltip is initialized
                if( tooltipImpl.allComponentsReady ){
                    tooltipImpl.setAllData( currentDate, timeFramesMap );
                } else {
                    tooltipImpl.addEventListener( EventingDayCanvasTooltip.ALL_READY_EVENT, function(eventtt:Event):void{
                        tooltipImpl.setAllData( currentDate, timeFramesMap );
                    } );
                }

                event.toolTip = tooltipImpl;
            }
        }

        private function onShowTooltip(event:ToolTipEvent):void{
            var pt:Point = contentToGlobal( new Point(0,0) );
            tooltipImpl.y = pt.y + topVerticalMargin_ - 20 + 4;
        }

        private function checkTooltipDisplay(event:MouseEvent):void {

            if( ToolTipManager.currentToolTip ){

                // hide tooltip for irrelevant areas
                var pt:Point = contentToLocal( new Point(contentMouseX, contentMouseY) );
                var index:int = localY2quarter( pt.y );
                if(activity[index] != EMPTY){
                    tooltipImpl.width   = ( tooltipImpl.width == 0 ) ? tooltipImpl.defaultWidth : tooltipImpl.width;
                    tooltipImpl.height  = ( tooltipImpl.height == 0 ) ? tooltipImpl.defaultHeight : tooltipImpl.height;
                } else {
                    tooltipImpl.width = tooltipImpl.height = 0;
                }
            }
        }
    
        //------------------------------------------------        

        private var currentDate_:Date;
        public function set currentDate(value:Date):void{
            currentDate_ = value;
            if( value ){
                titleLabel.text = dateFormatter.format( value );    
            }
        }
        public function get currentDate():Date{
            return currentDate_;
        }

        //------------------------------------------------
       
        private var dataProvider_:EventingPartEntity;
        public function set dataProvider(value:EventingPartEntity):void{
            dataProvider_ = value;
            if( value ){
                activity = ("-"+value.map).split('');
                currentDate = value.day;
            }
        }
        public function get dataProvider():EventingPartEntity{
            return dataProvider_;
        }
        public function get updatedDataProvider():EventingPartEntity{
            dataProvider_.map = activity.join('').substring(1);
            return dataProvider_;
        }

        //------------------------------------------------

        private var timeFramesMap_:HashArray;
        public function set timeFramesMap(value:HashArray):void{
            timeFramesMap_ = value;
        }
        public function get timeFramesMap():HashArray{
            return timeFramesMap_;
        }

        //------------------------------------------------

        protected override function mouseClickHandler(event:MouseEvent):void {
            changeAvailability( event );
        }

        private function changeAvailability(event:MouseEvent):void {

            if(stateSelector){
                var temp:Number = localY2quarter(event.localY);
                if( event.shiftKey ){
                    // "range" mode
                    changeState( localY2quarter(previousY), temp, event );
                } else {
                    // "single" mode
                    if(activity[ temp ]!=EMPTY){
                        // only non-empty
                        changeState( temp, temp, event );
                    }
                }
                previousX = event.localX;
                previousY = event.localY;
            }

        }

        protected var from:Number = 0, to:Number = 0;
        private function changeState(from:Number, to:Number, event:MouseEvent):void {
            this.from   = Math.min(from, to);
            this.to     = Math.max(from, to);

            // position for selector
            // find 'x', first - make sure that x > 0 (will not exceed parent's left border), '2' just in case
            var tempY:int, tempX:int = Math.max(2, this.x+((this.width - stateSelector.width)*0.5));
            // second 'x' - must be smaller than (parent's right border position - selector's width), '-2' just in case
            tempX = ((tempX+stateSelector.width+2) > (this.parent.width)) ? (this.parent.width-stateSelector.width-2) : tempX;
            // be sure not to cross parent's top or bottom border
            tempY = (to>(96*0.5)) ? event.localY-10-stateSelector.height : event.localY+10;
            stateSelector.move( tempX, tempY );
            
            if( !stateSelector.visible ){
                // size of selector, show
                stateSelector.show( 120, (stateSelector.stateButtons.size*(20+2)) + 2 );
                // add 'click' listener
                stateSelector.registeredSelectedListener = stateSelected;
                stateSelector.addEventListener( StateSelector.STATE_SELECTED_EVENT, stateSelected );
            }
        }
        private function stateSelected(event:Event):void {
            if( event is SsmeEvent ){
                // hide, de-size
                if( stateSelector.visible ){
                    stateSelector.hide( );            
                }
                var selectedState:StateEnum = (event as SsmeEvent).eventProperties['state'];
                for( var i:int=from; i<=to; i++ ){
                    if( activity[i]!=EMPTY ){
                        activity[i] = selectedState.mnemonic;
                    }
                }
                invalidateDisplayList();
            }
        }

        //------------------------------------------------        

        protected override function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
            super.updateDisplayList( unscaledWidth, unscaledHeight );

            titleLabel.width = unscaledWidth;
            titleLabel.height = topVerticalMargin_ - 5;
        }

        protected override function paint():void {

            if(rowsPosition && rowsPosition.length>0){
                var color:uint, alfa:uint;                
                var outBorderThickness:Number = outerBorderThickness_;
                var topVerticalMargin:Number = topVerticalMargin_;
                var bottomVerticalMargin:Number = bottomVerticalMargin_;
                var leftHorizontalMargin:Number = leftHorizontalMargin_;
                var rightHorizontalMargin:Number = rightHorizontalMargin_;

                var tooltip:String = "";
                for(var i:int = 0; i<96; i++){
                    if( activity[i] != EMPTY ){
                        color = StateEnum.findByMnemonic( activity[i] ).color;
                        color = ( enabled ) ? color : Color.getInstance(color).makeLighter(20).desaturate(20).RGB;
                        
                        graphics.beginFill(color, 1.0);
                        Logger.debug(
                                "[ID:"+this.id+"]"+
                                " [i:"+i+"] ["+activity[i]+"] X:"+outBorderThickness+leftHorizontalMargin+
                                " Y:"+rowsPosition[0][i-1]+
                                " W:"+(unscaledWidth-(2*outBorderThickness+leftHorizontalMargin+rightHorizontalMargin))+
                                " H:"+(rowsPosition[0][i] - rowsPosition[0][i-1])
                            );
                        graphics.drawRoundRect(outBorderThickness+leftHorizontalMargin,                             // x
                                rowsPosition[0][i-1],                                                               // y
                                unscaledWidth-(2*outBorderThickness+leftHorizontalMargin+rightHorizontalMargin),    // width
                                (rowsPosition[0][i] - rowsPosition[0][i-1]),                                        // height
                                0, 0);

                        graphics.endFill();
                    }
                }

            }
        }        

    }

}