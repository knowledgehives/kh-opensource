/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
    import com.kh.ssme.model.enum.StateEnum;
    import com.kh.ssme.util.Color;
    import com.kh.ssme.util.HashArray;

import com.kh.ssme.util.SsmeEvent;

import flash.events.Event;
import flash.events.MouseEvent;

    import mx.containers.VBox;
    import mx.controls.Alert;
    import mx.controls.Button;
import mx.core.UIComponent;

[Event( name="stateSelected", type="flash.events.Event")]

    public class StateSelector extends VBox {

        public static const STATE_SELECTED_EVENT:String = "stateSelected";
        
        public function StateSelector() {
            super();
            this.setStyle("verticalGap",    2);
            this.setStyle("paddingLeft",    2);
            this.setStyle("paddingRight",   2);
            this.setStyle("paddingTop",     2);
            this.setStyle("paddingBottom",  2);
            this.setStyle("borderColor",    0x000000);
            this.setStyle("borderThickness",1);
            this.setStyle("backgroundColor",0xffffff);
            this.setStyle("backgroundAlpha",1);
        }

        public function show(width:Number, height:Number):void{
            this.width = width;
            this.height = height;
            this.visible = true;
            this.toolTip = "x:"+x+" y:"+y+" width:"+width+" height:"+height;
            if( this.parent && this.parent is UIComponent) (this.parent as UIComponent).validateNow();
            validateNow();
        }
        public function hide():void{
            move( 0, 0 );
            this.visible = false;
            if(registeredSelectedListener!=null)  removeEventListener( STATE_SELECTED_EVENT, registeredSelectedListener );
            registeredSelectedListener = null;
            validateNow();
        }

        public var registeredSelectedListener:Function;

        public var stateButtons:HashArray;
        override protected function createChildren():void{

            if(!stateButtons){
                stateButtons = new HashArray();

                var button:Button, lighter:uint, darker:uint;
                var statesToDisplay:Array = [ StateEnum.ACCEPTED, StateEnum.REJECTED, StateEnum.TENTATIVE, StateEnum.UNDECIDED ];
                for each(var state:StateEnum in statesToDisplay){
                    lighter = Color.makeSimpleLighter( state.color, 10);
                    darker = Color.makeSimpleDarker( state.color, 10);

                    button = new Button();
                    button.id = state.state; 
                    button.label = state.uiText;
                    button.percentWidth = 100;
                    button.height = 20;
                    button.visible = true;
                    button.includeInLayout = true;
                    button.setStyle("cornerRadius", 0);
                    button.setStyle("highlightAlphas", [0.25, 0]);                    
                    button.setStyle("fillAlphas", [0.85, 0.85, 1, 1]);
                    button.setStyle("fillColors", [ lighter, darker, lighter, darker ]);
                    button.setStyle("borderColor", state.color);
                    button.setStyle("themeColor", state.color);
                    button.addEventListener( MouseEvent.CLICK, stateSelected );
                    
                    stateButtons.put( state, button );
                    addChild( button );
                }
            }

        }

        private function stateSelected(event:MouseEvent):void {
            var ev:SsmeEvent = new SsmeEvent(STATE_SELECTED_EVENT, { state: StateEnum.find(event.currentTarget.id) });
            dispatchEvent( ev );
        }
    }
}