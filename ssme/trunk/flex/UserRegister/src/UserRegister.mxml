<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ SmartSchedule.me - Semantic Calendar
  ~ Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
  ~
  ~ Contribution from: Gdansk University of Technology, Poland
  ~ Invented by: Sebastian R. Kruk
  ~ Implemented by: Michał Szopiński, Mariusz Cygan
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU Affero General Public License as
  ~ published by the Free Software Foundation, either version 3 of the
  ~ License, or (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU Affero General Public License for more details.
  ~
  ~ You should have received a copy of the GNU Affero General Public License
  ~ along with this program.  If not, see <http://www.gnu.org/licenses/>.
  -->

<components:SSMEApplication
                xmlns:mx="http://www.adobe.com/2006/mxml"
                xmlns:components="com.kh.ssme.components.*"
				width="100%" height="100%"
				initialize="onInitialize(event);"
                backgroundGradientAlphas="[0, 0]" >

    <components:HeaderlessPanel
              id="panel" left="5" right="5" top="5" bottom="10"
              width="100%" height="100%" dropShadowEnabled="true"
              horizontalAlign="left" verticalAlign="middle"
              backgroundAlpha="0.4">       

        <mx:Form indicatorGap="10">

            <mx:FormItem id="error_item" label="" required="false" visible="{ error.text.length > 0 }">
                <mx:Label id="error" color="red" text="" />
            </mx:FormItem>            

            <mx:FormItem id="login_item" label="Login: " required="true">
                <mx:TextInput id="login" />
            </mx:FormItem>

            <mx:VBox width="100%" verticalGap="0">
                <components:PasswordChecker id="gradient" width="100%" height="16" dataProvider="{ pas1 }" cornerRadius="10" border="1"/>
            </mx:VBox>
            
            <mx:FormItem id="pas1_item" label="Password: " required="true">
                <mx:Box id="pas1box">
                    <mx:TextInput id="pas1" displayAsPassword="true" valid="{ SSMEValidator.markAsValid( pas1box ) }" invalid="{ SSMEValidator.markAsInvalid( pas1box ) }" />
                </mx:Box>
            </mx:FormItem>
            <mx:FormItem id="pas2_item" label="Confirm password: " required="true">
                <mx:Box id="pas2box">
                    <mx:TextInput id="pas2" displayAsPassword="true" valid="{ SSMEValidator.markAsValid( pas2box ) }" invalid="{ SSMEValidator.markAsInvalid( pas2box ) }" />
                </mx:Box>
            </mx:FormItem>

            <mx:Spacer width="100%" height="10" />

            <mx:FormItem id="name_item" label="Name: " required="false">
                <mx:TextInput id="name_" />
            </mx:FormItem>
            <mx:FormItem id="surname_item" label="Surname: " required="false">
                <mx:TextInput id="surname" />
            </mx:FormItem>
            <mx:FormItem id="mail_item" label="Mail: " required="true">
                <mx:TextInput id="mail" />
            </mx:FormItem>
            <mx:FormItem id="mobile_item" label="Mobile: " required="false">
                <mx:TextInput id="mobile" />
            </mx:FormItem>

            <mx:FormItem width="100%" height="10">
                <mx:Spacer width="100%" height="10"/>
            </mx:FormItem>

            <mx:FormItem>
                <mx:Button label="Register" click="userUpdate(event)"/>
            </mx:FormItem>

        </mx:Form>

    </components:HeaderlessPanel >
	
	<mx:Script>
		<![CDATA[
        import com.adobe.serialization.json.JSON;
        import com.kh.ssme.components.ModalSpinner;
        import com.kh.ssme.connector.BasicQueuedConnector;
        import com.kh.ssme.connector.ConnectorResult;
        import com.kh.ssme.connector.RegisterConnector;
        import com.kh.ssme.util.JSONObject;
        import com.kh.ssme.util.Logger;
        import com.kh.ssme.util.LoggingComponentsEnum;
        import com.kh.ssme.util.SSMEValidator;
        import com.kh.ssme.util.SsmeEvent;

        import mx.events.ValidationResultEvent;
        import mx.validators.EmailValidator;
        import mx.validators.RegExpValidator;
        import mx.validators.Validator;

        private var connector:RegisterConnector;

        // all validators array
        private var validators:Array;

        private var loginValidator:Validator;
        private var password1Validator:Validator;
        private var password2Validator:SSMEValidator;
        private var nameValidator:Validator;
        private var surnameValidator:Validator;
        private var mailValidator:EmailValidator;
        private var mobileValidator:RegExpValidator;



        private function onInitialize(event:Event):void {
            connector = new RegisterConnector(this.url);
            connector.addEventListener(BasicQueuedConnector.EVENT_CREATE, onUserCreateResponse);         
            connector.addEventListener(BasicQueuedConnector.EVENT_ERROR, onUserError);
            connector.addEventListener(BasicQueuedConnector.EVENT_FAULT, onUserCreateFault);

            //--------------------------------

            validators = [ ];

            loginValidator = new Validator();
            loginValidator.source = login;
            loginValidator.property = "text";
            loginValidator.trigger = login;
            loginValidator.triggerEvent = "focusOut";
            loginValidator.required = login_item['required'];
            loginValidator.requiredFieldError = "Field '" + (login_item['label']) + "' is required."
            validators.push(loginValidator);

            password1Validator = new Validator();
            password1Validator.source = pas1;
            password1Validator.property = "text";
            password1Validator.trigger = pas1;
            password1Validator.triggerEvent = "focusOut";
            password1Validator.required = pas1_item['required'];
            password1Validator.requiredFieldError = "Field '" + (pas1_item['label']) + "' is required."
            validators.push(password1Validator);

            password2Validator = new SSMEValidator();
            password2Validator.source = pas2;
            password2Validator.property = "text";
            password2Validator.trigger = pas2;
            password2Validator.triggerEvent = "focusOut";
            password2Validator.required = pas2_item['required'];
            password2Validator.requiredFieldError = "Field '" + (pas2_item['label']) + "' is required."
            password2Validator.validateFunction = function (value:Object = null, suppressEvents:Boolean = false):ValidationResultEvent {
                var resultEvent:ValidationResultEvent;
                if (!pas2.text || pas2.text.length < 1) {
                    pas2.errorString = password2Validator.requiredFieldError;
                    resultEvent = new ValidationResultEvent(ValidationResultEvent.INVALID);
                } else if (pas1.text == pas2.text) {
                    pas2.errorString = "";
                    resultEvent = new ValidationResultEvent(ValidationResultEvent.VALID);
                } else {
                    pas2.errorString = "Passwords need to match each other";
                    resultEvent = new ValidationResultEvent(ValidationResultEvent.INVALID);
                }
                if (!suppressEvents) {
                    dispatchEvent(resultEvent);
                }
                return resultEvent;
            };
            validators.push(password2Validator);

            nameValidator = new Validator();
            nameValidator.source = name_;
            nameValidator.property = "text";
            nameValidator.trigger = name_;
            nameValidator.triggerEvent = "focusOut";
            nameValidator.required = name_item['required'];
            nameValidator.requiredFieldError = "Field '" + (name_item['label']) + "' is required."
            validators.push(nameValidator);

            surnameValidator = new Validator();
            surnameValidator.source = surname;
            surnameValidator.property = "text";
            surnameValidator.trigger = surname;
            surnameValidator.triggerEvent = "focusOut";
            surnameValidator.required = surname_item['required'];
            surnameValidator.requiredFieldError = "Field '" + (surname_item['label']) + "' is required."
            validators.push(surnameValidator);

            mailValidator = new EmailValidator();
            mailValidator.source = mail;
            mailValidator.property = "text";
            mailValidator.trigger = mail;
            mailValidator.triggerEvent = "focusOut";
            mailValidator.required = mail_item['required'];
            mailValidator.requiredFieldError = "Field '" + (mail_item['label']) + "' is required."
            validators.push(mailValidator);

            mobileValidator = new RegExpValidator();
            mobileValidator.source = mobile;
            mobileValidator.property = "text";
            mobileValidator.trigger = mobile;
            mobileValidator.triggerEvent = "focusOut";
            mobileValidator.required = mobile_item['required'];
            mobileValidator.requiredFieldError = "Field '" + (mobile_item['label']) + "' is required."
            mobileValidator.expression = "^\\s*(\\(){1}(\\+)?([0-9]+)(\\)){1}\\s*([0-9]{9}|[0-9]{10}){1}\\s*$";
            mobileValidator.flags = "gi";
            mobileValidator.noMatchError = "Field '" + (mobile_item['label']) + "' is invalid. Valid format is similar to '(+48) 123456789'";
            validators.push(mobileValidator);

        }

        private function onUserCreateResponse(event:Event):void {
            // inform
            error.setStyle("color","black");            
            error.text = "Registration successful, redirecting to user's page."
            ModalSpinner.instance.hideSpinner();
            // 'redirect'
            setTimeout(function():void {
                navigateToURL(new URLRequest(applicationURL + "user/"), "_self");
            }, 5000);
        }

        private function onUserError(event:Event):void {
            var result:ConnectorResult = (event && event is SsmeEvent) ? (event as SsmeEvent).eventProperties['result'] : null;
            var json:Object = result.lastResult;
            error.text = json[JSONObject.errorMessage];
            pas1.text = pas2.text = "";
            ModalSpinner.instance.hideSpinner();
        }

        private function onUserCreateFault(event:Event):void {
            Logger.errorEvent("Register error", event, LoggingComponentsEnum.REG);
            ModalSpinner.instance.hideSpinner();
            error.text = "Register error, please try again.";
            pas1.text = pas2.text = "";
        }

        public function userUpdate(event:Event):void {
            if (Validator.validateAll(validators).length == 0) {

                ModalSpinner.instance.showSpinner("Registering user...", 0x1313cd, panel, 0.2);

                var json:String = JSON.encode({
                    uuid: null,
                    login: login.text,
                    password: pas1.text,
                    name:name_.text,
                    surname:surname.text,
                    mail:mail.text,
                    mobile:mobile.text
                });
                connector.registerUser(json, null);
            }
        }

		]]>
	</mx:Script>
	
</components:SSMEApplication>
