/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.util {

    public class Color {

        private var red_:uint;               //  (0 - 255)
        private var green_:uint;             //  (0 - 255)
        private var blue_:uint;              //  (0 - 255)
        public var hue_:Number;             //  (0 - 360)
        public var saturationHSV_:Number;   //  (0 - 100)
        public var value_:Number;           //  (0 - 100)
        public var saturationHSL_:Number;   //  (0 - 100)
        public var lightness_:Number;       //  (0 - 100)

        public function Color() {
            this.red_ = 0;
            this.green_ = 0;
            this.blue_ = 0;
            this.hue_ = 0;
            this.saturationHSV_ = 0;
            this.value_ = 0;
            this.saturationHSL_ = 0;
            this.lightness_ = 0;
        }

        public static function getInstance(rgb:uint):Color{
            var color:Color = new Color();
            color.RGB = rgb;
            return color;
        }

        public function clone():Color{
            var color:Color = new Color();
            color.RGB = this.RGB;
            return color;
        }

        public function set RGB(value:uint):void{
            this.red_ = (value >> 16) & 0x0000FF,
            this.green_ = (value >> 8) & 0x0000FF;
            this.blue_ = value & 0xFF;
            RGB2HSL();
            RGB2HSV();
        }

        public function setRGB(red:uint, green:uint, blue:uint):Color {
            this.red_ = red & 0x0000FF,
            this.green_ = green & 0x0000FF;
            this.blue_ = blue & 0x000FF;
            RGB2HSL();
            RGB2HSV();
            return this;
        }

        public function get RGB():uint{
            return (this.red_ << 16) | (this.green_ << 8) | (this.blue_);
        }

        /**
         * set params for HSL model
         * @param hue (0-360)
         * @param saturation (0-100)
         * @param lightness (0-100)
         * @return Color
         */
        public function setHSL(hue:Number, saturation:Number, lightness:Number):Color{
            this.hue_ = Math.min(Math.max(hue, 0), 360);
            this.saturationHSL_ = Math.min(Math.max(saturation, 0), 100);
            this.lightness_ = Math.min(Math.max(lightness, 0), 100);
            HSL2RGB();
            RGB2HSV();
            return this;
        }

        /**
         * set params for HSV model
         * @param hue (0-360)
         * @param saturation (0-100)
         * @param value (0-100)
         * @return Color
         */
        public function setHSV(hue:Number, saturation:Number, value:Number):Color{
            this.hue_ = Math.min(Math.max(hue, 0), 360);
            this.saturationHSV_ = Math.min(Math.max(saturation, 0), 100);
            this.value_ = Math.min(Math.max(value, 0), 100);
            HSV2RGB();
            RGB2HSL();
            return this;
        }

        /**
         * set params for HSL model
         * @param lightness (0-100)
         */
        public function setLightness(lightness:Number):Color{
            return setHSL(this.hue_, this.saturationHSL_, lightness);
        }

        public function toString():String{
            return "COLOR:[RGB{r:"+this.red_+"; g:"+this.green_+"; b:"+this.blue_+";} = " + Color.colorUint2hex(this.RGB) + " "+
                        "HSV{h:"+this.hue_+"; s:"+this.saturationHSV_+"; v:"+this.value_+";} " +
                        "HSL{h:"+this.hue_+"; s:"+this.saturationHSL_+"; l:"+this.lightness_+";}]";
        }

        //---------------------------------------------------------
        // recalculate
        /**
         * Calculates elements of HSV model from red, green and blue attributes
         */
        private function RGB2HSV():void{
            //pre
            var max:Number = Math.max(this.red_, this.green_, this.blue_);
            var min:Number = Math.min(this.red_, this.green_, this.blue_);

            // HUE
            if (min==max){
                this.hue_ = 0;
            } else {
                if(this.red_ == max){
                    this.hue_ = /*0 +*/(((this.green_ - this.blue_)*60) / (max - min));
                } else if(this.green_ == max){
                    this.hue_ = 120 + (((this.blue_ - this.red_)*60) / (max - min));
                } else {    // (this.blue_ == max)
                    this.hue_ = 240 + (((this.red_ - this.green_)*60) / (max - min));
                }
            }
            if(this.hue_ < 0){
                this.hue_ += 360;
            }

            // SATURATION HSV
            if (max==0){
                this.saturationHSV_ = 0;
            } else {
                this.saturationHSV_ = ((max - min) * 100) / max;
            }

            // VALUE
            this.value_ = (100 * max) / 255;
        }

        /**
         * Calculates elements of RGB model from hue, saturation(HSV) and value attributes
         */
        private function HSV2RGB():void{
            if(this.value_==0){
                this.red_ = 0, this.green_ = 0, this.blue_ = 0;
            } else {
                var i:int = Math.floor(this.hue_/60);
                var f:Number = (this.hue_ / 60) - i;
                var v:Number = this.value_/100;
                var s:Number = this.saturationHSV_/100;
                var p:Number = (v * (1 - s)) * 255;
                var q:Number = (v * (1 - (s * f))) * 255;
                var t:Number = (v * (1 - (s * (1 - f)))) * 255;
                if(i==0){
                    this.red_ = this.value_; this.green_ = t; this.blue_ = p;
                } else if(i==1){
                    this.red_ = q; this.green_ = this.value_; this.blue_ = p;
                } else if(i==2){
                    this.red_ = p; this.green_ = this.value_; this.blue_ = t;
                } else if(i==3){
                    this.red_ = p; this.green_ = q; this.blue_ = this.value_;
                } else if(i==4){
                    this.red_ = t; this.green_ = p; this.blue_ = this.value_;
                } else if(i==5){
                    this.red_ = this.value_; this.green_ = p; this.blue_ = q;
                }
            }
        }

        /**
         * Calculates elements of HSL model from red, green and blue attributes
         */
        private function RGB2HSL():void{
            //pre
            var max:Number = Math.max(this.red_, this.green_, this.blue_);
            var min:Number = Math.min(this.red_, this.green_, this.blue_);

            // HUE
            if (min==max){
                this.hue_ = 0;
            } else {
                if(this.red_ == max){
                    this.hue_ = /*0 +*/(((this.green_ - this.blue_)*60) / (max - min));
                } else if(this.green_ == max){
                    this.hue_ = 120 + (((this.blue_ - this.red_)*60) / (max - min));
                } else {    // (this.blue_ == max)
                    this.hue_ = 240 + (((this.red_ - this.green_)*60) / (max - min));
                }
            }
            if(this.hue_ < 0){
                this.hue_ += 360;
            }

            // LIGHTNESS
            this.lightness_ = (((max + min) * 50) / 255); //    ((max+min) / 2) * 100 / 255

            // SATURATION HSL
            var mx:Number = max / 255;
            var mn:Number = min / 255;
            var l:Number = (mx + mn) / 2;
            if (max==min){
                this.saturationHSL_ = 0;
            } else if (l <= 0.5){
                this.saturationHSL_ = ((mx - mn) / (mx + mn )) * 100;
            } else {
                this.saturationHSL_ = ((mx - mn) / (2 - mx - mn)) * 100;
            }

        }

        /**
         * Calculates elements of RGB model from hue, saturation(HSL) and lightness attributes
         */
        private function HSL2RGB():void{
            var s:Number = this.saturationHSL_ / 100.0;
            var l:Number = this.lightness_ / 100.0;
            if(this.saturationHSL_==0){
                this.red_ = l * 255, this.green_ = l * 255, this.blue_ = l * 255;
            } else {
                //pre
                var q:Number = (l < 0.5) ? (l * (1.0 + s)) : (l + s - (l * s));
                var p:Number = 2 * l - q;
                var h:Number = this.hue_/360;
                var tr:Number = h + (1/3);
                var tg:Number = h;
                var tb:Number = h - (1/3);
                tr = (tr < 0) ? (tr + 1) : ((tr > 1)? (tr - 1) : tr);
                tg = (tg < 0) ? (tg + 1) : ((tg > 1)? (tg - 1) : tg);
                tb = (tb < 0) ? (tb + 1) : ((tb > 1)? (tb - 1) : tb);

                // red
                if(tr < (1/6)){
                    tr = p + ((q - p) * 6 * tr);
                } else if(tr < 0.5){
                    tr = q;
                } else if(tr < (2/3)){
                    tr = p + ((q - p) * 6 * (2/3 - tr));
                } else {
                    tr = p;
                }

                // green
                if(tg < (1/6)){
                    tg = p + ((q - p) * 6 * tg);
                } else if(tg < 0.5){
                    tg = q;
                } else if(tg < (2/3)){
                    tg = p + ((q - p) * 6 * (2/3 - tg));
                } else {
                    tg = p;
                }

                // blue
                if(tb < (1/6)){
                    tb = p + ((q - p) * 6 * tb);
                } else if(tb < 0.5){
                    tb = q;
                } else if(tb < (2/3)){
                    tb = p + ((q - p) * 6 * (2/3 - tb));
                } else {
                    tb = p;
                }

                this.red_ = Math.round(tr * 255);
                this.green_ = Math.round(tg * 255);
                this.blue_ = Math.round(tb * 255);
            }
        }
        //---------------------------------------------------------


        //---------------------------------------------------------
        // utility
        public static function averageColor(color1:uint, color2:uint, factor:Number = 0.5):uint{

            var primeFactor:Number = (factor>=0 && factor<=1) ? factor : 0.5;
            var secondFactor:Number = 1 - primeFactor;

            var red:int     = ((color1 >> 16) & 0x0000FF)*primeFactor + ((color2 >> 16) & 0x0000FF)*secondFactor;
            var green:int   = ((color1 >> 8) & 0x0000FF)*primeFactor + ((color2 >> 8) & 0x0000FF)*secondFactor;
            var blue:int    = (color1 & 0xFF)*primeFactor + (color2 & 0xFF)*secondFactor;

            return (red << 16) | (green << 8) | (blue);
        }
        
        public static function negativeTextColor(color:uint, threshold:Number = 60, lightColor:Number = NaN, darkColor:Number = NaN):uint{
            var calculate:Color = new Color();
            calculate.RGB = color;
            return (calculate.lightness_>threshold) ? (isNaN(darkColor)?0x000000:darkColor as uint) : (isNaN(lightColor)?0xFFFFFF:lightColor as uint);
        }

        public static function toUnsigned(color:String):String{
            if(!color){
                return "";
            }
            if(color.indexOf("#") > -1){
                return color.replace("#","0x");
            }
            if(color.toLowerCase().indexOf("rgb") > -1){
                var pattern:RegExp = /^\s*rgb\s*\(\s*(?P<r>(\d+))\s*,\s*(?P<g>(\d+))\s*,\s*(?P<b>(\d+))\s*\).*$/i;
                var colorObject:Object = pattern.exec( color.toLowerCase() );
                if(colorObject && colorObject.r && colorObject.g && colorObject.b){
                    return "0x"+colorUint2hex( ((colorObject.r & 0xff) << 16) | ((colorObject.g & 0xff) << 8) | (colorObject.b & 0xff) );
                }
            }
            return "";            
        }

        private static var hexMask:String = "000000";
        public static function colorUint2hex(color:uint):String{
            var colorHex:String = Number(color).toString(16).toUpperCase();
            return hexMask.substr(0, 6-colorHex.length)+colorHex;
        }
        public static function colorString2uint(color:String):uint{
            try{
                var c:String = color.replace("#","").replace("0x","");
                var u:uint = parseInt( c, 16 );
                return u;
            } catch (e:Error){}
            return 0;
        }

        /**
         * Return color that is lighter than given color
         */
        public static function makeSimpleLighter(color:uint, percentage:int=10):uint{
            if(percentage>100) percentage = 100;
            if(percentage<0) percentage = 0;
            var value:Number = (100.0 + (percentage as Number))/100.0;
            return changeBrightness(color, value);
        }

        /**
         * Return color that is darker than given color
         */
        public static function makeSimpleDarker(color:uint, percentage:int=10):uint{
            if(percentage>100) percentage = 100;
            if(percentage<0) percentage = 0;
            var value:Number = (100.0 - (percentage as Number))/100.0;
            return changeBrightness(color, value);
        }

        /**
         * Multiply all color factors by given value and normalize them
         * @param color
         * @param value
         */
        private static function changeBrightness(color:uint, value:Number):uint{
            var element:Array = [ ((color >> 16) & 0x0000FF) * value, ((color >> 8) & 0x0000FF) * value, (color & 0x0000FF) * value ];      // [ R, G, B ]
            var len:int = element.length;
            for (var i:int; i<len; i++){
                if(element[i]<0)    element[i] = 0;
                if(element[i]>255)  element[i] = 255;
            }
            return  (element[0] << 16) + (element[1] << 8) + (element[2]);
        }

        /**
         * Return color that is lighter than given color
         */
        public function makeLighter(percentage:int=10):Color{
            if(percentage>100) percentage = 100;
            if(percentage<0) percentage = 0;
            var value:Number = (100.0 + (percentage as Number))/100.0;
            return this.changeNumericBrightness(value);
        }
        public static function makeLighter(color:Color, percentage:int=10):Color{
            return color.makeLighter(percentage);
        }

        /**
         * Return color that is darker than given color
         */
        public function makeDarker(percentage:int=10):Color{
            if(percentage>100) percentage = 100;
            if(percentage<0) percentage = 0;
            var value:Number = (100.0 - (percentage as Number))/100.0;
            return this.changeNumericBrightness(value);
        }
        public static function makeDarker(color:Color, percentage:int=10):Color{
            return color.makeDarker(percentage);
        }
        
        /**
         * Return color that is desaturated
         */
        public function desaturate(percentage:int=10):Color{
            if(percentage>100) percentage = 100;
            if(percentage<0) percentage = 0;
            var value:Number = (100.0 - (percentage as Number))/100.0;
            return this.changeNumericSaturation(value);
        }
        public static function desaturate(color:Color, percentage:int=10):Color{
            return color.desaturate(percentage);
        }

        /**
         * Return color that is more saturated
         */
        public function saturate(percentage:int=10):Color{
            if(percentage>100) percentage = 100;
            if(percentage<0) percentage = 0;
            var value:Number = (100.0 + (percentage as Number))/100.0;
            return this.changeNumericSaturation(value);
        }
        public static function saturate(color:Color, percentage:int=10):Color{
            return color.saturate(percentage);
        }


        /**
         * change 'lightness' factor
         * @param color
         * @param value
         * @return
         */
        private function changeNumericBrightness(value:Number):Color{
            var newColor:Color = new Color();
            newColor.setHSL( this.hue_, this.saturationHSL_, this.lightness_*value );
            return newColor;
        }

        /**
         * change 'saturation' factor
         * @param color
         * @param value
         * @return
         */
        private function changeNumericSaturation(value:Number):Color{
            var newColor:Color = new Color();
            newColor.setHSL( this.hue_, this.saturationHSL_*value, this.lightness_ );
            return newColor;
        }        
        //
        //---------------------------------------------------------

    }

}