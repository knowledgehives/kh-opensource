/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {

import com.kh.ssme.model.entity.CalendarEntity;
import com.kh.ssme.util.HashArray;
    import com.kh.ssme.util.JSONObject;

    public class UserEntity extends BasicEntity implements IEntity {

        public var login:String;
        public var name:String;
        public var surname:String;
        public var mail:String;
        public var mobile:String;

        public var rolesEntities:HashArray;
        public var rolesNames:HashArray;        

        public var groupsEntities:HashArray;
        public var groupsNames:HashArray;

        public var ownedGroupsEntities:HashArray;
        public var ownedGroupsNames:HashArray;

        public var calendarsEntities:HashArray;  
        public var calendarsNames:HashArray;

        public var eventingsEntities:HashArray;
        public var eventingsNames:HashArray;        

        // additional
        public var fullName:String;

        //------------------------------------------------        

        protected static const login_field:String = 'login';
        protected static const name_field:String = 'name';
        protected static const surname_field:String = 'surname';
        protected static const mail_field:String = 'mail';
        protected static const mobile_field:String = 'mobile';
        protected static const roles_field:String = 'roles';
        protected static const groups_field:String = 'groups';
        protected static const ownedGroups_field:String = 'ownedGroups';
        protected static const calendars_field:String = 'calendars';
        protected static const eventings_field:String = 'eventings';
        
        //------------------------------------------------        

        public function UserEntity() {
            super();
        }

        public override function clone(entity:* = null):*{
            var newData:UserEntity = super.clone( (entity)?entity:new UserEntity() );

            // plain
            newData.login = login;
            newData.name = name;
            newData.surname = surname;
            newData.mail = mail;
            newData.mobile = mobile;

            // hasharrays
            newData.rolesEntities = (rolesEntities) ? rolesEntities.clone() : null;
            newData.rolesNames = (rolesNames) ? rolesNames.clone() : null;
            newData.groupsEntities = (groupsEntities) ? groupsEntities.clone() : null;
            newData.groupsNames = (groupsNames) ? groupsNames.clone() : null;
            newData.ownedGroupsEntities = (ownedGroupsEntities) ? ownedGroupsEntities.clone() : null;
            newData.ownedGroupsNames = (ownedGroupsNames) ? ownedGroupsNames.clone() : null;
            newData.calendarsEntities = (calendarsEntities) ? calendarsEntities.clone() : null;
            newData.calendarsNames = (calendarsNames) ? calendarsNames.clone() : null;
            newData.eventingsEntities = (eventingsEntities) ? eventingsEntities.clone() : null;
            newData.eventingsNames = (eventingsNames) ? eventingsNames.clone() : null;            

            // helpers
            newData.fullName = fullName;

            return newData;
        }

        public static function createNew(login:String, name:String, surname:String):UserEntity{
            var result:UserEntity = new UserEntity();

            result.login = login;
            result.name = name;
            result.surname = surname;
            result.mail = "";
            result.mobile = "";

            result.rolesEntities = new HashArray();
            result.rolesNames = new HashArray();
            result.groupsEntities = new HashArray();
            result.groupsNames = new HashArray();
            result.ownedGroupsEntities = new HashArray();
            result.ownedGroupsNames = new HashArray();
            result.calendarsEntities = new HashArray();
            result.calendarsNames = new HashArray();
            result.eventingsEntities = new HashArray();
            result.eventingsNames = new HashArray();            

            // helpers            
            result.fullName = name+" "+surname+" ("+login+")";
            return result;
        }        

        //------------------------------------------------        

        public override function parseJSONObject(json:Object, recursive:int = 0):* {
            super.parseJSONObject( json );
            var i:int = 0;

            // login
            login = json[login_field];

            // name
            name = json[name_field];

            // surname
            surname = json[surname_field];

            // mail
            mail = json[mail_field];

            // mobile
            mobile = json[mobile_field];

            // roles
            rolesEntities = new HashArray();
            rolesNames = new HashArray();
            if(json[roles_field].length>0){
                if(recursive>0){
                    var role:RoleEntity;
                    for(i = 0; i<json[roles_field].length; i++){
                        role = (new RoleEntity()).parseJSONObject( (json[roles_field][i]), recursive-1 );
                        rolesEntities.put( role.UUID, role );
                        rolesNames.put( role.UUID, role.role );
                    }
                } else {
                    for(i = 0; i<json[roles_field].length; i++){
                        rolesEntities.put( (json[roles_field][i])[uuid_field], null );
                        rolesNames.put( (json[roles_field][i])[uuid_field], (json[roles_field][i])[readableName_field] );
                    }
                }
            }

            //groups
            groupsEntities = new HashArray();
            groupsNames = new HashArray();
            if(json[groups_field].length>0){
                if(recursive>0){
                    var grp:GroupEntity;
                    for(i = 0; i<json[groups_field].length; i++){
                        grp = (new GroupEntity()).parseJSONObject( (json[groups_field][i]), recursive-1 );
                        groupsEntities.put( grp.UUID, grp );
                        groupsNames.put( grp.UUID, grp.name );
                    }
                } else {
                    for(i = 0; i<json[groups_field].length; i++){
                        groupsEntities.put( (json[groups_field][i])[uuid_field], null );
                        groupsNames.put( (json[groups_field][i])[uuid_field], (json[groups_field][i])[readableName_field] );
                    }
                }
            }

            // ownedGroups
            ownedGroupsEntities = new HashArray();
            ownedGroupsNames = new HashArray();
            if(json[ownedGroups_field].length>0){
                if(recursive>0){
                    var ogrp:GroupEntity;
                    for(i = 0; i<json[ownedGroups_field].length; i++){
                        ogrp = (new GroupEntity()).parseJSONObject( (json[ownedGroups_field][i]), recursive-1 );
                        ownedGroupsEntities.put( ogrp.UUID, ogrp );
                        ownedGroupsNames.put( ogrp.UUID, ogrp.name );
                    }
                } else {
                    for(i = 0; i<json[ownedGroups_field].length; i++){
                        ownedGroupsEntities.put( (json[ownedGroups_field][i])[uuid_field], null );
                        ownedGroupsNames.put( (json[ownedGroups_field][i])[uuid_field], (json[ownedGroups_field][i])[readableName_field] );
                    }
                }
            }            

            // calendars
            calendarsEntities = new HashArray();
            calendarsNames = new HashArray();
            if(json[calendars_field].length>0){
                if(recursive>0){
                    var cal:CalendarEntity;
                    for(i = 0; i<json[calendars_field].length; i++){
                        cal = (new CalendarEntity()).parseJSONObject( (json[calendars_field][i]), recursive-1 );
                        calendarsEntities.put( cal.UUID, cal );
                        calendarsNames.put( cal.UUID, cal.name );
                    }
                } else {
                    for(i = 0; i<json[calendars_field].length; i++){
                        calendarsEntities.put( (json[calendars_field][i])[uuid_field], null );
                        calendarsNames.put( (json[calendars_field][i])[uuid_field], (json[calendars_field][i])[readableName_field] );                       
                    }
                }
            }

            // eventings
            eventingsEntities = new HashArray();
            eventingsNames = new HashArray();
            if(json[eventings_field].length>0){
                if(recursive>0){
                    var ev:EventingEntity;
                    for(i = 0; i<json[eventings_field].length; i++){
                        ev = (new EventingEntity()).parseJSONObject( (json[eventings_field][i]), recursive-1 );
                        eventingsEntities.put( ev.UUID, ev );
                        eventingsNames.put( ev.UUID, "Eventing:"+ev.UUID );
                    }
                } else {
                    for(i = 0; i<json[eventings_field].length; i++){
                        eventingsEntities.put( (json[eventings_field][i])[uuid_field], null );
                        eventingsNames.put( (json[eventings_field][i])[uuid_field], (json[eventings_field][i])[readableName_field] );
                    }
                }
            }                

            fullName = name+" "+surname+" ("+login+")";            
            return this;
        }

        public override function toJSONObject():Object{
            var json:Object = super.toJSONObject();
            var tab:Array = new Array();
            var key:String;              

            // login
            json[login_field] = login;

            // name
            json[name_field] = name;

            //surname
            json[surname_field] = surname;

            // mail
            json[mail_field] = mail;

            // mobile
            json[mobile_field] = mobile;

            // roles
            tab = new Array();
            for (key in rolesNames){
                tab.push( { uuid:key, name:rolesNames[key] } );
            }
            json[roles_field] = tab;            

            // groups
            tab = new Array();
            for (key in groupsNames){
                tab.push( { uuid:key, name:groupsNames[key] } );
            }
            json[groups_field] = tab;

            // ownedGroups
            tab = new Array();
            for (key in ownedGroupsNames){
                tab.push( { uuid:key, name:ownedGroupsNames[key] } );
            }
            json[ownedGroups_field] = tab;

            // calendars
            tab = new Array();
            for (key in calendarsNames){
                tab.push( { uuid:key, name:calendarsNames[key] } );
            }
            json[calendars_field] = tab;

           // eventings
            tab = new Array();
            for (key in eventingsNames){
                tab.push( { uuid:key, name:"" } );
            }
            json[eventings_field] = tab;                

            return json;
        }
   
    }

}