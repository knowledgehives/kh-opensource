/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.util {
import flash.utils.describeType;

public class ObjectUtil {

        public function ObjectUtil() {
        }

        /**
         * Tries to clone given object and result clone as a result, if operation is not possible then prototype object is returned  
         * @param object
         * @return
         */
        public static function tryClone(object:*):*{
            if(isCloneable(object)){
                try{
                    //try to run clone() method and return it's result
                    return object['clone']();
                } catch(e:Error){
                    // hmm, it wasn't clone-able, whatsoever
                }
            }
            return object;  // return prototype object
        }

        /**
         * Check if given object is "cloneable" (has method named 'clone')
         * @param object
         * @return
         */
        public static function isCloneable(object:*):Boolean{
            if(object!=null){
                var metaInfo:XML = describeType( object );
                var hasMethod:XMLList = metaInfo.method.(@name == "clone");
                return (hasMethod != null && hasMethod.length()>0);
            } else {
                return false;
            }
        }

    }

}