/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * On the base of original flex 3.5 mx.controls.NumericStepper class
 */
package com.kh.ssme.components {

    import mx.controls.NumericStepper;
    import mx.core.mx_internal;
    import mx.events.FlexEvent;

    public class HiddenNumericStepper extends NumericStepper {
        public function HiddenNumericStepper() {
            super();
            this.addEventListener( FlexEvent.INITIALIZE, onInit )            
        }

        private function onInit(event:FlexEvent):void {
            mx_internal::inputField.width = mx_internal::inputField.maxWidth = 0;
            mx_internal::inputField.visible = mx_internal::inputField.includeInLayout = false;
            //lastValue = currentValue = min;
        }

        override protected function createChildren():void{
            super.createChildren();
        }

        /**
         *  @private
         *  Return the preferred sizes of the stepper.
         */
        override protected function measure():void{
            super.measure();

            mx_internal::inputField.width = 0;
            mx_internal::inputField.height = 0;

            var buttonHeight:Number = mx_internal::prevButton.getExplicitOrMeasuredHeight() +
                                      mx_internal::nextButton.getExplicitOrMeasuredHeight();
            var h:Number = Math.max(20, buttonHeight);


            var buttonWidth:Number = Math.max(mx_internal::prevButton.getExplicitOrMeasuredWidth(),
                                              mx_internal::nextButton.getExplicitOrMeasuredWidth());
            var w:Number = Math.max(20, buttonWidth);

            measuredMinWidth = 20;
            measuredMinHeight = 20;
            measuredWidth = w;
            measuredHeight = h;
        }

        /**
         *  @private
         *  Place the buttons to the right of the text field.
         */
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
            super.updateDisplayList(unscaledWidth, unscaledHeight);

            var w:Number = mx_internal::nextButton.getExplicitOrMeasuredWidth();
            var h:Number = Math.round(unscaledHeight / 2);
            var h2:Number = unscaledHeight - h;

            mx_internal::nextButton.x = 0;
            mx_internal::nextButton.y = 0;
            mx_internal::nextButton.setActualSize(w, h2);

            mx_internal::prevButton.x = 0;
            mx_internal::prevButton.y = unscaledHeight - h;
            mx_internal::prevButton.setActualSize(w, h);

            mx_internal::inputField.setActualSize(0,0);

            this.toolTip = "this.width:"+this.width+", "+
                            "this.height:"+this.height+", \n"+
                            "nextButton.width:"+mx_internal::nextButton.width+", "+
                            "nextButton.height:"+mx_internal::nextButton.height+", \n"+
                            "prevButton.width:"+mx_internal::prevButton.width+", "+
                            "prevButton.height:"+mx_internal::prevButton.height+", \n"+
                            "inputField.width:"+mx_internal::inputField.width+", "+
                            "inputField.height:"+mx_internal::inputField.height+", \n"+
                           ""

        }


    }
}