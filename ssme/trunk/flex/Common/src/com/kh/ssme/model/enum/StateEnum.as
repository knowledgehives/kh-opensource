/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.enum {
import com.kh.ssme.util.Color;
import com.kh.ssme.util.HashArray;

    public final class StateEnum {

        public static const ACCEPTED:StateEnum  = new StateEnum("accepted", "Accepted", 0x00ee00, 'A', 0);
        public static const REJECTED:StateEnum  = new StateEnum("rejected", "Rejected", 0xee0000, 'R', 1);
        public static const TENTATIVE:StateEnum = new StateEnum("tentative", "Tentative", 0xff7800, 'T', 2);
        //------------------------------------------------------
	    public static const DECIDED:StateEnum   = new StateEnum("decided", "Decided", 0xaaaaaa, 'D', 3);
        public static const UNDECIDED:StateEnum = new StateEnum("undecided", "Undecided", 0xaaaaaa, 'U', 4);

		private static const VALUES:Object = new Object();
        {
            //static initialization
            VALUES[ACCEPTED.state_]  = ACCEPTED;
            VALUES[REJECTED.state_]  = REJECTED;
            VALUES[TENTATIVE.state_] = TENTATIVE;
            VALUES[DECIDED.state_]   = DECIDED;
            VALUES[UNDECIDED.state_] = UNDECIDED;
        }
		public static function find(id:String):StateEnum{
			return VALUES[id];
		}

		private static const VALUES_BY_MNE:Object = new Object();
        {
            //static initialization
            VALUES_BY_MNE[ACCEPTED.mnemonic_]   = ACCEPTED;
            VALUES_BY_MNE[REJECTED.mnemonic_]   = REJECTED;
            VALUES_BY_MNE[TENTATIVE.mnemonic_]  = TENTATIVE;
            VALUES_BY_MNE[DECIDED.mnemonic_]    = DECIDED;
            VALUES_BY_MNE[UNDECIDED.mnemonic_]  = UNDECIDED;
        }
		public static function findByMnemonic(id:String):StateEnum{
			return VALUES_BY_MNE[id];
		}

		//-------------------------------------------

		private var state_:String;
        private var uiText_:String;
        private var color_:uint;
        private var mnemonic_:String;
        private var index_:int;

        public function StateEnum(state:String, uiText:String, color:uint, mnemonic:String, index:int) {
            state_ = state;
            uiText_ = uiText;
            color_ = color;
            mnemonic_ = mnemonic;
            index_ = index;
        }

		public function get state():String{
			return this.state_;
		}
		public function get uiText():String{
			return this.uiText_;
		}
        public function get color():uint{
            return this.color_;
        }
        public function get mnemonic():String{
            return this.mnemonic_;
        }
        public function get index():int{
            return this.index_;
        }

		public function toString():String{
			return "StateEnum:{state:"+this.state_+", color: "+Color.colorUint2hex(this.color)+", mnemonic:"+this.mnemonic_+", index:"+this.index_+"}";
		}
        
		public function equals(type:StateEnum):Boolean{
			return (type!=null && this.state_ == type.state);
		}

    }

}