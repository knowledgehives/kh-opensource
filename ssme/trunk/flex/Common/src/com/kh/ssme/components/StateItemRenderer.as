/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
import com.kh.ssme.model.enum.StateEnum;

import com.kh.ssme.util.Color;

import mx.containers.VBox;
    import mx.controls.Label;
import mx.controls.dataGridClasses.DataGridListData;
import mx.controls.listClasses.BaseListData;
import mx.controls.listClasses.IDropInListItemRenderer;
import mx.core.IDataRenderer;
import mx.core.IFactory;

    public class StateItemRenderer extends VBox implements IDataRenderer, IFactory, IDropInListItemRenderer{

        private var label_:Label;        

        public function StateItemRenderer() {
            super();
            setStyle("borderThickness", 1);
            horizontalScrollPolicy="off";
            verticalScrollPolicy="off";

            label_ = new Label();
            setStyle("textAlign","center");
            height=20;     
            addChild( label_ )
        }

        private var itemData_:StateEnum;        
        public override function set data(value:Object):void{
            super.data = value;
            if(data && listData_){
                itemData_ = value[ listData_.dataField ];
                invalidateItem();
            }            

        }

        private function invalidateItem():void {
            if(itemData_){
                //this
                setStyle("backgroundColor", itemData_.color);
                setStyle("borderColor", Color.makeSimpleDarker(itemData_.color, 20) );
                this.toolTip = itemData_.toString();
                //label
                if( label_ ){
                    label_.text = itemData_.uiText;
                    label_.setStyle( "color", Color.negativeTextColor(itemData_.color) );
                }
                invalidateDisplayList();                
            }
        }        

        private var listData_:DataGridListData;
        public function set listData(value:BaseListData):void {
            listData_ = (value as DataGridListData);
        }          
        public function get listData():BaseListData {
            return listData_;
        }

        public function newInstance():* {
            return new StateItemRenderer();
        }        

    }
}