/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity.meta {
    import com.kh.ssme.model.entity.*;
    import com.kh.ssme.model.enum.RequestHintsEnum;
    import com.kh.ssme.util.HashArray;
    import com.kh.ssme.util.JSONObject;

    public final class RequestHintsCollectionEntity implements IEntity{

        public var hints:HashArray;
               
        //------------------------------------------------

        public function RequestHintsCollectionEntity() {
            hints = new HashArray();
        }
        public function toString():String{
            var result:String = "RequestHintsCollectionEntity:\n";
            for( var i:int = 0; i<hints.size ; i++){
                result += "["+i+"] : ["+hints.getKeyAt(i)+"] : {"+hints.getValueAt(i)+"}\n"
            }
            return result;
        }

        //------------------------------------------------            

        public function clone(entity:* = null):* {
            var newData:* = (entity) ? (entity) : new RequestHintsCollectionEntity();            
            newData.hints = (hints) ? hints.clone() : null;
            return newData;
        }        

        public function parseJSONObject(json:Object, recursive:int = 0):* {
            var he:RequestHintsEnum;
            var rhe:RequestHintEntity;

            hints = new HashArray();
            if(json){
                for( var hintType:String in json ){
                    he = RequestHintsEnum.find(hintType);
                    if( he!=null ){
                        rhe = (new RequestHintEntity(he)).parseJSONObject( json[hintType] ); 
                        hints.put( hintType, rhe );
                    }
                }                
            }

            return this;
        }

        public function toJSONObject():Object{
            // read only
            return "";
        }

        public function parseJSONString(json:String, recursive:int = 0):* {
            var jsonEntity:Object = JSONObject.decode(json);
            return parseJSONObject( jsonEntity, recursive );
        }

        public function toJSONString():String {
            var jsonObject:Object = toJSONObject();
            var result:String = JSONObject.encode( jsonObject );
            return result;
        }   

    }

}
