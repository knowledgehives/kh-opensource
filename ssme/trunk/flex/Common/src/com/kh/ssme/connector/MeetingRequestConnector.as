/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.connector {
    import com.kh.ssme.model.entity.MeetingRequestEntity;

	[Event(name="ssme_requestAll_objectGet", type="com.kh.ssme.util.SsmeEvent")]

    public class MeetingRequestConnector extends BasicQueuedConnector {

        /**
         * Path to proper servlet
         */
        private static const meetingRequestServlet:String = "meetingRequest/";
        public override function get servlet():String{
            return meetingRequestServlet;
        }
        public override function getResourceURL(uuid:String):String{
            return baseURL + meetingRequestServlet + uuid;
        }

        public static const EVENT_GET_COLLECTION:String = "ssme_requestAll_objectGet";

        public function MeetingRequestConnector(url:String) {
            super(url);
        }

        public function getMeetingRequest( uuid:String, params:Object = null ):void{
            processCommand( ACTION_GET, EVENT_GET, uuid, null, params );
        }

        public function getAllMeetingRequest( params:Object = null ):void{
            processCommand( ACTION_GET, EVENT_GET_COLLECTION, null, null, params );
        }

        public function getAllActiveMeetingRequest( params:Object = null ):void{
            if(!params) params = {};
            params[Params.ACTIVE_REQUEST] = true;
            processCommand( ACTION_GET, EVENT_GET_COLLECTION, null, null, params );
        }

        public function addMeetingRequest( data:MeetingRequestEntity, params:Object = null ):void{
            processCommand( ACTION_CREATE, EVENT_CREATE, null, (data)?data.toJSONString():emptyJSON, params );
        }

        public function updateMeetingRequest( uuid:String, data:MeetingRequestEntity = null, params:Object = null ):void{
            processCommand( ACTION_UPDATE, EVENT_UPDATE, uuid, (data)?data.toJSONString():emptyJSON, params );
        }

        public function deleteMeetingRequest( uuid:String, params:Object = null ):void{
            processCommand( ACTION_DELETE, EVENT_DELETE, uuid, null, params );
        }

        public function updateMeetingRequestToTimeFrame( uuid:String, from:Date, to:Date, params:Object = null ):void{
            if(!params) params = {};
            params[Params.UPDATE_REQUEST_TO_EVENT] = true;
            params[Params.CALENDAR_FROM] = from.time;
            params[Params.CALENDAR_TO] = to.time;                   
            processCommand( ACTION_UPDATE, EVENT_UPDATE, uuid, emptyJSON, params );
        }        

        public function deleteMeetingRequestWithChildren( uuid:String, params:Object = null ):void{
            if(!params) params = {};
            params[Params.REMOVE_REQUEST_CHILDREN] = true;
            processCommand( ACTION_DELETE, EVENT_DELETE, uuid, null, params );
        }


    }

}