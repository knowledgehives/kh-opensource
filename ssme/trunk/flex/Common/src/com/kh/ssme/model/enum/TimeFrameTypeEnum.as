/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.enum {

    public final class TimeFrameTypeEnum {

		public static const AVAILABLE:TimeFrameTypeEnum = new TimeFrameTypeEnum("available");
	    public static const REQUEST:TimeFrameTypeEnum   = new TimeFrameTypeEnum("request");
        // TODO: [...]

        private static const VALUES:Object = new Object();
        {
            // static initialization
            VALUES[AVAILABLE.timeFrameType] = AVAILABLE;
            VALUES[REQUEST.timeFrameType]   = REQUEST;
        }

        public static function find(id:String):TimeFrameTypeEnum {
            return VALUES[id];
        }

        // ---------- private parts ---------------
        private var timeFrameType_:String;

        public function TimeFrameTypeEnum(timeFrameType:String) {
            this.timeFrameType_ = timeFrameType;
        }

        public function get timeFrameType():String {
            return this.timeFrameType_
        }

        public function toString():String {
            return this.timeFrameType_;
        }

        public function equals(type:TimeFrameTypeEnum):Boolean {
            return (type!=null && this.timeFrameType_ == type.timeFrameType_);
        }

    }

}