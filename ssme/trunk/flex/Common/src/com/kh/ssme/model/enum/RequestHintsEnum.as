/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.enum {

    public class RequestHintsEnum {

		public static const SIMPLE:RequestHintsEnum                         = new RequestHintsEnum("simple");
        public static const WEIGHTED_PARTICIPANT:RequestHintsEnum           = new RequestHintsEnum("weightedParticipant");
        public static const WEIGHTED_STATE:RequestHintsEnum                 = new RequestHintsEnum("weightedState");
        public static const WEIGHTED_PARTICIPANT_STATE:RequestHintsEnum     = new RequestHintsEnum("weightedParticipantAndState");
        
        public static const VALUES:Object = new Object();
        {
            // static initialization
            VALUES[SIMPLE.type]                     = SIMPLE;
            VALUES[WEIGHTED_PARTICIPANT.type]       = WEIGHTED_PARTICIPANT;
            VALUES[WEIGHTED_STATE.type]             = WEIGHTED_STATE;
            VALUES[WEIGHTED_PARTICIPANT_STATE.type] = WEIGHTED_PARTICIPANT_STATE;
        }

        public static function find(id:String):RequestHintsEnum {
            return VALUES[id];
        }


        // ---------- private parts ---------------
        private var type_:String;

        public function RequestHintsEnum(type:String) {
            this.type_ = type;
        }

        public function get type():String {
            return this.type_;
        }

        public function toString():String {
            return this.type_;
        }

        public function equals(hintType:RequestHintsEnum):Boolean {
            return (hintType != null && this.type_ == hintType.type_);
        }        

    }

}