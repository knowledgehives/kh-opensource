/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.util {
import com.kh.ssme.util.ObjectUtil;

/**
     *
     */
    public dynamic class HashArray extends Object {

		/**
		 * Map to keep an ordered index of keys.
		 */
        private var keys_:Array;
		/**
		 * Size
		 */
        private var size_:int;
		/**
		 * Array of Objects with pairs <key, value>
		 */
        private var array_:Array;


		/**
		 * Constructor - initializes size to zero, and initializes empty index of keys
		 */
        public function HashArray() {
			this.size_ = 0;
            this.keys_ = [ ];
            this.array_ = [ ];
		}

		/**
		 * Adds association key-value
		 */
		public function put(key:*, value:*):Object{
            var contains:Boolean = containsKey(key);
            //trace(" contains:"+contains+" - "+key+":"+value);
			if (!contains){
                this.keys_.push( key );                         // add to keys array
                this.array_.push( { key: key, value: value } ); // add to pairs array
                this.size_++;
			}
			return (this[key] = value);     // add to associative object
		}

		/**
		 * Deletes item under key
		 */
		public function deleteWithKey(key:*):void{
            if(containsKey(key)){
                var index:int = keys_.indexOf(key); // splice index
                keys_.splice( index, 1);            // remove from keys array
                array_.splice( index, 1);           // remove from pairs array
                delete this[ key ];                 // remove from associative object
                this.size_--;
            }
		}

		/**
		 * Deletes item at specific position
		 */
		public function deleteAt(index:int):void{
            if(index < this.size_){
                delete this[ keys_[index] ];    // remove from associative object
                keys_.splice( index, 1 );       // remove from keys array
                array_.splice( index, 1 );      // remove from pairs array
                this.size_--;
            }
		}


		/**
		 * Retrieves element at index idx
		 */
		public function getValueAt(idx:int):*{
			return (idx < this.size_) ? this[this.keys_[idx]] : null;
		}


		/**
		 * Retrieves key of element at index idx
		 */
		public function getKeyAt(idx:int):*{
			return (idx < this.size_) ? this.keys_[idx] : null;
		}

		public function get(key:*):*{
			return this[key];
		}


        /**
         * Check if exist item with given key
         */
        public function containsKey(key:*):Boolean{
            return (this[key] && this[key]!= null);
        }


		/**
		 * Returns the size of the array
		 */
		public function get size():int{
            return this.size_;
		}
		/**
		 * Returns the size of the array
		 */
		public function get length():int{
            return this.size_;
		}


		/**
		 * Clears information stored in this map
		 */
		public function clear():void
		{
            this.size_ = 0;
            for each(var key:* in keys_){
                delete this[ key ];
			}
            keys_ = [];
            array_ = [];
            this.size_ = 0;
		}

        /**
         * Returns representation of HashArray as Array containing pairs <key,value>
         */
		public function getAsArray():Array{
            return array_;
		}

        /**
         * Returns representation of HashArray as Array containing pairs <key,value>
         */
		public function getKeysArray():Array{
            return keys_;
		}

        /**
         * Returns representation of HashArray as Array containing pairs <key,value>
         */
		public function getValuesArray():Array{
            var tempArray:Array = [];
            for each(var key:* in keys_){
                tempArray.push( this[ key ] );
			}
            return tempArray;
		}

        /**
         * Clones this object
         * @return clone
         */
        public function clone():HashArray{
            var ret:HashArray = new HashArray();
            var item:*;
            for(var i:int=0; i<keys_.length; i++){
                item = getValueAt(i);
                ret.put( getKeyAt(i), ObjectUtil.tryClone(item) );   // 'clone' recursively
            }
            return ret;
        }


    }

}