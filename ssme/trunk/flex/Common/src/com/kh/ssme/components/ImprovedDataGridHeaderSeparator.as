/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
    import flash.display.Graphics;

    import mx.skins.ProgrammaticSkin;

    public class ImprovedDataGridHeaderSeparator extends ProgrammaticSkin{

        public function ImprovedDataGridHeaderSeparator() {
		    super();            
        }

        override protected function updateDisplayList(w:Number, h:Number):void
        {
            super.updateDisplayList(w, h);
            var g:Graphics = graphics;
            g.clear();

            // Highlight
//            g.lineStyle(getStyle("borderThickness"), 0xFFFFFF, 0.5);
//            g.moveTo(0, 0);
//            g.lineTo(0, h);
//            g.lineStyle(getStyle("borderThickness"), getStyle("borderColor"), getStyle("borderAlpha"));
//            g.moveTo(1, 0);
//            g.lineTo(1, h);
        }
    }

}