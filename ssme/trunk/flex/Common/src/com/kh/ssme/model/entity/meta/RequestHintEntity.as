/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity.meta {
    import com.kh.ssme.model.entity.IEntity;
    import com.kh.ssme.model.enum.RequestHintsEnum;
import com.kh.ssme.util.DateUtil;
import com.kh.ssme.util.HashArray;
import com.kh.ssme.util.JSONObject;

public class RequestHintEntity implements IEntity{

        public var type:RequestHintsEnum;
        public var description:String;
        public var parts:Array;

        //------------------------------------------------

        protected static const description_field:String = 'description';
        protected static const parts_field:String = 'parts';
        protected static const from_field:String = 'from';
        protected static const to_field:String = 'to';    

        //------------------------------------------------        

        public function RequestHintEntity(type:RequestHintsEnum){
            this.type = type;
            parts = [ ];
        }
        public function toString():String{
            var result:String = "RequestHintEntity: description:"+description+", parts:\n";
            for( var i:int = 0; i<parts.length ; i++){
                result += "\t\t\t["+i+"] from:"+parts[i][from_field]+", to:["+parts[i][to_field]+"]\n"
            }
            return result;
        }

        //------------------------------------------------            

        public function clone(entity:* = null):* {
            var newData:* = (entity) ? (entity) : new RequestHintEntity(type);            

            // plain
            newData.description = new String(description);
            // entities
            newData.parts = (parts) ? parts.clone() : null;
            
            return newData;
        }

        public function parseJSONObject(json:Object, recursive:int = 0):* {
            var ob:Object;
            var from:Date, to:Date;

            // map
            description = json[description_field];

            parts = [ ];            
            if(json[parts_field].length>0){
                for(var i:int = 0; i<json[parts_field].length; i++){
                    from = JSONObject.parseString2Date( json[parts_field][i][from_field] );
                    to = JSONObject.parseString2Date( json[parts_field][i][to_field] );
                    ob = { from: from, to: to };
                    parts.push( ob );
                }
            }
            parts.sort( sortParts );

            return this;                  
        }
        protected static function sortParts(a:Object, b:Object):int {            
            return DateUtil.getInstance( a[from_field] ).compare( new DateUtil( b[from_field] ) );
        }

        public function toJSONObject():Object {
            // read only
            return "";
        }

        public function parseJSONString(json:String, recursive:int = 0):* {
            var jsonEntity:Object = JSONObject.decode(json);
            return parseJSONObject( jsonEntity, recursive );
        }

        public function toJSONString():String {
            var jsonObject:Object = toJSONObject();
            var result:String = JSONObject.encode( jsonObject );
            return result;
        }   
    }

}