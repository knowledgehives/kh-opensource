/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.enum {

    public final class PriorityEnum {

		public static const HIGH:PriorityEnum = new PriorityEnum("high", 0xff0000);
        public static const MEDIUM:PriorityEnum = new PriorityEnum("medium", 0xff9900);
        public static const LOW:PriorityEnum = new PriorityEnum("low", 0xffee00);

		private static const VALUES:Object = new Object();
        {
            //static initialization
            VALUES[HIGH.priority] = HIGH;
            VALUES[MEDIUM.priority] = MEDIUM;
            VALUES[LOW.priority] = LOW;
        }

		public static function find(id:String):PriorityEnum
		{
			return VALUES[id];
		}


		// ---------- private parts ---------------
		private var priority_:String;
        private var borderColor_:uint;        
        public function PriorityEnum(priority:String, borderColor:uint) {
            this.priority_ = priority;
            this.borderColor_ = borderColor;
        }

		public function get priority():String{
			return this.priority_;
		}
		public function get borderColor():uint{
			return this.borderColor_;
		}
        
		public function toString():String{
			return this.priority_;
		}
		public function equals(type:PriorityEnum):Boolean
		{
			return (type!=null && this.priority_ == type.priority);
		}

    }

}