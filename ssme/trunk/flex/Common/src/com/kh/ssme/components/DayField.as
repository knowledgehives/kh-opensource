/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
    import com.kh.ssme.util.DayEventsFieldTypeEnum;

    import com.kh.ssme.util.HashArray;

    import mx.core.UIComponent;
    import mx.formatters.DateFormatter;

    public class DayField extends RoundedCornerContainer implements IMonthtDisplayItem {

        protected var dateFormatter:DateFormatter = new DateFormatter();

        public function DayField() {
            super();
            dateFormatter.formatString = "DD MMMM";

        }



        protected var calendarsColorsMap_:HashArray;
        public function set calendarsColorsMap(value:HashArray):void {
            calendarsColorsMap_ = value;
        }
        public function get calendarsColorsMap():HashArray {
            return calendarsColorsMap_;
        }

    
        protected var dataProvider_:HashArray;
        public function set dataProvider(value:HashArray):void {
            dataProvider_ = value;
        }
        public function get dataProvider():HashArray {
            return dataProvider_;
        }

    
        /**
         * currently selected day
         */
        protected var currentDay_:Date;
        public function set currentDay(value:Date):void{
            currentDay_ = value;
        }
        public function get currentDay():Date{
            return currentDay_;
        }

        /**
         * day of the week
         */
        protected var dayOfWeek_:int;
        public function set dayOfWeek(value:int):void{
            dayOfWeek_ = value;
        }
        public function get dayOfWeek():int{
            return dayOfWeek_;
        }

        /**
         * week of the month
         */
        protected var weekOfMonth_:int;
        public function set weekOfMonth(value:int):void{
            weekOfMonth_ = value;
        }
        public function get weekOfMonth():int{
            return weekOfMonth_;
        }

        /**
         * Type of day: common, saturday, sunday
         */
        protected var dayType_:DayEventsFieldTypeEnum;
        public function set dayType(value:DayEventsFieldTypeEnum):void{
            dayType_ = value;
            setStyle("backgroundColor", dayType_.color);
        }
        public function get dayType():DayEventsFieldTypeEnum{
            return dayType_;
        }


        protected var selectedItem_:UIComponent = null;
        public function selectItem(item:UIComponent):void {
            selectedItem_ = item;
        }
        public function deselectedItem():void {
            selectedItem_ = null;
        }
        public function get isSelected():Boolean {
            return selectedItem_!=null;
        }        

    }

}