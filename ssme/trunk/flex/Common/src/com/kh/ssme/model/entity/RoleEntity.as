/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {
    import com.kh.ssme.util.JSONObject;

    public class RoleEntity extends BasicEntity implements IEntity {

        public var login:String;
        public var role:String;

        //------------------------------------------------           

        private const login_field:String = "login";
        private const role_field:String = "role";

        //------------------------------------------------

        public function RoleEntity() {
            super();
        }

        public override function clone( entity:* = null ):*{
            var newData:RoleEntity = super.clone((entity)?entity:new RoleEntity());

            // plain
            newData.login = new String( login );
            newData.role = new String( role );
            
            return newData;
        }

        public static function createNew(login:String, role:String):RoleEntity{
            var result:RoleEntity = new RoleEntity();
            result.login = login;
            result.role = role;
            return result;
        }

        //------------------------------------------------        

        public override function parseJSONObject(json:Object, recursive:int = 0):* {
            super.parseJSONObject( json );
            var i:int = 0;

            // login
            login = json[login_field];

            // role
            role = json[role_field];

            return this;
        }

        public override function toJSONObject():Object{
            var json:Object = super.toJSONObject();

            // login
            json[login_field] = login;

            // role
            json[role_field] = role;

            return json;
        }

    }

}