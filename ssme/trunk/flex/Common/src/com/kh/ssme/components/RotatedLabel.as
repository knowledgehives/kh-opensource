package com.kh.ssme.components {
/**
 * MIT License
 Copyright (c) 2009 Doug Marttila

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 */

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.DisplayObject;
import flash.geom.Rectangle;

import mx.controls.Label;
import mx.core.UIComponent;

    /**
     * Creates a Label that can be rotated w/o embedding fonts. Works by
     * creating a bitmap of the text and rotating that. The downside is the user
     * can't directly edit or select the text.
     * Something similar to this is implemented in Flex Charting axis labels.
     * That one's a bit smarter though as it checks for embedded fonts and
     * doesn't create the bitmap if they are embedded.
     */
    public class RotatedLabel extends Label
    {
        /**
         * Need to store the rotation and set it in update display list
         * The rotation is only set on the bitmap
         */
        override public function set rotation(value:Number):void {
            _myRotation = value;
            invalidateDisplayList();
        }

        /**
         * Creates the bitmap if it doesn't exist (or recreates it if the
         * text changes). And sets the rotation on the bitmap. Bitmap
         * has to be created on updateDisplay list otherwise the label
         * isn't created and there's nothing to create a bitmap of.
         */
        override protected function updateDisplayList(unscaledWidth:Number,
                                                      unscaledHeight:Number):void
        {
            super.updateDisplayList(unscaledWidth, unscaledHeight);
            if (_updateBitmap) _createTheBitmap();
            _updateBitmap = false;
            _bitmap.rotation = _myRotation;
        }

        /**
         * Create a new bitmap whenever the text changes.
         */
        override public function set text(value:String):void {
            super.text = value;
            _updateBitmap = true;
        }

        /**
         * Does the bitmap need to be recreated on the next screen draw?
         */
        private var _updateBitmap:Boolean = true;
        private var _myRotation:Number;

        private var _bitmapContainer:UIComponent;
        private var _bitmap:Bitmap;
        private var _textBitmap:BitmapData;

        private function _createBitmapContainer():void {
            _bitmapContainer = new UIComponent()
            addChild(_bitmapContainer);
        }

        private function _createTheBitmap():void {
            if (!_bitmapContainer) _createBitmapContainer();
            //get rid of the old bitmap - seems like there should be a way
            //to clear BitmapData or Bitmap, but, I couldn't get it to work
            if (_textBitmap) {
                _textBitmap.dispose();
                _bitmapContainer.removeChild(_bitmap);
            }
            _textBitmap = new BitmapData(width, height);
            _bitmap = new Bitmap(_textBitmap);
            _bitmap.smoothing = true;
            _bitmapContainer.addChild(DisplayObject(_bitmap));

            _textBitmap.fillRect(new Rectangle(0, 0, width, height), 0);
            _textBitmap.draw(this);

            //need to remove the actual text
            text = '';
        }
    }

}