/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {

    import com.kh.ssme.model.entity.TimeFrameEntity;
    import com.kh.ssme.util.SsmeEvent;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import mx.core.UIComponent;
    import mx.events.ToolTipEvent;

    [Event(name="ssme_itemSelected", type="flash.events.Event")]

    [Exclude(name="backgroundColor", kind="style")]
    [Exclude(name="borderColor", kind="style")]

    [Style(name="backgroundDeselectedColor", type="uint", format="Color", inherit="no")]   
    [Style(name="borderDeselectedColor", type="uint", format="Color", inherit="no")]
    [Style(name="backgroundSelectedColor", type="uint", format="Color", inherit="no")]
    [Style(name="borderSelectedColor", type="uint", format="Color", inherit="no")]
    
    public class EventField extends RoundedCornerContainer implements IEventDisplayObject{

        public function EventField() {
            this.visible = this.includeInLayout = true;

            eventInfoTextArea = new SimpleEventDisplay();
            eventInfoTextArea.x = eventInfoTextArea.y = 0;
            eventInfoTextArea.visible = eventInfoTextArea.includeInLayout = true;            
            this.addChild( eventInfoTextArea );

            this.toolTip = " ";
            this.addEventListener( ToolTipEvent.TOOL_TIP_CREATE, createEventTooltip);
            this.mouseChildren = false;
            this.addEventListener( MouseEvent.CLICK, itemClicked );

        }


        protected var eventInfoTextArea:SimpleEventDisplay;
        protected var eventData_:TimeFrameEntity;
        public function set eventData(value:TimeFrameEntity):void{
            this.eventData_ = value;

            //prepare text to be displayed            
            if(value){
                eventInfoTextArea.text = "";
                eventInfoTextArea.text += eventData_.title+"\n";
                eventInfoTextArea.text += eventInfoTextArea.timeFormatter.format( eventData_.from );
                eventInfoTextArea.text += " - "+eventInfoTextArea.timeFormatter.format( eventData_.to )+"\n";
                eventInfoTextArea.text += eventData_.description+"\n";
                // tags
                if(eventData_.tagsNames && eventData_.tagsNames.size>0){
                    eventInfoTextArea.text += "["+(eventData_.tagsNames.getValuesArray().join("]["))+"]\n";
                }
            }              
        }        
        public function get eventData():TimeFrameEntity{
            return eventData_;
        }

        /**
         * Index of beginning quarter
         */
        public var beginIndex:int;

        /**
         * Index of ending quarter
         */
        public var endIndex:int;

        /**
         * number of neighbour events
         */
        public var neighbours:int;

        /**
         * Component position rank
         */
        public var rank:int;

        /**
         * Component position rank multiplier (for manual override of calculations)
         */
        public var rankMultiplier:Number = 1.0;

        public function get summarisedRank():Number{
            return rank * rankMultiplier;
        }

        /**
         * Component left alignment
         */
        public var leftPosition:int;


        private var selected_:Boolean = false;
        public function selectItem(item:UIComponent):void {
            selected_ = true;
            this.setStyle( "backgroundColor", getStyle( (selected_)?"backgroundSelectedColor":"backgroundDeselectedColor" ) );
            this.setStyle( "borderColor", getStyle( (selected_)?"borderSelectedColor":"borderDeselectedColor" ) );
        }
        public function deselectedItem():void {
            selected_ = false;
            this.setStyle( "backgroundColor", getStyle( (selected_)?"backgroundSelectedColor":"backgroundDeselectedColor" ) );
            this.setStyle( "borderColor", getStyle( (selected_)?"borderSelectedColor":"borderDeselectedColor" ) );
        }
        public function get isSelected():Boolean {
            return selected_;
        }        
        public function itemClicked(event:Event):void{
            if(!selected_){
                selectItem( event.target as UIComponent );
                dispatchEvent( new SsmeEvent( SsmeEvent.ITEM_SELECTED, { timeFrameEntity:eventData_, selectedItem:this } ) );
            }
        }
        
        protected override function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
            super.updateDisplayList( unscaledWidth, unscaledHeight );

            eventInfoTextArea.x = this.borderThickness_;
            eventInfoTextArea.y = this.borderThickness_;
            eventInfoTextArea.width = this.width - this.borderThickness_*2;
            eventInfoTextArea.height = this.height - this.borderThickness_*2;
            //this.toolTip = "Indexes : ["+beginIndex+" - "+endIndex+"]\n" +
            //    "neighbours: "+neighbours+"; rank:"+rank+"\n" +
            //    "x: "+x+"; y:"+y+"; width:"+width+"; height:"+height+
            //    "UUID:"+eventData.UUID+"  desc:"+eventData.description+" selected:"+isSelected;
        }
        
        private function createEventTooltip(event:ToolTipEvent):void {
            event.toolTip = new EventTooltip();
            (event.toolTip as EventTooltip).eventData = (eventData) ? eventData : null;
        }

        public override function setStyle(styleProp:String, newValue:*):void{

            if(styleProp == "backgroundDeselectedColor"){
                super.setStyle("backgroundColor", newValue);
                eventInfoTextArea.setStyle("parentBackgroundColor", newValue );
            }
            if(styleProp == "borderDeselectedColor"){
                super.setStyle("borderColor", newValue);
            }

            if(styleProp == "backgroundColor"){        
                eventInfoTextArea.setStyle("parentBackgroundColor", newValue );
            }

            super.setStyle(styleProp, newValue);
        }


    }

}