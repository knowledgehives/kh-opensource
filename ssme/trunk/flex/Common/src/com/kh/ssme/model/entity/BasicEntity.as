/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {
    import com.kh.ssme.util.JSONObject;

    public class BasicEntity implements IEntity{

        public var createDate:Date;
        public var modifiedDate:Date;
        public var removedDate:Date;
        public var deleted:Boolean;
        public var UUID:String;

        //------------------------------------------------

        protected static const createDate_field:String = 'createDate';
        protected static const modifiedDate_fields:String = 'modifiedDate';
        protected static const removedDate_fields:String = 'removedDate';
        protected static const deleted_field:String = 'deleted';
        protected static const uuid_field:String = 'uuid';
        protected static const readableName_field:String = 'readableName';      // read-only
        protected static const collection_field:String = 'collection';        

        //------------------------------------------------        

        public function BasicEntity() {

        }

        public function clone( entity:* = null ):*{
            var newData:* = (entity) ? (entity) : new BasicEntity();
            
            // plain
            (newData as BasicEntity).createDate = new Date( createDate );
            (newData as BasicEntity).modifiedDate = new Date( modifiedDate );
            (newData as BasicEntity).removedDate = new Date( removedDate );
            (newData as BasicEntity).deleted = new Boolean( deleted );
            (newData as BasicEntity).UUID = new String( UUID );

            return newData;
        }
        
        //------------------------------------------------

        public function parseJSONObject(json:Object, recursive:int = 0):* {

            createDate = JSONObject.parseString2Date( json[createDate_field] );
            modifiedDate = JSONObject.parseString2Date( json[modifiedDate_fields] );
            removedDate = JSONObject.parseString2Date( json[removedDate_fields] );            
            deleted = (json[deleted_field]) == "true";
            UUID = json[uuid_field];

            return this;
        }

        public function toJSONObject():Object {
            var json:Object = new Object();

            json[createDate_field] = JSONObject.parseDate2String( createDate );
            json[modifiedDate_fields] =  JSONObject.parseDate2String( modifiedDate );
            json[removedDate_fields] =  JSONObject.parseDate2String( removedDate );
            json[deleted_field] = deleted.toString();
            json[uuid_field] = UUID;

            return json;
        }

        //------------------------------------------------        

        public function parseJSONString(json:String, recursive:int = 0):* {
            var jsonEntity:Object = JSONObject.decode(json);
            return parseJSONObject( jsonEntity, recursive );
        }
        
        public function toJSONString():String {
            var jsonObject:Object = toJSONObject();
            var result:String = JSONObject.encode( jsonObject );
            return result;
        }

        public static function parseJSONCollection(jsonCollection:String, entity:IEntity, recursive:int = 0):Array{
            var json:Object = JSONObject.decode( jsonCollection );;
            var result:Array = [];
            if( json ){
                var jsonArray:Array = json[collection_field]
                if(jsonArray && jsonArray.length>0){
                    for(var i:int=0; i<jsonArray.length; i++){
                        result.push( entity.clone().parseJSONObject( jsonArray[i], recursive ) );
                    }
                }
            }
            return result;
        }
    }

}