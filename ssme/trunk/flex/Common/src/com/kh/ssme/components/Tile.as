/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
import com.kh.ssme.util.Color;

import flash.display.GradientType;
import flash.geom.Matrix;

import mx.containers.Canvas;

    [Style(name="colors", type="Array", arrayType="uint", format="Color", inherit="no")]
    [Style(name="borderColors", type="Array", arrayType="uint", format="Color", inherit="no")]
    //-------------------------
    [Style(name="leftDisplayMargin", type="Number", inherit="no")]
    [Style(name="rightDisplayMargin", type="Number", inherit="no")]
    [Style(name="topDisplayMargin", type="Number", inherit="no")]
    [Style(name="bottomDisplayMargin", type="Number", inherit="no")]
    public class Tile extends Canvas {

        private var LEFT_ADJUST:int = 1; // flex do not substract cell left border from width        

        public function Tile() {
            super();
        }


        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
            super.updateDisplayList(unscaledWidth, unscaledHeight);

            var colors:Array = getStyle("colors");
            colors = (colors) ?
                        (colors.length>=2) ? colors :
                            (colors.length==1) ? [ Color.makeSimpleLighter(colors[0],20), Color.makeSimpleDarker(colors[0],20) ] : [ 0xffffff, 0x000000 ]
                        : [ 0xffffff, 0x000000 ];
            var borderColors:Array = getStyle("borderColors");
            borderColors = (borderColors) ?
                            (borderColors.length>=2) ? borderColors :
                                (borderColors.length==1) ?
                                    [ Color.makeSimpleLighter(borderColors[0],20), Color.makeSimpleDarker(borderColors[0],20) ]
                                    : [ Color.makeSimpleDarker(colors[0],20), Color.makeSimpleDarker(colors[1],20) ] :
                            [ Color.makeSimpleDarker(colors[0],20), Color.makeSimpleDarker(colors[1],20) ];

            var cornerRad:Number            = (getStyle("cornerRadius")) ? getStyle("cornerRadius") : 0;
            var leftDisplayMargin:int       = (getStyle("leftDisplayMargin")) ? getStyle("leftDisplayMargin") : 1;
            var rightDisplayMargin:int      = (getStyle("rightDisplayMargin")) ? getStyle("rightDisplayMargin") : 1;
            var topDisplayMargin:int        = (getStyle("topDisplayMargin")) ? getStyle("topDisplayMargin") : 1;
            var bottomDisplayMargin:int     = (getStyle("bottomDisplayMargin")) ? getStyle("bottomDisplayMargin") : 1;
            var border:int                  = (getStyle("borderThickness")) ? getStyle("borderThickness") : 1;
            var alpha:Number                = (getStyle("backgroundAlpha")) ? getStyle("backgroundAlpha") : 1.0;


            graphics.clear();
            var matr:Matrix = new Matrix();


            var rectWidth:Number = unscaledWidth - leftDisplayMargin - rightDisplayMargin - LEFT_ADJUST - (2 * border);
            var rectHeight:Number = unscaledHeight - topDisplayMargin - bottomDisplayMargin - (2 * border);

            if( alpha==1.0 ){
                //make custom border..., if alpha!=1.0 then we can't do this this way
                matr.createGradientBox(unscaledWidth, unscaledHeight, Math.PI / 2);
                drawRoundRect(
                        LEFT_ADJUST + leftDisplayMargin, topDisplayMargin,
                        rectWidth + (2 * border), rectHeight + (2 * border),
                        cornerRad,
                        borderColors,
                        [alpha, alpha],
                        matr,
                        GradientType.LINEAR);
            }
            //draw rest...
            drawRoundRect(
                    LEFT_ADJUST + leftDisplayMargin + border, topDisplayMargin + border,
                    rectWidth, rectHeight,
                    cornerRad,
                    colors,
                    [alpha, alpha],
                    matr,
                    GradientType.LINEAR);
        }

        
    }

}