/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {

    import com.kh.ssme.connector.BasicQueuedConnector;
    import com.kh.ssme.connector.ConnectorResult;
    import com.kh.ssme.connector.UserConnector;
    import com.kh.ssme.model.entity.meta.ErrorMessageEntity;
    import com.kh.ssme.model.entity.UserEntity;
    import com.kh.ssme.util.Color;

    import com.kh.ssme.util.Logger;
    import com.kh.ssme.util.LoggingComponentsEnum;
    import com.kh.ssme.util.SsmeEvent;

import flash.display.Sprite;
import flash.events.Event;

    import flash.external.ExternalInterface;

    import mx.core.Application;
    import mx.events.FlexEvent;
import mx.utils.StringUtil;

[Event(name="ssme_application_initialized", type="com.kh.ssme.util.SsmeEvent")]
    [Event(name="ssme_userLoaded", type="com.kh.ssme.util.SsmeEvent")]
    [Event(name="ssme_drilldown_processed", type="com.kh.ssme.util.SsmeEvent")]    
    [Event(name="stateChanged",type="flash.events.Event")]        


    /**
     * Common application class for all components 
     */
    public class SSMEApplication extends Application implements IApplication{


        /**
         * Application URL
         */
        protected var applicationURL:String;

        /**
         * Page URL
         * "http://192.168.1.102:8080/SmartScheduleMe/user/739f77e9-7fd6-48ba-b4cc-4a572a1ef456?_method=GET&flexApp"
         */
        protected var pageURL_:String;
        public function get pageURL():String{
            return pageURL_;
        }    

        /**
         * Path of page - "location" of the page on server
         * "SmartScheduleMe/user/739f77e9-7fd6-48ba-b4cc-4a572a1ef456"
         */
        protected var pagePath_:String;
        public function get pagePath():String{
            return pagePath_;
        }

        /**
         * Query part of URL
         * "?_method=GET&flexApp"
         */
        protected var pageQuery_:String;
        public function get pageQuery():String{
            return pageQuery_;
        }    

        /**
         * Name of application using this animation
         * "SmartScheduleMe"
         */
        protected var globalApplicationName_:String;
        public function get globalApplicationName():String{
            return globalApplicationName_;
        }

        /**
         * Part of URL from protocol to ApplicationName
         * "http://192.168.1.102:8080/SmartScheduleMe/"
         */
        protected var globalApplicationPath_:String;       
        public function get globalApplicationPath():String{
            return globalApplicationPath_;
        }

        /**
         * Servlet used currently by application
         * "user"
         */
        protected var servlet_:String;
        public function get servlet():String{
            return servlet_;
        }

        /**
         * UUID of resource mapped in URL
         * "739f77e9-7fd6-48ba-b4cc-4a572a1ef456"
         */
        protected var resourceUUID_:String;
        public function get resourceUUID():String{
            return resourceUUID_;
        }

    

        /**
         * Should we load logged user entity
         */
        public var loadUserEntity:Boolean = false;

        /**
         * User connector
         */
        private var loggedUserConnector_:UserConnector = null;

        /**
         * Loaded logged-in user entity
         */
        protected var loggedUserEntity_:UserEntity = null;
        [Bindable(event="ssme_userLoaded")]
        public function get loggedUserEntity():UserEntity{
            return this.loggedUserEntity_;
        }

        /**
         * Flag determining if logged-in user entity was already loaded
         */
        protected var userEntityLoaded_:Boolean = false;
        public function get userEntityLoaded():Boolean{
            return userEntityLoaded_;
        }

        /**
         * Flag determining if logged-in user entity was already loaded
         */
        protected var applicationInitialized_:Boolean = false;
        public function get applicationInitialized():Boolean{
            return applicationInitialized_;
        }

        public var appBgColor_:uint;
        [Bindable(event="appBgColor_event")]
        public function get appBgColor():uint{
            return appBgColor_;
        }
        private function set appBgColor(value:uint):void{
            appBgColor_ = value;
            dispatchEvent( new Event("appBgColor_event") );
        }        

        //---------------------------------------------------------------

        public function SSMEApplication() {
            super();
            this.addEventListener( mx.events.FlexEvent.INITIALIZE, onInit );
        }
        
        protected function onInit(event:Event):void {

            //this.layout = "absolute";
            Application.application.width = "100%";
            Application.application.height = "100%";

            this.setStyle("paddingLeft", 0);
            this.setStyle("paddingRight", 0);
            this.setStyle("paddingTop", 0);
            this.setStyle("paddingBottom", 5);
            this.setStyle("backgroundGradientAlphas", [0, 0]);
            this.setStyle("borderStyle", "solid");
            this.setStyle("borderThickness", 0);
            this.setStyle("borderColor", 0xff0000);

            // dropShadowEnabled
            // dropShadowColor

            //---------------------------------------
            // prepare user loading handlers
            loggedUserConnector_ = new UserConnector(this.url);
            loggedUserConnector_.addEventListener(BasicQueuedConnector.EVENT_GET, onLoadUserResponse);
            loggedUserConnector_.addEventListener(UserConnector.EVENT_GET_RELOAD, onLoadUserResponse);
            loggedUserConnector_.addEventListener(BasicQueuedConnector.EVENT_UPDATE, onLoadUserResponse);
            loggedUserConnector_.addEventListener(BasicQueuedConnector.EVENT_ERROR, onLoadUserResponse);
            loggedUserConnector_.addEventListener(BasicQueuedConnector.EVENT_FAULT, onLoadUserResponse);
            //---------------------------------------            

            //---------------------------------------
            // flash app url & properties
            if (url) {
                applicationURL = url.replace(/flex\/[^\/]+.swf$/gi, "").replace(/\/$/gi, "") + "/";
            }
            if (Application.application.parameters) {
                var appBgColorString:String = (Application.application.parameters) ? Color.toUnsigned(Application.application.parameters['bgcolor']) : "";
                if (appBgColorString && appBgColorString.length > 0) {
                    Application.application.setStyle("backgroundColor", appBgColorString);
                    private::appBgColor = Color.colorString2uint( appBgColorString );
                } else {
                    private::appBgColor = Application.application.getStyle("backgroundColor")
                }
            }
            //---------------------------------------

            //---------------------------------------
            // page url
            if( ExternalInterface.available ){

                var currentPageURL:String = ExternalInterface.call('window.location.href.toString');
                reloadLocation( currentPageURL );
                
            }
            //---------------------------------------


            //---------------------------------------
            // logged user
            loadUser();
            //---------------------------------------            

            //---------------------------------------
            // listen for javascript events
            if (ExternalInterface.available){
                ExternalInterface.addCallback("processDrilldown", processDrilldown);
            }


            // all done
            applicationInitialized_ = true;
            dispatchEvent(new Event("ssme_application_initialized"));
        }

        private function reloadLocation(newPageURL:String):void {

            // i.e. "http://192.168.1.102:8080/SmartScheduleMe/user/739f77e9-7fd6-48ba-b4cc-4a572a1ef456?_method=GET&flexApp"       
            pageURL_ = newPageURL;

            var pattern:RegExp = /^(?P<protocol>([a-z]+))(:\/\/){1}(?P<host>([^:\/]+))(:(?P<port>(\d{1,5})))?(\/){1}(?P<path>([^\?]+))(?P<queryPart>(\?.*))?$/i;
            var urlObject:Object = pattern.exec( pageURL_ );
            if( urlObject ){
                //urlObject.protocol
                //urlObject.host
                //urlObject.port
                pagePath_ = urlObject.path;         //  "SmartScheduleMe/user/739f77e9-7fd6-48ba-b4cc-4a572a1ef456"
                pageQuery_ = urlObject.queryPart;   //  "?_method=GET&flexApp"
            }
            var a:Array = pagePath_.split('/');
            if(a && a.length>1){
                globalApplicationName_ = a[0];      //  "SmartScheduleMe"
                servlet_ = a[1]                     //  "user"

                // "http://192.168.1.102:8080/SmartScheduleMe/"
                globalApplicationPath_ = newPageURL.substr( 0, newPageURL.indexOf(servlet_) );

                // "739f77e9-7fd6-48ba-b4cc-4a572a1ef456"
                resourceUUID_ = (  a.length>2 && /^\w{8}-\w{4}-\w{4}-\w{4}-\w{12}$/gi.test( a[2] ) ) ? a[2] : "";
            }

        }        

        public function loadUser():void{
            if(loadUserEntity){
                ModalSpinner.instance.showSpinner("Loading user data", 0x1313cd, Sprite(this), 0.2);
                loggedUserConnector_.getUser('');  // no uuid, load currently logged user 
            }
        }
        public function reLoadUser():void{
            if(loadUserEntity){
                ModalSpinner.instance.showSpinner("Loading user data", 0x1313cd, Sprite(this), 0.2);
                loggedUserConnector_.reloadUser('');  // no uuid, load currently logged user
            }
        }

        private function onLoadUserResponse(event:Event):void {
            var result:ConnectorResult = (event && event is SsmeEvent) ? (event as SsmeEvent).eventProperties['result'] : null;

            if (result) {
                // success
                if (result.resultType == ConnectorResult.SUCCESS) {
                    loggedUserEntity_ = (new UserEntity()).parseJSONString( result.lastResult as String );
                    userEntityLoaded_ = true;                       
                    if( result.eventMnemonic == BasicQueuedConnector.EVENT_GET ){
                        dispatchEvent(new Event("ssme_userLoaded"));
                    }
                    if( result.eventMnemonic == UserConnector.EVENT_GET_RELOAD ){
                        dispatchEvent(new Event("ssme_userReLoaded"));
                    }
                }
                // error
                else if (result.resultType == ConnectorResult.ERROR) {
                    loggedUserEntity_ = null;
                    var json:ErrorMessageEntity = (new ErrorMessageEntity()).parseJSONString( result.lastResult as String );
                    Logger.error("Logged User error : "+json.errorMessage, null, LoggingComponentsEnum.ALL);
                }
                // fault
                else if (result.resultType == ConnectorResult.FAULT) {
                    loggedUserEntity_ = null;
                    Logger.errorEvent("Logged User fault", result.innerEvent, LoggingComponentsEnum.ALL);
                }
            }
            ModalSpinner.instance.hideSpinner();
        }

        protected var previousState:String;
        public function changeState(newState:String):void{
            previousState = this.currentState;
            this.currentState = newState;
            dispatchEvent( new Event("stateChanged") );
        }


        //--------------------------------------
        // javascript bindings for drilldowns
        public function processDrilldown( query:String ):void{

            reloadLocation( query );

            dispatchEvent(new SsmeEvent("ssme_drilldown_processed", { query: query }));
        }

        public static const APP_FlexClient:String = "FlexClient";
        public static const APP_Menu:String = "Menu";                
        public function callDrilldown( query:String, externalApp:String = null ):void{

            Logger.debug("callDrilldown("+query+","+externalApp+");", LoggingComponentsEnum.ALL);
            if( !externalApp ){
                processDrilldown( query );            
            } else {
                if( ExternalInterface.available ){
                    ExternalInterface.call("Flex.queryApp", externalApp, query);
                } else {
                    Logger.error("fault callDrilldown("+query+","+externalApp+");", null, LoggingComponentsEnum.ALL);
                }
            }
            
        }
        //
        //--------------------------------------        


    }

}