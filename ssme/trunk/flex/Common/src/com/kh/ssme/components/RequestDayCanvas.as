/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {

    import com.kh.ssme.model.enum.StateEnum;
    import flash.events.MouseEvent;

    [Style(name="activeBarColor", type="Number", format="Color")]
    [Style(name="activeBarAlpha", type="Number", format="Number")]

    public class RequestDayCanvas extends GridCanvas {

        public var EMPTY:String = '-';
        public var NEW:String = StateEnum.UNDECIDED.mnemonic;

        /**
         * Array of active quarters
         */
        private var activity_:Array = [];
        public var numberOfSelected:int;        
        public function set activity(value:Array):void{
            activity_ = value;
            numberOfSelected = 0;
            for(var i:int = 0; i<activity_.length; i++){
                if(activity_[i] == NEW) numberOfSelected++;
            }
        }
        public function get activity():Array{
            return activity_;
        }

        public function RequestDayCanvas() {
            super();
            reset();
            this.addEventListener( MouseEvent.CLICK, mouseClickHandler );
        }

        public function reset():void{
            previousY = 0;
            previousX = 0;
            activity = [];
            for(var i:int = 0; i<=96; i++)   activity[i] = EMPTY;
            invalidateDisplayList();            
        }

        protected var previousX:Number = 0, previousY:Number = 0;
        protected function mouseClickHandler(event:MouseEvent):void {
            changeActivity(event);
        }

        private function changeActivity(event:MouseEvent):void {
            var temp:Number;
	        if( event.shiftKey ){
                // "range" mode
                if( event.ctrlKey ){
                    // remove
                    unmark( localY2quarter(previousY), localY2quarter(event.localY) );
                } else {
                    // add
                    mark( localY2quarter(previousY), localY2quarter(event.localY) );
                }
            } else {
                // "single" mode
                temp = localY2quarter(event.localY);
                if( event.ctrlKey ){
                    // remove
                    unmark( temp, temp );
                } else {
                    // add
                    mark( temp, temp );
                }
            }

            previousX = event.localX;
            previousY = event.localY;
            invalidateDisplayList();            
        }

        private function unmark(startIndex:int, endIndex:int):void {
            for(var i:int = startIndex; i<=endIndex; i++){
                if(activity[i] == NEW)    numberOfSelected--;
                activity[i] = EMPTY;
            }
        }
        private function mark(startIndex:int, endIndex:int):void {
            for(var i:int = startIndex; i<=endIndex; i++){
                if(activity[i] == EMPTY)    numberOfSelected++;
                activity[i] = NEW;
            }
        }

        protected function localY2quarter(localY:Number):int{
            if( rowsPosition && rowsPosition.length>1 ){
                //var rows:Array = rowsPosition[1];
                var rows:Array = rowsPosition[0];
                for(var i:int = 0; i<rows.length; i++){
                    if( localY < (rows[i+1] as Number) ){
                        return i+1;
                    }
                }
                return rows.length-1;
            }
            return 0;
        }

        protected override function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{

            graphics.clear();       // erase old drawings

            paint();

            forceNoClear = true;    // do not clear graphics it was already done
            super.updateDisplayList( unscaledWidth, unscaledHeight );

            //this.toolTip = tooltip+"{width:"+this.width+", height:"+this.height+"}";

        }

        protected function paint():void {
            
            if(rowsPosition && rowsPosition.length>0){
                var color:uint = this.getStyle_("activeBarColor", 0);
                var alfa:uint   = this.getStyle_("activeBarAlpha", 0);

                var outBorderThickness:Number = outerBorderThickness_;
                var topVerticalMargin:Number = topVerticalMargin_;
                var bottomVerticalMargin:Number = bottomVerticalMargin_;
                var leftHorizontalMargin:Number = leftHorizontalMargin_;
                var rightHorizontalMargin:Number = rightHorizontalMargin_;

                var tooltip:String = "";
                for(var i:int = 0; i<=96; i++){
                    if( activity[i] != EMPTY ){

                        graphics.beginFill(color, alfa);
                        graphics.drawRoundRect(outBorderThickness+leftHorizontalMargin,                             // x
                                rowsPosition[0][i-1],                                                               // y
                                unscaledWidth-(2*outBorderThickness+leftHorizontalMargin+rightHorizontalMargin),    // width
                                (rowsPosition[0][i] - rowsPosition[0][i-1]),                                        // height
                                0, 0);
                        graphics.endFill();
                        tooltip += "["+i+"]{ " +
                                   "x:"+(outBorderThickness+leftHorizontalMargin)+", " +
                                   "y:"+(rowsPosition[0][i-1])+", " +
                                   "w:"+(unscaledWidth-(2*outBorderThickness+leftHorizontalMargin+rightHorizontalMargin))+", " +
                                   "h:"+(rowsPosition[0][i-1] - rowsPosition[0][i])+", " +
                                   "}\n"
                    }
                }
                
            }            
        }
        
    }

}