/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
    import flash.events.Event;

    import mx.containers.VBox;

    public class VerticalBox extends VBox {
        public function VerticalBox() {
            super();
        }

        private var height_:Number = 0;        
        public override function set height(value:Number):void{
            if(value != height_){
                height_ = (maxHeight>value)?value:maxHeight;
                dispatchEvent (new Event ("ssme_heightChanged"));
            }
            //super.height = height_;
        }

        [Bindable(event="ssme_heightChanged")]
        public override function get height():Number{
            return (maxHeight && !isNaN(maxHeight)) ? ((maxHeight>height_)?height_:maxHeight) : height_;            
        }

    }

}