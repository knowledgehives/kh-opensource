/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {
import com.kh.ssme.model.enum.ParticipationTypeEnum;
import com.kh.ssme.model.enum.StateEnum;
import com.kh.ssme.util.HashArray;

    public class EventingEntity extends BasicEntity  implements IEntity {

        public var participantType:ParticipationTypeEnum;
        public var state:StateEnum;

        public var requestUUID:String;
        public var requestName:String;
        public var requestEntity:MeetingRequestEntity;

        public var userUUID:String;
        public var userName:String;
        public var userEntity:UserEntity;

        public var partsEntities:HashArray;
        public var partsNames:HashArray;

        //------------------------------------------------

        protected static const participantType_field:String = 'participantType';
        protected static const state_field:String = 'state';         
        protected static const request_field:String = 'request';
        protected static const user_field:String = 'user';
        protected static const parts_field:String = 'parts';


        //------------------------------------------------

        public function EventingEntity() {
            super();
        }

        public override function clone(entity:* = null):*{
            var newData:EventingEntity = super.clone((entity)?entity:new EventingEntity());

            // plain
            newData.participantType = participantType;
            newData.state = state;

            // entities
            newData.requestUUID = new String( requestUUID );
            newData.requestName = new String( requestName );
            newData.requestEntity = (requestEntity) ? requestEntity.clone() : null;
            newData.userUUID = new String( userUUID );
            newData.userName = new String( userName );
            newData.userEntity = (userEntity) ?  userEntity.clone() : null;

            // hasharrays
            newData.partsEntities = (partsEntities) ? partsEntities.clone() : null;
            newData.partsNames = (partsNames) ? partsNames.clone() : null;

            return newData;
        }

        public static function createNew():EventingEntity{
            var result:EventingEntity = new EventingEntity();

            // plain
            result.participantType = ParticipationTypeEnum.CREATOR;
            result.state = StateEnum.UNDECIDED;

            // entities
            result.requestUUID = "";
            result.requestName = "";
            result.userUUID = "";
            result.userName = "";

            // hasharrays
            result.partsEntities = new HashArray();
            result.partsNames = new HashArray();

            return result;
        }

        //------------------------------------------------

        public override function parseJSONObject(json:Object, recursive:int = 0):* {
            super.parseJSONObject( json, recursive );
            var i:int;

            // participantType
            participantType = ParticipationTypeEnum.find( json[participantType_field] );

            // state
            state = StateEnum.find( json[state_field] );

            // request
            // user
            userUUID = null; userName = null; userEntity = null;
            requestUUID = null; requestName = null; requestEntity = null; 
            if(recursive>0){
                if(json[request_field]){
                    requestEntity = (new MeetingRequestEntity()).parseJSONObject( json[request_field], recursive-1 );
                    requestUUID = requestEntity.UUID;
                    requestName = requestEntity.title;
                }
                if(json[user_field]){                
                    userEntity = (new UserEntity()).parseJSONObject( json[user_field], recursive-1 );
                    userUUID = userEntity.UUID;
                    userName = userEntity.name;
                }
            } else {
                if(json[request_field]){
                    requestUUID = (json[request_field]) ? (json[request_field])[uuid_field] : null;
                    requestName = (json[request_field]) ? (json[request_field])[readableName_field] : null;
                    requestEntity = null;
                }
                if(json[user_field]){
                    userUUID = (json[user_field]) ? (json[user_field])[uuid_field] : null;
                    userName = (json[user_field]) ? (json[user_field])[readableName_field] : null;
                    userEntity = null;
                }                    
            }

            // parts
            partsEntities = new HashArray();
            partsNames = new HashArray();
            if(json[parts_field].length>0){
                if(recursive>0){
                    var ep:EventingPartEntity;
                    for(i = 0; i<json[parts_field].length; i++){
                        ep = (new EventingPartEntity()).parseJSONObject( (json[parts_field][i]), recursive-1 );
                        partsEntities.put( ep.UUID, ep );
                        partsNames.put( ep.UUID, "EventingPartEntity:"+ep.UUID );
                    }
                } else {
                    for(i = 0; i<json[parts_field].length; i++){
                        partsEntities.put( (json[parts_field][i])[uuid_field], null );
                        partsNames.put( (json[parts_field][i])[uuid_field], (json[parts_field][i])[readableName_field] );
                    }
                }
            }

            return this;
        }

        public override function toJSONObject():Object {
            var json:Object = super.toJSONObject();
            var key:String;
            var tab:Array = new Array();

            // participantType
            json[participantType_field] = participantType.type;

            // state
            json[state_field] = state.state;

            // request
            json[request_field] = new Object();
            (json[request_field])[uuid_field] = requestUUID;
            (json[request_field])[readableName_field] = requestName;

            // user
            json[user_field] = new Object();
            (json[user_field])[uuid_field] = userUUID;
            (json[user_field])[readableName_field] = userName;

            // parts
            tab = new Array();
            for (key in partsNames){
                tab.push( { uuid:key, name:"" } );
            }
            json[parts_field] = tab;

            return json;
        }

    }

}