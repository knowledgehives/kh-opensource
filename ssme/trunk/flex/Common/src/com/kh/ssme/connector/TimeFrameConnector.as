/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.connector {

    import com.kh.ssme.model.entity.TimeFrameEntity;

    [Event(name="ssme_timeframes_objectGet", type="com.kh.ssme.util.SsmeEvent")]
    [Event(name="ssme_timeframes_addPrototype", type="com.kh.ssme.util.SsmeEvent")]
    [Event(name="ssme_timeframes_updatePrototype",  type="com.kh.ssme.util.SsmeEvent")]
    [Event(name="ssme_timeframes_removeParent",  type="com.kh.ssme.util.SsmeEvent")]    

    public class TimeFrameConnector extends BasicQueuedConnector {

        /**
         * Path to proper servlet
         */
        private static const timeFrameServlet:String = "timeFrame/";
        public override function get servlet():String{
            return timeFrameServlet;
        }
        public override function getResourceURL(uuid:String):String{
            return baseURL + timeFrameServlet + uuid;
        }

        public function TimeFrameConnector(url:String) {
            super(url);
		}

		public static const EVENT_GET_TIMEFRAMES:String = "ssme_timeframes_objectGet";
		public static const EVENT_CREATE_TIMEFRAME_PROTOTYPE:String = "ssme_timeframes_addPrototype";
		public static const EVENT_UPDATE_TIMEFRAME_PROTOTYPE:String = "ssme_timeframes_updatePrototype";
        public static const EVENT_UPDATE_TIMEFRAME_INTO_PROTOTYPE:String = "ssme_timeframes_updateIntoPrototype";        
		public static const EVENT_UPDATE_REMOVE_PARENT:String = "ssme_timeframes_removeParent";             


        public function getTimeFrame( uuid:String, params:Object = null ):void{
            processCommand( ACTION_GET, EVENT_GET, uuid, null, params );
        }

//        public function getTimeFrames( calendarUuid:String, from:Date, to:Date, params:Object = null):void{
//            if(calendarUuid) params["calendarUuid"] = from.toDateString();
//            if(from)         params["from"] = from.toDateString();
//            if(to)           params["to"] = to.toDateString();
//            processCommand( ACTION_GET, EVENT_GET_TIMEFRAMES, null, null, params );
//        }

        public function addTimeFrame( data:TimeFrameEntity = null, params:Object = null ):void{
            processCommand( ACTION_CREATE, EVENT_CREATE, null, (data)?data.toJSONString():emptyJSON, params );
        }

        public function addTimeFramePrototype( data:TimeFrameEntity = null, params:Object = null ):void{
            processCommand( ACTION_CREATE, EVENT_CREATE_TIMEFRAME_PROTOTYPE, null, (data)?data.toJSONString():emptyJSON, params );
        }

        public function updateTimeFrame( uuid:String, data:TimeFrameEntity = null, params:Object = null ):void{
            processCommand( ACTION_UPDATE, EVENT_UPDATE, uuid, (data)?data.toJSONString():emptyJSON, params );
        }

        public function updateTimeFramePrototype( uuid:String, data:TimeFrameEntity = null, params:Object = null ):void{
            processCommand( ACTION_UPDATE, EVENT_UPDATE_TIMEFRAME_PROTOTYPE, uuid, (data)?data.toJSONString():emptyJSON, params );
        }

        public function updateTimeFrameIntoPrototype( uuid:String, data:TimeFrameEntity = null, params:Object = null ):void{
            processCommand( ACTION_UPDATE, EVENT_UPDATE_TIMEFRAME_INTO_PROTOTYPE, uuid, (data)?data.toJSONString():emptyJSON, params );
        }

        public function updateTimeFrameRemoveParent( uuid:String, repeatTypeUUID:String, data:TimeFrameEntity = null, params:Object = null ):void{
            if(!params) params = {};
            params[Params.TIMEFRAME_PARENT] = repeatTypeUUID;
            processCommand( ACTION_UPDATE, EVENT_UPDATE_REMOVE_PARENT, uuid, (data)?data.toJSONString():emptyJSON, params );
        }


        public function deleteTimeFrame( uuid:String, params:Object = null ):void{
            processCommand( ACTION_DELETE, EVENT_DELETE, uuid, emptyJSON, params );
        }

	}

}