/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.connector {

    public class Params {

        public static var DATA_RECURSION:String = "dataRecursion";
		public static var FLEX_APPLICATION:String = "flexApp";
		public static var JSON:String = "json";
		public static var LOGIN_ERROR:String = "loginError";
		public static var METHOD:String = "method";
		public static var CALENDAR_FROM:String = "from";
		public static var CALENDAR_TO:String = "to";
        public static var TIMEFRAME_PARENT:String = "parentUUID";
        public static var REMOVE_CHILDREN_EVENTS:String = "removeChildrenEvents";
        public static var META_COMMAND:String = "command";
        public static var REMOVE_REQUEST_CHILDREN:String = "removeRequestChildren";
        public static var UPDATE_REQUEST_TO_EVENT:String = "updateRequestToEvent";
        public static var ACTIVE_REQUEST:String = "activeRequests";
        public static var REQUEST_EVENTINGS:String = "requestsEventings";           
        public static var IMPORT_TYPE:String = "importType";
        public static var EXPORT_TYPE:String = "exportType";
        public static var TAGS_LIKE:String = "tagsLike";

    }

}