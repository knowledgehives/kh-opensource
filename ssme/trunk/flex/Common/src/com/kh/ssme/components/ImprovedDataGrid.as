/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
import com.kh.ssme.util.SsmeEvent;

import mx.controls.DataGrid;

    [Style(name="headerRatios", type="Array", arrayType="uint", format="Color", inherit="no")]
    [Style(name="headerAlphas", type="Array", arrayType="uint", format="Color", inherit="no")]
    [Style(name="headerBorderColor", type="Number", inherit="no")]
    [Style(name="headerBorderAlpha", type="Number", inherit="no")]

    [Event(name="onVerticalScrollPositionChanged", type="com.kh.ssme.util.SsmeEvent")]

    public class ImprovedDataGrid extends DataGrid {

        public static var ON_VERTICAL_SCROLL_POSITION_CHANGED:String = "onVerticalScrollPositionChanged";

        public function ImprovedDataGrid() {
            super();
            setStyle("headerBackgroundSkin", ImprovedDataGridHeaderBackgroundSkin);
            setStyle("headerSeparatorSkin", ImprovedDataGridHeaderSeparator);
        }

        /**
         *  @private
         */
        override public function set verticalScrollPosition(value:Number):void{
            super.verticalScrollPosition = value;
            dispatchEvent( new SsmeEvent(ON_VERTICAL_SCROLL_POSITION_CHANGED,{ }) );
        }

    }

}