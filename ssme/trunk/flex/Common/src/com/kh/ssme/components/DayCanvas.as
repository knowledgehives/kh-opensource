/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {

    import com.kh.ssme.model.entity.TimeFrameEntity;
    import com.kh.ssme.util.Color;
    import com.kh.ssme.util.DateUtil;
    import com.kh.ssme.util.HashArray;

    import com.kh.ssme.util.Logger;
    import com.kh.ssme.util.SsmeEvent;

    import flash.events.Event;

    import mx.core.UIComponent;

    [Event(name="ssme_itemSelected", type="flash.events.Event")]

    public class DayCanvas extends GridCanvas implements ITimeCanvas {

        public function DayCanvas() {
            super();
            verticalScrollPolicy = "off";
            horizontalScrollPolicy = "off";    
        }



        private var oldWidth:Number, oldHeight:Number;
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{

            if(visible){    // necessary only if visible

                // call first in order to get grid drawn correctly
                super.updateDisplayList(unscaledWidth, unscaledHeight);                

                // call repaint only if dimensions were changed
                if((unscaledWidth - oldWidth) != 0 || (unscaledHeight - oldHeight) != 0){
                    //there was change in width or height since last call

                    oldWidth = unscaledWidth;
                    oldHeight = unscaledHeight;
                    repaint();    
                }
            }
        }

        private var selectedItem_:UIComponent;
        public function set selectedItem(item:UIComponent):void {
            selectedItem_ = item;
        }
        public function get selectedItem():UIComponent {
            return selectedItem_;
        }
        // "re-dispatch"
        public function itemClicked(event:Event):void{
            dispatchEvent( (event as SsmeEvent).cloneEvent() );
            selectedItem_ = (event as SsmeEvent).eventProperties[SsmeEvent.ITEM_SELECTED__ITEM_PROP];
        }       



        /**
         * Sets data provider and all properties required by data provider,
         * it takes care about setting them in proper order
         * @param currentDay
         * @param calendarsColorsMap
         * @param dataProvider
         */
        public function setAllData(currentDay:Date, calendarsColorsMap:HashArray, dataProvider:HashArray):void{
            this.currentDay = currentDay;
            this.calendarsColorsMap = calendarsColorsMap;
            this.dataProvider = dataProvider;
        }


        public var calendarsColorsMap:HashArray;

        
        public var currentDay:Date;


        [ArrayElementType("com.kh.ssme.model.entity.TimeFrameEntity")]
        private var dataProvider_:HashArray;
        private var eventsMap:HashArray = new HashArray();        
        public function set dataProvider(value:HashArray):void{
            dataProviderAvailable = false;
            eventFieldsUpToDate = false;
            recreateEvents = true;
            dataProvider_ = value;
            
            for(var i:int = 0; i<eventsMap.size; i++){
                if( eventsMap.getValueAt(i) ){
                    this.removeChild( eventsMap.getValueAt(i) );                    
                }
            }
            eventsMap.clear();
            dataProviderAvailable = true;            

            repaint();
            validateNow();
        }
        public function get dataProvider():HashArray{
            return dataProvider_;
        }    

        private var dataProviderAvailable:Boolean = false;
        private var recreateEvents:Boolean = false;        
        private var eventFieldsUpToDate:Boolean = false;
        public function repaint():void{
            if(dataProviderAvailable){
                createEvents();
                repaintEvents();
            }
        }


        /**
         * Create display object for each TimeFrameEntity in dataProvider
         */
        protected function createEvents():void{

            // in order to save time we recreate EventFields
            // only if it is necessary
            if(recreateEvents){

                // create events
                var ev:EventField;
                var i:int = 0, timeBegin:int, timeEnd:int;
                var from:DateUtil, to:DateUtil, current:DateUtil = (new DateUtil(currentDay)).dayBegin(), calendarColor:uint;
                for each(var json:TimeFrameEntity in dataProvider_){
                    ev = getEventFieldInstance();
                    calendarColor = calendarsColorsMap[ json.calendarUUID ];
                    ev.setStyle("backgroundDeselectedColor", Color.getInstance(calendarColor).makeLighter(30).desaturate(30).RGB);
                    ev.setStyle("backgroundSelectedColor", Color.getInstance(calendarColor).makeLighter(20).RGB );
                    ev.setStyle("borderDeselectedColor", Color.getInstance(json.priority.borderColor).makeLighter(30).desaturate(30).RGB);
                    ev.setStyle("borderSelectedColor", Color.getInstance(json.priority.borderColor).makeLighter(20).RGB );
                    ev.setStyle("borderThickness", 5);                    
                    ev.setStyle("cornerRadius", 20);

                    ev.eventData = json;
                    ev.name = json.UUID;
                    ev.addEventListener( SsmeEvent.ITEM_SELECTED, itemClicked );
                    eventsMap.put(json.UUID, ev);

                    // event boundaries, index
                    timeBegin = (json.from.getHours() * 4) + ((json.from.getMinutes() as int)/15);
                    timeEnd = (json.to.getHours() * 4) + ((json.to.getMinutes() as int)/15);
                    // check if event exceed current day
                    from = (new DateUtil(json.from)).dayBegin();
                    to = (new DateUtil(json.to)).dayBegin();
                    if( current.compare(from) > 0 )    timeBegin = 0;
                    if( current.compare(to) < 0 )      timeEnd = rowsPosition[1].length - 1;
                    // store for future use
                    ev.beginIndex = timeBegin;
                    ev.endIndex = timeEnd;
                  
                    addChild( ev );
                }

                recreateEvents = false;
            }
        }

        protected function getEventFieldInstance():EventField{
            return (new EventField());
        }

        // used by repaint
        private var clusterOfEventsMap:HashArray = new HashArray();        
        /**
         * Calculates rank and coordinates for each TimeFrame display object  
         */
        protected function repaintEvents():void{
            var ev:EventField;
            var countArray:Array;
            var i:int, j:int = 0;

            // in order to save time we recalculate ranks, clusters etc.
            // only if it is necessary
            if(!eventFieldsUpToDate){
                //------------------------------------------------
                // "rank" events
                // count number of events for each quarter
                countArray = new Array(96);
                for each(ev in eventsMap){
                    for(i=ev.beginIndex; i<ev.endIndex; i++){
                        if(countArray[i]<0 || isNaN(countArray[i]))     countArray[i]=0;
                        countArray[i]++;
                    }
                }

                // calculate rank and number of neighbours for each event
                var neighbours:int;
                for each(ev in eventsMap){
                    neighbours = 0;
                    for(i=ev.beginIndex; i<ev.endIndex; i++)   neighbours = Math.max( neighbours, countArray[i] );
                    ev.neighbours = neighbours - 1;
                    ev.rank = neighbours*10000 + (ev.endIndex - ev.beginIndex)*100 + ev.beginIndex;
                }
                //------------------------------------------------


                //------------------------------------------------
                //clustering events into separated groups which don't intersect each other
                var clusterIndex:Array = new Array(96);
                var helpMap:HashArray, lastCreatedClusterID:int = 1;
                var trace:String = "";

                // clear old clusters
                clusterOfEventsMap.clear();
                
                // for each of the events
                for each(ev in eventsMap){
                    helpMap = new HashArray();

                    // check the time span of current event
                    // if there were already clusters defined for this span then collect their ids
                    for(i=ev.beginIndex; i<ev.endIndex; i++){
                        if(clusterIndex[i]<0 || isNaN(clusterIndex[i]))     clusterIndex[i]=0;
                        if(!helpMap.containsKey(clusterIndex[i]) && clusterIndex[i]>0){
                            helpMap.put(clusterIndex[i], clusterIndex[i]);
                        }
                    }

                    if(helpMap.size == 0){  //  helpMap.size == #clusters
                        // there were no clusters so far for this span,
                        // so we 'book' the span and create new cluster on the base of current event
                        for(i=ev.beginIndex; i<ev.endIndex; i++)    clusterIndex[i] = lastCreatedClusterID;
                        clusterOfEventsMap.put(lastCreatedClusterID, [ ev ]);
                        lastCreatedClusterID++;
                    }
                    if(helpMap.size == 1){
                        // there was one cluster for this span
                        // so we just join current event to cluster we found
                        var currentCluster:int = helpMap.getKeyAt(0);
                        for(i=ev.beginIndex; i<ev.endIndex; i++)    clusterIndex[i] = currentCluster;
                        clusterOfEventsMap[currentCluster].push( ev );
                    }
                    if(helpMap.size > 1){
                        // there was more than one cluster for this span
                        // we join cluster we found into new single one
                        // so we just join current event to cluster we found
                        var temp:Array = [ ev ];
                        var minIndex:int = ev.beginIndex, maxIndex:int = ev.endIndex;
                        for each(var value:* in helpMap){
                            for each(var clusterElement:EventField in clusterOfEventsMap[value]){
                                temp.push( clusterElement ); // add to new cluster
                                minIndex = Math.min(minIndex, clusterElement.beginIndex );
                                maxIndex = Math.max(maxIndex, clusterElement.beginIndex );
                            }
                            clusterOfEventsMap.deleteWithKey(value);
                        }
                        for(i=minIndex; i<maxIndex; i++)    clusterIndex[i] = lastCreatedClusterID;
                        clusterOfEventsMap.put(lastCreatedClusterID, temp);
                        lastCreatedClusterID++;
                    }

                }

//                for (var key:String in clusterOfEventsMap){
//                    trace = "";
//                    var el:EventField;
//                    for each(el in clusterOfEventsMap[key])  trace+= (el.eventData as TimeFrameEntity).UUID.substr(0, 3)+"|";
//                    Logger.debug( "~("+key+") -> ["+trace+"]" );
//                }
                //------------------------------------------------

                eventFieldsUpToDate = true;
            }


            //------------------------------------------------
            // for each cluster size it's events
            j = 0;
            var placeArray:Array = new Array(96);
            var minPosition:int = 0, numberOfPositions:int = 0;
            for each(var eventsArray:Array in clusterOfEventsMap){
                // sort events by rank
                eventsArray.sort( sortEventsByRank );

                // calculate sub-optimal position within cluster
                // surely there's a better algorithm :)
                numberOfPositions = 0;                
                for each(ev in eventsArray){
                    minPosition = 0;
                    // find minimal possible position (maximum of available position in span)
                    for(i=ev.beginIndex; i<ev.endIndex; i++){
                        if(placeArray[i]<0 || isNaN(placeArray[i]))     placeArray[i]=0;
                        minPosition = Math.max( minPosition, placeArray[i] );
                    }
                    // store information in event
                    ev.leftPosition = minPosition;
                    //update position availability array for current span
                    for(i=ev.beginIndex; i<ev.endIndex; i++){
                        placeArray[i] = minPosition + 1;
                    }
                    numberOfPositions = Math.max( numberOfPositions, minPosition );
                }                

                // size events
                for each(ev in eventsArray){
                    sizeEvent( ev, eventsArray.length, numberOfPositions, j );
                    j += 100;
                }
            }
            //------------------------------------------------        
        }
        private function sortEventsByRank(a:EventField, b:EventField):int {
            return ( a.summarisedRank < b.summarisedRank ) ? -1 : (( a.summarisedRank > b.summarisedRank ) ? 1 : 0 );
        }    

        private function sizeEvent(ev:EventField, clusterSize:int, numberOfPositions:int, i:int):void{
            var data:TimeFrameEntity = ev.eventData;
            if(data){
                if(rowsPosition && rowsPosition.length>0){
                    var availableWidth:Number = this.width - this.leftHorizontalMargin_ - this.rightHorizontalMargin_ - 2*this.outerBorderThickness_;
                    ev.width = availableWidth * (2 / (numberOfPositions+2));
                    ev.height = rowsPosition[1][ev.endIndex] - rowsPosition[1][ev.beginIndex];
                    ev.x = this.leftHorizontalMargin_ + this.outerBorderThickness_ + ((availableWidth / (numberOfPositions+2)) * ev.leftPosition);
                    ev.y = rowsPosition[1][ev.beginIndex];
                    this.setChildIndex( ev, 0 );
                    ev.invalidateSize();
                    ev.invalidateDisplayList();
                }
            }

        }

    }

}