/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {

    import com.kh.ssme.util.HashArray;
    import com.kh.ssme.util.JSONObject;

    public final class CalendarEntity extends BasicEntity implements IEntity {

        public static const ALL_EVENTS_NAME:String = "All";

        public static const DEFAULT_NAME:String = "Default";

        //------------------------------------------------        

        public var name:String;

        public var userUUID:String;
        public var userName:String;
        public var userEntity:UserEntity;        

        public var timeFramesEntities:HashArray;
        public var timeFramesNames:HashArray;        

        //------------------------------------------------

        protected static const name_field:String = 'name';
        protected static const user_field:String = 'user';
        protected static const timeFrames_field:String = 'timeFrames';

        //------------------------------------------------

        public function CalendarEntity() {
            super();
        }

        public override function clone(entity:* = null):*{
            var newData:CalendarEntity = super.clone((entity)?entity:new CalendarEntity());

            // plain
            newData.name = new String( name );

            // entities
            newData.userUUID = new String( userUUID );
            newData.userName = new String( userName );
            newData.userEntity = (userEntity) ? userEntity.clone() : null;

            // hasharrays
            newData.timeFramesEntities = (timeFramesEntities) ? timeFramesEntities.clone() : null;
            newData.timeFramesNames = (timeFramesNames) ? timeFramesNames.clone() : null;


            return newData;
        }        

        public static function createNew(name:String, userUUID:String):CalendarEntity{
            var result:CalendarEntity = new CalendarEntity();
            result.name = name;
            result.timeFramesEntities = new HashArray();
            result.timeFramesNames = new HashArray();
            result.userUUID = userUUID;
            result.userName = "";
            result.userEntity = null;            
            return result;
        }

        //------------------------------------------------        

        public override function parseJSONObject(json:Object, recursive:int = 0):* {
            super.parseJSONObject( json, recursive );
            var i:int;            

            // name
            name = json[name_field];

            // user
            userUUID = null; userName = null; userEntity = null;             
            if(recursive>0){
                if(json[user_field]){
                    userEntity = (new UserEntity()).parseJSONObject( json[user_field], recursive-1 );
                    userUUID = userEntity.UUID;
                    userName = userEntity.name+" "+userEntity.surname+" ("+userEntity.login+")";
                }
            } else {
                if(json[user_field]){
                    userUUID = (json[user_field]) ? (json[user_field])[uuid_field] : null;
                    userName = (json[user_field]) ? (json[user_field])[readableName_field] : null;
                    userEntity = null;
                }
            }

            // timeFrames
            timeFramesEntities = new HashArray();
            timeFramesNames = new HashArray();
            if(json[timeFrames_field].length>0){
                if(recursive>0){
                    var tf:TimeFrameEntity;
                    for(i = 0; i<json[timeFrames_field].length; i++){
                        tf = (new TimeFrameEntity()).parseJSONObject( (json[timeFrames_field][i]), recursive-1 );
                        timeFramesEntities.put( tf.UUID, tf );
                        timeFramesNames.put( tf.UUID, tf.title );
                    }
                } else {
                    for(i = 0; i<json[timeFrames_field].length; i++){
                        timeFramesEntities.put( (json[timeFrames_field][i])[uuid_field], null );
                        timeFramesNames.put( (json[timeFrames_field][i])[uuid_field], (json[timeFrames_field][i])[readableName_field] );
                    }
                }
            }
                 
            return this;
        }

        public override function toJSONObject():Object {
            var json:Object = super.toJSONObject();
            var key:String;            
            var tab:Array;            

            // name
            json[name_field] = name;

            // user
            json[user_field] = new Object();            
            (json[user_field])[uuid_field] = userUUID;
            (json[user_field])[readableName_field] = userName;

            // timeFrames
            tab = new Array();
            for (key in timeFramesNames){
                tab.push( { uuid:key, name:"" } );
            }
            json[timeFrames_field] = tab;

            return json;
        }


        public static function sortCalendars(a:Object, b:Object):int {
            return ( a.name < b.name ) ? -1 : (( a.name > b.name ) ? 1 : 0 );
        }

    }

}