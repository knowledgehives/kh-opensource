/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.connector
{
	
	/**
	 * Bean class to hold a single connection pool command waiting to be executed
	 */	
	public class ConnectionQueueEntry
	{
		
		/**
		 * Action to be performed
		 */
		public var action:String;

		/**
		 *  Event to be dispatched after receiving data
		 */
		public var eventMnemonic:String;
		
		/**
		 * Data UUID
		 */
		public var UUID:String;
		
		/**
		 * Data to be send
		 */
		public var data:Object;		
		
		/**
		 * Request params
		 */
		public var params   :Object;
		
		
		
		public function ConnectionQueueEntry(action:String, eventMnemonic:String, UUID:String=null, data:Object=null, params:Object=null)
		{
			this.action = action;
            this.eventMnemonic = eventMnemonic
			this.UUID = UUID;
			this.data = data;
			this.params = params;
		}
	}
}