/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.connector {

    [Event(name="ssme_repeatType_deleteWithChildren",  type="com.kh.ssme.util.SsmeEvent")]

    public class MetaDataConnector extends BasicQueuedConnector {

        public static const COMMANDS_REQUEST_HINTS:String = "requestHints";
		public static const EVENT_REQUEST_HINTS:String = "ssme_meta_requestHints";

        public function MetaDataConnector(url:String) {
            super(url);
        }

        /**
         * Path to proper servlet
         */
        private static const metaServlet:String = "meta/";
        public override function get servlet():String{
            return metaServlet;
        }
        public override function getResourceURL(uuid:String):String{
            return baseURL + metaServlet + uuid;
        }

        public function getRequestHints( requestUUID:String, params:Object = null ):void{
            if(!params) params = {};
            params[Params.META_COMMAND] = COMMANDS_REQUEST_HINTS;
            processCommand( ACTION_GET, EVENT_REQUEST_HINTS, requestUUID, null, params );
        }

    }

}