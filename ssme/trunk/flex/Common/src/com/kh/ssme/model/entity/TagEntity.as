/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {
    import com.kh.ssme.util.HashArray;

    public final class TagEntity extends BasicEntity implements IEntity {

        public var name:String;
        public var description:String;
        public var url:String;

        public var timeFramesEntities:HashArray;
        public var timeFramesNames:HashArray;

        //------------------------------------------------

        protected static const name_field:String = 'name';
        protected static const description_field:String = 'description';
        protected static const url_field:String = 'url';
        protected static const timeFrames_field:String = 'timeFrames';

        //------------------------------------------------

        public function TagEntity() {
            super();            
        }

        public override function clone(entity:* = null):*{
            var newData:TagEntity = super.clone((entity)?entity:new TagEntity());

            // plain
            newData.name = new String( name );
            newData.description = new String( description );
            newData.url = new String( url );

            // hasharrays
            newData.timeFramesEntities = (timeFramesEntities) ? timeFramesEntities.clone() : null;
            newData.timeFramesNames = (timeFramesNames) ? timeFramesNames.clone() : null;

            return newData;
        }

        public static function createNew(name:String, description:String, url:String):TagEntity{
            var result:TagEntity = new TagEntity();
            result.name = name;
            result.description = description;
            result.url = url;
            result.timeFramesEntities = new HashArray();
            result.timeFramesNames = new HashArray();
            return result;
        }

        //------------------------------------------------

        public override function parseJSONObject(json:Object, recursive:int = 0):* {
            super.parseJSONObject( json, recursive );
            var i:int;

            // name
            name = json[name_field];
            // descripton
            description = json[description_field];
            // url
            url = json[url_field];

            // timeFrames
            timeFramesEntities = new HashArray();
            timeFramesNames = new HashArray();
            if(json[timeFrames_field].length>0){
                if(recursive>0){
                    var tf:TimeFrameEntity;
                    for(i = 0; i<json[timeFrames_field].length; i++){
                        tf = (new TimeFrameEntity()).parseJSONObject( (json[timeFrames_field][i]), recursive-1 );
                        timeFramesEntities.put( tf.UUID, tf );
                        timeFramesNames.put( tf.UUID, tf.title );
                    }
                } else {
                    for(i = 0; i<json[timeFrames_field].length; i++){
                        timeFramesEntities.put( (json[timeFrames_field][i])[uuid_field], null );
                        timeFramesNames.put( (json[timeFrames_field][i])[uuid_field], (json[timeFrames_field][i])[readableName_field] );
                    }
                }
            }

            return this;            
        }

        public override function toJSONObject():Object {
            var json:Object = super.toJSONObject();
            var key:String;
            var tab:Array;

            // name
            json[name_field] = name;
            json[description_field] = description;
            json[url_field] = url;

            // timeFrames
            tab = new Array();
            for (key in timeFramesNames){
                tab.push( { uuid:key, name:"" } );
            }
            json[timeFrames_field] = tab;

            return json;
        }

        public static function sortTags(a:Object, b:Object):int {
            return ( a.name < b.name ) ? -1 : (( a.name > b.name ) ? 1 : 0 );
        }

    }

}