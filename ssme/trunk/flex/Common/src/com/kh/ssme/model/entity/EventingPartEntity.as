/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {

    import com.kh.ssme.model.enum.StateEnum;
    import com.kh.ssme.util.JSONObject;

    public class EventingPartEntity extends BasicEntity implements IEntity  {

        public var state:StateEnum;
        public var day:Date;    
        public var map:String;

        public var eventingUUID:String;
        public var eventingName:String;
        public var eventingEntity:EventingEntity;        

        //------------------------------------------------

        protected static const state_field:String = 'state';
        protected static const day_field:String = 'day';
        protected static const map_field:String = 'map';        
        protected static const eventing_field:String = 'eventing';

        //------------------------------------------------

        public function EventingPartEntity() {
            super();
        }

        public override function clone( entity:* = null ):*{
            var newData:EventingPartEntity = super.clone((entity)?entity:new EventingPartEntity());

            // plain
            newData.state = state;
            newData.day = new Date(day.time);
            newData.map = new String(map);
            
            // entities
            newData.eventingUUID = new String( eventingUUID );
            newData.eventingName = new String( eventingName );
            newData.eventingEntity = (eventingEntity) ? eventingEntity.clone() : null;

            return newData;
        }

        public static function createNew(day:Date, map:String):EventingPartEntity{
            var result:EventingPartEntity = new EventingPartEntity();
            
            result.state = StateEnum.UNDECIDED;
            result.day = day;
            result.map = map;
            result.eventingUUID = "";
            result.eventingName = "";
            result.eventingEntity = null;

            return result;
        }

        //------------------------------------------------

        public override function parseJSONObject(json:Object, recursive:int = 0):* {
            super.parseJSONObject( json );

            // state
            state = StateEnum.find( json[state_field] );

            // day
            day = JSONObject.parseString2Date( json[day_field] );            

            // map
            map = json[map_field];

            // eventing
            eventingUUID = null; eventingName = null; eventingEntity = null; 
            if(recursive>0){
                if(json[eventing_field]){
                    eventingEntity = (new EventingEntity()).parseJSONObject( json[eventing_field], recursive-1 );
                    eventingUUID = eventingEntity.UUID;
                    eventingName = "Eventing:"+eventingEntity.UUID;
                }
            } else {
                if(json[eventing_field]){
                    eventingUUID = (json[eventing_field]) ? (json[eventing_field])[uuid_field] : null;
                    eventingName = (json[eventing_field]) ? (json[eventing_field])[readableName_field] : null;
                    eventingEntity = null;
                }
            }

            return this;
        }

        public override function toJSONObject():Object {
            var json:Object = super.toJSONObject();

            // state
            json[state_field] = state.state;

            // day
            json[day_field] = JSONObject.parseDate2String( day );

            // map
            json[map_field] =  map;

            // eventing
            json[eventing_field] = new Object();
            (json[eventing_field])[uuid_field] = eventingUUID;
            (json[eventing_field])[readableName_field] = eventingName;

            return json;
        }

    }

}