/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.util {
import mx.core.Container;
import mx.events.ValidationResultEvent;
import mx.managers.ToolTipManager;
import mx.validators.Validator;

    public class SSMEValidator extends Validator {

        public function SSMEValidator() {

            super();
        }

        /**
         * Custom validation function handler.
         * If function is provided it will be called instead of Validator.validate().
         *
         * 'validateFunction' should have following signature:
         * function (value:Object = null, suppressEvents:Boolean = false):ValidationResultEvent
         */
        public var validateFunction:Function = null;

        public override function validate(value:Object = null, suppressEvents:Boolean = false):ValidationResultEvent {
            if (validateFunction != null){
                try{
                    return validateFunction.call(null, value, suppressEvents);
                } catch (e:Error){  ;   }
            }
            return super.validate( value, suppressEvents );
        }

        /**
         * Mark container as invalid, used by validators
         * @param container
         */
        public static function markAsInvalid(container:Container):void {
            container.setStyle('borderStyle', 'solid');
            container.setStyle('borderColor', 'red');
        }

        /**
         * Mark container as valid, used by validators
         * @param container
         */
        public static function markAsValid(container:Container):void {
            container.setStyle('borderStyle', 'none');
            if (ToolTipManager.currentTarget == container && ToolTipManager.currentToolTip != null) {
                ToolTipManager.currentToolTip.visible = false;
            }
        }        
    }
}