/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
import flash.events.Event;

    import mx.containers.Panel;
    import mx.events.FlexEvent;

    //[Exclude(name="headerHeight", kind="style")]
    [Exclude(name="headerColors", kind="style")]    

    [Exclude(name="borderThickness", kind="style")]
    [Exclude(name="borderColor", kind="style")]
    [Exclude(name="borderStyle", kind="style")]
    [Exclude(name="borderAlpha", kind="style")]
    [Exclude(name="borderSides", kind="style")]
    [Exclude(name="borderSkin", kind="style")]

    [Exclude(name="borderThicknessBottom", kind="style")]
    [Exclude(name="borderThicknessLeft", kind="style")]
    [Exclude(name="borderThicknessRight", kind="style")]
    [Exclude(name="borderThicknessTop", kind="style")]

    public class HeaderlessPanel extends Panel {

        public function HeaderlessPanel() {
            super();
            this.addEventListener( mx.events.FlexEvent.INITIALIZE, onInit );
        }

        private function onInit(event:Event):void {

            // prohibit
            this.setStyle("borderThickness", 0);
            this.setStyle("borderColor", 0);
            this.setStyle("borderStyle", "solid");
            this.setStyle("borderAlpha", 0.5);
            if(!title_) this.setStyle("headerHeight", 0);

            // default
            this.setStyle("roundedBottomCorners", true);
            this.setStyle("cornerRadius", 8);            
            this.setStyle("paddingTop", 5);
            this.setStyle("paddingLeft", 0);
            this.setStyle("paddingRight", 0);
            this.setStyle("paddingBottom", 5);
            this.setStyle("horizontalGap", 20);
            this.setStyle("verticalGap", 5);

        }

        private var title_:String;
        public override function get title():String{
            return title_;    
        }
        public override function set title(value:String):void{
            if(value && value.length>0){
                this.setStyle("headerHeight", 24);
            } else {
                this.setStyle("headerHeight", 0);
            }
            title_  = value;
        }

    }
}