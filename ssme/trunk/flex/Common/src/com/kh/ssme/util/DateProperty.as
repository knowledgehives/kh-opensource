/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.util {

    public class DateProperty {

        public static const MILLISECONDS:DateProperty = new DateProperty("milliseconds");
        public static const SECONDS:DateProperty = new DateProperty("seconds");
        public static const MINUTES:DateProperty = new DateProperty("minutes");
        public static const HOURS:DateProperty = new DateProperty("hours");        
        public static const DAY:DateProperty = new DateProperty("day");
        public static const DATE:DateProperty = new DateProperty("date");
        public static const MONTH:DateProperty = new DateProperty("month");
        public static const YEAR:DateProperty = new DateProperty("fullYear");

		private static const VALUES:Object = new Object();
        {
            //static initialization
            VALUES[MILLISECONDS.property] = MILLISECONDS;
            VALUES[SECONDS.property] = SECONDS;
            VALUES[MINUTES.property] = MINUTES;
            VALUES[HOURS.property] = HOURS;
            VALUES[DAY.property] = DAY;
            VALUES[DATE.property] = DATE;
            VALUES[MONTH.property] = MONTH;
            VALUES[YEAR.property] = YEAR;   
        }



		public static function find(id:String):DateProperty{
			return VALUES[id];
		}


		// ---------- private parts ---------------
		private var property_:String;
        public function DateProperty(property:String) {
            this.property_ = property;
        }

		public function get property():String{
			return this.property_;
		}

		public function toString():String{
			return this.property_;
		}
        
		public function equals(type:DateProperty):Boolean{
			return (type!=null && this.property_ == type.property_);
		}

    }

}