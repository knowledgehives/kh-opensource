/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
    import flash.display.GradientType;
    import flash.display.Graphics;

    import flash.geom.Matrix;

    import mx.skins.ProgrammaticSkin;
    import mx.styles.StyleManager;

    public class ImprovedDataGridHeaderBackgroundSkin extends ProgrammaticSkin {
        
        public function ImprovedDataGridHeaderBackgroundSkin() {
            super();
        }

        override protected function updateDisplayList(w:Number, h:Number):void
        {
            var g:Graphics = graphics;
            g.clear();

            var headerColors:Array = getStyle("headerColors");
            var headerRatios:Array = getStyle("headerRatios");
            var headerAlphas:Array = getStyle("headerAlphas");
            var headerBorderColor:uint = (getStyle("headerBorderColor")) ? getStyle("headerBorderColor") : 0x000000;
            var headerBorderAlpha:uint = (getStyle("headerBorderAlpha")) ? getStyle("headerBorderAlpha") : 1.0;

            headerColors = (headerColors && headerColors.length>2) ? [ headerColors[0], headerColors[1], headerColors[2]] :
                                            (headerColors.length==2) ? [ headerColors[0], headerColors[0], headerColors[1] ] :
                                            (headerColors.length==1) ? [ headerColors[0], headerColors[0], headerColors[0] ] : [];
            headerRatios = (headerRatios) ? headerRatios : [ 0, 60, 255 ];
            headerAlphas = (headerAlphas) ? headerAlphas : [ 1.0, 1.0, 1.0 ];
            StyleManager.getColorNames(headerColors);

            var matrix:Matrix = new Matrix();
            matrix.createGradientBox(w, h + 1, Math.PI/2, 0, 0);            

            g.beginGradientFill(GradientType.LINEAR, headerColors, headerAlphas, headerRatios, matrix);
            g.lineStyle(0, 0x000000, 0);
            g.moveTo(0, 0);
            g.lineTo(w, 0);
            g.lineTo(w, h - 0.5);
            g.lineStyle(0, headerBorderColor, headerBorderAlpha);
            g.lineTo(0, h - 0.5);
            g.lineStyle(0, 0x000000, 0);
            g.endFill();

//            g.beginGradientFill(GradientType.LINEAR, headerColors, headerAlphas, headerRatios, matrix);
//            g.drawRect(0, 0, w, h);
//            g.endFill();
//            g.lineStyle(0, 0xff0000, 0);
//            g.moveTo(0, 0);
//            g.lineTo(w, 0);
//            g.lineTo(w, h - 0.5);
//            g.lineStyle(0, headerBorderColor, headerBorderAlpha);
//            g.lineTo(0, h - 0.5);
//            g.lineStyle(0, headerBorderColor, 0);

//                trace( " borderThickness: "+getStyle("borderThickness")+";\n" );
//                trace( " borderAlpha: "+getStyle("borderAlpha")+";\n" );

        }


//        public override function getStyle(styleProp:String):*{
//            if(styleProp == "borderThickness"){
//                return 0;
//            }
//            if(styleProp == "borderAlpha"){
//                return 0;
//            }
//            return super.getStyle(styleProp);
//        }

    }

}