/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {

    import com.kh.ssme.util.HashArray;

    public final class LocationEntity extends BasicEntity implements IEntity {

        public var longitude:String;

        public var latitude:String;

        public var address:String;

        public var suggested:Boolean;

        public var stayPlaces:HashArray;

        public var eventPlaces:HashArray;

        public var travelFromPlaces:HashArray;

        public var travelToPlaces:HashArray;

        public var suggestedLocationStateUUID:String;
        public var suggestedLocationStateName:String;

        public var meetingRequests:HashArray;

        public function LocationEntity() {
            super();
        }

        public override function parseJSONObject(json:Object, recursive:int = 0):* {
            super.parseJSONObject( json );
            var i:int = 0;

//            longitude = json['longitude'];
//            latitude = json['latitude'];
//            address = json['address'];
//            suggested = json['suggested'];
//
//            //stayPlaces_ = new HashArray();
//            //for(i = 0; i<json['stayPlaces'].length; i++){
//            //    stayPlaces_.put( (json['stayPlaces'][i])['uuid'], (json['stayPlaces'][i])['name'] );
//            //}
//
//            //eventPlaces_ = new HashArray();
//            //for(i = 0; i<json['eventPlaces'].length; i++){
//            //    eventPlaces_.put( (json['eventPlaces'][i])['uuid'], (json['eventPlaces'][i])['name'] );
//            //}
//
//            //travelFromPlaces_ = new HashArray();
//            for(i = 0; i<json['travelFromPlaces'].length; i++){
//                travelFromPlaces.put( (json['travelFromPlaces'][i])['uuid'], (json['travelFromPlaces'][i])['name'] );
//            }
//
//            //travelToPlaces_ = new HashArray();
//            for(i = 0; i<json['travelToPlaces'].length; i++){
//                travelToPlaces.put( (json['travelToPlaces'][i])['uuid'], (json['travelToPlaces'][i])['name'] );
//            }
//
//            //meetingRequests_ = new HashArray();
//            //for(i = 0; i<json['meetingRequests'].length; i++){
//            //    meetingRequests_.put( (json['meetingRequests'][i])['uuid'], (json['meetingRequests'][i])['name'] );
//            //}
//
//            //stayPlaces_ = new HashArray();
//            //suggestedLocationStateUUID = (json['suggestedLocationState']) ? (json['suggestedLocationState'])['uuid'] : null;
//            //suggestedLocationStateName = (json['suggestedLocationState']) ? (json['suggestedLocationState'])['name'] : null;

            return this;
        }

        public override function toJSONObject():Object {
            var json:Object = super.toJSONObject();
            var tab:Array = new Array();
            var key:String;

//            json['longitude'] = longitude;
//            json['latitude'] = latitude;
//            json['address'] = address;
//            json['suggested'] = suggested;
//
//
//            //for (key in stayPlaces_){
//            //    tab.push( { uuid:key, name:stayPlaces_[key] } );
//            //}
//            //json['stayPlaces_'] = tab;
//
//            //tab = new Array();
//            //for (key in eventPlaces_){
//            //    tab.push( { uuid:key, name:eventPlaces_[key] } );
//            //}
//            //json['eventPlaces'] = tab;
//
//            tab = new Array();
//            for (key in travelFromPlaces){
//                tab.push( { uuid:key, name:travelFromPlaces[key] } );
//            }
//            json['travelFromPlaces'] = tab;
//
//            tab = new Array();
//            for (key in travelToPlaces){
//                tab.push( { uuid:key, name:travelToPlaces[key] } );
//            }
//            json['travelToPlaces'] = tab;
//
////            json[''] = suggestedLocationStateUUID;
////            json[''] = suggestedLocationStateName;
//
//            //tab = new Array();
//            //for (key in meetingRequests_){
//            //    tab.push( { uuid:key, name:meetingRequests_[key] } );
//            //}
//            json['meetingRequests'] = tab;

            return json;
        }

    }
}