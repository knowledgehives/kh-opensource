/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
    import com.kh.ssme.model.entity.UserEntity;
    import flash.events.IEventDispatcher;

    public interface IApplication extends IEventDispatcher {

        //-------------------------------------------------------------------------
        /**
         * Whole Page URL
         * "http://192.168.1.102:8080/SmartScheduleMe/user/739f77e9-7fd6-48ba-b4cc-4a572a1ef456?_method=GET&flexApp"
         */
        function get pageURL():String;

        /**
         * Path of page - "location" of the page on server
         * "SmartScheduleMe/user/739f77e9-7fd6-48ba-b4cc-4a572a1ef456"
         */
        function get pagePath():String;

        /**
         * Query part of URL
         * "?_method=GET&flexApp"
         */
        function get pageQuery():String;

        /**
         * Name of application using this animation
         * "SmartScheduleMe"
         */
        function get globalApplicationName():String;

        /**
         * Part of URL from protocol to ApplicationName
         * "http://192.168.1.102:8080/SmartScheduleMe/"
         */
        function get globalApplicationPath():String;

        /**
         * Servlet used currently by application
         * "user"
         */
        function get servlet():String;

        /**
         * UUID of resource mapped in URL
         * "739f77e9-7fd6-48ba-b4cc-4a572a1ef456"
         */
        function get resourceUUID():String;

        /**
         *  The URL from which this Application's SWF file was loaded.
         */
        function get url():String;
        //-------------------------------------------------------------------------

        
        /**
         * Application background color
         */
        function get appBgColor():uint;        

        /**
         * Flag that determine if application was initialized
         */
        function get applicationInitialized():Boolean;                

        /**
         * Flag that determine if logged-in user entity was loaded
         */
        function get userEntityLoaded():Boolean;

        /**
         * Logged-in user entity was loaded
         */
        [Bindable(event="ssme_userLoaded")]        
        function get loggedUserEntity():UserEntity;

        /**
         * Call loading of user entity
         */
        function loadUser():void;

        /**
         * Perform internal drilldown using given query to given application 
         * @param query         - query to be used
         * @param externalApp   - target application or null if '_self'
         */
        function callDrilldown( query:String, externalApp:String = null ):void;

    }

}