/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.connector {
    import com.kh.ssme.model.entity.TagEntity;

	[Event(name="ssme_tagLike_objectGet",  type="com.kh.ssme.util.SsmeEvent")]

    public class TagConnector extends BasicQueuedConnector {

        /**
         * Path to proper servlet
         */
        private static const calendarServlet:String = "tag/";
        public override function get servlet():String{
            return calendarServlet;
        }
        public override function getResourceURL(uuid:String):String{
            return baseURL + calendarServlet + uuid;
        }

        public function TagConnector(url:String) {
            super(url);
        }

		public static const EVENT_GET_TAG_LIKE_COLLECTION:String = "ssme_tagLike_objectGet";

        public function getTag( uuid:String, params:Object = null ):void{
            processCommand( ACTION_GET, EVENT_GET, uuid, null, params );
        }

        public function getTagsLike( likePhrase:String, params:Object = null ):void{
            if(!params) params = {};
            params[Params.TAGS_LIKE] = likePhrase;
            processCommand( ACTION_GET, EVENT_GET_TAG_LIKE_COLLECTION, null, null, params );
        }

        public function addTag( data:TagEntity, params:Object = null ):void{
            processCommand( ACTION_CREATE, EVENT_CREATE, null, (data)?data.toJSONString():emptyJSON, params );
        }

        public function updateTag( uuid:String, data:TagEntity = null, params:Object = null ):void{
            processCommand( ACTION_UPDATE, EVENT_UPDATE, uuid, (data)?data.toJSONString():emptyJSON, params );
        }

        public function deleteTag( uuid:String, params:Object = null ):void{
            processCommand( ACTION_DELETE, EVENT_DELETE, uuid, null, params );
        }


    }

}