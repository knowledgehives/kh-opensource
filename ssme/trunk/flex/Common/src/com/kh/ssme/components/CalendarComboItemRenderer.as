/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
    import mx.controls.Label;
    import mx.controls.listClasses.IDropInListItemRenderer;
    import mx.core.IDataRenderer;
    import mx.core.IFactory;

    public class CalendarComboItemRenderer extends Label implements IDataRenderer, IFactory, IDropInListItemRenderer{

        public function CalendarComboItemRenderer() {
            super();
            this.truncateToFit = true;
        }

        private function invalidateItem():void {
            if(data){
                this.toolTip = data['name'];
            }
        }

        public function newInstance():* {
            return new CalendarComboItemRenderer();
        }

    }
}