/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {

import com.kh.ssme.model.enum.StateEnum;

import mx.controls.dataGridClasses.DataGridListData;
    import mx.controls.listClasses.BaseListData;
    import mx.controls.listClasses.IDropInListItemRenderer;
    import mx.core.IDataRenderer;
    import mx.core.IFactory;


    public class EventPartItemRenderer extends Tile implements IDataRenderer, IFactory, IDropInListItemRenderer{
    
        public function EventPartItemRenderer() {
            super();
            setStyle("cornerRadius", 4);
        }
        
        private var rowData_:Object;
        private var data_:StateEnum;
        public override function set data(value:Object):void{
            rowData_ = value;
            if(rowData_ && listData_){
                data_ = value[ listData_.dataField ];
                invalidateItem();
            }
        }
        public override function get data():Object{
            return rowData_;
        }


        private var listData_:DataGridListData;
        public function get listData():BaseListData {
            return listData_;
        }
        public function set listData(value:BaseListData):void {
            listData_ = (value as DataGridListData);
        }        

        private function invalidateItem():void {
            if(data_){
                setStyle("colors", [ data_.color ]);
                invalidateDisplayList();
            }
        }

        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
            super.updateDisplayList(unscaledWidth, unscaledHeight);

            if( data_ && listData_ ){
                this.toolTip = "[ data:"+data_.toString()+" \n" +
                               "listData: {"+listData_.columnIndex+"|"+listData_.rowIndex+"|"+listData_.dataField+"} ]" +
                               " [ "+data['user']+" ]"
                        ;
            }
        }

        public function newInstance():* {
            return new EventPartItemRenderer();
        }
    }
}