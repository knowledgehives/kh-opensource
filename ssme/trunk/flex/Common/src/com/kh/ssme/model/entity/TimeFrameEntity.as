/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {
import com.kh.ssme.model.enum.PriorityEnum;
import com.kh.ssme.model.enum.TimeFrameTypeEnum;
import com.kh.ssme.model.enum.TimeFrameTypeEnum;
import com.kh.ssme.util.HashArray;
	import com.kh.ssme.util.JSONObject;

    public final class TimeFrameEntity extends BasicEntity implements IEntity {

        public var from:Date;
        public var to:Date;
        public var title:String;
        public var description:String;
        public var isPrototype:Boolean;
        public var type:TimeFrameTypeEnum;
        public var priority:PriorityEnum;        

        public var repeatUUID:String;
        public var repeatName:String;
        public var repeatEntity:RepeatTypeEntity;

        public var calendarUUID:String;
        public var calendarName:String;
        public var calendarEntity:CalendarEntity;

        public var meetingRequestUUID:String;
        public var meetingRequestName:String;
        public var meetingRequestEntity:MeetingRequestEntity;

        public var parentUUID:String;
        public var parentName:String;
        public var parentEntity:RepeatTypeEntity;

        public var tagsEntities:HashArray;
        public var tagsNames:HashArray;

        //------------------------------------------------        

        protected static const from_field:String = 'from';
        protected static const to_field:String = 'to';
        protected static const title_field:String = 'title';
        protected static const description_field:String = 'description';
        protected static const prototype_field:String = 'prototype';
        protected static const type_field:String = 'type';
        protected static const priority_field:String = 'priority';
        protected static const repeat_field:String = 'repeat';        
        protected static const calendar_field:String = 'calendar';
        protected static const meetingRequest_field:String = 'meetingRequest';
        protected static const parent_field:String = 'parent';
        protected static const tags_field:String = 'tags';        

        //------------------------------------------------
        
        public function TimeFrameEntity() {
            super();
        }

        public override function clone(entity:* = null):*{
            var newData:TimeFrameEntity = super.clone((entity)?entity:new TimeFrameEntity());

            // plain
            newData.from = new Date(from.time);
            newData.to = new Date(to.time);
            newData.title = new String( title );
            newData.description = new String( description );
            newData.isPrototype = (isPrototype);
            newData.type = type;
            newData.priority = priority;

            // entities
            newData.repeatUUID = new String( repeatUUID );
            newData.repeatName = new String( repeatName );
            newData.repeatEntity = repeatEntity.clone();
            newData.calendarUUID = new String( calendarUUID );
            newData.calendarName = new String( calendarName );
            newData.calendarEntity = (calendarEntity) ? calendarEntity.clone() : null;
            newData.parentUUID = new String( parentUUID );
            newData.parentName = new String( parentName );
            newData.parentEntity = (parentEntity) ? parentEntity.clone() : null;
            newData.meetingRequestUUID = new String( meetingRequestUUID );
            newData.meetingRequestName = new String( meetingRequestName );
            newData.meetingRequestEntity = (meetingRequestEntity) ? meetingRequestEntity.clone() : null;

            // hasharrays
            newData.tagsEntities = (tagsEntities) ? tagsEntities.clone() : null;
            newData.tagsNames = (tagsNames) ? tagsNames.clone() : null;            

            return newData;
        }

        public static function createNew(title:String, description:String, from:Date, to:Date):TimeFrameEntity{
            var result:TimeFrameEntity = new TimeFrameEntity();

            result.from = from;
            result.to = to;
            result.title = title;
            result.description = description;
            result.isPrototype = false; 
            result.type = TimeFrameTypeEnum.AVAILABLE;
            result.priority = PriorityEnum.MEDIUM;

            result.repeatUUID = "";
            result.repeatName = "";
            result.repeatEntity = null;            
            result.calendarUUID = "";
            result.calendarName = "";
            result.calendarEntity = null;
            result.meetingRequestUUID = "";
            result.meetingRequestName = "";
            result.meetingRequestEntity = null;
            result.parentUUID = "";
            result.parentName = "";
            result.parentEntity = null;

            result.tagsEntities = new HashArray();
            result.tagsNames = new HashArray();                  

            return result;
        }

        //------------------------------------------------

        /**
         * Checks if tis a repeatable event
         * @return
         */
        public function get isRepeatable():Boolean{
            return (repeatUUID!=null && repeatUUID.length>1);
        }

        //------------------------------------------------

        public override function parseJSONObject(json:Object, recursive:int = 0):* {
            var i:int;
            super.parseJSONObject( json );

            // from
            from = JSONObject.parseString2Date( json[from_field] );

            // to
            to = JSONObject.parseString2Date( json[to_field] );

            // title
            title = json[title_field];

            // description
            description = json[description_field];

            // prototype
            isPrototype = (json[prototype_field] == "true");

            // type
            type = TimeFrameTypeEnum.find( json[type_field] );

            // priority
            priority = PriorityEnum.find( json[priority_field] );

            // repeat
            // calendar
            // meetingRequest
            // parent
            repeatUUID = null; repeatName = null; repeatEntity = null;
            calendarUUID = null; calendarName = null; calendarEntity = null;
            meetingRequestUUID = null; meetingRequestName = null; meetingRequestEntity = null;
            parentUUID = null; parentName = null; parentEntity = null;            
            if(recursive>0){
                if(json[repeat_field]){
                    repeatEntity = (new RepeatTypeEntity()).parseJSONObject( json[repeat_field], recursive-1 );
                    repeatUUID = repeatEntity.UUID;
                    repeatName = "RepeatType:"+repeatEntity.UUID;
                }
                if(json[calendar_field]){
                    calendarEntity = (new CalendarEntity()).parseJSONObject( json[calendar_field], recursive-1 );
                    calendarUUID = calendarEntity.UUID;
                    calendarName = calendarEntity.name;
                }
                if(json[meetingRequest_field]){
                    meetingRequestEntity = (new MeetingRequestEntity()).parseJSONObject( json[meetingRequest_field], recursive-1 );
                    meetingRequestUUID = meetingRequestEntity.UUID;
                    meetingRequestName = "MeetingRequest:"+meetingRequestEntity.UUID;
                }
                if(json[parent_field]){
                    parentEntity = (new RepeatTypeEntity()).parseJSONObject( json[parent_field], recursive-1 );
                    parentUUID = parentEntity.UUID;
                    parentName = "ParentRepeatType:"+parentEntity.UUID;
                }
            } else {
                if(json[repeat_field]){                
                    repeatUUID = (json[repeat_field]) ? (json[repeat_field])[uuid_field] : null;
                    repeatName = (json[repeat_field]) ? (json[repeat_field])[readableName_field] : null;
                    repeatEntity = null;
                }
                if(json[calendar_field]){                    
                    calendarUUID = (json[calendar_field]) ? (json[calendar_field])[uuid_field] : null;
                    calendarName = (json[calendar_field]) ? (json[calendar_field])[readableName_field] : null;
                    calendarEntity = null;
                }
                if(json[meetingRequest_field]){                    
                    meetingRequestUUID = (json[meetingRequest_field]) ? (json[meetingRequest_field])[uuid_field] : null;
                    meetingRequestName = (json[meetingRequest_field]) ? (json[meetingRequest_field])[readableName_field] : null;
                    meetingRequestEntity = null;
                }
                if(json[parent_field]){
                    parentUUID = (json[parent_field]) ? (json[parent_field])[uuid_field] : null;
                    parentName = (json[parent_field]) ? (json[parent_field])[readableName_field] : null;
                    parentEntity = null;
                }
            }

            // tags
            tagsEntities = new HashArray();
            tagsNames = new HashArray();
            if(json[tags_field] && json[tags_field].length>0){
                if(recursive>0){
                    var tag:TagEntity;
                    for(i = 0; i<json[tags_field].length; i++){
                        tag = (new TagEntity()).parseJSONObject( (json[tags_field][i]), recursive-1 );
                        tagsEntities.put( tag.UUID, tag );
                        tagsNames.put( tag.UUID, tag.name );
                    }
                } else {
                    for(i = 0; i<json[tags_field].length; i++){
                        tagsEntities.put( (json[tags_field][i])[uuid_field], null );
                        tagsNames.put( (json[tags_field][i])[uuid_field], (json[tags_field][i])[readableName_field] );
                    }
                }
            }

            return this;
        }

        public override function toJSONObject():Object{
            var json:Object = super.toJSONObject();
            var tab:Array = new Array();
            var key:String;

            // from
            json[from_field] = JSONObject.parseDate2String( from );

            // to            
            json[to_field] =  JSONObject.parseDate2String( to );

            // title
            json[title_field] = title;
                                            
            // description
            json[description_field] = description;

            // prototype
            json[prototype_field] = (isPrototype) ? "true" : "false";

            // type
            json[type_field] = type.timeFrameType;

            // priority
            json[priority_field] = priority.priority;

            // repeat
            json[repeat_field] = new Object();
            (json[repeat_field])[uuid_field] = repeatUUID;
            (json[repeat_field])[readableName_field] = repeatName;

            // calendar
            json[calendar_field] = new Object();
            (json[calendar_field])[uuid_field] = calendarUUID;
            (json[calendar_field])[readableName_field] = calendarName;

            // meetingRequests
            json[meetingRequest_field] = new Object();
            (json[meetingRequest_field])[uuid_field] = meetingRequestUUID;
            (json[meetingRequest_field])[readableName_field] = meetingRequestName;

            // parent
            json[parent_field] = new Object();
            (json[parent_field])[uuid_field] = parentUUID;
            (json[parent_field])[readableName_field] = parentName;

            // tags
            tab = new Array();
            for (key in tagsNames){
                tab.push( { uuid:key, name:"" } );
            }
            json[tags_field] = tab;


            return json;
        }       
        
    }

}