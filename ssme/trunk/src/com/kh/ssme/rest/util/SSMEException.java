/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest.util;

/**
 * @author Michal Szopinski
 * 
 */
public class SSMEException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2545221933016441149L;
	
	private int status_;
	private String message_;

	public SSMEException(int status, String message) {
		this.status_ = status;
		this.message_ = message;
	}

	public int getStatus() {
		return status_;
	}
	
	@Override
	public String getMessage() {
		return this.message_;
	}		
	
}
