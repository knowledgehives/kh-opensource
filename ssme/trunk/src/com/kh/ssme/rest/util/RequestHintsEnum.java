/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.rest.util;

import com.kh.ssme.business.MeetingRequestService;
import com.kh.ssme.model.enums.ParticipationTypeEnum;
import com.kh.ssme.model.enums.StateEnum;
import com.kh.ssme.model.ifc.Eventing;
import com.kh.ssme.model.ifc.EventingPart;
import com.kh.ssme.model.ifc.MeetingRequest;

import javax.swing.undo.StateEdit;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: MadMax
 * Date: 2011-02-17
 * Time: 22:47:04
 * To change this template use File | Settings | File Templates.
 */
public enum RequestHintsEnum {

    SIMPLE("simple"){
        @Override
        public Hint getHint(String requestUUID) {
            MeetingRequest request = MeetingRequestService.get( requestUUID );
            Map<Integer, Integer> day;
            String[] states;

            // get list of times
            Map<Date, Map<Integer, Integer>> list = new HashMap<Date, Map<Integer, Integer>>();
            for(EventingPart ep : request.getEventings().get(0).getParts()){
                day = new HashMap<Integer, Integer>();
                int i = 0;
                for( String state : ep.getMap().split("") ){    // ['', '-', '-' ...]
                    if( state.length()>0 && !state.equalsIgnoreCase(StateEnum.EMPTY.getMnemonic()) ){
                        day.put( i, 0 );
                    }
                    i++;
                }
                list.put(ep.getDay(), day);
            }

            // count score
            // calculated all timespans with state accepted
            for( Eventing e : request.getEventings() ){
                if( !StateEnum.REJECTED.equals(e.getState()) ){
                    for( EventingPart ep : e.getParts() ){
                        day = list.get( ep.getDay() );
                        states = ep.getMap().split("");
                        for( int i : day.keySet() ){
                            int value = day.get( i );
                            value += ( StateEnum.ACCEPTED.getMnemonic().equals(states[i]) ) ? 1 : 0;
                            day.put( i, value );
                        }
                    }
                }
            }

            // calculate result
            // get one with highest score
            Calendar hintFrom = Calendar.getInstance(), hintTo;
            hintFrom.setTimeInMillis( 0L );
            int max_sum = 0;
            for( Date date : list.keySet() ){
                day = list.get( date );

                for( int i : day.keySet() ){
                    
                    int sum = 0;
                    for(int j=0; j<request.getDuration(); j++){
                        if( day.containsKey((i+j)) ){
                            sum += day.get((i+j));
                        } else {
                            sum = -1;
                            break;
                        }
                    }

                    if( sum>0 && sum>max_sum){
                        max_sum = sum;
                        hintFrom = Calendar.getInstance();
                        hintFrom.setTime( date );
                        hintFrom = TimeUtil.midnight( hintFrom );
                        hintFrom.add( Calendar.MINUTE, (i-1)*15 );
                    }

                    if( max_sum == request.getEventings().size() ){
                        break;
                    }
                }
                if( max_sum == request.getEventings().size() ){
                    break;
                }
            }

            // prepare result
            if( max_sum>0 ){
                // we got the winner!
                Hint result = new Hint("");
                hintTo = (Calendar)hintFrom.clone();
                hintTo.add( Calendar.MINUTE, request.getDuration()*15 );
                result.description = "Highest number of acceptances";
                result.parts = new HintPart[]{ new HintPart( hintFrom.getTime(), hintTo.getTime() ) };
                return result;
            }
            return null;
        }
    },

    WEIGHTED_PARTICIPANT("weightedParticipant"){
        @Override
        public Hint getHint(String requestUUID) {
            Map<Integer, Double> day;
            String[] states;
            MeetingRequest request = MeetingRequestService.get( requestUUID );
            // get list of times
            Map<Date, Map<Integer, Double>> list = new HashMap<Date, Map<Integer, Double>>();
            for(EventingPart ep : request.getEventings().get(0).getParts()){
                day = new HashMap<Integer, Double>();
                int i = 0;
                for( String state : ep.getMap().split("") ){    // ['', '-', '-' ...]
                    if( state.length()>0 && !state.equalsIgnoreCase(StateEnum.EMPTY.getMnemonic()) ){
                        day.put( i, 0.0 );
                    }
                    i++;
                }
                list.put(ep.getDay(), day);
            }

            // count score
            // calculated all timespans with any state (with given weight)
            for( Eventing e : request.getEventings() ){
                for( EventingPart ep : e.getParts() ){
                    day = list.get( ep.getDay() );
                    states = ep.getMap().split("");
                    for( int i : day.keySet() ){
                        double value = day.get( i );
                        value += (StateEnum.ACCEPTED.getMnemonic().equals(states[i])  ? (
                                ParticipationTypeEnum.CREATOR.getName().equals(e.getParticipantType()) ? CREATOR_WEIGHT :
                                    (ParticipationTypeEnum.PARTICIPANT.getName().equals(e.getParticipantType()) ? PARTICIPANT_WEIGHT : OBSERVER_WEIGHT)  
                                ):0);

                        day.put( i, value );
                    }
                }
            }

            // calculate result
            // get one with highest score
            Calendar hintFrom = Calendar.getInstance(), hintTo;
            hintFrom.setTimeInMillis( 0L );
            double max_sum = 0;
            for( Date date : list.keySet() ){
                day = list.get( date );

                for( int i : day.keySet() ){

                    double sum = 0;
                    for(int j=0; j<request.getDuration(); j++){
                        if( day.containsKey((i+j)) ){
                            sum += day.get((i+j));
                        } else {
                            sum = -1;
                            break;
                        }
                    }

                    if( sum>0 && sum>max_sum){
                        max_sum = sum;
                        hintFrom = Calendar.getInstance();
                        hintFrom.setTime( date );
                        hintFrom = TimeUtil.midnight( hintFrom );
                        hintFrom.add( Calendar.MINUTE, (i-1)*15 );
                    }

                    if( max_sum == request.getEventings().size() ){
                        break;
                    }
                }
                if( max_sum == request.getEventings().size() ){
                    break;
                }
            }

            // prepare result
            if( max_sum>0 ){
                // we got the winner!
                Hint result = new Hint("");
                hintTo = (Calendar)hintFrom.clone();
                hintTo.add( Calendar.MINUTE, request.getDuration()*15 );
                result.description = "Highest number of acceptances";
                result.parts = new HintPart[]{ new HintPart( hintFrom.getTime(), hintTo.getTime() ) };
                return result;
            }
            return null;
         }

    } ,
    
    WEIGHTED_STATE("weightedState"){
         @Override
        public Hint getHint(String requestUUID) {
            Map<Integer, Double> day;
            String[] states;
            MeetingRequest request = MeetingRequestService.get( requestUUID );
            // get list of times
            Map<Date, Map<Integer, Double>> list = new HashMap<Date, Map<Integer, Double>>();
            for(EventingPart ep : request.getEventings().get(0).getParts()){
                day = new HashMap<Integer, Double>();
                int i = 0;
                for( String state : ep.getMap().split("") ){    // ['', '-', '-' ...]
                    if( state.length()>0 && !state.equalsIgnoreCase(StateEnum.EMPTY.getMnemonic()) ){
                        day.put( i, 0.0 );
                    }
                    i++;
                }
                list.put(ep.getDay(), day);
            }

            // count score
            // calculated all timespans with any state (with given weight)
            for( Eventing e : request.getEventings() ){
                for( EventingPart ep : e.getParts() ){
                    day = list.get( ep.getDay() );
                    states = ep.getMap().split("");
                    for( int i : day.keySet() ){
                        double value = day.get( i );
                        value += (StateEnum.ACCEPTED.getMnemonic().equals(states[i]) ) ? ACCEPTED_WEIGHT : (
                                        (StateEnum.TENTATIVE.getMnemonic().equals(states[i])) ? TENTATIVE_WEIGHT :
                                                (StateEnum.REJECTED.getMnemonic().equals(states[i]) ? REJECTED_WEIGHT : 0 ));
                        day.put( i, value );
                    }
                }
            }

            // calculate result
            // get one with highest score
            Calendar hintFrom = Calendar.getInstance(), hintTo;
            hintFrom.setTimeInMillis( 0L );
            double max_sum = 0;
            for( Date date : list.keySet() ){
                day = list.get( date );

                for( int i : day.keySet() ){

                    double sum = 0;
                    for(int j=0; j<request.getDuration(); j++){
                        if( day.containsKey((i+j)) ){
                            sum += day.get((i+j));
                        } else {
                            sum = -1;
                            break;
                        }
                    }

                    if( sum>0 && sum>max_sum){
                        max_sum = sum;
                        hintFrom = Calendar.getInstance();
                        hintFrom.setTime( date );
                        hintFrom = TimeUtil.midnight( hintFrom );
                        hintFrom.add( Calendar.MINUTE, (i-1)*15 );
                    }

                    if( max_sum == request.getEventings().size() ){
                        break;
                    }
                }
                if( max_sum == request.getEventings().size() ){
                    break;
                }
            }

            // prepare result
            if( max_sum>0 ){
                // we got the winner!
                Hint result = new Hint("");
                hintTo = (Calendar)hintFrom.clone();
                hintTo.add( Calendar.MINUTE, request.getDuration()*15 );
                result.description = "Highest number of acceptances";
                result.parts = new HintPart[]{ new HintPart( hintFrom.getTime(), hintTo.getTime() ) };
                return result;
            }
            return null;
        }
    },

    WEIGHTED_PARTICIPANT_STATE("weightedParticipantAndState"){
         @Override
        public Hint getHint(String requestUUID) {           
          Map<Integer, Double> day;
            String[] states;
            MeetingRequest request = MeetingRequestService.get( requestUUID );
            // get list of times
            Map<Date, Map<Integer, Double>> list = new HashMap<Date, Map<Integer, Double>>();
            for(EventingPart ep : request.getEventings().get(0).getParts()){
                day = new HashMap<Integer, Double>();
                int i = 0;
                for( String state : ep.getMap().split("") ){    // ['', '-', '-' ...]
                    if( state.length()>0 && !state.equalsIgnoreCase(StateEnum.EMPTY.getMnemonic()) ){
                        day.put( i, 0.0 );
                    }
                    i++;
                }
                list.put(ep.getDay(), day);
            }

            // count score
            // calculated all timespans with any state (with given weight)
            for( Eventing e : request.getEventings() ){
                for( EventingPart ep : e.getParts() ){
                    day = list.get( ep.getDay() );
                    states = ep.getMap().split("");
                    for( int i : day.keySet() ){
                        double value = day.get( i );                        
                        double stateElement = 0.0;
                        double participantElement = 0.0;
                        switch (ep.getState()){
                            case ACCEPTED :
                                        stateElement = ACCEPTED_WEIGHT;
                                        break;
                            case REJECTED:
                                        stateElement = REJECTED_WEIGHT;
                                        break;
                            case TENTATIVE:
                                        stateElement = TENTATIVE_WEIGHT;
                                        break;
                        }
                       switch (e.getParticipantType()){
                           case CREATOR:
                                        participantElement = CREATOR_WEIGHT;
                                        break;
                           case PARTICIPANT:
                                        participantElement = PARTICIPANT_WEIGHT;
                                        break;
                           case OBSERVER :
                                        participantElement = OBSERVER_WEIGHT;
                                        break;
                       }
                        value += stateElement * participantElement;
                        day.put( i, value );
                    }
                }
            }

            // calculate result
            // get one with highest score
            Calendar hintFrom = Calendar.getInstance(), hintTo;
            hintFrom.setTimeInMillis( 0L );
            double max_sum = 0;
            for( Date date : list.keySet() ){
                day = list.get( date );

                for( int i : day.keySet() ){

                    double sum = 0;
                    for(int j=0; j<request.getDuration(); j++){
                        if( day.containsKey((i+j)) ){
                            sum += day.get((i+j));
                        } else {
                            sum = -1;
                            break;
                        }
                    }

                    if( sum>0 && sum>max_sum){
                        max_sum = sum;
                        hintFrom = Calendar.getInstance();
                        hintFrom.setTime( date );
                        hintFrom = TimeUtil.midnight( hintFrom );
                        hintFrom.add( Calendar.MINUTE, (i-1)*15 );
                    }

                    if( max_sum == request.getEventings().size() ){
                        break;
                    }
                }
                if( max_sum == request.getEventings().size() ){
                    break;
                }
            }

            // prepare result
            if( max_sum>0 ){
                // we got the winner!
                Hint result = new Hint("");
                hintTo = (Calendar)hintFrom.clone();
                hintTo.add( Calendar.MINUTE, request.getDuration()*15 );
                result.description = "Highest number of acceptances";
                result.parts = new HintPart[]{ new HintPart( hintFrom.getTime(), hintTo.getTime() ) };
                return result;
            }
            return null;
        }
    };

    private static final double OBSERVER_WEIGHT = 0.2;
    private static final double CREATOR_WEIGHT = 1;
    private static final double PARTICIPANT_WEIGHT = 0.8;
    private static final double ACCEPTED_WEIGHT = 1;
    private static final double TENTATIVE_WEIGHT = 0.7;
    private static final double REJECTED_WEIGHT = -0.5;

    private String type_;

    private RequestHintsEnum(String type){
        this.type_ = type;
    }

    public String type(){
        return type_;
    }

    public abstract Hint getHint(String requestUUID);

    private static final Map<String,RequestHintsEnum> reverse_ = new HashMap<String,RequestHintsEnum>();
    static {
        for(RequestHintsEnum rte : EnumSet.allOf(RequestHintsEnum.class)){
            reverse_.put(rte.type(), rte);
        }
    }    
    public static RequestHintsEnum get(String type){
        return reverse_.get(type);
    }

    public class Hint{
        public String description;
        public HintPart[] parts;        

        Hint(String description){
            this.description = description;
        }
    }
    public class HintPart{
        public Date from;
        public Date to;

        HintPart(Date from, Date to){
            this.from = from;
            this.to = to;
        }
    }

}
