/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Michal Szopinski
 *
 */
public class TimeUtil {

    public static final long _1ms = 1L;
    public static final long _1s = _1ms*1000;
    public static final long _1min = _1s*60;
    public static final long _1h = _1min*60;
    public static final long _1day = _1h*24;
    public static final long _1week = _1day*7;
	
	/**
	 * date format (yyyy-MM-dd'T'HH:mm:ss.SSSZ) ie. (2009-10-12T21:10:29.171+0200)
	 */
	//private static final DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");		
	private static final DateFormat dateTimeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS 'UTC'Z");
    private static final DateFormat simpleDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * Parse given String into Date object
	 * @param string
	 * @return
	 */
	public static Date parse(String string) {
		Date date = null;
		if(string!=null && string.trim().length()>0){
            synchronized (dateTimeFormat) {
                try {
                    date = dateTimeFormat.parse(string.replaceFirst("(\\d{2}):(\\d{2})$", "$1$2"));
                } catch (ParseException e) {
                    //throw new IllegalArgumentException("String does not have an appropriate format! : " + string);
                    System.out.println( "[ERROR] String does not have an appropriate format! : " + string );
                }
            }
        }

		return date;
	}
	
	/**
	 * Formats given Date into String
	 * @param date
	 * @return
	 */
	public static String format(Date date){
        synchronized (dateTimeFormat) {
            return dateTimeFormat.format(date);
        }
	}

	/**
	 * Formats given Date into String
	 * @param date
	 * @return
	 */
	public static String simpleFormat(Date date){
        synchronized (simpleDateTimeFormat) {
            return simpleDateTimeFormat.format(date);
        }
	}

    public static Calendar midnight(Calendar cal){
        Calendar result = Calendar.getInstance();
        result.setTimeInMillis( cal.getTimeInMillis() );
        result.set(Calendar.HOUR_OF_DAY, 0);
        result.set(Calendar.MINUTE, 0);
        result.set(Calendar.SECOND, 0);
        result.set(Calendar.MILLISECOND, 0);
        return result;
    }

}
