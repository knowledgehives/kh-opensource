/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.rest;

import com.kh.ssme.business.MetaService;
import com.kh.ssme.rest.util.Params;
import com.kh.ssme.rest.util.SSMEException;
import com.kh.vulcan.annotation.Servlet;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: MadMax
 * Date: 2011-02-16
 * Time: 21:53:28
 * To change this template use File | Settings | File Templates.
 */
@Servlet(urlMappings = { "/meta/*", "/meta" })
public class MetaDataServlet extends UtilServlet {


	/**
	 *
	 */
	private static final long serialVersionUID = -7871422893206760214L;

	@SuppressWarnings("hiding")
	protected static Logger logger_ = LoggerFactory.getLogger(ErrorServlet.class);	    

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        String[] uuids = getUUIDs(request);        
        String command = request.getParameter( Params.RequestParams.META_COMMAND.paramName() );
        
        if( command!= null && Commands.get(command)!=null ){
            JSONObject result = new JSONObject();

            switch( Commands.get(command) ){
                case REQUEST_HINTS:{
                        result = MetaService.getRequestHints( uuids[0] ); 
                        break;
                    }
            }

            respondWithJSON(response, result, "application/json", true);            
        }

    }

    public enum Commands{

        REQUEST_HINTS("requestHints");

		private String command_;
	    private static final Map<String,Commands> reverse_ = new HashMap<String,Commands>();
	    static {
	        for(Commands rte : EnumSet.allOf(Commands.class)){
	        	reverse_.put(rte.command(), rte);
            }
	    }

		private Commands(String command){
			this.command_ = command;
		}

	    public String command(){
	    	return command_;
	    }

	    public static Commands get(String command){
	    	return reverse_.get(command);
	    }
    }    

}
