/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest;

import com.kh.ssme.business.UserService;
import com.kh.ssme.model.ifc.User;
import com.kh.ssme.rest.util.Params;
import com.kh.ssme.rest.util.ResponseType;
import com.kh.ssme.rest.util.SSMEException;
import com.kh.ssme.rest.util.SSMEExceptionType;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.util.regex.Pattern;

/**
 * @author Michal Szopinski
 *
 */
public abstract class UtilServlet extends HttpServlet {
	
		
	/**
	 * 
	 */
	private static final long serialVersionUID = -7871422893206760213L;
	
	protected static Logger logger_ = LoggerFactory.getLogger(UtilServlet.class);	
		
	public static final String LOGIN_SERVLET = "login.jsp";
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		if ("delete".equalsIgnoreCase(request.getParameter( Params.RequestParams.METHOD.paramName() ))) {
			doDelete(request, response);
		} else if ("put".equalsIgnoreCase(request.getParameter( Params.RequestParams.METHOD.paramName() ))) {
			doPut(request, response);
		} else {
			super.service(request, response);
		}

	}
	
	
	/**
	 * Returns UUID from the request URL.
	 * 
	 * @param request
	 * @return
	 */
	protected String[] getUUIDs(HttpServletRequest request) {
		String[] strings = request.getRequestURI().split("/");
		if(strings.length == 4 && strings[3]!=null && !"null".equalsIgnoreCase(strings[3])){
			return new String[] { strings[3] };
		}
		return null;
	}	
	
	/**
	 * Returns level of data creations recursion
	 * 
	 * @param request
	 * @return
	 */
	protected int getDataRecursionLevel(HttpServletRequest request) {
		try{
			return Integer.parseInt( request.getParameter( Params.RequestParams.DATA_RECURSION.paramName() ) );
		} catch(Exception e){
			return 0;
		}
		 
	}		
	
	/**
	 * Return UserEntity object of currently logged user
	 * @param req
	 * @return
	 */
	protected User getLoggedUser(HttpServletRequest req){
		String uuid = (String) req.getSession().getAttribute( Params.SessionAttributes.USER_ENTITY.paramName() );
		if( uuid!=null && uuid.trim().length()>0 ){
			return UserService.get( uuid );				
		}
		return null;
	}
	
	/**
	 * 
	 * @param response
	 * @param json
	 * @param type
	 * @param isCache
	 * @throws IOException 
	 */
	protected void respondWithJSON(HttpServletResponse response, JSONObject json, String type, boolean isCache) throws IOException {
		response.setContentType(type+";charset=UTF-8"); 
		
		if (isCache){
			response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Pragma","no-cache"); //HTTP 1.0  
			response.setDateHeader("Expires", 0); 
		}
		if (json != null){
			Writer writer = response.getWriter();
			writer.append( json.toString() );
			writer.flush();
		} else {
			response.setContentLength(0); 
		}
		
	}

	/**
	 * @param response
	 * @param content
	 * @param type
	 * @param isCache
	 * @throws IOException
	 */
	protected void respondWithText(HttpServletResponse response, String content, String type, boolean isCache) throws IOException {
		response.setContentType(type+";charset=UTF-8");

		if (isCache){
			response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Pragma","no-cache"); //HTTP 1.0
			response.setDateHeader("Expires", 0);
		}
		if (content != null){
			Writer writer = response.getWriter();
			writer.append( content );
			writer.flush();
		} else {
			response.setContentLength(0);
		}

	}

	/**
	 * Response with error page when exception occurs
	 * @param request
	 * @param response
	 * @param e
	 * @throws IOException
	 */
	protected void respondWithException(HttpServletRequest request, HttpServletResponse response, SSMEException e) throws IOException{
		ResponseType.determine(request).sendError( response, e );
	}

	protected void respondWithException(HttpServletRequest request, HttpServletResponse response, Exception e) throws IOException{
        respondWithException(request, response, e, 500);    
    }
	protected void respondWithException(HttpServletRequest request, HttpServletResponse response, Exception e, int status) throws IOException{
		e.printStackTrace();
		ResponseType.determine(request).sendExceptionError( response, status, e.getMessage() );
	}
	
	
	/**
	 * Retrieves JSON object from request
	 * @param request
	 * @param mustExist
	 * @return
	 * @throws SSMEException
	 */
	protected JSONObject retrieveJSON(HttpServletRequest request, boolean mustExist) throws SSMEException {
		JSONObject object = null;
		String contentType = request.getContentType();
		

		if (contentType == null || 
			contentType.startsWith("application/json") || 				
			contentType.startsWith("application/x-www-form-urlencoded") || 
			contentType.startsWith("text/x-json") || 
			contentType.startsWith("text/x-javascript")) {

			try {
				String json = request.getParameter("json");
				if (json != null) {
					object = new JSONObject(json);
				} else {
					StringBuffer buffer = new StringBuffer();
					BufferedReader reader = request.getReader();
					String temp;
					while ( (temp = reader.readLine()) != null){
						buffer.append( temp );
					}
					object = new JSONObject( buffer.toString() );
				}
			} catch (Exception e) {
				throw new SSMEException( SSMEExceptionType.JSON_PARSE_ERROR.getStatus(), SSMEExceptionType.JSON_PARSE_ERROR.getMessage() );
			} catch (Error e) {
				throw new SSMEException( SSMEExceptionType.JSON_PARSE_ERROR.getStatus(), SSMEExceptionType.JSON_PARSE_ERROR.getMessage() );
			}
		}

		if (mustExist && object == null) {
			throw new SSMEException( SSMEExceptionType.JSON_NOT_FOUND.getStatus(), SSMEExceptionType.JSON_NOT_FOUND.getMessage() );
		}

		return object;
	}		
	
	public static Pattern UUID_REGEXP = Pattern.compile( "[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}" );
	
	protected boolean isUUID(String uuid){
		return UUID_REGEXP.matcher(uuid).matches();
	}
	
}
