/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.rest;

import com.kh.ssme.business.CalendarService;
import com.kh.ssme.business.ExportService;
import com.kh.ssme.model.ifc.Calendar;
import com.kh.ssme.model.ifc.User;
import com.kh.ssme.rest.parsers.ExportParsersEnum;
import com.kh.ssme.rest.util.Params;
import com.kh.ssme.rest.util.TimeUtil;
import com.kh.vulcan.annotation.Servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: MadMax
 * Date: 2011-04-07
 * Time: 16:38:30
 * To change this template use File | Settings | File Templates.
 */

@Servlet(urlMappings = {"/export/*", "/export"})
public class ExportServlet extends UtilServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws javax.servlet.ServletException, java.io.IOException {

        try {
            // get the file
            String[] uuids = getUUIDs(request);
            Calendar calendar;
            if(uuids!=null && uuids.length>0){
                calendar = CalendarService.get( uuids[0] );
            } else {
                long now = new Date().getTime();
                calendar = CalendarService.getAll(getLoggedUser(request).getUUID(), now, now+(TimeUtil._1week*53));    // ~1 year
            }

            // export
            String exportType = request.getParameter(Params.RequestParams.EXPORT_TYPE.paramName());
            String result = "";
            if (ExportParsersEnum.containsType(exportType)) {
                result = ExportService.process(ExportParsersEnum.getType(exportType), calendar);
            }

            respondWithText(response, result, "text/plain", true);
        } catch (Exception e) {
            respondWithException(request, response, e);
        }

    }

}
