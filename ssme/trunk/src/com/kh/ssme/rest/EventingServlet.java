/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.rest;

import com.kh.ssme.business.EventingService;
import com.kh.ssme.business.MeetingRequestService;
import com.kh.ssme.model.ifc.BasicData;
import com.kh.ssme.model.ifc.Eventing;
import com.kh.ssme.model.ifc.User;
import com.kh.ssme.rest.parsers.EntityParserTools;
import com.kh.ssme.rest.util.Params;
import com.kh.vulcan.annotation.Servlet;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: MadMax
 * Date: 2010-12-05
 * Time: 19:04:30
 * To change this template use File | Settings | File Templates.
 */
@Servlet(urlMappings = { "/eventing/*", "/eventing" })
public class EventingServlet extends BasicServlet {
        /**
         *
         */
        private static final long serialVersionUID = -888754659268254934L;

        @SuppressWarnings("hiding")
        protected static Logger logger_ = LoggerFactory.getLogger(EventingServlet.class);

        /* (non-Javadoc)
         * @see com.kh.ssme.rest.BasicServlet#doDelete(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
         */
        @Override
        protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
            String[] uuids = getUUIDs(request);

            try {
                Eventing eventing = null;
                eventing = EventingService.delete(uuids[0]);
                respond(request, response, eventing);
            } catch (Exception e) {
                respondWithException(request, response, e);
            }
        }

        /* (non-Javadoc)
         * @see com.kh.ssme.rest.BasicServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
         */
        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
            String[] uuids = getUUIDs(request);
            try{
                if(uuids != null && uuids.length > 0){
                    String requestsParticipants = request.getParameter( Params.RequestParams.REQUEST_EVENTINGS.paramName() );
                    if( "true".equalsIgnoreCase(requestsParticipants) ){
                        List list = EventingService.getEventingsForRequest(uuids[0]);
                        respondWithList(request, response, list, getDataRecursionLevel(request));
                    } else {
                        Eventing eventing = EventingService.get(uuids[0]);
                        respond(request, response, eventing, getDataRecursionLevel(request));
                    }

                } else {
                    // no uuid - load dummy 'all eventing' collection for currently logged user
                    User user = getLoggedUser(request);
                    if(user!=null){
                        List list = EventingService.getAllParticipations( user.getUUID() );
                        respondWithList(request, response, list, getDataRecursionLevel(request));
                    }
                }
                    
            } catch (Exception e) {
                respondWithException(request, response, e);
            }
        }

        /* (non-Javadoc)
         * @see com.kh.ssme.rest.BasicServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
         */
        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
            String[] uuids = getUUIDs(request);

            try {
                Eventing eventing = null;
                JSONObject object = retrieveJSON(request, true);
                eventing = EventingService.update(uuids[0], object);
                respond(request, response, eventing);
            } catch (Exception e) {
                respondWithException(request, response, e);
            }
        }

        /* (non-Javadoc)
         * @see com.kh.ssme.rest.BasicServlet#doPut(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
         */
        @Override
        protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
            try {
                Eventing eventing = null;
                JSONObject object = retrieveJSON(request, true);
                eventing = EventingService.create(object);
                respond(request, response, eventing);
            } catch (Exception e) {
                respondWithException(request, response, e);
            }
        }

        /* (non-Javadoc)
         * @see com.kh.ssme.rest.BasicServlet#getJSP()
         */
        @Override
        protected String getJSP() {
            return "/eventing.jsp";
        }

        /* (non-Javadoc)
         * @see com.kh.ssme.rest.BasicServlet#getJSON(com.kh.ssme.model.ifc.BasicData)
         */
        @Override
        protected JSONObject getJSON(BasicData data, int recursionLevel) {
            JSONObject object = new JSONObject();
            try {
                EntityParserTools.getCreator(EntityParserTools.CreatorType.JSON_CREATOR).create((Eventing)data, object, recursionLevel);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return object;
        }

        @Override
        protected String getRDF(BasicData data, int recursionLevel) {
            StringBuilder builder = new StringBuilder();
            try {
                EntityParserTools.getCreator(EntityParserTools.CreatorType.RDF_CREATOR).create((Eventing)data, builder, recursionLevel);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return builder.toString();
        }

    }

