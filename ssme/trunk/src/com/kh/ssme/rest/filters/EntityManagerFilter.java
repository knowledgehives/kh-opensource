/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.rest.filters;

import com.kh.ssme.manage.PersistenceManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: MadMax
 * Date: 2011-02-07
 * Time: 20:47:05
 * To change this template use File | Settings | File Templates.
 */
public class EntityManagerFilter implements Filter {

	protected static Logger logger_ = LoggerFactory.getLogger(EntityManagerFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // empty;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		try{
			filterChain.doFilter(servletRequest, servletResponse);
		}
		catch(Exception ex){
			logger_.info("Problem with invoking next filter: ", ex);
		}
		finally{
            // release EntityManager scoped for this request
			PersistenceManager.getInstance().getEntityManager().remove();
		}
    }

    @Override
    public void destroy() {
        // empty;
    }
}
