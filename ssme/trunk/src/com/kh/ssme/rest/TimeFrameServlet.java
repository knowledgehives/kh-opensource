/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest;

import com.kh.ssme.business.TimeFrameService;
import com.kh.ssme.model.ifc.BasicData;
import com.kh.ssme.model.ifc.TimeFrame;
import com.kh.ssme.rest.parsers.EntityParserTools;
import com.kh.ssme.rest.parsers.EntityParserTools.CreatorType;
import com.kh.ssme.rest.util.Params;
import com.kh.vulcan.annotation.Servlet;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Michal Szopinski
 *
 */
@Servlet(urlMappings = { "/timeFrame/*", "/timeFrame" })
public class TimeFrameServlet extends BasicServlet {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = -5899154429652663817L;
	@SuppressWarnings("hiding")
	protected static Logger logger_ = LoggerFactory.getLogger(TimeFrameServlet.class);


	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.BasicServlet#doDelete(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String[] uuids = getUUIDs(request);	
		
		try {
			TimeFrame timeFrame = null;
			timeFrame = TimeFrameService.delete(uuids[0]);
			respond(request, response, timeFrame);
		} catch (Exception e) {
			respondWithException(request, response, e);
		}
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.BasicServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String[] uuids = getUUIDs(request);
		try {
			TimeFrame timeFrame = null;
			timeFrame = TimeFrameService.get(uuids[0]);
			respond(request, response, timeFrame);
		} catch (Exception e) {
			respondWithException(request, response, e);
		}	
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.BasicServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String[] uuids = getUUIDs(request);
		
		try {
			// get
			TimeFrame timeFrame = null;
			JSONObject object = retrieveJSON(request, true);
			
			// type of update
			String repeatTypeUUID = request.getParameter( Params.RequestParams.TIMEFRAME_PARENT.paramName() );			
			if(repeatTypeUUID != null && isUUID(repeatTypeUUID)){
				timeFrame = TimeFrameService.updateRemoveParent(uuids[0], repeatTypeUUID, object);			
			} else {
				timeFrame = TimeFrameService.update(uuids[0], object);
			}
			respond(request, response, timeFrame);
		} catch (Exception e) {
			respondWithException(request, response, e);
		}	
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.BasicServlet#doPut(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			TimeFrame timeFrame = null;
			JSONObject object = retrieveJSON(request, true);
			timeFrame = TimeFrameService.create(object);
			respond(request, response, timeFrame);
		} catch (Exception e) {
			respondWithException(request, response, e);
		}	
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.BasicServlet#getJSP()
	 */
	@Override
	protected String getJSP() {
		return "/timeFrame.jsp";
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.BasicServlet#getJSON(com.kh.ssme.model.ifc.BasicData)
	 */
	@Override
	protected JSONObject getJSON(BasicData data, int recursionLevel) {
		JSONObject object = new JSONObject();		
		try {
			EntityParserTools.getCreator(CreatorType.JSON_CREATOR).create((TimeFrame)data, object, recursionLevel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}

    @Override
	protected String getRDF(BasicData data, int recursionLevel) {
		StringBuilder builder = new StringBuilder();
		try {
			EntityParserTools.getCreator(CreatorType.RDF_CREATOR).create((TimeFrame)data, builder, recursionLevel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return builder.toString();
	}

}
