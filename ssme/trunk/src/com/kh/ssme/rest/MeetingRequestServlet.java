/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.rest;

import com.kh.ssme.business.MeetingRequestService;
import com.kh.ssme.model.ifc.BasicData;
import com.kh.ssme.model.ifc.MeetingRequest;
import com.kh.ssme.model.ifc.User;
import com.kh.ssme.rest.parsers.EntityParserTools;
import com.kh.ssme.rest.util.Params;
import com.kh.vulcan.annotation.Servlet;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * User: MadMax
 * Date: 2010-11-18
 * Time: 20:53:03
 * To change this template use File | Settings | File Templates.
 */
@Servlet(urlMappings = { "/meetingRequest/*", "/meetingRequest" })
public class MeetingRequestServlet extends BasicServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = -888754659268254934L;
    
	@SuppressWarnings("hiding")
	protected static Logger logger_ = LoggerFactory.getLogger(MeetingRequestServlet.class);

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.BasicServlet#doDelete(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String[] uuids = getUUIDs(request);

		try {
			MeetingRequest meetingRequest = null;
			String removeChildren = request.getParameter( Params.RequestParams.REMOVE_REQUEST_CHILDREN.paramName() );
            if( removeChildren!=null && "true".equalsIgnoreCase(removeChildren) ){
			    meetingRequest = MeetingRequestService.deleteWithChildren(uuids[0]);
            } else {
			    meetingRequest = MeetingRequestService.delete(uuids[0]);
            }
			respond(request, response, meetingRequest);
		} catch (Exception e) {
			respondWithException(request, response, e);
		}
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.BasicServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String[] uuids = getUUIDs(request);
		try{

			if(uuids != null && uuids.length > 0){
                MeetingRequest meetingRequest = MeetingRequestService.get(uuids[0]);
                respond(request, response, meetingRequest, getDataRecursionLevel(request));
			} else {
				// no uuid - load dummy 'all' meetingRequest for currently logged user
                String activeRequests = request.getParameter( Params.RequestParams.ACTIVE_REQUEST.paramName() );
				User user = getLoggedUser(request);
				if(user!=null){
                    List list;
                    if(activeRequests!=null && "true".equalsIgnoreCase(activeRequests)){
					    list = MeetingRequestService.getActive(user.getUUID());
                    } else {
					    list = MeetingRequestService.getCreated(user.getUUID());
                    }
			        respondWithList(request, response, list, getDataRecursionLevel(request));                    
				}
			}
		} catch (Exception e) {
			respondWithException(request, response, e);
		}
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.BasicServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String[] uuids = getUUIDs(request);

		try {
			MeetingRequest meetingRequest = null;
			JSONObject object = retrieveJSON(request, true);

			long from = -1;
			long to = -1;
			try{
				// check if timerange is specified
				from = Long.parseLong( request.getParameter( Params.RequestParams.CALENDAR_FROM.paramName() ) );
				to = Long.parseLong( request.getParameter( Params.RequestParams.CALENDAR_TO.paramName() ) );
			} catch (Exception e){
				//empty block
			}
			String updateToEvent = request.getParameter( Params.RequestParams.UPDATE_REQUEST_TO_EVENT.paramName() );
            if( updateToEvent!=null && "true".equalsIgnoreCase(updateToEvent)
                    && from>0 && to>0){
			    meetingRequest = MeetingRequestService.updateToEvent(uuids[0], from, to);
            } else {
			    meetingRequest = MeetingRequestService.update(uuids[0], object);
            }

			respond(request, response, meetingRequest);
		} catch (Exception e) {
			respondWithException(request, response, e);
		}
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.BasicServlet#doPut(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {
		try {
			MeetingRequest meetingRequest = null;
			JSONObject object = retrieveJSON(request, true);
			meetingRequest = MeetingRequestService.create(object);
			respond(request, response, meetingRequest);
		} catch (Exception e) {
			respondWithException(request, response, e);
		}
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.BasicServlet#getJSP()
	 */
	@Override
	protected String getJSP() {
		return "/meetingRequest.jsp";
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.BasicServlet#getJSON(com.kh.ssme.model.ifc.BasicData)
	 */
	@Override
	protected JSONObject getJSON(BasicData data, int recursionLevel) {
		JSONObject object = new JSONObject();
		try {
			EntityParserTools.getCreator(EntityParserTools.CreatorType.JSON_CREATOR).create((MeetingRequest)data, object, recursionLevel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return object;
	}

	@Override
	protected String getRDF(BasicData data, int recursionLevel) {
		StringBuilder builder = new StringBuilder();
		try {
			EntityParserTools.getCreator(EntityParserTools.CreatorType.RDF_CREATOR).create((MeetingRequest)data, builder, recursionLevel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return builder.toString();
	}

}
