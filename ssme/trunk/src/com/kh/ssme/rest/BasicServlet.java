/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest;

import com.kh.ssme.model.ifc.BasicData;
import com.kh.ssme.rest.util.ResponseType;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Michal Szopinski
 * Basic servlet class for REST API servlets
 */
public abstract class BasicServlet extends UtilServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1765991437759871774L;
	
	@SuppressWarnings("hiding")
	protected static Logger logger_ = LoggerFactory.getLogger(BasicServlet.class);
	
	/**
	 * pattern that describes the canonical form of UUID:
	 * <code>\d{8}[-]\d{4}[-]\d{4}[-]\d{4}[-]\d{12}</code>
	 */
	public static final Pattern UUID_PATTERN;
	static{
		UUID_PATTERN = Pattern.compile("\\w{8}[-]\\w{4}[-]\\w{4}[-]\\w{4}[-]\\w{12}");		
	}
	
	@Override	
	protected abstract void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException;
	
	@Override
	protected abstract void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException;
		
	@Override	
	protected abstract void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException;
	
	@Override
	protected abstract void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException;	
	
	/**
	 * Return jsp page which will render content to HTML
	 * @return 
	 */
	protected abstract String getJSP();
	
	/**
	 * Return jsp page which will render content to HTML
	 * @return 
	 * @throws Exception 
	 */
	protected abstract JSONObject getJSON(BasicData data, int recursionLevel);

	protected abstract String getRDF(BasicData data, int recursionLevel);

	protected JSONObject getJSONCollection(List<BasicData> list, int recursionLevel){
		JSONObject result = new JSONObject();
        JSONArray collection = new JSONArray();
        for(BasicData data : list){
            if( !data.isDeleted() ){
                collection.put( getJSON(data, recursionLevel) );                
            }
        }
        try{
            result.put( BasicData.FIELD_COLLECTION, collection );
        } catch (Exception e){
            e.printStackTrace( );
        }
        return result;
	}
	
	protected void respond(HttpServletRequest request, HttpServletResponse response, BasicData data) throws IOException, ServletException{
		respond(request, response, data, 0);	// no recursion while data generation
	}
			
	/**
	 * Responds with proper type
	 * @param request
	 * @param response
	 * @param data
	 * @param recursionLevel
	 * @throws IOException
	 * @throws ServletException
	 */
	protected void respond(HttpServletRequest request, HttpServletResponse response, BasicData data, int recursionLevel) throws IOException, ServletException{
		JSONObject object = null;
		switch (ResponseType.determine(request)) {
			case JSON:
				if( data!=null && !data.isDeleted() ){
					object = getJSON(data, recursionLevel);
				}
				respondWithJSON(response, object, "application/json", true);
				break;
				
			case FLEX_JSON:
				if( data!=null && !data.isDeleted() ){				
					object = getJSON(data, recursionLevel);
				}
				respondWithJSON(response, object, "application/json", true);
				break;

			case RDF:
                String rdf = "";
				if( data!=null && !data.isDeleted() ){
					rdf = getRDF(data, recursionLevel);
				}
				respondWithText(response, rdf, "application/rdf+xml", true);
				break;
				
			default:
				if( data!=null && !data.isDeleted() ){		
					request.setAttribute("entity", data);
				}
				request.getRequestDispatcher(getJSP()).forward(request, response);
				break;				
		}
			
	}

    protected void respondWithList(HttpServletRequest request, HttpServletResponse response, List data, int recursionLevel) throws IOException, ServletException{
		JSONObject object = null;
		switch (ResponseType.determine(request)) {
			case JSON:
				if( data!=null && !data.isEmpty() ){
					object = getJSONCollection(data, recursionLevel);
				}
				respondWithJSON(response, object, "application/json", true);
				break;

			case FLEX_JSON:
				if( data!=null && !data.isEmpty() ){
					object = getJSONCollection(data, recursionLevel);
				}
				respondWithJSON(response, object, "application/json", true);
				break;

			default:
				if( data!=null && !data.isEmpty() ){
					request.setAttribute("entityList", data);
				}
				request.getRequestDispatcher(getJSP()).forward(request, response);
				break;
		}
    }

	
}
