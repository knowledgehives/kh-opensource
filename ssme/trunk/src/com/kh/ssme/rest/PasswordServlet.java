/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest;

import com.kh.ssme.business.PasswordService;
import com.kh.ssme.model.ifc.User;
import com.kh.ssme.rest.util.SSMEException;
import com.kh.ssme.rest.util.SSMEExceptionType;
import com.kh.vulcan.annotation.Servlet;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * @author Michal Szopinski
 *
 */
@Servlet(urlMappings = { "/password/*", "/password" })
public class PasswordServlet extends UserServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5899558971232129050L;
	
	@SuppressWarnings("hiding")
	protected static Logger logger_ = LoggerFactory.getLogger(PasswordServlet.class);

	
	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.BasicServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String[] uuids = getUUIDs(request);
		User user = getLoggedUser(request);
		
		try {
			if(user != null){
				JSONObject object = retrieveJSON(request, true);
				String oldPlainPassword = (String) object.get("oldPlainPassword");
				String newPlainPassword = (String) object.get("newPlainPassword");		
				

				if(user.isAdmin() && uuids.length>0 && uuids[0]!=null){
					// ADMIN can change password for everyone					
					user = PasswordService.updateUsersPassword(uuids[0], oldPlainPassword, newPlainPassword);
				} else {
					// Both USER and ADMIN can change his/her password
					user = PasswordService.updateUsersPassword(user.getUUID(), oldPlainPassword, newPlainPassword);
				}								
			}
			
			if(user == null){
				respondWithException( request, response, new SSMEException( SSMEExceptionType.WRONG_PASSWORD.getStatus(), SSMEExceptionType.WRONG_PASSWORD.getMessage() ) );			
			} 			
			respond(request, response, user);				
		} catch (Exception e) {
			respondWithException(request, response, e);
		}	
		
			
	}
	
	
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "");	
	}
		
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {		
		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "");		
	}
	
	
	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException {	
		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "");			
	}
	
}
