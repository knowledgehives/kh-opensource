/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.rest;

import com.kh.ssme.business.ImportService;
import com.kh.ssme.model.ifc.User;
import com.kh.ssme.rest.parsers.ImportParsersEnum;
import com.kh.ssme.rest.util.Params;
import com.kh.ssme.rest.util.SSMEException;
import com.kh.ssme.rest.util.SSMEExceptionType;
import com.kh.vulcan.annotation.Servlet;
import net.fortuna.ical4j.data.ParserException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: MadMax
 * Date: 2011-03-22
 * Time: 20:22:19
 * To change this template use File | Settings | File Templates.
 */

@Servlet(urlMappings = { "/import/*", "/import" })
public class ImportServlet extends UtilServlet {

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.BasicServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

		try {
            // get the file
            String fileContent = retrieveImportFile( request );
		    String[] uuids = getUUIDs(request);            
            User user = getLoggedUser(request);

            // process
            if( fileContent!=null && fileContent.trim().length()>0 ){
                String importType = request.getParameter( Params.RequestParams.IMPORT_TYPE.paramName() );
                if( ImportParsersEnum.containsType( importType ) ){
                    ImportService.process( ImportParsersEnum.getType( importType ), fileContent, user );
                }
            }
		} catch (ParserException e) {
			respondWithException(request, response, e, HttpServletResponse.SC_BAD_REQUEST);
		} catch (Exception e) {
			respondWithException(request, response, e);
		}

	}

    /**
     * Retrives imported file
     * @param request
     * @return
     * @throws SSMEException
     */
	protected String retrieveImportFile(HttpServletRequest request) throws SSMEException {

        try {
            String fileContent = request.getParameter("data");
            if (fileContent == null){
                StringBuffer buffer = new StringBuffer();
                BufferedReader reader = request.getReader();
                String temp;
                while ( (temp = reader.readLine()) != null){
                    buffer.append( temp );
                }
                fileContent = buffer.toString();
            }
            return fileContent;
        } catch (Exception e) {
            throw new SSMEException( SSMEExceptionType.IMPORT_ERROR.getStatus(), SSMEExceptionType.IMPORT_ERROR.getMessage() );
        }
        
    }
}
