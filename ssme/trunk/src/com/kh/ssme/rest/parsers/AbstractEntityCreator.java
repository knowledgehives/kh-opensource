/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest.parsers;

import com.kh.ssme.model.ifc.*;


/**
 * @author Michal Szopinski
 * Interface with methods for creating object for respond from entity object. 
 */
public interface AbstractEntityCreator {
	
	/**
	 * Create object for respond from common BasicData entity 
	 * @param basicData
	 * @param result
	 * @param recursionLevel
     * @throws Exception
	 */
	public void create(BasicData basicData, Object result, int recursionLevel) throws Exception;
	
	/**
	 * Create object for respond from Calendar entity 
	 * @param calendar
	 * @param result
	 * @param recursionLevel
     * @throws Exception
	 */
	public void create(Calendar calendar, Object result, int recursionLevel) throws Exception;
	
	/**
	 * Create object for respond from Eventing entity 
	 * @param eventing
	 * @param result
	 * @param recursionLevel
     * @throws Exception
	 */
	public void create(Eventing eventing, Object result, int recursionLevel) throws Exception;

	/**
	 * Create object for respond from EventingPart entity
	 * @param eventingPart
	 * @param result
	 * @param recursionLevel
     * @throws Exception
	 */
	public void create(EventingPart eventingPart, Object result, int recursionLevel) throws Exception;
	
	/**
	 * Create object for respond from Group entity 
	 * @param group
	 * @param result
	 * @param recursionLevel
     * @throws Exception
	 */
	public void create(Group group, Object result, int recursionLevel) throws Exception;
		
	/**
	 * Create object for respond from MeetingRequest entity 
	 * @param meetingRequest
	 * @param result
	 * @param recursionLevel
     * @throws Exception
	 */
	public void create(MeetingRequest meetingRequest, Object result, int recursionLevel) throws Exception;
	
	/**
	 * Create object for respond from RepeatType entity 
	 * @param repeatType
	 * @param result
	 * @param recursionLevel
     * @throws Exception
	 */
	public void create(RepeatType repeatType, Object result, int recursionLevel) throws Exception;
	
	/**
	 * Create object for respond from Role entity 
	 * @param role
	 * @param result
	 * @param recursionLevel
     * @throws Exception
	 */
	public void create(Role role, Object result, int recursionLevel) throws Exception;

	/**
	 * Create object for respond from Tag entity 
	 * @param tag
	 * @param result
	 * @param recursionLevel
     * @throws Exception
	 */
	public void create(Tag tag, Object result, int recursionLevel) throws Exception;

	/**
	 * Create object for respond from TimeFrame entity 
	 * @param timeFrame
	 * @param result
	 * @param recursionLevel
     * @throws Exception
	 */
	public void create(TimeFrame timeFrame, Object result, int recursionLevel) throws Exception;	
		
	/**
	 * Create object for respond from User entity 
	 * @param user
	 * @param result
	 * @param recursionLevel
     * @throws Exception
	 */
	public void create(User user, Object result, int recursionLevel) throws Exception;

}
