/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest.parsers;

import org.json.JSONObject;


/**
 * @author Michal Szopinski
 * Parsers Factory
 */
public class EntityParserTools {
	
	/**
	 * Enumerates provided types of Creator
	 * @author Michal Szopinski
	 *
	 */
	public enum CreatorType {
		JSON_CREATOR,
        RDF_CREATOR
	}
	
	/**
	 * Returns merger linked with given object type
	 * @param o object to be parsed
	 * @return merger
	 */
	public static AbstractEntityMerger getMerger(Object o){
		if(o instanceof JSONObject){
			return JSONEntityMerger.getInstance();
		}
		return null;		
	}
	
	public static AbstractEntityCreator getCreator(CreatorType type){
		if(type == CreatorType.JSON_CREATOR){
			return JSONEntityCreator.getInstance();
		}
		if(type == CreatorType.RDF_CREATOR){
			return RDFEntityCreator.getInstance();
		}
		return null;
	}

}
