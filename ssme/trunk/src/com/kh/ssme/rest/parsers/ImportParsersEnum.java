/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.rest.parsers;

import com.kh.ssme.model.entity.CalendarEntity;
import com.kh.ssme.model.entity.RepeatTypeEntity;
import com.kh.ssme.model.entity.TimeFrameEntity;
import com.kh.ssme.model.enums.PriorityEnum;
import com.kh.ssme.model.enums.RepeatTypeEnum;
import com.kh.ssme.model.enums.TimeFrameTypeEnum;
import com.kh.ssme.model.ifc.*;
import com.kh.ssme.model.ifc.Calendar;
import com.kh.ssme.rest.util.TimeUtil;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.model.*;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.RRule;

import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by IntelliJ IDEA.
 * User: MadMax
 * Date: 2011-03-22
 * Time: 20:38:25
 * To change this template use File | Settings | File Templates.
 */
public enum ImportParsersEnum {
    I_CALENDAR("iCalendar"){
        
		@Override
		public List<BasicData> process(String fileContent, User user) throws IOException, ParserException, ParseException {
            StringReader sin = new StringReader( fileContent );
            CalendarBuilder builder = new CalendarBuilder();
            Calendar resultCalendar = new CalendarEntity();
            List<BasicData> result = new ArrayList<BasicData>();
            result.add( resultCalendar );

            // parse calendar
            net.fortuna.ical4j.model.Calendar calendar = builder.build(sin);

            // bind resultCalendar to user
            resultCalendar.setUser( user );
            user.getCalendars().add(resultCalendar);
            // set resultCalendar properties
            String[] prodidparts = calendar.getProductId().getValue().split("//");
            String name = "Imported: ";
            name += (prodidparts.length>1) ? prodidparts[2]+" " : "";
            name += "("+TimeUtil.simpleFormat(new Date())+")";
            resultCalendar.setName( name );

            // attach events
            // get time zone
            TimeZone tz = TimeZone.getTimeZone( "UTC" );
            for(Object component : calendar.getComponents() ){
                if( component instanceof VTimeZone ){
                    tz = TimeZone.getTimeZone( ((VTimeZone)component).getTimeZoneId().getValue() );
                    break;
                }
            }
            // parse events
            DateFormat dateTimeFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
            dateTimeFormat.setTimeZone( tz );
            for(Object component : calendar.getComponents() ){
                if( component instanceof VEvent){
                    VEvent event = (VEvent)component;
                    TimeFrameEntity prototype = new TimeFrameEntity();
                    // bind
                    prototype.setCalendar(resultCalendar);
                    resultCalendar.getTimeFrames().add( prototype );
                    //set
                    prototype.setTo( dateTimeFormat.parse( event.getEndDate().getValue() ) );       // eg. 20110314T123000
                    prototype.setFrom( dateTimeFormat.parse( event.getStartDate().getValue() ) );   // eg. 20110314T113000
                    prototype.setDescription( event.getDescription().getValue() );
                    prototype.setTitle( event.getSummary().getValue() );
                    prototype.setTimeFrameType(TimeFrameTypeEnum.EVENT);
                    prototype.setPriority(PriorityEnum.MEDIUM); //TODO:priority event.getPriority();
                    prototype.setMeetingRequest(null);
                    prototype.setParent(null);

                    // repeating
                    RRule recur = (RRule)event.getProperty("RRULE");
                    if(recur!=null){
                        // repeatable
                        RepeatType repeatType = new RepeatTypeEntity();
                        // bind
                        prototype.setPrototype( true );
                        repeatType.setPrototype(prototype);                        
                        // set
                        repeatType.setRequest( null );
                        repeatType.setFrequency(recur.getRecur().getInterval()); // interval
                        repeatType.setCount( recur.getRecur().getCount() );
                        repeatType.setRepeatType( iCal2ssmeRepeatMap.get(recur.getRecur().getFrequency()) );
                        if(RepeatTypeEnum.WEEKLY.equals(repeatType.getRepeatType())){
                            repeatType.setFlags( iCal2ssmeFlags(recur.getRecur().getDayList()) );
                        } else {
                            repeatType.setFlags( 0 );
                        }
                        repeatType.setFrom( prototype.getFrom() );
                        Date until = recur.getRecur().getUntil();
                        if(until==null){
                            until = new Date( prototype.getFrom().getTime()+(TimeUtil._1week*52*3) );    // from + ~3 years
                        }
                        repeatType.setUntil( until );
                        repeatType.setTimeFrames( repeatType.getRepeatType().getDates(repeatType) );
                        result.add( repeatType );
                    } else {
                        // plain
                        prototype.setPrototype( false );
                        prototype.setRepeatType( null );
                    }
                    result.add( prototype );
                }
            }
            
            return result;
        }

        private int iCal2ssmeFlags(WeekDayList days){
            int result = 0;
            for(Object d : days){
                result += iCal2ssmeFlagsMap.get(((WeekDay)d).getDay());
            }
            return result;
        }
        private Map<String, Integer> iCal2ssmeFlagsMap = new HashMap<String, Integer>(){{
            put(WeekDay.MO.getDay(), RepeatType.MONDAY_FLAG );
            put(WeekDay.TU.getDay(), RepeatType.TUESDAY_FLAG);
            put(WeekDay.WE.getDay(), RepeatType.WEDNESDAY_FLAG);
            put(WeekDay.TH.getDay(), RepeatType.THURSDAY_FLAG);
            put(WeekDay.FR.getDay(), RepeatType.FRIDAY_FLAG);
            put(WeekDay.SA.getDay(), RepeatType.SATURDAY_FLAG);
            put(WeekDay.SU.getDay(), RepeatType.SUNDAY_FLAG);
        }};
        private Map<String, RepeatTypeEnum> iCal2ssmeRepeatMap = new HashMap<String, RepeatTypeEnum>(){{
            put(Recur.DAILY, RepeatTypeEnum.DAILY);
            put(Recur.WEEKLY, RepeatTypeEnum.WEEKLY);
            put(Recur.MONTHLY, RepeatTypeEnum.MONTHLY);
            put(Recur.YEARLY, RepeatTypeEnum.YEARLY);
        }};


    };

    private final String type_;
    ImportParsersEnum(String type){
    	type_ = type;
    }
    public String getType(){
    	return type_;
    }

    public abstract List<BasicData> process(String fileContent, User user) throws IOException, ParserException, ParseException;

    private static final Map<String,ImportParsersEnum> reverse_ = new HashMap<String,ImportParsersEnum>();
    static {
        for(ImportParsersEnum rte : EnumSet.allOf(ImportParsersEnum.class))
        	reverse_.put(rte.getType(), rte);
    }

    public static boolean containsType(String type){
    	return reverse_.containsKey(type);
    }
    public static ImportParsersEnum getType(String type){
    	return reverse_.get(type);
    }

}
