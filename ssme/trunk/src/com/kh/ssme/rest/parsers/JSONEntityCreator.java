/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest.parsers;

import com.kh.ssme.model.ifc.*;
import com.kh.ssme.rest.util.TimeUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * @author Michal Szopinski 
 * Implementation of interface AbstractEntityCreator with methods for creating JSON object for respond from entity object.
 */
public class JSONEntityCreator implements AbstractEntityCreator {

	private static final JSONEntityCreator instance_;
	static {
		instance_ = new JSONEntityCreator();
	}

	public synchronized static AbstractEntityCreator getInstance() {
		return instance_;
	}

	private JSONEntityCreator() {
		/* empty block */
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityCreator#createBasicData(com.kh.ssme.model.ifc.BasicData, org.json.jdk5.simple.JSONObject)
	 */
	@Override
	public void create(BasicData basicData, Object jsonResult, int recursionLevel) throws JSONException {
		// protected Long id_;
		// protected Date createDate_;
		// protected Date modifiedDate_;
		// protected Boolean deleted_;
		// protected String uuid_;
        JSONObject json = (JSONObject)jsonResult;

		if (basicData.getCreateDate() != null) {
			json.put(BasicData.FIELD_CREATE_DATE, TimeUtil.format(basicData.getCreateDate()));
		}

		if (basicData.getModifiedDate() != null) {
			json.put(BasicData.FIELD_MODIFIED_DATE, TimeUtil.format(basicData.getModifiedDate()));
		}

		if (basicData.getUUID() != null) {
			json.put(BasicData.FIELD_UUID, basicData.getUUID());
		}

	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityCreator#create(com.kh.ssme.model.ifc.Calendar, org.json.jdk5.simple.JSONObject)
	 */
	@Override
	public void create(Calendar calendar, Object jsonResult, int recursionLevel) throws JSONException {
		// private String name_ = "";
		// private Set<TimeFrame> timeFrames_ = new TreeSet<TimeFrame>();
		// private User user_;
        JSONObject json = (JSONObject)jsonResult;

		create((BasicData) calendar, json, recursionLevel);

		if (calendar.getName() != null) {
			json.put(Calendar.FIELD_NAME, calendar.getName());
		}

		if (calendar.getTimeFrames() != null) {
			JSONArray timeFrames = new JSONArray();
            for (TimeFrame tf : calendar.getTimeFrames()) {
                if( !tf.isDeleted() && !tf.isPrototype() ){
                    JSONObject timeFrame = new JSONObject();
                    if (recursionLevel > 0) {
                        // store full object
                        create(tf, timeFrame, recursionLevel - 1);
                    } else {
                        // store simplified data
                        timeFrame.put(BasicData.FIELD_UUID, tf.getUUID());
                        timeFrame.put(BasicData.FIELD_READABLE_NAME, tf.getTitle());
                    }
                    timeFrames.put(timeFrame);
                }
            }
			json.put(Calendar.FIELD_TIME_FRAMES, timeFrames);
		}

		if (calendar.getUser() != null && !calendar.getUser().isDeleted()) {
			JSONObject user = new JSONObject();			
			if (recursionLevel > 0) {
				// store full object
				create(calendar.getUser(), user, recursionLevel - 1);
			} else {
				// store simplified data
				user.put(BasicData.FIELD_UUID, calendar.getUser().getUUID());
				String tempName = calendar.getUser().getName();
				tempName += " " + calendar.getUser().getSurname();
				tempName += " (" + calendar.getUser().getLogin() + ")";
				user.put(BasicData.FIELD_READABLE_NAME, tempName);
			}
			json.put(Calendar.FIELD_USER, user);			
		}

	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityCreator#create(com.kh.ssme.model.ifc.Eventing, org.json.jdk5.simple.JSONObject)
	 */
	@Override
	public void create(Eventing eventing, Object jsonResult, int recursionLevel) throws JSONException {
        //private ParticipationTypeEnum participantType_;
        //private MeetingRequest request_;
        //private User user_;
        //private List<EventingPart> parts_;
        //private StateEnum state_;
        JSONObject json = (JSONObject)jsonResult;

		create((BasicData) eventing, json, recursionLevel);        

		if (eventing.getParticipantType() != null) {
			json.put(Eventing.FIELD_PARTICIPANT_TYPE, eventing.getParticipantType().getName());
		}
		
		if (eventing.getRequest() != null && !eventing.getRequest().isDeleted()) {
			JSONObject request = new JSONObject();			
			if (recursionLevel > 0) {
				// store full object
				create(eventing.getRequest(), request, recursionLevel - 1);
			} else {
				// store simplified data
				request.put(BasicData.FIELD_READABLE_NAME, eventing.getRequest().getTitle());
				request.put(BasicData.FIELD_UUID, eventing.getRequest().getUUID());
			}
			json.put(Eventing.FIELD_REQUEST, request);			
		}

		if (eventing.getUser() != null && !eventing.getUser().isDeleted()) {
			JSONObject user = new JSONObject();
			if (recursionLevel > 0) {
				// store full object
				create(eventing.getUser(), user, recursionLevel - 1);
			} else {
				// store simplified data
				user.put(BasicData.FIELD_READABLE_NAME, eventing.getUser().getName());
				user.put(BasicData.FIELD_UUID, eventing.getUser().getUUID());
			}
			json.put(Eventing.FIELD_USER, user);			
		}

		if (eventing.getParts() != null) {
			JSONArray parts = new JSONArray();
            for (EventingPart ep : eventing.getParts()) {
                if ( !ep.isDeleted() ){
                    JSONObject tf = new JSONObject();
                    if (recursionLevel > 0) {
                        // store full object
                        create(ep, tf, recursionLevel - 1);
                    } else {
                        // store simplified data
                        tf.put(BasicData.FIELD_UUID, ep.getUUID());
                        tf.put(BasicData.FIELD_READABLE_NAME, EventingPart.class.getName() + ep.getUUID());
                    }
                    parts.put(tf);
                }
            }
			json.put(Eventing.FIELD_PARTS, parts);
		}
		
		if (eventing.getState() != null) {
			json.put(Eventing.FIELD_STATE, eventing.getState().getName());
		}

	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityCreator#create(com.kh.ssme.model.ifc.EventingPart, org.json.jdk5.simple.JSONObject)
	 */
	@Override
	public void create(EventingPart eventingPart, Object jsonResult, int recursionLevel) throws JSONException {
        //private StateEnum state_;
        //private Eventing eventing_;
        //private Date day_;
        //private String map_;    
        JSONObject json = (JSONObject)jsonResult;

		create((BasicData) eventingPart, json, recursionLevel);

		if (eventingPart.getState() != null) {
			json.put(EventingPart.FIELD_STATE, eventingPart.getState().getName());
		}

		if (eventingPart.getEventing() != null && !eventingPart.getEventing().isDeleted()) {
			JSONObject eventing = new JSONObject();
			if (recursionLevel > 0) {
				// store full object
				create(eventingPart.getEventing(), eventing, recursionLevel - 1);
			} else {
				// store simplified data
				eventing.put(BasicData.FIELD_UUID, eventingPart.getEventing().getUUID());
				eventing.put(BasicData.FIELD_READABLE_NAME, Eventing.class.getName() +"#"+ eventingPart.getEventing().getUUID());
			}
			json.put(EventingPart.FIELD_EVENTING, eventing);
		}        

		if (eventingPart.getDay() != null) {
			json.put(EventingPart.FIELD_DAY, TimeUtil.format(eventingPart.getDay()));
		}

		if (eventingPart.getMap() != null) {
			json.put(EventingPart.FIELD_MAP, eventingPart.getMap());
		}
        
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityCreator#create(com.kh.ssme.model.ifc.Group, org.json.jdk5.simple.JSONObject)
	 */
	@Override
	public void create(Group group, Object jsonResult, int recursionLevel) throws JSONException {
		// private String name_ = "";
		// private Set<User> users_ = new TreeSet<User>();
		// private User owner_ = new User();
        JSONObject json = (JSONObject)jsonResult;

		create((BasicData) group, json, recursionLevel);

		if (group.getName() != null) {
			json.put(Group.FIELD_NAME, group.getName());
		}

		if (group.getDescription() != null) {
			json.put(Group.FIELD_DESCRIPTION, group.getDescription());
		}
		
		if (group.getUsers() != null) {
			JSONArray users = new JSONArray();
            for (User u : group.getUsers()) {
                if( !u.isDeleted() ){
                    JSONObject user = new JSONObject();
                    if (recursionLevel > 0) {
                        // store full object
                        create(u, user, recursionLevel - 1);
                    } else {
                        // store simplified data
                        user.put(BasicData.FIELD_UUID, u.getUUID());
                        user.put(BasicData.FIELD_READABLE_NAME, u.getLogin());
                    }
                    users.put(user);
                }
            }
			json.put(Group.FIELD_USERS, users);
		}		

		if (group.getOwner() != null && !group.getOwner().isDeleted()) {
			JSONObject owner = new JSONObject();			
			if (recursionLevel > 0) {
				// store full object
				create(group.getOwner(), owner, recursionLevel - 1);
			} else {
				// store simplified data
				String tempName = group.getOwner().getName();
				tempName += " " + group.getOwner().getSurname();
				tempName += " (" + group.getOwner().getLogin() + ")";
				owner.put(BasicData.FIELD_READABLE_NAME, tempName);
				owner.put(BasicData.FIELD_UUID, group.getOwner().getUUID());
			}
			json.put(Group.FIELD_OWNER, owner);			
		}
		


	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityCreator#create(com.kh.ssme.model.ifc.MeetingRequest, org.json.jdk5.simple.JSONObject)
	 */
	@Override
	public void create(MeetingRequest meetingRequest, Object jsonResult, int recursionLevel) throws JSONException {
        //private String title;
        //private String description;
        //private Date notSoonerThen_;
        //private Date notLaterThen_;
        //private StateEnum state_;
        //private Integer duration_;
        //private List<TimeFrame> decidedTimes_;
        //private Eventing eventings_; private RepeatType repeatType_;  
        JSONObject json = (JSONObject)jsonResult;

		create((BasicData) meetingRequest, json, recursionLevel);

		if (meetingRequest.getNotSoonerThen() != null) {
			json.put(MeetingRequest.FIELD_NOT_SOONER_THEN, TimeUtil.format(meetingRequest.getNotSoonerThen()));
		}

		if (meetingRequest.getNotLaterThen() != null) {
			json.put(MeetingRequest.FIELD_NOT_LATER_THEN, TimeUtil.format(meetingRequest.getNotLaterThen()));
		}

		if (meetingRequest.getState() != null) {
			json.put(MeetingRequest.FIELD_STATE, meetingRequest.getState().getName());
		}

		if (meetingRequest.getTitle() != null) {
			json.put(MeetingRequest.FIELD_TITLE, meetingRequest.getTitle());
		}

		if (meetingRequest.getDescription() != null) {
			json.put(MeetingRequest.FIELD_DESCRIPTION, meetingRequest.getDescription());
		}

        if (meetingRequest.getDuration() != null) {
            json.put(MeetingRequest.FIELD_DURATION, meetingRequest.getDuration());
        }


		if (meetingRequest.getDecidedTimes() != null) {
			JSONArray decidedTimes = new JSONArray();
            for (TimeFrame tf : meetingRequest.getDecidedTimes()) {
                if( !tf.isDeleted() && !tf.isPrototype() ){
                    JSONObject decidedTime = new JSONObject();
                    if (recursionLevel > 0) {
                        // store full object
                        create(tf, decidedTime, recursionLevel - 1);
                    } else {
                        // store simplified data
                        decidedTime.put(BasicData.FIELD_UUID, tf.getUUID());
                        decidedTime.put(BasicData.FIELD_READABLE_NAME, tf.getTitle());
                    }
                    decidedTimes.put(decidedTime);
                }
            }
            json.put(MeetingRequest.FIELD_DECIDED_TIMES, decidedTimes);
		}

		// decidedPlace
		
		if (meetingRequest.getEventings() != null) {
			JSONArray eventings = new JSONArray();
            for (Eventing e : meetingRequest.getEventings()) {
                if( !e.isDeleted() ){
                    JSONObject eventing = new JSONObject();
                    if (recursionLevel > 0) {
                        // store full object
                        create(e, eventing, recursionLevel - 1);
                    } else {
                        // store simplified data
                        eventing.put(BasicData.FIELD_UUID, e.getUUID());
                        eventing.put(BasicData.FIELD_READABLE_NAME, Eventing.class.getName() + e.getUUID());
                    }
                    eventings.put(eventing);
                }
            }
            json.put(MeetingRequest.FIELD_EVENTINGS, eventings);
		}

		if (meetingRequest.getRepeatType() != null && !meetingRequest.getRepeatType().isDeleted()) {
			JSONObject repeatType = new JSONObject();
			if (recursionLevel > 0) {
				// store full object
				create(meetingRequest.getRepeatType(), repeatType, recursionLevel - 1);
			} else {
				// store simplified data
				repeatType.put(BasicData.FIELD_UUID, meetingRequest.getRepeatType().getUUID());
				repeatType.put(BasicData.FIELD_READABLE_NAME, "RepeatType:"+meetingRequest.getRepeatType().getUUID());
			}
			json.put(MeetingRequest.FIELD_REPEAT_TYPE, repeatType);
		}
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityCreator#create(com.kh.ssme.model.ifc.RepeatType, org.json.jdk5.simple.JSONObject)
	 */
	@Override
	public void create(RepeatType repeatType, Object jsonResult, int recursionLevel) throws JSONException {
        //private Integer count_;
        //private Integer frequency_;
        //private Date from_;
        //private Date until_;
        //private RepeatTypeEnum repeatType_;
        //private Integer flags_;
        //private List<TimeFrame> timeFrames_;
        //private TimeFrame prototype_;
        //private MeetingRequest request_;		
        JSONObject json = (JSONObject)jsonResult;

		create((BasicData) repeatType, json, recursionLevel);

		if (repeatType.getCount() != null) {
			json.put(RepeatType.FIELD_COUNT, repeatType.getCount());
		}
		if (repeatType.getFrequency() != null) {
			json.put(RepeatType.FIELD_FREQUENCY, repeatType.getFrequency());
		}
		if (repeatType.getFrom() != null) {
			json.put(RepeatType.FIELD_FROM, repeatType.getFrom());
		}
		if (repeatType.getUntil() != null) {
			json.put(RepeatType.FIELD_UNTIL, TimeUtil.format(repeatType.getUntil()));
		}
		if (repeatType.getRepeatType() != null) {
			json.put(RepeatType.FIELD_REPEAT_TYPE, repeatType.getRepeatType().getName());
		}

		if (repeatType.getFlags() != null) {
			json.put(RepeatType.FIELD_FLAGS, repeatType.getFlags());
		}        
				
		if (repeatType.getTimeFrames() != null) {
			JSONArray timeFrames = new JSONArray();
            for (TimeFrame tf : repeatType.getTimeFrames()) {
                if( !tf.isDeleted() && !tf.isPrototype() ){
                    JSONObject timeFrame = new JSONObject();
                    if(recursionLevel>0){
                        // store full object
                        create(tf, timeFrame, recursionLevel-1);
                    } else {
                        // store simplified data
                        timeFrame.put(BasicData.FIELD_UUID, tf.getUUID());
                        timeFrame.put(BasicData.FIELD_READABLE_NAME, tf.getTitle());
                    }
                    timeFrames.put( timeFrame );
                }
            }
			json.put(RepeatType.FIELD_TIME_FRAMES, timeFrames);
		}
		
		if (repeatType.getPrototype() != null && !repeatType.getPrototype().isDeleted()) {
			JSONObject prototype = new JSONObject();			
			if (recursionLevel > 0) {
				// store full object
				create(repeatType.getPrototype(), prototype, recursionLevel - 1);
			} else {
				// store simplified data
				prototype.put(BasicData.FIELD_READABLE_NAME, "Prototype:"+repeatType.getPrototype().getTitle());
				prototype.put(BasicData.FIELD_UUID, repeatType.getPrototype().getUUID());
			}
			json.put(RepeatType.FIELD_PROTOTYPE, prototype);			
		}

		if (repeatType.getRequest() != null && !repeatType.getRequest().isDeleted()) {
			JSONObject request = new JSONObject();
			if (recursionLevel > 0) {
				// store full object
				create(repeatType.getRequest(), request, recursionLevel - 1);
			} else {
				// store simplified data
				request.put(BasicData.FIELD_READABLE_NAME, repeatType.getRequest().getTitle());
				request.put(BasicData.FIELD_UUID, repeatType.getRequest().getUUID());
			}
			json.put(Eventing.FIELD_REQUEST, request);
		}        
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityCreator#create(com.kh.ssme.model.ifc.Role, org.json.JSONObject, int)
	 */
	@Override
	public void create(Role role, Object jsonResult, int recursionLevel) throws JSONException {
        JSONObject json = (JSONObject)jsonResult;

		create((BasicData) role, json, recursionLevel);        

		if (role.getUser() != null && !role.getUser().isDeleted()) {
			JSONObject user = new JSONObject();			
			if (recursionLevel > 0) {
				// store full object
				create(role.getUser(), user, recursionLevel - 1);
			} else {
				// store simplified data
				String tempName = role.getUser().getName();
				tempName += " " + role.getUser().getSurname();
				tempName += " (" + role.getUser().getLogin() + ")";
				user.put(BasicData.FIELD_READABLE_NAME, tempName);
				user.put(BasicData.FIELD_UUID, role.getUser().getUUID());
			}
			json.put(Role.FIELD_USER, user);			
		}
		
		if (role.getLogin() != null) {
			json.put(Role.FIELD_LOGIN, role.getLogin());
		}		
		
		if (role.getRole() != null) {
			json.put(Role.FIELD_ROLE, role.getRole().getRole());
		}		
		
	}

    @Override
    public void create(Tag tag, Object jsonResult, int recursionLevel) throws JSONException {
        JSONObject json = (JSONObject)jsonResult;

		create((BasicData) tag, json, recursionLevel);

		if (tag.getName() != null) {
			json.put(Tag.FIELD_NAME, tag.getName());
		}

		if (tag.getDescription() != null) {
			json.put(Tag.FIELD_DESCRIPTION, tag.getDescription());
		}

		if (tag.getUrl() != null) {
			json.put(Tag.FIELD_URL, tag.getUrl());
		}

		if (tag.getTimeFrames() != null) {
			JSONArray timeFrames = new JSONArray();
            for (TimeFrame tf : tag.getTimeFrames()) {
                if( !tf.isDeleted() && !tf.isPrototype() ){
                    JSONObject timeFrame = new JSONObject();
                    if (recursionLevel > 0) {
                        // store full object
                        create(tf, timeFrame, recursionLevel - 1);
                    } else {
                        // store simplified data
                        timeFrame.put(BasicData.FIELD_UUID, tf.getUUID());
                        timeFrame.put(BasicData.FIELD_READABLE_NAME, tf.getTitle());
                    }
                    timeFrames.put(timeFrame);
                }
            }
			json.put(Calendar.FIELD_TIME_FRAMES, timeFrames);
		}        

    }

    /* (non-Javadoc)
      * @see com.kh.ssme.rest.parsers.AbstractEntityCreator#create(com.kh.ssme.model.ifc.TimeFrame, org.json.jdk5.simple.JSONObject)
      */
	@Override
	public void create(TimeFrame timeFrame, Object jsonResult, int recursionLevel) throws JSONException {
        //private Date from_;
        //private Date to_;
        //private RepeatType repeat_;
        //private String title_;
        //private String description_;
        //private Boolean prototype_;
        //private TimeFrameTypeEnum type_;
        //private PriorityEnum priority_;
        //private Calendar calendar_;
        //private MeetingRequest meetingRequest_;
        //private RepeatTypeEntity parent_;
        //private List<Tag> tags_;

        JSONObject json = (JSONObject)jsonResult;

		create((BasicData) timeFrame, json, recursionLevel);

		if (timeFrame.getFrom() != null) {
			json.put(TimeFrame.FIELD_FROM, TimeUtil.format(timeFrame.getFrom()));
		}

		if (timeFrame.getTo() != null) {
			json.put(TimeFrame.FIELD_TO, TimeUtil.format(timeFrame.getTo()));
		}

		if (timeFrame.getRepeatType() != null && !timeFrame.getRepeatType().isDeleted()) {
			JSONObject repeatType = new JSONObject();
			if (recursionLevel > 0) {
				// store full object
				create(timeFrame.getRepeatType(), repeatType, recursionLevel - 1);
			} else {
				// store simplified data
				repeatType.put(BasicData.FIELD_UUID, timeFrame.getRepeatType().getUUID());
				repeatType.put(BasicData.FIELD_READABLE_NAME, "RepeatType:"+timeFrame.getRepeatType().getUUID());
			}
			json.put(TimeFrame.FIELD_REPEAT, repeatType);
		}        

		if (timeFrame.getTitle() != null) {
			json.put(TimeFrame.FIELD_TITLE, timeFrame.getTitle());
		}        

		if (timeFrame.getDescription() != null) {
			json.put(TimeFrame.FIELD_DESCRIPTION, timeFrame.getDescription());
		}
						
		if (timeFrame.isPrototype() != null) {
			json.put(TimeFrame.FIELD_PROTOTYPE, timeFrame.isPrototype());
		}

		if (timeFrame.getTimeFrameType() != null) {
			json.put(TimeFrame.FIELD_TYPE, timeFrame.getTimeFrameType().getName());
		}

		if (timeFrame.getPriority() != null) {
			json.put(TimeFrame.FIELD_PRIORITY, timeFrame.getPriority().getName());
		}        
		
		if (timeFrame.getCalendar() != null && !timeFrame.getCalendar().isDeleted()) {
			JSONObject calendar = new JSONObject();			
			if (recursionLevel > 0) {
				// store full object
				create(timeFrame.getCalendar(), calendar, recursionLevel - 1);
			} else {
				// store simplified data				
				calendar.put(BasicData.FIELD_UUID, timeFrame.getCalendar().getUUID());
				calendar.put(BasicData.FIELD_READABLE_NAME, timeFrame.getCalendar().getName());
			}
			json.put(TimeFrame.FIELD_CALENDAR, calendar);			
		}

		if (timeFrame.getMeetingRequest() != null && !timeFrame.getMeetingRequest().isDeleted()) {
			JSONObject meetingRequest = new JSONObject();			
			if (recursionLevel > 0) {
				// store full object
				create(timeFrame.getMeetingRequest(), meetingRequest, recursionLevel - 1);
			} else {
				// store simplified data
				meetingRequest.put(BasicData.FIELD_UUID, timeFrame.getMeetingRequest().getUUID());
				meetingRequest.put(BasicData.FIELD_READABLE_NAME, timeFrame.getMeetingRequest().getTitle());
			}
			json.put(TimeFrame.FIELD_MEETING_REQUEST, meetingRequest);			
		}		
		
		if (timeFrame.getParent() != null && !timeFrame.getParent().isDeleted()) {
			JSONObject parent = new JSONObject();			
			if (recursionLevel > 0) {
				// store full object
				create(timeFrame.getParent(), parent, recursionLevel - 1);
			} else {
				// store simplified data
				parent.put(BasicData.FIELD_READABLE_NAME, "ParentRepeatType:"+timeFrame.getParent().getUUID());
				parent.put(BasicData.FIELD_UUID, timeFrame.getParent().getUUID());
			}
			json.put(TimeFrame.FIELD_PARENT, parent);			
		}			

		if (timeFrame.getTags() != null) {
			JSONArray tags = new JSONArray();
            for (Tag t : timeFrame.getTags()) {
                if( !t.isDeleted() ){
                    JSONObject tag = new JSONObject();
                    if (recursionLevel > 0) {
                        // store full object
                        create(t, tag, recursionLevel - 1);
                    } else {
                        // store simplified data
                        tag.put(BasicData.FIELD_UUID, t.getUUID());
                        tag.put(BasicData.FIELD_READABLE_NAME, t.getName());
                    }
                    tags.put(tag);
                }
            }
			json.put(TimeFrame.FIELD_TAGS, tags);
		}

	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityCreator#create(com.kh.ssme.model.ifc.User, org.json.jdk5.simple.JSONObject)
	 */
	@Override
	public void create(User user, Object jsonResult, int recursionLevel) throws JSONException {
		// private String login_ = "";
		// private String name_ = "";
		// private String surname_ = "";
		// private String mail_ = "";
		// private String mobile_ = "";
		// private Set<GroupEntity> groups = new TreeSet<GroupEntity>();
		// private Set<StayPlace> stayPlaces_ = new TreeSet<StayPlace>();
		// private Set<com.kh.ssme.model.ifc.Calendar> calendars_ = new
		// TreeSet<com.kh.ssme.model.ifc.Calendar>();
		// private Set<Group> ownedGroups_ = new TreeSet<Group>();
        JSONObject json = (JSONObject)jsonResult;

		create((BasicData) user, json, recursionLevel);

		if (user.getLogin() != null) {
			json.put(User.FIELD_LOGIN, user.getLogin());
		}

		if (user.getName() != null) {
			json.put(User.FIELD_NAME, user.getName());
		}

		if (user.getSurname() != null) {
			json.put(User.FIELD_SURNAME, user.getSurname());
		}

		if (user.getMail() != null) {
			json.put(User.FIELD_MAIL, user.getMail());
		}

		if (user.getMobile() != null) {
			json.put(User.FIELD_MOBILE, user.getMobile());
		}

		if (user.getGroups() != null) {
			JSONArray groups = new JSONArray();
            for (Group g : user.getGroups()) {
                if( !g.isDeleted() ){
                    JSONObject group = new JSONObject();
                    if (recursionLevel > 0) {
                        // store full object
                        create(g, group, recursionLevel - 1);
                    } else {
                        // store simplified data
                        group.put(BasicData.FIELD_UUID, g.getUUID());
                        group.put(BasicData.FIELD_READABLE_NAME, g.getName());
                    }
                    groups.put(group);
                }
            }
			json.put(User.FIELD_GROUPS, groups);
		}

		if (user.getOwnedGroups() != null) {
			JSONArray ownedGroups = new JSONArray();
            for (Group g : user.getOwnedGroups()) {
                if( !g.isDeleted() ){
                    JSONObject group = new JSONObject();
                    if (recursionLevel > 0) {
                        // store full object
                        create(g, group, recursionLevel - 1);
                    } else {
                        // store simplified data
                        group.put(BasicData.FIELD_UUID, g.getUUID());
                        group.put(BasicData.FIELD_READABLE_NAME, g.getName());
                    }
                    ownedGroups.put(group);
                }
            }
			json.put(User.FIELD_OWNED_GROUPS, ownedGroups);
		}

		if (user.getRoles() != null) {
			JSONArray roles = new JSONArray();
            for (Role r : user.getRoles()) {
                if( !r.isDeleted() ){
                    JSONObject role = new JSONObject();
                    if (recursionLevel > 0) {
                        // store full object
                        create(r, role, recursionLevel - 1);
                    } else {
                        // store simplified data
                        role.put(BasicData.FIELD_UUID, r.getUUID());
                        role.put(BasicData.FIELD_READABLE_NAME, r.getRole().getRole());
                    }
                    roles.put(role);
                }
            }
			json.put(User.FIELD_ROLES, roles);
		}

		if (user.getCalendars() != null) {
			JSONArray calendars = new JSONArray();
            for (Calendar c : user.getCalendars()) {
                if( !c.isDeleted() ){
                    JSONObject calendar = new JSONObject();
                    if (recursionLevel > 0) {
                        // store full object
                        create(c, calendar, recursionLevel - 1);
                    } else {
                        // store simplified data
                        calendar.put(BasicData.FIELD_UUID, c.getUUID());
                        calendar.put(BasicData.FIELD_READABLE_NAME, c.getName());
                    }
                    calendars.put(calendar);
                }
            }
			json.put(User.FIELD_CALENDARS, calendars);
		}

		if (user.getEventings() != null) {
			JSONArray eventings = new JSONArray();
            for (Eventing t : user.getEventings()) {
                if( !t.isDeleted() ){
                    JSONObject tf = new JSONObject();
                    if (recursionLevel > 0) {
                        // store full object
                        create(t, tf, recursionLevel - 1);
                    } else {
                        // store simplified data
                        tf.put(BasicData.FIELD_UUID, t.getUUID());
                        tf.put(BasicData.FIELD_READABLE_NAME, Eventing.class.getName() + t.getUUID());
                    }
                    eventings.put(tf);
                }
            }
			json.put(User.FIELD_EVENTINGS, eventings);
		}

	}

}
