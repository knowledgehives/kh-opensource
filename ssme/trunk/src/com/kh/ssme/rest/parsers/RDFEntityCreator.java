/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Michał Szopiński, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.rest.parsers;

import com.kh.ssme.model.ifc.*;

/**
 * Created by IntelliJ IDEA.
 * User: Max
 * Date: 12.10.11
 * Time: 20:24
 * To change this template use File | Settings | File Templates.
 */
public class RDFEntityCreator implements AbstractEntityCreator {


	private static final RDFEntityCreator instance_;
	static {
		instance_ = new RDFEntityCreator();
	}

	public synchronized static AbstractEntityCreator getInstance() {
		return instance_;
	}

	private RDFEntityCreator() {
		/* empty block */
	}

    @Override
    public void create(BasicData basicData, Object builder, int recursionLevel) throws Exception {
        StringBuilder result = (StringBuilder)builder;
        result.append("[").append(basicData.getClass().getSimpleName()).append(":").append(basicData.getUUID()).append("]");
    }

    @Override
    public void create(Calendar calendar, Object builder, int recursionLevel) throws Exception {
        StringBuilder result = (StringBuilder)builder;
        result.append("[").append(calendar.getClass().getSimpleName()).append(":").append(calendar.getUUID()).append("]");
    }

    @Override
    public void create(Eventing eventing, Object builder, int recursionLevel) throws Exception {
        StringBuilder result = (StringBuilder)builder;
        result.append("[").append(eventing.getClass().getSimpleName()).append(":").append(eventing.getUUID()).append("]");
    }

    @Override
    public void create(EventingPart eventingPart, Object builder, int recursionLevel) throws Exception {
        StringBuilder result = (StringBuilder)builder;
        result.append("[").append(eventingPart.getClass().getSimpleName()).append(":").append(eventingPart.getUUID()).append("]");
    }

    @Override
    public void create(Group group, Object builder, int recursionLevel) throws Exception {
        StringBuilder result = (StringBuilder)builder;
        result.append("[").append(group.getClass().getSimpleName()).append(":").append(group.getUUID()).append("]");
    }

    @Override
    public void create(MeetingRequest meetingRequest, Object builder, int recursionLevel) throws Exception {
        StringBuilder result = (StringBuilder)builder;
        result.append("[").append(meetingRequest.getClass().getSimpleName()).append(":").append(meetingRequest.getUUID()).append("]");
    }

    @Override
    public void create(RepeatType repeatType, Object builder, int recursionLevel) throws Exception {
        StringBuilder result = (StringBuilder)builder;
        result.append("[").append(repeatType.getClass().getSimpleName()).append(":").append(repeatType.getUUID()).append("]");
    }

    @Override
    public void create(Role role, Object builder, int recursionLevel) throws Exception {
        StringBuilder result = (StringBuilder)builder;
        result.append("[").append(role.getClass().getSimpleName()).append(":").append(role.getUUID()).append("]");
    }

    @Override
    public void create(Tag tag, Object builder, int recursionLevel) throws Exception {
        StringBuilder result = (StringBuilder)builder;
        result.append("[").append(tag.getClass().getSimpleName()).append(":").append(tag.getUUID()).append("]");
    }

    @Override
    public void create(TimeFrame timeFrame, Object builder, int recursionLevel) throws Exception {
        StringBuilder result = (StringBuilder)builder;
        result.append("[").append(timeFrame.getClass().getSimpleName()).append(":").append(timeFrame.getUUID()).append("]");
    }

    @Override
    public void create(User user, Object builder, int recursionLevel) throws Exception {
        StringBuilder result = (StringBuilder)builder;
        result.append("[").append(user.getClass().getSimpleName()).append(":").append(user.getUUID()).append("]");
    }

}
