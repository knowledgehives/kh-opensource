/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.rest.parsers;

import com.kh.ssme.model.enums.PriorityEnum;
import com.kh.ssme.model.enums.RepeatTypeEnum;
import com.kh.ssme.model.ifc.Calendar;
import com.kh.ssme.model.ifc.RepeatType;
import com.kh.ssme.model.ifc.TimeFrame;
import net.fortuna.ical4j.model.*;
import net.fortuna.ical4j.model.TimeZone;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.property.*;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: MadMax
 * Date: 2011-04-07
 * Time: 16:33:20
 * To change this template use File | Settings | File Templates.
 */
public enum ExportParsersEnum {

    I_CALENDAR("iCalendar"){
        @Override
        public String exportCalendar(Calendar calendar) {

            // calendar
            net.fortuna.ical4j.model.Calendar iCalendar = new net.fortuna.ical4j.model.Calendar();
            iCalendar.getProperties().add(new ProdId("-//SmartSchedule.me//ssme export 0.1//EN"));
            iCalendar.getProperties().add(Version.VERSION_2_0);
            iCalendar.getProperties().add(CalScale.GREGORIAN);

            // timezone
            TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
            TimeZone timezone = registry.getTimeZone("Europe/Warsaw");
            VTimeZone vtz = timezone.getVTimeZone();
            iCalendar.getComponents().add( vtz );
            
            //events
            Set<Long> repeats = new HashSet<Long>();
            RepeatType rt;
            TimeFrame prototype;
            VEvent event;
            for(TimeFrame tf : calendar.getTimeFrames()){

                rt = tf.getRepeatType();
                if( rt==null ){ // not repeatable = single || prototype
                    if( !tf.isPrototype() ){    // single
                        // create VEvent
                        event = new VEvent( new DateTime(tf.getFrom()),
                                            new DateTime(tf.getTo()),
                                            tf.getTitle());
                        // properties
                        PropertyList properties = new PropertyList();
                        properties.add( new Uid(tf.getUUID()) );                        
                        properties.add( new Created( new DateTime(tf.getCreateDate()) ) );
                        properties.add( new LastModified( new DateTime(tf.getModifiedDate()) ) );
                        properties.add( new Description(tf.getDescription()) );
                        properties.add( ssme2iCalPriority(tf.getPriority()) );

                        // bind
                        event.getProperties().addAll( properties );
                        iCalendar.getComponents().add( event );
                    }
                } else {
                    if(!repeats.contains(rt.getId())){ // not added, yet
                        repeats.add(rt.getId());
                        // get                        
                        prototype = rt.getPrototype();
                        // create VEvent
                        event = new VEvent( new DateTime(prototype.getFrom()),
                                            new DateTime(prototype.getTo()),
                                            prototype.getTitle());
                        // properties
                        PropertyList properties = new PropertyList();
                        properties.add( new Uid(prototype.getUUID()) );                        
                        properties.add( new Created( new DateTime(prototype.getCreateDate()) ) );
                        properties.add( new LastModified( new DateTime(prototype.getModifiedDate()) ) );                        
                        properties.add( new Description(prototype.getDescription()) );
                        properties.add( ssme2iCalPriority(prototype.getPriority()) );
                        properties.add( createRecur(rt) );
                        // bind
                        event.getProperties().addAll( properties );
                        iCalendar.getComponents().add( event );                        
                    }
                }
            }

            // result
            return iCalendar.toString().replaceAll("\r\n","\n");
        }

        RRule createRecur( RepeatType rt ){
            Recur result = new Recur( ssme2iCalFrequency(rt.getRepeatType()), rt.getCount() );
            result.setInterval( rt.getFrequency() );
            if(rt.getCount()==0){
                result.setUntil( new net.fortuna.ical4j.model.Date(rt.getUntil().getTime()) );            
            }
            if(RepeatTypeEnum.WEEKLY.equals(rt.getRepeatType())){
                result.getDayList().clear();
                if((rt.getFlags()&RepeatType.MONDAY_FLAG)>0)    result.getDayList().add(WeekDay.MO);
                if((rt.getFlags()&RepeatType.TUESDAY_FLAG)>0)   result.getDayList().add(WeekDay.TU);
                if((rt.getFlags()&RepeatType.WEDNESDAY_FLAG)>0) result.getDayList().add(WeekDay.WE);
                if((rt.getFlags()&RepeatType.THURSDAY_FLAG)>0)  result.getDayList().add(WeekDay.TH);
                if((rt.getFlags()&RepeatType.FRIDAY_FLAG)>0)    result.getDayList().add(WeekDay.FR);
                if((rt.getFlags()&RepeatType.SATURDAY_FLAG)>0)  result.getDayList().add(WeekDay.SA);
                if((rt.getFlags()&RepeatType.SUNDAY_FLAG)>0)    result.getDayList().add(WeekDay.SU);
            }
            return new RRule(result);
        }

        String ssme2iCalFrequency(RepeatTypeEnum repeat){
            return ssme2iCalFreqMap.get(repeat);
        }
        private Map<RepeatTypeEnum, String> ssme2iCalFreqMap = new HashMap<RepeatTypeEnum, String>(){{
            put(RepeatTypeEnum.DAILY,   Recur.DAILY);
            put(RepeatTypeEnum.WEEKLY,  Recur.WEEKLY);
            put(RepeatTypeEnum.MONTHLY, Recur.MONTHLY);
            put(RepeatTypeEnum.YEARLY,  Recur.YEARLY);
        }};

        Priority ssme2iCalPriority(PriorityEnum priority){
            Priority result = ssme2iCalPriorityMap.get(priority);
            return (result!=null) ? result : Priority.UNDEFINED;
        }
        private Map<PriorityEnum, Priority> ssme2iCalPriorityMap = new HashMap<PriorityEnum, Priority>(){{
            put(PriorityEnum.HIGH,   Priority.HIGH);
            put(PriorityEnum.MEDIUM, Priority.MEDIUM);
            put(PriorityEnum.LOW,    Priority.LOW);
        }};
    };

    private final String type_;
    ExportParsersEnum(String type){
    	type_ = type;
    }
    public String getType(){
    	return type_;
    }

    public abstract String exportCalendar(Calendar calendar);

    private static final Map<String,ExportParsersEnum> reverse_ = new HashMap<String,ExportParsersEnum>();
    static {
        for(ExportParsersEnum rte : EnumSet.allOf(ExportParsersEnum.class))
        	reverse_.put(rte.getType(), rte);
    }

    public static boolean containsType(String type){
    	return reverse_.containsKey(type);
    }
    public static ExportParsersEnum getType(String type){
    	return reverse_.get(type);
    }
}
