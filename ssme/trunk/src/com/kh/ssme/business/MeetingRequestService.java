package com.kh.ssme.business;

import com.kh.ssme.manage.CalendarManager;
import com.kh.ssme.manage.MeetingRequestManager;
import com.kh.ssme.manage.PersistenceManager;
import com.kh.ssme.manage.UserManager;
import com.kh.ssme.model.entity.MeetingRequestEntity;
import com.kh.ssme.model.entity.TimeFrameEntity;
import com.kh.ssme.model.enums.ParticipationTypeEnum;
import com.kh.ssme.model.enums.PriorityEnum;
import com.kh.ssme.model.enums.StateEnum;
import com.kh.ssme.model.enums.TimeFrameTypeEnum;
import com.kh.ssme.model.ifc.*;
import com.kh.ssme.rest.parsers.EntityParserTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 */
public class MeetingRequestService {
	/*
	 * 
	 */
	private static final Logger logger_ = LoggerFactory.getLogger(MeetingRequestService.class);

	/**
	 * @param json
	 * @return
	 */
	public static MeetingRequest create(Object json) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			MeetingRequest request = new MeetingRequestEntity();
			EntityParserTools.getMerger(json).parseAndMerge(json, request);
			MeetingRequestManager.createMeetingRequest(request);
			PersistenceManager.getInstance().commitTransaction();
			return request;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @param json
	 * @return
	 */
	public static MeetingRequest update(String uuid, Object json) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			MeetingRequest request = MeetingRequestManager.findMeetingRequestByUUID(uuid);
			EntityParserTools.getMerger(json).parseAndMerge(json, request);
			PersistenceManager.getInstance().commitTransaction();
			return request;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @return
	 */
	public static MeetingRequest delete(String uuid) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			MeetingRequest request = MeetingRequestManager.findMeetingRequestByUUID(uuid);
			request.setDeleted(Boolean.TRUE);
		    request.setRemovedDate( java.util.Calendar.getInstance().getTime() );
			PersistenceManager.getInstance().commitTransaction();
			return request;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @return
	 */
	public static MeetingRequest get(String uuid) {
		try {
			return MeetingRequestManager.findMeetingRequestByUUID(uuid);
		} catch (Exception e) {
			logger_.error("", e);
		}
		return null;
	}

    /**
     * Get all meeting requests for given user
     * @param userUUID
     * @return
     */
    public static List getAll(String userUUID) {
		try {
            return MeetingRequestManager.findMeetingRequestForUser(userUUID); 
		} catch (Exception e) {
			logger_.error("", e);
		}
		return null;
    }

    /**
     * Get all meeting requests created by given user
     * @param userUUID
     * @return
     */
    public static List getCreated(String userUUID) {
		try {
            return MeetingRequestManager.findMeetingRequestCreatedByUser(userUUID);
		} catch (Exception e) {
			logger_.error("", e);
		}
		return null;
    }

    /**
     * Get all active meeting requests created by given user
     * @param userUUID
     * @return
     */
    public static List getActive(String userUUID) {
		try {
            return MeetingRequestManager.findActiveMeetingRequestCreatedByUser(userUUID);
		} catch (Exception e) {
			logger_.error("", e);
		}
		return null;
    }

    public static MeetingRequest deleteWithChildren(String uuid) {
		PersistenceManager.getInstance().beginTransaction();
		try{
			MeetingRequest request = MeetingRequestManager.findMeetingRequestByUUID(uuid);
            if(request.getRepeatType()!=null){
                request.getRepeatType().setDeleted( Boolean.TRUE );
            }
            for(Eventing e : request.getEventings()){
                for(EventingPart ep : e.getParts()){
                    e.setDeleted( Boolean.TRUE );                    
                }
                e.setParts( new ArrayList<EventingPart>() );
                e.setDeleted( Boolean.TRUE );
            }
            request.setEventings( new ArrayList<Eventing>() );
            request.setDeleted( Boolean.TRUE );            
			PersistenceManager.getInstance().commitTransaction();
			return request;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
    }

    public static MeetingRequest updateToEvent(String uuid, long from, long to) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			MeetingRequest request = MeetingRequestManager.findMeetingRequestByUUID(uuid);
            String title = request.getTitle();
            String description = request.getDescription();
            description += "\ngenerated from MeetingRequest marked as ";
            boolean repeatable = (request.getRepeatType() != null);

            // create events
            Calendar c;
            TimeFrame prototype;
            RepeatType repeatType;            
            for(Eventing e : request.getEventings()){
                // for each user who accepted or tentatived meeting
                System.out.println( "Eventing: "+e );
                if(StateEnum.ACCEPTED.equals(e.getState()) || StateEnum.TENTATIVE.equals(e.getState())){
                    // get calendar
                    c = CalendarManager.findCalendarByUserAndName( Calendar.DEFAULT_CALENDAR, e.getUser().getUUID() );
System.out.println( "\t\tCalendar: "+c );
System.out.println( "\t\tUser: "+e.getUser() );
                    // create event prototype
                    PriorityEnum pe = (ParticipationTypeEnum.CREATOR.equals(e.getParticipantType()) || ParticipationTypeEnum.PARTICIPANT.equals(e.getParticipantType()))
                            ? PriorityEnum.HIGH : PriorityEnum.MEDIUM;
                    prototype = new TimeFrameEntity(title, description+e.getState().getName()+" on "+e.getModifiedDate(),
                                                    new Date(from), new Date(to),
                                                    TimeFrameTypeEnum.REQUEST,
                                                    pe
                                                    );
                    // bind calendar
                    prototype.setCalendar( c );
                    c.getTimeFrames().add( prototype );
System.out.println( "\t\tPrototype: "+prototype );                    
                    if( repeatable ){
                        // if this is repeatable event then use this instance of timeframe as a prototype for repeattype
                        prototype.setPrototype( Boolean.TRUE );
                        // create repeattype instance
                        repeatType = request.getRepeatType().cloneMe();
                        // clear request                        
                        repeatType.setRequest( null );
                        // bind repeattype
                        repeatType.setPrototype( prototype );
                        prototype.setRepeatType( repeatType );
                        // create timeframes
System.out.println( "\t\tPrototype4repeatable: "+prototype );
			            repeatType.setTimeFrames( repeatType.getRepeatType().getDates( repeatType ) );
System.out.println( "\t\tPERSIST repeatType: "+repeatType );
                        PersistenceManager.getInstance().persistEntity( repeatType );
                    }
                    // bind request
                    prototype.setMeetingRequest( request );
                    request.getDecidedTimes().add( prototype );
System.out.println( "\t\tPERSIST prototype: "+prototype );
                    PersistenceManager.getInstance().persistEntity( prototype );                    
                }

                // clear eventings and it's parts - not necessary anymore                
                for(EventingPart ep : e.getParts()){
                    e.setDeleted( Boolean.TRUE );
                }
                e.setParts( new ArrayList<EventingPart>() );
                e.setDeleted( Boolean.TRUE );
            }
            request.setEventings( new ArrayList<Eventing>() );
            request.setState( StateEnum.ACCEPTED ); 

			PersistenceManager.getInstance().commitTransaction();
            //PersistenceManager.getInstance().rollbackTransaction();
			return request;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;                
    }

}
