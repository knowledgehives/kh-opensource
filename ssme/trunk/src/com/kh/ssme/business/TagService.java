/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan 
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.business;

import com.kh.ssme.manage.PersistenceManager;
import com.kh.ssme.manage.TagManager;
import com.kh.ssme.model.entity.TagEntity;
import com.kh.ssme.model.ifc.Tag;
import com.kh.ssme.rest.parsers.EntityParserTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: MadMax
 * Date: 2011-04-14
 * Time: 19:25:58
 * To change this template use File | Settings | File Templates.
 */
public class TagService {
    
    /*
     * 
     */
    private static final Logger logger_ = LoggerFactory.getLogger(CalendarService.class);
	
    /**
     * @param json
     * @return
     */
    public static Tag create(Object json){
        PersistenceManager.getInstance().beginTransaction();
        try{
            Tag tag = new TagEntity();
            EntityParserTools.getMerger(json).parseAndMerge(json, tag);
            TagManager.createTag(tag);
            PersistenceManager.getInstance().commitTransaction();
            return tag;
        } catch (Exception e){
            PersistenceManager.getInstance().rollbackTransaction();
            logger_.error("",e);
        }
        return null;
    }
	
    /**
     * @param uuid
     * @param json
     * @return
     */
    public static Tag update(String uuid, Object json){
        PersistenceManager.getInstance().beginTransaction();
        try{
            Tag tag = TagManager.findTagByUUID(uuid);
            EntityParserTools.getMerger(json).parseAndMerge(json, tag);
            PersistenceManager.getInstance().commitTransaction();
            return tag;
        } catch (Exception e){
            PersistenceManager.getInstance().rollbackTransaction();
            logger_.error("",e);
        }
        return null;
    }
	
    /**
     * @param uuid
     * @return
     */
    public static Tag delete(String uuid){
        PersistenceManager.getInstance().beginTransaction();
        try{
            Tag tag = TagManager.findTagByUUID(uuid);
            tag.setDeleted(Boolean.TRUE);
            tag.setRemovedDate( java.util.Calendar.getInstance().getTime() );         
            PersistenceManager.getInstance().commitTransaction();
            return tag;
        } catch (Exception e){
            PersistenceManager.getInstance().rollbackTransaction();
            logger_.error("",e);
        }
        return null;
    }
	
    /**
     * @param uuid
     * @return
     */
    public static Tag get(String uuid){
        try{
            return TagManager.findTagByUUID(uuid);
        } catch (Exception e){
            logger_.error("",e);
        }
        return null;
    }

    public static List getTagsLike(String likeClause){
		try {
            return TagManager.findTagLike(likeClause);
		} catch (Exception e) {
			logger_.error("", e);
		}
		return null;
    }

    
    
}
