package com.kh.ssme.business;

import com.kh.ssme.manage.CalendarManager;
import com.kh.ssme.manage.PersistenceManager;
import com.kh.ssme.manage.TimeFrameManager;
import com.kh.ssme.manage.UserManager;
import com.kh.ssme.model.entity.CalendarEntity;
import com.kh.ssme.model.ifc.Calendar;
import com.kh.ssme.model.ifc.TimeFrame;
import com.kh.ssme.rest.parsers.EntityParserTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


/**
 * @author Michal Szopinski
 *
 */
public class CalendarService {
	
	/*
	 * 
	 */
	private static final Logger logger_ = LoggerFactory.getLogger(CalendarService.class);
	
	/**
	 * @param json
	 * @return
	 */
	public static Calendar create(Object json){
		PersistenceManager.getInstance().beginTransaction();
		try{
			Calendar calendar = new CalendarEntity();
			EntityParserTools.getMerger(json).parseAndMerge(json, calendar);			
			CalendarManager.createCalendar(calendar);
			UserService.get( calendar.getUser().getUUID() ).getCalendars().add( calendar );
			PersistenceManager.getInstance().commitTransaction();
			return calendar;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	/**
	 * @param uuid
	 * @param json
	 * @return
	 */
	public static Calendar update(String uuid, Object json){
		PersistenceManager.getInstance().beginTransaction();
		try{
			Calendar calendar = CalendarManager.findCalendarByUUID(uuid);
			EntityParserTools.getMerger(json).parseAndMerge(json, calendar);
			PersistenceManager.getInstance().commitTransaction();
			return calendar;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	/**
	 * @param uuid
	 * @return
	 */
	public static Calendar delete(String uuid){
		PersistenceManager.getInstance().beginTransaction();
		try{
			Calendar calendar = CalendarManager.findCalendarByUUID(uuid);

            // move timeframes to 'default' calendar
            Calendar defaultCalendar = CalendarManager.findCalendarByUserAndName( Calendar.DEFAULT_CALENDAR, calendar.getUser().getUUID() );
            for(TimeFrame tf : calendar.getTimeFrames()){
                tf.setCalendar( defaultCalendar );
                defaultCalendar.getTimeFrames().add( tf );
            }
            calendar.setTimeFrames( new ArrayList<TimeFrame>() );

			calendar.setDeleted(Boolean.TRUE);
		    calendar.setRemovedDate( java.util.Calendar.getInstance().getTime() );         
			PersistenceManager.getInstance().commitTransaction();
			return calendar;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	/**
	 * @param uuid
	 * @return
	 */
	public static Calendar get(String uuid){
		try{
			return CalendarManager.findCalendarByUUID(uuid);
		} catch (Exception e){
			logger_.error("",e);
		}
		return null;
	}

    /**
     * @param calendarUuid
     * @param from
     * @param to
     * @return
     */
	public static Calendar getRange(String calendarUuid, long from, long to) {
		Calendar origin = get( calendarUuid );
		Calendar clone = new CalendarEntity();
		clone.setName( origin.getName() );
		clone.setDeleted( origin.isDeleted() );
		clone.setUser( origin.getUser() );
		//clone.setEventings( origin.getEventings() );
		
		List<TimeFrame> timeframes = TimeFrameManager.findEventsInRange(calendarUuid, from, to);
		for(TimeFrame tf : timeframes){
			clone.getTimeFrames().add( tf );
		}
		return clone;
	}

    /**
     * @param userUuid
     * @param from
     * @param to
     * @return
     */
	public static Calendar getAll(String userUuid, long from, long to) {
		Calendar dummy = new CalendarEntity();
		dummy.setName( "All" );
		dummy.setDeleted( false );
		dummy.setUser( UserManager.findUserByUUID(userUuid) );		
		//dummy.setEventings( origin.getEventings() );
		
		List<TimeFrame> timeframes = TimeFrameManager.findAllEvents(userUuid, from, to);
		for(TimeFrame tf : timeframes){
			dummy.getTimeFrames().add( tf );
		}			
		return dummy;
	}		

}
