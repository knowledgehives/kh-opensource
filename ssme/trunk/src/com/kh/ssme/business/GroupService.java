/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.business;

import com.kh.ssme.manage.GroupManager;
import com.kh.ssme.manage.PersistenceManager;
import com.kh.ssme.manage.UserManager;
import com.kh.ssme.model.entity.GroupEntity;
import com.kh.ssme.model.ifc.Group;
import com.kh.ssme.rest.parsers.EntityParserTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Michal Szopinski
 *
 */
public class GroupService {

	private static final Logger logger_ = LoggerFactory.getLogger(GroupService.class);
	
	public static Group create(Object json){
		PersistenceManager.getInstance().beginTransaction();
		try{
			Group group = new GroupEntity();
			EntityParserTools.getMerger(json).parseAndMerge(json, group);			
			GroupManager.createGroup(group);
			UserManager.addToOwnedGroups(group.getOwner(), group);
			PersistenceManager.getInstance().commitTransaction();
			return group;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	public static Group update(String uuid, Object json){
		PersistenceManager.getInstance().beginTransaction();
		try{
			Group group = GroupManager.findGroupByUUID(uuid);
			EntityParserTools.getMerger(json).parseAndMerge(json, group);
			PersistenceManager.getInstance().commitTransaction();
			return group;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	public static Group delete(String uuid){
		PersistenceManager.getInstance().beginTransaction();
		try{
			Group group = GroupManager.findGroupByUUID(uuid);
			group.setDeleted(Boolean.TRUE);
		    group.setRemovedDate( java.util.Calendar.getInstance().getTime() );            
			PersistenceManager.getInstance().commitTransaction();
			return group;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	public static Group get(String uuid){
		try{
			return GroupManager.findGroupByUUID(uuid);
		} catch (Exception e){
			logger_.error("",e);
		}
		return null;
	}		
	
}
