/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.business;

import com.kh.ssme.manage.PersistenceManager;
import com.kh.ssme.model.ifc.BasicData;
import com.kh.ssme.model.ifc.Calendar;
import com.kh.ssme.model.ifc.User;
import com.kh.ssme.rest.parsers.ExportParsersEnum;
import net.fortuna.ical4j.data.ParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: MadMax
 * Date: 2011-04-07
 * Time: 16:31:50
 */
public class ExportService {

    private static final Logger logger_ = LoggerFactory.getLogger(ImportService.class);

    public static String process(ExportParsersEnum parser, Calendar calendar){
        // export
        return parser.exportCalendar( calendar );
    }

}
