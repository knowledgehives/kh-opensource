/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.business;

import com.kh.ssme.manage.EventingPartManager;
import com.kh.ssme.manage.PersistenceManager;
import com.kh.ssme.model.entity.EventingPartEntity;
import com.kh.ssme.model.ifc.EventingPart;
import com.kh.ssme.rest.parsers.EntityParserTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class EventingPartService {
	/*
	 *
	 */
	private static final Logger logger_ = LoggerFactory.getLogger(EventingPartService.class);

	/**
	 * @param json
	 * @return
	 */
	public static EventingPart create(Object json) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			EventingPart eventingPart = new EventingPartEntity();
			EntityParserTools.getMerger(json).parseAndMerge(json, eventingPart);
			EventingPartManager.createEventingPart(eventingPart);
			PersistenceManager.getInstance().commitTransaction();
			return eventingPart;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @param json
	 * @return
	 */
	public static EventingPart update(String uuid, Object json) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			EventingPart eventingPart = EventingPartManager.findEventingPartByUUID(uuid);
			EntityParserTools.getMerger(json).parseAndMerge(json, eventingPart);
			PersistenceManager.getInstance().commitTransaction();
			return eventingPart;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @return
	 */
	public static EventingPart delete(String uuid) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			EventingPart eventingPart = EventingPartManager.findEventingPartByUUID(uuid);
			eventingPart.setDeleted(Boolean.TRUE);
		    eventingPart.setRemovedDate( java.util.Calendar.getInstance().getTime() );
			PersistenceManager.getInstance().commitTransaction();
			return eventingPart;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @return
	 */
	public static EventingPart get(String uuid) {
		try {
			return EventingPartManager.findEventingPartByUUID(uuid);
		} catch (Exception e) {
			logger_.error("", e);
		}
		return null;
	}
}