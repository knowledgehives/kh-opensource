package com.kh.ssme.business;

import com.kh.ssme.manage.EventingManager;
import com.kh.ssme.manage.MeetingRequestManager;
import com.kh.ssme.manage.PersistenceManager;
import com.kh.ssme.model.entity.EventingEntity;
import com.kh.ssme.model.ifc.Eventing;
import com.kh.ssme.rest.parsers.EntityParserTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 
 */
public class EventingService {
	/*
	 * 
	 */
	private static final Logger logger_ = LoggerFactory.getLogger(EventingService.class);

	/**
	 * @param json
	 * @return
	 */
	public static Eventing create(Object json) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			Eventing eventing = new EventingEntity();
			EntityParserTools.getMerger(json).parseAndMerge(json, eventing);
			EventingManager.createEventing(eventing);
			PersistenceManager.getInstance().commitTransaction();
			return eventing;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @param json
	 * @return
	 */
	public static Eventing update(String uuid, Object json) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			Eventing eventing = EventingManager.findEventingByUUID(uuid);
			EntityParserTools.getMerger(json).parseAndMerge(json, eventing);
			PersistenceManager.getInstance().commitTransaction();
			return eventing;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @return
	 */
	public static Eventing delete(String uuid) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			Eventing eventing = EventingManager.findEventingByUUID(uuid);
			eventing.setDeleted(Boolean.TRUE);
		    eventing.setRemovedDate( java.util.Calendar.getInstance().getTime() );
			PersistenceManager.getInstance().commitTransaction();
			return eventing;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @return
	 */
	public static Eventing get(String uuid) {
		try {
			return EventingManager.findEventingByUUID(uuid);
		} catch (Exception e) {
			logger_.error("", e);
		}
		return null;
	}

    /**
     * Get all eventings for which user were requested to participate in.
     * (But not the one that he participate in as a creator)
     * @param userUUID
     * @return
     */
    public static List getAllParticipations(String userUUID) {
		try {
            return EventingManager.findUsersParticipatedEventings(userUUID);
		} catch (Exception e) {
			logger_.error("", e);
		}
		return null;
    }

    public static List getEventingsForRequest(String requestUUID){
		try {
            return EventingManager.findMeetingRequestEventings(requestUUID);
		} catch (Exception e) {
			logger_.error("", e);
		}
		return null;
    }    
        
}
