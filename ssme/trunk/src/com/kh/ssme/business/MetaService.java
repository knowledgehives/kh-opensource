/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.business;

import com.kh.ssme.rest.util.RequestHintsEnum;
import com.kh.ssme.rest.util.TimeUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: MadMax
 * Date: 2011-02-17
 * Time: 22:43:28
 * To change this template use File | Settings | File Templates.
 */
public class MetaService {

    public static JSONObject getRequestHints(String requestUUID){
        JSONObject resultJSON   = new JSONObject();
        JSONArray arrayJSON;
        RequestHintsEnum.Hint res;
        
        for( RequestHintsEnum hintType : RequestHintsEnum.values() ){
            try {
                res = hintType.getHint( requestUUID );
                if( res!= null ){
                    arrayJSON = new JSONArray();
                    for(RequestHintsEnum.HintPart part : res.parts){
                        arrayJSON.put( new JSONObject().put( "from", TimeUtil.format(part.from) ).put( "to", TimeUtil.format(part.to) ) );
                    }
                    resultJSON.put( hintType.type(), new JSONObject().put( "description", res.description ).put( "parts", arrayJSON) );
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return resultJSON;
    }

}
