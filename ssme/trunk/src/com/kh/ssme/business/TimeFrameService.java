package com.kh.ssme.business;

import com.kh.ssme.manage.PersistenceManager;
import com.kh.ssme.manage.RepeatTypeManager;
import com.kh.ssme.manage.TimeFrameManager;
import com.kh.ssme.model.entity.TimeFrameEntity;
import com.kh.ssme.model.ifc.RepeatType;
import com.kh.ssme.model.ifc.TimeFrame;
import com.kh.ssme.rest.parsers.EntityParserTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Michal Szopinski
 *
 */
public class TimeFrameService {
	
	/*
	 * 
	 */
	private static final Logger logger_ = LoggerFactory.getLogger(TimeFrameService.class);
	
	/**
	 * @param json
	 * @return
	 */
	public static TimeFrame create(Object json){
		PersistenceManager.getInstance().beginTransaction();
		try{
			TimeFrame timeFrame = new TimeFrameEntity();
			EntityParserTools.getMerger(json).parseAndMerge(json, timeFrame);			
			TimeFrameManager.createTimeFrame(timeFrame);
			PersistenceManager.getInstance().commitTransaction();
			return timeFrame;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	/**
	 * @param uuid
	 * @param json
	 * @return
	 */
	public static TimeFrame update(String uuid, Object json){
		PersistenceManager.getInstance().beginTransaction();
		try{
			TimeFrame timeFrame = TimeFrameManager.findTimeFrameByUUID(uuid);
			EntityParserTools.getMerger(json).parseAndMerge(json, timeFrame);
			PersistenceManager.getInstance().commitTransaction();
			return timeFrame;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	/**
	 * @param uuid
	 * @return
	 */
	public static TimeFrame delete(String uuid){
		PersistenceManager.getInstance().beginTransaction();
		try{
			TimeFrame timeFrame = TimeFrameManager.findTimeFrameByUUID(uuid);
			timeFrame.setDeleted(Boolean.TRUE);
		    timeFrame.setRemovedDate( java.util.Calendar.getInstance().getTime() );              
			PersistenceManager.getInstance().commitTransaction();
			return timeFrame;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	/**
	 * @param uuid
	 * @return
	 */
	public static TimeFrame get(String uuid){
		try{
			return TimeFrameManager.findTimeFrameByUUID(uuid);
		} catch (Exception e){
			logger_.error("",e);
		}
		return null;
	}	
	
	/**
	 * @param uuid
	 * @param json
	 * @return
	 */
	public static TimeFrame updateRemoveParent(String uuid, String parentUUID, Object json){
		PersistenceManager.getInstance().beginTransaction();
		try{
			// get
			TimeFrame timeFrame = TimeFrameManager.findTimeFrameByUUID(uuid);
			RepeatType repeatType = RepeatTypeManager.findRepeatTypeByUUID(parentUUID);
			// merge
			EntityParserTools.getMerger(json).parseAndMerge(json, timeFrame);
			// timeFrame - clear parent			
			timeFrame.setRepeatType( null );
			// repeatType - remove from children
			repeatType.getTimeFrames().remove( timeFrame );
			// commit
			PersistenceManager.getInstance().commitTransaction();
			return timeFrame;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}	

}
