/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.manage;

import com.kh.ssme.model.entity.CalendarEntity;
import com.kh.ssme.model.ifc.Calendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


/**
 * @author Michal Szopinski
 * Contains methods for manipulating CalendarEntity
 */
public class CalendarManager {
	
	private static final Logger logger_ = LoggerFactory.getLogger(CalendarManager.class);	
	
	/**
	 * Creates a Calendar
	 * @param calendar
	 */
	public static void createCalendar(Calendar calendar) {
		logger_.debug("Creating Calendar: " + calendar);		
		PersistenceManager.getInstance().persistEntity(calendar);
	}
	
	/**
	 * Deletes a Calendar
	 * @param calendar
	 */
	public static void deleteCalendar(Calendar calendar) {
		logger_.debug("Deleting Calendar: " + calendar);		
		calendar.setDeleted(Boolean.TRUE);
	}	
	
	/**
	 * Removes a Calendar
	 * @param calendar
	 */
	public static void removeCalendar(Calendar calendar) {
		logger_.debug("Removing Calendar: " + calendar);		
		PersistenceManager.getInstance().removeEntity(calendar);
	}
	
	/**
	 * Returns a Calendar with the specified id.
	 * @param id
	 * @return calendar entity
	 */
	public static Calendar findCalendar(Long id) {
		logger_.debug("Finding Calendar with id: " + id);				
		return PersistenceManager.getInstance().findEntity(CalendarEntity.class, id);
	}

	/**
	 * @param uuid
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Calendar findCalendarByUUID(String uuid) {
		String query = "SELECT c FROM Calendar c WHERE c.uuid_ = ?";
		List<Calendar> result = PersistenceManager.getInstance().performQuery(query, uuid);
		if(result!=null && !result.isEmpty()){
			return result.get(0);
		}
		return null;
	}


    public static Calendar findCalendarByUserAndName(String name, String userUUID) {
		String query = "SELECT c FROM Calendar c WHERE c.name_ = ? AND c.user_.uuid_ = ?";
        List<Calendar> result = PersistenceManager.getInstance().performQuery(query, name, userUUID);
		if(result!=null && !result.isEmpty()){
			return result.get(0);
		}
		return null;        
    }
}
