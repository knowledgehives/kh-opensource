/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Michał Szopiński, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.kh.ssme.manage;

import com.kh.ssme.model.entity.EventingPartEntity;
import com.kh.ssme.model.ifc.EventingPart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author Michal Szopinski
 * Contains methods for manipulating EventingPartEntity
 */
public class EventingPartManager {

	private static final Logger logger_ = LoggerFactory.getLogger(EventingPartManager.class);

	/**
	 * Creates an EventingPart
	 * @param eventingPart
	 */
	public static void createEventingPart(EventingPart eventingPart) {
		logger_.debug("Creating EventingPart: " + eventingPart);
		PersistenceManager.getInstance().persistEntity(eventingPart);
	}

	/**
	 * Deletes an EventingPart
	 * @param eventingPart
	 */
	public static void deleteEventingPart(EventingPart eventingPart) {
		logger_.debug("Deleting EventingPart: " + eventingPart);
		eventingPart.setDeleted(Boolean.TRUE);
	}

	/**
	 * Removes an EventingPart
	 * @param eventingPart
	 */
	public static void removeEventingPart(EventingPart eventingPart) {
		logger_.debug("Removing EventingPart: " + eventingPart);
		PersistenceManager.getInstance().removeEntity(eventingPart);
	}

	/**
	 * Returns an EventingPart with the specified id.
	 * @param id
	 * @return eventingPart entity
	 */
	public static EventingPart findEventingPart(Long id) {
		logger_.debug("Finding EventingPart with id: " + id);
		return PersistenceManager.getInstance().findEntity(EventingPartEntity.class, id);
	}

	/**
	 * Returns an EventingPart with the specified id.
	 * @param uuid
	 * @return eventingPart entity
	 */

	@SuppressWarnings("unchecked")
	public static EventingPart findEventingPartByUUID(String uuid) {
		String query = "SELECT e FROM EventingPart e WHERE e.uuid_ = ?";
		List<EventingPart> result = PersistenceManager.getInstance().performQuery(query, uuid);
		if (result != null && !result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}
}