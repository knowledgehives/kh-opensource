/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.manage;

import com.kh.ssme.model.entity.MeetingRequestEntity;
import com.kh.ssme.model.enums.ParticipationTypeEnum;
import com.kh.ssme.model.enums.StateEnum;
import com.kh.ssme.model.ifc.MeetingRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Michal Szopinski 
 * Contains methods for manipulating MeetingRequestEntity
 */
public class MeetingRequestManager {

	private static final Logger logger_ = LoggerFactory.getLogger(MeetingRequestManager.class);

	/**
	 * Creates a MeetingRequest
	 * @param meetingRequest
	 */
	public static void createMeetingRequest(MeetingRequest meetingRequest) {
		logger_.debug("Creating MeetingRequest: " + meetingRequest);
		PersistenceManager.getInstance().persistEntity(meetingRequest);
	}

	/**
	 * Deletes a MeetingRequest
	 * @param meetingRequest
	 */
	public static void deleteMeetingRequest(MeetingRequest meetingRequest) {
		logger_.debug("Deleting MeetingRequest: " + meetingRequest);
		meetingRequest.setDeleted(Boolean.TRUE);
	}

	/**
	 * Removes a MeetingRequest
	 * @param meetingRequest
	 */
	public static void removeMeetingRequest(MeetingRequest meetingRequest) {
		logger_.debug("Removing MeetingRequest: " + meetingRequest);
		PersistenceManager.getInstance().removeEntity(meetingRequest);
	}

	/**
	 * Returns a MeetingRequest with the specified id.
	 * @param id
	 * @return meetingRequest entity
	 */
	public static MeetingRequest findMeetingRequest(Long id) {
		logger_.debug("Finding MeetingRequest with id: " + id);
		return PersistenceManager.getInstance().findEntity(MeetingRequestEntity.class, id);
	}

	/**
	 * @param uuid
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static MeetingRequest findMeetingRequestByUUID(String uuid) {
		String query = "SELECT mr FROM MeetingRequest mr WHERE mr.uuid_ = ?";
		List<MeetingRequest> result = PersistenceManager.getInstance().performQuery(query, uuid);
		if (result != null && !result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}

    public static List<MeetingRequest> findMeetingRequestForUser(String userUUID) {

		String query = "SELECT mr FROM MeetingRequest mr, Eventing ev WHERE ev.user_.uuid_ = ? AND ev.request_.id_ = mr.id_ ";
		List<MeetingRequest> result = PersistenceManager.getInstance().performQuery(query, userUUID, ParticipationTypeEnum.CREATOR);
		if (result == null) return new ArrayList();
        return result;
        
    }

     public static List findMeetingRequestCreatedByUser(String userUUID) {

		String query = "SELECT mr " +
                        "FROM MeetingRequest mr, Eventing ev " +
                        "WHERE ev.user_.uuid_ = ? " +
                            "AND ev.request_.id_ = mr.id_ " +
                            "AND ev.participantType_ = ?";
		List<MeetingRequest> result = PersistenceManager.getInstance().performQuery(query, userUUID, ParticipationTypeEnum.CREATOR);
		if (result == null) return new ArrayList();
        return result;

    }

     public static List findActiveMeetingRequestCreatedByUser(String userUUID) {

		String query = "SELECT mr " +
                        "FROM MeetingRequest mr, Eventing ev " +
                        "WHERE ev.user_.uuid_ = ? " +
                            "AND ev.request_.id_ = mr.id_ " +
                            "AND ev.participantType_ = ? " + 
                            "AND mr.state_ <> ? ";
		List<MeetingRequest> result = PersistenceManager.getInstance().performQuery(query, userUUID, ParticipationTypeEnum.CREATOR, StateEnum.ACCEPTED);
		if (result == null) return new ArrayList();
        return result;

    }

}
