/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.manage;

import com.kh.ssme.model.entity.TimeFrameEntity;
import com.kh.ssme.model.ifc.TimeFrame;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Michal Szopinski
 * Contains methods for manipulating TimeFrameEntity
 */
public class TimeFrameManager {
	
	private static final Logger logger_ = LoggerFactory.getLogger(TimeFrameManager.class);	
	
	/**
	 * Creates a TimeFrame
	 * @param timeFrame
	 */
	public static void createTimeFrame(TimeFrame timeFrame) {
		logger_.debug("Creating TimeFrame: " + timeFrame);		
		PersistenceManager.getInstance().persistEntity(timeFrame);
	}
	
	/**
	 * Deletes a TimeFrame
	 * @param timeFrame
	 */
	public static void deleteTimeFrame(TimeFrame timeFrame) {
		logger_.debug("Deleting TimeFrame: " + timeFrame);		
		timeFrame.setDeleted(Boolean.TRUE);
	}	
	
	/**
	 * Removes a TimeFrame
	 * @param timeFrame
	 */
	public static void removeTimeFrame(TimeFrame timeFrame) {
		logger_.debug("Removing TimeFrame: " + timeFrame);		
		PersistenceManager.getInstance().removeEntity(timeFrame);
	}
	
	/**
	 * Returns a TimeFrame with the specified id.
	 * @param id
	 * @return timeFrame entity
	 */
	public static TimeFrame findTimeFrame(Long id) {
		logger_.debug("Finding TimeFrame with id: " + id);				
		return PersistenceManager.getInstance().findEntity(TimeFrameEntity.class, id);
	}

	/**
	 * @param uuid
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static TimeFrame findTimeFrameByUUID(String uuid) {
		String query = "SELECT t FROM TimeFrame t WHERE t.uuid_ = ?";
		List<TimeFrame> result = PersistenceManager.getInstance().performQuery(query, uuid);
		if(result!=null && !result.isEmpty()){
			return result.get(0);
		}
		return null;
	}	
	
	
	/**
	 * @param calendarUuid
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<TimeFrame> findEventsInRange(String calendarUuid, long from, long to) {
		String query = "SELECT t FROM TimeFrame t WHERE t.to_>? AND t.from_<? AND t.calendar_.uuid_ = ?";
				
		List<TimeFrame> result = PersistenceManager.getInstance().performQuery(query, (new Date(from)), (new Date(to)), calendarUuid);
		if(result!=null && !result.isEmpty()){
			return result;
		}
		return new ArrayList<TimeFrame>();
	}

	/**
	 * @param userUuid
	 * @param from
	 * @param to
	 * @return
	 */
	@SuppressWarnings("unchecked")	
	public static List<TimeFrame> findAllEvents(String userUuid, long from, long to) {
		if(from >=0 && to >=0){		
			String query = "SELECT t FROM TimeFrame t WHERE t.calendar_.user_.uuid_=? AND t.to_ >? AND t.from_<?";	
						
			List<TimeFrame> result = PersistenceManager.getInstance().performQuery(query, userUuid, (new Date(from)), (new Date(to)));
			if(result!=null && !result.isEmpty()){
				return result;
			}
		} else {
			String query = "SELECT t FROM TimeFrame t WHERE t.calendar_.user_.uuid_=?";	
						
			List<TimeFrame> result = PersistenceManager.getInstance().performQuery(query, userUuid);
			if(result!=null && !result.isEmpty()){
				return result;
			}			
		}
		return new ArrayList<TimeFrame>();		
	}			
}