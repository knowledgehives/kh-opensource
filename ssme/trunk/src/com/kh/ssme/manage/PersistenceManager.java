/*
 *	SmartSchedule.me - Semantic Calender
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.manage;

import com.kh.ssme.model.entity.BasicDataEntity;
import com.kh.ssme.model.ifc.BasicData;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.Calendar;
import java.util.List;

/**
 * @author Michal Szopinski
 * Wrapper for EntityManager class
 */
public class PersistenceManager {

	private static EntityManagerFactory entityManagerFactory_;

	// singleton
	private static final PersistenceManager instance_;
	static{
		instance_ = new PersistenceManager();
	}

	public synchronized static PersistenceManager getInstance() {
		return instance_;
	}		
	
	private PersistenceManager() {
		entityManagerFactory_ = Persistence.createEntityManagerFactory("ssmePU");
	}
	
	public synchronized void destroy() {
		if (entityManagerFactory_ != null) {
			entityManagerFactory_.close();
		}
	}		
	
	private static ThreadLocal<EntityManager> entityManager_ = new ThreadLocal<EntityManager>() {

		@Override
		public EntityManager get() {
			EntityManager entityManger = super.get();
			if (entityManger == null || !entityManger.isOpen()) {
				entityManger = entityManagerFactory_.createEntityManager();
				set(entityManger);
			}
			return entityManger;
		}

		@Override
		public void remove() {
			EntityManager entityManger = super.get();
			if (entityManger != null && entityManger.isOpen()) {
				entityManger.getTransaction().begin();
				entityManger.flush();
				entityManger.getTransaction().commit();
				entityManger.close();
				set(null);
			}
		}
	};
	
	public ThreadLocal<EntityManager> getEntityManager() {
		return entityManager_;
	}	
	
	public void flushManager() {
		entityManager_.get().getTransaction().begin();
		entityManager_.get().flush();
		entityManager_.get().getTransaction().commit();
	}
	
	public void beginTransaction() {
		entityManager_.get().getTransaction().begin();
	}	
	
	public void commitTransaction() {
		entityManager_.get().flush();		
		entityManager_.get().getTransaction().commit();
	}		
	
	public void rollbackTransaction() {
		entityManager_.get().getTransaction().rollback();
	}	
		
	public <T extends BasicDataEntity> T findEntity(Class<T> clazz, Long id) {
		T entity = entityManager_.get().find(clazz, id);
		return entity;
	}	
	
	public <T extends BasicDataEntity> T mergeEntity(T object) {
		T entity = entityManager_.get().merge(object);
		return entity;
	}	
	
	public void persistEntity(Object entity) {
		entityManager_.get().persist(entity);
	}

	public void removeEntity(Object entity) {
		//entityManager_.get().remove(entity);
		((BasicData)entity).setDeleted( Boolean.TRUE );
		((BasicData)entity).setRemovedDate( Calendar.getInstance().getTime() );
		persistEntity( entity );
	}	
	
	@SuppressWarnings("unchecked")
	public List performNativeQuery(String queryString, Object... parameters) {
		Query query = entityManager_.get().createNativeQuery(queryString);
		for (int i = 0; i < parameters.length; i++) {
			query.setParameter(i + 1, parameters[i]);
		}
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List performQuery(String queryString, Object... parameters) {
		Query query = entityManager_.get().createQuery(queryString);
		for (int i = 0; i < parameters.length; i++) {
			query.setParameter(i + 1, parameters[i]);
		}		
		return query.getResultList();
	}	

}
