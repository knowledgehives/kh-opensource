/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.manage;

import com.kh.ssme.model.entity.TagEntity;
import com.kh.ssme.model.ifc.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by IntelliJ IDEA.
 * User: MadMax
 * Date: 2011-04-14
 * Time: 19:14:12
 *
 * Contains methods for manipulating TagEntity
 */
public class TagManager {
    
	private static final Logger logger_ = LoggerFactory.getLogger(CalendarManager.class);
	
	/**
	 * Creates a Tag
	 * @param tag
	 */
	public static void createTag(Tag tag) {
		logger_.debug("Creating Tag: " + tag);		
		PersistenceManager.getInstance().persistEntity(tag);
	}
	
	/**
	 * Deletes a Tag
	 * @param tag
	 */
	public static void deleteTag(Tag tag) {
		logger_.debug("Deleting Tag: " + tag);		
		tag.setDeleted(Boolean.TRUE);
	}	
	
	/**
	 * Removes a Tag
	 * @param tag
	 */
	public static void removeTag(Tag tag) {
		logger_.debug("Removing Tag: " + tag);		
		PersistenceManager.getInstance().removeEntity(tag);
	}
	
	/**
	 * Returns a Tag with the specified id.
	 * @param id
	 * @return tag entity
	 */
	public static Tag findTag(Long id) {
		logger_.debug("Finding Tag with id: " + id);				
		return PersistenceManager.getInstance().findEntity(TagEntity.class, id);
	}

	/**
	 * @param uuid
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Tag findTagByUUID(String uuid) {
		String query = "SELECT t FROM Tag t WHERE t.uuid_ = ?";
		List<Tag> result = PersistenceManager.getInstance().performQuery(query, uuid);
		if(result!=null && !result.isEmpty()){
			return result.get(0);
		}
		return null;
	}

     public static List findTagLike(String likeClause) {

		String query = "SELECT t \n" +
                        "FROM Tag t\n" +
                        "WHERE t.name_ LIKE ? ESCAPE '\' ";
		List<Tag> result = PersistenceManager.getInstance().performQuery(query, "%"+likeClause+"%");
		if (result == null) return new ArrayList();
        return result;

    }
    
}
