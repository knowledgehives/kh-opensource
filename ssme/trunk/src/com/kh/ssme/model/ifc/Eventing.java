/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.ifc;

import com.kh.ssme.model.enums.ParticipationTypeEnum;
import com.kh.ssme.model.enums.StateEnum;

import java.util.List;

/**
 * Represents relationship between user's Calendar, MeetingRequest and possible TimeFrames and Locations
 * @author Michal Szopinski 
 */
public interface Eventing extends BasicData {
	//private ParticipationTypeEnum participantType_;
	//private MeetingRequest request_;
	//private User user_;
	//private List<EventingPart> parts_;
	//private StateEnum state_;
	
	public static final String FIELD_PARTICIPANT_TYPE = "participantType";
	public static final String FIELD_REQUEST = "request";
	public static final String FIELD_USER = "user";
	public static final String FIELD_PARTS = "parts";
	public static final String FIELD_STATE = "state";	

	public ParticipationTypeEnum getParticipantType();

	public void setParticipantType(ParticipationTypeEnum participantType);

	public MeetingRequest getRequest();

	public void setRequest(MeetingRequest meetingRequest);

	public User getUser();

	public void setUser(User user);

	public List<EventingPart> getParts();

	public void setParts(List<EventingPart> parts);

	public StateEnum getState();

	public void setState(StateEnum state);
}
