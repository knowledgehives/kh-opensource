/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.ifc;

import com.kh.ssme.model.enums.StateEnum;

import java.util.Date;
import java.util.List;

/**
 * Represents decision on performing a new meeting
 * @author Michal Szopinski 
 */
public interface MeetingRequest extends BasicData {
	//private String title;
	//private String description;
	//private Date notSoonerThen_;
	//private Date notLaterThen_;
	//private StateEnum state_;
	//private Integer duration_;        
	//private List<TimeFrame> decidedTimes_;
	//private Eventing eventings_; private RepeatType repeatType_;


	public static final String FIELD_TITLE = "title";
	public static final String FIELD_DESCRIPTION = "description";    
	public static final String FIELD_NOT_SOONER_THEN = "notSoonerThen";
	public static final String FIELD_NOT_LATER_THEN = "notLaterThen";
	public static final String FIELD_STATE = "state";
    public static final String FIELD_DURATION = "duration";
	public static final String FIELD_DECIDED_TIMES = "decidedTimes";
	public static final String FIELD_EVENTINGS = "eventings";
	public static final String FIELD_REPEAT_TYPE = "repeatType";
    
    public Date getNotSoonerThen();

	public void setNotSoonerThen(Date notSoonerThen);

	public Date getNotLaterThen();

	public StateEnum getState();

	public void setState(StateEnum state);

	public void setNotLaterThen(Date notLaterThen);

	public List<TimeFrame> getDecidedTimes();

	public void setDecidedTimes(List<TimeFrame> decidedTimes);

	public List<Eventing> getEventings();

	public void setEventings(List<Eventing> eventings);

	public String getTitle();

	public void setTitle(String title);

	public String getDescription();

	public void setDescription(String description);

	public RepeatType getRepeatType();

	public void setRepeatType(RepeatType repeatType);

	public Integer getDuration();

	public void setDuration(Integer duration);    

}
