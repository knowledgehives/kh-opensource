/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.ifc;

import java.util.List;

/**
 * Represents one of the user's Calendars
 * @author Michal Szopinski 
 */
public interface Calendar extends BasicData {
	//private String name_;	
	//private List<TimeFrame> timeFrames_;
	//private User user_;

    public static final String DEFAULT_CALENDAR = "Default";
	
	public static final String FIELD_NAME = "name";	
	public static final String FIELD_TIME_FRAMES = "timeFrames";
	public static final String FIELD_USER = "user";

	public String getName();
	
	public void setName(String name);
	
	public List<TimeFrame> getTimeFrames();
	
	public void setTimeFrames(List<TimeFrame> timeFrames);
	
	public User getUser();
	
	public void setUser(User user);
		
}
