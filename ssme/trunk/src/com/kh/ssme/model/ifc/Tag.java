/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.ifc;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: MadMax
 * Date: 2011-04-14
 * Time: 18:48:22
 * To change this template use File | Settings | File Templates.
 */
public interface Tag extends BasicData {

    //private String name_;
    //private String description_;
    //private String url_;
    //private List<TimeFrame> timeFrames_;

	public static final String FIELD_NAME = "name";
	public static final String FIELD_DESCRIPTION = "description";
	public static final String FIELD_URL = "url";
	public static final String FIELD_TIME_FRAMES = "timeFrames";

	public String getName();

	public void setName(String name);

	public String getDescription();

	public void setDescription(String description);

	public String getUrl();

	public void setUrl(String url);

	public List<TimeFrame> getTimeFrames();

	public void setTimeFrames(List<TimeFrame> timeFrames);

}
