/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.entity;

import com.kh.ssme.model.enums.RoleEnum;
import com.kh.ssme.model.ifc.Role;
import com.kh.ssme.model.ifc.User;

import javax.persistence.*;

/**
 * @author Michal Szopinski
 *
 */
@Entity(name = "Role")
@Table(name = "role")
public class RoleEntity extends BasicDataEntity implements Role {
	
	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = -765915665878345759L;

	@ManyToOne(optional=false, targetEntity = UserEntity.class)
    @JoinColumn(name="user_id", nullable=false, updatable=true)	
	private User user_;
	
	@Basic
	@Column(name = "login")
	private String login_;	
	
	@Basic
	@Column(name = "role")
	@Enumerated(EnumType.STRING)	
	private RoleEnum role_;

	/**
	 * Creates new instance of RoleEntity
	 */
	public RoleEntity(){
		super();
	}
	
	/**
	 * Creates new instance of RoleEntity
	 * @param user - user
	 * @param role - user's role
	 */
	public RoleEntity(User user, RoleEnum role){
		super();
		user_ = user;
		login_ = user.getLogin();
		role_ = role;
	}		
	
	@Override
	public String toString(){
		return "{RoleEntity:[user:"+getUser().toString()+"; login:"+getLogin()+";role:"+getRole()+"; ]}"; 
	}	

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Role#getUser()
	 */
	@Override
	public User getUser() {
		return user_;
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Role#setUser(com.kh.ssme.model.ifc.User)
	 */
	@Override
	public void setUser(User user) {
		user_ = user; 		
	}		
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Role#getLogin()
	 */
	@Override
	public String getLogin() {
		return login_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Role#setLogin(java.lang.String)
	 */
	@Override
	public void setLogin(String login) {
		login_ = login;
	}	
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Role#getRole()
	 */
	@Override
	public RoleEnum getRole() {
		return role_;
	}	

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Role#setRole(com.kh.ssme.model.enums.RoleEnum)
	 */
	@Override
	public void setRole(RoleEnum role) {
		role_ = role;
	}
	

}
