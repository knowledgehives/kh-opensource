/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2011,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity;

import com.kh.ssme.model.ifc.Tag;
import com.kh.ssme.model.ifc.TimeFrame;
import com.kh.ssme.model.ifc.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: MadMax
 * Date: 2011-04-14
 * Time: 18:48:58
 * To change this template use File | Settings | File Templates.
 */
@Entity(name = "Tag")
@Table(name = "tag")
public class TagEntity extends BasicDataEntity implements Tag {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1089341492246844811L;

	@Basic
	@Column(name = "name")
	private String name_ = "";

	@Basic
	@Column(name = "description")
	private String description_ = "";

	@Basic
	@Column(name = "url")
	private String url_ = "";

	@ManyToMany(targetEntity=TimeFrameEntity.class, mappedBy="tags_", cascade=CascadeType.ALL)
	private List<TimeFrame> timeFrames_ = new ArrayList<TimeFrame>();

    /**
	 * Creates new instance of TagEntity
	 */
	public TagEntity(){
		super();
	}

    /**
     * Creates new instance of TagEntity
     * @param name
     * @param description
     * @param url
     */
	public TagEntity(String name, String description, String url){
		super();
		name_ = name;
		description_ = description;
		url_ = url;
	}

    @Override
    public String getName() {
        return name_;
    }

    @Override
    public void setName(String name) {
        name_ = name;
    }

    @Override
    public String getDescription() {
        return description_;
    }

    @Override
    public void setDescription(String description) {
        description_ = description;
    }

    @Override
    public String getUrl() {
        return url_;
    }

    @Override
    public void setUrl(String url) {
        url_ = url;
    }

    @Override
    public List<TimeFrame> getTimeFrames() {
        return timeFrames_;
    }

    @Override
    public void setTimeFrames(List<TimeFrame> timeFrames) {
        timeFrames_ = timeFrames;
    }
}
