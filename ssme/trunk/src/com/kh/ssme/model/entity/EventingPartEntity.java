/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Michał Szopiński, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.kh.ssme.model.entity;

import com.kh.ssme.model.enums.StateEnum;
import com.kh.ssme.model.ifc.Eventing;
import com.kh.ssme.model.ifc.EventingPart;

import javax.persistence.*;
import java.util.Date;

/**
 *
 */
@Entity(name = "EventingPart")
@Table(name = "eventingpart")
public class EventingPartEntity extends BasicDataEntity implements EventingPart {
	
    @Basic
    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private StateEnum state_;

	@Basic
	@Column(name = "day")
	private Date day_;

	@Basic
	@Column(name = "map")
	private String map_;

    @ManyToOne(optional=false, targetEntity=EventingEntity.class)
    @JoinColumn(name="eventing_id", nullable=false, updatable=true)
    private Eventing eventing_;

    
	/**
	 * Creates new instance of EventingPartEntity
	 */
	public EventingPartEntity() {
		super();
	}

    /**
     * Creates new instance of EventingPartEntity
     * @param eventing
     * @param map
     * @param state
     */
	public EventingPartEntity(Eventing eventing, String map, Date day, StateEnum state) {
		super();
        eventing_ = eventing;
        day_ = day;
        map_ = map;
		state_ = state;
	}


    @Override
    public StateEnum getState() {
        return state_;
    }

    @Override
    public void setState(StateEnum state) {
        state_ = state;
    }

    @Override
    public Eventing getEventing() {
        return eventing_;
    }

    @Override
    public void setEventing(Eventing eventing) {
        eventing_ = eventing;
    }

    @Override
    public Date getDay() {
        return day_;
    }

    @Override
    public void setDay(Date day) {
        day_ = day;
    }

    @Override
    public String getMap() {
        return map_;
    }

    @Override
    public void setMap(String map) {
        map_ = map;
    }

}