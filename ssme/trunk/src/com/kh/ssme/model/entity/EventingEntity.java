/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.entity;

import com.kh.ssme.model.enums.ParticipationTypeEnum;
import com.kh.ssme.model.enums.StateEnum;
import com.kh.ssme.model.ifc.Eventing;
import com.kh.ssme.model.ifc.EventingPart;
import com.kh.ssme.model.ifc.MeetingRequest;
import com.kh.ssme.model.ifc.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Michal Szopinski
 * 
 */
@Entity(name = "Eventing")
@Table(name = "eventing")
public class EventingEntity extends BasicDataEntity implements Eventing {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 3832845861444238462L;

	@Basic
	@Column(name = "participantType")
	@Enumerated(EnumType.STRING)
	private ParticipationTypeEnum participantType_;

	@ManyToOne(optional = false, targetEntity = MeetingRequestEntity.class)
	@JoinColumn(name = "request_id", nullable = false, updatable = true)
	private MeetingRequest request_;

	@ManyToOne(optional=false, targetEntity = UserEntity.class)
    @JoinColumn(name="user_id", nullable=false, updatable=true)
	private User user_;

	@OneToMany(targetEntity = EventingPartEntity.class, cascade = CascadeType.ALL, mappedBy = "eventing_")
	private List<EventingPart> parts_ = new ArrayList<EventingPart>();    

	@Basic
	@Column(name = "state")
	@Enumerated(EnumType.STRING)
	private StateEnum state_;

	/**
	 * Creates new instance of EventingEntity
	 */
	public EventingEntity() {
		super();
	}

	/**
	 * Creates new instance of EventingEntity
	 * 
	 * @param participantType
	 */
	public EventingEntity(ParticipationTypeEnum participantType) {
		super();
		participantType_ = participantType;
	}

	@Override
	public String toString() {
		return "{EventingEntity:[id:" + getId() + "; participant:" + getParticipantType()
				+ "; user:" + getUser() + "; request:" + getRequest() + "]}";
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#getParticipantType()
	 */
	@Override
	public ParticipationTypeEnum getParticipantType() {
		return participantType_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#setParticipantType(com.kh.ssme.model.enums.ParticipationTypeEnum)
	 */
	@Override
	public void setParticipantType(ParticipationTypeEnum participantType) {
		participantType_ = participantType;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#getRequest()
	 */
	@Override
	public MeetingRequest getRequest() {
		return request_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#setRequest(com.kh.ssme.model.ifc.MeetingRequest)
	 */
	@Override
	public void setRequest(MeetingRequest meetingRequest) {
		request_ = meetingRequest;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#getUser()
	 */
	@Override
	public User getUser() {
		return user_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#setUser(com.kh.ssme.model.ifc.User)
	 */
	@Override
	public void setUser(User user) {
		user_ = user;
	}

    @Override
    public List<EventingPart> getParts() {
        return parts_;
    }

    @Override
    public void setParts(List<EventingPart> parts) {
        parts_ = parts;
    }

    /**
	 * @param state the state to set
	 */
	public void setState(StateEnum state) {
		this.state_ = state;
	}

	/**
	 * @return the state
	 */
	public StateEnum getState() {
		return state_;
	}

}
