/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.entity;

import com.kh.ssme.model.ifc.Group;
import com.kh.ssme.model.ifc.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Michal Szopinski
 *
 */
@Entity(name = "Group")
@Table(name = "ugroup")
public class GroupEntity extends BasicDataEntity implements Group {
	
	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = -4121445493855443936L;

	@Basic
	@Column(name = "name")
	private String name_ = "";

	@Basic
	@Column(name = "description")
	private String description_ = "";	
	
	@ManyToMany(targetEntity=UserEntity.class, cascade=CascadeType.ALL)
	@JoinTable(name="ugroup_user", 
			joinColumns=@JoinColumn(name="ugroup__id", referencedColumnName="id"),
			inverseJoinColumns=@JoinColumn(name="user__id", referencedColumnName="id"))
	private List<User> users_ = new ArrayList<User>();
	
	@ManyToOne(optional=false, targetEntity=UserEntity.class)
	@JoinColumn(name="owner_user_id", nullable=false, updatable=false)	
	private User owner_;	
	
	
	/**
	 * Creates new instance of GroupEntity 
	 */
	public GroupEntity(){
		super(); 
	}

	/**
	 * Creates new instance of GroupEntity
	 * @param name - group name
	 * @param owner - User, group creator and owner
	 */
	public GroupEntity(String name, User owner){
		super(); 
		name_ = name;
		owner_ = owner;
	}	
	
	@Override
	public String toString(){
		return "{GroupEntity: [id:"+getId()+"; owner: "+getOwner()+"; name:"+getName()+";]}";
	}
	
	
	@Override
	public String getName() {
		return name_;
	}

	@Override
	public void setName(String name) {
		name_ = name;		
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Group#getDescription()
	 */
	@Override
	public String getDescription() {
		return description_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Group#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String description) {
		description_ = description;
	}	

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Group#getUsers()
	 */
	@Override
	public List<User> getUsers() {
		return users_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Group#setUsers(java.util.List)
	 */
	@Override
	public void setUsers(List<User> users) {
		users_ = users;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Group#getOwner()
	 */
	@Override
	public User getOwner() {
		return owner_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Group#setOwner(com.kh.ssme.model.ifc.User)
	 */
	@Override
	public void setOwner(User owner) {
		owner_ = owner;		
	}	

	
}
