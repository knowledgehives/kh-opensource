/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Michal Szopinski
 * Enumerates possible types of participation in event, the roles that user fulfill in event 
 */
public enum ParticipationTypeEnum {
    CREATOR("creator"),
    PARTICIPANT("participant"),
    OBSERVER("observer");
    
    private final String name_;
    private static final Map<String,ParticipationTypeEnum> reverse_ = new HashMap<String,ParticipationTypeEnum>();  
    static {
        for(ParticipationTypeEnum rte : EnumSet.allOf(ParticipationTypeEnum.class))
        	reverse_.put(rte.getName(), rte);
    }
    
    ParticipationTypeEnum(String name){
    	name_ = name;
    }
    
    public String getName(){
    	return name_;
    }
    
    public static ParticipationTypeEnum getType(String participationType){
    	return reverse_.get(participationType);
    }
}
