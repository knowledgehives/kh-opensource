/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.enums;

import java.util.EnumSet;
import java.util.HashMap;

/**
 * @author Michal Szopinski
 * Enumerates possible roles of user
 */
public enum RoleEnum {
	USER("user"),
    ADMIN("admin");
    
    private final String role_;
    private static final HashMap<String, RoleEnum> reverse_ = new HashMap<String,RoleEnum>();  
    static {
        for(RoleEnum rte : EnumSet.allOf(RoleEnum.class))
        	reverse_.put(rte.getRole(), rte);
    }
    
    RoleEnum(String role){
    	role_ = role;
    }
    
    public String getRole(){
    	return role_;
    }   
    
    public static RoleEnum getRole(String role){
    	return reverse_.get(role);
    }
    
    public boolean enumEquals(RoleEnum r){
    	return (r!=null && (r.getRole().equals(this.getRole())));    	
    } 
}
