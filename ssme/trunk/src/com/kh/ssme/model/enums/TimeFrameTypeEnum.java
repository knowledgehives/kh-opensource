/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Michal Szopinski
 * Enumerates types of TimeFrame
 */
public enum TimeFrameTypeEnum {
	AVAILABLE("available"),
    BUSYHIDDEN("busyhidden"),
    BUSYTRANSPORT("busytransport"),
	EVENT("event"),
	STAYPLACE("stayplace"),
	REQUEST("request"),    
	SUGGESTED("suggested");	
	
    private final String name_;
    private static final Map<String,TimeFrameTypeEnum> reverse_ = new HashMap<String,TimeFrameTypeEnum>();  
    static {
        for(TimeFrameTypeEnum rte : EnumSet.allOf(TimeFrameTypeEnum.class))
        	reverse_.put(rte.getName(), rte);
    }
    
    TimeFrameTypeEnum(String name){
    	name_ = name;
    }
    
    public String getName(){
    	return name_;
    }
    
    public static TimeFrameTypeEnum getType(String repeatType){
    	return reverse_.get(repeatType);
    }
}
