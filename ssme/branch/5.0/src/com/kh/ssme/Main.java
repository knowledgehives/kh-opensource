/**
 *
 */
package com.kh.ssme;

import com.kh.ssme.business.PasswordService;
import com.kh.ssme.manage.*;
import com.kh.ssme.model.entity.*;
import com.kh.ssme.model.enums.PriorityEnum;
import com.kh.ssme.model.enums.RoleEnum;
import com.kh.ssme.model.ifc.Calendar;
import com.kh.ssme.model.ifc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author MadMax
 */
public class Main {

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("ssmePU");
    private static final Logger logger_ = LoggerFactory.getLogger(Main.class);

    public Main() {/*Empty block*/
    }


    public static void main(String[] args) {
        //SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

        System.out.println(dateTimeFormat.format(java.util.Calendar.getInstance().getTime()));

        logger_.trace("Trace");
        logger_.debug("Debug");
        logger_.info("Info");
        logger_.warn("Warn");
        logger_.error("Error");

        String secretPassword = PasswordService.Digest("secret", "SHA-1", null);
        // e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4

        User u666 = new UserEntity();
        u666.setName("Name_666");
        u666.setSurname("Surname_666");
        u666.setLogin("user_666");
        u666.setMail("user666@someserver.com");
        u666.setMobile("(+48) 1234567666");
        u666.setPassword(secretPassword);    // "secret"

        Role role666 = new RoleEntity();
        role666.setLogin(u666.getLogin());
        role666.setRole(RoleEnum.USER);

        //create special calendar for proposed meetings
        Calendar calendar = new CalendarEntity( CalendarEntity.CALENDAR_NAME_4_REQUESTS  );
        u666.getCalendars().add( calendar );
        calendar.setUser( u666 );

        PersistenceManager.getInstance().beginTransaction();
        UserManager.createUser(u666);
        PersistenceManager.getInstance().commitTransaction();

        u666 = UserManager.findUserByLogin(u666.getLogin());
        role666.setUser(u666);
        PersistenceManager.getInstance().beginTransaction();
        RoleManager.createRole(role666);
        PersistenceManager.getInstance().commitTransaction();
        //users.add(u);


        if (false) {
            int simpleUsersCount = 15;
            int groupsCount = 10;
            List<User> users = new ArrayList<User>();
            for (int i = 0; i < simpleUsersCount; i++) {
                User u = new UserEntity();
                u.setName("Name_" + Integer.valueOf(i + 1));
                u.setSurname("Surname_" + Integer.valueOf(i + 1));
                u.setLogin("user_" + Integer.valueOf(i + 1));
                u.setMail("user" + Integer.valueOf(i + 1) + "@someserver.com");
                u.setMobile("(+48) 12345678" + (i > 9 ? Integer.valueOf(i) : "0" + Integer.valueOf(i)));
                u.setPassword(secretPassword);    // "secret"
                Role role = new RoleEntity();
                role.setLogin(u.getLogin());
                role.setRole(RoleEnum.USER);

                PersistenceManager.getInstance().beginTransaction();
                UserManager.createUser(u);

                PersistenceManager.getInstance().commitTransaction();
                u = UserManager.findUserByLogin(u.getLogin());
                role.setUser(u);
                PersistenceManager.getInstance().beginTransaction();
                RoleManager.createRole(role);
                PersistenceManager.getInstance().commitTransaction();
                users.add(u);
            }

            User superUser = new UserEntity();
            superUser.setName("SuperUserName");
            superUser.setSurname("SuperUserSurname");
            superUser.setLogin("superUser");
            superUser.setMail("superuser@someserver.com");
            superUser.setMobile("(+48) 666666666");
            superUser.setPassword(secretPassword);    // "secret"
            PersistenceManager.getInstance().beginTransaction();
            UserManager.createUser(superUser);
            PersistenceManager.getInstance().commitTransaction();
            superUser = UserManager.findUserByLogin(superUser.getLogin());
            Role role = new RoleEntity();
            role.setLogin(superUser.getLogin());
            role.setRole(RoleEnum.USER);
            role.setUser(superUser);
            Role role_admin = new RoleEntity();
            role_admin.setLogin(superUser.getLogin());
            role_admin.setRole(RoleEnum.ADMIN);
            role_admin.setUser(superUser);
            PersistenceManager.getInstance().beginTransaction();
            RoleManager.createRole(role);
            RoleManager.createRole(role_admin);
            PersistenceManager.getInstance().commitTransaction();

            Random random_user = new Random();

            //tworzymy 10 grup dla SU
            for (int i = 0; i < groupsCount; i++) {
                Group g = new GroupEntity();
                g.setDescription("Description of group no " + Integer.valueOf(i + 1));
                g.setName("Group_" + Integer.valueOf(i + 1));
                g.setOwner(superUser);

                // uzytkownicy
                // rozmiar
                Map<String, User> alreadyAdded = new HashMap<String, User>();
                List<User> usersToAdd = new ArrayList<User>();
                int size = random_user.nextInt(simpleUsersCount);
                for (int j = 0; j < size; j++) {
                    int nextNum = random_user.nextInt(simpleUsersCount);
                    if (!alreadyAdded.containsKey(users.get(nextNum).getName())) {
                        usersToAdd.add(users.get(nextNum));
                        alreadyAdded.put(users.get(nextNum).getName(), users.get(nextNum));
                    }
                }

                g.setUsers(usersToAdd);
                PersistenceManager.getInstance().beginTransaction();
                GroupManager.createGroup(g);
                PersistenceManager.getInstance().commitTransaction();
                g = GroupManager.findGroupByName(g.getName());
            }

            //kalendarze
            int noOfSuperUserCalendars = 3;
            int maxNumberOfEvents = 20;
            for (int i = 0; i < noOfSuperUserCalendars; i++) {
                Calendar c = new CalendarEntity();
                c.setUser(superUser);
                c.setName("Calendar_" + Integer.valueOf(i + 1));
                PersistenceManager.getInstance().beginTransaction();
                CalendarManager.createCalendar(c);
                PersistenceManager.getInstance().commitTransaction();
                c = CalendarManager.findCalendarByUUID(c.getUUID());

                createEventsForCalendar(random_user, maxNumberOfEvents, i, c);
            }

            for (User u : users) {
                Calendar c = new CalendarEntity();
                c.setUser(u);
                c.setName("Calendar_for_" + u.getLogin());
                PersistenceManager.getInstance().beginTransaction();
                CalendarManager.createCalendar(c);
                PersistenceManager.getInstance().commitTransaction();
                c = CalendarManager.findCalendarByUUID(c.getUUID());

                createEventsForCalendar(random_user, maxNumberOfEvents, 1, c);
            }

            //drugi superuser
            superUser = new UserEntity();
            superUser.setName("Super_User_2_Name");
            superUser.setSurname("Super_User_2_Surname");
            superUser.setLogin("superUser_2");
            superUser.setMail("superuser2@someserver.com");
            superUser.setMobile("(+48) 966666666");
            superUser.setPassword(secretPassword);    // "secret"
            PersistenceManager.getInstance().beginTransaction();
            UserManager.createUser(superUser);

            PersistenceManager.getInstance().commitTransaction();
            superUser = UserManager.findUserByLogin(superUser.getLogin());
            role = new RoleEntity();
            role.setLogin(superUser.getLogin());
            role.setRole(RoleEnum.USER);
            role.setUser(superUser);
            role_admin = new RoleEntity();
            role_admin.setLogin(superUser.getLogin());
            role_admin.setRole(RoleEnum.ADMIN);
            role_admin.setUser(superUser);
            PersistenceManager.getInstance().beginTransaction();
            RoleManager.createRole(role);
            RoleManager.createRole(role_admin);
            PersistenceManager.getInstance().commitTransaction();

            Group g = new GroupEntity();
            g.setDescription("Description of extra group");
            g.setName("Group_extra");
            g.setOwner(superUser);

            // uzytkownicy
            // rozmiar
            Map<String, User> alreadyAdded = new HashMap<String, User>();
            List<User> usersToAdd = new ArrayList<User>();
            int size = random_user.nextInt(simpleUsersCount);
            for (int j = 0; j < size; j++) {
                int nextNum = random_user.nextInt(simpleUsersCount);
                if (!alreadyAdded.containsKey(users.get(nextNum).getName())) {
                    usersToAdd.add(users.get(nextNum));
                    alreadyAdded.put(users.get(nextNum).getName(), users.get(nextNum));
                }
            }
            g.setUsers(usersToAdd);
            PersistenceManager.getInstance().beginTransaction();
            GroupManager.createGroup(g);
            PersistenceManager.getInstance().commitTransaction();
            g = GroupManager.findGroupByName(g.getName());

        }


    }


    private static void createEventsForCalendar(Random random_user, int maxNumberOfEvents, int i, Calendar c) {
        int numberOfEvents = random_user.nextInt(maxNumberOfEvents + 1);
        @SuppressWarnings("unused")
        TimeFrame previous = null;
        java.util.Calendar firstDay = java.util.Calendar.getInstance();
        firstDay.set(java.util.Calendar.DAY_OF_WEEK, 2);
        firstDay.set(java.util.Calendar.SECOND, 0);
        firstDay.set(java.util.Calendar.MINUTE, 0);

        for (int j = 0; j < numberOfEvents; j++) {

            int numOfDays = random_user.nextInt(5);

            TimeFrame tf = new TimeFrameEntity();
            tf.setCalendar(c);
            tf.setDescription("Description calendar: " + Integer.valueOf(i + 1) + ", event: " + Integer.valueOf(j + 1));
            tf.setTitle("Title calendar: " + Integer.valueOf(i + 1) + ", event: " + Integer.valueOf(j + 1));
            tf.setPriority((j % 3 == 0) ? PriorityEnum.HIGH : (j % 3 == 1) ? PriorityEnum.MEDIUM : PriorityEnum.LOW);

            java.util.Calendar begin = ((java.util.Calendar) firstDay.clone());
            begin.roll(java.util.Calendar.DAY_OF_WEEK, numOfDays);
            java.util.Calendar end = ((java.util.Calendar) firstDay.clone());
            end.roll(java.util.Calendar.DAY_OF_WEEK, numOfDays);

            begin.roll(java.util.Calendar.HOUR, random_user.nextInt(24));
            end.roll(java.util.Calendar.HOUR, random_user.nextInt(24));

            if (begin.before(end)) {
                tf.setFrom(begin.getTime());
                tf.setTo(end.getTime());
            } else if (begin.after(end)) {
                tf.setFrom(end.getTime());
                tf.setTo(begin.getTime());
            } else {
                begin.set(java.util.Calendar.HOUR, 11);
                tf.setFrom(begin.getTime());
                end.set(java.util.Calendar.HOUR, 13);
                tf.setTo(end.getTime());
            }


            /*
                 if (j == 0){
                     tf.setCalendar(c);
                     tf.setFrom(firstDate.getTime());
                     int randomDifference = random_user.nextInt(10);
                     if (randomDifference % 2 == 0){
                         //firstDate.roll(java.util.Calendar.DAY_OF_YEAR, randomDifference);
                         firstDate.roll(java.util.Calendar.HOUR_OF_DAY, randomDifference);
                     } else {
                         //firstDate.roll(java.util.Calendar.MONTH, randomDifference);
                         firstDate.roll(java.util.Calendar.MINUTE, randomDifference);
                     }
   //				  if (tf.getFrom().getTime() > firstDate.getTime().getTime()){
   //					  firstDate.roll(java.util.Calendar.YEAR, 1);
   //				  }
                     tf.setTo(firstDate.getTime());

                 } else {
                     //odejmujemy od poprzedniego n dni
                     Date toPrevious = previous.getTo();
                     java.util.Calendar toPreviousC = java.util.Calendar.getInstance();
                     toPreviousC.setTime(toPrevious);
                     int randomDifference = random_user.nextInt(10);
                     if (randomDifference % 2 == 0){
                         //toPreviousC.roll(java.util.Calendar.DAY_OF_YEAR, -randomDifference);
                         toPreviousC.roll(java.util.Calendar.HOUR_OF_DAY, -randomDifference);
                     } else {
                         //toPreviousC.roll(java.util.Calendar.MONTH, -randomDifference);
                         toPreviousC.roll(java.util.Calendar.MINUTE, -randomDifference);
                     }
                     tf.setFrom(toPreviousC.getTime());
                     randomDifference = random_user.nextInt(20);
                   //teraz obliczamy DO
                     if (randomDifference % 2 == 0){
                         //toPreviousC.roll(java.util.Calendar.DAY_OF_YEAR, randomDifference);
                         toPreviousC.roll(java.util.Calendar.HOUR_OF_DAY, randomDifference);
                     } else {
                         //toPreviousC.roll(java.util.Calendar.MONTH, randomDifference);
                         toPreviousC.roll(java.util.Calendar.MINUTE, randomDifference);
                     }
   //				  if (tf.getFrom().getTime() > toPreviousC.getTime().getTime()){
   //					  toPreviousC.roll(java.util.Calendar.YEAR, 1);
   //				  }

                     tf.setTo(toPreviousC.getTime());

                 }*/
            PersistenceManager.getInstance().beginTransaction();
            TimeFrameManager.createTimeFrame(tf);
            PersistenceManager.getInstance().commitTransaction();
            tf = TimeFrameManager.findTimeFrameByUUID(tf.getUUID());
            previous = tf;
        }
    }

    public static void persist(Object object) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            em.persist(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    public void merge(Object object) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            em.merge(object);
            em.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    public <T> T find(Class<T> entityClass, Object object) {
        EntityManager em = emf.createEntityManager();
        return em.find(entityClass, object);
    }

}