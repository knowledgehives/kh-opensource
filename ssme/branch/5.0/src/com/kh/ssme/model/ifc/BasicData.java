/*
 *	SmartSchedule.me - Semantic Calender
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.ifc;

import java.util.Date;

/**
 * Interface for common mapped superclass for all entity classes
 * @author Michal Szopinski
 */
public interface BasicData {
	//protected Long id_;	
	//protected Date createDate_;	
	//protected Date modifiedDate_;		
	//protected Date removedDate_;	
	//protected Boolean deleted_;
	//protected String uuid_;
	
	public static final String FIELD_ID = "id";
	public static final String FIELD_CREATE_DATE = "createDate";	
	public static final String FIELD_MODIFIED_DATE = "modifiedDate";		
	public static final String FIELD_REMOVED_DATE = "removedDate";	
	public static final String FIELD_DELETED = "deleted";	
	public static final String FIELD_UUID = "uuid";	
	public static final String FIELD_READABLE_NAME = "readableName";
			
	public Long getId();
	
	public Date getCreateDate();
	
	public Date getModifiedDate();
	
	public Date getRemovedDate();
	
	public Date setRemovedDate(Date removedDate);	
	
	public void setDeleted(Boolean deleted);
	
	public Boolean isDeleted();
	
	public String getUUID();
	
}
