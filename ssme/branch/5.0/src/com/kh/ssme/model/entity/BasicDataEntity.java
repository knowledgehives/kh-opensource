/*
 *	SmartSchedule.me - Semantic Calender
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan  
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.entity;

import com.kh.ssme.model.ifc.BasicData;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

/**
 *
 */
@MappedSuperclass
public abstract class BasicDataEntity implements BasicData, Serializable, Comparable<BasicDataEntity> {
	
	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = -6373470894164731063L;

	@Id
	@Column(name = "id")
	@GeneratedValue
	protected Long id_;	

	@Basic
	@Column(name = "created", nullable = false)
	protected Date createDate_;	
	
	@Basic
	@Column(name = "modified", nullable = false)
	protected Date modifiedDate_;		
	
	@Basic
	@Column(name = "removed", nullable = true)
	protected Date removedDate_;		
	
	@Basic
	@Column(name = "deleted", nullable = false)	
	protected Boolean deleted_;
	
	@Basic
	@Column(name = "uuid", nullable = false)
	protected String uuid_;

	
	public int compareTo(BasicDataEntity o){
		return this.uuid_.compareTo(o.getUUID());
	}	
	
	@Override
	public boolean equals(Object obj){
		if(obj instanceof BasicDataEntity && this.getUUID().equalsIgnoreCase(((BasicDataEntity)obj).getUUID())){
			return Boolean.TRUE;
		} 
		return Boolean.FALSE;
	}
	
	protected BasicDataEntity(){
		createDate_ = Calendar.getInstance().getTime();
		modifiedDate_ = createDate_;
		deleted_ = Boolean.FALSE;
		uuid_ = UUID.randomUUID().toString();
	}
	
	@Override
	public Long getId() {
		return id_;
	}

	@Override
	public Date getCreateDate() {
		return createDate_;
	}

	@Override
	public Date getModifiedDate() {
		return modifiedDate_;
	}
	
	@Override
	public Date getRemovedDate() {
		return removedDate_;
	}

	@Override
	public Date setRemovedDate(Date removedDate) {
		return removedDate_ = removedDate;
	}	
	
	@PreUpdate
	protected void preUpdate() {
		modifiedDate_ = Calendar.getInstance().getTime();
	}	
	
	@Override	
	public void setDeleted(Boolean deleted){
		deleted_ = deleted;
	}
	
	@Override
	public Boolean isDeleted(){
		return deleted_;
	}
	
	@Override
	public String getUUID(){
		return uuid_;
	}
	 
}
