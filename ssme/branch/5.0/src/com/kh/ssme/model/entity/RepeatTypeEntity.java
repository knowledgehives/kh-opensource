/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.entity;

import com.kh.ssme.model.enums.RepeatTypeEnum;
import com.kh.ssme.model.ifc.RepeatType;
import com.kh.ssme.model.ifc.TimeFrame;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Michal Szopinski
 *
 */
@Entity(name = "RepeatType")
@Table(name = "repeattype")
public class RepeatTypeEntity extends BasicDataEntity implements RepeatType {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = -4353038365006545645L;

	@Basic
	@Column(name = "count")
	private Integer count_;

	@Basic
	@Column(name = "frequency")
	private Integer frequency_;

	@Basic
	@Column(name = "fromTime")
	private Date from_;
	
	@Basic
	@Column(name = "untilTime")
	private Date until_;	

	@Basic
	@Column(name = "repeatType")
	@Enumerated(EnumType.STRING)
	private RepeatTypeEnum repeatType_;
	
    @Basic
    @Column(name = "flags")
    private Integer flags_;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="repeat_", targetEntity=TimeFrameEntity.class)
	private List<TimeFrame> timeFrames_ = new ArrayList<TimeFrame>();
		
	@OneToOne(optional=false, targetEntity=TimeFrameEntity.class)
	@JoinColumn(name="prototype_id", nullable=false, updatable=true)
	private TimeFrame prototype_;	
	

	/**
	 * Creates new instance of RepeatTypeEntity
	 */
	public RepeatTypeEntity() {
		super();
	}

	/**
	 * Creates new instance of RepeatTypeEntity
	 * @param count - number of repeat 
	 * @param until - repeat until given date
	 * @param repeatType - type of repeat
	 */
	public RepeatTypeEntity(int count, int frequency, Date from, Date until, RepeatTypeEnum repeatType){
		super();
		count_ = count;
		frequency_ = frequency;
		from_ = from;
		until_ = until;
		repeatType_ = repeatType;
	}

	@Override
	public String toString() {
		return "{RepeatTypeEntity:[id:" + getId() + "; count:" + getCount() + "; until:"
				+ getUntil() + "; repeatType:" + getRepeatType() + ";]}";
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.RepeatType#getCount()
	 */
	@Override
	public Integer getCount() {
		return count_;
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.RepeatType#setCount(java.lang.Integer)
	 */
	@Override
	public void setCount(Integer count) {
		count_ = count;
	}


	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.RepeatType#getRepeatType()
	 */
	@Override
	public RepeatTypeEnum getRepeatType() {
		return repeatType_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.RepeatType#setRepeatType(com.kh.ssme.model.enums.RepeatTypeEnum)
	 */
	@Override
	public void setRepeatType(RepeatTypeEnum repeatType) {
		repeatType_ = repeatType;
	}


	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.RepeatType#setFrequency(java.lang.Integer)
	 */
	@Override
	public void setFrequency(Integer frequency) {
		this.frequency_ = frequency;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.RepeatType#getFrequency()
	 */
	@Override
	public Integer getFrequency() {
		return frequency_;
	}	
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.RepeatType#getUntil()
	 */
	@Override
	public Date getUntil() {
		return until_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.RepeatType#setUntil(java.util.Date)
	 */
	@Override
	public void setUntil(Date until) {
		until_= until;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.RepeatType#getFrom()
	 */
	@Override
	public Date getFrom() {
		return from_;
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.RepeatType#setFrom(java.util.Date)
	 */
	@Override
	public void setFrom(Date from) {
		from_ = from;
	}	

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.RepeatType#setTimeFrames_(java.util.Set)
	 */
	@Override
	public void setTimeFrames(List<TimeFrame> timeFrames) {
		timeFrames_ = timeFrames;
	}	

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.RepeatType#getTimeFrames()
	 */
	@Override
	public List<TimeFrame> getTimeFrames() {
		return timeFrames_;
	}	
	
	/**
	 * @param flags the flags to set
	 */
	public void setFlags(Integer flags) {
		this.flags_ = flags;
	}

	/**
	 * @return the flags
	 */
	public Integer getFlags() {
		return flags_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.RepeatType#getPrototype()
	 */
	@Override
	public TimeFrame getPrototype() {
		return prototype_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.RepeatType#setPrototype(com.kh.ssme.model.ifc.TimeFrame)
	 */
	@Override
	public void setPrototype(TimeFrame prototype) {
		prototype_ = prototype;
	}	

}
