/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Michal Szopinski
 * Enumerates possible states of event.
 */
public enum StateEnum {
	ACCEPTED("accepted"),
    REJECTED("rejected"),
    TENTATIVE("tentative"),
	UNDECIDED("undecided");
    
    private final String name_;
    private static final Map<String,StateEnum> reverse_ = new HashMap<String,StateEnum>();  
    static {
        for(StateEnum rte : EnumSet.allOf(StateEnum.class))
        	reverse_.put(rte.getName(), rte);
    }
    
    StateEnum(String name){
    	name_ = name;
    }
    
    public String getName(){
    	return name_;
    }
    
    public static StateEnum getState(String state){
    	return reverse_.get(state);
    }
}
