/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.entity;

import com.kh.ssme.model.enums.StateEnum;
import com.kh.ssme.model.ifc.Eventing;
import com.kh.ssme.model.ifc.Location;
import com.kh.ssme.model.ifc.LocationState;

import javax.persistence.*;

/**
 * @author Michal Szopinski
 *
 */
@Entity(name = "LocationState")
@Table(name = "locationstate")
public class LocationStateEntity extends BasicDataEntity implements LocationState {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 245680058028005733L;

	@Basic
	@Column(name = "state")
	@Enumerated(EnumType.STRING)	
	private StateEnum state_;
	
	@OneToOne(optional=false, targetEntity=LocationEntity.class)
	@JoinColumn(name="location_id", nullable=false, updatable=true)
	private Location location_;
	
    @ManyToOne(optional=false, targetEntity=EventingEntity.class)
	@JoinColumn(name="eventing_id", nullable=false, updatable=true)	  
	private Eventing eventing_;
    
    
    
    /**
	 * Creates new instance of LocationStateEntity
     */
    public LocationStateEntity(){
    	super();
    }
    
    /**
	 * Creates new instance of LocationStateEntity
     * @param state - state
     */
    public LocationStateEntity(StateEnum state){
    	super();
    }
    
	@Override
	public String toString(){
		return "{LocationStateEntity:[id:"+getId()+"; state:"+getState()
				+" eventing:"+getEventing()+"; location:"+getLocation()+"]}"; 
	}       

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.LocationState#getEventing()
	 */
	@Override
	public Eventing getEventing() {
		return eventing_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.LocationState#getLocation()
	 */
	@Override
	public Location getLocation() {
		return location_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.LocationState#getState()
	 */
	@Override
	public StateEnum getState() {
		return state_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.LocationState#getState(com.kh.ssme.model.enums.StateEnum)
	 */
	@Override
	public void setState(StateEnum state) {
		state_ = state;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.LocationState#setEventing(com.kh.ssme.model.ifc.Eventing)
	 */
	@Override
	public void setEventing(Eventing eventing) {
		eventing_ = eventing; 
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.LocationState#setLocation(com.kh.ssme.model.ifc.Location)
	 */
	@Override
	public void setLocation(Location location) {
		location_ = location;
	}	
	
}
