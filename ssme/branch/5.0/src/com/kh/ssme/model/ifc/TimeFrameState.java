/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.ifc;

import com.kh.ssme.model.enums.StateEnum;

/**
 * Represents state of a TimeFrame connected with Eventing
 * @author Michal Szopinski
 */
public interface TimeFrameState extends BasicData {	
	//private StateEnum state_;
	//private TimeFrame timeFrame_;
	//private Eventing eventing_;
	
	public static final String FIELD_STATE = "state";
	public static final String FIELD_TIME_FRAME = "timeFrame";
	public static final String FIELD_EVENTING = "eventing";		
	
	public StateEnum getState();
	
	public void setState(StateEnum state);
	
	public TimeFrame getTimeFrame();
	
	public void setTimeFrame(TimeFrame timeFrame);
	
	public Eventing getEventing();
	
	public void setEventing(Eventing eventing);
		
}
