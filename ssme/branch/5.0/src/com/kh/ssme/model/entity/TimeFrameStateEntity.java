/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.entity;

import com.kh.ssme.model.enums.StateEnum;
import com.kh.ssme.model.ifc.Eventing;
import com.kh.ssme.model.ifc.TimeFrame;
import com.kh.ssme.model.ifc.TimeFrameState;

import javax.persistence.*;

/**
 * @author Michal Szopinski
 *
 */
@Entity(name = "TimeFrameState")
@Table(name = "timeframestate")
public class TimeFrameStateEntity extends BasicDataEntity implements TimeFrameState {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 4050051056586985550L;
	
	@Basic
	@Column(name = "state")
	@Enumerated(EnumType.STRING)	
	private StateEnum state_;
	
	@OneToOne(optional=false, targetEntity=TimeFrameEntity.class)
	@JoinColumn(name="timeframe_id", nullable=false, updatable=true)
	private TimeFrame timeFrame_;
	
    @ManyToOne(optional=false, targetEntity=EventingEntity.class)
	@JoinColumn(name="eventing_id", nullable=false, updatable=true)	  
	private Eventing eventing_;

    
    
    /**
	 * Creates new instance of TimeFrameStateEntity
     */
    public TimeFrameStateEntity(){
    	super();
    }

    /**
	 * Creates new instance of TimeFrameStateEntity
     * @param state - state
     */
    public TimeFrameStateEntity(StateEnum state){
    	super();
    }
    
	@Override
	public String toString(){
		return "{TimeFrameStateEntity:[id:"+getId()+"; state:"+getState()
				+" eventing:"+getEventing()+"; timeFrame:"+getTimeFrame()+"]}"; 
	}    
    
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrameState#getEventing()
	 */
	@Override
	public Eventing getEventing() {
		return eventing_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrameState#getState()
	 */
	@Override
	public StateEnum getState() {
		return state_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrameState#getTimeFrame()
	 */
	@Override
	public TimeFrame getTimeFrame() {
		return timeFrame_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrameState#setEventing(com.kh.ssme.model.ifc.Eventing)
	 */
	@Override
	public void setEventing(Eventing eventing) {
		eventing_ = eventing;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrameState#setState(com.kh.ssme.model.enums.StateEnum)
	 */
	@Override
	public void setState(StateEnum state) {
		state_ = state;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrameState#setTimeFrame(com.kh.ssme.model.ifc.TimeFrame)
	 */
	@Override
	public void setTimeFrame(TimeFrame timeFrame) {
		timeFrame_ = timeFrame;
	}

}
