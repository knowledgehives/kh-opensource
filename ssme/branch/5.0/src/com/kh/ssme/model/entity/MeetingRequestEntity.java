/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.entity;

import com.kh.ssme.model.enums.StateEnum;
import com.kh.ssme.model.ifc.Eventing;
import com.kh.ssme.model.ifc.Location;
import com.kh.ssme.model.ifc.MeetingRequest;
import com.kh.ssme.model.ifc.TimeFrame;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Michal Szopinski
 */
@Entity(name = "MeetingRequest")
@Table(name = "meetingrequest")
public class MeetingRequestEntity extends BasicDataEntity implements MeetingRequest {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = -115611857251071191L;

	@Basic
	@Column(name = "notSoonerThen")
	private Date notSoonerThen_;

	@Basic
	@Column(name = "notLaterThen")
	private Date notLaterThen_;

	@Basic
	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private StateEnum state_;

	@Basic
	@Column(name = "title")
	private String title_;

	@Basic
	@Column(name = "description")
	private String description_;
	
	@OneToMany(targetEntity = TimeFrameEntity.class, cascade = CascadeType.ALL, mappedBy = "meetingRequest_")
	private List<TimeFrame> decidedTimes_ = new ArrayList<TimeFrame>();

	@ManyToOne(optional = true, targetEntity = LocationEntity.class)
	@JoinColumn(name = "decidedPlace_id", nullable = true, updatable = true)
	private Location decidedPlace_;

	@OneToMany(targetEntity = EventingEntity.class, cascade = CascadeType.ALL, mappedBy = "request_")
	private List<Eventing> eventings_ = new ArrayList<Eventing>();

	// private Eventing eventing_;	

	/**
	 * Creates new instance of MeetingRequestEntity
	 */
	public MeetingRequestEntity() {
		super();
	}	
	
	/**
	 * Creates new instance of MeetingRequestEntity
	 * 
	 * @param notSoonerThen
	 * @param notLaterThen
	 */
	public MeetingRequestEntity(Date notSoonerThen, Date notLaterThen) {
		super();
	}	
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title_;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title_ = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description_;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description_ = description;
	}


	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.MeetingRequest#getNotLaterThat()
	 */
	@Override
	public Date getNotLaterThen() {
		return notLaterThen_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.MeetingRequest#getNotSoonerThat()
	 */
	@Override
	public Date getNotSoonerThen() {
		return notSoonerThen_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.MeetingRequest#getTimeFrames()
	 */
	@Override
	public List<TimeFrame> getDecidedTimes() {
		return decidedTimes_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.MeetingRequest#setNotLaterThat(java.util.Date)
	 */
	@Override
	public void setNotLaterThen(Date notLaterThen) {
		notLaterThen_ = notLaterThen;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.MeetingRequest#setNotSoonerThat(java.util.Date)
	 */
	@Override
	public void setNotSoonerThen(Date notSoonerThen) {
		notSoonerThen_ = notSoonerThen;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.MeetingRequest#setTimeFrame(com.kh.ssme.model.ifc.TimeFrame)
	 */
	@Override
	public void setDecidedTimes(List<TimeFrame> decidedTimes) {
		decidedTimes_ = decidedTimes;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.MeetingRequest#getDecidedPlace()
	 */
	@Override
	public Location getDecidedPlace() {
		return decidedPlace_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.MeetingRequest#setDecidedPlace(com.kh.ssme.model.ifc.Location)
	 */
	@Override
	public void setDecidedPlace(Location decidedPlace) {
		this.decidedPlace_ = decidedPlace;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.MeetingRequest#getState()
	 */
	@Override
	public StateEnum getState() {
		return state_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.MeetingRequest#setState(com.kh.ssme.model.enums.StateEnum)
	 */
	@Override
	public void setState(StateEnum state) {
		state_ = state;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.MeetingRequest#getEventings()
	 */
	@Override
	public List<Eventing> getEventings() {
		return eventings_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.MeetingRequest#setEventing(com.kh.ssme.model.ifc.Eventing)
	 */
	@Override
	public void setEventings(List<Eventing> eventings) {
		eventings_ = eventings;
	}

	@Override
	public String toString() {
		return "{MeetingRequestEntity:[id:" + getId() + "; notSoonerThen:" + getNotSoonerThen()
				+ "; notLaterThen:" + getNotLaterThen() + "]}";
	}	
	
}
