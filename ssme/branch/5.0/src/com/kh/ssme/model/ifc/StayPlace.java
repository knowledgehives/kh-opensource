/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.ifc;

import java.util.List;

/**
 * Represents relationship between User and his Locations at specific TimeFrame
 * @author Michal Szopinski
 */
public interface StayPlace extends BasicData {
	//private Location location_;
	//private User user_;
	//private List<TimeFrame> timeFrames_;	
	
	public Location getLocation();
	
	public void setLocation(Location location);
	
	public User getUser();
	
	public void setUser(User user);
	
	public List<TimeFrame> getTimeFrames();
	
	public void setTimeFrames(List<TimeFrame> timeFrames);
		
}
