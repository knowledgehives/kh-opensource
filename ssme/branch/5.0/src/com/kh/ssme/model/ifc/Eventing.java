/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.ifc;

import com.kh.ssme.model.enums.ParticipationTypeEnum;
import com.kh.ssme.model.enums.StateEnum;

import java.util.List;

/**
 * Represents relationship between user's Calendar, MeetingRequest and possible TimeFrames and Locations
 * @author Michal Szopinski 
 */
public interface Eventing extends BasicData {
	// private ParticipationTypeEnum participantType_;
	// private MeetingRequest request_;
	// private Calendar calendar_;
	// private List<TimeFrameState> timeFrameStates_;
	// private List<LocationState> locationStates_;
	// private StateEnum state_;
	
	public static final String FIELD_PARTICIPANT_TYPE = "participantType";
	public static final String FIELD_REQUEST = "request";
	public static final String FIELD_CALENDAR = "calendar";
	public static final String FIELD_TIME_FRAME_STATES = "timeFrameStates";
	public static final String FIELD_LOCATION_STATES = "locationStates";
	public static final String FIELD_STATE = "state";	

	public ParticipationTypeEnum getParticipantType();

	public void setParticipantType(ParticipationTypeEnum participantType);

	public MeetingRequest getRequest();

	public void setRequest(MeetingRequest meetingRequest);

	public Calendar getCalendar();

	public void setCalendar(Calendar calendar);

	public List<TimeFrameState> getTimeFrameStates();

	public void setTimeFrameStates(List<TimeFrameState> timeFrameStates);

	public List<LocationState> getLocationStates();

	public void setLocationStates(List<LocationState> locationStates);

	public StateEnum getState();

	public void setState(StateEnum state);
}
