/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.ifc;

import com.kh.ssme.model.enums.PriorityEnum;
import com.kh.ssme.model.enums.TimeFrameTypeEnum;

import java.util.Date;


/**
 * Represents TimeFrame, specified piece of time with given characteristics 
 * @author Michal Szopinski
 */
public interface TimeFrame extends BasicData {
	//private Date from_;
	//private Date to_;
	//private RepeatType repeat_;
	//private String title_;
	//private String description_;
	//private Boolean prototype_;		
	//private TimeFrameTypeEnum type_;
	//private PriorityEnum priority_;
	//private Location eventPlace_;
	//private Location travelFrom_;
	//private Location travelTo_;		
	//private StayPlace stayPlace_;
	//private Calendar calendar_;
	//private TimeFrameState suggestedTimeState_;
	//private MeetingRequest meetingRequest_;	
	//private RepeatTypeEntity parent_;		
	
	
	
	public static final String FIELD_FROM = "from";
	public static final String FIELD_TO = "to";
	public static final String FIELD_REPEAT = "repeat";
	public static final String FIELD_TITLE = "title";
	public static final String FIELD_DESCRIPTION = "description";
	public static final String FIELD_PROTOTYPE = "prototype";			
	public static final String FIELD_TYPE = "type";
	public static final String FIELD_PRIORITY = "priority";
	public static final String FIELD_EVENT_PLACE = "eventPlace";
	public static final String FIELD_TRAVEL_FROM = "travelFrom";
	public static final String FIELD_TRAVEL_TO = "travelTo";		
	public static final String FIELD_STAY_PLACE = "stayPlace";
	public static final String FIELD_CALENDAR = "calendar";
	public static final String FIELD_SUGGESTED_TIME_STATE = "suggestedTimeState";
	public static final String FIELD_MEETING_REQUEST = "meetingRequest";
	public static final String FIELD_PARENT = "parent";			

	public Date getFrom();

	public void setFrom(Date from);

	public Date getTo();

	public void setTo(Date to);

	public RepeatType getRepeatType();

	public void setRepeatType(RepeatType repeatType);

	public String getTitle();

	public void setTitle(String title);

	public String getDescription();

	public void setDescription(String description);

	public TimeFrameTypeEnum getTimeFrameType();

	public void setTimeFrameType(TimeFrameTypeEnum type);

	public PriorityEnum getPriority();

	public void setPriority(PriorityEnum priority);

	public Location getEventPlace();

	public void setEventPlace(Location eventPlace);

	public Location getTravelFrom();

	public void setTravelFrom(Location travelFrom);

	public Location getTravelTo();

	public void setTravelTo(Location travelFrom);

	public StayPlace getStayPlace();

	public void setStayPlace(StayPlace stayPlace);

	public Calendar getCalendar();

	public void setCalendar(Calendar calendar);

	public TimeFrameState getSuggestedTimeState();

	public void setSuggestedTimeState(TimeFrameState state);

	public MeetingRequest getMeetingRequest();

	public void setMeetingRequest(MeetingRequest meetingRequest);

	public RepeatType getParent();
	
	public void setParent(RepeatType parent);
	
	public Boolean isPrototype();
	
	public void setPrototype(Boolean prototype);
		
}
