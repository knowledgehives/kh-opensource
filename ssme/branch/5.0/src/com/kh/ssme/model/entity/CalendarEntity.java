/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.entity;

import com.kh.ssme.model.ifc.Calendar;
import com.kh.ssme.model.ifc.Eventing;
import com.kh.ssme.model.ifc.TimeFrame;
import com.kh.ssme.model.ifc.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Michal Szopinski
 */
@Entity(name = "Calendar")
@Table(name = "calendar")
public class CalendarEntity extends BasicDataEntity implements Calendar {
	
	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 7272135575691391524L;

	@Basic
	@Column(name = "name")
	private String name_ = "";
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="calendar_", targetEntity=TimeFrameEntity.class)
	private List<TimeFrame> timeFrames_ = new ArrayList<TimeFrame>();
	
	@ManyToOne(optional=false, targetEntity = UserEntity.class)
    @JoinColumn(name="user_id", nullable=false, updatable=true)	
	private User user_;

	@OneToMany(targetEntity=EventingEntity.class, cascade=CascadeType.ALL, mappedBy="calendar_")
	private List<Eventing> eventings_ = new ArrayList<Eventing>();


	public static final String CALENDAR_NAME_4_REQUESTS = "Metting requests";
	
	/**
	 * Creates new instance of CalendarEntity
	 */
	public CalendarEntity(){
		super();
	}
	
	/**
	 * Creates new instance of CalendarEntity 
	 * @param name - calendar name
	 */
	public CalendarEntity(String name){
		super();
		name_ = name;
	}
	
	@Override
	public String toString(){
		return "{CalendarEntity:[id:"+getId()+"; name:"+getName()+"; user:"+getUser()+";]}"; 
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Calendar#getName()
	 */
	@Override
	public String getName() {
		return name_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Calendar#getTimeFrames()
	 */
	@Override
	public List<TimeFrame> getTimeFrames() {
		return timeFrames_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Calendar#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		name_ = name;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Calendar#setTimeFrames(java.util.Set)
	 */
	@Override
	public void setTimeFrames(List<TimeFrame> timeFrames) {
		timeFrames_ = timeFrames;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Calendar#getUser()
	 */
	@Override
	public User getUser() {
		return user_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Calendar#setUser(com.kh.ssme.model.ifc.User)
	 */
	@Override
	public void setUser(User user) {
		user_ = user;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Calendar#getEventing()
	 */
	@Override
	public List<Eventing> getEventings() {
		return eventings_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Calendar#setEventing(com.kh.ssme.model.ifc.Eventing)
	 */
	@Override
	public void setEventings(List<Eventing> eventings) {
		eventings_ = eventings;
	}

}
