/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.ifc;

import java.util.List;

/**
 * Represents geographical location for events.
 * @author Michal Szopinski
 */
public interface Location extends BasicData {
	//private String longitude_;	
	//private String latitude_;		
	//private String address_;		
	//private Boolean suggested_;
	//private StayPlaceEntity place_;
	//private List<StayPlace> stayPlaces_;
	//private List<TimeFrame> eventPlaces_;
	//private List<TimeFrame> travelFrom_;
	//private List<TimeFrame> travelTo_;	
	//private LocationState suggestedLocationState_;
	//private List<MeetingRequest> meetingRequests_;	
	
	public String getLongitude();
	
	public void setLongitude(String longitude);
	
	public String getLatitude();
	
	public void setLatitude(String latitude);
	
	public String getAddress();
	
	public void setAddress(String address);
	
	public Boolean isSuggested();
	
	public void setSuggested(Boolean suggested);
	
	public List<StayPlace> getStayPlaces();
	
	public void setStayPlaces(List<StayPlace> stayPlaces);
	
	public List<TimeFrame> getEventPlaces();
	
	public void setEventPlaces(List<TimeFrame> eventPlaces);
	
	public List<TimeFrame> getTravelFrom();
	
	public void setTravelFrom(List<TimeFrame> travelFrom);
	
	public List<TimeFrame> getTravelTo();
	
	public void setTravelTo(List<TimeFrame> travelTo);
	
	public LocationState getLocationState();
	
	public void setLocationState(LocationState state);
	
	public List<MeetingRequest> getMeetingRequests();
	
	public void setMeetingRequests(List<MeetingRequest> meetingRequests);
	
}
