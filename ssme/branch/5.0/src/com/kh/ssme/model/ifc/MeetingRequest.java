/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.ifc;

import com.kh.ssme.model.enums.StateEnum;

import java.util.Date;
import java.util.List;

/**
 * Represents decision on performing a new meeting
 * @author Michal Szopinski 
 */
public interface MeetingRequest extends BasicData {
	// private Date notSoonerThen_;
	// private Date notLaterThen_;
	// private StateEnum state_;
	// private List<TimeFrame> decidedTimes_;
	// private Location decidedPlace_;
	// private Eventing eventing_;
	// private String title;
	// private String description,

	public static final String FIELD_NOT_SOONER_THEN = "notSoonerThen";
	public static final String FIELD_NOT_LATER_THEN = "notLaterThen";
	public static final String FIELD_STATE = "state";
	public static final String FIELD_DECIDED_TIMES = "decidedTimes";
	public static final String FIELD_DECIDED_PLACE = "decidedPlace";
	public static final String FIELD_EVENTING = "eventing";
	public static final String FIELD_TITLE = "title";
	public static final String FIELD_DESCIPTION = "description";	
	
	public Date getNotSoonerThen();

	public void setNotSoonerThen(Date notSoonerThen);

	public Date getNotLaterThen();

	public StateEnum getState();

	public void setState(StateEnum state);

	public void setNotLaterThen(Date notLaterThen);

	public List<TimeFrame> getDecidedTimes();

	public void setDecidedTimes(List<TimeFrame> decidedTimes);

	public Location getDecidedPlace();

	public void setDecidedPlace(Location decidedPlace);

	public List<Eventing> getEventings();

	public void setEventings(List<Eventing> eventings);

	public String getTitle();

	public void setTitle(String title);

	public String getDescription();

	public void setDescription(String description);

}
