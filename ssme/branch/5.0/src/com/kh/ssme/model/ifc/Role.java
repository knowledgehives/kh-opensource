/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.ifc;

import com.kh.ssme.model.enums.RoleEnum;


/**
 * Represents role of user
 * @author Michal Szopinski
 */
public interface Role extends BasicData {
	//private User user_;
	//private String login_;
	//private RoleEnum role_;	
	
	public static final String FIELD_USER = "user";
	public static final String FIELD_LOGIN = "login";
	public static final String FIELD_ROLE = "role";	
	
	public User getUser();
	
	public void setUser(User user);
	
	public String getLogin();
	
	public void setLogin(String login);	
	
	public RoleEnum getRole();
	
	public void setRole(RoleEnum role);		

}
