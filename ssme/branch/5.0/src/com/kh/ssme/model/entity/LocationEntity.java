/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.entity;

import com.kh.ssme.model.ifc.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Michal Szopinski
 */
@Entity(name = "Location")
@Table(name = "location")
public class LocationEntity extends BasicDataEntity implements Location {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 5715399518153100823L;

	@Basic
	@Column(name = "longitude")
	private String longitude_ = "";	
	
	@Basic
	@Column(name = "latitude")
	private String latitude_ = "";		
	
	@Basic
	@Column(name = "address")
	private String address_ = "";		
	
	@Basic
	@Column(name = "suggested")
	private Boolean suggested_ = Boolean.FALSE;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="location_", targetEntity=StayPlaceEntity.class)
	private List<StayPlace> stayPlaces_ = new ArrayList<StayPlace>();
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="eventPlace_", targetEntity=TimeFrameEntity.class)
	private List<TimeFrame> eventPlaces_ = new ArrayList<TimeFrame>();
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="travelFrom_", targetEntity=TimeFrameEntity.class)
	private List<TimeFrame> travelFrom_ = new ArrayList<TimeFrame>();
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="travelTo_", targetEntity=TimeFrameEntity.class)
	private List<TimeFrame> travelTo_ = new ArrayList<TimeFrame>();

	@OneToOne(targetEntity=LocationStateEntity.class, optional=true, mappedBy="location_")	
	private LocationState suggestedLocationState_;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="decidedPlace_", targetEntity=MeetingRequestEntity.class)
	private List<MeetingRequest> meetingRequests_ = new ArrayList<MeetingRequest>();		
	
	
	
	/**
	 * Creates new instance of LocationEntity
	 */
	public LocationEntity (){
		super();
	}
	
	/**
	 * Creates new instance of LocationEntity
	 */
	public LocationEntity (String longitude, String latitude, String address, Boolean suggested){
		super();
		longitude_ = longitude;
		latitude_ = latitude;
		address_ = address;
		suggested_ = suggested;
	}
	
	@Override
	public String toString(){
		return "{LocationEntity:[id:"+getId()+"; address:"+getAddress()+"; longitude:"+getLongitude()
				+" latitude:"+getLatitude()+"; suggested:"+isSuggested()+"]}"; 
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#getAddress()
	 */
	@Override
	public String getAddress() {
		return address_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#getLatitude()
	 */
	@Override
	public String getLatitude() {
		return latitude_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#getLongitude()
	 */
	@Override
	public String getLongitude() {
		return longitude_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#isSuggested()
	 */
	@Override
	public Boolean isSuggested() {
		return suggested_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#setAddress()
	 */
	@Override
	public void setAddress(String address) {
		address_ = address;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#setLatitude(java.lang.String)
	 */
	@Override
	public void setLatitude(String latitude) {
		latitude_ = latitude;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#setLongitude(java.lang.String)
	 */
	@Override
	public void setLongitude(String longitude) {
		longitude_ = longitude;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#setSuggested()
	 */
	@Override
	public void setSuggested(Boolean suggested) {
		suggested_ = suggested;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#getStayPlaces()
	 */
	@Override
	public List<StayPlace> getStayPlaces() {
		return stayPlaces_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#setStayPlaces(java.util.Set)
	 */
	@Override
	public void setStayPlaces(List<StayPlace> stayPlaces) {
		stayPlaces_ = stayPlaces;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#getEventPlaces()
	 */
	@Override
	public List<TimeFrame> getEventPlaces() {
		return eventPlaces_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#getTravelFromPlaces()
	 */
	@Override
	public List<TimeFrame> getTravelFrom() {
		return travelFrom_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#getTravelToPlaces()
	 */
	@Override
	public List<TimeFrame> getTravelTo() {
		return travelTo_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#setEventPlaces(java.util.Set)
	 */
	@Override
	public void setEventPlaces(List<TimeFrame> eventPlaces) {
		eventPlaces_ = eventPlaces;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#setTravelFromPlaces(java.util.Set)
	 */
	@Override
	public void setTravelFrom(List<TimeFrame> travelFromPlaces) {
		travelFrom_ = travelFromPlaces;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#setTravelToPlaces(java.util.Set)
	 */
	@Override
	public void setTravelTo(List<TimeFrame> travelToPlaces) {
		travelTo_ = travelToPlaces;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#getLocationState()
	 */
	@Override
	public LocationState getLocationState() {		
		return suggestedLocationState_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#setLocationState(com.kh.ssme.model.ifc.LocationState)
	 */
	@Override
	public void setLocationState(LocationState state) {
		suggestedLocationState_ = state;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#getMeetingRequests()
	 */
	@Override
	public List<MeetingRequest> getMeetingRequests() {
		return meetingRequests_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Location#setMeetingRequests(java.util.Set)
	 */
	@Override
	public void setMeetingRequests(List<MeetingRequest> meetingRequests) {
		meetingRequests_ = meetingRequests;
	}
	
}
