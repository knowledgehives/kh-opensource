/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.enums;

import com.kh.ssme.model.entity.TimeFrameEntity;
import com.kh.ssme.model.ifc.RepeatType;
import com.kh.ssme.model.ifc.TimeFrame;

import java.util.*;

/**
 * Enumerates possible types of TimeFrame repeat
 * @author Michal Szopinski
 */
public enum RepeatTypeEnum {
    DAILY("daily", 365) {
		@Override
		public List<TimeFrame> getDates(RepeatType repeatType){
			List<TimeFrame> result = new ArrayList<TimeFrame>();	
			int repeatCount = repeatType.getCount().intValue(), alreadyAdded = 0; 
			// if 'repeatCount' not given then we assume it has maximum possible value ~= Integer.MAX_VALUE
			int countToBeAdded = Math.min(this.getMaxCount(), (repeatCount==0)?Integer.MAX_VALUE:repeatCount);			
			int frequency = repeatType.getFrequency().intValue();

			// dates from prototype
			// initial 'from'
			Calendar currentDateFrom = Calendar.getInstance();
			currentDateFrom.setTime( repeatType.getPrototype().getFrom() );
			// initial 'to'
			Calendar currentDateTo = Calendar.getInstance();
			currentDateTo.setTime( repeatType.getPrototype().getTo() );
			
			// 'repeat until' constraint date
			Calendar repeatUntil = Calendar.getInstance();
			// if until date not given then set it to 'somewhere far in the future' ~= new Date(Long.MAX_VALUE/2)
			repeatUntil.setTime( (repeatType.getUntil()==null)?(new Date(Long.MAX_VALUE/2)):repeatType.getUntil() );	
			repeatUntil.add(Calendar.DATE, 1);
			repeatUntil.add(Calendar.MILLISECOND, -1);
			
			while( alreadyAdded<countToBeAdded){

				TimeFrame tf = new TimeFrameEntity();
				// primitive
				tf.setFrom(currentDateFrom.getTime());
				tf.setTo(currentDateTo.getTime());
				tf.setTitle(repeatType.getPrototype().getTitle());
				tf.setDescription(repeatType.getPrototype().getDescription());
				tf.setPriority(repeatType.getPrototype().getPriority());
				tf.setTimeFrameType(repeatType.getPrototype().getTimeFrameType());				
				// entity
				tf.setRepeatType(repeatType);
				tf.setCalendar(repeatType.getPrototype().getCalendar());				
				tf.setSuggestedTimeState(repeatType.getPrototype().getSuggestedTimeState());
				tf.setMeetingRequest(repeatType.getPrototype().getMeetingRequest());							
//				tf.setEventPlace(repeatType.getPrototype().getEventPlace());			
//				tf.setTravelFrom(repeatType.getPrototype().getTravelFrom());
//				tf.setTravelTo(repeatType.getPrototype().getTravelTo());				
//				tf.setStayPlace(repeatType.getPrototype().getStayPlace());				
				result.add(tf);
				
				// prepare for next iteration
				currentDateFrom.add(Calendar.DATE, frequency);
				currentDateTo.add(Calendar.DATE, frequency);
				alreadyAdded++;			
				// if 'repeat until' constraint is met then leave the loop
				if ( currentDateTo.after( repeatUntil ) ){
					break;
				}

			}
			
			return result;
		}
	},
    WEEKLY("weekly", 105) {
		@Override
		public List<TimeFrame> getDates(RepeatType repeatType){			
			List<TimeFrame> result = new ArrayList<TimeFrame>();	
			int countToBeAdded = Math.min(this.getMaxCount(), repeatType.getCount().intValue()), alreadyAdded = 0; 
			int frequency = repeatType.getFrequency().intValue();

			// dates from prototype
			// initial 'from'
			Calendar currentDateFrom = Calendar.getInstance();
			currentDateFrom.setTime( repeatType.getPrototype().getFrom() );
			// initial 'to'
			Calendar currentDateTo = Calendar.getInstance();
			currentDateTo.setTime( repeatType.getPrototype().getTo() );
			
			// 'repeat until' constraint date
			Calendar repeatUntil = Calendar.getInstance();
			repeatUntil.setTime( repeatType.getUntil() );	
			repeatUntil.add(Calendar.DATE, 1);
			repeatUntil.add(Calendar.MILLISECOND, -1);
			
			Map<Integer, Integer> flags = new HashMap<Integer,Integer>();
			int tmp = repeatType.getFlags().intValue();
			//MONDAY
			int currentFlag = 0x01; 
			if ((tmp & currentFlag) > 0){
				flags.put(Calendar.MONDAY, Calendar.MONDAY);
			}
			currentFlag = 0x02;
			if ((tmp & currentFlag) > 0){
				flags.put(Calendar.TUESDAY, Calendar.TUESDAY);
			}
			currentFlag = 0x04;
			if ((tmp & currentFlag) > 0){
				flags.put(Calendar.WEDNESDAY, Calendar.WEDNESDAY);
			}
			currentFlag = 0x08;
			if ((tmp & currentFlag) > 0){
				flags.put(Calendar.THURSDAY, Calendar.THURSDAY);
			}
			currentFlag = 0x0F;
			if ((tmp & currentFlag) > 0){
				flags.put(Calendar.FRIDAY, Calendar.FRIDAY);
			}
			currentFlag = 0x10;
			if ((tmp & currentFlag) > 0){
				flags.put(Calendar.SATURDAY, Calendar.SATURDAY);
			}
			currentFlag = 0x20;
			if ((tmp & currentFlag) > 0){
				flags.put(Calendar.SUNDAY, Calendar.SUNDAY);
			}
			
			int FREQUENCY = repeatType.getFrequency() * 7;
			List<Long> tempBegin = new ArrayList<Long>();
			Calendar newUntil = Calendar.getInstance();
			newUntil.setTime( repeatUntil.getTime() );		
			newUntil.add( Calendar.MILLISECOND, (int)(currentDateFrom.getTimeInMillis()-currentDateTo.getTimeInMillis()));
			
			// for each day of weak starting from Calendar.TEUSDAY
			for( int i=2, flag=0x01; i<=8; i++, flag*=2){
				
				// if day of week is selected to be added
				if((tmp & flag) > 0){
					// prototype from and to
					currentDateFrom.setTime( repeatType.getPrototype().getFrom() );
					// prototype day of week
					int day = currentDateFrom.get(Calendar.DAY_OF_WEEK);
					if(day<i){
						// before so get back to the beginning of the week and add (freq*7) days
						currentDateFrom.add(Calendar.DATE, (day-(i%7))+FREQUENCY);
					} else {
						// we start in current week so just move to proper day
						currentDateFrom.add(Calendar.DATE, ((i%7)-day));
					}
					// else - we can just start
					
					// while it is event that ends before until iterate using given frequency
					do{
						tempBegin.add( new Long(currentDateFrom.getTimeInMillis()) );	// add to temp list
						currentDateFrom.add( Calendar.DATE, FREQUENCY);					// move in time for next iteration
					} while(!currentDateFrom.after(newUntil));							// check if there should be next iteration						
				}				
			}
			
			// liste sort
			
			// liste cut
			
			
			
			
			
			
			
			while( alreadyAdded<countToBeAdded){

				TimeFrame tf = new TimeFrameEntity();
				// primitive
				
				tf.setTitle(repeatType.getPrototype().getTitle());
				tf.setDescription(repeatType.getPrototype().getDescription());
				tf.setPriority(repeatType.getPrototype().getPriority());
				tf.setTimeFrameType(repeatType.getPrototype().getTimeFrameType());				
				// entity
				tf.setRepeatType(repeatType);
				tf.setCalendar(repeatType.getPrototype().getCalendar());				
				tf.setSuggestedTimeState(repeatType.getPrototype().getSuggestedTimeState());
				tf.setMeetingRequest(repeatType.getPrototype().getMeetingRequest());							
//				tf.setEventPlace(repeatType.getPrototype().getEventPlace());			
//				tf.setTravelFrom(repeatType.getPrototype().getTravelFrom());
//				tf.setTravelTo(repeatType.getPrototype().getTravelTo());				
//				tf.setStayPlace(repeatType.getPrototype().getStayPlace());				
				result.add(tf);
				
				// prepare for next iteration				
				currentDateFrom.add(Calendar.DATE, frequency);
				currentDateTo.add(Calendar.DATE, frequency);
				alreadyAdded++;

				// if 'repeat until' constraint is met then leave the loop
				if ( currentDateTo.after( repeatUntil ) ){
					break;
				}
			}
			return result;
		}
	},
    MONTHLY("monthly", 36) {
		@Override
		public List<TimeFrame> getDates(RepeatType repeatType){
			List<TimeFrame> result = new ArrayList<TimeFrame>();
			return result;
		}
	},
    YEARLY("yearly", 50) {
		@Override
		public List<TimeFrame> getDates(RepeatType repeatType){
			List<TimeFrame> result = new ArrayList<TimeFrame>();
			return result;
		}
	};
    
    private final String name_;
    private final int maxCount_;    
    private static final Map<String,RepeatTypeEnum> reverse_ = new HashMap<String,RepeatTypeEnum>();  
    static {
        for(RepeatTypeEnum rte : EnumSet.allOf(RepeatTypeEnum.class))
        	reverse_.put(rte.getName(), rte);
    }
    
    RepeatTypeEnum(String name, int maxCount){
    	name_ = name;
    	maxCount_ = maxCount;
    }
    
    public String getName(){
    	return name_;
    }
    
    public int getMaxCount(){
    	return maxCount_;
    }    

    public abstract List<TimeFrame> getDates(RepeatType repeatType);
    
    public static RepeatTypeEnum getType(String repeatType){
    	return reverse_.get(repeatType);
    }

}
