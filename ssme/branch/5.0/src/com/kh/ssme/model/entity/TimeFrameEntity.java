/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.entity;

import com.kh.ssme.model.enums.PriorityEnum;
import com.kh.ssme.model.enums.TimeFrameTypeEnum;
import com.kh.ssme.model.ifc.*;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Michal Szopinski
 * 
 */
@Entity(name = "TimeFrame")
@Table(name = "timeframe")
public class TimeFrameEntity extends BasicDataEntity implements TimeFrame {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 7177605832932322025L;

	@Basic
	@Column(name = "fromTime")
	private Date from_;

	@Basic
	@Column(name = "toTime")
	private Date to_;

	@ManyToOne(optional = true, targetEntity = RepeatTypeEntity.class)
	@JoinColumn(name = "repeatType_id", nullable = true, updatable = true)
	private RepeatType repeat_;

	@Basic
	@Column(name = "title")
	private String title_;

	@Basic
	@Column(name = "description")
	private String description_;

	@Basic
	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private TimeFrameTypeEnum type_;

	@Basic
	@Column(name = "priority")
	@Enumerated(EnumType.STRING)
	private PriorityEnum priority_;
	
	@Basic
	@Column(name = "prototype")
	private Boolean prototype_ = Boolean.FALSE;	
	
	@ManyToOne(optional = true, targetEntity = LocationEntity.class)
	@JoinColumn(name = "eventPlace_location_id", nullable = true, updatable = true)
	private Location eventPlace_;

	@ManyToOne(optional = true, targetEntity = LocationEntity.class)
	@JoinColumn(name = "travelFrom_location_id", nullable = true, updatable = true)
	private Location travelFrom_;

	@ManyToOne(optional = true, targetEntity = LocationEntity.class)
	@JoinColumn(name = "travelTo_location_id", nullable = true, updatable = true)
	private Location travelTo_;

	@ManyToOne(optional = true, targetEntity = StayPlaceEntity.class)
	@JoinColumn(name = "stayPlace_id", nullable = true, updatable = true)
	private StayPlace stayPlace_;

	@ManyToOne(optional = false, targetEntity = CalendarEntity.class)
	@JoinColumn(name = "calendar_id", nullable = false, updatable = true)
	private Calendar calendar_;

	@OneToOne(targetEntity = TimeFrameStateEntity.class, optional = true, mappedBy = "timeFrame_")
	private TimeFrameState suggestedTimeState_;

	@ManyToOne(optional = true, targetEntity = MeetingRequestEntity.class)
	@JoinColumn(name = "meetingRequest_id", nullable = true, updatable = true)
	private MeetingRequest meetingRequest_;
	
	@OneToOne(targetEntity = RepeatTypeEntity.class, optional = true, mappedBy = "prototype_")
	private RepeatType parent_;		
		

	
	/**
	 * Creates new instance of TimeFrameEntity
	 */
	public TimeFrameEntity() {
		super();
	}

	/**
	 * Creates new instance of TimeFrameEntity 
	 * @param title - event title
	 * @param description - event description
	 * @param from - start date and time
	 * @param to - end date and time
	 * @param type - type
	 * @param priority - priority
	 */
	public TimeFrameEntity(String title, String description, Date from, Date to,
			TimeFrameTypeEnum type, PriorityEnum priority) {
		super();
		title_ = title;
		description_ = description;
		from_ = from;
		to_ = to;
		type_ = type;
		priority_ = priority;
	}

	@Override
	public String toString() {
		return "{TimeFrameEntity:[id:" + getId() + "; title:" + getTitle() + "; type:"
				+ getTimeFrameType() + " from:" + getFrom() + "; to:" + getTo() + "]}";
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#getDescription()
	 */
	@Override
	public String getDescription() {
		return description_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#getEventPlace()
	 */
	@Override
	public Location getEventPlace() {
		return eventPlace_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#getFrom()
	 */
	@Override
	public Date getFrom() {
		return from_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#getPriority()
	 */
	@Override
	public PriorityEnum getPriority() {
		return priority_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#getRepeatType()
	 */
	@Override
	public RepeatType getRepeatType() {
		return repeat_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#getTimeFrameType()
	 */
	@Override
	public TimeFrameTypeEnum getTimeFrameType() {
		return type_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#getTitle()
	 */
	@Override
	public String getTitle() {
		return title_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#getTo(java.util.Date)
	 */
	@Override
	public Date getTo() {
		return to_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#getTravelFrom()
	 */
	@Override
	public Location getTravelFrom() {
		return travelFrom_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#getTravelTo()
	 */
	@Override
	public Location getTravelTo() {
		return travelTo_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String description) {
		description_ = description;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setEventPlace(com.kh.ssme.model.ifc.Location
	 * )
	 */
	@Override
	public void setEventPlace(Location eventPlace) {
		eventPlace_ = eventPlace;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setFrom(java.util.Date)
	 */
	@Override
	public void setFrom(Date from) {
		from_ = from;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setPriority(com.kh.ssme.model.enums.PriorityEnum)
	 */
	@Override
	public void setPriority(PriorityEnum priority) {
		priority_ = priority;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setRepeatType(com.kh.ssme.model.ifc.RepeatType)
	 */
	@Override
	public void setRepeatType(RepeatType repeatType) {
		repeat_ = repeatType;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setTimeFrameType(com.kh.ssme.model.enums.TimeFrameTypeEnum)
	 */
	@Override
	public void setTimeFrameType(TimeFrameTypeEnum type) {
		type_ = type;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setTitle(java.lang.String)
	 */
	@Override
	public void setTitle(String title) {
		title_ = title;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setTo(java.util.Date)
	 */
	@Override
	public void setTo(Date to) {
		to_ = to;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setTravelFrom(com.kh.ssme.model.ifc.Location
	 * )
	 */
	@Override
	public void setTravelFrom(Location travelFrom) {
		travelFrom_ = travelFrom;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setTravelTo(com.kh.ssme.model.ifc.Location)
	 */
	@Override
	public void setTravelTo(Location travelFrom) {
		travelFrom_ = travelFrom;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#getStayPlace()
	 */
	@Override
	public StayPlace getStayPlace() {
		return stayPlace_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setStayPlace(com.kh.ssme.model.ifc.StayPlace)
	 */
	@Override
	public void setStayPlace(StayPlace stayPlace) {
		stayPlace_ = stayPlace;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#getCalendars()
	 */
	@Override
	public Calendar getCalendar() {
		return calendar_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setCalendars(java.util.Set)
	 */
	@Override
	public void setCalendar(Calendar calendar) {
		calendar_ = calendar;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#getSuggestedTimeState()
	 */
	@Override
	public TimeFrameState getSuggestedTimeState() {
		return suggestedTimeState_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setSuggestedTimeState(com.kh.ssme.model.ifc.TimeFrameState)
	 */
	@Override
	public void setSuggestedTimeState(TimeFrameState state) {
		suggestedTimeState_ = state;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#getMeetingRequests()
	 */
	@Override
	public MeetingRequest getMeetingRequest() {
		return meetingRequest_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setMeetingRequests(java.util.Set)
	 */
	@Override
	public void setMeetingRequest(MeetingRequest meetingRequest) {
		meetingRequest_ = meetingRequest;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#getParent()
	 */
	@Override
	public RepeatType getParent() {
		return parent_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setParent(com.kh.ssme.model.ifc.RepeatType)
	 */
	@Override
	public void setParent(RepeatType parent) {
		parent_ = parent;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#isPrototype()
	 */
	@Override
	public Boolean isPrototype() {
		return prototype_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.TimeFrame#setPrototype(java.lang.Boolean)
	 */
	@Override
	public void setPrototype(Boolean prototype) {
		prototype_ = prototype;
	}
	
}
