/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.ifc;

import com.kh.ssme.model.enums.RepeatTypeEnum;

import java.util.Date;
import java.util.List;

/**
 * Represents information for repeat of timeFrame calculations
 * @author Michal Szopinski
 */
public interface RepeatType extends BasicData {
	//private Integer count_;
	//private Integer frequency_;
	//private Date from_;	
	//private Date until_;	
	//private RepeatTypeEnum repeatType_;
    //private Integer flags_;	
	//private List<TimeFrame> timeFrames_;		
	//private TimeFrame prototype_;		
	
	public static final String FIELD_COUNT = "count";
	public static final String FIELD_FREQUENCY = "frequency";
	public static final String FIELD_FROM = "from";	
	public static final String FIELD_UNTIL = "until";	
	public static final String FIELD_REPEAT_TYPE = "repeatType";
	public static final String FIELD_FLAGS = "flags";	
	public static final String FIELD_TIME_FRAMES = "timeFrames";
	public static final String FIELD_PROTOTYPE = "prototype";		

	public Integer getCount();

	public void setCount(Integer count);

	public Integer getFrequency();
	
	public void setFrequency(Integer frequency);	
	
	public Date getUntil();

	public void setUntil(Date until);

	public Date getFrom();
	
	public void setFrom(Date from);	
	
	public RepeatTypeEnum getRepeatType();

	public void setRepeatType(RepeatTypeEnum repeatType);

	public List<TimeFrame> getTimeFrames();

	public void setTimeFrames(List<TimeFrame> timeFrames);

	public Integer getFlags();

	public void setFlags(Integer flags);
	
	public TimeFrame getPrototype();
	
	public void setPrototype(TimeFrame prototype);
}
