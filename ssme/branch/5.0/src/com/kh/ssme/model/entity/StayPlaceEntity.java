/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.entity;

import com.kh.ssme.model.ifc.Location;
import com.kh.ssme.model.ifc.StayPlace;
import com.kh.ssme.model.ifc.TimeFrame;
import com.kh.ssme.model.ifc.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Michal Szopinski
 *
 */
@Entity(name = "StayPlace")
@Table(name = "stayplace")
public class StayPlaceEntity extends BasicDataEntity implements StayPlace {	
	
	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = -29138080463095413L;

	@ManyToOne(optional=false, targetEntity=LocationEntity.class)
	@JoinColumn(name="location_id", nullable=false, updatable=true)
	private Location location_;
	
	@ManyToOne(optional=false, targetEntity = UserEntity.class)
    @JoinColumn(name="user_id", nullable=false, updatable=true)	
	private User user_;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="stayPlace_", targetEntity=TimeFrameEntity.class)
	private List<TimeFrame> timeFrames_ = new ArrayList<TimeFrame>();	

	
	
	/**
	 * Creates new instance of StayPlaceEntity
	 */
	public StayPlaceEntity (){
		super();		
	}
	
	@Override
	public String toString(){
		return "{StayPlaceEntity: [id:"+getId()+"; user:"+getUser()+"; location:"+getLocation()+"]}";
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.StayPlace#getLocation()
	 */
	@Override
	public Location getLocation() {
		return location_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.StayPlace#setLocation(com.kh.ssme.model.entity.LocationEntity)
	 */
	@Override
	public void setLocation(Location location) {
		location_ = location;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.StayPlace#getUser()
	 */
	@Override
	public User getUser() {
		return user_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.StayPlace#setUser(com.kh.ssme.model.ifc.User)
	 */
	@Override
	public void setUser(User user) {
		user_ = user;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.StayPlace#getTimeFrames()
	 */
	@Override
	public List<TimeFrame> getTimeFrames() {		
		return timeFrames_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.StayPlace#setTimeFrames(java.util.Set)
	 */
	@Override
	public void setTimeFrames(List<TimeFrame> timeFrames) {
		timeFrames_ = timeFrames;
	}


}
