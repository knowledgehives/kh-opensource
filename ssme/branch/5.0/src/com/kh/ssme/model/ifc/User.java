/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.ifc;

import java.util.List;

/**
 * Represents User
 * @author Michal Szopinski
 */
public interface User extends BasicData {
	//private String login_;	
	//private String password_;		
	//private String name_;	
	//private String surname_;	
	//private String mail_;
	//private String mobile_;	
	//private List<Role> roles_;		
	//private List<GroupEntity> groups;
	//private List<StayPlace> stayPlaces_;
	//private List<com.kh.ssme.model.ifc.Calendar> calendars_;
	//private List<Group> ownedGroups_;

	public static final String FIELD_LOGIN = "login";	
	public static final String FIELD_PASSWORD = "password";		
	public static final String FIELD_NAME = "name";	
	public static final String FIELD_SURNAME = "surname";	
	public static final String FIELD_MAIL = "mail";
	public static final String FIELD_MOBILE = "mobile";	
	public static final String FIELD_ROLES = "roles";		
	public static final String FIELD_GROUPS = "groups";
	public static final String FIELD_STAY_PLACES = "stayPlaces";
	public static final String FIELD_CALENDARS = "calendars";
	public static final String FIELD_OWNED_GROUPS = "ownedGroups";	
	
	public String getLogin();
	
	public void setLogin(String login);
	
	public String getPassword();
	
	public void setPassword(String password);	
	
	public String getName();
	
	public void setName(String name);
	
	public String getSurname();
	
	public void setSurname(String surname);
	
	public String getMail();
	
	public void setMail(String mail);
	
	public String getMobile();
	
	public void setMobile(String mobile);	
	
	public List<Group> getGroups();
	
	public void setRoles(List<Role> roles); 
	
	public List<Role> getRoles();	
	
	public void setGroups(List<Group> groups); 
	
	public List<StayPlace> getStayPlaces();
	
	public void setStayPlaces(List<StayPlace> stayPlaces);
	
	public List<com.kh.ssme.model.ifc.Calendar> getCalendars();
	
	public void setCalendars(List<com.kh.ssme.model.ifc.Calendar> calendars);
	
	public List<Group> getOwnedGroups();
	
	public void setOwnedGroups(List<Group> ownedGroups); 	
	
	//Util
	public boolean isAdmin();
	
	public boolean isUser();	
	
}
