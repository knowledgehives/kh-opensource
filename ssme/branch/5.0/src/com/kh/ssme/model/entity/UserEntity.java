/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.entity;

import com.kh.ssme.model.enums.RoleEnum;
import com.kh.ssme.model.ifc.Group;
import com.kh.ssme.model.ifc.Role;
import com.kh.ssme.model.ifc.StayPlace;
import com.kh.ssme.model.ifc.User;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Michal Szopinski
 *
 */
@Entity(name = "User")
@Table(name = "user")
public class UserEntity extends BasicDataEntity implements User {
	
	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 1089341492246844813L;

	@Basic
	@Column(name = "login")
	private String login_ = "";	
	
	@Basic
	@Column(name = "password")
	private String password_ = "";		
	
	@Basic
	@Column(name = "name")
	private String name_ = "";	
	
	@Basic
	@Column(name = "surname")
	private String surname_ = "";	
		
	@Basic
	@Column(name = "mail")
	private String mail_ = "";
	
	@Basic
	@Column(name = "mobile")
	private String mobile_ = "";
	
 
	@OneToMany(targetEntity=RoleEntity.class, cascade=CascadeType.ALL, mappedBy="user_")
	private List<Role> roles_ = new ArrayList<Role>();	
	
	@ManyToMany(targetEntity=GroupEntity.class, mappedBy="users_", cascade=CascadeType.ALL)
	private List<Group> groups_ = new ArrayList<Group>();
	
	@OneToMany(targetEntity=StayPlaceEntity.class, cascade=CascadeType.ALL, mappedBy="user_")
	private List<StayPlace> stayPlaces_ = new ArrayList<StayPlace>();
	
	@OneToMany(targetEntity=CalendarEntity.class, cascade=CascadeType.ALL, mappedBy="user_")
	private List<com.kh.ssme.model.ifc.Calendar> calendars_ = new ArrayList<com.kh.ssme.model.ifc.Calendar>();
	
	@OneToMany(targetEntity=GroupEntity.class, cascade=CascadeType.ALL, mappedBy="owner_")
	private List<Group> ownedGroups_ = new ArrayList<Group>();
	
	
	/**
	 * Creates new instance of UserEntity
	 */
	public UserEntity(){
		super();
	}

	/**
	 * Creates new instance of UserEntity
	 * @param login - user name
	 */
	public UserEntity(String login){
		super();
		login_ = login;
	}	
	
	@Override
	public String toString(){
		return "{UserEntity:[id:"+getId()+"; login:"+getLogin()+"; name:"+getName()+"; surname:"+getSurname()
				+"; mail:"+getMail()+"; mobile:"+getMobile()
				+"; #calendars:"+((getCalendars()!=null)?getCalendars().size():"null")
				+"; #groups:"+((getGroups()!=null)?getGroups().size():"null")
				+"; #owned:"+((getOwnedGroups()!=null)?getOwnedGroups().size():"null")+" ]}"; 
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#getLogin()
	 */
	@Override
	public String getLogin() {
		return login_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#setLogin(java.lang.String)
	 */
	@Override
	public void setLogin(String login) {
		login_ = login;
	}
	

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#getPassword()
	 */
	@Override
	public String getPassword() {
		return password_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#setPassword(java.lang.String)
	 */
	@Override
	public void setPassword(String password) {
		this.password_ = password;
	}	
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#getPassword()
	 */
	public String getDummyPassword() {
		return "********";
	}	
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#getMail()
	 */
	@Override
	public String getMail() {
		return mail_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#setMail(java.lang.String)
	 */
	@Override
	public void setMail(String mail) {
		mail_ = mail;
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#getMobile()
	 */
	@Override
	public String getMobile() {
		return mobile_;
	}	

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#setMobile(java.lang.String)
	 */
	@Override
	public void setMobile(String mobile) {
		mobile_ = mobile;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#getName()
	 */
	@Override
	public String getName() {
		return name_;
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		name_ = name;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#getSurname()
	 */
	@Override
	public String getSurname() {
		return surname_;
	}	
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#setSurname(java.lang.String)
	 */
	@Override
	public void setSurname(String surname) {
		surname_ = surname;
	}	
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#getRoles()
	 */
	@Override
	public List<Role> getRoles() {
		return roles_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#setRoles(java.util.List)
	 */
	@Override
	public void setRoles(List<Role> roles) {
		roles_ = roles;
	}	

	@Override
	public List<Group> getGroups() {		
		return groups_;
	}

	@Override
	public void setGroups(List<Group> groups) {
		groups_ = groups;		
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#getStayPlaces()
	 */
	@Override
	public List<StayPlace> getStayPlaces() {
		return stayPlaces_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#setStayPlaces(java.util.List)
	 */
	@Override
	public void setStayPlaces(List<StayPlace> stayPlaces) {
		stayPlaces_ = stayPlaces;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#getCalendars()
	 */
	@Override
	public List<com.kh.ssme.model.ifc.Calendar> getCalendars() {
		return calendars_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#setCalendars(java.util.List)
	 */
	@Override
	public void setCalendars(List<com.kh.ssme.model.ifc.Calendar> calendars) {
		calendars_ = calendars;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#getOwnedGroups()
	 */
	@Override
	public List<Group> getOwnedGroups() {
		return ownedGroups_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#setOwnedGroups(java.util.List)
	 */
	@Override
	public void setOwnedGroups(List<Group> ownedGroups) {
		ownedGroups_ = ownedGroups;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#isAdmin()
	 */
	@Override
	public boolean isAdmin() {
		for(Role r  : getRoles()){
			if(RoleEnum.ADMIN.enumEquals( r.getRole() )){
				return true;
			}
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.User#isUser()
	 */
	@Override
	public boolean isUser() {
		for(Role r  : getRoles()){
			if(RoleEnum.USER.enumEquals( r.getRole() )){
				return true;
			}
		}
		return false;
	}

	
}
