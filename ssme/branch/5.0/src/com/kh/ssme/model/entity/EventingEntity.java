/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.model.entity;

import com.kh.ssme.model.enums.ParticipationTypeEnum;
import com.kh.ssme.model.enums.StateEnum;
import com.kh.ssme.model.ifc.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Michal Szopinski
 * 
 */
@Entity(name = "Eventing")
@Table(name = "eventing")
public class EventingEntity extends BasicDataEntity implements Eventing {

	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 3832845861444238462L;

	@Basic
	@Column(name = "participantType")
	@Enumerated(EnumType.STRING)
	private ParticipationTypeEnum participantType_;

	@ManyToOne(optional = false, targetEntity = MeetingRequestEntity.class)
	@JoinColumn(name = "request_id", nullable = false, updatable = true)
	private MeetingRequest request_;

	@ManyToOne(optional = false, targetEntity = CalendarEntity.class)
	@JoinColumn(name = "calendar_id", nullable = false, updatable = true)
	private Calendar calendar_;

	@OneToMany(targetEntity = TimeFrameStateEntity.class, cascade = CascadeType.ALL, mappedBy = "eventing_")
	private List<TimeFrameState> timeFrameStates_ = new ArrayList<TimeFrameState>();

	@OneToMany(targetEntity = LocationStateEntity.class, cascade = CascadeType.ALL, mappedBy = "eventing_")
	private List<LocationState> locationStates_ = new ArrayList<LocationState>();

	@Basic
	@Column(name = "state")
	@Enumerated(EnumType.STRING)
	private StateEnum state_;

	/**
	 * Creates new instance of EventingEntity
	 */
	public EventingEntity() {
		super();
	}

	/**
	 * Creates new instance of EventingEntity
	 * 
	 * @param participantType
	 */
	public EventingEntity(ParticipationTypeEnum participantType) {
		super();
		participantType_ = participantType;
	}

	@Override
	public String toString() {
		return "{EventingEntity:[id:" + getId() + "; participant:" + getParticipantType()
				+ "; calendar:" + getCalendar() + "; request:" + getRequest() + "]}";
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#getCalendar()
	 */
	@Override
	public Calendar getCalendar() {
		return calendar_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#getLocations()
	 */
	@Override
	public List<LocationState> getLocationStates() {
		return locationStates_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#getTimeFrames()
	 */
	@Override
	public List<TimeFrameState> getTimeFrameStates() {
		return timeFrameStates_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#setCalendar(com.kh.ssme.model.ifc.Calendar)
	 */
	@Override
	public void setCalendar(Calendar calendar) {
		calendar_ = calendar;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#setLocations(java.util.Set)
	 */
	@Override
	public void setLocationStates(List<LocationState> locations) {
		locationStates_ = locations;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#setTimeFrames(java.util.Set)
	 */
	@Override
	public void setTimeFrameStates(List<TimeFrameState> timeFrames) {
		timeFrameStates_ = timeFrames;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#getParticipantType()
	 */
	@Override
	public ParticipationTypeEnum getParticipantType() {
		return participantType_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#setParticipantType(com.kh.ssme.model.enums.ParticipationTypeEnum)
	 */
	@Override
	public void setParticipantType(ParticipationTypeEnum participantType) {
		participantType_ = participantType;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#getRequest()
	 */
	@Override
	public MeetingRequest getRequest() {
		return request_;
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.model.ifc.Eventing#setRequest(com.kh.ssme.model.ifc.MeetingRequest)
	 */
	@Override
	public void setRequest(MeetingRequest meetingRequest) {
		request_ = meetingRequest;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(StateEnum state) {
		this.state_ = state;
	}

	/**
	 * @return the state
	 */
	public StateEnum getState() {
		return state_;
	}

}
