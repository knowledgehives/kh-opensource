/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.manage;

import com.kh.ssme.model.entity.RoleEntity;
import com.kh.ssme.model.ifc.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author Michal Szopinski
 *
 */
public class RoleManager {
	
	private static final Logger logger_ = LoggerFactory.getLogger(RoleManager.class);		
	
	/**
	 * Creates a Role
	 * @param role
	 */
	public static void createRole(Role role) {
		logger_.debug("Creating Role: " + role);		
		PersistenceManager.getInstance().persistEntity(role);
	}
	
	/**
	 * Deletes a Role
	 * @param role
	 */
	public static void deleteRole(Role role) {
		logger_.debug("Deleting Role: " + role);		
		role.setDeleted(Boolean.TRUE);
	}	
	
	/**
	 * Removes a Role
	 * @param role
	 */
	public static void removeRole(Role role) {
		logger_.debug("Removing Role: " + role);		
		PersistenceManager.getInstance().removeEntity(role);
	}	
	
	/**
	 * Returns a Role with the specified id.
	 * @param id
	 * @return role entity
	 */
	public static Role findRole(Long id) {
		logger_.debug("Finding Role with id: " + id);		
		return PersistenceManager.getInstance().findEntity(RoleEntity.class, id);
	}	
	
	/**
	 * Returns a Role with the specified UUID.
	 * @param uuid
	 * @return role entity
	 */
	@SuppressWarnings("unchecked")
	public static Role findRoleByUUID(String uuid) {
		logger_.debug("Finding Role with uuid: " + uuid);			
		String query = "SELECT r FROM Role r WHERE r.uuid_ = ?";		
		List<Role> result = PersistenceManager.getInstance().performQuery(query, uuid);
		if(result!=null && !result.isEmpty()){	
			return result.get(0);
		}
		return null;
	}		

}
