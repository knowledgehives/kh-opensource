/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.manage;

import com.kh.ssme.model.entity.TimeFrameStateEntity;
import com.kh.ssme.model.ifc.TimeFrameState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author Michal Szopinski Contains methods for manipulating
 *         TimeFrameStateEntity
 */
public class TimeFrameStateManager {

	private static final Logger logger_ = LoggerFactory.getLogger(TimeFrameStateManager.class);

	/**
	 * Creates a TimeFrameState
	 * 
	 * @param timeFrameState
	 */
	public static void createTimeFrameState(TimeFrameState timeFrameState) {
		logger_.debug("Creating TimeFrameState: " + timeFrameState);
		PersistenceManager.getInstance().persistEntity(timeFrameState);
	}

	/**
	 * Deletes a TimeFrameState
	 * 
	 * @param timeFrameState
	 */
	public static void deleteTimeFrameState(TimeFrameState timeFrameState) {
		logger_.debug("Deleting TimeFrameState: " + timeFrameState);
		timeFrameState.setDeleted(Boolean.TRUE);
	}

	/**
	 * Removes a TimeFrameState
	 * 
	 * @param timeFrameState
	 */
	public static void removeTimeFrameState(TimeFrameState timeFrameState) {
		logger_.debug("Removing TimeFrameState: " + timeFrameState);
		PersistenceManager.getInstance().removeEntity(timeFrameState);
	}

	/**
	 * Returns a TimeFrameState with the specified id.
	 * 
	 * @param id
	 * @return timeFrameState entity
	 */
	public static TimeFrameState findTimeFrameState(Long id) {
		logger_.debug("Finding TimeFrameState with id: " + id);
		return PersistenceManager.getInstance().findEntity(TimeFrameStateEntity.class, id);
	}

	/**
	 * @param uuid
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static TimeFrameState findTimeFrameStateByUUID(String uuid) {
		String query = "SELECT tfs FROM TimeFrameState tfs WHERE tfs.uuid_ = ?";
		List<TimeFrameState> result = PersistenceManager.getInstance().performQuery(query, uuid);
		if (result != null && !result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}
}