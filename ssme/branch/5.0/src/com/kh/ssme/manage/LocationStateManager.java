/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.manage;

import com.kh.ssme.model.entity.LocationStateEntity;
import com.kh.ssme.model.ifc.LocationState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Michal Szopinski
 * Contains methods for manipulating LocationStateEntity
 */
public class LocationStateManager {
	
	private static final Logger logger_ = LoggerFactory.getLogger(LocationStateManager.class);	
	
	/**
	 * Creates a LocationState
	 * @param locationState
	 */
	public static void createLocationState(LocationState locationState) {
		logger_.debug("Creating LocationState: " + locationState);		
		PersistenceManager.getInstance().persistEntity(locationState);
	}
	
	/**
	 * Deletes a LocationState
	 * @param locationState
	 */
	public static void deleteLocationState(LocationState locationState) {
		logger_.debug("Deleting LocationState: " + locationState);		
		locationState.setDeleted(Boolean.TRUE);
	}	
	
	/**
	 * Removes a LocationState
	 * @param locationState
	 */
	public static void removeLocationState(LocationState locationState) {
		logger_.debug("Removing LocationState: " + locationState);		
		PersistenceManager.getInstance().removeEntity(locationState);
	}
	
	/**
	 * Returns a LocationState with the specified id.
	 * @param id
	 * @return locationState entity
	 */
	public static LocationState findLocationState(Long id) {
		logger_.debug("Finding LocationState with id: " + id);				
		return PersistenceManager.getInstance().findEntity(LocationStateEntity.class, id);
	}	
}
