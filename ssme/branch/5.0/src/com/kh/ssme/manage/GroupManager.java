/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.manage;

import com.kh.ssme.model.entity.GroupEntity;
import com.kh.ssme.model.ifc.Group;
import com.kh.ssme.model.ifc.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author Michal Szopinski
 * Contains methods for manipulating GroupEntity
 */
public class GroupManager {
	
	private static final Logger logger_ = LoggerFactory.getLogger(GroupManager.class);	
	
	/**
	 * Creates a Group
	 * @param group
	 */
	public static void createGroup(Group group) {
		logger_.debug("Creating Group: " + group);		
		PersistenceManager.getInstance().persistEntity(group);
	}
	
	/**
	 * Deletes a Group
	 * @param group
	 */
	public static void deleteGroup(Group group) {
		logger_.debug("Deleting Group: " + group);		
		group.setDeleted(Boolean.TRUE);
	}	
	
	/**
	 * Removes a Group
	 * @param group
	 */
	public static void removeGroup(Group group) {
		logger_.debug("Removing Group: " + group);		
		PersistenceManager.getInstance().removeEntity(group);
	}
	
	/**
	 * Returns a Group with the specified id.
	 * @param id
	 * @return group entity
	 */
	public static Group findGroup(Long id) {
		logger_.debug("Finding Group with id: " + id);				
		return PersistenceManager.getInstance().findEntity(GroupEntity.class, id);
	}	
	
	/**
	 * Returns a Group with the specified UUID.
	 * @param uuid
	 * @return user entity
	 */
	@SuppressWarnings("unchecked")
	public static Group findGroupByUUID(String uuid) {
		String query = "SELECT g FROM Group g WHERE g.uuid_ = ?";
		List<Group> result = PersistenceManager.getInstance().performQuery(query, uuid);
		if(result!=null && !result.isEmpty()){
			return result.get(0);
		}
		return null;
	}		

	/**
	 * Returns a User with the specified login.
	 * @param login
	 * @return user entity
	 */
	@SuppressWarnings("unchecked")
	public static Group findGroupByName(String name) {
		String query = "SELECT g FROM Group g WHERE g.name_ = ?";
		List<Group> result = PersistenceManager.getInstance().performQuery(query, name);
		if(result!=null && !result.isEmpty()){
			return result.get(0);
		}
		return null;
	}		
	
	/**
	 * Adds user to group
	 * @param group
	 * @param user
	 */
	public static void joinUserToGroup(Group group, User user) {
		logger_.debug("User "+user+" added to group "+group);	
		if(!group.getUsers().contains(user)){
			group.getUsers().add(user);
			PersistenceManager.getInstance().persistEntity(group);			
		}
	}		
	
}
