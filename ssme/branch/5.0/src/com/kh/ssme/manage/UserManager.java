/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.manage;

import com.kh.ssme.model.entity.UserEntity;
import com.kh.ssme.model.ifc.Group;
import com.kh.ssme.model.ifc.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author Michal Szopinski
 * Contains methods for manipulating UserEntity
 */
public class UserManager {
	
	private static final Logger logger_ = LoggerFactory.getLogger(UserManager.class);	
	
	/**
	 * Creates a User
	 * @param user
	 */
	public static void createUser(User user) {
		logger_.debug("Creating User: " + user);		
		PersistenceManager.getInstance().persistEntity(user);
	}
	
	/**
	 * Deletes a User
	 * @param user
	 */
	public static void deleteUser(User user) {
		logger_.debug("Deleting User: " + user);		
		user.setDeleted(Boolean.TRUE);
	}	
	
	/**
	 * Removes a User
	 * @param user
	 */
	public static void removeUser(User user) {
		logger_.debug("Removing User: " + user);		
		PersistenceManager.getInstance().removeEntity(user);
	}
	
	/**
	 * Returns a User with the specified id.
	 * @param id
	 * @return user entity
	 */
	public static User findBookmark(Long id) {
		logger_.debug("Finding User with id: " + id);		
		return PersistenceManager.getInstance().findEntity(UserEntity.class, id);
	}	
	
	/**
	 * Returns a User with the specified UUID.
	 * @param uuid
	 * @return user entity
	 */
	@SuppressWarnings("unchecked")
	public static User findUserByUUID(String uuid) {
		logger_.debug("Finding User with uuid: " + uuid);			
		String query = "SELECT u FROM User u WHERE u.uuid_ = ?";
		List<User> result = PersistenceManager.getInstance().performQuery(query, uuid);
		if(result!=null && !result.isEmpty()){
			logger_.debug("Finding User with uuid: user found: " + result.get(0));		
			return result.get(0);
		}
		return null;
	}		

	/**
	 * Returns a User with the specified login.
	 * @param login
	 * @return user entity
	 */
	@SuppressWarnings("unchecked")
	public static User findUserByLogin(String login) {
		String query = "SELECT u FROM User u WHERE u.login_ = ?";
		List<User> result = PersistenceManager.getInstance().performQuery(query, login);
		if(result!=null && !result.isEmpty()){
			return result.get(0);
		}
		return null;
	}

	/**
	 * Adds given group to users owned groups
	 * @param owner
	 * @param group
	 */
	public static void addToOwnedGroups(User owner, Group group) {
		logger_.debug("Group "+group+" added to user "+owner+" owned groups ");	
		if(!owner.getOwnedGroups().contains(group)){
			owner.getOwnedGroups().add(group);
			PersistenceManager.getInstance().persistEntity(owner);
		}
	}		
	
}
