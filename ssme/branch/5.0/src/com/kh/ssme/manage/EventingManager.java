/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.manage;

import com.kh.ssme.model.entity.EventingEntity;
import com.kh.ssme.model.ifc.Eventing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author Michal Szopinski 
 * Contains methods for manipulating EventingEntity
 */
public class EventingManager {

	private static final Logger logger_ = LoggerFactory.getLogger(EventingManager.class);

	/**
	 * Creates an Eventing
	 * @param eventing
	 */
	public static void createEventing(Eventing eventing) {
		logger_.debug("Creating Eventing: " + eventing);
		PersistenceManager.getInstance().persistEntity(eventing);
	}

	/**
	 * Deletes an Eventing
	 * @param eventing
	 */
	public static void deleteEventing(Eventing eventing) {
		logger_.debug("Deleting Eventing: " + eventing);
		eventing.setDeleted(Boolean.TRUE);
	}

	/**
	 * Removes an Eventing
	 * @param eventing
	 */
	public static void removeEventing(Eventing eventing) {
		logger_.debug("Removing Eventing: " + eventing);
		PersistenceManager.getInstance().removeEntity(eventing);
	}

	/**
	 * Returns an Eventing with the specified id.
	 * @param id
	 * @return eventing entity
	 */
	public static Eventing findEventing(Long id) {
		logger_.debug("Finding Eventing with id: " + id);
		return PersistenceManager.getInstance().findEntity(EventingEntity.class, id);
	}

	/**
	 * Returns an Eventing with the specified id.
	 * @param id
	 * @return eventing entity
	 */

	@SuppressWarnings("unchecked")
	public static Eventing findEventingByUUID(String uuid) {
		String query = "SELECT e FROM Eventing e WHERE e.uuid_ = ?";
		List<Eventing> result = PersistenceManager.getInstance().performQuery(query, uuid);
		if (result != null && !result.isEmpty()) {
			return result.get(0);
		}
		return null;
	}
}
