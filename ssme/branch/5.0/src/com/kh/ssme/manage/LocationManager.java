/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.manage;

import com.kh.ssme.model.entity.LocationEntity;
import com.kh.ssme.model.ifc.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Michal Szopinski
 * Contains methods for manipulating LocationEntity
 */
public class LocationManager {
	
	private static final Logger logger_ = LoggerFactory.getLogger(LocationManager.class);	
	
	/**
	 * Creates a Location
	 * @param location
	 */
	public static void createLocation(Location location) {
		logger_.debug("Creating Location: " + location);		
		PersistenceManager.getInstance().persistEntity(location);
	}
	
	/**
	 * Deletes a Location
	 * @param location
	 */
	public static void deleteLocation(Location location) {
		logger_.debug("Deleting Location: " + location);		
		location.setDeleted(Boolean.TRUE);
	}	
	
	/**
	 * Removes a Location
	 * @param location
	 */
	public static void removeLocation(Location location) {
		logger_.debug("Removing Location: " + location);		
		PersistenceManager.getInstance().removeEntity(location);
	}
	
	/**
	 * Returns a Location with the specified id.
	 * @param id
	 * @return location entity
	 */
	public static Location findLocation(Long id) {
		logger_.debug("Finding Location with id: " + id);				
		return PersistenceManager.getInstance().findEntity(LocationEntity.class, id);
	}	
}
