/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.manage;

import com.kh.ssme.model.entity.RepeatTypeEntity;
import com.kh.ssme.model.ifc.RepeatType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * @author Michal Szopinski
 * Contains methods for manipulating RepeatTypeEntity
 */
public class RepeatTypeManager {
	private static final Logger logger_ = LoggerFactory.getLogger(RepeatTypeManager.class);	
	
	/**
	 * Creates a RepeatType
	 * @param repeatType
	 */
	public static void createRepeatType(RepeatType repeatType) {
		logger_.debug("Creating RepeatType: " + repeatType);		
		PersistenceManager.getInstance().persistEntity(repeatType);
	}
	
	/**
	 * Deletes a RepeatType
	 * @param repeatType
	 */
	public static void deleteRepeatType(RepeatType repeatType) {
		logger_.debug("Deleting RepeatType: " + repeatType);		
		repeatType.setDeleted(Boolean.TRUE);
	}	
	
	/**
	 * Removes a RepeatType
	 * @param repeatType
	 */
	public static void removeRepeatType(RepeatType repeatType) {
		logger_.debug("Removing RepeatType: " + repeatType);		
		PersistenceManager.getInstance().removeEntity(repeatType);
	}
	
	/**
	 * Returns a RepeatType with the specified id.
	 * @param id
	 * @return repeatType entity
	 */
	public static RepeatType findRepeatType(Long id) {
		logger_.debug("Finding RepeatType with id: " + id);				
		return PersistenceManager.getInstance().findEntity(RepeatTypeEntity.class, id);
	}	
	/**
	 * @param uuid
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static RepeatType findRepeatTypeByUUID(String uuid) {
		String query = "SELECT r FROM RepeatType r WHERE r.uuid_ = ?";
		List<RepeatType> result = PersistenceManager.getInstance().performQuery(query, uuid);
		if(result!=null && !result.isEmpty()){
			return result.get(0);
		}
		return null;
	}	
}