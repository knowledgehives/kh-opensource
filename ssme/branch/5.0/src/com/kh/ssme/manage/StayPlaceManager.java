/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.manage;

import com.kh.ssme.model.entity.StayPlaceEntity;
import com.kh.ssme.model.ifc.StayPlace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Michal Szopinski
 * Contains methods for manipulating StayPlaceEntity
 */
public class StayPlaceManager {
	
	private static final Logger logger_ = LoggerFactory.getLogger(StayPlaceManager.class);	
	
	/**
	 * Creates a StayPlace
	 * @param stayPlace
	 */
	public static void createStayPlace(StayPlace stayPlace) {
		logger_.debug("Creating StayPlace: " + stayPlace);		
		PersistenceManager.getInstance().persistEntity(stayPlace);
	}
	
	/**
	 * Deletes a StayPlace
	 * @param stayPlace
	 */
	public static void deleteStayPlace(StayPlace stayPlace) {
		logger_.debug("Deleting StayPlace: " + stayPlace);		
		stayPlace.setDeleted(Boolean.TRUE);
	}	
	
	/**
	 * Removes a StayPlace
	 * @param stayPlace
	 */
	public static void removeStayPlace(StayPlace stayPlace) {
		logger_.debug("Removing StayPlace: " + stayPlace);		
		PersistenceManager.getInstance().removeEntity(stayPlace);
	}
	
	/**
	 * Returns a StayPlace with the specified id.
	 * @param id
	 * @return stayPlace entity
	 */
	public static StayPlace findStayPlace(Long id) {
		logger_.debug("Finding StayPlace with id: " + id);				
		return PersistenceManager.getInstance().findEntity(StayPlaceEntity.class, id);
	}	
}