/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.business;

import com.kh.ssme.manage.PersistenceManager;
import com.kh.ssme.manage.UserManager;
import com.kh.ssme.model.entity.CalendarEntity;
import com.kh.ssme.model.entity.UserEntity;
import com.kh.ssme.model.ifc.Calendar;
import com.kh.ssme.model.ifc.User;
import com.kh.ssme.rest.parsers.EntityParserTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Michal Szopinski
 *
 */
public class UserService {
	
	private static final Logger logger_ = LoggerFactory.getLogger(UserService.class);		
	
	public static User create(Object o){
		PersistenceManager.getInstance().beginTransaction();
		try{
			User user = new UserEntity();
			EntityParserTools.getMerger(o).parseAndMerge(o, user);	
			if( UserManager.findUserByLogin( user.getLogin() ) != null ){
				// already exist
				return null;
			}

            //create special calendar for proposed meetings
			Calendar calendar = new CalendarEntity( CalendarEntity.CALENDAR_NAME_4_REQUESTS  );
            user.getCalendars().add( calendar );
            calendar.setUser( user );

			UserManager.createUser(user);
			PersistenceManager.getInstance().commitTransaction();
			return user;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	public static User update(String uuid, Object o){
		PersistenceManager.getInstance().beginTransaction();
		try{
			User user = UserManager.findUserByUUID(uuid);
			EntityParserTools.getMerger(o).parseAndMerge(o, user);
			PersistenceManager.getInstance().commitTransaction();
			return user;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	public static User delete(String uuid){
		PersistenceManager.getInstance().beginTransaction();
		try{
			User user = UserManager.findUserByUUID(uuid);
			user.setDeleted(Boolean.TRUE);
		    user.setRemovedDate( java.util.Calendar.getInstance().getTime() );
			PersistenceManager.getInstance().commitTransaction();
			return user;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	public static User get(String uuid){
		try{
			return UserManager.findUserByUUID(uuid);
		} catch (Exception e){
			logger_.error("",e);
		}
		return null;
	}			
	
}
