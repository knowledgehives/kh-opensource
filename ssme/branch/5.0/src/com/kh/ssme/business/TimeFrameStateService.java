package com.kh.ssme.business;

import com.kh.ssme.manage.PersistenceManager;
import com.kh.ssme.manage.TimeFrameStateManager;
import com.kh.ssme.model.entity.TimeFrameStateEntity;
import com.kh.ssme.model.ifc.TimeFrameState;
import com.kh.ssme.rest.parsers.EntityParserTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 */
public class TimeFrameStateService {
	/*
	 * 
	 */
	private static final Logger logger_ = LoggerFactory.getLogger(TimeFrameStateService.class);

	/**
	 * @param json
	 * @return
	 */
	public static TimeFrameState create(Object json) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			TimeFrameState tfstate = new TimeFrameStateEntity();
			EntityParserTools.getMerger(json).parseAndMerge(json, tfstate);
			TimeFrameStateManager.createTimeFrameState(tfstate);
			PersistenceManager.getInstance().commitTransaction();
			return tfstate;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @param json
	 * @return
	 */
	public static TimeFrameState update(String uuid, Object json) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			TimeFrameState tfstate = TimeFrameStateManager.findTimeFrameStateByUUID(uuid);
			EntityParserTools.getMerger(json).parseAndMerge(json, tfstate);
			PersistenceManager.getInstance().commitTransaction();
			return tfstate;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @return
	 */
	public static TimeFrameState delete(String uuid) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			TimeFrameState tfstate = TimeFrameStateManager.findTimeFrameStateByUUID(uuid);
			tfstate.setDeleted(Boolean.TRUE);
		    tfstate.setRemovedDate( java.util.Calendar.getInstance().getTime() );
			PersistenceManager.getInstance().commitTransaction();
			return tfstate;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @return
	 */
	public static TimeFrameState get(String uuid) {
		try {
			return TimeFrameStateManager.findTimeFrameStateByUUID(uuid);
		} catch (Exception e) {
			logger_.error("", e);
		}
		return null;
	}
}
