package com.kh.ssme.business;

import com.kh.ssme.manage.EventingManager;
import com.kh.ssme.manage.PersistenceManager;
import com.kh.ssme.model.entity.EventingEntity;
import com.kh.ssme.model.ifc.Eventing;
import com.kh.ssme.rest.parsers.EntityParserTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 */
public class EventingService {
	/*
	 * 
	 */
	private static final Logger logger_ = LoggerFactory.getLogger(EventingService.class);

	/**
	 * @param json
	 * @return
	 */
	public static Eventing create(Object json) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			Eventing eventing = new EventingEntity();
			EntityParserTools.getMerger(json).parseAndMerge(json, eventing);
			EventingManager.createEventing(eventing);
			PersistenceManager.getInstance().commitTransaction();
			return eventing;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @param json
	 * @return
	 */
	public static Eventing update(String uuid, Object json) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			Eventing eventing = EventingManager.findEventingByUUID(uuid);
			EntityParserTools.getMerger(json).parseAndMerge(json, eventing);
			PersistenceManager.getInstance().commitTransaction();
			return eventing;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @return
	 */
	public static Eventing delete(String uuid) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			Eventing eventing = EventingManager.findEventingByUUID(uuid);
			eventing.setDeleted(Boolean.TRUE);
		    eventing.setRemovedDate( java.util.Calendar.getInstance().getTime() );
			PersistenceManager.getInstance().commitTransaction();
			return eventing;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @return
	 */
	public static Eventing get(String uuid) {
		try {
			return EventingManager.findEventingByUUID(uuid);
		} catch (Exception e) {
			logger_.error("", e);
		}
		return null;
	}
}
