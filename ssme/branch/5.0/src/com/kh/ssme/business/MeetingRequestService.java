package com.kh.ssme.business;

import com.kh.ssme.manage.MeetingRequestManager;
import com.kh.ssme.manage.PersistenceManager;
import com.kh.ssme.model.entity.MeetingRequestEntity;
import com.kh.ssme.model.ifc.MeetingRequest;
import com.kh.ssme.rest.parsers.EntityParserTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 */
public class MeetingRequestService {
	/*
	 * 
	 */
	private static final Logger logger_ = LoggerFactory.getLogger(MeetingRequestService.class);

	/**
	 * @param json
	 * @return
	 */
	public static MeetingRequest create(Object json) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			MeetingRequest request = new MeetingRequestEntity();
			EntityParserTools.getMerger(json).parseAndMerge(json, request);
			MeetingRequestManager.createMeetingRequest(request);
			PersistenceManager.getInstance().commitTransaction();
			return request;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @param json
	 * @return
	 */
	public static MeetingRequest update(String uuid, Object json) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			MeetingRequest request = MeetingRequestManager.findMeetingRequestByUUID(uuid);
			EntityParserTools.getMerger(json).parseAndMerge(json, request);
			PersistenceManager.getInstance().commitTransaction();
			return request;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @return
	 */
	public static MeetingRequest delete(String uuid) {
		PersistenceManager.getInstance().beginTransaction();
		try {
			MeetingRequest request = MeetingRequestManager.findMeetingRequestByUUID(uuid);
			request.setDeleted(Boolean.TRUE);
		    request.setRemovedDate( java.util.Calendar.getInstance().getTime() );
			PersistenceManager.getInstance().commitTransaction();
			return request;
		} catch (Exception e) {
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("", e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @return
	 */
	public static MeetingRequest get(String uuid) {
		try {
			return MeetingRequestManager.findMeetingRequestByUUID(uuid);
		} catch (Exception e) {
			logger_.error("", e);
		}
		return null;
	}
}
