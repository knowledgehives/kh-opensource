/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.business;

import com.kh.ssme.manage.PersistenceManager;
import com.kh.ssme.manage.UserManager;
import com.kh.ssme.model.ifc.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;

/**
 * @author Michal Szopinski
 *
 */
public class PasswordService {
	
	private static final Logger logger_ = LoggerFactory.getLogger(UserService.class);		
	
	private static final String SHA = "SHA-1"; 
	
	/**
	 * Updates password of user with UUID equal 'userUuid' to one given with 'newPlainPassword' if 'oldPlainPassword' is correct.
	 *  
	 * @param userUuid - User's UUID
	 * @param oldPlainPassword - old password, plain text 
	 * @param newPlainPassword - new password, plain text
	 * @return user if succeed or null if failed
	 */
	public static User updateUsersPassword(String userUuid, String oldPlainPassword, String newPlainPassword){
		PersistenceManager.getInstance().beginTransaction();
		try{
			User user = UserManager.findUserByUUID(userUuid);
			
			String oldDigestPassword = Digest(oldPlainPassword, SHA, null);
			String newDigestPassword = Digest(newPlainPassword, SHA, null); 
			
			if(user.getPassword() != null && user.getPassword().equalsIgnoreCase(oldDigestPassword)){
				user.setPassword(newDigestPassword);				
				PersistenceManager.getInstance().commitTransaction();
				return user;				
			}
			PersistenceManager.getInstance().rollbackTransaction();	
		} catch (Exception e){ 
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	/**
	 * Digest 'credentials' with given 'alghorithm' threating 'credentials' as a String with 'encoding' charset.
	 * 
	 * @param credentials
	 * @param algorithm
	 * @param encoding
	 * @return
	 */
	public static final String Digest(String credentials, String algorithm, String encoding){
        try {
            // new message
            MessageDigest md = (MessageDigest) MessageDigest.getInstance(algorithm).clone();
            
            // update message with the encoded credentials
            if (encoding == null) {
                md.update(credentials.getBytes());
            } else {
                md.update(credentials.getBytes(credentials));                
            }
            
            // digest the credentials 
            byte[] digest = md.digest();
            
            // parse hash from byte[] into hex String
    		StringBuffer buf = new StringBuffer();	            
			for(byte b : digest){
				buf.append(digits[(b & 0xf0) >> 4]);
				buf.append(digits[(b & 0x0f)]);				
			}
			return buf.toString();

        } catch(Exception ex) {
            return credentials;
        }
	}
	
    private final static char[] digits = { '0' , '1' , '2' , '3' , 
									       '4' , '5' , '6' , '7' , 
									       '8' , '9' , 'a' , 'b' ,
									       'c' , 'd' , 'e' , 'f' , 
							       		};

}
