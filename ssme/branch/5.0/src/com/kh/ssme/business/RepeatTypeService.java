package com.kh.ssme.business;

import com.kh.ssme.manage.PersistenceManager;
import com.kh.ssme.manage.RepeatTypeManager;
import com.kh.ssme.model.entity.RepeatTypeEntity;
import com.kh.ssme.model.ifc.RepeatType;
import com.kh.ssme.model.ifc.TimeFrame;
import com.kh.ssme.rest.parsers.EntityParserTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;


/**
 * @author Michal Szopinski
 *
 */
public class RepeatTypeService {
	
	/*
	 * 
	 */
	private static final Logger logger_ = LoggerFactory.getLogger(RepeatTypeService.class);
	
	/**
	 * @param json
	 * @return
	 */
	public static RepeatType create(Object json){
		PersistenceManager.getInstance().beginTransaction();
		try{
			RepeatType repeatType = new RepeatTypeEntity();
			EntityParserTools.getMerger(json).parseAndMerge(json, repeatType);
			repeatType.setTimeFrames( repeatType.getRepeatType().getDates( repeatType ) );
			RepeatTypeManager.createRepeatType(repeatType);			
			PersistenceManager.getInstance().commitTransaction();
			return repeatType;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	/**
	 * @param uuid
	 * @param json
	 * @return
	 */
	public static RepeatType update(String uuid, Object json){
		PersistenceManager.getInstance().beginTransaction();
		try{
			// merge
			RepeatType repeatType = RepeatTypeManager.findRepeatTypeByUUID(uuid);
			EntityParserTools.getMerger(json).parseAndMerge(json, repeatType);
			
			List<TimeFrame> templist = repeatType.getRepeatType().getDates( repeatType );
			// remove old timeframes
			Date prototypeFrom = repeatType.getPrototype().getFrom();
			for(TimeFrame t : repeatType.getTimeFrames()){
				if( prototypeFrom.getTime() > t.getTo().getTime() ){
					templist.add( t );
				} else {
					t.setDeleted( true );					
				}
			}
			
			// create new - updated ones
			repeatType.setTimeFrames( templist );			
			PersistenceManager.getInstance().commitTransaction();
			return repeatType;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	/**
	 * @param uuid
	 * @return
	 */
	public static RepeatType delete(String uuid){
		PersistenceManager.getInstance().beginTransaction();
		try{
			RepeatType repeatType = RepeatTypeManager.findRepeatTypeByUUID(uuid);
			repeatType.setDeleted(Boolean.TRUE);
		    repeatType.setRemovedDate( java.util.Calendar.getInstance().getTime() );              
			PersistenceManager.getInstance().commitTransaction();
			return repeatType;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}

	/**
	 * @param uuid
	 * @return
	 */
	public static RepeatType deleteWithChildren(String uuid){
		PersistenceManager.getInstance().beginTransaction();
		try{
			RepeatType repeatType = RepeatTypeManager.findRepeatTypeByUUID(uuid);
			repeatType.setDeleted(Boolean.TRUE);
            for(TimeFrame t : repeatType.getTimeFrames()){
                t.setDeleted( Boolean.TRUE );
            }
			PersistenceManager.getInstance().commitTransaction();
			return repeatType;
		} catch (Exception e){
			PersistenceManager.getInstance().rollbackTransaction();
			logger_.error("",e);
		}
		return null;
	}
	
	/**
	 * @param uuid
	 * @return
	 */
	public static RepeatType get(String uuid){
		try{
			return RepeatTypeManager.findRepeatTypeByUUID(uuid);
		} catch (Exception e){
			logger_.error("",e);
		}
		return null;
	}		

}
