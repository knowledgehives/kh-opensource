/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest;

import com.kh.ssme.rest.util.Params;
import com.kh.ssme.rest.util.ResponseType;
import com.kh.ssme.rest.util.SSMEException;
import com.kh.ssme.rest.util.SSMEExceptionType;
import com.kh.vulcan.annotation.Servlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Michal Szopinski
 *
 */
@Servlet(urlMappings = { "/error/*", "/error" })
public class ErrorServlet extends UtilServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7871422893206760213L;	
	
	@SuppressWarnings("hiding")
	protected static Logger logger_ = LoggerFactory.getLogger(ErrorServlet.class);	
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		System.out.println(" ERROR : "+request.getRequestURL().toString()+" : ");		
		if(request.getParameter( Params.RequestParams.LOGIN_ERROR.paramName() ) != null || 												// param provided OR
				(request.getPathInfo() != null && request.getPathInfo().indexOf( Params.RequestParams.LOGIN_ERROR.paramName() ) > 0)){	// additional path info
			
			ResponseType.determine(request).sendError( response, new SSMEException( SSMEExceptionType.LOGIN_ERROR.getStatus(), SSMEExceptionType.LOGIN_ERROR.getMessage() ) );
			
		}

	}	

}
