/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest.util;

import com.kh.ssme.model.enums.PriorityEnum;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Michal Szopinski
 *
 */
public class Params {
	
	public enum SessionAttributes {
		USER_ENTITY("LoggedUser");
		
		private String paramName_;
	    private static final Map<String,PriorityEnum> reverse_ = new HashMap<String,PriorityEnum>();  		
	    static {
	        for(PriorityEnum rte : EnumSet.allOf(PriorityEnum.class))
	        	reverse_.put(rte.getName(), rte);
	    }	    
		
		private SessionAttributes(String paramName){
			this.paramName_ = paramName;
		}
	    
	    public String paramName(){
	    	return paramName_;
	    }
	    
	    public static PriorityEnum getSessionAttribute(String paramName){
	    	return reverse_.get(paramName);
	    }		
	}
	
	public enum RequestParams {
		FLEX_APPLICATION("flexApp"),
		JSON("json"),
		LOGIN_ERROR("loginError"),
		METHOD("_method"),
		CALENDAR_FROM("from"),
		CALENDAR_TO("to"),
		TIMEFRAME_PARENT("parentUUID"),
		DATA_RECURSION("dataRecursion"),
        REMOVE_CHILDREN_EVENTS("removeChildrenEvents");        
		
		
		private String paramName_;
	    private static final Map<String,PriorityEnum> reverse_ = new HashMap<String,PriorityEnum>();  		
	    static {
	        for(PriorityEnum rte : EnumSet.allOf(PriorityEnum.class))
	        	reverse_.put(rte.getName(), rte);
	    }	    
		
		private RequestParams(String paramName){
			this.paramName_ = paramName;
		}
	    
	    public String paramName(){
	    	return paramName_;
	    }
	    
	    public static PriorityEnum getSessionAttribute(String paramName){
	    	return reverse_.get(paramName);
	    }		
	}	

}
