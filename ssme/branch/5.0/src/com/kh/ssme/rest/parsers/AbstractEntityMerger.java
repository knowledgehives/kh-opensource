/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest.parsers;

import com.kh.ssme.model.ifc.*;

/**
 * @author Michal Szopinski
 * Interface with methods for merging changes into entity object. 
 */
public interface AbstractEntityMerger {
	
	/**
	 * Parses given object into common BasicData for given entity 
	 * @param o object to be parsed 
	 * @param basicData
	 */
	public void parseAndMerge(Object o, BasicData basicData) throws Exception;
	
	/**
	 * Parses given object into Calendar entity 
	 * @param o object to be parsed 
	 * @return entity
	 */
	public void parseAndMerge(Object o, Calendar calendar) throws Exception;
	
	/**
	 * Parses given object into Eventing entity 
	 * @param o object to be parsed 
	 * @return entity
	 */
	public void parseAndMerge(Object o, Eventing eventing) throws Exception;	
	
	/**
	 * Parses given object into Group entity 
	 * @param o object to be parsed 
	 * @return entity
	 */
	public void parseAndMerge(Object o, Group group) throws Exception;
	
	/**
	 * Parses given object into Location entity 
	 * @param o object to be parsed 
	 * @return entity
	 */
	public void parseAndMerge(Object o, Location location) throws Exception;
	
	/**
	 * Parses given object into LocationState entity 
	 * @param o object to be parsed 
	 * @return entity
	 */
	public void parseAndMerge(Object o, LocationState locationState) throws Exception;
	
	/**
	 * Parses given object into MeetingRequest entity 
	 * @param o object to be parsed 
	 * @return entity
	 */
	public void parseAndMerge(Object o, MeetingRequest meetingRequest) throws Exception;
	
	/**
	 * Parses given object into RepeatType entity 
	 * @param o object to be parsed 
	 * @return entity
	 */
	public void parseAndMerge(Object o, RepeatType repeatType) throws Exception;
	
	/**
	 * Parses given object into Role entity 
	 * @param o object to be parsed 
	 * @return entity
	 */
	public void parseAndMerge(Object o, Role role) throws Exception;	
	
	/**
	 * Parses given object into StayPlace entity 
	 * @param o object to be parsed 
	 * @return entity
	 */
	public void parseAndMerge(Object o, StayPlace stayPlace) throws Exception;	
	
	/**
	 * Parses given object into TimeFrame entity 
	 * @param o object to be parsed 
	 * @return entity
	 */
	public void parseAndMerge(Object o, TimeFrame timeFrame) throws Exception;	
	
	/**
	 * Parses given object into TimeFrameState entity 
	 * @param o object to be parsed 
	 * @return entity
	 */
	public void parseAndMerge(Object o, TimeFrameState timeFrameState) throws Exception;	
	
	/**
	 * Parses given object into User entity 
	 * @param o object to be parsed 
	 * @return entity
	 */
	public void parseAndMerge(Object o, User user) throws Exception;		
	
}
