/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest.util;

import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


/**
 * @author Michal Szopinski
 * Types of HTTP Responses supported by the service
 */
public enum ResponseType {
	
		JSON("application/json", 
			"text/x-json", 
			"text/x-javascript"){
				@Override
				public boolean accepts(HttpServletRequest request) {
					boolean ajaxSend = "XMLHttpRequest".equals(request.getHeader("X-Requested-With")); //$NON-NLS-1$ //$NON-NLS-2$
					boolean result = false;
					
					if(!result)
						result = "true".equals(request.getParameter( Params.RequestParams.JSON.paramName() ));
					
					if(!result)
						result = _accepts(request);
					
					return result && ajaxSend;
				}

				@Override
				public void sendError(HttpServletResponse response, SSMEException ex) throws IOException {
					response.sendError(ex.getStatus(), ex.getMessage());					
				}	
				
				@Override
				public void sendExceptionError(HttpServletResponse response, int status, String message) throws IOException{
					response.sendError(status, message);					
				}				
			},
		
		FLEX_JSON("application/x-www-form-urlencoded",
			"application/json"){
				@Override
				public boolean accepts(HttpServletRequest request) {
					//boolean typeValid = _accepts(request);
					boolean result = false;
					
					if(!result)
						result = (request.getParameter( Params.RequestParams.FLEX_APPLICATION.paramName() ) != null);					
					
					if(!result)
						result = (request.getParameter( Params.RequestParams.JSON.paramName() ) != null);				
					
					return result;	// both type is valid and at least one of param conditions met
					//return (result && typeValid);	// both type is valid and at least one of param conditions met ??
				}
				
				@Override
				public void sendError(HttpServletResponse response, SSMEException ex) throws IOException {
					sendExceptionError(response, ex.getStatus(), ex.getMessage());
				}	
				
				@Override
				public void sendExceptionError(HttpServletResponse response, int status, String message) throws IOException {
					JSONObject json = new JSONObject();
					try {
						response.setStatus(status);		
						response.setContentType("application/json");
						
						json.put("errorStatus",""+status);
						json.put("errorMessage",message);									
						Writer writer = response.getWriter();
						writer.append( json.toString() );
						writer.flush();			
					} catch (JSONException e) {
						e.printStackTrace();
					}				
				}						
			},		

		HTML{
				@Override
				public boolean accepts(HttpServletRequest request) {
					return false;
				}
				
				@Override
				public void sendError(HttpServletResponse response, SSMEException ex) throws IOException {
					response.sendError(ex.getStatus(), ex.getMessage());					
				}		
				
				@Override
				public void sendExceptionError(HttpServletResponse response, int status, String message) throws IOException{
					response.sendError(status, message);					
				}						
			};
		
		
		
		
		protected Set<String> types_; 		
		
		private ResponseType(String ... types){
			this.types_ = new HashSet<String>();
			if(types != null)
				this.types_.addAll(Arrays.asList(types));
		}
		
		/**
		 * Determines if given type can support provided request.
		 * @param request
		 * @return
		 */
		public abstract boolean accepts(HttpServletRequest request);
		
		public abstract void sendError(HttpServletResponse response, SSMEException ex) throws IOException;		
		
		public abstract void sendExceptionError(HttpServletResponse response, int i, String message) throws IOException; 
		
		/**
		 * To be called by {@link #accepts(HttpServletRequest)} internally
		 * @param request
		 * @return
		 */
		protected boolean _accepts(HttpServletRequest request){
			boolean result = false;
			
			String saccept = request.getHeader("Accept");
			Set<String> accept;
			
			if(saccept != null){
				accept = new HashSet<String>(Arrays.asList(saccept.split(",")));
			} else {
				accept = new HashSet<String>();
			}
			
			accept.retainAll(this.types_);
			
			result = !accept.isEmpty();
			
			if(!result){
				String contentType = request.getContentType();
				if(contentType != null){
					for(String type : this.types_){
						result = contentType.startsWith( type );
						if(result) break;
					}
				}
			}
			
			return result;
		}
		
		/**
		 * Allows to find the type of accepted request. 
		 * @param request
		 * @return
		 */
		public static ResponseType determine(HttpServletRequest request){
			for(ResponseType type : values())
				if(type.accepts(request))
					return type;
			return HTML;
		}

				

}
