/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Michal Szopinski
 *
 */
public enum SSMEExceptionType {
	JSON_PARSE_ERROR	("JSON Parse error", 								400, false),
	JSON_NOT_FOUND		("JSON not found in request", 						400, false),
	USER_NOT_FOUND		("Logged user's UserEntity object not found", 		401, false),	
	METHOD_NOT_ALLOWED	("Method not allowed", 								405, false),
	LOGIN_ERROR		    ("Login or password is wrong", 						403, false),	
	WRONG_PASSWORD		("Given password is wrong", 						403, false);	
	
	private String message_;
	private int status_;
	private boolean format_; 
	
	Pattern var = Pattern.compile("\\{\\d+\\}");
	
	SSMEExceptionType(String message, int status, boolean format){
		this.message_ = message;
		this.status_ = status;
		this.format_ = format;
	}	
	

	public String getMessage() {
		return (this.format_) ? null : this.message_;
	}
	
	public String getFormattedMessage(String... params) {
		if(this.format_){
	        Matcher matcher = var.matcher( this.message_ );

	        StringBuilder builder = new StringBuilder();
	        int pointer = 0, index = 0;
	        String match;
	        while ( matcher.find() ) {
	            builder.append( this.message_ .substring( pointer, matcher.start() ));
	            match = matcher.group(0);
	            try {
	                index = Integer.parseInt( match.trim().replaceAll("\\{","").replaceAll("\\}","").trim() );
	                builder.append( params[index] );                
	            } catch (Exception e){
	                builder.append( match );                  
	            }
	            pointer = matcher.end();
	        }
	        builder.append( this.message_ .substring( pointer, this.message_ .length() ) );
	        return builder.toString(); 			
		}
		return null;
	}	
	
	public int getStatus() {
		return this.status_;
	}	
	
	public boolean isFormatted(){
		return this.format_;
	}
	
	
	@Override
	public String toString(){
		return this.message_;
	}
		
	
}
