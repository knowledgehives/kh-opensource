/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest.parsers;

import com.kh.ssme.business.CalendarService;
import com.kh.ssme.business.RepeatTypeService;
import com.kh.ssme.business.TimeFrameService;
import com.kh.ssme.business.UserService;
import com.kh.ssme.manage.*;
import com.kh.ssme.model.enums.*;
import com.kh.ssme.model.ifc.*;
import com.kh.ssme.rest.util.TimeUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static org.json.JSONObject.NULL;

/**
 * @author Michal Szopinski
 * merges changes into entity object from JSON object
 */
public class JSONEntityMerger implements AbstractEntityMerger {

	private static final JSONEntityMerger instance_;
	static {
		instance_ = new JSONEntityMerger();
	}

	public synchronized static AbstractEntityMerger getInstance() {
		return instance_;
	}

	private JSONEntityMerger() {
		/*empty block*/
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityMerger#parseAndMerge(java.lang.Object, com.kh.ssme.model.ifc.BasicData)
	 */
	@Override
	public void parseAndMerge(Object o, BasicData basicData) throws JSONException {
		//protected Long id_;	
		//protected Date createDate_;	
		//protected Date modifiedDate_;		
		//protected Boolean deleted_;
		//protected String uuid_;
		/*
		JSONObject json = (JSONObject)o;
		
		Long id = new Long((String)json.get("id"));
		String createDate = (String) json.get("createDate");
		String modifiedDate = (String) json.get("modifiedDate");		
		String mod = (String) json.get("deleted");
		String uuid = (String) json.get("uuid");
		*/		
		
		JSONObject json = (JSONObject) o;		
		if ( json.has(BasicData.FIELD_DELETED) ){
			basicData.setDeleted( json.getBoolean(BasicData.FIELD_DELETED) );
		}		
	}	
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityParser#parseAndMerge(java.lang.Object, com.kh.ssme.model.ifc.Calendar)
	 */
	@Override
	public void parseAndMerge(Object o, Calendar calendar) throws JSONException {
		//private String name_ = "";
		//private Set<TimeFrame> timeFrames_ = new TreeSet<TimeFrame>();
		//private User user_;
		//private Set<Eventing> eventings_ = new TreeSet<Eventing>();
		
		JSONObject json = (JSONObject) o;		
		if ( json.has(Calendar.FIELD_NAME) ){
			calendar.setName( json.getString(Calendar.FIELD_NAME) );
		}
		
		JSONObject user = json.has(Calendar.FIELD_USER) ? (JSONObject) json.get(Calendar.FIELD_USER) : null;
		if(user != null){
			String userUUID = (user.has(BasicData.FIELD_UUID) && !NULL.equals(user.get(BasicData.FIELD_UUID))) ? (String) user.get(BasicData.FIELD_UUID) : null;
			if( userUUID!=null && ( calendar.getUser()==null || !userUUID.equalsIgnoreCase(calendar.getUser().getUUID())) ){	
				// uuid provided && (user not available || UUID changed)
				calendar.setUser(UserService.get(userUUID));
			}			
		}
		
		JSONArray timeFrames = json.has(Calendar.FIELD_TIME_FRAMES) ? (JSONArray) json.get(Calendar.FIELD_TIME_FRAMES) : null;
		if(timeFrames != null){
			calendar.getTimeFrames().clear();			
			for (int i = 0; i < timeFrames.length(); i++) {
				JSONObject tfJson = (JSONObject) timeFrames.get( i );
				if( tfJson!=null ){					
					String uuid = (tfJson.has(BasicData.FIELD_UUID) && !NULL.equals(tfJson.get(BasicData.FIELD_UUID))) ? (String) tfJson.get(BasicData.FIELD_UUID) : null;
					if( uuid !=null ){
						TimeFrame tf = TimeFrameManager.findTimeFrameByUUID(uuid);
						if( tf!=null ){
							calendar.getTimeFrames().add( tf );
						}
					}
				}
			}
		}
		
		JSONArray eventings = json.has(Calendar.FIELD_EVENTINGS) ? (JSONArray) json.get(Calendar.FIELD_EVENTINGS) : null;
		if (eventings != null) {
			calendar.getEventings().clear();
			for (int i = 0; i < eventings.length(); i++) {
				JSONObject evJson = (JSONObject) eventings.get( i );
				if( evJson!=null ){					
					String uuid = (evJson.has(BasicData.FIELD_UUID) && !NULL.equals(evJson.get(BasicData.FIELD_UUID))) ? (String) evJson.get(BasicData.FIELD_UUID) : null;
					if( uuid !=null ){
						Eventing ev = EventingManager.findEventingByUUID(uuid);
						if( ev!=null ){
							calendar.getEventings().add( ev );
						}
					}
				}				
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityParser#parseAndMerge(java.lang.Object, com.kh.ssme.model.ifc.Eventing)
	 */
	@Override
	public void parseAndMerge(Object o, Eventing eventing) throws JSONException {
		// private ParticipationTypeEnum participantType_;
		// private MeetingRequest request_;
		// private Calendar calendar_;
		// private Set<TimeFrameState> timeFrames_;
		// private Set<LocationState> locations_; // skipped
		// private StateEnum state_;

		JSONObject json = (JSONObject) o;

		String participantType = json.has(Eventing.FIELD_PARTICIPANT_TYPE) ? (String) json.get(Eventing.FIELD_PARTICIPANT_TYPE) : null;
		if (participantType != null) {
			eventing.setParticipantType(ParticipationTypeEnum.getType(participantType));
		}

		JSONObject request = json.has(Eventing.FIELD_REQUEST) ? (JSONObject) json.get(Eventing.FIELD_REQUEST) : null;
		if (request != null) {
			String requestUUID = (request.has(BasicData.FIELD_UUID) && !NULL.equals(request.get(BasicData.FIELD_UUID))) ? (String) request.get(BasicData.FIELD_UUID) : null;
			if ( requestUUID!=null && (eventing.getRequest() == null || !requestUUID.equalsIgnoreCase(eventing.getRequest().getUUID())) ) {
				eventing.setRequest(MeetingRequestManager.findMeetingRequestByUUID(requestUUID));
			}
		}

		JSONObject calendar = json.has(Eventing.FIELD_CALENDAR) ? (JSONObject) json.get(Eventing.FIELD_CALENDAR) : null;
		if (calendar != null) {
			String calendarUUID = (calendar.has(BasicData.FIELD_UUID) && !NULL.equals(calendar.get(BasicData.FIELD_UUID))) ? (String) calendar.get(BasicData.FIELD_UUID) : null;
			if ( calendarUUID!=null && (eventing.getCalendar() == null || !calendarUUID.equalsIgnoreCase(eventing.getCalendar().getUUID())) ) {
				eventing.setCalendar(CalendarManager.findCalendarByUUID(calendarUUID));
			}
		}

		JSONArray timeFrames = json.has(Eventing.FIELD_TIME_FRAME_STATES) ? (JSONArray) json.get(Eventing.FIELD_TIME_FRAME_STATES) : null;
		if (timeFrames != null) {
			eventing.getTimeFrameStates().clear();
			for (int i = 0; i < timeFrames.length(); i++) {
				JSONObject tfJson = (JSONObject) timeFrames.get( i );
				if( tfJson!=null ){					
					String uuid = (tfJson.has(BasicData.FIELD_UUID) && !NULL.equals(tfJson.get(BasicData.FIELD_UUID))) ? (String) tfJson.get( BasicData.FIELD_UUID ) : null;
					if( uuid !=null ){
						TimeFrameState tfs = TimeFrameStateManager.findTimeFrameStateByUUID(uuid);
						if( tfs!=null ){
							eventing.getTimeFrameStates().add( tfs );
						}
					}
				}
			}
		}
		
		// locationStates_

		String state = json.has(Eventing.FIELD_STATE) ? (String) json.get(Eventing.FIELD_STATE) : null;
		if (state != null) {
			eventing.setState(StateEnum.getState(state));
		}
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityParser#parseAndMerge(java.lang.Object, com.kh.ssme.model.ifc.Group)
	 */
	@Override
	public void parseAndMerge(Object o, Group group) throws JSONException {
		//private String name_ = "";
		//private Set<User> users_ = new TreeSet<User>();
		//private User owner_ = new User();	
		
		JSONObject json = (JSONObject)o;	
		
		String name = json.has(Group.FIELD_NAME) ? (String) json.get(Group.FIELD_NAME) : null;
		if(name!=null){
			group.setName(name);			
		}

		String description = json.has(Group.FIELD_DESCRIPTION) ? (String) json.get(Group.FIELD_DESCRIPTION) : null;
		if(description!=null){
			group.setDescription(description);			
		}		
		
		JSONArray users = json.has(Group.FIELD_USERS) ? (JSONArray) json.get(Group.FIELD_USERS) : null;
		if(users != null){
			group.getUsers().clear();			
			for (int i=0; i<users.length(); i++) {
				JSONObject userJson = (JSONObject) users.get( i );
				if( userJson!=null ){					
					String uuid = (userJson.has(BasicData.FIELD_UUID) && !NULL.equals(userJson.get(BasicData.FIELD_UUID))) ? (String) userJson.get( BasicData.FIELD_UUID ) : null;
					if( uuid !=null ){
						User usr = UserManager.findUserByUUID(uuid);
						if( usr!=null ){
							group.getUsers().add( usr );
						}
					}
				}				
			}
		}
		
		JSONObject owner = json.has(Group.FIELD_OWNER) ? (JSONObject) json.get(Group.FIELD_OWNER) : null;
		if(owner!=null){
			String ownerUUID = (owner.has(BasicData.FIELD_UUID) && !NULL.equals(owner.get(BasicData.FIELD_UUID))) ? (String) owner.get(BasicData.FIELD_UUID) : null;
			if ( ownerUUID!=null && (group.getOwner() == null || !ownerUUID.equalsIgnoreCase(group.getOwner().getUUID())) ) {
				group.setOwner(UserService.get(ownerUUID));
			}			
		}		
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityParser#parseAndMerge(java.lang.Object, com.kh.ssme.model.ifc.MeetingRequest)
	 */
	@Override
	public void parseAndMerge(Object o, MeetingRequest meetingRequest) throws JSONException {
		// private Date notSoonerThen_;
		// private Date notLaterThen_;
		// private StateEnum state_;
		// private TimeFrame decidedTime_;
		// private Location decidedPlace_;
		// private Eventing eventing_;
		// private String title;
		// private String description,

		JSONObject json = (JSONObject) o;

		String notSoonerThen = json.has(MeetingRequest.FIELD_NOT_SOONER_THEN) ? (String) json.get(MeetingRequest.FIELD_NOT_SOONER_THEN) : null;
		if (notSoonerThen != null) {
			meetingRequest.setNotSoonerThen(TimeUtil.parse(notSoonerThen));
		}

		String notLaterThen = json.has(MeetingRequest.FIELD_NOT_LATER_THEN) ? (String) json.get(MeetingRequest.FIELD_NOT_LATER_THEN) : null;
		if (notLaterThen != null) {
			meetingRequest.setNotLaterThen(TimeUtil.parse(notLaterThen));
		}

		String state = json.has(MeetingRequest.FIELD_STATE) ? (String) json.get(MeetingRequest.FIELD_STATE) : null;
		if (state != null) {
			meetingRequest.setState(StateEnum.getState(state));
		}

		JSONArray decidedTimes = json.has(MeetingRequest.FIELD_DECIDED_TIMES) ? (JSONArray) json.get(MeetingRequest.FIELD_DECIDED_TIMES) : null;
		if (decidedTimes != null) {
			meetingRequest.getDecidedTimes().clear();
			for (int i = 0; i < decidedTimes.length(); i++) {
				JSONObject tfJson = (JSONObject) decidedTimes.get( i );
				if( tfJson!=null ){					
					String uuid = (tfJson.has(BasicData.FIELD_UUID) && !NULL.equals(tfJson.get(BasicData.FIELD_UUID))) ? (String) tfJson.get( BasicData.FIELD_UUID ) : null;
					if( uuid !=null ){
						TimeFrame tf = TimeFrameManager.findTimeFrameByUUID(uuid);
						if( tf!=null ){
							meetingRequest.getDecidedTimes().add( tf );
						}
					}
				}
			}
		}

		JSONArray eventings = json.has(MeetingRequest.FIELD_DECIDED_TIMES) ? (JSONArray) json.get(MeetingRequest.FIELD_DECIDED_TIMES) : null;
		if (eventings != null) {
			meetingRequest.getEventings().clear();
			for (int i = 0; i < eventings.length(); i++) {
				JSONObject evJson = (JSONObject) eventings.get( i );
				if( evJson!=null ){					
					String uuid = (evJson.has(BasicData.FIELD_UUID) && !NULL.equals(evJson.get(BasicData.FIELD_UUID))) ? (String) evJson.get( BasicData.FIELD_UUID ) : null;
					if( uuid !=null ){
						Eventing ev = EventingManager.findEventingByUUID(uuid);
						if( ev!=null ){
							meetingRequest.getEventings().add( ev );
						}
					}
				}
			}
		}

		String title = json.has(MeetingRequest.FIELD_TITLE) ? (String) json.get(MeetingRequest.FIELD_TITLE) : null;
		if (title != null) {
			meetingRequest.setTitle(title);
		}

		String description = json.has(MeetingRequest.FIELD_DESCIPTION) ? (String) json.get(MeetingRequest.FIELD_DESCIPTION) : null;
		if (description != null) {
			meetingRequest.setDescription(description);
		}
	}


	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityParser#parseAndMerge(java.lang.Object, com.kh.ssme.model.ifc.RepeatType)
	 */
	@Override
	public void parseAndMerge(Object o, RepeatType repeatType) throws JSONException {
		//private Integer count_;
		//private Date from_;
		//private Date until_;
		//private RepeatTypeEnum repeatType_;
		//private Set<TimeFrame> timeFrames_;
		//private Integer frequency_;
		//private Integer flags_;
		
		JSONObject json = (JSONObject)o;
		
		Integer count = json.has(RepeatType.FIELD_COUNT) ? (Integer) json.get(RepeatType.FIELD_COUNT) : null;
		if (count != null){
			repeatType.setCount(count);
		}

		Integer frequency = json.has(RepeatType.FIELD_FREQUENCY) ? (Integer) json.get(RepeatType.FIELD_FREQUENCY) : null;
		if (frequency != null){
			repeatType.setFrequency(frequency);
		}		
		
		String from = json.has(RepeatType.FIELD_FROM) ? (String) json.get(RepeatType.FIELD_FROM) : null;
		if (from != null){
			repeatType.setFrom(TimeUtil.parse(from));
		}		
		
		String until = json.has(RepeatType.FIELD_UNTIL) ? (String) json.get(RepeatType.FIELD_UNTIL) : null;
		if (until != null){
			repeatType.setUntil(TimeUtil.parse(until));
		}
		
		String repeatTypeName = json.has(RepeatType.FIELD_REPEAT_TYPE) ? (String) json.get(RepeatType.FIELD_REPEAT_TYPE) : null;
		if (repeatTypeName != null){
			repeatType.setRepeatType(RepeatTypeEnum.getType(repeatTypeName));
		}
		
		JSONArray timeFrames = json.has(RepeatType.FIELD_TIME_FRAMES) ? (JSONArray) json.get(RepeatType.FIELD_TIME_FRAMES) : null;
		if(timeFrames != null){
			repeatType.getTimeFrames().clear();			
			for (int i = 0; i < timeFrames.length(); i++) {
				JSONObject tfJson = (JSONObject) timeFrames.get( i );
				if( tfJson!=null ){					
					String uuid = (tfJson.has(BasicData.FIELD_UUID) && !NULL.equals(tfJson.get(BasicData.FIELD_UUID))) ? (String) tfJson.get( BasicData.FIELD_UUID ) : null;
					if( uuid !=null ){
						TimeFrame tf = TimeFrameManager.findTimeFrameByUUID(uuid);
						if( tf!=null ){
							repeatType.getTimeFrames().add( tf );
						}
					}
				}				
			}
		}		

		Integer flags = json.has(RepeatType.FIELD_FLAGS) ? (Integer) json.get(RepeatType.FIELD_FLAGS) : null;
		if (flags != null) {
			repeatType.setFlags(flags);
	    }
		
		JSONObject prototype = json.has(RepeatType.FIELD_PROTOTYPE) ? (JSONObject) json.get(RepeatType.FIELD_PROTOTYPE) : null;
		if(prototype!=null){
			String prototypeUUID = (prototype.has(BasicData.FIELD_UUID) && !NULL.equals(prototype.get(BasicData.FIELD_UUID))) ? (String) prototype.get(BasicData.FIELD_UUID) : null;
			if ( prototypeUUID!=null && (repeatType.getPrototype() == null || !prototypeUUID.equalsIgnoreCase(repeatType.getPrototype().getUUID())) ) {
				repeatType.setPrototype(TimeFrameService.get(prototypeUUID));
			}			
		}			
	}	
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityMerger#parseAndMerge(java.lang.Object, com.kh.ssme.model.ifc.Role)
	 */
	@Override
	public void parseAndMerge(Object o, Role role) throws Exception {
		//private User user_;
		//private String login_;
		//private RoleEnum role_;	
		
		JSONObject json = (JSONObject)o;		
		
		JSONObject user = json.has(Role.FIELD_USER) ? (JSONObject) json.get(Role.FIELD_USER) : null;
		if(user!=null){
			String userUUID = (user.has(BasicData.FIELD_UUID) && !NULL.equals(user.get(BasicData.FIELD_UUID))) ? (String) user.get(BasicData.FIELD_UUID) : null;
			if ( userUUID!=null && (role.getUser() == null || !userUUID.equalsIgnoreCase(role.getUser().getUUID())) ) {
				role.setUser(UserService.get(userUUID));
			}			
		}	
		
		String login = json.has(Role.FIELD_LOGIN) ? (String) json.get(Role.FIELD_LOGIN) : null;		
		if( login!=null ){
			role.setLogin( login );			
		}
		
		String roleName = json.has(Role.FIELD_ROLE) ? (String) json.get(Role.FIELD_ROLE) : null;
		if (roleName != null){
			role.setRole(RoleEnum.getRole(roleName));
		}		
					
	}	

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityParser#parseAndMerge(java.lang.Object, com.kh.ssme.model.ifc.TimeFrame)
	 */
	@Override
	public void parseAndMerge(Object o, TimeFrame timeFrame) throws JSONException {
		//private Date from_;
		//private Date to_;
		//private RepeatType repeat_;
		//private String title_;	
		//private String description_;
		//private TimeFrameTypeEnum type_;
		//private PriorityEnum priority_;
		//private Location eventPlace_;	
		//private Location travelFrom_;	
		//private Location travelTo_; 
		//private StayPlace stayPlace_; 
		//private Calendar calendar_;
		//private TimeFrameState suggestedTimeState_;
		//private Set<MeetingRequest> meetingRequests
		//TreeSet<MeetingRequest>();
		
		JSONObject json = (JSONObject)o;
		
		String title = json.has(TimeFrame.FIELD_TITLE) ? (String) json.get(TimeFrame.FIELD_TITLE) : null;
		if(title != null){
			timeFrame.setTitle(title);
		}

		String description = json.has(TimeFrame.FIELD_DESCRIPTION) ? (String) json.get(TimeFrame.FIELD_DESCRIPTION) : null;
		if (description != null){
			timeFrame.setDescription(description);
		}

		String from = json.has(TimeFrame.FIELD_FROM) ? (String) json.get(TimeFrame.FIELD_FROM) : null;
		if (from != null){
			timeFrame.setFrom(TimeUtil.parse(from));
		}

		String to = json.has(TimeFrame.FIELD_TO) ? (String) json.get(TimeFrame.FIELD_TO) : null;
		if (to != null){
			timeFrame.setTo(TimeUtil.parse(to));
		}
		
		String prototype = json.has(TimeFrame.FIELD_PROTOTYPE) ? (String) json.get(TimeFrame.FIELD_PROTOTYPE) : null;
		if (prototype != null){
			timeFrame.setPrototype( prototype.trim().equalsIgnoreCase("true") ? Boolean.TRUE : Boolean.FALSE );
		}		
		
		String priority = json.has(TimeFrame.FIELD_PRIORITY) ? (String) json.get(TimeFrame.FIELD_PRIORITY) : null;
		if (priority != null){
			timeFrame.setPriority(PriorityEnum.getPriority(priority));
		}

		String timeFrameType = json.has(TimeFrame.FIELD_TYPE) ? (String) json.get(TimeFrame.FIELD_TYPE) : null;
		if (timeFrameType != null){
			timeFrame.setTimeFrameType(TimeFrameTypeEnum.getType(timeFrameType));
		}

		JSONObject calendar = json.has(TimeFrame.FIELD_CALENDAR) ? (JSONObject) json.get(TimeFrame.FIELD_CALENDAR) : null;
		if (calendar != null){
			String calendarUUID = (calendar.has(BasicData.FIELD_UUID) && !NULL.equals(calendar.get(BasicData.FIELD_UUID))) ? (String) calendar.get(BasicData.FIELD_UUID) : null;
			if ( calendarUUID!=null && (timeFrame.getCalendar() == null || !calendarUUID.equalsIgnoreCase(timeFrame.getCalendar().getUUID())) ) {
				timeFrame.setCalendar(CalendarService.get(calendarUUID));
			}			
		}
		JSONObject repeatType = json.has(TimeFrame.FIELD_REPEAT) ? (JSONObject) json.get(TimeFrame.FIELD_REPEAT) : null;
		if (repeatType != null){
			String repeatUUID = (repeatType.has(BasicData.FIELD_UUID) && !NULL.equals(repeatType.get(BasicData.FIELD_UUID))) ? (String) repeatType.get(BasicData.FIELD_UUID) : null;
			if ( repeatUUID!=null && (timeFrame.getRepeatType() == null || !repeatUUID.equalsIgnoreCase(timeFrame.getRepeatType().getUUID())) ) {
				timeFrame.setRepeatType(RepeatTypeService.get(repeatUUID));
			}
		}
		
		// eventPlace
		
		// travelFrom
		
		// travelTo
		
		// stayPlace			
		
		JSONObject meetingRequest = json.has(TimeFrame.FIELD_MEETING_REQUEST) ? (JSONObject) json.get(TimeFrame.FIELD_MEETING_REQUEST) : null;
		if (meetingRequest != null) {
			String meetingUUID = (meetingRequest.has(BasicData.FIELD_UUID) && !NULL.equals(meetingRequest.get(BasicData.FIELD_UUID))) ? (String) meetingRequest.get(BasicData.FIELD_UUID) : null;
			if ( meetingUUID!=null && (timeFrame.getMeetingRequest() == null || !meetingUUID.equalsIgnoreCase(timeFrame.getMeetingRequest().getUUID())) ) {
				timeFrame.setMeetingRequest(MeetingRequestManager.findMeetingRequestByUUID(meetingUUID));
			}
		}
		
		JSONObject suggestedTimeState = json.has(TimeFrame.FIELD_SUGGESTED_TIME_STATE) ? (JSONObject) json.get(TimeFrame.FIELD_SUGGESTED_TIME_STATE) : null;
		if (suggestedTimeState != null) {
			String stateUUID = (suggestedTimeState.has(BasicData.FIELD_UUID) && !NULL.equals(suggestedTimeState.get(BasicData.FIELD_UUID))) ? (String) suggestedTimeState.get(BasicData.FIELD_UUID) : null;
			if ( stateUUID!=null && (timeFrame.getSuggestedTimeState() == null || !stateUUID.equalsIgnoreCase(timeFrame.getSuggestedTimeState().getUUID())) ) {
				timeFrame.setSuggestedTimeState(TimeFrameStateManager.findTimeFrameStateByUUID(stateUUID));
			}
		}		
		
		JSONObject parent = json.has(TimeFrame.FIELD_PARENT) ? (JSONObject) json.get(TimeFrame.FIELD_PARENT) : null;
		if (parent != null) {
			String parentUUID = (parent.has(BasicData.FIELD_UUID) && !NULL.equals(parent.get(BasicData.FIELD_UUID))) ? (String) parent.get(BasicData.FIELD_UUID) : null;
			if ( parentUUID!=null && (timeFrame.getParent() == null || !parentUUID.equalsIgnoreCase(timeFrame.getParent().getUUID())) ) {
				timeFrame.setParent(RepeatTypeManager.findRepeatTypeByUUID(parentUUID));
			}
		}			
			
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityParser#parseAndMerge(java.lang.Object, com.kh.ssme.model.ifc.TimeFrameState)
	 */
	@Override
	public void parseAndMerge(Object o, TimeFrameState timeFrameState) throws JSONException {
		// private StateEnum state_;
		// private TimeFrame timeFrame_;
		// private Eventing eventing_;

		JSONObject json = (JSONObject) o;

		String state = json.has(TimeFrameState.FIELD_STATE) ? (String) json.get(TimeFrameState.FIELD_STATE) : null;
		if (state != null) {
			timeFrameState.setState(StateEnum.getState(state));
		}

		JSONObject timeFrame = json.has(TimeFrameState.FIELD_TIME_FRAME) ? (JSONObject) json.get(TimeFrameState.FIELD_TIME_FRAME) : null;
		if (timeFrame != null) { 
			String tfUUID = (timeFrame.has(BasicData.FIELD_UUID) && !NULL.equals(timeFrame.get(BasicData.FIELD_UUID))) ? (String) timeFrame.get(BasicData.FIELD_UUID) : null;
			if ( tfUUID!=null && (timeFrameState.getTimeFrame() == null || !tfUUID.equalsIgnoreCase(timeFrameState.getTimeFrame().getUUID())) ) {
				timeFrameState.setTimeFrame(TimeFrameManager.findTimeFrameByUUID(tfUUID));
			}
		}

		JSONObject eventing = json.has(TimeFrameState.FIELD_EVENTING) ? (JSONObject) json.get(TimeFrameState.FIELD_EVENTING) : null;
		if (eventing != null) {
			String eventingUUID = (eventing.has(BasicData.FIELD_UUID) && !NULL.equals(eventing.get(BasicData.FIELD_UUID))) ? (String) eventing.get(BasicData.FIELD_UUID) : null;
			if ( eventingUUID!=null && (timeFrameState.getEventing() == null || !eventingUUID.equalsIgnoreCase(timeFrameState.getEventing().getUUID())) ) {
				timeFrameState.setEventing(EventingManager.findEventingByUUID(eventingUUID));
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityParser#parseAndMerge(java.lang.Object, com.kh.ssme.model.ifc.User)
	 */
	@Override
	public void parseAndMerge(Object o, User user) throws JSONException {
		//private String login_ = "";		
		//private String name_ = "";	
		//private String surname_ = "";	
		//private String mail_ = "";
		//private String mobile_ = "";			
		//private Set<GroupEntity> groups = new TreeSet<GroupEntity>();
		//private Set<StayPlace> stayPlaces_ = new TreeSet<StayPlace>();
		// private Set<com.kh.ssme.model.ifc.Calendar> calendars;
		//private Set<Group> ownedGroups_ = new TreeSet<Group>();
		//private List<Role> roles_ = new ArrayList<Role>();
		
		JSONObject json = (JSONObject)o;
		
		String login = json.has(User.FIELD_LOGIN) ? (String) json.get(User.FIELD_LOGIN) : null;		
		if( login!=null ){
			user.setLogin( login );			
		}
		
		String name = json.has(User.FIELD_NAME) ? (String) json.get(User.FIELD_NAME) : null;
		if( name!=null ){
			user.setName( name );			
		}
		
		String surname = json.has(User.FIELD_SURNAME) ? (String) json.get(User.FIELD_SURNAME) : null;
		if( surname!=null ){
			user.setSurname( surname );			
		}

		String mail = json.has(User.FIELD_MAIL) ? (String) json.get(User.FIELD_MAIL) : null;
		if( mail!= null ){
			user.setMail( mail );			
		}
		
		String mobile = json.has(User.FIELD_MOBILE) ? (String) json.get(User.FIELD_MOBILE) : null;
		if( mobile!=null ){
			user.setMobile( mobile );			
		}		
					
		JSONArray groups = json.has(User.FIELD_GROUPS) ? json.getJSONArray(User.FIELD_GROUPS) : null;			
		if(groups!=null){
			user.getGroups().clear();	
			for (int i=0; i<groups.length(); i++) {		
				JSONObject group = (JSONObject)groups.get( i );	
				if( group!=null ){
					String groupUUID = (group.has(BasicData.FIELD_UUID) && !NULL.equals(group.get(BasicData.FIELD_UUID))) ? (String) group.get( BasicData.FIELD_UUID ) : null;
					if(groupUUID!=null){
						Group gr = GroupManager.findGroupByUUID(groupUUID);
						if(gr!=null){
							user.getGroups().add(gr);
						}
					}
				}
			}
		}		
	
		JSONArray ownedGroups = json.has(User.FIELD_OWNED_GROUPS) ? json.getJSONArray(User.FIELD_OWNED_GROUPS) : null;
		if(ownedGroups!=null){
			user.getOwnedGroups().clear();	
			for (int i=0; i<ownedGroups.length(); i++) {	
				JSONObject ownedGroup = (JSONObject)ownedGroups.get( i );					
				if( ownedGroup!=null ){
					String groupUUID = (ownedGroup.has(BasicData.FIELD_UUID) && !NULL.equals(ownedGroup.get(BasicData.FIELD_UUID))) ? (String) ownedGroup.get( BasicData.FIELD_UUID ) : null;
					if(groupUUID!=null){
						Group gr = GroupManager.findGroupByUUID(groupUUID);
						if(gr!=null){						
							user.getOwnedGroups().add(gr);
						}
					}
				}											
			}
		}	
		
		JSONArray roles = json.has(User.FIELD_ROLES) ? json.getJSONArray(User.FIELD_ROLES) : null;
		if(roles!=null){
			user.getRoles().clear();	
			for (int i=0; i<roles.length(); i++) {					
				JSONObject role = (JSONObject)roles.get( i );	
				if(role!=null){
					String roleUUID = (role.has(BasicData.FIELD_UUID) && !NULL.equals(role.get(BasicData.FIELD_UUID))) ? role.getString(BasicData.FIELD_UUID) : null;
					if(roleUUID!=null){
						Role rl = RoleManager.findRoleByUUID(roleUUID);
						user.getRoles().add(rl);
					}
				}				
			}
		}		

		JSONArray calendars = json.has(User.FIELD_CALENDARS) ? (JSONArray) json.get(User.FIELD_CALENDARS) : null;
		if(calendars!=null){
			user.getCalendars().clear();
			for (int i = 0; i > calendars.length(); i++) {
				JSONObject calendar = (JSONObject) calendars.get(i);
				if(calendar!=null){
					String calendarUUID = (calendar.has(BasicData.FIELD_UUID) && !NULL.equals(calendar.get(BasicData.FIELD_UUID))) ? (String) calendar.getString(BasicData.FIELD_UUID) : null;
					if(calendarUUID!=null){
						Calendar cal = CalendarManager.findCalendarByUUID(calendarUUID);
						user.getCalendars().add(cal);
					}
				}				
			}
		}

		// stayPlaces
	}
	
	//-------------------------------------------
	// 4future
	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityParser#parseAndMerge(java.lang.Object, com.kh.ssme.model.ifc.StayPlace)
	 */
	@Override
	public void parseAndMerge(Object o, StayPlace stayPlace) throws JSONException {
		/* empty block */
	}
	
	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityParser#parseAndMerge(java.lang.Object, com.kh.ssme.model.ifc.Location)
	 */
	@Override
	public void parseAndMerge(Object o, Location location) throws JSONException {
		/* empty block */
	}

	/* (non-Javadoc)
	 * @see com.kh.ssme.rest.parsers.AbstractEntityParser#parseAndMerge(java.lang.Object, com.kh.ssme.model.ifc.LocationState)
	 */
	@Override
	public void parseAndMerge(Object o, LocationState locationState) throws JSONException {
		/* empty block */
	}	
	//-------------------------------------------	

}
