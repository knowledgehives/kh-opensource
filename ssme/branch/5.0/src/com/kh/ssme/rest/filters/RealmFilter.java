/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest.filters;

import com.kh.ssme.manage.UserManager;
import com.kh.ssme.rest.util.Params;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author Michal Szopinski
 *
 */
public class RealmFilter implements Filter {

	protected static Logger logger_ = LoggerFactory.getLogger(RealmFilter.class);		
	
	
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		
		try{
		
			// put user entity of logged user to session if it is not present
			HttpServletRequest hreq = (req instanceof HttpServletRequest) ? (HttpServletRequest)req : null;			
			if(hreq !=null && hreq.getUserPrincipal()!=null){					
				System.out.println(" FILTER : "+hreq.getRequestURL().toString()+" : ");
				if( hreq.getSession().getAttribute( Params.SessionAttributes.USER_ENTITY.paramName() ) == null ){
					hreq.getSession().setAttribute( Params.SessionAttributes.USER_ENTITY.paramName(), UserManager.findUserByLogin(hreq.getUserPrincipal().getName()).getUUID() );
				} else {
					/* empty block */
				}				
				//System.out.println(" FILTER : " + RealmBase.Digest(hreq.getUserPrincipal().getName(), "SHA", null) + " <- " + hreq.getUserPrincipal().getName());	
			} else {
				System.out.println(" FILTER : "+req.getLocalAddr()+" from "+req.getRemoteAddr());
			}								
			
		} catch (Exception e){
			logger_.error("RealmFilter : ", e);
		} finally {
			chain.doFilter(req, resp);
		}
				
	}


	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		/* empty block */
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		/* empty block */
	}	

}
