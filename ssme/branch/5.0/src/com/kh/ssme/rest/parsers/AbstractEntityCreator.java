/*
 *	SmartSchedule.me - Semantic Calendar
 *	Copyright (C) 2009,  Knowledge Hives sp. z o.o.
 *	
 *	Contribution from: Gdansk University of Technology, Poland
 *	Invented by: Sebastian R. Kruk
 *	Implemented by: Michał Szopiński, Mariusz Cygan 
 *	
 *	This program is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU Affero General Public License as
 *	published by the Free Software Foundation, either version 3 of the
 *	License, or (at your option) any later version.
 *	
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU Affero General Public License for more details.
 *	
 *	You should have received a copy of the GNU Affero General Public License
 *	along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *	
 */
package com.kh.ssme.rest.parsers;

import com.kh.ssme.model.ifc.*;
import org.json.JSONObject;


/**
 * @author Michal Szopinski
 * Interface with methods for creating object for respond from entity object. 
 */
public interface AbstractEntityCreator {
	
	/**
	 * Create object for respond from common BasicData entity 
	 * @param basicData
	 * @param json
	 * @param recursionLevel
	 */
	public void create(BasicData basicData, JSONObject json, int recursionLevel) throws Exception;
	
	/**
	 * Create object for respond from Calendar entity 
	 * @param calendar
	 * @param json
	 * @param recursionLevel
	 */
	public void create(Calendar calendar, JSONObject json, int recursionLevel) throws Exception;
	
	/**
	 * Create object for respond from Eventing entity 
	 * @param eventing
	 * @param json
	 * @param recursionLevel
	 */
	public void create(Eventing eventing, JSONObject json, int recursionLevel) throws Exception;
	
	/**
	 * Create object for respond from Group entity 
	 * @param group
	 * @param json
	 * @param recursionLevel
	 */
	public void create(Group group, JSONObject json, int recursionLevel) throws Exception;
	
	/**
	 * Create object for respond from Location entity 
	 * @param location
	 * @param json
	 * @param recursionLevel
	 */
	public void create(Location location, JSONObject json, int recursionLevel) throws Exception;
	
	/**
	 * Create object for respond from LocationState entity 
	 * @param locationState
	 * @param json
	 * @param recursionLevel
	 */
	public void create(LocationState locationState, JSONObject json, int recursionLevel) throws Exception;
	
	/**
	 * Create object for respond from MeetingRequest entity 
	 * @param meetingRequest
	 * @param json
	 * @param recursionLevel
	 */
	public void create(MeetingRequest meetingRequest, JSONObject json, int recursionLevel) throws Exception;
	
	/**
	 * Create object for respond from RepeatType entity 
	 * @param repeatType
	 * @param json
	 * @param recursionLevel
	 */
	public void create(RepeatType repeatType, JSONObject json, int recursionLevel) throws Exception;
	
	/**
	 * Create object for respond from Role entity 
	 * @param role
	 * @param json
	 * @param recursionLevel
	 */
	public void create(Role role, JSONObject json, int recursionLevel) throws Exception;
		
	/**
	 * Create object for respond from StayPlace entity 
	 * @param stayPlace
	 * @param json
	 * @param recursionLevel
	 */
	public void create(StayPlace stayPlace, JSONObject json, int recursionLevel) throws Exception;	
	
	/**
	 * Create object for respond from TimeFrame entity 
	 * @param timeFrame
	 * @param json
	 * @param recursionLevel
	 */
	public void create(TimeFrame timeFrame, JSONObject json, int recursionLevel) throws Exception;	
	
	/**
	 * Create object for respond from TimeFrameState entity 
	 * @param timeFrameState
	 * @param json
	 * @param recursionLevel 
	 */
	public void create(TimeFrameState timeFrameState, JSONObject json, int recursionLevel) throws Exception;	
	
	/**
	 * Create object for respond from User entity 
	 * @param user
	 * @param json
	 * @param recursionLevel
	 */
	public void create(User user, JSONObject json, int recursionLevel) throws Exception;			
}
