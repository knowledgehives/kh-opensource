/** ***********************************************************************************
 * JS Util class
 */
Util = Class.create();
Util.prototype = {	

}

//static
Util.functionRegexp = /^function/gi;

//static
Util.listAttributes = function(object) {
	var ret = "{ "+object+" -> " ;
	try{
		var regex = /./gi;
		for(att in object){
			regex.compile(Util.functionRegexp);
			if(regex.test(object[att])){
				ret += "["+att+":FUNCTION]";
			} else {
				ret += "["+att+":"+object[att]+"]";
			}
		}
	} catch(e){
		ret += '[EXCEPTION:'+e+']';
	}
	return ret+"}";
}
/**************************************************************************************/




/** ***********************************************************************************
 * JS Logger class
 */
Logger = Class.create();
Logger.prototype = {	

}

//static
Logger.debug = function(message) {
	Logger._print(message, "[DEBUG] : "+message);
}

//static
Logger.error = function(message) {
	Logger._print(message, '<span style="color: red;">[ERROR] : '+message+'</span>');
}

//static
Logger._print = function(rawMessage, message) {
	try{
		if( $('logger') ){
			$('logger').innerHTML += message + "<br/>";
		}
	} catch (e) {}
	try {
        console.debug(rawMessage);
    } catch (e) {};
}
/**************************************************************************************/




/** ***********************************************************************************
 * Global handling
 */
Global = Class.create();
Global.prototype = {	

}

Global.preload = function() {
	Logger.debug("Global.preload()");
	
	// logout button
	var aElement = $$('#logout_section input.logout')[0];	
	if(aElement){		
		aElement.onclick = Global.onLogoutClick.bindAsEventListener(this, aElement);
	}
	
	// logout YES button	
	if($('global_logout_yes')){
		$('global_logout_yes').onclick = function(){
			Global.redirectToLogOut();
		};		
	}
	
	// logout NO button	
	if($('global_logout_no')){
		$('global_logout_no').onclick = facebox.close();		
	}
	
	// login button	
	if($('global_login')){
		$('global_login').onclick = function(){
			Global.redirectToLogIn();
		};			
	}
	
	// register button
	if($('global_register')){
		$('global_register').onclick = function(){
			window.location = CONTEXT_PATH+'/register.jsp';
		};			
	}	
		
}

Global.redirectToLogIn = function(){
	window.location = CONTEXT_PATH+'/user/';	
}
Global.redirectToLogOut = function(){
	window.location = CONTEXT_PATH+'/logout';
}

Global.onLogoutClick = function(event, element) {
	 Event.stop(event);			 
	 facebox.buttonClick_handler(element);		
}

Event.observe(window, "load", Global.preload);	
/**************************************************************************************/



/** ***********************************************************************************
 * Global handling
 */

LogIn = Class.create();
LogIn.prototype = { }
LogIn.sendLogin = function(login, password) {
    var json = "j_username="+login+"&j_password="+password+"&flexApp";

    var req = new Ajax.Request(CONTEXT_PATH+"/j_security_check",
    {
        method: "post",
        contentType: "application/x-www-form-urlencoded",
        postBody: json,
        onSuccess: function(response){
            LogIn.loginSuccess();
        },
        onFailure: function(response, problem){
            if ( response.responseJSON === null ){
                response.responseJSON = response.responseText.evalJSON();
            }
            LogIn.loginFail( response.responseJSON.errorMessage );
        }
    });
}
LogIn.loginSuccess = function() {
    location.reload();
}
LogIn.loginFail = function(message) {
    var flexApp = Flex.getFlexApp('LogUserIn');
    if(flexApp){
        flexApp.loginFail(message);
    }
}					
/**************************************************************************************/