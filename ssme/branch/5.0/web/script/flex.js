/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/** ***********************************************************************************
 * Flex handling
 */
Flex = Class.create();

Flex.prototype = {

}

Flex.embed = function() {
	Logger.debug("Flex.embed()");

	var width      = "700";
	var height     = "400";
	var bgcolor	   = "#999999";
	var url        = location.protocol.replace(":","")+"://"+location.host+"/";

	var params =
	{
		base                : "." ,
		align               : "center",
		allowScriptAccess   : "always",
		bgcolor             : "red",
		devicefont          : "true",
		loop                : "false",
		menu                : "false",
		play                : "false",
		quality             : "best",
		salign              : "lt",
		scale               : "noscale",
		swliveconnect       : "false",
		wmode				: "transparent" // looks better than "window"
	};
	var attributes =
	{
		id                  : "UserRegister",
		name                : "UserRegisterName"
	}


	$$('div.flexContainer').each(
        function(el){
            var ID = el.id;
            var animation = ID.split('_')[1];

            var appWidth = el.getStyle('width');
            var appHeight = el.getStyle('height');
            Logger.debug("ID : ["+ID+"] READ appWidth:["+appWidth+"]; appHeight:["+appHeight+"]");
            appWidth = (appWidth != null && (typeof(appWidth ) != 'undefined')) ? appWidth : width;
            appHeight = (appHeight != null && (typeof(appHeight ) != 'undefined')) ? appHeight : height;


            var appBgColor = el.getStyle('background-color');
            Logger.debug("ID : ["+ID+"]; READ appBgColor:["+appBgColor+"];");
            appBgColor = (appBgColor != null && (typeof(appBgColor ) != 'undefined')) ? appBgColor : height

            var flashvars = {
                    jsloggingenabled    : "true",
                    baseURL             : url,
                    //appWidth            : appWidth,
                    //appHeight           : appHeight,
                    bgcolor				: appBgColor
                };
            Logger.debug("ID : ["+ID+"]; FLASHVARS jsloggingenabled:["+"true"+"]; " +
                                                            "baseURL:["+url+"]; " +
                                                            "appWidth:["+width+"]; " +
                                                            "appHeight:["+height+"]; " +
                                                            "bgcolor:["+appBgColor+"]; ");

            Logger.debug("ID : ["+ID+"]; animation:["+animation+"]; path:["+(CONTEXT_PATH+'/flex/'+animation+'.swf')+"]");
            swfobject.embedSWF(CONTEXT_PATH+'/flex/'+animation+'.swf',	// SWF
                        ID,              			// content to be replaced
                        appWidth, appHeight,        // width/height
                        "9.0.0",                    // required flash player version
                        "expressInstall.swf",       // expressInstall.swf for upgrades
                        flashvars,
                        params,
                        {	id:animation, name:animation+'Name'	},
                        null);

        }.bind(this)
    );
}

Flex.getFlexApp = function(appName){
    if (navigator.appName.indexOf("Microsoft")!=-1){
        return window[appName];
    } else {
        return document[appName];
    }
}

Flex.queryApp = function(appName, queryString){
    var app = Flex.getFlexApp( appName );
    try{
        app.processDrilldown( queryString );
    } catch(e){
        Logger.error( e );    
    }
}

Event.observe(window, "load", Flex.embed);
/**************************************************************************************/
