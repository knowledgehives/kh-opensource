<%@ tag language="java" pageEncoding="UTF-8"%>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" href="${pageContext.request.contextPath}/css/ssme.css" type="text/css" media="screen" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/facebox.css" type="text/css" media="screen" />	

<script type="text/javascript" src="${pageContext.request.contextPath}/script/prototype.js" type="text/javascript"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/script/effects.js" type="text/javascript"></script>	
<script type="text/javascript" src="${pageContext.request.contextPath}/script/dragdrop.js" type="text/javascript"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/script/controls.js" type="text/javascript"></script>	
<script type="text/javascript" src="${pageContext.request.contextPath}/script/facebox.js" type="text/javascript"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/script/swfobject.js" type="text/javascript"></script>
	
<script type="text/javascript" src="${pageContext.request.contextPath}/script/util.js" type="text/javascript"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/script/toggleButton.js" type="text/javascript"></script>
	
<script type="text/javascript" src="${pageContext.request.contextPath}/script/user.js" type="text/javascript"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/script/group.js" type="text/javascript"></script>	
<script type="text/javascript" src="${pageContext.request.contextPath}/script/flex.js" type="text/javascript"></script>

<script type='text/javascript'>	
	/**
	 * Global variables
	 */
	 var CONTEXT_PATH = '${pageContext.request.contextPath}';
  
</script>
	
