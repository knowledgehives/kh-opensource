/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
import com.kh.ssme.util.HashArray;

public class RequestMonthCanvas extends MonthGridCanvas {

        public function RequestMonthCanvas() {
            super();
        }

        private var oldWidth:Number, oldHeight:Number;
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{

            if(visible){    // necessary only if visible

                // call first in order to get grid drawn correctly
                super.updateDisplayList(unscaledWidth, unscaledHeight);

                // call repaint only if dimensions were changed
                if((unscaledWidth - oldWidth) != 0 || (unscaledHeight - oldHeight) != 0 || recreateEvents){
                    //there was change in width or height since last call

                    oldWidth = unscaledWidth;
                    oldHeight = unscaledHeight;
                    repaint();                    

                }

            }
        }

        protected override function getMonthDisplayItemImpl():IMonthtDisplayItem {
            return new RequestDayField();
        }

        public function selectByDate(date:Date):void{
            var rdf:RequestDayField = eventsMapByDate.get( date.toDateString() );
            if( rdf!=null ){
                rdf.selectItem( rdf );                
            }
        }

        public function markAllActiveDays(daysList:HashArray):void{
            if(daysList && eventsMapByDate){
                var rfd:RequestDayField;               
                for(var key:String in eventsMapByDate){
                    rfd = (eventsMapByDate.get(key) as RequestDayField);
                    if(rfd) rfd.isIncluded = daysList.containsKey( key );
                }
            }
        }

    }

}