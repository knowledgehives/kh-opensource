/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {

import com.kh.ssme.util.Color;
import com.kh.ssme.util.SsmeEvent;

    import flash.events.Event;

    import flash.events.MouseEvent;

    import mx.containers.VBox;
    import mx.controls.Button;
    import mx.controls.Label;
    import mx.core.UIComponent;

    public class RequestDayField extends DayField implements IMonthtDisplayItem {

        private var title_:Label;
        private var container_:VBox;
        private var includedButton_:Button;

        public function RequestDayField() {
            super();

            this.visible = this.includeInLayout = true;
            setStyle("borderThickness", 2);
            setStyle("borderColor", 0xBBBBFF);
            this.verticalScrollPolicy = "off";
            this.horizontalScrollPolicy = "off";

            container_ = new VBox();
            container_.percentWidth = 100;
            container_.percentHeight = 100;
            container_.setStyle("verticalGap",0);
            container_.setStyle("textAlign","center");
            container_.verticalScrollPolicy = "off";
            container_.horizontalScrollPolicy = "off";
            this.addChild( container_ );            

            title_ = new Label();
            title_.visible = title_.includeInLayout = true;            
            title_.percentWidth = 100;
            title_.height = 20;
            title_.setStyle("textAlign", "center");
            container_.addChild( title_ );

            includedButton_ = new Button();
            includedButton_.width = 28;
            includedButton_.height = 28;
            includedButton_.styleName = "requestField";
            includedButton_.visible = false;
            includedButton_.includeInLayout = true;
            //container_.addChild( includedButton_ );
            this.addChild( includedButton_ );

            addEventListener( MouseEvent.CLICK, clicked )

        }

        public function set isIncluded(value:Boolean):void{
             if(includedButton_) includedButton_.visible = value;
        }
        public function get isIncluded():Boolean{
            return (includedButton_ && includedButton_.visible);
        }

        /**
         * currently selected day
         */
        public override function set currentDay(value:Date):void{
            super.currentDay = value;
            title_.text = dateFormatter.format( currentDay );
        }

        public function clicked(event:Event):void{
            selectItem( this );
        }

        public override function selectItem(item:UIComponent):void {
            super.selectItem( item );
            setStyle("backgroundColor", Color.getInstance(dayType_.color).makeDarker(5).RGB);
            setStyle("borderThickness", 4);
            setStyle("borderColor", Color.getInstance(0xBBBBFF).makeDarker(5).RGB);
            invalidateDisplayList();
            dispatchEvent( new SsmeEvent( SsmeEvent.ITEM_SELECTED, { timeFrameEntity:null, selectedItem:this } ) );             
        }
        public override function deselectedItem():void {
            super.deselectedItem( );
            setStyle("backgroundColor", dayType_.color);
            setStyle("borderThickness", 2);
            setStyle("borderColor", 0xBBBBFF);
            invalidateDisplayList();
        }

        protected override function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
            super.updateDisplayList( unscaledWidth, unscaledHeight );

            container_.x = this.borderThickness_;
            container_.y = this.borderThickness_;
            container_.width = this.width - this.borderThickness_*2;
            container_.height = this.height - this.borderThickness_*2;

            includedButton_.x = (this.width - includedButton_.width) / 2;
            includedButton_.y = container_.y+title_.height+2;

//            this.toolTip = "{THIZ:"+this+"; width:"+this.width+", height:"+this.height+"}";
//            var t:String = "{x:"+includedButton_.x+", y:"+includedButton_.y+", width:"+includedButton_.width+", height:"+includedButton_.height+"}";
//            if(includedButton_.parent)  t+= "{PARENT:"+includedButton_.parent+" : x:"+includedButton_.parent.x+", y:"+includedButton_.parent.y
//                    +", width:"+includedButton_.parent.width+", height:"+includedButton_.parent.height+"}";
//            includedButton_.toolTip = t;

        }


    }

}