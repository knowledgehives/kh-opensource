/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {

    import mx.containers.Canvas;
    import mx.controls.Label;

    public class HourVLine extends Canvas {

        public function HourVLine() {
            super();
        }

        private var labels:Array = [];

        public var topTextMargin:int = 0;

        public var bottomTextMargin:int = 0;

        override protected function createChildren():void{
            super.createChildren();

            removeAllChildren();

            var score:int;
            var el:Label;
            for(var i:int=0; i<=24; i++){
                el = new Label();

                score = 0x20;
                if(i%24 == 0)    score |= 0x01;
                if(i%12 == 0)    score |= 0x02;
                if(i%6 == 0)     score |= 0x04;
                if(i%3 == 0)     score |= 0x08;
                if(i%2 == 0)     score |= 0x10;

                el.text = ((i<10)?("0"+i):(i))+":00";
                el.setStyle("textAlign","center");
                el.setStyle("color", 0xff0000);
                el.visible = el.includeInLayout = false;//((i%24) == 0);
                labels.push( { score:score, element:el } );
                this.addChild( el );
            }

        }

        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
            super.updateDisplayList( unscaledWidth, unscaledHeight );

            graphics.clear();   //erase old drawings            

            if(labels.length>0){ // added

                var textHeight:int = (labels[0].element as Label).textHeight;                // label text height
                var count:int = Math.floor( unscaledHeight / (textHeight*1.5) );    // determine number of labels
                var mark:int = 0x01;
                if(count <= 3)              mark = 0x02;
                else if (count <= 5)        mark = 0x04;
                else if (count <= 9)        mark = 0x08;
                else if (count <= 12)       mark = 0x10;
                else /*if (count <= 24)*/   mark = 0x20;

                var verticalSpan:Number = unscaledHeight-topTextMargin-bottomTextMargin;
                var iter:Number = topTextMargin - textHeight;
                var step:Number = verticalSpan*0.04;

                    graphics.lineStyle(1, 0xff0000,1.0,true);                

                for(var i:int=0; i<=24; i++){
                    //labels[i].element.visible = (labels[i].score & mark) > 0;
                    labels[i].element.x = 0;
                    labels[i].element.y = iter;
                    labels[i].element.width = unscaledWidth;
                    labels[i].element.height = Math.floor( step );

                    graphics.moveTo(0, iter);
                    graphics.lineTo(0+unscaledWidth-5, iter);// - inBorderThickness*0.5);

                    trace("["+i+"]["+mark+"] : ("+labels[i].element.x+","+labels[i].element.y+") :" +
                          " [ "+labels[i].element.width+" x "+labels[i].element.height+" ] --> " +
                          " VIS:"+(labels[i].score & mark)+" "+labels[i].score+" ");
                    iter+=step;                    
                }
                
            }


        }

    }

}