/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
    import com.kh.ssme.util.DateProperty;
    import com.kh.ssme.util.DateUtil;
    import com.kh.ssme.util.DayEventsFieldTypeEnum;
    import com.kh.ssme.util.HashArray;
    import com.kh.ssme.util.Logger;
    import com.kh.ssme.util.SsmeEvent;

    import flash.events.Event;

    import mx.core.UIComponent;

    [Event(name="ssme_itemSelected", type="flash.events.Event")]

    public class MonthGridCanvas extends GridCanvas {
        
        public function MonthGridCanvas() {
            super();
        }

        protected var currentDay_:Date;
        public var numberOfDays:int;
        public var numberOfWeeks:int;
        public var monthBeginDay:DateUtil;
        public var monthEndDay:DateUtil;
        public function set currentDay(value:Date):void{
            if(value){
                currentDay_ = value;
                monthBeginDay = (new DateUtil(value)).monthBegin();
                monthEndDay = monthBeginDay.clone().add(DateProperty.MONTH, 1).dayEnd();

                numberOfDays = monthEndDay.diff( monthBeginDay, DateProperty.DAY );
                // numberOfWeeks == CEIL(((7 - dayOfWeek) + numberOfDays) / 7)
                numberOfWeeks = Math.ceil((7 - ((monthBeginDay.getProperty(DateProperty.DAY)+7)%7) + numberOfDays) / 7.0 );

                recreateEvents = true;
                repaint();                
            }
        }
        public function get currentDay():Date{
            return currentDay_;
        }


        /**
         * Repaints control
         */
        public function repaint():void{
            createEvents();
            repaintEvents();            
        }


        protected var recreateEvents:Boolean = false;    
        protected var eventsMap:HashArray = new HashArray();
        protected var eventsMapByDate:HashArray = new HashArray();

        /**
         * Create display object for each TimeFrameEntity in dataProvider
         */
        protected function getMonthDisplayItemImpl():IMonthtDisplayItem {
            throw Error("Method need to be overriden!");    
        }

        protected function createEvents():void{

            // in order to save time we recreate EventFields
            // only if it is necessary
            if(recreateEvents && currentDay){
                var i:int;

                // remove old events objects
                for(i = 0; i<eventsMap.size; i++){
                    if( eventsMap.getValueAt(i) ){
                        this.removeChild( eventsMap.getValueAt(i) );         
                    }
                }
                eventsMap.clear();
                eventsMapByDate.clear();


                // create IMonthtDisplayItem instance for each day in month
                var ev:IMonthtDisplayItem;
                var monthIter:DateUtil = monthBeginDay.clone();
                var complement:int = ((monthBeginDay.getProperty(DateProperty.DAY)+6)%7);
                for(i=1; i<=numberOfDays; i++, monthIter.add( DateProperty.DATE, 1 )) {

                    ev = getMonthDisplayItemImpl();
                    ev.currentDay = monthIter.date;
                    ev.dayOfWeek = (monthIter.getProperty( DateProperty.DAY ) + 6) % 7;
                    ev.weekOfMonth = (monthIter.getProperty( DateProperty.DATE ) + complement - 1) / 7;
                    ev.weekOfMonth = (complement == 0) ? ev.weekOfMonth+1 : ev.weekOfMonth; // add 1 if month starts at monday, it's for better display
                    ev.dayType = (ev.dayOfWeek < 5) ? DayEventsFieldTypeEnum.COMMON : (ev.dayOfWeek == 5) ? DayEventsFieldTypeEnum.SATURDAY : DayEventsFieldTypeEnum.SUNDAY_HOLIDAY;
                    ev.setStyle("cornerRadius", cornerRadius_);

                    Logger.debug( " complement:["+complement+"] date:["+monthIter.getProperty( DateProperty.DATE )+"] day:"+monthIter.getProperty( DateProperty.DAY )
                            +" dayOfWeek"+ev.dayOfWeek+" weekOfMonth:"+ev.weekOfMonth );
                    addChild( ev as UIComponent );
                    ev.addEventListener( SsmeEvent.ITEM_SELECTED, itemClicked );                    
                    eventsMap.put(i-1, ev);
                    eventsMapByDate.put(ev.currentDay.toDateString(),ev);
                }
                recreateEvents = false;
            }

        }

        /**
         * Calculates rank and coordinates for each TimeFrame display object
         */
        protected function repaintEvents():void{
            var ev:IMonthtDisplayItem;
            var i:int;

            for(i = 0; i<eventsMap.size; i++){
                ev = eventsMap.getValueAt( i );
                ev.x = columnsPosition[0][ev.dayOfWeek] + (innerBorderThicknesses_[0] * 0.5);
                ev.y = rowsPosition[0][ev.weekOfMonth] + (innerBorderThicknesses_[0] * 0.5);
                ev.width = Math.max( (columnsPosition[0][ev.dayOfWeek+1] - columnsPosition[0][ev.dayOfWeek] - innerBorderThicknesses_[0]), 0);
                ev.height = Math.max( (rowsPosition[0][ev.weekOfMonth+1] - rowsPosition[0][ev.weekOfMonth] - innerBorderThicknesses_[0]), 0);
                ev.invalidateSize();
                ev.invalidateDisplayList();
            }
        }

        //-----------------------------------------
        // event selection
        private var selectedItem_:UIComponent;
        public function set selectedItem(item:UIComponent):void {
            selectedItem_ = item;
        }
        public function get selectedItem():UIComponent {
            return selectedItem_;
        }
        // "re-dispatch"
        public function itemClicked(event:Event):void{
            dispatchEvent( (event as SsmeEvent).cloneEvent() );
            selectedItem_ = (event as SsmeEvent).eventProperties[SsmeEvent.ITEM_SELECTED__ITEM_PROP];
        }
        //-----------------------------------------       
        
    }

}