/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
import com.kh.ssme.model.entity.CalendarEntity;

import com.kh.ssme.util.Logger;
import com.kh.ssme.util.SsmeEvent;

    import flash.events.MouseEvent;

    import mx.containers.HBox;
    import mx.controls.Button;
    import mx.controls.Label;

    [Event(name="ssme_calendarListItemRemoved", type="com.kh.ssme.util.SsmeEvent")]

    //public class CalendarListItem extends HBox {
    public class CalendarListItem extends RoundedCornerContainer {

        private var label_:Label;
        private var removeButton_:Button;
        public function CalendarListItem() {
            super();

            this.setStyle( "verticalAlign", "middle" );
            this.setStyle( "backgroundColor", 0xBBBBFF );
            this.setStyle( "borderThickness", 1 );
            this.setStyle( "borderColor", 0x4444FF );
            this.setStyle( "cornerRadius", 10 );

            label_ = new Label();
            label_.height = 20;
            label_.visible = label_.includeInLayout = true;
            this.addChild( label_ );

            removeButton_ = new Button();
            removeButton_.styleName = "removeButton";
            removeButton_.width = 32;
            removeButton_.height = 32;
            removeButton_.visible = removeButton_.includeInLayout = true;            
            removeButton_.addEventListener( MouseEvent.CLICK, onRemoveClicked );
            this.addChild( removeButton_ );
        }

        private function onRemoveClicked(event:MouseEvent):void {
            if( parent ){
                parent.removeChild( this );                
                dispatchEvent(new SsmeEvent("ssme_calendarListItemRemoved", { data:this.data_ }));
            }
        }

        override protected function measure():void {
            super.measure();
            label_.width = this.width - removeButton_.width - (2*cornerRadius_);
        }

        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
            super.updateDisplayList( unscaledWidth, unscaledHeight );

            label_.x = cornerRadius_;
            label_.y = (unscaledHeight - label_.height)/2;

            removeButton_.x = unscaledWidth - removeButton_.width - (cornerRadius_*0.5);
            removeButton_.y = (unscaledHeight - removeButton_.height)/2;

//            Logger.debug("label_.x:"+label_.x+
//                            "; label_.y:"+label_.y+
//                            "; label_.width:"+label_.width+
//                            "; label_.height:"+label_.height+
//                            "; removeButton_.x:"+removeButton_.x+
//                            "; removeButton_.y:"+removeButton_.y+"\n"+
//                            "unscaledWidth:["+unscaledWidth+"] "+
//                            "this.width:["+this.width+"] "+
//                            "removeButton_.width:["+removeButton_.width+"] "+
//                            "cornerRadius:["+cornerRadius_+"] "
//                    );
        }

        private var data_:Object;
        public override function set data(value:Object):void{
            data_ = value;
            label_.text = data_['name'];
            removeButton_.visible = (CalendarEntity.ALL_EVENTS_NAME != data_['name']);
        }
        public override function get data():Object{
            return data_;
        }

        public function get calendarName():String{
            return (data_) ? data_['name'] : null;
        }

        public function get uuid():String{
            return (data_) ? data_['uuid'] : null;
        }



    }

}