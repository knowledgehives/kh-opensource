/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
    import com.kh.ssme.util.HashArray;
    import mx.containers.Canvas;
    import mx.controls.Label;

    public class VerticalLabelsList extends GridCanvas {

        public function VerticalLabelsList() {
        }

        public var showHourLabels:Boolean = false;

        private var hourLabels:HashArray = new HashArray();

        override protected function createChildren():void {
            super.createChildren();

            if (showHourLabels) {
                var score:int;
                var el:Label;
                for (var i:int = 0; i <= 24; i++) {
                    el = new Label();
                    el.id = el.text = ((i < 10) ? ("0" + i) : (i)) + ":00";

                    if (!hourLabels.containsKey(el.id)) {

                        this.addChild(el);

                        el.setStyle("textAlign", "right");
                        el.setStyle("color", 0x000000);
                        el.setStyle("fontSize", 11);
                        el.visible = el.includeInLayout = false;//((i%24) == 0);
                        score = 0x20;
                        if (i % 24 == 0)    score |= 0x01;
                        if (i % 12 == 0)    score |= 0x02;
                        if (i % 6 == 0)     score |= 0x04;
                        if (i % 3 == 0)     score |= 0x08;
                        if (i % 2 == 0)     score |= 0x10;

                        hourLabels.put(el.id, { score:score, element:el });

                    }

                }
            }

        }

        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{

            // do not erase old drawings!
            graphics.clear();

            if(showHourLabels && hourLabels.size>0){ // added

                var textHeight:int = (hourLabels.getValueAt(0).element as Label).textHeight;    // label text height
                var count:int = Math.floor( unscaledHeight / (textHeight*1.5) );    // determine number of labels
                var mark:int = 0x01;
                if(count <= 3)              mark = 0x02;
                else if (count <= 5)        mark = 0x04;
                else if (count <= 9)        mark = 0x08;
                else if (count <= 12)       mark = 0x10;
                else /*if (count <= 24)*/   mark = 0x20;

                var item:Object;
                var inBorderThicknesses:Array = getStyle_("innerBorderThicknesses", [ 1 ]);
                var ystep:Number = (unscaledHeight - (2*outerBorderThickness_+2*verticalMargin_) - (23*inBorderThicknesses[0])) / 24;
                var y:Number = outerBorderThickness_+verticalMargin_;
                //graphics.lineStyle(1, 0x0000ff, 0.7, true);   // debug
                for (var row:int = 0; row <=24; row++ ){
                    //graphics.drawRoundRect(0, y - (textHeight*0.66), unscaledWidth, Math.min( Math.floor( ystep ), textHeight), 0, 0);// debug

                    item = hourLabels.getValueAt(row);
                    item.element.visible = item.element.includeInLayout = true;//(item.score & mark) > 0;
                    item.element.x = 0;
                    item.element.y = Math.floor( y - (textHeight*0.66) );
                    item.element.width = unscaledWidth;
                    item.element.height = Math.min( Math.floor( ystep ), textHeight);

                    y += (ystep + inBorderThicknesses[0]);
                }

            }

        }

    }

}