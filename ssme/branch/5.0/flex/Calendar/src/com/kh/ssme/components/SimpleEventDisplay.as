/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
    import com.kh.ssme.model.entity.TimeFrameEntity;
    import com.kh.ssme.util.Color;

    import mx.controls.TextArea;
    import mx.formatters.DateFormatter;

    // will be set with background color
    [Exclude(name="color", kind="style")]    
    [Exclude(name="backgroundColor", kind="style")]
    [Exclude(name="backgroundAlpha", kind="style")]
    [Exclude(name="backgroundDisabledColor", kind="style")]
    [Exclude(name="borderColor", kind="style")]
    [Exclude(name="borderThickness", kind="style")]

    [Style(name="parentBackgroundColor", type="uint", format="Color", inherit="no")]    

    public class SimpleEventDisplay extends TextArea {

        public var timeFormatter:DateFormatter = new DateFormatter();

        

        public function SimpleEventDisplay() {
            super();
            this.percentWidth = 100;
            this.percentHeight = 100;
            this.wordWrap = true;
            this.editable = false;
            this.enabled = false;
            this.visible = true;
            this.includeInLayout = true;
            this.verticalScrollPolicy = "off";
            this.horizontalScrollPolicy = "off";
            timeFormatter.formatString = "JJ:NN";

            this.setStyle("backgroundAlpha", 0);
            this.setStyle("borderColor", 0x000000);
            this.setStyle("borderThickness", 0);            
        }

        public override function setStyle(styleProp:String, newValue:*):void{

            if(styleProp == "parentBackgroundColor"){
                var clr:uint = Color.negativeTextColor(newValue);
                this.setStyle("color", clr);
                this.setStyle("disabledColor", clr);
            }

            super.setStyle(styleProp, newValue);
        }

    }

}