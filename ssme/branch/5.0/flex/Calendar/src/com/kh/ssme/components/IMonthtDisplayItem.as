/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
    import com.kh.ssme.util.DayEventsFieldTypeEnum;
    import com.kh.ssme.util.HashArray;

    public interface IMonthtDisplayItem extends IEventDisplayObject {

        function set calendarsColorsMap(value:HashArray):void;

        function get calendarsColorsMap():HashArray;

        function set dataProvider(value:HashArray):void;

        function get dataProvider():HashArray;

        function set currentDay(value:Date):void;

        function get currentDay():Date;

        function set dayOfWeek(value:int):void;

        function get dayOfWeek():int;

        function set weekOfMonth(value:int):void;

        function get weekOfMonth():int;

        function set dayType(value:DayEventsFieldTypeEnum):void;

        function get dayType():DayEventsFieldTypeEnum;

        function getStyle(styleProp:String):*;

        function setStyle(styleProp:String, newValue:*):void


        function set x(x:Number):void;

        function set y(y:Number):void;

        function set width(width:Number):void;

        function set height(height:Number):void;

        function invalidateSize():void;

        function invalidateDisplayList():void;

    }
}