/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.util {

    public class DayEventsFieldTypeEnum {

        //----------------------------------------
        // common
		public static const COMMON:DayEventsFieldTypeEnum           = new DayEventsFieldTypeEnum("common",   0xFFFFFF);
        public static const SATURDAY:DayEventsFieldTypeEnum         = new DayEventsFieldTypeEnum("saturday", 0xE5E5FF);
        public static const SUNDAY_HOLIDAY:DayEventsFieldTypeEnum   = new DayEventsFieldTypeEnum("sunday",   0xFFCC66);


		private static const VALUES:Object = new Object();
        {
            //static initialization
            VALUES[COMMON.ID] = COMMON;
            VALUES[SATURDAY.ID] = SATURDAY;
            VALUES[SUNDAY_HOLIDAY.ID] = SUNDAY_HOLIDAY;
        }

		public static function find(id:String):LoggingComponentsEnum
		{
			return VALUES[id];
		}


		// ---------- private parts ---------------
		private var id_:String;
        private var color_:uint;
		public function DayEventsFieldTypeEnum(id:String, color:uint)
		{
			this.id_ = id;
            this.color_ = color;
		}

		public function get ID():String
		{
			return this.id_;
		}
		public function get color():uint
		{
			return this.color_;
		}

		public function toString():String
		{
			return "["+this.id_+":"+this.color_+"]";
		}
		public function equals(type:DayEventsFieldTypeEnum):Boolean
		{
			return (type!=null && this.id_ == type.ID);
		}


    }

}