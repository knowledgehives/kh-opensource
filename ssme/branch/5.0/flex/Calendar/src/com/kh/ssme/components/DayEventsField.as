/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {

    import com.kh.ssme.model.entity.TimeFrameEntity;
    import com.kh.ssme.util.Color;
    import com.kh.ssme.util.DayEventsFieldTypeEnum;
    import com.kh.ssme.util.HashArray;
    import com.kh.ssme.util.SsmeEvent;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import mx.containers.VBox;
    import mx.controls.Button;
    import mx.controls.Label;
    import mx.core.UIComponent;
    import mx.events.ToolTipEvent;
    import mx.formatters.DateFormatter;

    [Event(name="ssme_itemSelected", type="flash.events.Event")]

    public class DayEventsField extends DayField implements IMonthtDisplayItem {

        private var container_:VBox;
        private var title_:Label;
        private var innerContainer_:VBox;
        private var bottomTitle_:Label;
        private var timeFormatter:DateFormatter = new DateFormatter();    
    
        public function DayEventsField() {
            super();
            this.visible = this.includeInLayout = true;
            setStyle("borderThickness", 2);
            setStyle("borderColor", 0xBBBBFF);
            this.verticalScrollPolicy = "off";
            this.horizontalScrollPolicy = "off";

            container_ = new VBox();
            container_.percentWidth = 100;
            container_.percentHeight = 100;
            container_.setStyle("verticalGap",0);
            container_.setStyle("textAlign","center");               
            container_.verticalScrollPolicy = "off";
            container_.horizontalScrollPolicy = "off";            
            this.addChild( container_ );

            title_ = new Label();
            title_.percentWidth = 100;
            title_.height = 20;
            title_.setStyle("textAlign", "center");
            container_.addChild( title_ );
            
            innerContainer_ = new VBox();
            innerContainer_.percentWidth = 100;
            innerContainer_.percentHeight = 100;
            innerContainer_.setStyle("verticalGap",1);
            innerContainer_.setStyle("paddingLeft",5);
            innerContainer_.setStyle("paddingRight",5);
            innerContainer_.setStyle("textAlign","center");            
            innerContainer_.verticalScrollPolicy = "off";
            innerContainer_.horizontalScrollPolicy = "off";            
            container_.addChild( innerContainer_ );

            bottomTitle_ = new Label();
            bottomTitle_.percentWidth = 100;
            bottomTitle_.height = 20;
            bottomTitle_.setStyle("textAlign", "center");
            container_.addChild( bottomTitle_ );

            timeFormatter.formatString = "JJ:NN";

        }

        /**
         * currently selected day
         */
        public override function set currentDay(value:Date):void{
            super.currentDay = value;
            title_.text = dateFormatter.format( currentDay );
        }        

        public override function selectItem(item:UIComponent):void {
            super.selectItem( item );
            (item as EventBox).selectMe();
        }
        public override function deselectedItem():void {
            if(selectedItem_) (selectedItem_ as EventBox).deselectMe();
            super.deselectedItem();            
        }
        public function itemClicked(event:Event):void{
            if( event.target != selectedItem_ ){
                deselectedItem( );
                selectItem( event.target as UIComponent );
                dispatchEvent( new SsmeEvent( SsmeEvent.ITEM_SELECTED, { timeFrameEntity:event.target.data, selectedItem:this } ) );
            }
        }    

        private var dataProviderArray_:Array;
        public override function set dataProvider(value:HashArray):void{
            innerContainer_.removeAllChildren();

            if(value){
                super.dataProvider = value;
                bottomTitle_.text = "+"+value.size;

                dataProviderArray_ = value.getAsArray();
                dataProviderArray_.sort( sortTimeFrames );
                var timeframe:TimeFrameEntity, o:Object, tempButton:EventBox, color:uint;
                for each(o in dataProviderArray_){
                    timeframe = (o.value as TimeFrameEntity);
                    if(timeframe){
                        color = calendarsColorsMap[ timeframe.calendarUUID ];
                        tempButton = new EventBox();
                        tempButton.backgroundSelectedColors = [
                                    Color.getInstance(color).makeLighter(24).RGB,
                                    Color.getInstance(color).makeLighter(16).RGB,
                                    Color.getInstance(color).makeLighter(24).RGB,
                                    Color.getInstance(color).makeLighter(16).RGB
                                ];
                        tempButton.backgroundDeselectedColors = [
                                    Color.getInstance(color).makeLighter(34).desaturate(30).RGB,
                                    Color.getInstance(color).makeLighter(26).desaturate(30).RGB,
                                    Color.getInstance(color).makeLighter(34).desaturate(30).RGB,
                                    Color.getInstance(color).makeLighter(26).desaturate(30).RGB
                                ];
                        tempButton.borderSelectedColor = Color.getInstance(timeframe.priority.borderColor).makeLighter(20).RGB;
                        tempButton.borderDeselectedColor = Color.getInstance(timeframe.priority.borderColor).makeLighter(30).desaturate(30).RGB;
                        tempButton.height = 15;
                        tempButton.width = 120;
                        tempButton.label = timeFormatter.format( timeframe.from )+" - "+timeFormatter.format( timeframe.to )+" "+timeframe.description;
                        tempButton.data = timeframe;
                        tempButton.deselectMe();
                        tempButton.addEventListener( MouseEvent.CLICK, itemClicked );

                        innerContainer_.addChild( tempButton );
                    }
                }

                invalidateDisplayList();
                innerContainer_.invalidateDisplayList();
            }
        }
            
        private function sortTimeFrames(a:Object, b:Object):int {
            var result:Number = 0;
            if(a && b && a.value && b.value && a.value is TimeFrameEntity && b.value is TimeFrameEntity){
                result = (a.value as TimeFrameEntity).from.time - (b.value as TimeFrameEntity).from.time;
            }
            if(result == 0){
                return (a.value as TimeFrameEntity).to.time - (b.value as TimeFrameEntity).to.time;
            }
            return result;
        }
        private function createEventTooltip(event:ToolTipEvent, timeframe:TimeFrameEntity):void {
            event.toolTip = new EventTooltip();
            (event.toolTip as EventTooltip).data = timeframe;
        }

    
        protected override function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
            super.updateDisplayList( unscaledWidth, unscaledHeight );

            container_.x = this.borderThickness_;
            container_.y = this.borderThickness_;
            container_.width = this.width - this.borderThickness_*2;
            container_.height = this.height - this.borderThickness_*2;

            var i:int = 0, displayed:int = 0, sumHeight:Number=(title_.height + bottomTitle_.height), button:Button;
            for(i=0; i<innerContainer_.numChildren; i++){
                button = innerContainer_.getChildAt( i ) as Button;
                sumHeight += button.height;
                button.visible = button.includeInLayout = sumHeight < unscaledHeight;
                button.width = unscaledWidth - 8;//Math.max( 0, unscaledWidth-button.width/2 );
                if(sumHeight < unscaledHeight)  displayed++;
            }
            bottomTitle_.text = (dataProvider_ && dataProvider_.size > displayed) ? "+"+(dataProvider_.size - displayed) : "";              
        }

    }

}