/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
    import com.kh.ssme.util.Color;

    import flash.events.Event;
    import flash.geom.Matrix;
    import mx.containers.Canvas;
    import mx.controls.Label;
    import mx.controls.TextInput;


    public class PasswordChecker extends Canvas {

        public function PasswordChecker() {
            super();
        }

        //-------------------------------------------
        // logic
        private var dataProvider_:TextInput;
        public function get dataProvider():TextInput{
            return dataProvider_;    
        }
        public function set dataProvider(value:TextInput):void{
            if(value != dataProvider_ && dataProvider_){
                dataProvider_.removeEventListener( flash.events.Event.CHANGE, calculatePasswordStrength );    
            }
            dataProvider_ = value;
            dataProvider_.addEventListener( flash.events.Event.CHANGE, calculatePasswordStrength );
        }

        // assuming 1k attempts per second password would be breaken after:
        private var ranges:Array = [ { min:0,   max:20, text:"Very weak" },       // <9;20)bits = very weak     ~16 minutes
                                     { min:20,  max:25, text:"Weak" },            // <20;25)bits = weak         ~9 godzin
                                     { min:25,  max:30, text:"Good" },            // <25;30)bits = good         ~1 week
                                     { min:30,  max:40, text:"Strong" },          // <30;40)bits = strong       ~20 years
                                     { min:40,  max:60, text:"Very strong" },     // <40;60)bits = very strong  ~20M years
                                     { min:60,  max:100, text:"Perfect" },        // <60;+oo)bits = perfect     more than 20M years
                                   ];

        // entropy of single character (http://en.wikipedia.org/wiki/Password_strength)
        private var types:Array = [ 3.322, //Arabic numerals (0�9) (e.g. PIN) 	                - 3.322 bits
                                    4.700, //Case insensitive Latin alphabet (a-z or A-Z) 	    - 4.700 bits
                                    5.170, //Case insensitive alphanumeric (a-z or A-Z, 0�9)    - 5.170 bits
                                    5.700, //Case sensitive Latin alphabet (a-z, A-Z) 	        - 5.700 bits
                                    5.954, //Case sensitive alphanumeric (a-z, A-Z, 0�9) 	    - 5.954 bits
                                    6.555, //All ASCII printable characters 	                - 6.555 bits
                                  ];
        
        private var passwordStrength:Number = 0;
        private function calculatePasswordStrength(event:Event):void{

            if(!dataProvider_)  return;

            var newPasswordStrength:Number = 0;
            var text:String = dataProvider_.text;

            // determine type
            if ( text.match( /^[0-9]*$/ ) ){ // Arabic numerals
                newPasswordStrength = types[0];
            } else if ( text.match( /^[a-z]*$/ ) || text.match( /^[A-Z]*$/ ) ){ // Case insensitive
                newPasswordStrength = types[1];
            } else if ( text.match( /^[a-z0-9]*$/ ) || text.match( /^[A-Z0-9]*$/ )){ // Case insensitive alphanumeric
                newPasswordStrength = types[2];
            } else if ( text.match( /^[a-zA-Z]*$/ ) ){    // Case sensitive 
                newPasswordStrength = types[3];
            } else if ( text.match( /^\w*$/ ) ){ // Case sensitive alphanumeric
                newPasswordStrength = types[4];
            } else if ( text.match( /^.*$/ ) ){   // All ASCII printable characters 
                newPasswordStrength = types[5];
            }
            newPasswordStrength *= dataProvider_.length;
            newPasswordStrength = Math.min( newPasswordStrength, 99.9 );   

            // reset component with new value if needed
            if(newPasswordStrength != passwordStrength){
                passwordStrength = newPasswordStrength;
                // determine label
                for each(var o:Object in ranges){
                    if(o['min'] <= newPasswordStrength && newPasswordStrength < o['max']){
                        labelField.text = o.text;
                    }
                }
                invalidateDisplayList();           
            }
        }
        //-------------------------------------------        



        //-------------------------------------------
        // display
        public var leftDisplayMargin:int = 0;
        public var rightDisplayMargin:int = 0;
        public var topDisplayMargin:int = 0;
        public var bottomDisplayMargin:int = 0;
        public var border:int = 0;
        private var rectWidth:Number = 0;
        private var rectHeight:Number = 0;
        private var adjustedLeftMargin:Number = 0;
        private var leftMarginAdjustmentValue:Number = 1; // flex do not substract cell left border from width

        private var labelField:Label;

        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
            super.updateDisplayList(unscaledWidth, unscaledHeight);

            rectWidth = unscaledWidth - leftDisplayMargin - rightDisplayMargin - leftMarginAdjustmentValue-(2*border);
            rectHeight = unscaledHeight - topDisplayMargin - bottomDisplayMargin - (2*border);
            adjustedLeftMargin = leftDisplayMargin + leftMarginAdjustmentValue; // flex do not substract cell left border from width
            graphics.clear();   //erase old drawings

            if(rectWidth>0 && rectHeight>0){
                //var ratios:Array = null;
                var matr:Matrix = new Matrix();
                var cornerRad:Number = this.getStyle("cornerRadius");

                matr.createGradientBox(rectWidth, rectHeight, 0);

                // border
                graphics.beginGradientFill(
                        "linear",
                        [Color.makeSimpleDarker(0xFF0000,10), Color.makeSimpleDarker(0xFFFF00,10), Color.makeSimpleDarker(0x00FF00,10)], [100,100,100],
                        [0, 100, 200], matr
                        );
                graphics.drawRoundRect(
                        adjustedLeftMargin, topDisplayMargin,
                        rectWidth+(2*border), rectHeight+(2*border), cornerRad, cornerRad);
                graphics.endFill();

                //inner
                graphics.beginGradientFill(
                        "linear",
                        [0xFF0000, 0xFFFF00, 0x00FF00], [100,100,100],
                        [0, 100, 200], matr
                        );
                graphics.drawRoundRect(
                        adjustedLeftMargin+border, topDisplayMargin+border,
                        (rectWidth * (passwordStrength/100)), rectHeight, cornerRad, cornerRad);
                graphics.endFill();
            }

            labelField.x = adjustedLeftMargin + border + 5;
            labelField.y = 0;//topDisplayMargin + border;
            labelField.setStyle("fontSize", rectHeight-4);//Math.floor(rectHeight*0.66));
                        
        }

        //-------------------------------------------   
        override protected function createChildren():void{
            super.createChildren();

            labelField = new Label();
            labelField.text = "";
            labelField.setStyle("color", 0xFFFFFF);
            
            addChild( labelField );

        }
        //-------------------------------------------        

    }
}