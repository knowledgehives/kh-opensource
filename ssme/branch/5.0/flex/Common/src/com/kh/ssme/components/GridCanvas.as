/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {

import flash.display.CapsStyle;
    import flash.display.LineScaleMode;

    [Style(name="innerBorderColors", type="Array", format="Color")]
    [Style(name="innerBorderThicknesses", type="Array", format="Number")]
    [Style(name="innerBorderAlphas", type="Array", format="Number")]
    [Style(name="outerBorderColor", type="Number", format="Color")]
    [Style(name="outerBorderThickness", type="Number", format="Number")]
    [Style(name="outerBorderAlpha", type="Number", format="Number")]
    [Style(name="verticalMargin", type="Number", format="Number")]
    [Style(name="leftHorizontalMargin", type="Number", format="Number")]
    [Style(name="rightHorizontalMargin", type="Number", format="Number")]    
    public class GridCanvas extends StyledCanvas {

        /**
         * Number of columns
         */
        private var columns_:Array = [ 1 ] ;  
        public function get columns():Array{
            return columns_;
        }
        public function set columns(value:Array):void{
            if(value.length>0){
                columns_ = value
                invalidateDisplayList();
            }
        }
        public var columnsPosition:Array;              


        /**
         * Number of rows
         */
        private var rows_:Array = [ 1 ];
        public function get rows():Array{
            return rows_;
        }
        public function set rows(value:Array):void{
            if(value.length>0){
                rows_ = value
                invalidateDisplayList();
            }
        }
        public var rowsPosition:Array;          


        public function GridCanvas() {
            super();
        }

        //---------------------------------------------
        // STYLE
        public function set outerBorderThickness_(value:Number):void{
            this.setStyle("outerBorderThickness", value);
        }
        public function get outerBorderThickness_():Number{
            return this.getStyle_("outerBorderThickness", 1);
        }
        public function set outerBorderColor_(value:uint):void{
            this.setStyle("outerBorderColor", value);
        }
        public function get outerBorderColor_():uint{
            return this.getStyle_("outerBorderColor", 0x000000);
        }
        public function set outerBorderAlpha_(value:Number):void{
            this.setStyle("outerBorderAlpha", value);
        }
        public function get outerBorderAlpha_():Number{
            return this.getStyle_("outerBorderAlpha", 1.0);
        }
        public function set innerBorderThicknesses_(value:Array):void{
            this.setStyle("innerBorderThicknesses", value);
        }
        [ArrayElementType("int")]        
        public function get innerBorderThicknesses_():Array{
            return this.getStyle_("innerBorderThicknesses", [ 0 ]);
        }
        public function set innerBorderColors_(value:Array):void{
            this.setStyle("innerBorderColors", value);
        }
        [ArrayElementType("uint")]        
        public function get innerBorderColors_():Array{
            return this.getStyle_("innerBorderColors", [ 0x000000 ]);
        }
        public function set innerBorderAlphas_(value:Array):void{
            this.setStyle("innerBorderAlphas", value);
        }
        [ArrayElementType("Number")]        
        public function get innerBorderAlphas_():Array{
            return this.getStyle_("innerBorderAlphas", [1.0]);
        }
        public function set cornerRadius_(value:Number):void{
            this.setStyle("cornerRadius", value);
        }
        public function get cornerRadius_():Number{
            return this.getStyle_("cornerRadius", 0);
        }
        public function set verticalMargin_(value:Number):void{
            this.setStyle("verticalMargin", value);
        }
        public function get verticalMargin_():Number{
            return this.getStyle_("verticalMargin", 0);
        }
        public function set leftHorizontalMargin_(value:Number):void{
            this.setStyle("leftHorizontalMargin", value);
        }
        public function get leftHorizontalMargin_():Number{
            return this.getStyle_("leftHorizontalMargin", 0);
        }
        public function set rightHorizontalMargin_(value:Number):void{
            this.setStyle("rightHorizontalMargin", value);
        }
        public function get rightHorizontalMargin_():Number{
            return this.getStyle_("rightHorizontalMargin", 0);
        }
        //---------------------------------------------

        /**
         * Allow forcing omission on graphics.clear()
         */
        protected var forceNoClear:Boolean = false;

        /**
         * Renders the grid under other components
         * @param unscaledWidth
         * @param unscaledHeight
         */
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
            var outBorderThickness:Number = outerBorderThickness_;
            var outBorderColor:uint = outerBorderColor_;
            var outBorderAlpha:Number = outerBorderAlpha_;

            var inBorderThicknesses:Array = innerBorderThicknesses_;
            var inBorderColors:Array = innerBorderColors_;
            var inBorderAlphas:Array = innerBorderAlphas_;

            var cornerRadius:Number = cornerRadius_;
            var verticalMargin:Number = verticalMargin_;
            var leftHorizontalMargin:Number = leftHorizontalMargin_;
            var rightHorizontalMargin:Number = rightHorizontalMargin_;


            if(!forceNoClear) graphics.clear();   // conditionally erase old drawings
            forceNoClear = false;

            // background
            if (this.getStyle("backgroundColor")){
                graphics.lineStyle(0, this.getStyle("backgroundColor"),this.getStyle("backgroundAlpha"),
                                   true,                    //pixel hinting
                                   LineScaleMode.NORMAL,    //scale
                                   CapsStyle.NONE);
                graphics.beginFill(this.getStyle("backgroundColor"), this.getStyle("backgroundAlpha"));
                graphics.drawRoundRect(outBorderThickness+leftHorizontalMargin, outBorderThickness+verticalMargin,  // x, y
                        unscaledWidth-(2*outBorderThickness+leftHorizontalMargin+rightHorizontalMargin),            // width
                        unscaledHeight-(2*outBorderThickness+2*verticalMargin),                                     // height
                        cornerRadius, cornerRadius);
                graphics.endFill();
            }

            //border
            if (outBorderThickness > 0)
            {
                graphics.lineStyle(outBorderThickness,
                                   outBorderColor,
                                   outBorderAlpha,
                                   true,                    //pixel hinting
                                   LineScaleMode.NORMAL,    //scale
                                   CapsStyle.NONE);
                graphics.drawRoundRect(leftHorizontalMargin+outBorderThickness*0.5, verticalMargin+outBorderThickness*0.5,  // x, y
                        unscaledWidth-outBorderThickness-leftHorizontalMargin-rightHorizontalMargin,                        // width,
                        unscaledHeight-outBorderThickness-2*verticalMargin,                                                 // height
                        cornerRadius, cornerRadius);
            }



            // inner border
            var inBorderThickness:int, xTempPos:Number, yTempPos:Number;
            columnsPosition = [];
            rowsPosition = [];
            for (var i:int = 0; i<inBorderThicknesses.length; i++){
                // for each border level
                if (inBorderThicknesses[i] > 0){
                    inBorderThickness = inBorderThicknesses[i];
                    graphics.lineStyle(inBorderThickness,inBorderColors[i],inBorderAlphas[i],
                                       true, //pixel hinting
                                       LineScaleMode.NORMAL, //scale
                                       CapsStyle.NONE);

                    var tempColumnsPos:Array = [], tempRowsPos:Array = [];                      
                    if(columns_.length>=i){
                        var xstep:Number = (unscaledWidth - (2*outBorderThickness+leftHorizontalMargin+rightHorizontalMargin)  - (columns_[i] - 1)*inBorderThickness) / columns_[i];
                        // left border position
                        var x:Number = outBorderThickness+leftHorizontalMargin;
                        xTempPos = x - inBorderThickness*0.5;
                        tempColumnsPos.push( xTempPos );
                        // draw column lines
                        for ( var col:int = 1; col < columns_[i]; col++ ){
                            x += (xstep + inBorderThickness);
                            xTempPos = x - inBorderThickness*0.5;
                            graphics.moveTo(xTempPos, outBorderThickness + verticalMargin);
                            graphics.lineTo(xTempPos, unscaledHeight - outBorderThickness - verticalMargin);
                            tempColumnsPos.push( xTempPos );
                        }
                        // right border position
                        x += (xstep + inBorderThickness);
                        xTempPos = x - inBorderThickness*0.5;
                        tempColumnsPos.push( xTempPos );
                    }
                    if(rows_.length>=i){
                        var ystep:Number = (unscaledHeight - (2*outBorderThickness+2*verticalMargin) - (rows_[i] - 1)*inBorderThickness) / rows_[i];
                        // top border position                        
                        var y:Number = outBorderThickness+verticalMargin;
                        yTempPos = y - inBorderThickness*0.5;
                        tempRowsPos.push( yTempPos );
                        // draw rows lines
                        for (var row:int = 1; row < rows_[i]; row++ ){
                            y += (ystep + inBorderThickness);
                            yTempPos = y - inBorderThickness*0.5;
                            graphics.moveTo(outBorderThickness + leftHorizontalMargin, yTempPos);
                            graphics.lineTo(unscaledWidth - outBorderThickness - rightHorizontalMargin, yTempPos);
                            tempRowsPos.push( yTempPos );
                        }
                        // bottom border position
                        y += (ystep + inBorderThickness);
                        yTempPos = y - inBorderThickness*0.5;
                        tempRowsPos.push( yTempPos );
                    }
                    columnsPosition.push( tempColumnsPos );
                    rowsPosition.push( tempRowsPos );                    
                }
            }

        }

    }

}