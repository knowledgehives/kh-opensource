/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {

    import flash.display.DisplayObjectContainer;
    import flash.display.Sprite;
    import flash.events.Event;

    import mx.containers.VBox;
    import mx.controls.Label;
    import mx.controls.Spacer;
    import mx.core.Application;
    import mx.core.IFlexDisplayObject;
    import mx.effects.Fade;
    import mx.managers.PopUpManager;

    public class ModalSpinner {
        
        /**
         * Effect to be played when message is about to be shown
         */
        private var showEffect:Fade;

        /**
         * Is modal curently displayed
         */
        private var isDisplayed:Boolean;

        public function ModalSpinner() {
            showEffect = new Fade();
            showEffect.alphaFrom = 0;
            showEffect.alphaTo = 1;
            showEffect.duration = 300;

            isDisplayed = false;
            componentsStack_ = [];
        }

        /**
         * "singleton"
         */
        private static var instance_:ModalSpinner;
        public static function get instance():ModalSpinner {
            if (instance_ == null) {
                instance_ = new ModalSpinner();
            }
            return instance_;
        }

        /**
         * Components currently shown in modal window
         */
        private var componentsStack_:Array;

        

        /**
         * Show spinner in modal window. @see TitleWindow for description of params
         * @param message - message
         * @param color - spinner color
         * @param modalTransparency - @default '0.5'
         * @param modalTransparencyBlur - @default '3.0'
         * @param modalTransparencyColor - @default '0xDDDDDD'
         * @param modalTransparencyDuration - @default '100'
         */
        public function showSpinner(message:String = null,
                                    color:uint = 0x015965,
                                    anchorParent:DisplayObjectContainer = null,
                                    modalTransparency:Number = 0.3,
                                    modalTransparencyBlur:Number = 2,
                                    modalTransparencyColor:uint = 0xBBBBBB,
                                    modalTransparencyDuration:int = 200):void{

            var spinner_:Spinner;   //  Component shown in modal window
            var message_:Label;     //  Optional message text
            var container_:VBox;    //  Box for showing both spinner end message

            spinner_ = new Spinner();

            // ticks Color
            spinner_.setStyle(Spinner.STYLE_TICK_COLOR, color);

            // configure modalWindow's modal area with values from component            
            spinner_.setStyle("modalTransparency", modalTransparency);
            spinner_.setStyle("modalTransparencyBlur", modalTransparencyBlur);
            spinner_.setStyle("modalTransparencyColor", modalTransparencyColor);
            spinner_.setStyle("modalTransparencyDuration", modalTransparencyDuration);

            //
            spinner_.play();

            // ensure component is shown
            spinner_.includeInLayout = spinner_.visible = true;

            // show component in modal window using standard flex popupmanager
            if(!message){
                // no message so show spinner only
                container_.addChild(spinner_);
            } else {
                // message defined so also include it to popup

                // create container
                container_ = new VBox();
                container_.setStyle("backgroundAlpha", 0.85);
                container_.setStyle("horizontalAlign", "center");
                container_.setStyle("borderAlpha", 0.85);

                // create message
                message_ = new Label();
                message_.text = message;
                message_.setStyle("color", color);
	            message_.setStyle("fontSize", 15);
                message_.setStyle("showEffect", showEffect);
                message_.visible = true;

                // fill container
                var sp:Spacer = new Spacer();
                sp.height = 15;
                container_.addChild(spinner_);
                container_.addChild(sp);
                container_.addChild(message_);
            }
            // store pop-up
            componentsStack_.push( {
                            spinner: spinner_,
                            message: message_,
                            container: container_
                    } );

            // show pop-up
            isDisplayed = true;
            PopUpManager.addPopUp(IFlexDisplayObject(container_), (anchorParent)?anchorParent:Sprite(Application.application), true);
            PopUpManager.centerPopUp(IFlexDisplayObject(container_));

        }

        /**
         * Close modal and try to restore component to it's parent
         */
        public function hideSpinner(closeEvent:Event=null):void{

            var o:Object = this.componentsStack_.pop();
            if(o){
                //var spinner_:Spinner = o.spinner;
                //var message_:Label = o.message;
                var container_:VBox = o.container;

                if(isDisplayed && container_){
                    // close modal
                    PopUpManager.removePopUp(container_);
                }
            }

        }
    
    }
}