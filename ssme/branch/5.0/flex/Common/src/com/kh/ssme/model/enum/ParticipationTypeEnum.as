/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.enum {

    public class ParticipationTypeEnum {

        public static const CREATOR:ParticipationTypeEnum = new ParticipationTypeEnum("creator");
        public static const PARTICIPANT:ParticipationTypeEnum = new ParticipationTypeEnum("participant");
        public static const OBSERVER:ParticipationTypeEnum = new ParticipationTypeEnum("observer");

		private static const VALUES:Object = new Object();
        {
            //static initialization
            VALUES[CREATOR.type] = CREATOR;
            VALUES[PARTICIPANT.type] = PARTICIPANT;
            VALUES[OBSERVER.type] = OBSERVER;
        }

		public static function find(id:String):ParticipationTypeEnum{
			return VALUES[id];
		}
        public static function get values():Object{
            return VALUES;
        }

		//-------------------------------------------        

		private var type_:String;

        public function ParticipationTypeEnum(type:String) {
            type_ = type;
        }

		public function get type():String{
			return this.type_;
		}

		public function toString():String{
			return this.type_;
		}

		public function equals(type:ParticipationTypeEnum):Boolean{
			return (type!=null && this.type_ == type.type);
		}

    }

}