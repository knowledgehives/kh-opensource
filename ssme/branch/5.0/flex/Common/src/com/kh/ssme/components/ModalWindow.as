/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
    import com.kh.ssme.util.Logger;

    import flash.display.DisplayObject;
    import flash.display.Sprite;

    import flash.events.Event;

    import mx.binding.utils.BindingUtils;
    import mx.core.Application;
    import mx.core.IFlexDisplayObject;
    import mx.core.UIComponent;
    import mx.events.CloseEvent;
    import mx.events.ResizeEvent;
    import mx.managers.PopUpManager;

    

    public class ModalWindow {

        public function ModalWindow() {
            componentsStack_ = [];
        }

        /**
         * "singleton"
         */
        private static var instance_:ModalWindow;
        public static function get instance():ModalWindow {
            if (instance_ == null) {
                instance_ = new ModalWindow();
            }
            return instance_;
        }

        /**
         * Components currently shown in modal window
         */
        private var componentsStack_:Array;


        /**
         * Show given component in modal window. @see TitleWindow for description of params
         * @param component - component to be displayed
         * @param title - title on modal
         * @param explicitWidth - if set to non-zero value it tells modal how much of the screen width it should take
         * @param explicitHeight - if set to non-zero value it tells modal how much of the screen height it should take
         * @param modalLayout - "absolute", "vertical" OR "horizontal" - @default "verticals"
         * @param modalTransparency - @default '0.6'
         * @param modalTransparencyBlur - @default '5.0'
         * @param modalTransparencyColor - @default '0xDDDDDD'
         * @param modalTransparencyDuration - @default '200'
         */
        public function showModal(
                    component:DisplayObject,
                    title:String,
                    explicitWidth:String = null,
                    explicitHeight:String = null,
                    modalLayout:String = "vertical",//ContainerLayout.VERTICAL,
                    modalTransparency:Number = 0.6,
                    modalTransparencyBlur:Number = 5,
                    modalTransparencyColor:uint = 0xDDDDDD,
                    modalTransparencyDuration:int = 200
                ):void{

            if(!component is IFlexDisplayObject || !component is UIComponent){
                Logger.error("ModalWindow.showModal : given component should implements IFlexDisplayObject and be descendant of UIComponent", null);
                return;
            }

            var titleWindow_:ModalTitleWindow = new ModalTitleWindow();
            var component_:DisplayObject;
            var parent_:Sprite;
            var parentChildIndex_:int;

            component_ = component;

            // store components' parent in order to restore component to it's proper place when modal is closed
            if(component.parent){
                parent_ = Sprite(component.parent);
                parentChildIndex_ = component.parent.getChildIndex(component);
            } else {
                parent_ = null;
            }

            titleWindow_.layout = modalLayout;
            titleWindow_.title = title;
            titleWindow_.showCloseButton = true;
            titleWindow_.setStyle("backgroundAlpha", 0.85);
            titleWindow_.setStyle("borderAlpha", 0.8);
            titleWindow_.setStyle("borderColor", 0xa7b9c2);
            titleWindow_.setStyle("paddingLeft", 0);
            titleWindow_.setStyle("paddingRight", 0);
            titleWindow_.setStyle("paddingTop", 0);
            titleWindow_.setStyle("paddingBottom", 0);
            titleWindow_.setStyle("horizontalAlign", "center");
            titleWindow_.setStyle("verticalAlign", "middle");

            // configure modalWindow's modal area with values from component
            titleWindow_.setStyle("modalTransparency", modalTransparency);
            titleWindow_.setStyle("modalTransparencyBlur", modalTransparencyBlur);
            titleWindow_.setStyle("modalTransparencyColor", modalTransparencyColor);
            titleWindow_.setStyle("modalTransparencyDuration", modalTransparencyDuration);

            // sizeing
            if(explicitWidth && explicitHeight){
                // WIDTH
                var calculatedWidth:Number;
                if(explicitWidth.indexOf("%") >= 0){
                    // percentage width
                    calculatedWidth  = parseInt( explicitWidth.replace("%","") );
                    calculatedWidth = ( isNaN(calculatedWidth) ) ? 80 : calculatedWidth;
                    titleWindow_.maxWidth = titleWindow_.width = Math.floor( Application.application.width * (calculatedWidth*0.01) );

                } else {
                    // explicit width
                    calculatedWidth = parseInt( explicitWidth );
                    calculatedWidth = ( isNaN(calculatedWidth) ) ? 800 : calculatedWidth;
                    titleWindow_.maxWidth = titleWindow_.width = Math.floor( calculatedWidth );
                }

                //HEIGHT
                var calculatedHeight:Number;
                if(explicitHeight.indexOf("%") >= 0){
                    // percentage height
                    calculatedHeight  = parseInt( explicitHeight.replace("%","") );
                    calculatedHeight = ( isNaN(calculatedHeight) ) ? 80 : calculatedHeight;
                    titleWindow_.maxHeight = titleWindow_.height = Math.floor( Application.application.height * (calculatedHeight*0.01) );

                } else {
                    // explicit height
                    calculatedHeight  = parseInt( explicitHeight );
                    calculatedHeight = ( isNaN(calculatedHeight) ) ? 600 : calculatedHeight;
                    titleWindow_.maxHeight = titleWindow_.height = Math.floor( calculatedHeight );
                }

                //BIND titleWindow dimensions to component dimensions
                BindingUtils.bindProperty( component_, "width", titleWindow_, "innerPaneWidth" );
                BindingUtils.bindProperty( component_, "height", titleWindow_, "innerPaneHeight" );
            } else {
                titleWindow_.maxWidth = Math.floor( Application.application.width * 0.80 );       //80%
                titleWindow_.maxHeight = Math.floor( Application.application.height * 0.80 );     //80%
            }

            // bound components close event with modal close handler - it alows to close modal from component
            titleWindow_.addEventListener(CloseEvent.CLOSE, this.closeModal);
            // close event for component
            component_.addEventListener(CloseEvent.CLOSE, this.closeModal);

            component_.addEventListener(ResizeEvent.RESIZE, this.onComponentResize);

            // ensure component is shown
            (component_ as UIComponent).includeInLayout = component_.visible = true;

            // add component to title window
            titleWindow_.addChild(component_);

            // pre-show
            if(component_ is IModalDisplayed){
                (component_ as IModalDisplayed).preShow();
            }

            // store pop
            componentsStack_.push( {
                            window: titleWindow_,
                            component: component_,
                            parent: parent_,
                            parentChildIndex: parentChildIndex_
                    } );
            titleWindow_.tooltipVisible = true;

            // show component in modal window using standard flex popupmanager
            PopUpManager.addPopUp(IFlexDisplayObject( titleWindow_ ), Sprite(Application.application), true);
            PopUpManager.centerPopUp(IFlexDisplayObject( titleWindow_ ));
        }

        /**
         * Performed when child object throws 'resize' event
         * @param event
         */
        private function onComponentResize(event:ResizeEvent):void {
            try{
                PopUpManager.centerPopUp(IFlexDisplayObject( event.currentTarget.parent ));
            }
            catch(e:Error){ ; }
        }

        /**
         * Close modal and try to restore component to it's parent
         */
        public function closeModal(closeEvent:Event=null):void{

            var o:Object = this.componentsStack_.pop();
            var window_:ModalTitleWindow = o.window;
            var component_:DisplayObject = o.component;
            var parent_:Sprite = o.parent;
            var parentChildIndex_:int = o.parentChildIndex;

            // remove listeners from component
            component_.removeEventListener(CloseEvent.CLOSE, this.closeModal);

            if(closeEvent && closeEvent.target != component_ && component_){
                // "re-throw" close event if close action was not performed by 'component_'
                // it allows to trigger automaticaly all listeners that are listening for 'close' event on 'component_'
                component_.dispatchEvent(new CloseEvent(CloseEvent.CLOSE, false));
            }

            // pre-hide
            if(component_ is IModalDisplayed){
                (component_ as IModalDisplayed).preHide();
            }               

            // restore component to parent if it is possible
            if(parent_){
                parent_.addChildAt(component_, parentChildIndex_);
                (component_ as UIComponent).includeInLayout = component_.visible = false;
            }

            // close modal
            PopUpManager.removePopUp( window_ );

        }

    }

}