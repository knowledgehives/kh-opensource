/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.enum {

    public class RepeatTypeEnum {

		public static const DAILY:RepeatTypeEnum    = new RepeatTypeEnum("daily", "Daily", 365);
        public static const WEEKLY:RepeatTypeEnum   = new RepeatTypeEnum("weekly", "Weekly", 105);
        public static const MONTHLY:RepeatTypeEnum  = new RepeatTypeEnum("monthly", "Monthly", 36);
        public static const YEARLY:RepeatTypeEnum   = new RepeatTypeEnum("yearly", "Yearly", 50);
        // TODO: [...]

        public static const VALUES:Object = new Object();
        {
            // static initialization
            VALUES[DAILY.repeatType] = DAILY;
            VALUES[WEEKLY.repeatType] = WEEKLY;
            VALUES[MONTHLY.repeatType] = MONTHLY;
            VALUES[YEARLY.repeatType] = YEARLY;            
        }

        public static function find(id:String):RepeatTypeEnum {
            return VALUES[id];
        }


        // ---------- private parts ---------------
        private var repeatType_:String;
        private var label_:String;
        private var limit_:int;        

        public function RepeatTypeEnum(repeatType:String, label:String, limit:int) {
            this.repeatType_ = repeatType;
            this.label_ = label;
            this.limit_ = limit;
        }

        public function get repeatType():String {
            return this.repeatType_;
        }
        public function get label():String {
            return this.label_;
        }
        public function get limit():int{
            return this.limit_;
        }

        public function toString():String {
            return this.label_;
        }

        public function equals(type:RepeatTypeEnum):Boolean {
            return (type != null && this.repeatType_ == type.repeatType_);
        }

    }

}