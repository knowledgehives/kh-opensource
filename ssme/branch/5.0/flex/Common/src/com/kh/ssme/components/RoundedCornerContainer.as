/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
import flash.display.CapsStyle;
import flash.display.LineScaleMode;

import mx.containers.Box;
    import mx.controls.TextArea;
import mx.core.Container;

    [Exclude(name="borderStyle", kind="style")]
    [Exclude(name="borderAlpha", kind="style")]
    
    [Exclude(name="paddingLeft", kind="style")]
    [Exclude(name="paddingRight", kind="style")]
    [Exclude(name="paddingTop", kind="style")]
    [Exclude(name="paddingBottom", kind="style")]

    [Style(name="borderThickness", type="Number", format="Length", inherit="no")]
    [Style(name="borderColor", type="uint", format="Color", inherit="no")]
    public class RoundedCornerContainer extends StyledCanvas {

        public function RoundedCornerContainer() {

            this.setStyle("paddingLeft", 5);
            this.setStyle("paddingRight", 5);
            this.setStyle("paddingTop", 5);
            this.setStyle("paddingBottom", 5);

            this.verticalScrollPolicy = "off";
            this.horizontalScrollPolicy = "off";
        }

        //---------------------------------------------
        // 'STYLE'
        public function set borderThickness_(value:Number):void{
            this.setStyle("borderThickness", value);
        }
        public function get borderThickness_():Number{
            return this.getStyle("borderThickness");
        }
        public function set cornerRadius_(value:Number):void{
            this.setStyle("cornerRadius", value);
        }
        public function get cornerRadius_():Number{
            return this.getStyle("cornerRadius");
        }        
        //---------------------------------------------


        //---------------------------------------------
        // OVERRIDE
        protected override function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{

            var borderThickness:Number = getStyle_("borderThickness", 10);
            var borderColor:uint = getStyle_("borderColor", 0x000000);
            var borderAlpha:Number = 1.0;
            var cornerRadius:Number = getStyle_("cornerRadius", 10);
            var backgroundAlpha:Number = getStyle_("backgroundAlpha", 1.0);


            graphics.clear();   //erase old drawings

            // background
            if (this.getStyle("backgroundColor")){
                graphics.lineStyle(0, this.getStyle("backgroundColor"),backgroundAlpha,
                                    true,                    //pixel hinting
                                   LineScaleMode.NORMAL,    //scale
                                   CapsStyle.NONE);
                graphics.beginFill(this.getStyle("backgroundColor"), this.getStyle("backgroundAlpha"));
                graphics.drawRoundRect(borderThickness*0.5, borderThickness*0.5, unscaledWidth-borderThickness, unscaledHeight-borderThickness, cornerRadius, cornerRadius);                
                graphics.endFill();
            }

            //border
            if (borderThickness > 0){
                graphics.lineStyle(borderThickness,
                                   borderColor,
                                   borderAlpha,
                                   true,                    //pixel hinting
                                   LineScaleMode.NORMAL,    //scale
                                   CapsStyle.NONE);
                graphics.drawRoundRect(borderThickness*0.5, borderThickness*0.5, unscaledWidth-borderThickness, unscaledHeight-borderThickness, cornerRadius, cornerRadius);
            }
        }
        //---------------------------------------------        

    }

}