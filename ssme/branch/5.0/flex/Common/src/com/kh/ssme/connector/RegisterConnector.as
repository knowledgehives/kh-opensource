/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.connector {

    public final class RegisterConnector extends BasicQueuedConnector {

        /**
         * Path to proper servlet
         */
        private static const registerServlet:String = "register/";
        public override function get servlet():String{
            return registerServlet;
        }        

        public function RegisterConnector(url:String) {
            super(url);
        }

        public function registerUser( data:Object = null, params:Object = null ):void{
            // we don't provide UUID for it shall be generated :)
            processCommand( ACTION_CREATE, EVENT_CREATE, null, data, params );
        }

    }

}