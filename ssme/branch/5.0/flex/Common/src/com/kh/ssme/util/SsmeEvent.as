/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.util
{
	import flash.events.Event;	
	
	public class SsmeEvent extends Event
	{
        public static const ITEM_SELECTED:String = "ssme_itemSelected";
        public static const ITEM_SELECTED__ENTITY_PROP:String = "timeFrameEntity";
        public static const ITEM_SELECTED__ITEM_PROP:String = "selectedItem";

		private var eventProperties_:Object;
		
		public function SsmeEvent(type:String, eventProperties:Object, bubbles:Boolean = false, cancelable:Boolean = false){
			this.eventProperties_ = eventProperties;
			super(type, bubbles, cancelable);
		}
		
		public function get eventProperties():Object{
			return this.eventProperties_;    
		}

        public function cloneEvent():SsmeEvent{
            return new SsmeEvent(type, eventProperties_);    
        }

	}
}