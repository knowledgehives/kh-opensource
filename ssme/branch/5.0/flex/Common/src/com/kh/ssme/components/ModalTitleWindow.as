/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {

    import flash.events.Event;
    import mx.containers.TitleWindow;

    public class ModalTitleWindow extends TitleWindow {

        public function ModalTitleWindow() {
            super();
            setStyle("roundedBottomCorners", true);
            setStyle("cornerRadius", 8);
        }

        public override function set width(value:Number):void {
            super.width = Math.floor( value );
            innerPaneWidth_ = Math.floor( value - 20 );           // borderThicknessLeft="10" + borderThicknessRight="10"
            dispatchEvent(new Event("innerPaneWidth"));
        }

        public override function get width():Number{
            return super.width;
        }

        public override function set height(value:Number):void {
            super.height = Math.floor( value );
            innerPaneHeight_ = Math.floor( value - 40 );          // borderThicknessLeft="10" + borderThicknessRight="10" + WTF?="20px"
            dispatchEvent(new Event("innerPaneHeight"));
        }

        public override function get height():Number {
            return super.height;
        }


        private var innerPaneWidth_:Number;
        [Bindable(event="innerPaneWidth")]
        public function get innerPaneWidth():Number{
            return innerPaneWidth_;
        }


        private var innerPaneHeight_:Number;
        [Bindable(event="innerPaneHeight")]
        public function get innerPaneHeight():Number {
            return innerPaneHeight_;
        }

        public var tooltipVisible:Boolean;
        override protected function updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void{
            super.updateDisplayList( unscaledWidth, unscaledHeight );
            if(tooltipVisible){
                toolTip="WIDTH:"+width+"; HEIGHT:"+height+"; " +
                        "X:"+x+"; Y:"+y+";";
            }
        }




    }

}