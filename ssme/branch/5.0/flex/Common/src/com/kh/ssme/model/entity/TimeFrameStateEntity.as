/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {

    import com.kh.ssme.model.enum.StateEnum;

    public class TimeFrameStateEntity extends BasicEntity implements IEntity  {

        public var state:StateEnum;

        public var timeFrameUUID:String;
        public var timeFrameName:String;
        public var timeFrameEntity:TimeFrameEntity;
        public var eventingUUID:String;
        public var eventingName:String;
        public var eventingEntity:EventingEntity;        

        //------------------------------------------------

        protected static const state_field:String = 'state';
        protected static const timeFrame_field:String = 'timeFrame';
        protected static const eventing_field:String = 'eventing';

        //------------------------------------------------

        public function TimeFrameStateEntity() {
            super();
        }

        public override function clone( entity:* = null ):*{
            var newData:TimeFrameStateEntity = super.clone((entity)?entity:new TimeFrameStateEntity());

            // plain
            newData.state = state;

            // entities
            newData.timeFrameUUID = new String( timeFrameUUID );
            newData.timeFrameName = new String( timeFrameName );
            newData.timeFrameEntity = (timeFrameEntity) ? timeFrameEntity.clone() : null;
            newData.eventingUUID = new String( eventingUUID );
            newData.eventingName = new String( eventingName );
            newData.eventingEntity = (eventingEntity) ? eventingEntity.clone() : null;

            return newData;
        }

        public static function createNew():TimeFrameStateEntity{
            var result:TimeFrameStateEntity = new TimeFrameStateEntity();
            
            result.state = StateEnum.UNDECIDED;
            result.timeFrameUUID = "";
            result.timeFrameName = "";
            result.timeFrameEntity = null;
            result.eventingUUID = "";
            result.eventingName = "";
            result.eventingEntity = null;

            return result;
        }

        //------------------------------------------------

        public override function parseJSONObject(json:Object, recursive:int = 0):* {
            super.parseJSONObject( json );

            // state
            state = StateEnum.find( json[state_field] );

            // timeFrame
            // eventing
            if(recursive>0){
                timeFrameEntity = (new TimeFrameEntity()).parseJSONObject( json[timeFrame_field], recursive-1 );
                timeFrameUUID = timeFrameEntity.UUID;
                timeFrameName = timeFrameEntity.title;
                eventingEntity = (new EventingEntity()).parseJSONObject( json[eventing_field], recursive-1 );
                eventingUUID = eventingEntity.UUID;
                eventingName = "Eventing:"+eventingEntity.UUID;
            } else {
                timeFrameUUID = (json[timeFrame_field]) ? (json[timeFrame_field])[uuid_field] : null;
                timeFrameName = (json[timeFrame_field]) ? (json[timeFrame_field])[readableName_field] : null;
                timeFrameEntity = null;
                eventingUUID = (json[eventing_field]) ? (json[eventing_field])[uuid_field] : null;
                eventingName = (json[eventing_field]) ? (json[eventing_field])[readableName_field] : null;
                eventingEntity = null;
            }

            return this;
        }

        public override function toJSONObject():Object {
            var json:Object = super.toJSONObject();

            // state
            json[state_field] = state.state;

            // timeFrame
            json[timeFrame_field] = new Object();
            (json[timeFrame_field])[uuid_field] = timeFrameUUID;
            (json[timeFrame_field])[readableName_field] = timeFrameName;

            // eventing
            json[eventing_field] = new Object();
            (json[eventing_field])[uuid_field] = eventingUUID;
            (json[eventing_field])[readableName_field] = eventingName;

            return json;
        }

    }

}