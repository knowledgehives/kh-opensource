/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.connector {
    import com.kh.ssme.model.entity.EventingEntity;

    public class EventingConnector extends BasicQueuedConnector {

        /**
         * Path to proper servlet
         */
        private static const eventingServlet:String = "eventing/";
        public override function get servlet():String{
            return eventingServlet;
        }
        public override function getResourceURL(uuid:String):String{
            return baseURL + eventingServlet + uuid;
        }

        public function EventingConnector(url:String) {
            super(url);
        }

        public function getEventing( uuid:String, params:Object = null ):void{
            processCommand( ACTION_GET, EVENT_GET, uuid, null, params );
        }

        public function addEventing( data:EventingEntity, params:Object = null ):void{
            processCommand( ACTION_CREATE, EVENT_CREATE, null, (data)?data.toJSONString():"", params );
        }

        public function updateEventing( uuid:String, data:EventingEntity = null, params:Object = null ):void{
            processCommand( ACTION_UPDATE, EVENT_UPDATE, uuid, (data)?data.toJSONString():"", params );
        }

        public function deleteEventing( uuid:String, params:Object = null ):void{
            processCommand( ACTION_DELETE, EVENT_DELETE, uuid, null, params );
        }

    }

}