/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {
    import com.kh.ssme.model.enum.RepeatTypeEnum;
    import com.kh.ssme.util.HashArray;
    import com.kh.ssme.util.JSONObject;

    public final class RepeatTypeEntity extends BasicEntity implements IEntity {

        public var count:int;
        public var frequency:int;
        public var from:Date;        
        public var until:Date;
        public var repeatType:RepeatTypeEnum;
        public var flags:int;

        public var timeFramesEntities:HashArray;
        public var timeFramesNames:HashArray;

        public var prototypeUUID:String;
        public var prototypeName:String;
        public var prototypeEntity:TimeFrameEntity;

        //------------------------------------------------

        protected static const count_field:String = 'count';
        protected static const frequency_field:String = 'frequency';
        protected static const from_field:String = 'from';
        protected static const until_field:String = 'until';
        protected static const repeatType_field:String = 'repeatType';
        protected static const flags_field:String = 'flags';        
        protected static const timeFrames_field:String = 'timeFrames';
        protected static const prototype_field:String = 'prototype';        

        //------------------------------------------------

        public function RepeatTypeEntity() {
            super();
        }

        public override function clone(entity:* = null):*{
            var newData:RepeatTypeEntity = super.clone((entity)?entity:new RepeatTypeEntity());

            // plain
            newData.count = int( count );
            newData.frequency = int( frequency );
            newData.from = new Date( from );
            newData.until = new Date( until );
            newData.repeatType = repeatType;
            newData.flags = flags;

            // entity
            newData.prototypeUUID = new String( prototypeUUID );
            newData.prototypeName = new String( prototypeName );
            newData.prototypeEntity = (prototypeEntity) ? prototypeEntity.clone() : null;

            // hasharrays
            newData.timeFramesEntities = (timeFramesEntities) ? timeFramesEntities.clone() : null;
            newData.timeFramesNames = (timeFramesNames) ? timeFramesNames.clone() : null;

            return newData;
        }

        public static function createNew():RepeatTypeEntity{
            var result:RepeatTypeEntity = new RepeatTypeEntity();
            result.count = 0;
            result.frequency = 0;
            result.from = null;
            result.until = null;
            result.repeatType = RepeatTypeEnum.DAILY;
            result.flags = 0;
            result.timeFramesEntities = new HashArray();
            result.timeFramesNames = new HashArray();
            result.prototypeUUID = "";
            result.prototypeName = "";
            result.prototypeEntity = null;            
            return result;
        }

        //------------------------------------------------

        public override function parseJSONObject(json:Object, recursive:int = 0):* {
            super.parseJSONObject( json );
            var i:int;

            // count
            count = json[count_field];

            // frequency
            frequency = json[frequency_field];

            // from
            from = JSONObject.parseString2Date( json[from_field] );

            // until
            until = JSONObject.parseString2Date( json[until_field] );

            // repeatType
            repeatType = RepeatTypeEnum.find(json[repeatType_field]);

            // flags
            flags = json[flags_field];

            // timeFrames
            timeFramesEntities = new HashArray();
            timeFramesNames = new HashArray();
            if(json[timeFrames_field].length>0){
                if(recursive>0){
                    var tf:TimeFrameEntity;
                    for(i = 0; i<json[timeFrames_field].length; i++){
                        tf = (new TimeFrameEntity()).parseJSONObject( (json[timeFrames_field][i]), recursive-1 );
                        timeFramesEntities.put( tf.UUID, tf );
                        timeFramesNames.put( tf.UUID, tf.title );
                    }
                } else {
                    for(i = 0; i<json[timeFrames_field].length; i++){
                        timeFramesEntities.put( (json[timeFrames_field][i])[uuid_field], null );
                        timeFramesNames.put( (json[timeFrames_field][i])[uuid_field], (json[timeFrames_field][i])[readableName_field] );
                    }
                }
            }

            // prototype
            if(recursive>0){
                prototypeEntity = (new TimeFrameEntity()).parseJSONObject( json[prototype_field], recursive-1 );
                prototypeUUID = prototypeEntity.UUID;
                prototypeName = prototypeEntity.title;
            } else {
                prototypeUUID = (json[prototype_field]) ? (json[prototype_field])[uuid_field] : null;
                prototypeName = (json[prototype_field]) ? (json[prototype_field])[readableName_field] : null;
                prototypeEntity = null;
            }

            return this;
        }

        public override function toJSONObject():Object {
            var json:Object = super.toJSONObject();
            var tab:Array = new Array();

            // count
            json[count_field] = count;

            // frequency            
            json[frequency_field] = frequency;
            
            // from
            json[from_field] = JSONObject.parseDate2String( from );

            // until            
            json[until_field] = JSONObject.parseDate2String( until );

            // repeatType            
            json[repeatType_field] = repeatType.repeatType;

            // flags
            json[flags] = flags;

            // prototype
            json[prototype_field] = new Object();
            (json[prototype_field])[uuid_field] = prototypeUUID;
            (json[prototype_field])[readableName_field] = prototypeName;            

            // timeFrames            
            for (var key:String in timeFramesNames){
                tab.push( { uuid:key, name:timeFramesNames[key] } );
            }
            json[timeFrames_field] = tab;

            return json;
        }

    }

}