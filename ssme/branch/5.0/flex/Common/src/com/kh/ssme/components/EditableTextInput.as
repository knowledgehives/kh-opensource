/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
import com.kh.ssme.util.SsmeEvent;

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;

    import flash.ui.Keyboard;

    import mx.controls.TextInput;
import mx.core.UIComponent;
import mx.events.FlexEvent;
    import mx.events.FlexMouseEvent;
import mx.events.ValidationResultEvent;
import mx.managers.FocusManager;
import mx.managers.ToolTipManager;
import mx.validators.Validator;

[Event(name="modified",type="flash.events.Event")]

    public class EditableTextInput extends TextInput {

        public static const MODIFIED:String = "modified";

        /**
         * pre-modified value
         */
        private var oldText:String;

        /**
         * Validator used while commiting new value 
         */
        public var validator:Validator;

        public function EditableTextInput() {
            super();
            this.addEventListener( mx.events.FlexEvent.INITIALIZE, onInit );
            this.addEventListener( flash.events.MouseEvent.DOUBLE_CLICK, onDoubleClick );
            this.addEventListener( mx.events.FlexMouseEvent.MOUSE_DOWN_OUTSIDE, onMouseDownOut );
            this.addEventListener( flash.events.KeyboardEvent.KEY_DOWN, onKeyDown );
        }


        private function onInit(event:Event):void {
            this.doubleClickEnabled = true;
            this.enabled = false;
        }        


        public override function get enabled():Boolean{
            return super.enabled;
        }
        public override function set enabled(value:Boolean):void{
            super.enabled = value;
        }

        
        protected function edit():void{
            oldText = this.text;
            this.enabled = true;
            if(validator) validator.enabled = true;
            focusManager.setFocus( this );
        }
        protected function commit():void{
            var valid:Boolean = true;
            if(validator){
                var result:Event = validator.validate();
                if(!result || (result.type == ValidationResultEvent.INVALID)){
                    valid = false;
                }
            }
            
            if(valid){
                dispatchEvent( new SsmeEvent(MODIFIED, { oldValue:oldText, newValue:this.text }) );
                oldText = this.text;
                this.enabled = false;
                if(validator) validator.enabled = false;
                stage.focus = null;
            }
        }
        protected function rollback():void{
            this.text = oldText;
            this.enabled = false;
            if(validator) {
                // disable validator
                validator.enabled = false;
                //clear validator errors
                //var trigger:Object = (validator.trigger) ? validator.trigger : validator.source;
                //trigger.setStyle('borderStyle', 'none');
                //if (ToolTipManager.currentTarget == trigger && ToolTipManager.currentToolTip != null) {
                //    ToolTipManager.currentToolTip.visible = false;
                //}
            }
            stage.focus = null;
        }


        private function onDoubleClick(event:flash.events.MouseEvent):void {
            edit();
        }
        private function onMouseDownOut(event:mx.events.FlexMouseEvent):void {
            commit();
        }        

        
        private function onKeyDown(event:flash.events.KeyboardEvent):void {
            if (event.keyCode == Keyboard.ENTER) {
                commit();
            }
            if (event.keyCode == Keyboard.ESCAPE){
                rollback();
            }
        }


    }

}