/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.connector{

    import com.kh.ssme.util.JSONObject;
    import com.kh.ssme.util.Logger;
    import com.kh.ssme.util.LoggingComponentsEnum;
    import com.kh.ssme.util.SsmeEvent;
    import com.kh.ssme.util.UrlUtils;

import flash.events.Event;
import flash.events.EventDispatcher;
	import mx.core.Application;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;
	import mx.utils.Base64Encoder;
	
	[Event(name="ssme_objectGet", type="com.kh.ssme.util.SsmeEvent")]
	[Event(name="ssme_objectCreate",  type="com.kh.ssme.util.SsmeEvent")]
	[Event(name="ssme_objectUpdate", type="com.kh.ssme.util.SsmeEvent")]
	[Event(name="ssme_objectDelete",  type="com.kh.ssme.util.SsmeEvent")]
	[Event(name="ssme_error", type="com.kh.ssme.util.SsmeEvent")]
	[Event(name="ssme_fault", type="com.kh.ssme.util.SsmeEvent")]    
	
	public class BasicQueuedConnector extends EventDispatcher{
		/**
		 * Helper object used for authentication purposes
		 */
		protected const encoder:Base64Encoder = new Base64Encoder();
		
		/**
		 * Base URL
		 */		
		protected var baseURL:String;		

		/**
		 * Use this one to load tags
		 */
		protected var service:HTTPService;	
		
		/**
		 * Path to proper servlet 
		 */
        private static const servlet_:String = "";
		public function get servlet():String{
			return servlet_;
		}
        public function getResourceURL(uuid:String):String{
            return baseURL + servlet_ + uuid;
        }        
		
		public static const ACTION_GET:String = "get";
		public static const ACTION_CREATE:String = "create";
		public static const ACTION_UPDATE:String = "update";
		public static const ACTION_DELETE:String = "delete";			
			
		public static const EVENT_GET:String = "ssme_objectGet";
		public static const EVENT_CREATE:String = "ssme_objectCreate";
		public static const EVENT_UPDATE:String = "ssme_objectUpdate";
		public static const EVENT_DELETE:String = "ssme_objectDelete";
		public static const EVENT_ERROR:String = "ssme_error";
		public static const EVENT_FAULT:String = "ssme_fault";	        
			
		public function BasicQueuedConnector(url:String){
            if(url){
                baseURL = url.replace(/flex\/[^\/]+.swf$/gi, "").replace(/\/$/gi, "") + "/";
            }
			
			this.service = new HTTPService();
			this.service.useProxy = false;
			this.service.resultFormat = "text";
            this.service.contentType = "application/x-www-form-urlencoded";                
			this.service.addEventListener(FaultEvent.FAULT, this.onResult);
			this.service.addEventListener(ResultEvent.RESULT, this.onResult);
			
//			this.encoder.encode(""); //(credentials);
//			this.service.headers["Authorization"] = "Basic " + encoder.toString();
		}

        /**
         * Singe "exit point"
         * @param event
         */
        protected function onResult(event:Event):void{

            if(event is FaultEvent){
                //  FAULT                
                Logger.error("#PoolConnector : onFault: ", (event as FaultEvent).fault, LoggingComponentsEnum.CON);
                dispatchEvent(new SsmeEvent(EVENT_FAULT, { result:new ConnectorResult(currentCommand, ConnectorResult.FAULT, null, event) }));
            } else {
                Logger.debug("#PoolConnector : result : \n"+service.lastResult.toString(), LoggingComponentsEnum.CON);                

                var json:Object = JSONObject.decode((service.lastResult) ? service.lastResult.toString() : "");
                if(json && json[JSONObject.errorStatus]){
                    //  ERROR                    
                    dispatchEvent(new SsmeEvent(currentCommand.eventMnemonic, { result:new ConnectorResult(currentCommand, ConnectorResult.ERROR, json, event) }));
                } else {
                    //  SUCCESS - dispatch event with recived data
                    dispatchEvent(new SsmeEvent(currentCommand.eventMnemonic, { result:new ConnectorResult(currentCommand, ConnectorResult.SUCCESS, service.lastResult, event) }));
                }
            }
            
            // fire process if there is command in queue
			if(connectionQueue_.length > 0){
				processPool();
			}            
        }

		
		
		protected function get_object():void{
            this.service.contentType = "application/x-www-form-urlencoded";               
			this.service.method  = "GET";
			this.service.url = baseURL + servlet + ((currentCommand.UUID)? currentCommand.UUID : "");
            this.service.url += "?_method=GET&flexApp" + UrlUtils.objectToString(currentCommand.params, "&", "&", true);
			this.service.send();
			Logger.debug("#ReportsConnector get report ["+this.service.url+"]", LoggingComponentsEnum.CON);
		}		

		protected function create_object():void{
            this.service.contentType = "application/x-www-form-urlencoded";               
			this.service.method = "POST";
			this.service.url = baseURL + servlet + ((currentCommand.UUID)? currentCommand.UUID : "");
            this.service.url += "?_method=PUT&flexApp" + UrlUtils.objectToString(currentCommand.params, "&", "&", true);
			this.service.send({ json:(currentCommand.data as String) });
			Logger.debug("#Connector CREATE ["+this.service.url+"]", LoggingComponentsEnum.CON);
			Logger.debug("#Connector DATA ["+(currentCommand.data as String)+"]", LoggingComponentsEnum.CON);            
		}		
		
		protected function update_object():void{
            this.service.contentType = "application/x-www-form-urlencoded";               
			this.service.method = "POST";
			this.service.url = baseURL + servlet + ((currentCommand.UUID)? currentCommand.UUID : "");
            this.service.url += "?_method=POST&flexApp" + UrlUtils.objectToString(currentCommand.params, "&", "&", true);
			this.service.send({ json:(currentCommand.data as String) });
			Logger.debug("#Connector UPDATE ["+this.service.url+"]", LoggingComponentsEnum.CON);
			Logger.debug("#Connector DATA ["+(currentCommand.data as String)+"]", LoggingComponentsEnum.CON);                 
		}

        protected function delete_object():void{
            this.service.contentType = "application/x-www-form-urlencoded";               
            this.service.method = "POST";           
            this.service.url = baseURL + servlet + ((currentCommand.UUID)? currentCommand.UUID : "");
            this.service.url += "?_method=DELETE&flexApp" + UrlUtils.objectToString(currentCommand.params, "&", "&", true);
            this.service.send();
            Logger.debug("#Connector DELETE ["+this.service.url+"]", LoggingComponentsEnum.CON);
        }        
		
		
		//-------------------------------------------------
		// CONNECTIONS QUEUE HANDLING
		/**
		 * Connection queue
		 */
		[ArrayElementType("com.kh.ssme.connector.ConnectionQueueEntry")]
		protected var connectionQueue_:Array = [];
		
		/**
		 * Curently processed command
		 */		
		protected var currentCommand:ConnectionQueueEntry;
				
		protected function processCommand(action:String, eventMnemonic:String, id:String = null, dataObject:Object = null, params:Object = null):void{
			connectionQueue_.push(new ConnectionQueueEntry(action, eventMnemonic, id, dataObject, params));
			if(connectionQueue_.length <=1){
				// fire process only if there is only one command in queue
				processPool();	
			}
		}		
		
		private function processPool():void{
			if (connectionQueue_.length > 0)
			{
				this.currentCommand = connectionQueue_.shift();
				
				switch (this.currentCommand.action)
				{
					case ACTION_GET:
						this.get_object();
						break;
					case ACTION_CREATE:
						this.create_object();
						break;
					case ACTION_UPDATE:
						this.update_object();
						break;
					case ACTION_DELETE:
						this.delete_object();
						break;
				}
			}
		}
		//-------------------------------------------------

        protected function P(name:*, value:*):String{
            return name+"="+value;
        }
		
	}
}