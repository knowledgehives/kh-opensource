/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.enum {

    public class StateEnum {

        public static const ACCEPTED:StateEnum = new StateEnum("accepted");
        public static const REJECTED:StateEnum = new StateEnum("rejected");
        public static const TENTATIVE:StateEnum = new StateEnum("tentative");
	    public static const UNDECIDED:StateEnum = new StateEnum("undecided");

		private static const VALUES:Object = new Object();
        {
            //static initialization
            VALUES[ACCEPTED.state] = ACCEPTED;
            VALUES[REJECTED.state] = REJECTED;
            VALUES[TENTATIVE.state] = TENTATIVE;
            VALUES[UNDECIDED.state] = UNDECIDED;
        }

		public static function find(id:String):StateEnum{
			return VALUES[id];
		}

		//-------------------------------------------

		private var state_:String;        

        public function StateEnum(state:String) {
            state_ = state;
        }

		public function get state():String{
			return this.state_;
		}

		public function toString():String{
			return this.state_;
		}
        
		public function equals(type:StateEnum):Boolean{
			return (type!=null && this.state_ == type.state);
		}

    }

}