/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {
import com.kh.ssme.model.enum.PriorityEnum;
import com.kh.ssme.model.enum.TimeFrameTypeEnum;
import com.kh.ssme.model.enum.TimeFrameTypeEnum;
import com.kh.ssme.util.HashArray;
	import com.kh.ssme.util.JSONObject;

    public final class TimeFrameEntity extends BasicEntity implements IEntity {

        public var from:Date;
        public var to:Date;
        public var title:String;
        public var description:String;
        public var isPrototype:Boolean;
        public var type:TimeFrameTypeEnum;
        public var priority:PriorityEnum;        

        public var repeatUUID:String;
        public var repeatName:String;
        public var repeatEntity:RepeatTypeEntity;

//        public var eventPlaceUUID:String;
//        public var eventPlaceName:String;
//        public var eventPlaceEntity:LocationEntity;
//
//        public var travelFromUUID:String;
//        public var travelFromName:String;
//        public var travelFromEntity:LocationEntity;
//
//        public var travelToUUID:String;
//        public var travelToName:String;
//        public var travelToEntity:LocationEntity;
//
//        public var stayPlaceUUID:String;
//        public var stayPlaceName:String;
//        public var stayPlaceEntity:StayPlaceEntity;

        public var calendarUUID:String;
        public var calendarName:String;
        public var calendarEntity:CalendarEntity;

        public var suggestedTimeStateUUID:String;
        public var suggestedTimeStateName:String;
        public var suggestedTimeStateEntity:TimeFrameStateEntity;

        public var meetingRequestUUID:String;
        public var meetingRequestName:String;
        public var meetingRequestEntity:MeetingRequestEntity;

        public var parentUUID:String;
        public var parentName:String;
        public var parentEntity:RepeatTypeEntity;        

        //------------------------------------------------        

        protected static const from_field:String = 'from';
        protected static const to_field:String = 'to';
        protected static const title_field:String = 'title';
        protected static const description_field:String = 'description';
        protected static const prototype_field:String = 'prototype';
        protected static const type_field:String = 'type';
        protected static const priority_field:String = 'priority';
        protected static const repeat_field:String = 'repeat';        
//        protected static const eventPlace_field:String = 'eventPlace';
//        protected static const travelFrom_field:String = 'travelFrom';
//        protected static const travelTo_field:String = 'travelTo';
//        protected static const stayPlace_field:String = 'stayPlace';
        protected static const calendar_field:String = 'calendar';
	    protected static const suggestedTimeState_field:String = 'suggestedTimeState';
        protected static const meetingRequest_field:String = 'meetingRequest';
        protected static const parent_field:String = 'parent';                

        //------------------------------------------------
        
        public function TimeFrameEntity() {
            super();
        }

        public override function clone(entity:* = null):*{
            var newTF:TimeFrameEntity = super.clone((entity)?entity:new TimeFrameEntity());

            // plain
            newTF.from = new Date(from.time);
            newTF.to = new Date(to.time);
            newTF.title = new String( title );
            newTF.description = new String( description );
            newTF.isPrototype = (isPrototype == true); 
            newTF.type = type;
            newTF.priority = priority;

            // entities
            newTF.repeatUUID = new String( repeatUUID );
            newTF.repeatName = new String( repeatName );
            newTF.repeatEntity = repeatEntity.clone();
//            newTF.eventPlaceUUID = new String( eventPlaceUUID );
//            newTF.eventPlaceName = new String( eventPlaceName );
//            newTF.eventPlaceEntity = eventPlaceEntity.clone()
//            newTF.travelFromUUID = new String( travelFromUUID );
//            newTF.travelFromName = new String( travelFromName );
//            newTF.travelFromEntity = travelFromEntity.clone()
//            newTF.travelToUUID = new String( travelToUUID );
//            newTF.travelToName = new String( travelToName );
//            newTF.travelToEntity = travelToEntity.clone()
//            newTF.stayPlaceUUID = new String( stayPlaceUUID );
//            newTF.stayPlaceName = new String( stayPlaceName );
//            newTF.stayPlaceEntity = stayPlaceEntity.clone()
            newTF.calendarUUID = new String( calendarUUID );
            newTF.calendarName = new String( calendarName );
            newTF.calendarEntity = (calendarEntity) ? calendarEntity.clone() : null;
            newTF.suggestedTimeStateUUID = new String( suggestedTimeStateUUID );
            newTF.suggestedTimeStateName = new String( suggestedTimeStateName );
            newTF.suggestedTimeStateEntity = (suggestedTimeStateEntity) ? suggestedTimeStateEntity.clone() : null;
            newTF.parentUUID = new String( parentUUID );
            newTF.parentName = new String( parentName );
            newTF.parentEntity = (parentEntity) ? parentEntity.clone() : null;
            newTF.meetingRequestUUID = new String( meetingRequestUUID );
            newTF.meetingRequestName = new String( meetingRequestName );
            newTF.meetingRequestEntity = (meetingRequestEntity) ? meetingRequestEntity.clone() : null;

            return newTF;
        }

        public static function createNew(title:String, description:String, from:Date, to:Date):TimeFrameEntity{
            var result:TimeFrameEntity = new TimeFrameEntity();

            result.from = from;
            result.to = to;
            result.title = title;
            result.description = description;
            result.isPrototype = false; 
            result.type = TimeFrameTypeEnum.AVAILABLE;
            result.priority = PriorityEnum.MEDIUM;

            result.repeatUUID = "";
            result.repeatName = "";
            result.repeatEntity = null;            
//            result.eventPlaceUUID = "";
//            result.eventPlaceName = "";
//            result.eventPlaceEntity = null;
//            result.travelFromUUID = "";
//            result.travelFromName = "";
//            result.travelFromEntity = null;
//            result.travelToUUID = "";
//            result.travelToName = "";
//            result.travelToEntity = null;
//            result.stayPlaceUUID = "";
//            result.stayPlaceName = "";
//            result.stayPlaceEntity = null;
            result.calendarUUID = "";
            result.calendarName = "";
            result.calendarEntity = null;
            result.suggestedTimeStateUUID = "";
            result.suggestedTimeStateName = "";
            result.suggestedTimeStateEntity = null;
            result.meetingRequestUUID = "";
            result.meetingRequestName = "";
            result.meetingRequestEntity = null;
            result.parentUUID = "";
            result.parentName = "";
            result.parentEntity = null;

            return result;
        }

        //------------------------------------------------

        /**
         * Checks if tis a repeatable event
         * @return
         */
        public function get isRepeatable():Boolean{
            return (repeatUUID==null) || (repeatUUID!=null && repeatUUID.length<1);
        }

        //------------------------------------------------

        public override function parseJSONObject(json:Object, recursive:int = 0):* {
            var i:int;
            super.parseJSONObject( json );

            // from
            from = JSONObject.parseString2Date( json[from_field] );

            // to
            to = JSONObject.parseString2Date( json[to_field] );

            // title
            title = json[title_field];

            // description
            description = json[description_field];

            // prototype
            isPrototype = (json[prototype_field] == "true");

            // type
            type = TimeFrameTypeEnum.find( json[type_field] );

            // priority
            priority = PriorityEnum.find( json[priority_field] );

            // repeat
            // calendar
            // suggestedTimeState
            // meetingRequest
            // parent
            if(recursive>0){
                repeatEntity = (new RepeatTypeEntity()).parseJSONObject( json[repeat_field], recursive-1 );
                repeatUUID = repeatEntity.UUID;
                repeatName = "RepeatType:"+repeatEntity.UUID;
                calendarEntity = (new CalendarEntity()).parseJSONObject( json[calendar_field], recursive-1 );
                calendarUUID = calendarEntity.UUID;
                calendarName = calendarEntity.name;
                suggestedTimeStateEntity = (new TimeFrameStateEntity()).parseJSONObject( json[suggestedTimeState_field], recursive-1 );
                suggestedTimeStateUUID = suggestedTimeStateEntity.UUID;
                suggestedTimeStateName = "TimeFrameState:"+suggestedTimeStateEntity.UUID;
                meetingRequestEntity = (new MeetingRequestEntity()).parseJSONObject( json[meetingRequest_field], recursive-1 );
                meetingRequestUUID = meetingRequestEntity.UUID;
                meetingRequestName = "MeetingRequest:"+meetingRequestEntity.UUID;
                parentEntity = (new RepeatTypeEntity()).parseJSONObject( json[parent_field], recursive-1 );
                parentUUID = parentEntity.UUID;
                parentName = "ParentRepeatType:"+parentEntity.UUID;
            } else {
                repeatUUID = (json[repeat_field]) ? (json[repeat_field])[uuid_field] : null;
                repeatName = (json[repeat_field]) ? (json[repeat_field])[readableName_field] : null;
                repeatEntity = null;
                calendarUUID = (json[calendar_field]) ? (json[calendar_field])[uuid_field] : null;
                calendarName = (json[calendar_field]) ? (json[calendar_field])[readableName_field] : null;
                calendarEntity = null;
                suggestedTimeStateUUID = (json[suggestedTimeState_field]) ? (json[suggestedTimeState_field])[uuid_field] : null;
                suggestedTimeStateName = (json[suggestedTimeState_field]) ? (json[suggestedTimeState_field])[readableName_field] : null;
                suggestedTimeStateEntity = null;
                meetingRequestUUID = (json[meetingRequest_field]) ? (json[meetingRequest_field])[uuid_field] : null;
                meetingRequestName = (json[meetingRequest_field]) ? (json[meetingRequest_field])[readableName_field] : null;
                meetingRequestEntity = null;
                parentUUID = (json[parent_field]) ? (json[parent_field])[uuid_field] : null;
                parentName = (json[parent_field]) ? (json[parent_field])[readableName_field] : null;
                parentEntity = null;
            }

            return this;
        }

        public override function toJSONObject():Object{
            var json:Object = super.toJSONObject();
            var tab:Array = new Array();
            var key:String;

            // from
            json[from_field] = JSONObject.parseDate2String( from );

            // to            
            json[to_field] =  JSONObject.parseDate2String( to );

            // title
            json[title_field] = title;
                                            
            // description
            json[description_field] = description;

            // prototype
            json[prototype_field] = (isPrototype) ? "true" : "false";

            // type
            json[type_field] = type.timeFrameType;

            // priority
            json[priority_field] = priority.priority;

            // repeat
            json[repeat_field] = new Object();
            (json[repeat_field])[uuid_field] = repeatUUID;
            (json[repeat_field])[readableName_field] = repeatName;

            // calendar
            json[calendar_field] = new Object();
            (json[calendar_field])[uuid_field] = calendarUUID;
            (json[calendar_field])[readableName_field] = calendarName;

            // suggestedTimeState            
            json[suggestedTimeState_field] = new Object();
            (json[suggestedTimeState_field])[uuid_field] = suggestedTimeStateUUID;
            (json[suggestedTimeState_field])[readableName_field] = suggestedTimeStateName;

            // meetingRequests
            json[meetingRequest_field] = new Object();
            (json[meetingRequest_field])[uuid_field] = meetingRequestUUID;
            (json[meetingRequest_field])[readableName_field] = meetingRequestName;

            // parent
            json[parent_field] = new Object();
            (json[parent_field])[uuid_field] = parentUUID;
            (json[parent_field])[readableName_field] = parentName;

            return json;
        }       
        
    }

}