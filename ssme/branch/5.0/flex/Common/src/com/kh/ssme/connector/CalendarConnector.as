/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.connector {

    import com.kh.ssme.model.entity.CalendarEntity;

	[Event(name="ssme_calendarRange_objectGet", type="com.kh.ssme.util.SsmeEvent")]
	[Event(name="ssme_calendarAll_objectGet",  type="com.kh.ssme.util.SsmeEvent")]

    public class CalendarConnector extends BasicQueuedConnector {

        /**
         * Path to proper servlet
         */
        private static const calendarServlet:String = "calendar/";
        public override function get servlet():String{
            return calendarServlet;
        }
        public override function getResourceURL(uuid:String):String{
            return baseURL + calendarServlet + uuid;
        }        

        public function CalendarConnector(url:String) {
            super(url);
        }

		public static const EVENT_GET_CALENDAR_RANGE:String = "ssme_calendarRange_objectGet";
		public static const EVENT_GET_CALENDAR_ALL:String = "ssme_calendarAll_objectGet";        


        public function getCalendar( uuid:String, params:Object = null ):void{
            processCommand( ACTION_GET, EVENT_GET, uuid, null, params );
        }

        public function getCalendarAll( timestampFrom:Number, timestampTo:Number, params:Object = null ):void{
            if(!params) params = {};
            params[Params.CALENDAR_FROM] = timestampFrom;
            params[Params.CALENDAR_TO] = timestampTo;
            processCommand( ACTION_GET, EVENT_GET_CALENDAR_ALL, null, null, params );
        }

        public function getCalendarRange( uuid:String, timestampFrom:Number, timestampTo:Number, params:Object = null ):void{
            if(!params) params = {};
            params[Params.CALENDAR_FROM] = timestampFrom;
            params[Params.CALENDAR_TO] = timestampTo;
            processCommand( ACTION_GET, EVENT_GET_CALENDAR_RANGE, uuid, null, params );
        }

        public function addCalendar( data:CalendarEntity, params:Object = null ):void{
            processCommand( ACTION_CREATE, EVENT_CREATE, null, (data)?data.toJSONString():"", params );
        }

        public function updateCalendar( uuid:String, data:CalendarEntity = null, params:Object = null ):void{
            processCommand( ACTION_UPDATE, EVENT_UPDATE, uuid, (data)?data.toJSONString():"", params );
        }

        public function deleteCalendar( uuid:String, params:Object = null ):void{
            processCommand( ACTION_DELETE, EVENT_DELETE, uuid, null, params );
        }        

    }

}
