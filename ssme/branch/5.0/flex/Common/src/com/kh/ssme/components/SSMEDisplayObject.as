/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.components {
    import com.kh.ssme.model.entity.UserEntity;
    import flash.events.Event;

    import mx.core.Application;

    public class SSMEDisplayObject extends HeaderlessPanel {

        [Bindable]
        protected var application_:IFlexClient;

        public function SSMEDisplayObject() {
            var o:Object = Application.application as IFlexClient;   
            application_ = IFlexClient(o);
        }

        public function hide():void {
            this.includeInLayout = false;
            this.visible = false;
        }

        public function show():void {
            this.includeInLayout = true;
            this.visible = true;
        }

        /**
         * Called when application was initialized and we can proceed with loading data
         * or we reload data after processing drilldown
         * @param event
         */        
        public function reloadObjectContent(event:Event):void{
            // empty!!    
        }


        //----------------------------------------------------
        // bindings to application
        public function get url():String{
            return application_.url;
        }
        public function get resourceUUID():String{
            return application_.resourceUUID;
        }
        public function get userEntityLoaded():Boolean{
            return application_.userEntityLoaded;
        }
        public function get loggedUserEntity():UserEntity{
            return application_.loggedUserEntity;
        }
        public function loadUser():void{
            application_.loadUser();
        }
        public function get application():IApplication{
            return application_;
        }
        public function makeInnerDrilldown(resource:String, uuid:String = null, query:String = null):void{
            var tempUrl:String = application_.globalApplicationPath+resource+"/";
            if(uuid && uuid.length>0)   tempUrl+=uuid;
            if(query && query.length>0) tempUrl+="?"+query
            application_.callDrilldown( tempUrl, null );    
        }
        public function callDrilldown( query:String, externalApp:String = null ):void{
            application_.callDrilldown( query, externalApp );
        }
        //
        //----------------------------------------------------

    }

}