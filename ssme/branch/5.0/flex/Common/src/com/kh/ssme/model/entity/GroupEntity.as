/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {
    import com.kh.ssme.util.HashArray;

    public final class GroupEntity extends BasicEntity  implements IEntity{

        public var name:String;

        public var description:String;

        public var ownerUUID:String;
        public var ownerName:String;
        public var ownerEntity:UserEntity;        

        public var usersEntites:HashArray;
        public var usersNames:HashArray;

        //------------------------------------------------

        protected static const name_field:String = 'name';
        protected static const description_field:String = 'description';
        protected static const owner_field:String = 'owner';
        protected static const users_field:String = 'users';

        //------------------------------------------------        

        public function GroupEntity() {
            super();
        }
        
        public override function clone( entity:* = null ):*{
            var newData:GroupEntity = super.clone((entity)?entity:new GroupEntity());

            // plain
            newData.name = new String( name );
            newData.description = new String( description );            

            // entities
            newData.ownerUUID = new String( ownerUUID );
            newData.ownerName = new String( ownerName );
            newData.ownerEntity = (ownerEntity) ? ownerEntity.clone() : null;

            // hasharrays
            newData.usersEntites = (usersEntites) ? usersEntites.clone() : null;
            newData.usersNames = (usersNames) ? usersNames.clone() : null;

            return newData;
        }

        public static function createNew(name:String, description:String, ownerUUID:String):GroupEntity{
            var result:GroupEntity = new GroupEntity();
            result.name = name;
            result.description = description;
            result.ownerUUID = ownerUUID;
            result.ownerName = "";
            result.ownerEntity = null;       
            result.usersEntites = new HashArray();
            result.usersNames =  new HashArray();
            return result;
        }

        //------------------------------------------------        

        public override function parseJSONObject(json:Object, recursive:int = 0):* {
            super.parseJSONObject( json );
            var i:int = 0;

            // name
            name = json[name_field];

            // description
            description = json[description_field];

            // owner
            if(recursive>0){
                ownerEntity = (new UserEntity()).parseJSONObject( json[owner_field], recursive-1 );
                ownerUUID = ownerEntity.UUID;
                ownerName = ownerEntity.name+" "+ownerEntity.surname;
            } else {
                ownerUUID = (json[owner_field]) ? json[owner_field][uuid_field] : '';
                ownerName = (json[owner_field]) ? json[owner_field][readableName_field] : '';
                ownerEntity = null;
            }

            // users
            usersEntites = new HashArray();
            usersNames = new HashArray();
            if(json[users_field].length>0){
                if(recursive>0){
                    var usr:UserEntity;
                    for(i = 0; i<json[users_field].length; i++){
                        usr = (new UserEntity()).parseJSONObject( (json[users_field][i]), recursive-1 );
                        usersEntites.put( usr.UUID, usr );
                        usersNames.put( usr.UUID, usr.name+" "+usr.surname+" ("+usr.login+") " );
                    }
                } else {
                    for(i=0; i<json[users_field].length; i++){
                        usersEntites.put( (json[users_field][i])[uuid_field], null );                        
                        usersNames.put( (json[users_field][i])[uuid_field], (json[users_field][i])[readableName_field] );
                    }
                }
            }

            return this;
        }

        public override function toJSONObject():Object{
            var json:Object = super.toJSONObject();
            var tab:Array = new Array();
            var key:String;

            // name            
            json[name_field] = name;

            // description
            json[description_field] = description;

            // owner            
            json[owner_field] = new Object();
            (json[owner_field])[uuid_field] = ownerUUID;
            (json[owner_field])[readableName_field] = ownerName;

            // users            
            tab = new Array();
            for (key in usersNames){
                tab.push( { uuid:key, name:usersNames[key] } );
            }
            json[users_field] = tab;

            return json;
        }

    }

}