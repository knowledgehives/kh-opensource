/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.util {
    import com.adobe.serialization.json.JSON;

    public class JSONObject extends JSON {

        public static var errorStatus:String = "errorStatus";
        public static var errorMessage:String = "errorMessage";

        public function JSONObject() {
            super();
        }


        public static function decode(param:String):* {
            try{
                return JSON.decode(param);    
            } catch(e:Error){
                Logger.error("decode json:[ "+param+" ] failed", e);
            }
            return null;            
        }

        public static function encode(param:Object):String {
            try{
                return JSON.encode(param);   
            } catch(e:Error){
                Logger.error("encode json:[ "+param+" ] failed", e);
            }
            return null;            
        }

        public static function parseString2Date(value:String):Date{
            //  format: "2010/06/24 19:38:32.000 UTC+0200" -> Date
            //  var pattern:RegExp = /^(?P<year>(\d{4}))-(?P<month>(\d{2}))-(?P<day>(\d{2}))T(?P<hour>(\d{2})):(?P<minute>(\d{2})):(?P<second>(\d{2})).(?P<milis>(\d{3}))(?P<dir>([+-]{1}))(?P<offset>(\d{4}))$/gi;
            var pattern:RegExp = /^(?P<date>(\d{4}\/\d{2}\/\d{2})) (?P<time>(\d{2}:\d{2}:\d{2}))\.(?P<milis>(\d{3})) (?P<offset>((GMT|UTC){1}(\+|-){1}\d{4}))$/i;

            if(pattern.test(value)){               
                var date:Object = pattern.exec( value );
                if( date ){
                    var milis:Number = Date.parse( date.date+" "+date.time+" "+date.offset ) + parseInt( date.milis );
                    return (new Date( milis ));
                }
            }
            return null;    
        }

        public static function parseDate2String(value:Date):String{
            //  format: Date -> "2010/06/24 19:38:32.000 UTC+0200"
            // toTimeString == 22:50:09 GMT+0200
            if(value){
                var timePattern:RegExp = /^(?P<time>(\d{2}:\d{2}:\d{2}))(?P<rest>(.*))$/i;
                var time:Object = timePattern.exec( value.toTimeString().replace(/\s+GMT/gi,"UTC") );
                var miliseconds:String = ("000"+(value.getMilliseconds()) as String);
                return (value.fullYear+"/"                                                      // year
                        +( ((value.month+1)>9)?(value.month+1):("0"+(value.month+1)) )+"/"      // month with leading zero
                        +( (value.getDate()>9)?(value.getDate()):("0"+value.getDate()) )+" "    // day with leading zero
                        +(time.time)+"."+                                                       // time
                        miliseconds.substring( miliseconds.length-3, miliseconds.length)+" "    // miliseconds with leading zeroes
                        + time.rest);                                                           // rest
            } else {
                return "";
            }
        }
        
    }
}