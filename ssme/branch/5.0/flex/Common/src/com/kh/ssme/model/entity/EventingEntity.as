/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {
import com.kh.ssme.model.enum.ParticipationTypeEnum;
import com.kh.ssme.model.enum.StateEnum;
import com.kh.ssme.util.HashArray;

    public class EventingEntity extends BasicEntity  implements IEntity {

        public var participantType:ParticipationTypeEnum;
        public var state:StateEnum;

        public var requestUUID:String;
        public var requestName:String;
        public var requestEntity:MeetingRequestEntity;

        public var calendarUUID:String;
        public var calendarName:String;
        public var calendarEntity:CalendarEntity;

        public var timeFrameStatesEntities:HashArray;
        public var timeFrameStatesNames:HashArray;

//        public var locationStatesEntities:HashArray;
//        public var locationStatesNames:HashArray;

        //------------------------------------------------

        protected static const participantType_field:String = 'participantType';
        protected static const state_field:String = 'state';         
        protected static const request_field:String = 'request';
        protected static const calendar_field:String = 'calendar';
        protected static const timeFrameStates_field:String = 'timeFrameStates';        
//        protected static const locationStates_field:String = 'locationStates';


        //------------------------------------------------

        public function EventingEntity() {
            super();
        }

        public override function clone(entity:* = null):*{
            var newData:EventingEntity = super.clone((entity)?entity:new EventingEntity());

            // plain
            newData.participantType = participantType;
            newData.state = state;

            // entities
            newData.requestUUID = new String( requestUUID );
            newData.requestName = new String( requestName );
            newData.requestEntity = (requestEntity) ? requestEntity.clone() : null;
            newData.calendarUUID = new String( calendarUUID );
            newData.calendarName = new String( calendarName );
            newData.calendarEntity = (calendarEntity) ?  calendarEntity.clone() : null;                                

            // hasharrays
            newData.timeFrameStatesEntities = (timeFrameStatesEntities) ? timeFrameStatesEntities.clone() : null;
            newData.timeFrameStatesNames = (timeFrameStatesNames) ? timeFrameStatesNames.clone() : null;
//            newData.locationStatesEntities = (locationStatesEntities) ? locationStatesEntities.clone() : null;
//            newData.locationStatesNames = (locationStatesNames) ? locationStatesNames.clone() : null;

            return newData;
        }

        public static function createNew():EventingEntity{
            var result:EventingEntity = new EventingEntity();

            // plain
            result.participantType = ParticipationTypeEnum.CREATOR;
            result.state = StateEnum.UNDECIDED;

            // entities
            result.requestUUID = "";
            result.requestName = "";
            result.calendarUUID = "";
            result.calendarName = "";

            // hasharrays
            result.timeFrameStatesEntities = new HashArray();
            result.timeFrameStatesNames = new HashArray();
//            result.locationStatesEntities = result.locationStatesNames = new HashArray();

            return result;
        }

        //------------------------------------------------

        public override function parseJSONObject(json:Object, recursive:int = 0):* {
            super.parseJSONObject( json, recursive );
            var i:int;

            // participantType
            participantType = ParticipationTypeEnum.find( json[participantType_field] );

            // state
            state = StateEnum.find( json[state_field] );

            // request
            // calendar            
            if(recursive>0){
                requestEntity = (new MeetingRequestEntity()).parseJSONObject( json[request_field], recursive-1 );
                requestUUID = requestEntity.UUID;
                requestName = requestEntity.title;
                calendarEntity = (new CalendarEntity()).parseJSONObject( json[calendar_field], recursive-1 );
                calendarUUID = calendarEntity.UUID;
                calendarName = calendarEntity.name;
            } else {
                requestUUID = (json[request_field]) ? (json[request_field])[uuid_field] : null;
                requestName = (json[request_field]) ? (json[request_field])[readableName_field] : null;
                requestEntity = null;
                calendarUUID = (json[calendar_field]) ? (json[calendar_field])[uuid_field] : null;
                calendarName = (json[calendar_field]) ? (json[calendar_field])[readableName_field] : null;
                calendarEntity = null;                
            }

            // timeFrameStates
            timeFrameStatesEntities = new HashArray();
            timeFrameStatesNames = new HashArray();
            if(json[timeFrameStates_field].length>0){
                if(recursive>0){
                    var tf:TimeFrameEntity;
                    for(i = 0; i<json[timeFrameStates_field].length; i++){
                        tf = (new TimeFrameEntity()).parseJSONObject( (json[timeFrameStates_field][i]), recursive-1 );
                        timeFrameStatesEntities.put( tf.UUID, tf );
                        timeFrameStatesNames.put( tf.UUID, tf.title );
                    }
                } else {
                    for(i = 0; i<json[timeFrameStates_field].length; i++){
                        timeFrameStatesEntities.put( (json[timeFrameStates_field][i])[uuid_field], null );
                        timeFrameStatesNames.put( (json[timeFrameStates_field][i])[uuid_field], (json[timeFrameStates_field][i])[readableName_field] );
                    }
                }
            }

//            // locationStates
//            locationStatesEntities = new HashArray();
//            locationStatesNames = new HashArray();
//            if(json[locationStates_field].length>0){
//                if(recursive>0){
//                    var ev:EventingEntity;
//                    for(i = 0; i<json[locationStates_field].length; i++){
//                        ev = (new EventingEntity()).parseJSONObject( (json[locationStates_field][i]), recursive-1 );
//                        locationStatesEntities.put( ev.UUID, ev );
//                        locationStatesNames.put( ev.UUID, "Eventing:"+ev.UUID );
//                    }
//                } else {
//                    for(i = 0; i<json[locationStates_field].length; i++){
//                        locationStatesEntities.put( (json[locationStates_field][i])[uuid_field], null );
//                        locationStatesNames.put( (json[locationStates_field][i])[uuid_field], (json[locationStates_field][i])[readableName_field] );
//                    }
//                }
//            }

            return this;
        }

        public override function toJSONObject():Object {
            var json:Object = super.toJSONObject();
            var key:String;
            var tab:Array = new Array();

            // participantType
            json[participantType_field] = participantType.type;

            // state
            json[state_field] = state.state;

            // request
            json[request_field] = new Object();
            (json[request_field])[uuid_field] = requestUUID;
            (json[request_field])[readableName_field] = requestName;

            // calendar
            json[calendar_field] = new Object();
            (json[calendar_field])[uuid_field] = calendarUUID;
            (json[calendar_field])[readableName_field] = calendarName;

            // timeFrameStates
            tab = new Array();
            for (key in timeFrameStatesNames){
                tab.push( { uuid:key, name:"" } );
            }
            json[timeFrameStates_field] = tab;

//            // locationStates
//            tab = new Array();
//            for (key in locationStatesNames){
//                tab.push( { uuid:key, name:"" } );
//            }
//            json[locationStates_field] = tab;

            return json;
        }

    }

}