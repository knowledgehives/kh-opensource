/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.util {

    public final class LoggingComponentsEnum {

        //----------------------------------------
        // common
		public static const DEF:LoggingComponentsEnum = new LoggingComponentsEnum("Default scope","DEF");
        public static const ALL:LoggingComponentsEnum = new LoggingComponentsEnum("All applications","ALL");
        public static const CON:LoggingComponentsEnum = new LoggingComponentsEnum("Connector","CON");
        public static const LIN:LoggingComponentsEnum = new LoggingComponentsEnum("Log in","LIN");
        public static const REG:LoggingComponentsEnum = new LoggingComponentsEnum("Register","REG");
        public static const USR:LoggingComponentsEnum = new LoggingComponentsEnum("User","USR");
        public static const CAL:LoggingComponentsEnum = new LoggingComponentsEnum("Calendar","CAL");        
        public static const GRP:LoggingComponentsEnum = new LoggingComponentsEnum("Group","GRP");               
        public static const REQ:LoggingComponentsEnum = new LoggingComponentsEnum("Request","REQ");

		private static const VALUES:Object = new Object();
        {
            //static initialization
            VALUES[DEF.ID] = DEF;
            VALUES[ALL.ID] = ALL;            
            VALUES[CON.ID] = CON;
            VALUES[LIN.ID] = LIN;
            VALUES[REG.ID] = REG;
            VALUES[USR.ID] = USR;
            VALUES[CAL.ID] = CAL;                        
            VALUES[GRP.ID] = GRP;
            VALUES[REQ.ID] = REQ;             
        }

		public static function find(id:String):LoggingComponentsEnum
		{
			return VALUES[id];
		}


		// ---------- private parts ---------------
		private var id_:String;
        private var acronym_:String;
		public function LoggingComponentsEnum(id:String, acronym:String)
		{
			this.id_ = id;
            this.acronym_ = acronym;
		}

		public function get ID():String
		{
			return this.id_;
		}
		public function get acronym():String
		{
			return this.acronym_;
		}

		public function toString():String
		{
			return this.acronym_;
		}
		public function equals(type:LoggingComponentsEnum):Boolean
		{
			return (type!=null && this.id_ == type.ID);
		}
    }
}