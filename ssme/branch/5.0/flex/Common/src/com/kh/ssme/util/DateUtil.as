/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.util {

    /**
     * Provides operations on Date object
     *
     * On the base of 'DateUtil' class by Jeremy Pyne (jeremy.pyne@gmail.com)
     * http://pynej.blogspot.com/2008/10/flex-dateutil-class.html 
     */
    public class DateUtil {

        private var date_:Date;
        public static const MILLISECOND_PER_SECOND:int = 1000;
        public static const MILLISECOND_PER_MINUTE:int = 1000 * 60;
        public static const MILLISECOND_PER_HOUR:int   = 1000 * 60 * 60;
        public static const MILLISECOND_PER_DAY:int    = 1000 * 60 * 60 * 24;        


        public function DateUtil(date:Date) {
            date_ = (new Date(date.time));  // 'clone'
        }

        public static function getInstance(date:Date = null):DateUtil {
            return new DateUtil( new Date( (date)?date.time:null ) );   //'clone'
        }
        public static function getInstanceFromMilis(time:Number):DateUtil {
            return new DateUtil( new Date( time ) );   //'clone'
        }

        /**
         * Adds given number of time units to date
         * @param field
         * @param value
         * @return
         */
        public function add(field:DateProperty, value:int):DateUtil{
            if(DateProperty.DAY.equals(field))
                field = DateProperty.DATE;
            date_[field.property] += value;
            return this;
        }

        /**
         * Substracts given number of time units from date
         * @param field
         * @param value
         * @return
         */
        public function sub(field:DateProperty, value:int):DateUtil{
            if(DateProperty.DAY.equals(field))
                field = DateProperty.DATE;
            date_[field.property] -= value;
            return this;
        }

        /**
         * Returns the difference between this object and given one in provided units
         * Difference is calculated as FLOOR(this - dateUtil)
         * @param dateUtil - DateUtil instance to be compared
         * @param units - Type of unit to be used while calculation, one of MILLISECONDS, SECONDS, MINUTES, HOURS, DAY/DATE
         * @return
         */
        public function diff(dateUtil:DateUtil, units:DateProperty):Number{
            var difference:Number = this.date_.time - dateUtil.timestamp;
            if(DateProperty.MILLISECONDS.equals(units)){
                return difference;
            } else if(DateProperty.SECONDS.equals(units)){
                return Math.floor( difference/MILLISECOND_PER_SECOND );
            } else if(DateProperty.MINUTES.equals(units)){
                return Math.floor( difference/MILLISECOND_PER_MINUTE );
            } else if(DateProperty.HOURS.equals(units)){
                return Math.floor( difference/MILLISECOND_PER_HOUR );
            } else if(DateProperty.DAY.equals(units) || DateProperty.DATE.equals(units)){
                difference = this.clone().dayBegin().timestamp - dateUtil.clone().dayBegin().timestamp;
                return Math.floor( difference/MILLISECOND_PER_DAY );
            }
            return 0;
        }

        /**
         * Assigns given value to the given property of date
         * @param field
         * @param value
         * @return
         */
        public function setProperty(field:DateProperty, value:int):DateUtil{
            date_[field.property] = value;
            return this;                        
        }

        /**
         * Assign given value to the given property of date
         * @param field
         * @param value
         * @return
         */
        public function getProperty(field:DateProperty):Number{
            return date_[field.property];
        }

        /**
         * Get the Date object
         * @return
         */
        public function get date():Date{
            return (new Date(date_.time));  // 'clone'
        }

        /**
         * Get the Date object
         * @return
         */
        public function get timestamp():Number{
           return date_.time;
        }


        /**
         * Create a new DateUtil for the passed in date.  Updates will not effect the original date.
         * @return
         */
        public function clone():DateUtil{
            return new DateUtil(new Date(date_.time));
        }

        /**
         * Returns whether this DateUtil represents a time before the time represented by the specified date param
         * @param date
         * @return
         */
        public function before(date:Date):Boolean{
            return date_.time < date.time;
        }

        /**
         * Returns whether this DateUtil represents a time before the time represented by the specified date param
         * @param date
         * @return
         */
        public function beforeOrEqual(date:Date):Boolean{
            return date_.time <= date.time;
        }

        /**
         * Returns whether this DateUtil represents a time after the time represented by the specified date param
         * @param date
         * @return
         */
        public function after(date:Date):Boolean{
            return date_.time > date.time;
        }

        /**
         * Returns whether this DateUtil represents a time after the time represented by the specified date param
         * @param date
         * @return
         */
        public function afterOrEqual(date:Date):Boolean{
            return date_.time >= date.time;
        }

        /**
         * Compares the time values (millisecond offsets from the Epoch) represented by this DateUtil and given Date.
         * @param date
         * @return the value 0 if the time represented by the argument is equal to the time represented by this DateUtil;
         *      a value less than 0 if the time of this Calendar is before the time represented by the argument;
         *      and a value greater than 0 if the time of this Calendar is after the time represented by the argument. 
         */
        public function compare(date:DateUtil):int{
            return (date_.time - date.timestamp) as int;
        }

        /**
         * String representation of this DateUtil 
         * @return
         */
        public function toString():String{
            return "[DateUtil: "+date_.toString()+"]";
        }

        //----------------------------------------------
        // DAY
        /**
         * Create a new DateUtil for the current data and time.
         * @return
         */
        public static function get now():DateUtil{
            return new DateUtil(new Date());
        }

        /**
         * Create a new DateUtil for the current data and time.
         * @return
         */
        public function dayBegin():DateUtil{
            date_.hours = date_.minutes = date_.seconds = date_.milliseconds = 0;
            return this;
        }

        /**
         * Create a new DateUtil for the current data and time.
         * @return
         */
        public static function dayBegin(day:Date):DateUtil{
            var newDay:Date = new Date(day.time);
            newDay.hours = newDay.minutes = newDay.seconds = newDay.milliseconds = 0;
            return new DateUtil(newDay);
        }

        /**
         * Create a new DateUtil for the current data and time.
         * @return
         */
        public function dayEnd():DateUtil{
            date_.hours = 23;
            date_.minutes = 59;
            date_.seconds = 59;
            date_.milliseconds = 999;
            return this;
        }        

        /**
         * Create a new DateUtil for the current data and time.
         * @return
         */
        public static function dayEnd(day:Date):DateUtil{
            var newDay:Date = new Date(day.time);
            newDay.hours = 23;
            newDay.minutes = 59;
            newDay.seconds = 59;
            newDay.milliseconds = 999;
            return new DateUtil(newDay);            
        }
        //----------------------------------------------


        //----------------------------------------------
        // WEEK
        /**
         * Return midnight of the day than begins the week determined by current date
         * @param firstDay
         * @return
         */
        public function weekBegin(firstDay:int):DateUtil{
            return (this.date_.day < firstDay) ?
                   this.sub(DateProperty.DATE, 7 - (firstDay-this.date_.day)).dayBegin()
                   :
                   this.sub(DateProperty.DATE, (this.date_.day - firstDay)).dayBegin();
        }

        /**
         * Return midnight of the day than begins the week determined by given date
         * @param firstDay
         * @return
         */
        public static function weekBegin(firstDay:int, date:Date):DateUtil{
            return (new DateUtil(date).weekBegin(firstDay));
        }

        /**
         * Return day than ends the week determined by current date, time is set to 1 milisecond before midnight
         * @param firstDay
         * @return
         */
        public function weekEnd(firstDay:int):DateUtil{
            return (this.date_.day < firstDay) ?
                   this.add(DateProperty.DATE, (firstDay-this.date_.day) -1).dayEnd()
                   :
                   this.add(DateProperty.DATE, 7 - (this.date_.day - firstDay) -1).dayEnd();
        }

        /**
         * Return day than ends the week determined by given date, time is set to 1 milisecond before midnight
         * @param firstDay
         * @return
         */
        public static function weekEnd(firstDay:int, date:Date):DateUtil{
            return (new DateUtil(date).weekEnd(firstDay));
        }
        //----------------------------------------------


        //----------------------------------------------
        // MONTH        
        /**
         * Return midnight of the day than begins the month determined by current date
         * @param firstDay
         * @return
         */
        public function monthBegin():DateUtil{
            return this.setProperty(DateProperty.DATE, 1).dayBegin();
        }

        /**
         * Return midnight of the day than begins the month determined by given date
         * @param firstDay
         * @return
         */
        public static function monthBegin(date:Date):DateUtil{
            return (new DateUtil(date).monthBegin());
        }

        /**
         * Return day than ends the week determined by current date, time is set to 1 milisecond before midnight
         * @param firstDay
         * @return
         */
        public function monthEnd():DateUtil{
            return this.add(DateProperty.MONTH, 1).setProperty(DateProperty.DATE, 1).sub(DateProperty.DATE, 1).dayEnd();
        }

        /**
         * Return day than ends the week determined by given date, time is set to 1 milisecond before midnight
         * @param firstDay
         * @return
         */
        public static function monthEnd(date:Date):DateUtil{
            return (new DateUtil(date).monthEnd());
        }
        //----------------------------------------------
    }

}