/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.util
{
	public class UrlUtils
	{
		public function UrlUtils(){}
		
		//-----------------------------------------------------------------------------
		// Object of params -> URL params
        // Based on Flex 3 "mx.utils.URLUtil.objectToString()" method.
		/**
		 *  Enumerates an object's dynamic properties (by using a <code>for..in</code> loop)
		 *  and returns a String. You typically use this method to convert an ActionScript object to a String that you then append to the end of a URL.
		 *  By default, invalid URL characters are URL-encoded (converted to the <code>%XX</code> format).
		 *
	     *  <p>For example:
	     *  <pre>
	     *  var o:Object = { name: "Alex", age: 21 };
	     *  var s:String = URLUtil.objectToString(o,";",true);
	     *  trace(s);
	     *  </pre>
	     *  Prints "name=Alex;age=21" to the trace log.
	     *  </p>
		 *
		 *  @param object The object to convert to a String.
		 *  @param separator The character that separates each of the object's <code>property:value</code> pair in the String.
		 *  @param prefix The string that will prefix converted string
		 *  @param encodeURL Whether or not to URL-encode the String.
		 *
		 *  @return The string converted from object that was passed to the method.
		 */
		public static function objectToString(object:Object, separator:String=';', prefix:String=';', encodeURL:Boolean = true):String {
			var result:String = internalObjectToString(object, separator, null, encodeURL);
			return (result) ? ((result.length>0) ? prefix+result : result ) : "";
		}
		
		private static function internalObjectToString(object:Object, separator:String, prefix:String, encodeURL:Boolean):String
		{
			var s:String = "";
			var first:Boolean = true;
			
			for (var p:String in object)
			{
				if (first){
					first = false;
				} else {
					s += separator;                    
				}
				
				var value:Object = object[p];
				var name:String = prefix ? prefix + "." + p : p;
				if (encodeURL){
					name = encodeURIComponent(name);                    
				}
				
				if (value is String) {
					s += name + '=' + (encodeURL ? encodeURIComponent(value as String) : value);
				} else if (value is Number) {
					value = value.toString();
					if (encodeURL){
						value = encodeURIComponent(value as String);                        
					}
					
					s += name + '=' + value;
				} else if (value is Boolean){
					s += name + '=' + (value ? "true" : "false");
				} else if (value is Array){
					s += internalArrayToString(value as Array, separator, name, encodeURL);
				}
			}
			return s;
		}
		
		private static function internalArrayToString(array:Array, separator:String, prefix:String, encodeURL:Boolean):String
		{
			var s:String = "";
			var first:Boolean = true;
			
			var n:int = array.length;
			for (var i:int = 0; i < n; i++)
			{
				if (first) {
					first = false;
				} else {
					s += separator;                    
				}
				
				
				var value:Object = array[i];
				var name:String = prefix;     //  instead of prefix + "." + i;
				if (encodeURL)
					name = encodeURIComponent(name);
				
				if (value is String) {
					s += name + '=' + (encodeURL ? encodeURIComponent(value as String) : value);
				} else if (value is Number) {
					value = value.toString();
					if (encodeURL)
						value = encodeURIComponent(value as String);
					
					s += name + '=' + value;
				} else if (value is Boolean) {
					s += name + '=' + (value ? "true" : "false");
				} 
			}
			return s;
		}
		//-----------------------------------------------------------------------------



        //-----------------------------------------------------------------------------
        public static function decodeURL(input:String):String{
            var result:String = new String(input);
            for (var key:String in URIToString){
                if(key == "%25")    continue;   // need to be replaced last
                result = result.replace(new RegExp(key,"g"),URIToString[key]);
            }
            result.replace("%25","%");
            return result;
        }
        public static function encodeURL(input:String):String{
            var result:String = new String(input);
            for (var key:String in stringToURI){
                if(key == "%")    continue;   // need to be replaced last
                result = result.replace(new RegExp(key,"g"),URIToString[key]);
            }
            result.replace("%","%25");
            return result;
        }
        public static var stringToURI:HashArray = new HashArray();
        stringToURI.put(" ","%20");
        stringToURI.put("!","%21");
        stringToURI.put("\"","%22");
        stringToURI.put("#","%23");
        stringToURI.put("$","%24");
        stringToURI.put("%","%25");
        stringToURI.put("&","%26");
        stringToURI.put("'","%27");
        stringToURI.put("(","%28");
        stringToURI.put(")","%29");
        stringToURI.put("*","%2A");
        stringToURI.put("+","%2B");
        stringToURI.put(",","%2C");
        stringToURI.put("-","%2D");
        stringToURI.put(".","%2E");
        stringToURI.put("/","%2F");
        stringToURI.put("0","%30");
        stringToURI.put("1","%31");
        stringToURI.put("2","%32");
        stringToURI.put("3","%33");
        stringToURI.put("4","%34");
        stringToURI.put("5","%35");
        stringToURI.put("6","%36");
        stringToURI.put("7","%37");
        stringToURI.put("8","%38");
        stringToURI.put("9","%39");
        stringToURI.put(":","%3A");
        stringToURI.put(";","%3B");
        stringToURI.put("<","%3C");
        stringToURI.put("=","%3D");
        stringToURI.put(">","%3E");
        stringToURI.put("?","%3F");
        stringToURI.put("@","%40");
        stringToURI.put("A","%41");
        stringToURI.put("B","%42");
        stringToURI.put("C","%43");
        stringToURI.put("D","%44");
        stringToURI.put("E","%45");
        stringToURI.put("F","%46");
        stringToURI.put("G","%47");
        stringToURI.put("H","%48");
        stringToURI.put("I","%49");
        stringToURI.put("J","%4A");
        stringToURI.put("K","%4B");
        stringToURI.put("L","%4C");
        stringToURI.put("M","%4D");
        stringToURI.put("N","%4E");
        stringToURI.put("O","%4F");
        stringToURI.put("P","%50");
        stringToURI.put("Q","%51");
        stringToURI.put("R","%52");
        stringToURI.put("S","%53");
        stringToURI.put("T","%54");
        stringToURI.put("U","%55");
        stringToURI.put("V","%56");
        stringToURI.put("W","%57");
        stringToURI.put("X","%58");
        stringToURI.put("Y","%59");
        stringToURI.put("Z","%5A");
        stringToURI.put("[","%5B");
        stringToURI.put("\\","%5C");
        stringToURI.put("]","%5D");
        stringToURI.put("^","%5E");
        stringToURI.put("_","%5F");
        stringToURI.put("`","%60");
        stringToURI.put("a","%61");
        stringToURI.put("b","%62");
        stringToURI.put("c","%63");
        stringToURI.put("d","%64");
        stringToURI.put("e","%65");
        stringToURI.put("f","%66");
        stringToURI.put("g","%67");
        stringToURI.put("h","%68");
        stringToURI.put("i","%69");
        stringToURI.put("j","%6A");
        stringToURI.put("k","%6B");
        stringToURI.put("l","%6C");
        stringToURI.put("m","%6D");
        stringToURI.put("n","%6E");
        stringToURI.put("o","%6F");
        stringToURI.put("p","%70");
        stringToURI.put("q","%71");
        stringToURI.put("r","%72");
        stringToURI.put("s","%73");
        stringToURI.put("t","%74");
        stringToURI.put("u","%75");
        stringToURI.put("v","%76");
        stringToURI.put("w","%77");
        stringToURI.put("x","%78");
        stringToURI.put("y","%79");
        stringToURI.put("z","%7A");
        stringToURI.put("{","%7B");
        stringToURI.put("|","%7C");
        stringToURI.put("}","%7D");
        stringToURI.put("~","%7E");
        stringToURI.put(" ","%7F");
        stringToURI.put("�","%80");
        stringToURI.put(" ","%81");
        stringToURI.put("�","%82");
        stringToURI.put("?","%83");
        stringToURI.put("�","%84");
        stringToURI.put("�","%85");
        stringToURI.put("�","%86");
        stringToURI.put("�","%87");
        stringToURI.put("?","%88");
        stringToURI.put("�","%89");
        stringToURI.put("�","%8A");
        stringToURI.put("�","%8B");
        stringToURI.put("?","%8C");
        stringToURI.put(" ","%8D");
        stringToURI.put("�","%8E");
        stringToURI.put(" ","%8F");
        stringToURI.put(" ","%90");
        stringToURI.put("�","%91");
        stringToURI.put("�","%92");
        stringToURI.put("�","%93");
        stringToURI.put("�","%94");
        stringToURI.put("�","%95");
        stringToURI.put("�","%96");
        stringToURI.put("�","%97");
        stringToURI.put("?","%98");
        stringToURI.put("�","%99");
        stringToURI.put("�","%9A");
        stringToURI.put("�","%9B");
        stringToURI.put("?","%9C");
        stringToURI.put(" ","%9D");
        stringToURI.put("�","%9E");
        stringToURI.put("?","%9F");
        stringToURI.put(" ","%A0");
        stringToURI.put("?","%A1");
        stringToURI.put("?","%A2");
        stringToURI.put("?","%A3");
        stringToURI.put(" ","%A4");
        stringToURI.put("?","%A5");
        stringToURI.put("|","%A6");
        stringToURI.put("�","%A7");
        stringToURI.put("�","%A8");
        stringToURI.put("�","%A9");
        stringToURI.put("?","%AA");
        stringToURI.put("�","%AB");
        stringToURI.put("�","%AC");
        stringToURI.put("?","%AD");
        stringToURI.put("�","%AE");
        stringToURI.put("?","%AF");
        stringToURI.put("�","%B0");
        stringToURI.put("�","%B1");
        stringToURI.put("?","%B2");
        stringToURI.put("?","%B3");
        stringToURI.put("�","%B4");
        stringToURI.put("�","%B5");
        stringToURI.put("�","%B6");
        stringToURI.put("�","%B7");
        stringToURI.put("�","%B8");
        stringToURI.put("?","%B9");
        stringToURI.put("?","%BA");
        stringToURI.put("�","%BB");
        stringToURI.put("?","%BC");
        stringToURI.put("?","%BD");
        stringToURI.put("?","%BE");
        stringToURI.put("?","%BF");
        stringToURI.put("?","%C0");
        stringToURI.put("�","%C1");
        stringToURI.put("�","%C2");
        stringToURI.put("?","%C3");
        stringToURI.put("�","%C4");
        stringToURI.put("?","%C5");
        stringToURI.put("?","%C6");
        stringToURI.put("�","%C7");
        stringToURI.put("?","%C8");
        stringToURI.put("�","%C9");
        stringToURI.put("?","%CA");
        stringToURI.put("�","%CB");
        stringToURI.put("?","%CC");
        stringToURI.put("�","%CD");
        stringToURI.put("�","%CE");
        stringToURI.put("?","%CF");
        stringToURI.put("?","%D0");
        stringToURI.put("?","%D1");
        stringToURI.put("?","%D2");
        stringToURI.put("�","%D3");
        stringToURI.put("�","%D4");
        stringToURI.put("?","%D5");
        stringToURI.put("�","%D6");
        stringToURI.put(" ","%D7");
        stringToURI.put("?","%D8");
        stringToURI.put("?","%D9");
        stringToURI.put("�","%DA");
        stringToURI.put("?","%DB");
        stringToURI.put("�","%DC");
        stringToURI.put("�","%DD");
        stringToURI.put("?","%DE");
        stringToURI.put("�","%DF");
        stringToURI.put("?","%E0");
        stringToURI.put("�","%E1");
        stringToURI.put("�","%E2");
        stringToURI.put("?","%E3");
        stringToURI.put("�","%E4");
        stringToURI.put("?","%E5");
        stringToURI.put("?","%E6");
        stringToURI.put("�","%E7");
        stringToURI.put("?","%E8");
        stringToURI.put("�","%E9");
        stringToURI.put("?","%EA");
        stringToURI.put("�","%EB");
        stringToURI.put("?","%EC");
        stringToURI.put("�","%ED");
        stringToURI.put("�","%EE");
        stringToURI.put("?","%EF");
        stringToURI.put("?","%F0");
        stringToURI.put("?","%F1");
        stringToURI.put("?","%F2");
        stringToURI.put("�","%F3");
        stringToURI.put("�","%F4");
        stringToURI.put("?","%F5");
        stringToURI.put("�","%F6");
        stringToURI.put("�","%F7");
        stringToURI.put("?","%F8");
        stringToURI.put("?","%F9");
        stringToURI.put("�","%FA");
        stringToURI.put("?","%FB");
        stringToURI.put("�","%FC");
        stringToURI.put("�","%FD");
        stringToURI.put("?","%FE");
        stringToURI.put("?","%FF");

        public static var URIToString:HashArray = new HashArray();
        URIToString.put("%20", " ");
        URIToString.put("%21","!");
        URIToString.put("%22","\"");
        URIToString.put("%23","#");
        URIToString.put("%24","$");
        URIToString.put("%25","%");
        URIToString.put("%26","&");
        URIToString.put("%27","'");
        URIToString.put("%28","(");
        URIToString.put("%29",")");
        URIToString.put("%2A","*");
        URIToString.put("%2B","+");
        URIToString.put("%2C",",");
        URIToString.put("%2D","-");
        URIToString.put("%2E",".");
        URIToString.put("%2F","/");
        URIToString.put("%30","0");
        URIToString.put("%31","1");
        URIToString.put("%32","2");
        URIToString.put("%33","3");
        URIToString.put("%34","4");
        URIToString.put("%35","5");
        URIToString.put("%36","6");
        URIToString.put("%37","7");
        URIToString.put("%38","8");
        URIToString.put("%39","9");
        URIToString.put("%3A",":");
        URIToString.put("%3B",";");
        URIToString.put("%3C","<");
        URIToString.put("%3D","=");
        URIToString.put("%3E",">");
        URIToString.put("%3F","?");
        URIToString.put("%40","@");
        URIToString.put("%41","A");
        URIToString.put("%42","B");
        URIToString.put("%43","C");
        URIToString.put("%44","D");
        URIToString.put("%45","E");
        URIToString.put("%46","F");
        URIToString.put("%47","G");
        URIToString.put("%48","H");
        URIToString.put("%49","I");
        URIToString.put("%4A","J");
        URIToString.put("%4B","K");
        URIToString.put("%4C","L");
        URIToString.put("%4D","M");
        URIToString.put("%4E","N");
        URIToString.put("%4F","O");
        URIToString.put("%50","P");
        URIToString.put("%51","Q");
        URIToString.put("%52","R");
        URIToString.put("%53","S");
        URIToString.put("%54","T");
        URIToString.put("%55","U");
        URIToString.put("%56","V");
        URIToString.put("%57","W");
        URIToString.put("%58","X");
        URIToString.put("%59","Y");
        URIToString.put("%5A","Z");
        URIToString.put("%5B","[");
        URIToString.put("%5C","\\");
        URIToString.put("%5D","]");
        URIToString.put("%5E","^");
        URIToString.put("%5F","_");
        URIToString.put("%60","`");
        URIToString.put("%61","a");
        URIToString.put("%62","b");
        URIToString.put("%63","c");
        URIToString.put("%64","d");
        URIToString.put("%65","e");
        URIToString.put("%66","f");
        URIToString.put("%67","g");
        URIToString.put("%68","h");
        URIToString.put("%69","i");
        URIToString.put("%6A","j");
        URIToString.put("%6B","k");
        URIToString.put("%6C","l");
        URIToString.put("%6D","m");
        URIToString.put("%6E","n");
        URIToString.put("%6F","o");
        URIToString.put("%70","p");
        URIToString.put("%71","q");
        URIToString.put("%72","r");
        URIToString.put("%73","s");
        URIToString.put("%74","t");
        URIToString.put("%75","u");
        URIToString.put("%76","v");
        URIToString.put("%77","w");
        URIToString.put("%78","x");
        URIToString.put("%79","y");
        URIToString.put("%7A","z");
        URIToString.put("%7B","{");
        URIToString.put("%7C","|");
        URIToString.put("%7D","}");
        URIToString.put("%7E","~");
        URIToString.put("%7F"," ");
        URIToString.put("%80","�");
        URIToString.put("%81"," ");
        URIToString.put("%82","�");
        URIToString.put("%83","?");
        URIToString.put("%84","�");
        URIToString.put("%85","�");
        URIToString.put("%86","�");
        URIToString.put("%87","�");
        URIToString.put("%88","?");
        URIToString.put("%89","�");
        URIToString.put("%8A","�");
        URIToString.put("%8B","�");
        URIToString.put("%8C","?");
        URIToString.put("%8D"," ");
        URIToString.put("%8E","�");
        URIToString.put("%8F"," ");
        URIToString.put("%90"," ");
        URIToString.put("%91","�");
        URIToString.put("%92","�");
        URIToString.put("%93","�");
        URIToString.put("%94","�");
        URIToString.put("%95","�");
        URIToString.put("%96","�");
        URIToString.put("%97","�");
        URIToString.put("%98","?");
        URIToString.put("%99","�");
        URIToString.put("%9A","�");
        URIToString.put("%9B","�");
        URIToString.put("%9C","?");
        URIToString.put("%9D"," ");
        URIToString.put("%9E","�");
        URIToString.put("%9F","?");
        URIToString.put("%A0"," ");
        URIToString.put("%A1","?");
        URIToString.put("%A2","?");
        URIToString.put("%A3","?");
        URIToString.put("%A4"," ");
        URIToString.put("%A5","?");
        URIToString.put("%A6","|");
        URIToString.put("%A7","�");
        URIToString.put("%A8","�");
        URIToString.put("%A9","�");
        URIToString.put("%AA","?");
        URIToString.put("%AB","�");
        URIToString.put("%AC","�");
        URIToString.put("%AD","?");
        URIToString.put("%AE","�");
        URIToString.put("%AF","?");
        URIToString.put("%B0","�");
        URIToString.put("%B1","�");
        URIToString.put("%B2","?");
        URIToString.put("%B3","?");
        URIToString.put("%B4","�");
        URIToString.put("%B5","�");
        URIToString.put("%B6","�");
        URIToString.put("%B7","�");
        URIToString.put("%B8","�");
        URIToString.put("%B9","?");
        URIToString.put("%BA","?");
        URIToString.put("%BB","�");
        URIToString.put("%BC","?");
        URIToString.put("%BD","?");
        URIToString.put("%BE","?");
        URIToString.put("%BF","?");
        URIToString.put("%C0","?");
        URIToString.put("%C1","�");
        URIToString.put("%C2","�");
        URIToString.put("%C3","?");
        URIToString.put("%C4","�");
        URIToString.put("%C5","?");
        URIToString.put("%C6","?");
        URIToString.put("%C7","�");
        URIToString.put("%C8","?");
        URIToString.put("%C9","�");
        URIToString.put("%CA","?");
        URIToString.put("%CB","�");
        URIToString.put("%CC","?");
        URIToString.put("%CD","�");
        URIToString.put("%CE","�");
        URIToString.put("%CF","?");
        URIToString.put("%D0","?");
        URIToString.put("%D1","?");
        URIToString.put("%D2","?");
        URIToString.put("%D3","�");
        URIToString.put("%D4","�");
        URIToString.put("%D5","?");
        URIToString.put("%D6","�");
        URIToString.put("%D7"," ");
        URIToString.put("%D8","?");
        URIToString.put("%D9","?");
        URIToString.put("%DA","�");
        URIToString.put("%DB","?");
        URIToString.put("%DC","�");
        URIToString.put("%DD","�");
        URIToString.put("%DE","?");
        URIToString.put("%DF","�");
        URIToString.put("%E0","?");
        URIToString.put("%E1","�");
        URIToString.put("%E2","�");
        URIToString.put("%E3","?");
        URIToString.put("%E4","�");
        URIToString.put("%E5","?");
        URIToString.put("%E6","?");
        URIToString.put("%E7","�");
        URIToString.put("%E8","?");
        URIToString.put("%E9","�");
        URIToString.put("%EA","?");
        URIToString.put("%EB","�");
        URIToString.put("%EC","?");
        URIToString.put("%ED","�");
        URIToString.put("%EE","�");
        URIToString.put("%EF","?");
        URIToString.put("%F0","?");
        URIToString.put("%F1","?");
        URIToString.put("%F2","?");
        URIToString.put("%F3","�");
        URIToString.put("%F4","�");
        URIToString.put("%F5","?");
        URIToString.put("%F6","�");
        URIToString.put("%F7","�");
        URIToString.put("%F8","?");
        URIToString.put("%F9","?");
        URIToString.put("%FA","�");
        URIToString.put("%FB","?");
        URIToString.put("%FC","�");
        URIToString.put("%FD","�");
        URIToString.put("%FE","?");
        URIToString.put("%FF","?");
        //-----------------------------------------------------------------------------
		
	}
}