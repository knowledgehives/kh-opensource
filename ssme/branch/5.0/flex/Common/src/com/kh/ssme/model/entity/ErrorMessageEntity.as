/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {
    import com.kh.ssme.util.JSONObject;

    public final class ErrorMessageEntity implements IEntity{

        public var errorStatus:String;
        public var errorMessage:String;        
        
        public function ErrorMessageEntity() {
            super();
        }

        public function clone(entity:* = null):* {
            var newData:ErrorMessageEntity = new ErrorMessageEntity();
            newData.errorStatus = new String(errorStatus);
            newData.errorMessage = new String(errorMessage);            
            return newData;
        }        

        public function parseJSONObject(json:Object, recursive:int = 0):* {

            errorStatus = json['errorStatus'];
            errorMessage = json['errorMessage'];
            return this;
        }

        public function toJSONObject():Object{
            var json:Object = new Object();

            json['errorStatus'] = errorStatus;
            json['errorMessage'] = errorMessage;

            return json;
        }

        public function parseJSONString(json:String, recursive:int = 0):* {
            var jsonEntity:Object = JSONObject.decode(json);
            return parseJSONObject( jsonEntity, recursive );
        }

        public function toJSONString():String {
            var jsonObject:Object = toJSONObject();
            var result:String = JSONObject.encode( jsonObject );
            return result;
        }   

    }

}
