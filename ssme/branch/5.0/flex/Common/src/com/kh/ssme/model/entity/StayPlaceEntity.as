/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {
    import com.kh.ssme.util.HashArray;
    import com.kh.ssme.util.JSONObject;

    public final class StayPlaceEntity extends BasicEntity implements IEntity {

        public var locationUUID:String;

        public var locationName:String;

        public var userUUID:String;

        public var userName:String;

        public var timeFrames:HashArray;

        public function StayPlaceEntity() {
            super();
        }

        public override function parseJSONObject(json:Object, recursive:int = 0):* {
            super.parseJSONObject( json );

//            locationUUID = (json['location']) ? (json['location'])['uuid'] : null;
//            locationName = (json['location']) ? (json['location'])['name'] : null;
//            timeFrames = new HashArray();
//            for(var i:int = 0; i<json['timeFrames'].length; i++){
//                timeFrames.put( (json['timeFrames'][i])['uuid'], (json['timeFrames'][i])['name'] );
//            }
//            //userUUID = (json['user']) ? (json['user'])['uuid'] : null;
//            //userName = (json['user']) ? (json['user'])['name'] : null;
//            return this;
//        }
//
//        public override function toJSONObject():Object {
//            var json:Object = super.toJSONObject();
//            var tab:Array = new Array();
//
//            json['location'] = new Object();
//            (json['location'])['uuid'] = locationUUID;
//            (json['location'])['uuid'] = locationName;
//            for (var key:String in timeFrames){
//                tab.push( { uuid:key, name:timeFrames[key] } );
//            }
//            json['timeFrames'] = tab;
////            json[''] = userUUID;
////            json[''] = userName;
            return json;
        }

        public override function toJSONObject():Object {
            var json:Object = super.toJSONObject();
            var tab:Array = new Array();
            var key:String;

//
//
//            

            return json;
        }


    }

}