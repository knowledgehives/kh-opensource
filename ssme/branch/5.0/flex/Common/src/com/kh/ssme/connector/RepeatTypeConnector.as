/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.connector {

    import com.kh.ssme.model.entity.RepeatTypeEntity;

    [Event(name="ssme_repeatType_deleteWithChildren",  type="com.kh.ssme.util.SsmeEvent")]

    public class RepeatTypeConnector extends BasicQueuedConnector {

        /**
         * Path to proper servlet
         */
        private static const repeatTypeServlet:String = "repeatType/";
        public override function get servlet():String{
            return repeatTypeServlet;
        }
        public override function getResourceURL(uuid:String):String{
            return baseURL + repeatTypeServlet + uuid;
        }        

        public function RepeatTypeConnector(url:String) {
            super(url);
        } 

        public function getRepeatType( uuid:String, params:Object = null ):void{
            processCommand( ACTION_GET, EVENT_GET, uuid, null, params );
        }

		public static const EVENT_DELETE_WITH_CHILDREN:String = "ssme_repeatType_deleteWithChildren";        

        public function addRepeatType( data:RepeatTypeEntity = null, params:Object = null ):void{
            processCommand( ACTION_CREATE, EVENT_CREATE, null, (data)?data.toJSONString():"", params );
        }

        public function updateRepeatType( uuid:String, data:RepeatTypeEntity = null, params:Object = null ):void{
            processCommand( ACTION_UPDATE, EVENT_UPDATE, uuid, (data)?data.toJSONString():"", params );
        }

        public function deleteRepeatType( uuid:String, params:Object = null ):void{
            processCommand( ACTION_DELETE, EVENT_DELETE, uuid, "", params );
        }

        public function deleteRepeatTypeWithChildren( uuid:String, params:Object = null ):void{
            if(!params) params = {};
            params[Params.REMOVE_CHILDREN_EVENTS] = true;            
            processCommand( ACTION_DELETE, EVENT_DELETE_WITH_CHILDREN, uuid, "", params );
        }        

    }

}