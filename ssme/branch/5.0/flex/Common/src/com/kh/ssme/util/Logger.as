/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.util {
    import flash.events.Event;
    import flash.external.ExternalInterface;
    import flash.system.Capabilities;
    import flash.system.System;
    import flash.utils.getTimer;
    import mx.core.Application;
    import mx.formatters.DateFormatter;

    public final class Logger {

        public static const at_exp:RegExp = /\W*(at)\W*/i;
		private static var JS_LOGGING_ENABLED:Boolean = (Application.application.parameters.jsloggingenabled == "true");

        private static const date_formatter:DateFormatter = new DateFormatter();
        {   //static
            date_formatter.formatString = "YYYY-MM-DD HH:NN:SS";
        }

        public static function errorEvent(message:*, event:Event, component:LoggingComponentsEnum = null):void {
            if (event){
                _print("[ERROR] "+message +" event:{" + event.toString() + "} ", component, true);
            } else {
                _print("[ERROR] "+message, component, true);
            }
        }

        public static function error(message:*, error:Error, component:LoggingComponentsEnum = null):void {
            if (error){
                _print("[ERROR] "+message +" name:{" + error.name + "} message:{" + error.message + "} stack:\n" + error.getStackTrace(), component, true);
            } else {
                _print("[ERROR] "+message, component, true);
            }
        }

        public static function debug(message:*, component:LoggingComponentsEnum = null):void {
            _print(message, component);
        }

        public static function debugWithStackTrace(message:*, component:LoggingComponentsEnum = null, depth:int = 2 , breakLine:Boolean = false):void{
            _print(message, component, false, depth, breakLine);
        }

        private static function _print(message:*, component:LoggingComponentsEnum = null, isError:Boolean = false, depth:int = 1, breakLine:Boolean = false):void {
            if(component==null) component = LoggingComponentsEnum.DEF;

            if(isError
                    || component == LoggingComponentsEnum.DEF               
                    || component == LoggingComponentsEnum.CON
                    || component == LoggingComponentsEnum.LIN
                    || component == LoggingComponentsEnum.REG
                    || component == LoggingComponentsEnum.USR
                    || component == LoggingComponentsEnum.GRP

                ){
                var now:Date = new Date();
                if(Capabilities.isDebugger){

                    try {
                        throwError();
                    } catch (e:Error) {
                        var stackTrace:Array = e.getStackTrace().split("\n");
                        var stack:String = "";
                        var temp:String, number:String;
                        for(var i:int = 4, j:int = 0; i<stackTrace.length && j<depth; ){
                            temp = stackTrace[i].replace(at_exp,"").replace(/^[^:]*::/i,"");
                            if(temp.match(/(as|mxml):.*$/i)){
                                number = temp.match(/(as|mxml):.*$/i)[0];
                                number = number.match(/[0-9]+/i)[0];
                            } else {
                                number = "";
                            }
                            temp = temp.match(/[^\[]+/i)[0];
                            temp = temp.replace(/\//i,((number.length>0)?":"+number:"")+"#");
                            stack += temp;
                            i++; j++;
                            if(i<stackTrace.length && j<depth){
                                if(breakLine){
                                    stack += " \n ";
                                } else {
                                    stack += " <- ";
                                }
                            }
                        }

                        var msg:String = "["+component+"]"+
                                        "[TIME:"+(date_formatter.format(now))+"."+now.getMilliseconds()+"]" +
                                        "[TIMER:"+getTimer()+"]"+
                                        "\""+message+"\" : " +
                                        "[MEM:"+(System.totalMemory/1024)+"kB]" +
                                        "[STACK:"+stack+"]";

                        trace(msg);
						printToLoggingArea_(msg);
                        printToConsole_(msg);
                    }
                }
				else
				{
					var msg_:String = "["+component+"][TIME:"+(date_formatter.format(now))+"."+now.getMilliseconds()+"]"+message;

                    trace(msg);                    
                    printToLoggingArea_(msg);
                    printToConsole_(msg);
                }
            }
        }

		/**
		 * Attempts to log to javascript console.
		 * @return true if logging to JS was successful
		 */
		private static function printToConsole_(text:String):Boolean
		{
			if (ExternalInterface.available && JS_LOGGING_ENABLED)
			{
				try
				{
					var txt:String = text.replace(/=\"\"/gi, '=\\"\\"').replace(/([^\\])["]/gi,'$1\\"').replace(/\n|\t|\r/gi," ");
					var log:String = " function(){ try { console.log(\""+txt+"\"); }catch(e){}; }";
					ExternalInterface.call(log);
				}
				catch(error:Error){
                    //we do not have any place to log anyway    
                }
				return true;
			}
			return false;
		}

		/**
		 * Attempts to write given log to logging area.
		 * @return true if logging area exists
		 */
		private static function printToLoggingArea_(text:String):Boolean
		{

			var log:Object = null;
			try {
                log = Application.application["loggingArea"];
            } catch(ex:Error) {
                //we do not have any place to log anyway
            }
			if (log)
			{
				log.text += text+"\n";
				return true;
			}
			return false;
		}

        private static function throwError():void {
            throw new Error();
        }
    }
}