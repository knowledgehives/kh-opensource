/*
 * SmartSchedule.me - Semantic Calendar
 * Copyright (C) 2009 - 2010,  Knowledge Hives sp. z o.o.
 *
 * Contribution from: Gdansk University of Technology, Poland
 * Invented by: Sebastian R. Kruk
 * Implemented by: Micha� Szopi�ski, Mariusz Cygan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.kh.ssme.model.entity {
    import com.kh.ssme.model.enum.StateEnum;
    import com.kh.ssme.util.HashArray;
import com.kh.ssme.util.JSONObject;

public class MeetingRequestEntity extends BasicEntity  implements IEntity {

        public var notSoonerThen:Date;
        public var notLaterThen:Date;
        public var state:StateEnum;
        public var title:String;
        public var description:String;

//        public var decidedPlaceUUID:String;
//        public var decidedPlaceName:String;
//        public var decidedPlaceEntity:UserEntity;

        public var eventingEntities:HashArray;
        public var eventingNames:HashArray;

        public var decidedTimesEntities:HashArray;
        public var decidedTimesNames:HashArray;

        //------------------------------------------------

        protected static const notSoonerThen_field:String = "notSoonerThen";
        protected static const notLaterThen_field:String = "notLaterThen";
        protected static const state_field:String = "state";
        protected static const decidedTimes_field:String = "decidedTimes";
//        protected static const decidedPlace_field:String = "decidedPlace";
        protected static const eventing_field:String = "eventing";
        protected static const title_field:String = "title";
        protected static const description_field:String = "description";

        //------------------------------------------------

        public function MeetingRequestEntity() {
            super();
        }

        public override function clone(entity:* = null):*{
            var newData:MeetingRequestEntity = super.clone((entity)?entity:new MeetingRequestEntity());

            // plain
            newData.notSoonerThen = new Date( notSoonerThen );
            newData.notLaterThen = new Date( notLaterThen );
            newData.state = state;
            newData.title = new String( title );
            newData.description = new String( description );

            // entities
//            newData.decidedPlaceUUID = new String( decidedPlaceUUID );
//            newData.decidedPlaceName = new String( decidedPlaceName );
//            newData.decidedPlaceEntity = decidedPlaceEntity.clone();

            // hasharrays
            newData.eventingEntities = (eventingEntities) ? eventingEntities.clone() : null;
            newData.eventingNames = (eventingNames) ? eventingNames.clone() : null;
            newData.decidedTimesEntities = (decidedTimesEntities) ? decidedTimesEntities.clone() : null;
            newData.decidedTimesNames = (decidedTimesNames) ? decidedTimesNames.clone() : null;

            return newData;
        }

        public static function createNew(title:String, description:String):MeetingRequestEntity{
            var result:MeetingRequestEntity = new MeetingRequestEntity();

            result.notSoonerThen = null;
            result.notLaterThen = null;
            result.state = StateEnum.UNDECIDED;
            result.title = new String( title );
            result.description = new String( description );

//            result.decidedPlaceUUID = "";
//            result.decidedPlaceName = "";
//            result.decidedPlaceEntity = null;

            result.eventingEntities = new HashArray();
            result.eventingNames = new HashArray();
            result.decidedTimesEntities = new HashArray();
            result.decidedTimesNames = new HashArray();            

            return result;
        }

        //------------------------------------------------

        public override function parseJSONObject(json:Object, recursive:int = 0):* {
            super.parseJSONObject( json, recursive );
            var i:int;

            // notSoonerThen
            notSoonerThen = JSONObject.parseString2Date( json[notSoonerThen_field] );

            // notLaterThen
            notLaterThen = JSONObject.parseString2Date( json[notLaterThen_field] );

            // state
            state = StateEnum.find( json[state_field] );

            // title
            title = json[title_field];

            // description
            description = json[description_field];

            // decidedPlace
//            if(recursive>0){
//                decidedPlaceEntity = (new LocationEntity()).parseJSONObject( json[decidedPlace_field], recursive-1 );
//                decidedPlaceUUID = decidedPlaceEntity.UUID;
//                decidedPlaceName = decidedPlaceEntity.name;
//            } else {
//                decidedPlaceUUID = (json[decidedPlace_field]) ? (json[decidedPlace_field])[uuid_field] : null;
//                decidedPlaceName = (json[decidedPlace_field]) ? (json[decidedPlace_field])[readableName_field] : null;
//                decidedPlaceEntity = null;
//            }

            // eventing
            eventingEntities = new HashArray();
            eventingNames = new HashArray();            
            if(json[eventing_field].length>0){
                if(recursive>0){
                    var ev:EventingEntity;
                    for(i = 0; i<json[eventing_field].length; i++){
                        ev = (new EventingEntity()).parseJSONObject( (json[eventing_field][i]), recursive-1 );
                        eventingEntities.put( ev.UUID, ev );
                        eventingNames.put( ev.UUID, "eventing:"+ev.UUID );
                    }
                } else {
                    for(i = 0; i<json[eventing_field].length; i++){
                        eventingEntities.put( (json[eventing_field][i])[uuid_field], null );
                        eventingNames.put( (json[eventing_field][i])[uuid_field], (json[eventing_field][i])[readableName_field] );
                    }
                }
            }

            // decidedTimes
            decidedTimesEntities = new HashArray();
            decidedTimesNames = new HashArray();
            if(json[decidedTimes_field].length>0){
                if(recursive>0){
                    var tf:TimeFrameEntity;
                    for(i = 0; i<json[decidedTimes_field].length; i++){
                        tf = (new TimeFrameEntity()).parseJSONObject( (json[decidedTimes_field][i]), recursive-1 );
                        decidedTimesEntities.put( tf.UUID, tf );
                        decidedTimesNames.put( tf.UUID, tf.title );
                    }
                } else {
                    for(i = 0; i<json[decidedTimes_field].length; i++){
                        decidedTimesEntities.put( (json[decidedTimes_field][i])[uuid_field], null );
                        decidedTimesNames.put( (json[decidedTimes_field][i])[uuid_field], (json[decidedTimes_field][i])[readableName_field] );
                    }
                }
            }

            return this;
        }

        public override function toJSONObject():Object {
            var json:Object = super.toJSONObject();
            var key:String;
            var tab:Array;

            // notSoonerThen
            json[notSoonerThen_field] = JSONObject.parseDate2String( notSoonerThen );

            // notLaterThen
            json[notLaterThen_field] =  JSONObject.parseDate2String( notLaterThen );

            // state
            json[state_field] = state.state;

            // title
            json[title_field] = title;

            // description
            json[description_field] = description;

            // decidedPlace
//            json[decidedPlace_field] = new Object();
//            (json[decidedPlace_field])[uuid_field] = decidedPlaceUUID;
//            (json[decidedPlace_field])[readableName_field] = decidedPlaceName;

            // eventing
            tab = new Array();
            for (key in eventingNames){
                tab.push( { uuid:key, name:"" } );
            }
            json[eventing_field] = tab;

            // decidedTimes
            tab = new Array();
            for (key in decidedTimesNames){
                tab.push( { uuid:key, name:"" } );
            }
            json[decidedTimes_field] = tab;


            return json;
        }


    }

}