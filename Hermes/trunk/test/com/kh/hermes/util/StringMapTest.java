/*
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.kh.hermes.util;

import java.util.HashMap;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;

public class StringMapTest {

	@Test
	public void test1() throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		map.put("a", "b");
		map.put("c", "d");

		System.out.println(map);
		System.out.println(StringMap.convert(map));
		System.out.println(StringMap.convert(StringMap.convert(map)));

		Assert.assertEquals(map, StringMap.convert(StringMap.convert(map)));
	}

	@Test
	public void test2() throws Exception {
		Map<String, String> map = new HashMap<String, String>();

		System.out.println(map);
		System.out.println(StringMap.convert(map));
		System.out.println(StringMap.convert(StringMap.convert(map)));

		Assert.assertEquals(map, StringMap.convert(StringMap.convert(map)));
	}

	@Test
	public void test3() throws Exception {
		String string = "a=b;c=d";

		System.out.println(StringMap.convert(string));
	}

	@Test
	public void test4() throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		map.put(",", "=");
		map.put(" ", " ");
		map.put("  ", " ");

		System.out.println(map);
		System.out.println(StringMap.convert(map));
		System.out.println(StringMap.convert(StringMap.convert(map)));

		Assert.assertEquals(map, StringMap.convert(StringMap.convert(map)));
	}
	
	@Test
	public void test5() throws Exception {
		Map<String, String> map = StringMap.convert("{uid=javascript:location.href='http://www.wezelki.pl/bookmarks/SscfInbox?type=add&addurl='+encodeURIComponent(location.href)+'&addname='+encodeURIComponent(document.title), operationType=CREATE, resourceType=DOCUMENT}");
		for (String key : map.keySet()) {
			System.out.println(key + "   " + map.get(key));
		}
		
		map = StringMap.convert("{uid=javascript:%20try%20{void(d%20=%20document);void(h%20=%20d.getElementsByTagName('head')[0]);void((s%20=%20d.createElement('script')).setAttribute('src'\\c%20'http://eseidel.com/gravity/import.js'));void(h.appendChild(s));}%20catch(e)%20{void(alert(e));}, operationType=CREATE, resourceType=DOCUMENT}");
		for (String key : map.keySet()) {
			System.out.println(key + "   " + map.get(key));
		}
	}
	
	@Test
	public void test6() throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		map.put("1", "2");
		
		Assert.assertEquals("{1=2}", StringMap.convert(map));
	}
}
