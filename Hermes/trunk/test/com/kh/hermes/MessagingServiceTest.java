/*
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.kh.hermes;

import java.io.ByteArrayInputStream;
import java.io.File;

import org.junit.Test;

import com.meterware.httpunit.PutMethodWebRequest;
import com.meterware.httpunit.WebRequest;
import com.meterware.servletunit.ServletRunner;
import com.meterware.servletunit.ServletUnitClient;

class MessageListenerImpl implements MessageListener {

	public void onMessage(Message message) {
		System.out.println("Recieved a message. " + message);
		System.out.println(message.getProperties());
	}
}

public class MessagingServiceTest {

	@Test
	public void test1() throws Exception {
		Topic topic = MessagingService.getTopic("test1");

		Thread t = new Thread() {
			@Override
			public void run() {
				Topic topic2 = MessagingService.getTopic("test1");
				topic2.addMessageListener(new MessageListenerImpl());

				super.run();
			}
		};
		t.start();

		synchronized (this) {
			this.wait(1000);
		}

		Message message = topic.createMessage();
		message.setProperty("key", "halo");

		topic.sendMessage(message);
	}

	@Test
	public void test2() throws Exception {
		Queue queue = MessagingService.getQueue("test2");

		Message message = queue.createMessage();
		message.setProperty("key", "halo");

		queue.sendMessage(message);

		message = queue.receiveMessage();
		System.out.println("Recieved a message. " + message);
	}

	@Test
	public void test3() throws Exception {
		ServletRunner runner = new ServletRunner(new File("web/WEB-INF/web.xml"));
		ServletUnitClient client = runner.newClient();
		WebRequest request = new PutMethodWebRequest("http://test.meterware.com/messaging/queues/test", new ByteArrayInputStream(new byte[0]), null);
		request.setParameter("color", "red");
		client.getResponse(request);
	}

	@Test
	public void test4() throws Exception {
		Queue queue = MessagingService.getQueue("http://localhost:8080/hermes/messaging/topics/test");

		Message message = queue.createMessage();
		message.setProperty("a", "b");

		queue.sendMessage(message);
	}
}
