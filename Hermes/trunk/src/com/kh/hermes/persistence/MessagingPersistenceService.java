/*
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.kh.hermes.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.kh.hermes.Message;
import com.kh.hermes.MessagingException;

public class MessagingPersistenceService {

	private static EntityManagerFactory entityManagerFactory_;

	private static MessagingPersistenceService instance;

	public synchronized static MessagingPersistenceService getInstance() {
		if (instance == null) {
			instance = new MessagingPersistenceService();
		}

		return instance;
	}

	private MessagingPersistenceService() {
		entityManagerFactory_ = Persistence.createEntityManagerFactory("messaging");
	}

	public void createMessage(Message message) throws MessagingException {
		EntityManager entityManger = null;
		try {
			entityManger = entityManagerFactory_.createEntityManager();
			entityManger.getTransaction().begin();
			entityManger.persist(message);
			entityManger.getTransaction().commit();
		} catch (Exception e) {
			throw new MessagingException("Exception occured while creating a message: " + message, e);
		} finally {
			if (entityManger != null) {
				entityManger.close();
			}
		}
	}

	public void destroy() {
		entityManagerFactory_.close();
	}

	@SuppressWarnings("unchecked")
	public List<Message> findUnreceivedMessagesBySessionName(String sessionName) {
		List<Message> messages = null;

		EntityManager entityManger = null;
		try {
			entityManger = entityManagerFactory_.createEntityManager();
			Query query = entityManger.createQuery("SELECT message FROM Message message WHERE received = null AND sessionname = ? ORDER BY sent DESC");
			query.setParameter(1, sessionName);
			messages = query.getResultList();
		} finally {
			if (entityManger != null) {
				entityManger.close();
			}
		}

		return messages;
	}

	public Message updateMessage(Message message) throws MessagingException {
		Message entity = null;

		EntityManager entityManger = null;
		try {
			entityManger = entityManagerFactory_.createEntityManager();
			entityManger.getTransaction().begin();
			entity = entityManger.merge(message);
			entityManger.getTransaction().commit();
		} catch (Exception e) {
			throw new MessagingException("Exception occured while creating a message: " + message, e);
		} finally {
			if (entityManger != null) {
				entityManger.close();
			}
		}

		return entity;
	}
}
