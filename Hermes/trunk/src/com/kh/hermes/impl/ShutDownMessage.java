/*
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.kh.hermes.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import com.kh.hermes.Message;

/**
 * This message is send to queues to inform them about shut down.
 * 
 */
public class ShutDownMessage implements Message {

	private Date createDate_ = new Date();

	private Date receiveDate_;

	private Date sendDate_;

	private String sessionName_;

	public ShutDownMessage(String sessionName) {
		this.sessionName_ = sessionName;
	}

	public Date getCreateDate() {
		return createDate_;
	}

	public Serializable getObject() {
		return null;
	}

	public Map<String, String> getProperties() {
		throw new UnsupportedOperationException();
	}

	public String getProperty(String key) {
		return null;
	}

	public Date getReceiveDate() {
		return receiveDate_;
	}

	public Date getSendDate() {
		return sendDate_;
	}

	public String getSessionName() {
		return sessionName_;
	}

	public String getUUID() {
		return null;
	}

	public void setProperty(String key, String value) {
		throw new UnsupportedOperationException();
	}

	public void setReceiveDate(Date receiveDate) {
		this.receiveDate_ = receiveDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate_ = sendDate;
	}
}
