/*
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.kh.hermes.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kh.hermes.Message;
import com.kh.hermes.MessageListener;
import com.kh.hermes.MessagingException;
import com.kh.hermes.MessagingService;
import com.kh.hermes.Topic;
import com.kh.hermes.persistence.MessagingPersistenceService;

class MessageListenerThread extends Thread {

	private final Logger logger = LoggerFactory.getLogger(MessagingService.class);

	private MessageListener messageListener_;

	private BlockingQueue<Message> queue_ = new LinkedBlockingQueue<Message>();

	public MessageListenerThread(MessageListener messageListener) {
		this.messageListener_ = messageListener;
	}

	public void addMessage(Message message) {
		queue_.add(message);
	}

	@Override
	public void run() {
		logger.debug("Starting listener thread.");

		while (true) {
			try {
				Message message = queue_.take();
				messageListener_.onMessage(message);

				if (message instanceof ShutDownMessage) {
					break;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		logger.debug("Stoping listener thread.");
	}
}

public class TopicImpl extends AbstractSession implements Topic {

	private Map<MessageListener, MessageListenerThread> listenersToThreads_ = new HashMap<MessageListener, MessageListenerThread>();

	public TopicImpl(String name) {
		super(name);
	}

	public void addMessageListener(MessageListener messageListener) {
		MessageListenerThread thread = new MessageListenerThread(messageListener);
		synchronized (listenersToThreads_) {
			listenersToThreads_.put(messageListener, thread);
		}

		thread.start();
	}

	public void removeMessageListener(MessageListener messageListener) {
		synchronized (listenersToThreads_) {
			listenersToThreads_.get(messageListener).addMessage(new ShutDownMessage(name_));
			listenersToThreads_.remove(messageListener);
		}
	}

	public void sendMessage(Message message) throws MessagingException {
		message.setSendDate(new Date());
		MessagingPersistenceService.getInstance().createMessage(message);

		synchronized (listenersToThreads_) {
			for (MessageListenerThread thread : listenersToThreads_.values()) {
				thread.addMessage(message);
			}
		}

		message.setReceiveDate(new Date());
		MessagingPersistenceService.getInstance().updateMessage(message);
	}

	public void shutDown() {
		synchronized (listenersToThreads_) {
			for (MessageListener listener : listenersToThreads_.keySet()) {
				listenersToThreads_.get(listener).addMessage(new ShutDownMessage(name_));
			}
		}
	}
}
