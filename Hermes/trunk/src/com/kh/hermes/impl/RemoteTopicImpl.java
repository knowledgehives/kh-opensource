/*
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.kh.hermes.impl;

import java.util.Date;

import com.kh.hermes.Message;
import com.kh.hermes.MessageListener;
import com.kh.hermes.MessagingException;
import com.kh.hermes.Topic;
import com.kh.hermes.persistence.MessagingPersistenceService;
import com.kh.hermes.servlets.MessagingServiceServlet;

public class RemoteTopicImpl extends AbstractSession implements Topic {

	public RemoteTopicImpl(String name) {
		super(name);
	}

	public void addMessageListener(MessageListener messageListener) {
		throw new UnsupportedOperationException("Remote queue does not support this method.");
	}

	public Message receiveMessage() {
		throw new UnsupportedOperationException("Remote queue does not support this method.");
	}

	public void removeMessageListener(MessageListener messageListener) {
		throw new UnsupportedOperationException("Remote queue does not support this method.");
	}

	public void sendMessage(Message message) throws MessagingException {
		message.setSendDate(new Date());
		MessagingPersistenceService.getInstance().createMessage(message);

		new MessagingServiceServlet().sendMessage(message);

		message.setReceiveDate(new Date());
		MessagingPersistenceService.getInstance().updateMessage(message);
	}
}
