/*
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.kh.hermes.impl;

import java.util.Collections;
import java.util.Date;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.kh.hermes.Message;
import com.kh.hermes.util.StringMap;

@Entity(name = "Message")
@Table(name = "message")
public class MessageImpl implements Message {

	@Basic
	@Column(name = "created")
	private Date createDate_;

	@Id
	@Column(name = "id")
	@GeneratedValue
	private Long id_;

	@Lob
	@Column(name = "properties")
	private String properties_ = "{}";

	@Basic
	@Column(name = "received")
	private Date receiveDate_;

	@Basic
	@Column(name = "sent")
	private Date sendDate_;

	@Basic
	@Column(name = "sessionname")
	private String sessionName_;

	@Basic
	@Column(name = "uuid")
	private String uuid_;

	public MessageImpl() {
		this.createDate_ = new Date();
	}

	public Date getCreateDate() {
		return createDate_;
	}

	public Long getId() {
		return id_;
	}

	public Map<String, String> getProperties() {
		return Collections.unmodifiableMap(StringMap.convert(properties_));
	}

	public String getProperty(String key) {
		return StringMap.convert(properties_).get(key);
	}

	public Date getReceiveDate() {
		return receiveDate_;
	}

	public Date getSendDate() {
		return sendDate_;
	}

	public String getSessionName() {
		return sessionName_;
	}

	public String getUUID() {
		return uuid_;
	}

	public void setProperty(String key, String value) {
		Map<String, String> properties = StringMap.convert(properties_);
		properties.put(key, value);
		properties_ = StringMap.convert(properties);
	}

	public void setReceiveDate(Date receiveDate) {
		receiveDate_ = receiveDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate_ = sendDate;
	}

	public void setSessionName(String sessionName) {
		this.sessionName_ = sessionName;
	}

	public void setUUID(String uuid) {
		this.uuid_ = uuid;
	}

	@Override
	public String toString() {
		return uuid_;
	}
}
