/*
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.kh.hermes.impl;

import java.util.Date;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kh.hermes.Message;
import com.kh.hermes.MessagingException;
import com.kh.hermes.Queue;
import com.kh.hermes.persistence.MessagingPersistenceService;

public class QueueImpl extends AbstractSession implements Queue {

	private static final Logger logger = LoggerFactory.getLogger(Queue.class);

	private BlockingQueue<Message> queue_ = new LinkedBlockingQueue<Message>();

	public QueueImpl(String name) {
		super(name);

		List<Message> messages = MessagingPersistenceService.getInstance().findUnreceivedMessagesBySessionName(name);
		for (Message message : messages) {
			queue_.add(message);
		}
	}

	public Message receiveMessage() {
		Message message = null;
		try {
			message = queue_.take();

			message.setReceiveDate(new Date());
			try {
				if (message instanceof MessageImpl) {
					message = MessagingPersistenceService.getInstance().updateMessage(message);
				}
			} catch (MessagingException e) {
				logger.error(e.getMessage(), e);
			}
		} catch (InterruptedException e) {
			logger.error(e.getMessage(), e);
		}

		return message;
	}

	public void sendMessage(Message message) throws MessagingException {
		message.setSendDate(new Date());
		MessagingPersistenceService.getInstance().createMessage(message);

		queue_.add(message);
	}

	public void shutDown() {
		queue_.clear();
		queue_.add(new ShutDownMessage(name_));
	}
}
