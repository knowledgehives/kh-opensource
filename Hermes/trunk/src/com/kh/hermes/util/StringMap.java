/*
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.kh.hermes.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Converts Map<String, String> to String and vice-versa.
 * 
 * @author Mariusz Cygan
 *
 */
public class StringMap {

	public static String convert(Map<String, String> map) {
		StringBuilder builder = new StringBuilder("{");
		for (String key : map.keySet()) {
			builder.append(escape(key)).append('=').append(escape(map.get(key))).append(", ");
		}
		if (builder.length() > 1) {
			builder.delete(builder.length() - 2, builder.length());
		}
		builder.append("}");

		return builder.toString();
	}

	public static Map<String, String> convert(String string) {
		Map<String, String> map = new HashMap<String, String>();
		if (string != null && string.length() > 2) {
			for (String s : string.replaceAll("^\\{|\\}$", "").split("[,;] ?")) {
				s = unescape(s);
				String key = s.substring(0, s.indexOf('='));
				String value = s.substring(s.indexOf('=') + 1);
				map.put(key, value);
			}
		}

		return map;
	}

	private static String escape(String string) {
		return string.replace(",", "\\c").replace(";", "\\s");
	}

	private static String unescape(String string) {
		return string.replace("\\c", ",").replace("\\s", ";");
	}
}
