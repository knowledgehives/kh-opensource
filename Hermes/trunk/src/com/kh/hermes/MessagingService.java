/*
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.kh.hermes;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kh.hermes.impl.QueueImpl;
import com.kh.hermes.impl.RemoteQueueImpl;
import com.kh.hermes.impl.RemoteTopicImpl;
import com.kh.hermes.impl.TopicImpl;
import com.kh.hermes.persistence.MessagingPersistenceService;

/**
 * The Messaging Service.
 * 
 */
public class MessagingService {

	private static final Logger logger = LoggerFactory.getLogger(MessagingService.class);

	private static Map<String, Queue> queuesByName_ = new HashMap<String, Queue>();

	private static final String REMOTE_SESSION_NAME_PREFIX = "http";

	private static Map<String, Topic> topicsByName_ = new HashMap<String, Topic>();

	/**
	 * Returns the {@link Queue}.
	 * 
	 * @return queue
	 */
	public static Queue getQueue(String name) {
		Queue queue;
		synchronized (queuesByName_) {
			if (queuesByName_.containsKey(name)) {
				queue = queuesByName_.get(name);
			} else {
				if (name.startsWith(REMOTE_SESSION_NAME_PREFIX)) {
					queue = new RemoteQueueImpl(name);
				} else {
					queue = new QueueImpl(name);
				}

				queuesByName_.put(name, queue);
			}
		}

		return queue;
	}

	/**
	 * Returns the {@link Topic}.
	 * 
	 * @return topic
	 */
	public static Topic getTopic(String name) {
		Topic topic;
		synchronized (topicsByName_) {
			if (topicsByName_.containsKey(name)) {
				topic = topicsByName_.get(name);
			} else {
				if (name.startsWith(REMOTE_SESSION_NAME_PREFIX)) {
					topic = new RemoteTopicImpl(name);
				} else {
					topic = new TopicImpl(name);
				}

				topicsByName_.put(name, topic);
			}
		}

		return topic;
	}

	public static void shutDown() {
		logger.info("Shutting down messaging service.");

		synchronized (queuesByName_) {
			for (Queue queue : queuesByName_.values()) {
				if (queue instanceof QueueImpl) {
					((QueueImpl) queue).shutDown();
				}
			}
		}
		synchronized (topicsByName_) {
			for (Topic topic : topicsByName_.values()) {
				if (topic instanceof TopicImpl) {
					((TopicImpl) topic).shutDown();
				}
			}
		}
		
		MessagingPersistenceService.getInstance().destroy();
	}
}
