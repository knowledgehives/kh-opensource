/*
 * Copyright (c) 2008-2009, 
 * 
 * Knowledge Hives Sp. z o. o.
 * http://www.knowledgehives.com/
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of Knowledge Hives Sp. z o. o. nor the names of its 
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS 
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package com.kh.hermes.servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;

import com.kh.hermes.Message;
import com.kh.hermes.MessagingException;
import com.kh.hermes.MessagingService;
import com.kh.hermes.Session;
import com.kh.hermes.util.StringMap;

public class MessagingServiceServlet extends HttpServlet {

	private static final String QUEUES_TYPE = "/queues";

	private static final long serialVersionUID = 5425691810427981395L;

	private static final String TOPICS_TYPE = "/topics";

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().write("Messaging\n");
	}

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] strings = request.getPathInfo().split("/");
		if (strings.length != 4) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
		String name = strings[2];
		String uuid = strings[3];

		BufferedReader reader = request.getReader();
		StringBuilder builder = new StringBuilder();
		char[] chars = new char[1024];
		int i = reader.read(chars);
		while (i > 0) {
			builder.append(chars, 0, i);
			i = reader.read(chars);
		}
		reader.close();
		String properties = builder.toString();

		Session session = null;
		if (request.getPathInfo().startsWith(QUEUES_TYPE)) {
			session = MessagingService.getQueue(name);
		} else if (request.getPathInfo().startsWith(TOPICS_TYPE)) {
			session = MessagingService.getTopic(name);
		} else {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}

		Message message = session.createMessage(uuid);
		for (Entry<String, String> entry : StringMap.convert(properties).entrySet()) {
			message.setProperty(entry.getKey(), entry.getValue());
		}

		try {
			session.sendMessage(message);
		} catch (MessagingException e) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	public int sendMessage(Message message) throws MessagingException {
		HttpClient client = new HttpClient();
		PutMethod method = new PutMethod(message.getSessionName() + '/' + message.getUUID());

		String properties = StringMap.convert(message.getProperties());
		try {
			method.setRequestEntity(new StringRequestEntity(properties, "text/plain", "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new MessagingException(e.getMessage() + " for " + message.getSessionName(), e);
		}

		int statusCode;
		try {
			statusCode = client.executeMethod(method);
			if (statusCode != HttpStatus.SC_OK) {
				throw new MessagingException(statusCode + " for " + message.getSessionName());
			}
		} catch (HttpException e) {
			throw new MessagingException(e.getMessage() + " for " + message.getSessionName(), e);
		} catch (IOException e) {
			throw new MessagingException(e.getMessage() + " for " + message.getSessionName(), e);
		} finally {
			method.releaseConnection();
		}

		return statusCode;
	}
}
